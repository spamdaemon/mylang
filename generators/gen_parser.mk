$(GEN_DIR)/parser.h $(OBJECT_DIR)/gen-lib-src/parser.o: $(BASE_DIR)/lib-src/mylang/grammar/grammar.y $(BASE_DIR)/lib-src/mylang/grammar/grammar.l
	@mkdir -p $(GEN_DIR) $(OBJECT_DIR)/gen-lib-src/;
	@../parasol/projects/idioma/tools/gen_parser.sh \
	-l $(BASE_DIR)/lib-src/mylang/grammar/grammar.l \
	-y $(BASE_DIR)/lib-src/mylang/grammar/grammar.y \
	$(addprefix -I,. $(BASE_INC_SEARCH_PATH) $(PROJ_INC_SEARCH_PATH)) \
	-g "mylang::grammar::PAst" \
	-G "mylang/grammar/PAst.h" \
	-O $(OBJECT_DIR)/gen-lib-src/parser.o \
	-H $(GEN_DIR)/parser.h \
	-n "mylang::grammar::parser" \
	;
