package {{support.package}};

public final class {{support.class}} {

	private static final java.nio.charset.Charset UTF8 = java.nio.charset.Charset.forName("UTF-8");

	private {{support.class}} ()
	{}
	
	/**
	 * Convert a string to bytes
	 * @param str a string
	 * @return the string encoded as UTF8 bytes
	 */
	public static byte[] char2bytes(char ch)
	{
		return java.lang.Character.toString(ch).getBytes(UTF8);
	}
	
	/**
	 * Parse bytes as a string
	 * @param str a string
	 * @return the string encoded as UTF8 bytes
	 */
	public static char bytes2char(byte[] bytes)
	{
		java.lang.String str = new java.lang.String(bytes,UTF8);
		if (str.length()!=1) {
			throw new java.lang.RuntimeException("Invalid byte array for characters");
		} 
		return str.charAt(0);
	}
	

}
