package {{support.package}};

public final class {{support.class}} {

	private static final java.nio.charset.Charset UTF8 = java.nio.charset.Charset.forName("UTF-8");

	private {{support.class}} ()
	{}
	
	/**
	 * Convert a string to bytes
	 * @param str a string
	 * @return the string encoded as UTF8 bytes
	 */
	public static byte[] string2bytes(java.lang.String str)
	{
		return str.getBytes(UTF8);
	}
	
	/**
	 * Parse bytes as a string
	 * @param str a string
	 * @return the string encoded as UTF8 bytes
	 */
	public static java.lang.String bytes2string(byte[] bytes)
	{
		return new java.lang.String(bytes,UTF8);
	}
	

}
