package {{runtime.package}};

public final class {{runtime.class}} {

	private static final java.io.charset.Charset UTF8 = new java.io.charset.Charset("UTF-8");

	private {{runtime.class}} ()
	{}
}
