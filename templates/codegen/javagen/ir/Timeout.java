package {{timeout.package}};

public final class {{timeout.class}} {

	public static final {{timeout.class}} NONE = new {{timeout.class}}(0);

	/** The duration in millis */
	private final long timeoutMS;
	private final long timeoutUS;
	private final long timeoutNS;

	public {{timeout.class}}(int timeoutMs) {
		if (timeoutMs < 0) {
			throw new java.lang.IllegalArgumentException("Negative timeout");
		}
		this.timeoutMS = timeoutMs;
		this.timeoutUS = 1000L * timeoutMs;
		this.timeoutNS = 1000L * timeoutUS;
	}

	/**
	 * Get the timeout in millis
	 * 
	 * @return the time out in millis
	 */
	public final long timeoutMS() {
		return timeoutMS;
	}

	/**
	 * Get the timeout in micros
	 * 
	 * @return the time out in micros
	 */
	public final long timeoutUS() {
		return timeoutUS;
	}

	/**
	 * Get the timeout in nananos
	 * 
	 * @return the time out in nanos
	 */
	public final long timeoutNS() {
		return timeoutNS;
	}

	public final boolean equals(Object obj) {
		return obj == this || (obj instanceof {{timeout.class}} && (({{timeout.class}}) obj).timeoutNS == timeoutNS);
	}

	public final int hashCode() {
		return java.lang.Long.hashCode(timeoutNS);
	}
}
