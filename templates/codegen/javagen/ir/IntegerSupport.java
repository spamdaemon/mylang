package {{support.package}};

public final class {{support.class}} {


	private {{support.class}} ()
	{}
	
	private static byte[] reverse(byte[] bytes)
	{
		if (bytes.length<2) {
			return bytes;
		}
		byte[] res = new byte[bytes.length];
		int i=0;
		int j=bytes.length-1;
		while(i<=j) { // make sure to copy the value where i==j
			res[j]=bytes[i];
			res[i]=bytes[j];
			++i;
			--j;
		}
		
		return res;
	}
	
	private static void reverse_self(byte[] bytes)
	{
		if (bytes.length<2) {
			return;
		}
		int i=0;
		int j=bytes.length-1;
		while(i<j) {  // no need to swap the value where i==j
			byte b = bytes[i];
			bytes[i]=bytes[j];
			bytes[j]=b;
			++i;
			--j;
		}
	}
	
	/**
	 * Convert a string to bytes
	 * @param str a string
	 * @return the string encoded as UTF8 bytes
	 */
	public static byte[] integer2bytes(java.math.BigInteger value)
	{
		byte[] bytes = value.toByteArray();
		reverse_self(bytes);
		return bytes;
	}
	
	/**
	 * Parse bytes as a string
	 * @param str a string
	 * @return the string encoded as UTF8 bytes
	 */
	public static java.math.BigInteger bytes2integer(byte[] bytes)
	{
		return new java.math.BigInteger(reverse(bytes));
	}
	
	/**
	 * Convert a string to bytes
	 * @param str a string
	 * @return the string encoded as UTF8 bytes
	 */
	public static boolean[] integer2bits(java.math.BigInteger value)
	{
		byte[] bytes = integer2bytes(value);
		boolean[] bits = new boolean[1+value.bitLength()];
		assert bits.length <= bytes.length*8 : "Internal error for integer to bits "+value;
		for (int i=0;i<bits.length;++i) {
        	byte b = bytes[i/8];
			bits[i] = (b & (1<< (i%8))) != 0;;
		}
        return bits;
	}
	
	/**
	 * Parse bytes as a string
	 * @param str a string
	 * @return the string encoded as UTF8 bytes
	 */
	public static java.math.BigInteger bits2integer(boolean[] bits)
	{
		byte[] bytes = new byte[(bits.length+7)/8];
		// if the last bit is set, then we have a negative integer
		// TODO: we should into account the bounds of the integer type 
		if (bits[bits.length-1]) {
			bytes[bytes.length-1] = (byte)0xff;
		}
		for (int i=0;i<bits.length;++i) {
			int b = bytes[i/8];
			int mask = 1<<(i%8);
			b = bits[i] ? (b | mask) : (b & ~mask);
			bytes[i/8] = (byte)b; 
		}
		reverse_self(bytes);		
		return new java.math.BigInteger(bytes);
	}
	

}
