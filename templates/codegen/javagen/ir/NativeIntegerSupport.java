package {{support.package}};

public final class {{support.class}} {

    private static final int N_LONG_BITS = 64;
	private static final int N_BYTES = {{type.size.bytes}};
	private static final int N_BITS = N_BYTES*8;
	private static final {{type.name}} ONE = 1;
	
	private {{support.class}} ()
	{}
	
	/**
	 * Count the number of bits required to represent the number.
	 * @param value a value
	 * @return the number of bits required to represent the signed value
	 */
	private static int countBits({{type.name}} value)
	{
		int n = java.lang.Long.numberOfLeadingZeros(value >= 0 ? value : ~value);
		if (n==N_LONG_BITS) {
			return 1; // always need at least 1 bit
		}
		// need 1 extra bit to indicate the sign of the original value
		// so, -1 only requires 1 bit, but the value 1 requires 2 bits 01
		return 1+N_LONG_BITS - n;
	}
	
	/**
	 * Count the number of bytes required to represent the number.
	 * @param value a value
	 * @return the number of bits required to represent the signed value
	 */
	private static int countBytes({{type.name}} value)
	{
		return (countBits(value)+7)/8;
	}
	
	
	/**
	 * Convert a string to bytes
	 * @param str a string
	 * @return the string encoded as UTF8 bytes
	 */
	public static byte[] integer2bytes({{type.name}} value)
	{
		byte[] res = new byte[countBytes(value)];
		for (int i=0;i<res.length;++i) {
			res[i] = (byte)((value>> (i*8)) & 0x00ff);
		}
		return res;		
	}
	
	/**
	 * Parse bytes as a string
	 * @param str a string
	 * @return the string encoded as UTF8 bytes
	 */
	public static {{type.name}} bytes2integer(byte[] bytes)
	{
		if (bytes.length > N_BYTES) {
			throw new java.lang.RuntimeException("Internal error: too many bytes. expected at most "+N_BYTES+", but got "+bytes.length);
		}
		{{type.name}} res = 0;
		for (int i=0;i<bytes.length;++i) {
			res |= ({{type.name}})((({{type.name}})(bytes[i] & 0x00ff)) << (i*8));
		}
		return res;
	}
	
	/**
	 * Convert an integer to bits 
	 * @param value an integer
	 * @return an array of booleans
	 */
	public static boolean[] integer2bits({{type.name}} value)
	{
		boolean[] bits = new boolean[countBits(value)];
		for (int i=0;i<bits.length;++i) {
			{{type.name}} mask = ({{type.name}})(ONE << i);
			bits[i]  = (value & mask) == mask;
		}
        return bits;
	}
	
	/**
	 * Convert an array of bits into an integer.
	 * @param bits the bit array
	 * @return the value
	 */
	public static {{type.name}} bits2integer(boolean[] bits)
	{
		if (bits.length>N_BITS) {
			throw new java.lang.RuntimeException("Internal error: too many bits. expected at most "+N_BITS+", but got "+bits.length);
		}
		{{type.name}} res=0;
		for (int i=0;i<bits.length;++i) {
			if (bits[i]) {
				res |= ({{type.name}})(ONE << i);
			}
		}
		return res;
	}
	

}
