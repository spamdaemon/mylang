package {{descriptor.package}};

public abstract class {{descriptor.class}} {

	private static final class Monitor {

		private boolean signalled = false;

		private Monitor() {}

		public synchronized final boolean await({{timeout.fullname}} timeout) {
			if (!signalled) {
				if (!{{timeout.fullname}}.NONE.equals(timeout)) {
					try {
						if (timeout == null) {
							wait();
						} else {
							long nanos = timeout.timeoutNS();
							wait(nanos / 1000000, (int) (nanos % 1000000));
						}
					} catch (java.lang.InterruptedException e) {
					}
				}
			}
			return signalled;
		}

		public synchronized final void signal() {
			this.signalled = true;
			this.notify();
		}
	}

	private Monitor monitor = null;
/*
	private final java.util.List<Monitor> monitors = new java.util.ArrayList<Monitor>();
*/

	protected {{descriptor.class}}()
	{}
	
	/**
	 * Signal this descriptor that new data is available.
	 */
	public void signal() {

		if (monitor != null) {
			monitor.signal();
		}
/*
		synchronized(monitors) {
			for (Monitor m : monitors) {
				m.signal();
			}
		}
*/
	}

	public void add(Monitor m)
	{ 
		monitor=m;
/*
		synchronized(monitors) {
			monitors.add(m);
		}
*/
	}
	
	public void remove(Monitor m)
	{ 
		monitor=null;
/*
		synchronized(monitors) {
			monitors.remove(m);
		}
*/
	}

	/**
	 * Notify this descriptor that a new monitor needs to be notified of events.
	 * Currently only 1 monitor needs to be supported at any given time.
	 * 
	 * @param m a monitor
	 */
	public abstract void beginAwait();

	/**
	 * Notify this descriptor that the specified monitor should no longer be
	 * notified. If this descriptor has pending event and implicitConsume is true,
	 * then the value should be consumed. If the descriptor is a writer, then
	 * implictConsume is ignored.
	 * 
	 * @param m
	 * @param implicitConsume
	 */
	public abstract void endAwait(boolean implicitConsume);

	/**
	 * Test if this descriptor satisfies the event conditions.
	 * 
	 * @return true if the event conditions are satisfied by this descriptor.
	 */
	public abstract long test();

	/**
	 * Determine if this descriptor is shutdown
	 */
	public abstract boolean isShutdown();
	
	/**
	 * Perform a poll, but return at most one of the readers. If a reader is
	 * returned, it is the one that has the oldest time.
	 * 
	 * @param D
	 * @param timeout
	 * @param implicitConsume
	 * @return
	 */
	public static {{descriptor.class}}[] poll({{descriptor.class}}[] D, {{timeout.fullname}} timeout, boolean implicitConsume,
			boolean pollAtMostOneReader) {
			
		if (D.length == 0) {
			return D;
		}
		Monitor m = new Monitor();
		java.util.HashSet<{{descriptor.class}}> ports = new java.util.HashSet<>();
		java.util.HashSet<{{descriptor.class}}> result = new java.util.HashSet<>();

		ports.addAll(java.util.Arrays.asList(D));
		
		{{descriptor.class}}[] best = new {{descriptor.class}}[] { null };
		long[] bestTime = new long[] { -1 };

		final java.util.function.Consumer<{{descriptor.class}}> collector;

		if (pollAtMostOneReader) {
			collector = d -> {
				long tm = d.test();
				if (tm > 0) {
					if (best[0] == null || bestTime[0] > tm) {
						best[0] = d;
						bestTime[0] = tm;
					}
				} else if (tm == 0) {
					result.add(d);
				}
			};
		} else {
			collector = d -> {
				long tm = d.test();
				if (tm >= 0) {
					result.add(d);
				}
			};
		}

		ports.forEach(d -> {
			d.add(m);
			d.beginAwait();
			collector.accept(d);
		});
		if (best[0] == null && result.isEmpty()) {
			m.await(timeout);
			ports.forEach(d -> {
				collector.accept(d);
			});
		}
		if (best[0] != null) {
			result.add(best[0]);
		}

		ports.forEach(d -> {
			d.remove(m);
			d.endAwait(implicitConsume ? (best[0] == null || best[0] == d) : false);
		});
		return result.toArray(new {{descriptor.class}}[result.size()]);
	}
}
