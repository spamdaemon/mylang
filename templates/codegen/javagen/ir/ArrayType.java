package {{array.package}};

public class {{array.class}} {

	private static final class Subrange extends {{array.class}} {
		private final int offset;
		private final int len;
		private Subrange({{element.type}}[] elements, int startOffset, int endOffset)
		{
			super(elements);
			this.offset=startOffset;
			this.len=endOffset-startOffset;
		}
		public {{element.type}}[] toArray() {
		    {{element.type}}[] res = new {{element.type}}[this.len];
		    java.lang.System.arraycopy(elements,this.offset,res,0,this.len);
		    return res;
		}
		public int length() { return this.len; }
		public {{element.type}} get(int index)
		{
			return elements[index+offset];
		}
		public {{array.class}} subrange(int off, int len)
		{
			return new Subrange(elements,offset+off,len);	
		}
		protected void copy({{element.type}}[] dest, int offset)
		{
			java.lang.System.arraycopy(elements,this.offset,dest,offset,this.len);
		}
	}


	protected final {{element.type}}[] elements;

	private {{array.class}}({{element.type}}[] elements) {
		this.elements = elements;
	}
	
	public static {{array.class}} create({{element.type}}[] elements) {
	    return new {{array.class}}(elements);
	}
	
	public static {{array.class}} create({{element.type}} e) {
	    return new {{array.class}}(new {{element.type}}[] { e });
	}
	
	public static {{array.class}} create({{array.class}} lhs, {{array.class}} rhs)
	{
	    final int n_lhs = lhs.length();
	    final int n_rhs = rhs.length();
	    if (n_lhs==0) {
		return rhs;
	    }
	    if (n_rhs==0) {
		return lhs;
	    }
	    
	    final {{element.type}}[] res = new {{element.type}}[n_lhs+n_rhs];
	    lhs.copy(res,0);
	    rhs.copy(res,n_lhs);
	    return new {{array.class}}(res);
	}

	public {{element.type}}[] toArray() { return elements; }
	public int length() { return elements.length; }
	public {{element.type}} get(int index) { return elements[index]; }

	public {{array.class}} subrange(int startOff, int endOff)
	{
	    return new Subrange(elements,startOff,endOff);	
	}
	
	public void copy({{element.type}}[] dest, int offset)
	{
		java.lang.System.arraycopy(elements,0,dest,offset,elements.length);
	}
	public String toString()
	{
	    return elements.toString();
	}
}
