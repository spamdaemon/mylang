package {{port.package}};

public final class {{port.class}} {

	private final  java.util.concurrent.locks.ReentrantLock lock = new  java.util.concurrent.locks.ReentrantLock();
	private final  java.util.concurrent.locks.Condition readable = lock.newCondition();
	private final  java.util.concurrent.locks.Condition writable = lock.newCondition();

	private boolean isWriteShutdown = false;

	private boolean isReadShutdown = false;
	private boolean isNew = false;
	private {{opt.element.fullname}} current = {{opt.element.nil}};

	/** The port buffer */
	private final int bufferSize;

	/** start counter at 1, because 0 has a special meaning */
	private static final java.util.concurrent.atomic.AtomicLong readCounter = new java.util.concurrent.atomic.AtomicLong(1);

	private static final class Readable {
		private final {{element.fullname}} value;
		private final long time;

		private Readable({{element.fullname}} value) {
			this.value = value;
			this.time = readCounter.getAndIncrement();
		}
	}

	private final java.util.LinkedList<Readable> buffer = new java.util.LinkedList<>();

	public final {{input.fullname}} reader = new {{input.fullname}}(this);
	public final {{output.fullname}} writer = new {{output.fullname}}(this);

	public {{port.class}}(int bufferSize) {
		this.bufferSize = bufferSize;
	}

	public {{port.class}}() {
		this(1);
	}

	public void doLocked(java.lang.Runnable run) {
		lock.lock();
		try {
			run.run();
		} finally {
			lock.unlock();
		}
	}

	public void assertLocked() {
		assert this.lock.isHeldByCurrentThread();
	}


	public boolean write({{element.fullname}} obj, {{timeout.fullname}} timeout) {
		lock.lock();
		try {
			if (awaitWritable(timeout)) {
				buffer.add(new Readable(obj));
				readable.signal();
				reader.signal();
				return true;
			}
			return false;
		} finally {
			lock.unlock();
		}
	}

	public boolean awaitWritable({{timeout.fullname}} timeout) {
		lock.lock();
		try {
			if (isFull() && !isWriteShutdown && !isReadShutdown) {
				if (timeout == null) {
					writable.await();
				} else if (!{{timeout.fullname}}.NONE.equals(timeout)) {
					writable.await(timeout.timeoutUS(), java.util.concurrent.TimeUnit.MICROSECONDS);
				}
			}
			if (isReadShutdown) {
				shutdownWrite();
			}
			return !isFull() && !isWriteShutdown;
		} catch (java.lang.InterruptedException e) {
			return false;
		} finally {
			lock.unlock();
		}
	}

	void clearNew() {
		lock.lock();
		try {
			isNew = false;
		} finally {
			lock.unlock();
		}
	}

	public long awaitReadable({{timeout.fullname}} timeout) {
		lock.lock();
		try {
			isNew = false;
			if (isEmpty() && !isReadShutdown && !isWriteShutdown) {
				if (timeout == null) {
					readable.await();
				} else if (!{{timeout.fullname}}.NONE.equals(timeout)) {
					readable.await(timeout.timeoutUS(), java.util.concurrent.TimeUnit.MICROSECONDS);
				}
			}
			if (isEmpty() && isWriteShutdown) {
				shutdownRead();
			}
			if (isEmpty() || isReadShutdown) {
				return -1;
			}
			return buffer.getFirst().time;
		} catch (java.lang.InterruptedException e) {
			return -1;
		} finally {
			lock.unlock();
		}
	}

	public void consume({{timeout.fullname}} timeout)
	{
		read(timeout);
	}
	
	public {{opt.element.fullname}} read({{timeout.fullname}} timeout) {
		lock.lock();
		try {
			if (awaitReadable(timeout) >= 0) {
				{{element.fullname}} e = buffer.removeFirst().value;
				current =  {{make_optional_element_e}};
				isNew = true;
				writable.signal();
				writer.signal();
				return current;
			} else {
				if (isWriteShutdown) {
					shutdownRead();
				}
				return {{opt.element.nil}};
			}
		} finally {
			lock.unlock();
		}
	}

	public void shutdownWrite() {
		lock.lock();
		try {
			isWriteShutdown = true;
			writable.signalAll();
			writer.signal();
			// if there is no more data in the buffer, then may sure to wake up any reader as well
			if (isEmpty() && !isReadShutdown) {
				shutdownRead();
			}
		} finally {
			lock.unlock();
		}
	}

	public void shutdownRead() {
		lock.lock();
		try {
			buffer.clear();
			isReadShutdown = true;
			readable.signalAll();
			reader.signal();
			// since we can't read anymore, we also cannot write anymore
			if (!isWriteShutdown) {
				shutdownWrite();
			}
		} finally {
			lock.unlock();
		}
	}

	public void clear() {
		lock.lock();
		try {
			current = {{opt.element.nil}};
			isNew = false;
		} finally {
			lock.unlock();
		}
	}

	public {{opt.element.fullname}} get() {
		lock.lock();
		try {
			return current;
		} finally {
			lock.unlock();
		}
	}

	public boolean isNew() {
		lock.lock();
		try {
			return isNew;
		} finally {
			lock.unlock();
		}
	}

	public boolean isWritable() {
		lock.lock();
		try {
			return !isFull() && !isWriteShutdown;
		} finally {
			lock.unlock();
		}
	}

	public boolean isReadable() {
		lock.lock();
		try {
			return !isEmpty() && !isReadShutdown;
		} finally {
			lock.unlock();
		}
	}

	public boolean isWriteShutdown() {
		lock.lock();
		try {
			return isWriteShutdown || isReadShutdown;
		} finally {
			lock.unlock();
		}
	}

	public boolean isReadShutdown() {
		lock.lock();
		try {
			return isReadShutdown || (isWriteShutdown && isEmpty());
		} finally {
			lock.unlock();
		}
	}

	private boolean isFull() {
		return buffer.size() == bufferSize;
	}

	private boolean isEmpty() {
		return buffer.isEmpty();
	}

}
