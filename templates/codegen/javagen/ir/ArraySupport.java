package {{support.package}};

public final class {{support.class}} {
	
	public static {{element.type}}[] concatenate({{element.type}}[] lhs, {{element.type}}[] rhs)
	{
	    final int n_lhs = lhs.length;
	    final int n_rhs = rhs.length;
	    if (n_lhs==0) {
		return rhs;
	    }
	    if (n_rhs==0) {
		return lhs;
	    }
	    
	    final {{element.type}}[] res = java.util.Arrays.copyOf(lhs,n_lhs+n_rhs);
		java.lang.System.arraycopy(rhs,0,res,n_lhs,n_rhs);
	    return res;
	}
		
}
