package {{support.package}};

public final class {{support.class}} {


	private {{support.class}} ()
	{}
	
	
	
	private static byte[] reverse(byte[] bytes)
	{
		if (bytes.length<2) {
			return bytes;
		}
		byte[] res = new byte[bytes.length];
		int i=0;
		int j=bytes.length-1;
		while(i<=j) { // make sure to copy the value where i==j
			res[j]=bytes[i];
			res[i]=bytes[j];
			++i;
			--j;
		}
		
		return res;
	}
	
	private static void reverse_self(byte[] bytes)
	{
		if (bytes.length<2) {
			return;
		}
		int i=0;
		int j=bytes.length-1;
		while(i<j) {  // no need to swap the value where i==j
			byte b = bytes[i];
			bytes[i]=bytes[j];
			bytes[j]=b;
			++i;
			--j;
		}
	}
	
	/**
	 * Convert a string to bytes
	 * @param str a string
	 * @return the string encoded as UTF8 bytes
	 */
	public static byte[] real2bytes(java.math.BigDecimal value)
	{
		byte[] unscaled = value.unscaledValue().toByteArray();
		int n = unscaled.length;
		byte[] bytes = java.util.Arrays.copyOf(unscaled,n+4);
		int scale = value.scale();
		bytes[n] = (byte)((scale >> 24) & 0xff);
		bytes[n+1] = (byte)((scale >> 16) & 0xff);
		bytes[n+2] = (byte)((scale >> 8) & 0xff);
		bytes[n+3] = (byte)((scale >> 0) & 0xff);
		reverse_self(bytes);
		
		// we end up with an array
		// {scale}{integer} in little endian
		return bytes;
	}
	
	/**
	 * Parse bytes as a string
	 * @param str a string
	 * @return the string encoded as UTF8 bytes
	 */
	public static java.math.BigDecimal bytes2real(byte[] bytes)
	{
		if (bytes.length<4) {
			throw new java.lang.RuntimeException("bytes2real: not an encoded big decimal");
		}
		int scale = bytes[0] & 0x00ff;
		scale |= (bytes[1] & 0x00ff) << 8;
		scale |= (bytes[2] & 0x00ff) << 16;
		scale |= (bytes[3] & 0x00ff) << 24;
		
		byte[] unscaled = java.util.Arrays.copyOfRange(bytes,4,bytes.length);
		reverse_self(unscaled);
		java.math.BigInteger unscaledInt = new java.math.BigInteger(unscaled);
		return new java.math.BigDecimal(unscaledInt,scale);
	}
	
	/**
	 * Convert a string to bytes
	 * @param str a string
	 * @return the string encoded as UTF8 bytes
	 */
	public static boolean[] real2bits(java.math.BigDecimal value)
	{
		byte[] bytes = real2bytes(value);
		boolean[] bits = new boolean[bytes.length*8];
		for (int i=0;i<bits.length;++i) {
        	byte b = bytes[i/8];
			bits[i] = (b & (1<< (i%8))) != 0;;
		}
        return bits;
	}
	
	/**
	 * Parse bytes as a string
	 * @param str a string
	 * @return the string encoded as UTF8 bytes
	 */
	public static java.math.BigDecimal bits2real(boolean[] bits)
	{
		if (bits.length % 8 != 0) {
			throw new java.lang.RuntimeException("bits2real: not a multiple of 8");
		}
		byte[] bytes = new byte[bits.length/8];
		for (int i=0;i<bits.length;++i) {
			int b = bytes[i/8];
			int mask = 1<<(i%8);
			b = bits[i] ? (b | mask) : (b & ~mask);
			bytes[i/8] = (byte)b; 
		}
		return bytes2real(bytes);
	}
	

}
