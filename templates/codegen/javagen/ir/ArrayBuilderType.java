package {{builder.package}};

public class {{builder.class}} {

    protected int sz;
    protected {{element.type}}[] elements;

	private {{builder.class}}(int n) {
	    // this is tricky, if the element type is an array!
	    this.elements = ({{element.type}}[])java.lang.reflect.Array.newInstance({{element.type}}.class,n);
	}
	
	public static {{builder.class}} create(int n) {
	    return new {{builder.class}}(n);
	}

	public void ensureAdditionalCapacity(int n)
	{
	    if (sz+n <= elements.length) {
		return;
	    }
	    int newCapacity = sz+n;
	    // FIXME: try to do better than O(n^2)
	    this.elements = java.util.Arrays.copyOf(this.elements,newCapacity);
	}
	
	public void appendElement({{element.type}} value)
	{
	    this.ensureAdditionalCapacity(1);
	    this.elements[this.sz++] = value;
	}
	
	public void appendElements({{element.type}}[] value)
	{
	    int n = value.length;
	    this.ensureAdditionalCapacity(n);
	    java.lang.System.arraycopy(value,0,this.elements,sz,n);
	    this.sz += n;
	}

	public {{element.type}}[] build()
	{
	    {{element.type}}[] res=this.elements;
	    this.elements=null; // triggers error in case of misue
	    return res;
        }
	public int length() { return this.sz; }
}
