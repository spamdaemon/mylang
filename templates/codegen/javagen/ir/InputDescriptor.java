package {{port.descriptor.package}};

public final class  {{port.descriptor.class}} extends {{descriptor.fullname}} {

	public final {{port.fullname}} port;

	 {{port.descriptor.class}}({{port.fullname}} port) {
		this.port = port;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj.getClass() == {{port.descriptor.class}}.class) {
			{{port.descriptor.class}} that = ({{port.descriptor.class}}) obj;
			return this.port == that.port;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return java.util.Objects.hashCode(port);
	}

	@Override
	public void beginAwait() {
		this.port.doLocked(() -> {
			port.clearNew();
		});
	}

	@Override
	public void endAwait(boolean implicitConsume) {
		if (implicitConsume) {
			this.port.doLocked(() -> {
				port.consume(Timeout.NONE);
			});
		}
	}

	@Override
	public long test() {
		if (port.isReadShutdown()) {
			return 0;
		}
		return port.awaitReadable(Timeout.NONE);
	}
	
	@Override
	public boolean isShutdown()
	{ return port.isReadShutdown(); }
}
