package {{port.descriptor.package}};

public final class  {{port.descriptor.class}} extends {{descriptor.fullname}} {

	public final {{port.fullname}} port;

	 {{port.descriptor.class}}({{port.fullname}} port) {
		this.port = port;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj.getClass() == {{port.descriptor.class}}.class) {
			{{port.descriptor.class}} that = ({{port.descriptor.class}}) obj;
			return this.port == that.port;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return java.util.Objects.hashCode(port);
	}

	@Override
	public void beginAwait() {
	}

	@Override
	public void endAwait(boolean implicitConsume) {
	}

	@Override
	public long test() {
		return (port.isWritable() || port.isWriteShutdown()) ? 0 : -1;
	}

	
	@Override
	public boolean isShutdown()
	{ return port.isWriteShutdown(); }
}
