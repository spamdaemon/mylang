#ifndef TEST_MYLANG_PARSER_H
#define TEST_MYLANG_PARSER_H

#include <filesystem>
#include <vector>
#include <string_view>
#include <mylang/defs.h>

mylang::GroupNodeCPtr parseText(const ::std::string_view &text);

#endif
