#include <mylang/ethir/ProgramBuilder.h>
#include <mylang/ethir/ir/LambdaExpression.h>
#include <mylang/ethir/types/VoidType.h>
#include <mylang/ethir/ssa/SSATestSupport.h>

namespace mylang::ethir {
  ProgramBuilder::ProgramBuilder()
  {
  }
  ProgramBuilder::~ProgramBuilder()
  {
  }

  ir::Variable::VariablePtr ProgramBuilder::addFunction(EType retTy, NoArgLambdaBuilder body)
  {

    ir::LambdaExpression::Parameters params;
    auto stmt = body();

    auto expr = ir::LambdaExpression::create(params, retTy, stmt);
    auto fnName = ir::Variable::create(ir::Declaration::Scope::GLOBAL_SCOPE, expr->type,
        Name::create("fn"));
    globals.push_back(ir::GlobalValue::create(fnName, expr, ir::GlobalValue::DEFAULT));

    return fnName;
  }

  ir::Variable::VariablePtr ProgramBuilder::addFunction(NoArgLambdaBuilder body)
  {
    return addFunction(types::VoidType::create(), body);
  }

  ir::Variable::VariablePtr ProgramBuilder::addFunction(EType retTy,
      ::std::vector<EType> paramTypes, LambdaBuilder body)
  {

    ir::LambdaExpression::Parameters params;
    Arguments args;
    for (auto ty : paramTypes) {
      auto p = ir::Parameter::create(Name::create("arg"), ty);
      params.push_back(p);
      args.push_back(p->variable);
    }

    auto stmt = body(args);

    auto expr = ir::LambdaExpression::create(params, retTy, stmt);
    auto fnName = ir::Variable::create(ir::Declaration::Scope::GLOBAL_SCOPE, expr->type,
        Name::create("fn"));
    globals.push_back(ir::GlobalValue::create(fnName, expr, ir::GlobalValue::EXPORTED));

    return fnName;
  }

  ir::Variable::VariablePtr ProgramBuilder::addFunction(::std::vector<EType> paramTypes,
      LambdaBuilder body)
  {
    return addFunction(types::VoidType::create(), paramTypes, body);
  }

  ir::Program::ProgramPtr ProgramBuilder::build()
  {
    return ir::Program::create(globals, processes, types);
  }

  ssa::SSAProgram::Ptr ProgramBuilder::buildSSA()
  {
    auto prg = ir::Program::create(globals, processes, types);
    return ssa::SSATestSupport::createTestSSA(prg);
  }
}
