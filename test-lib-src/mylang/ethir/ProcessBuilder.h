#ifndef MYLANG_ETHIR_PROCESSBUILDER_H
#define MYLANG_ETHIR_PROCESSBUILDER_H

#ifndef MYLANG_ETHIR_IR_STATEMENTBUILDER_H
#include <mylang/ethir/ir/StatementBuilder.h>
#endif

namespace mylang::ethir {

  class ProcessBuilder
  {
  public:
    ProcessBuilder();
    virtual ~ProcessBuilder();

  };
}
#endif
