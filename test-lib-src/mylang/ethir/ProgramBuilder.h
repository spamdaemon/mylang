#ifndef MYLANG_ETHIR_PROGRAMBUILDER_H
#define MYLANG_ETHIR_PROGRAMBUILDER_H

#ifndef MYLANG_ETHIR_SSA_SSAPROGRAM_H
#include <mylang/ethir/ssa/SSAProgram.h>
#endif

#ifndef MYLANG_ETHIR_IR_STATEMENTBUILDER_H
#include <mylang/ethir/ir/StatementBuilder.h>
#endif

#ifndef MYLANG_ETHIR_IR_NODES_H
#include <mylang/ethir/ir/nodes.h>
#endif

#ifndef MYLANG_ETHIR_TYPES_H
#include <mylang/ethir/types/types.h>
#endif

#ifndef MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#include <vector>
#include <functional>

namespace mylang::ethir {

  class ProgramBuilder
  {
  public:
    typedef ::std::vector<ir::Variable::VariablePtr> Arguments;

  public:
    typedef ::std::function<ir::Statement::StatementPtr(Arguments)> LambdaBuilder;
    typedef ::std::function<ir::Statement::StatementPtr()> NoArgLambdaBuilder;

  public:
    ProgramBuilder();
    virtual ~ProgramBuilder();

    /**
     * Build a program.
     */
    ir::Program::ProgramPtr build();

    /**
     * Build the program in SSA form.
     */
    ssa::SSAProgram::Ptr buildSSA();

    /**
     * Add a global function.
     * @param retTy return type
     * @param args arguments
     * @param body the body builder
     * @return the variable for the function
     */
  public:
    ir::Variable::VariablePtr addFunction(EType retTy, ::std::vector<EType> params,
        LambdaBuilder body);
    ir::Variable::VariablePtr addFunction(::std::vector<EType> params, LambdaBuilder body);
    ir::Variable::VariablePtr addFunction(EType retTy, NoArgLambdaBuilder body);
    ir::Variable::VariablePtr addFunction(NoArgLambdaBuilder body);

    /** The global values */
  private:
    ir::Program::Globals globals;
    ir::Program::Processes processes;
    ir::Program::NamedTypes types;

  };
}

#endif
