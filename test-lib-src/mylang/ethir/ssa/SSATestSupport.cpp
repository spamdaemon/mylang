#include <mylang/ethir/ssa/SSATestSupport.h>
#include <mylang/ethir/ssa/SSAProgram.h>
#include <mylang/ethir/ssa/SimplifyPass.h>
#include <mylang/ethir/ssa/ToSSA.h>
#include <mylang/ethir/TestSupport.h>
#include <mylang/ethir/prettyPrint.h>
#include <sstream>
#include <cctype>

namespace mylang::ethir::ssa {

  SSATestSupport::SSATestSupport()
  {
  }
  SSATestSupport::~SSATestSupport()
  {
  }

  EVariable SSATestSupport::newGlobal(const EType &ty, const ::std::string &prefix)
  {
    Name name = prefix.empty() ? Name::create("tmp") : Name::create(prefix);
    return ir::Variable::create(ir::Variable::Scope::GLOBAL_SCOPE, ty, name);
  }

  SSAProgram::Ptr SSATestSupport::createTestSSA(EProgram nonSSA)
  {
    auto ssaForm = ToSSA::transform(nonSSA);
    if (ssaForm == nullptr) {
      return nullptr;
    }

    struct Impl: public SSAProgram
    {
      Impl(ProgramPtr ssaForm)
          : SSAProgram(ssaForm)
      {
      }
      ~Impl()
      {
      }
    };
    return ::std::make_unique<Impl>(ssaForm);
  }

  bool SSATestSupport::checkBlock(mylang::ethir::ssa::SSAPass &pass, DriverFN driver,
      const ::std::string &expectedResult)
  {
    ProgramBuilder setup;

    auto fn = setup.addFunction([&]() {
      mylang::ethir::ir::StatementBuilder b;
      driver(b);
      return b.build();
    });

    auto ssa = createTestSSA(setup.build());

    // output the function before we transform it
    {
      auto findFN = ssa->getSSAForm()->findGlobal(fn->name);
      ::std::cout << "Before transform : " << ::std::endl;
      ::std::cout << prettyPrint(findFN) << ::std::endl;
      ::std::cout << "======================================" << ::std::endl;
    }

    pass.transformProgram(*ssa);

    auto findFN = ssa->getSSAForm()->findGlobal(fn->name);
    if (!findFN) {
      ::std::cerr << "Function " << fn->name << " was eliminated after transform" << ::std::endl;
      return false;
    }
    auto lambda = findFN->value->self<ir::LambdaExpression>();
    if (!lambda) {
      ::std::cerr << "Function " << fn->name << " should have been a lambda" << ::std::endl;
      return false;
    }
    // only output the lambda body
    return TestSupport::diff(lambda->body, expectedResult);
  }
  bool SSATestSupport::checkBlock(mylang::ethir::ssa::Peephole::Ptr &t, DriverFN driver,
      const ::std::string &expectedResult)
  {
    SimplifyPass pass( { t });
    return checkBlock(pass, driver, expectedResult);
  }

}
