#ifndef MYLANG_ETHIR_SSA_SSATESTSUPPORT_H
#define MYLANG_ETHIR_SSA_SSATESTSUPPORT_H

#ifndef MYLANG_ETHIR_SSA_SSATRANSFORM_H
#include <mylang/ethir/ssa/SSATransform.h>
#endif

#ifndef MYLANG_ETHIR_SSA_SSAPROGRAM_H
#include <mylang/ethir/ssa/SSAProgram.h>
#endif

#ifndef MYLANG_ETHIR_IR_STATEMENTBUILDER_H
#include <mylang/ethir/ir/StatementBuilder.h>
#endif

#ifndef MYLANG_ETHIR_SSA_SSAPASS_H
#include <mylang/ethir/ssa/SSAPass.h>
#endif

#ifndef MYLANG_ETHIR_SSA_PEEPHOLE_H
#include <mylang/ethir/ssa/Peephole.h>
#endif

#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/types/types.h>

#include <functional>
#include <string>
#include <cassert>

namespace mylang::ethir::ssa {

  /**
   * This test support help test individual SSA transforms.
   * */
  class SSATestSupport
  {

    /** The driver function takes a boolean and return some value */
  public:
    typedef ::std::function<void(mylang::ethir::ir::StatementBuilder&)> DriverFN;

  public:
    SSATestSupport();
    virtual ~SSATestSupport();

    /**
     * Create the SSA form a program for unit testing.
     * @param nonSSA a non-ssa program
     */
  public:
    static SSAProgram::Ptr createTestSSA(EProgram nonSSA);

    /**
     * Create an anonymous variable. Variables can be referenced in a test function without
     * having been defined!
     * @param ty a type
     */
  public:
    static EVariable newGlobal(const EType &ty, const ::std::string &prefix = ::std::string());

    /**
     * Test a block of code. The code is transformed into SSA form before the applying the transform.
     * @param t the transform to test
     * @param driver the driver that produces a code fragment within a function
     * @param expected result the expected result
     * @return true if the transform produced the expected result, false otherwise
     */
  public:
    static bool checkBlock(mylang::ethir::ssa::SSAPass &t, DriverFN driver,
        const ::std::string &expectedResult);

    /**
     * Apply a peephole to a block of code. The code is transformed into SSA form before the applying the transform.
     * @param t the transform to test
     * @param driver the driver that produces a code fragment within a function
     * @param expected result the expected result
     * @return true if the transform produced the expected result, false otherwise
     */
  public:
    static bool checkBlock(mylang::ethir::ssa::Peephole::Ptr &t, DriverFN driver,
        const ::std::string &expectedResult);
  };
}
#endif
