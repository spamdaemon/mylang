#ifndef MYLANG_ETHIR_TESTSUPPORT_H
#define MYLANG_ETHIR_TESTSUPPORT_H

#ifndef MYLANG_ETHIR_PROCESSBUILDER_H
#include <mylang/ethir/ProcessBuilder.h>
#endif

#ifndef MYLANG_ETHIR_PROGRAMBUILDER_H
#include <mylang/ethir/ProgramBuilder.h>
#endif

#include <cassert>

namespace mylang::ethir {

  class TestSupport
  {
  public:
    TestSupport();
    virtual ~TestSupport();

    /**
     * Perform a diff of two pretty printed values, ignoring newlines
     * @return true if they are the same
     */
  public:
    static bool diff(const ::std::string &a, const ::std::string &b);

    /**
     * Perform a diff of the pretty printed version of a node to the specified string, ignoring newlines
     * @return true if they are the same
     */
  public:
    static bool diff(const ENode &a, const ::std::string &b);
  };
}
#endif
