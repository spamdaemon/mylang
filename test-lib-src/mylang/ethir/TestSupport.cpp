#include <mylang/ethir/TestSupport.h>
#include <mylang/ethir/prettyPrint.h>
#include <sstream>
#include <cctype>

namespace mylang::ethir {

  namespace {
    static bool isWS(char ch)
    {
      return ::std::isspace(ch);
    }

    static void removeWS(::std::string &str, bool removeNewLine = true)
    {
      bool removeWS = true;

      // remove any whitespace following a newline
      for (size_t i = 0; i < str.length();) {
        if (removeWS && isWS(str[i])) {
          str.erase(i, 1);
        } else {
          removeWS = str[i] == '\n';
          ++i;
        }
      }
      removeWS = true;
      // remove any whitespace preceeding a newline
      for (size_t i = str.length(); i-- > 0;) {
        if (removeWS && isWS(str[i])) {
          str.erase(i, 1);
        } else if (str[i] == '\n') {
          if (removeNewLine) {
            str.erase(i, 1);
          }
          removeWS = true;
        } else {
          removeWS = false;
        }
      }
    }

    static ::std::string quote(::std::string str)
    {
      removeWS(str, false);
      ::std::string res;
      res += "\"\"\n";
      res += '"';
      for (char ch : str) {
        if (ch == '\\') {
          res += "\\\\";
        } else if (ch == '"') {
          res += '\\';
          res += ch;
        } else if (ch == '\n') {
          res += '"';
          res += ch;
          res += '"';
        } else {
          res += ch;
        }
      }
      res += "\"\n";
      res += "\"\"\n";
      return res;
    }
  }

  TestSupport::TestSupport()
  {
  }
  TestSupport::~TestSupport()
  {
  }
  bool TestSupport::diff(const ENode &a, const ::std::string &b)
  {
    ::std::ostringstream sbuf;
    prettyPrint(a, sbuf);
    if (diff(sbuf.str(), b)) {
//        ::std::cout << sbuf.str() << ::std::endl;
      return true;
    } else {
      ::std::cerr << "Failed comparison " << ::std::endl;
      ::std::cout << "<<<<< ACTUAL" << ::std::endl;
      ::std::cout << quote(sbuf.str()) << ::std::endl;
      ::std::cout << "================" << ::std::endl;
      ::std::cout << quote(b) << ::std::endl;
      ::std::cout << ">>>>> EXPECTED" << ::std::endl;
      return false;
    }
  }

  bool TestSupport::diff(const ::std::string &a, const ::std::string &b)
  {
    ::std::string left(a), right(b);
    removeWS(left);
    removeWS(right);
    return left == right;
  }
}
