#include <mylang/api/Parser.h>
#include <sstream>
#include <cassert>

#include <mylang/vast/transforms/AddGuards.h>
#include <mylang/vast/transforms/AddDefaultConstructors.h>
#include <mylang/vast/transforms/RegisterProcessEvents.h>
#include <mylang/vast/transforms/InstantiateGenerics.h>

static void feedback(mylang::FeedbackType, const mylang::SourceLocation&, const ::std::string&)
{
}

mylang::GroupNodeCPtr parseText(const ::std::string_view &txt)
{
  ::std::string text(txt);
  mylang::api::Parser::Options opts;
  opts.feedback = feedback;
  auto parser = mylang::api::Parser::create(opts);
  ::std::istringstream in(text);
  auto tree = parser->parse(in, ::std::nullopt);
  assert(tree && "Expected a semantically valid AST");
  // instantiate all generics should maybe even be done by validateAST?
  tree = mylang::vast::transforms::InstantiateGenerics::apply(tree);
  tree = mylang::vast::transforms::AddGuards::apply(tree);
  tree = mylang::vast::transforms::AddDefaultConstructors::apply(tree);
  tree = mylang::vast::transforms::RegisterProcessEvents::apply(tree);

  // resolve any transforms that may be present

  return tree;
}

