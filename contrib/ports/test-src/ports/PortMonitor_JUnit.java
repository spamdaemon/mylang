package ports;

import java.util.Optional;

import org.junit.After;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class PortMonitor_JUnit {

	public final Optional<String> message1 = Optional.of("Hello, World!");

	// the port queue size could be made a parameter of the test
	public final Port<String> port1 = new Port<String>("port1", 1);
	public final Port<String> port2 = new Port<String>("port2", 1);
	public final PortMonitor monitor = new PortMonitor();

	private Thread monitor1;
	private Thread monitor2;

	private void startMonitor1(Runnable r) {
		monitor1 = new Thread(r, "monitor1");
		monitor1.start();
	}

	private void joinThread(Thread t, int timeoutMS) {
		try {
			if (t != null) {
				t.join(timeoutMS);
				Assert.assertFalse("Failed to join thread: " + t.getName(), t.isAlive());
			}
		} catch (InterruptedException e) {
			Assert.fail("Interrupted join");
		}
	}

	@After
	public void teardown() throws Exception {
		joinThread(monitor2, 250);
		joinThread(monitor1, 250);
	}

	@Test
	public void shutdown_test() throws Exception {
		monitor.monitor(port1, PortEvent.READ);
		startMonitor1(() -> {
			try {
				monitor.await(Optional.empty());
			} catch (Exception e) {
				Assert.fail("Interrupted");
			}
		});
		Thread.sleep(150);
		monitor.shutdown();
		joinThread(monitor1, 100);
	}

	@Test
	public void monitorWrite() throws Exception {
		// a port by default is writable, so we can wait on it
		monitor.monitor(port1, PortEvent.WRITE);
		monitor.monitor(port2, PortEvent.WRITE);

		PortEvents events = monitor.await(Optional.empty());
		Assert.assertEquals(2, events.size());
	}

	@Test
	public void test_readable() throws Exception {
		// a port by default is writable, so we can wait on it
		monitor.monitor(port1, PortEvent.READ);
		monitor.monitor(port2, PortEvent.READ);
		port1.write(message1.get());

		PortEvents events = monitor.await(Optional.empty());
		Assert.assertEquals(1, events.size());
		Assert.assertTrue(events.isPortReadable(port1));
		Assert.assertFalse(events.isPortReadable(port2));
	}

	@Test
	public void write_read_await() throws Exception {
		// a port by default is writable, so we can wait on it
		monitor.monitor(port1, PortEvent.WRITE);

		// write, read, await will not return port 2 readable, since there is no data
		monitor.monitor(port2, PortEvent.READ);
		port2.write(message1.get());
		port2.read();

		PortEvents events = monitor.await(Optional.empty());
		Assert.assertEquals(1, events.size());
		Assert.assertTrue(events.isPortWritable(port1));
		Assert.assertFalse(events.isPortReadable(port2));
	}

	@Test
	public void test_writable() throws Exception {
		// a port by default is writable, so we can wait on it
		monitor.monitor(port1, PortEvent.WRITE);

		// write, read, await will not return port 2 readable, since there is no data
		monitor.monitor(port2, PortEvent.WRITE);
		port2.write(message1.get());
		PortEvents events = monitor.await(Optional.empty());
		Assert.assertTrue(events.isPortWritable(port1));
		Assert.assertFalse(events.isPortWritable(port2));
	}

	@Test
	public void concurrent_monitor() throws Exception {
		// a port by default is writable, so we can wait on it
		monitor.monitor(port1, PortEvent.READ);

		PortEvents[] events = new PortEvents[1];

		startMonitor1(() -> {
			try {
				events[0] = monitor.await(Optional.empty());
			} catch (Exception e) {
				Assert.fail("Interrupted");
			}
		});

		Thread.sleep(250);
		// write before registering
		port2.write(message1.get());
		monitor.monitor(port2, PortEvent.READ);
		joinThread(monitor1, 100);

		Assert.assertNotNull(events[0]);
		Assert.assertFalse(events[0].isPortReadable(port1));
		Assert.assertTrue(events[0].isPortReadable(port2));
	}

	@Test
	public void concurrent_event_monitor() throws Exception {
		// a port by default is writable, so we can wait on it
		monitor.monitor(port1, PortEvent.READ);

		PortEvents[] events = new PortEvents[1];

		startMonitor1(() -> {
			try {
				events[0] = monitor.await(Optional.empty());
			} catch (Exception e) {
				Assert.fail("Interrupted");
			}
		});

		Thread.sleep(250);
		// write before registering
		port1.write(message1.get());
		joinThread(monitor1, 100);

		Assert.assertNotNull(events[0]);
		Assert.assertTrue(events[0].isPortReadable(port1));
	}

}
