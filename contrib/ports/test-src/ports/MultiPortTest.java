package ports;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.After;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class MultiPortTest {

	public final int N_LOOPS = 100;

	public final Port<Integer> input = new Port<Integer>("input", 10);
	public final Port<Integer> output = new Port<Integer>("output", 1);

	/** The producer thread */
	public final Thread producer = new Thread(() -> {
		for (int i = 0; i < N_LOOPS; ++i) {
			input.write(Integer.valueOf(i));
			//System.err.println("Produced");
		}
		System.err.println("Producer finished");
		input.shutdown();
	},"producer");

	public final AtomicInteger outputCounter = new AtomicInteger();

	/** The producer thread */
	public final Thread consumer = new Thread(() -> {
		for (Optional<Integer> v; ((v = output.read())).isPresent();) {
			//System.err.println("Consumed "+v.get());
			outputCounter.incrementAndGet();
		}
		System.err.println("Consumer finished");
	},"consumer");

	public final Thread filter = new Thread(() -> {

		PortMonitor monitor = new PortMonitor();
		monitor.monitor(input, PortEvent.READ);

		try {
			while (true) {
				PortEvents e = monitor.await(Optional.empty());
				if (e.isEmpty()) {
					break;
				}
				for (IOPort p : e.ports()) {
					//System.err.println("Event on "+p);
					if (e.isPortReadable(p) && !p.isNew()) {
						p.readNext();
					}

					if (input.isNew()) {
						input.get(); // mark it read
						monitor.unmonitor(input, PortEvent.READ);
						monitor.monitor(output, PortEvent.WRITE);
					}
					else if (input.get().isPresent()) {
						output.write(input.get().get());
						input.clear();
						monitor.unmonitor(output, PortEvent.WRITE);
						//System.err.println("Monitor "+input);
						monitor.monitor(input, PortEvent.READ);
					}
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.err.println("Filter finished");
		output.shutdown();
	},"filter");

	@After
	public void teardown() {
		try {
			producer.join();
			consumer.join();
			filter.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void run_producer_consumer() {
		consumer.start();
		filter.start();
		producer.start();

		try {
			consumer.join();
			producer.join();
			filter.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
			Assert.fail();
		}
		Assert.assertEquals(N_LOOPS, outputCounter.get());

	}
}
