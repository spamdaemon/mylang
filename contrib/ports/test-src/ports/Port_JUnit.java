package ports;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class Port_JUnit {

	public final Optional<String> message1 = Optional.of("Hello, World!");

	// the port queue size could be made a parameter of the test
	public Port<String> port = new Port<String>("port", 1);

	private Thread writer;
	private Thread reader;

	private void startReader(Runnable r) {
		reader = new Thread(r, "reader");
		reader.start();
	}

	private void joinThread(Thread t, int timeoutMS) {
		try {
			if (t != null) {
				t.join(timeoutMS);
				Assert.assertFalse("Failed to join thread: " + t.getName(), t.isAlive());
			}
		} catch (InterruptedException e) {
			Assert.fail("Interrupted join");
		}
	}

	@After
	public void teardown() throws Exception {
		joinThread(reader, 250);
		joinThread(writer, 250);
	}

	@Test
	public void init_test() {
		Assert.assertTrue(port.isWriteable());
		Assert.assertFalse(port.isReadable());
		Assert.assertFalse(port.isNew());
		Assert.assertFalse(port.get().isPresent());
	}

	@Test
	public void write_test() {
		port = new Port<String>("port-2",2);
		port.write(message1.get());
		Assert.assertTrue(port.isWriteable());
		port.write(message1.get());
		Assert.assertFalse(port.isWriteable());
		Assert.assertTrue(port.isReadable());
		Assert.assertFalse(port.isNew());
		Assert.assertFalse(port.get().isPresent());
	}

	@Test
	public void read_write_test() {
		port.write(message1.get());
		Optional<String> message = port.read();
		Assert.assertEquals(message1, message);
		Assert.assertTrue(message.isPresent());
		Assert.assertTrue(port.isWriteable());
		Assert.assertFalse(port.isReadable());
		Assert.assertTrue(port.isNew());
		Assert.assertTrue(port.get().isPresent());
		Assert.assertEquals(message, port.get());
	}

	@Test
	public void concurrent_test() throws Exception {
		final List<Optional<String>> res = new ArrayList<>();
		startReader(() -> res.add(port.read()));
		Thread.sleep(250);
		port.write(message1.get());
		joinThread(reader, 100);
		Assert.assertEquals(1, res.size());
		Assert.assertEquals(message1, res.get(0));
	}

	@Test
	public void shutdown_test() throws Exception {
		final List<Optional<String>> res = new ArrayList<>();
		startReader(() -> res.add(port.read()));
		Thread.sleep(250);
		port.shutdown();
		joinThread(reader, 100);
		Assert.assertEquals(1, res.size());
		Assert.assertFalse(res.get(0).isPresent());
	}

	@Test
	public void delayed_shutdown_test() throws Exception {
		// write a message then shutdown; we'll be able to read the message
		port.write(message1.get());
		port.shutdown();
		Assert.assertTrue(port.isReadable());
		Assert.assertFalse(port.isWriteable());
		Optional<String> message = port.read();
		boolean shutdown = port.awaitReadable(Optional.empty());
		Assert.assertEquals(message1, message);
		Assert.assertFalse(shutdown);
		// the last message that was read remains in the port
		Assert.assertEquals(message1, port.get());
	}

	@Test
	public void immediate_shutdown_test() throws Exception {
		// immediate shutdown followed by a read, causes read to terminate immediately
		port.shutdown();
		Assert.assertFalse(port.isReadable());
		Assert.assertFalse(port.isWriteable());
		Optional<String> message = port.read();
		Assert.assertFalse(message.isPresent());
	}

	@Test
	public void test_clear() {
		port.write(message1.get());
		port.read();
		Assert.assertTrue(port.get().isPresent());
		port.clear();
		Assert.assertFalse(port.get().isPresent());
	}
}
