package mylang;

import java.util.Optional;

public interface InputPort {

	/**
	 * True if the value returned by get is new.
	 * 
	 * @return true if the data returned by get() is new.
	 */
	boolean isNew();

	/**
	 * True if the port can be written to without blocking. If multiple concurrent
	 * writers exist, then the result of the function is meaningless.
	 * 
	 * @return true if the port can be written without blocking.
	 */
	boolean isReadable();

	/**
	 * Clear the most recently read data.
	 */
	void clear();

	/**
	 * Determine if this port has been shutdown.
	 */
	boolean isShutdown();

	/**
	 * Wait until there is new data available.
	 * 
	 * @param timeout an optional timeout
	 * @return true if new data can be read, false if the port is has been shutdown
	 */
	boolean awaitReadable(Optional<Timeout> timeout);

	/**
	 * Read the next input.
	 * @return true if there was input, false on EOF
	 */
	boolean readNext();
}