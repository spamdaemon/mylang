package mylang;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class PortMonitor {

	private static final class Event {
		/** The events to select on */
		private int mask;

		/** The events currently pending on the port */
		private int pending;

		/** True if shutdown has happened */
		private int shutdownEvents;

		/**
		 * Constructor
		 * 
		 * @param mask    the events were interested int
		 * @param pending the currently pending events
		 */
		private Event(int mask, int pending, int shutdownEvents) {
			this.mask = mask;
			this.pending = pending;
			this.shutdownEvents = shutdownEvents;
		}
	}

	/** A mutex */
	private final ReentrantLock lock = new ReentrantLock();

	/** True if there is an event on a port */
	private final Condition signalled;

	/** The list of ports currently registered with the monitor */
	private final Map<IOPort, Event> monitoredPorts = new HashMap<>();

	/** True if the monitor has been terminated */
	private boolean isShutdown = false;

	public PortMonitor() {
		signalled = lock.newCondition();
	}

	public PortMonitor(Map<IOPort, PortEvent> monitoredPorts) {
		this();
		for (Map.Entry<IOPort, PortEvent> e : monitoredPorts.entrySet()) {
			monitor(e.getKey(), e.getValue().events);
		}
	}

	/**
	 * Get the number of monitored ports.
	 */
	public int portCount() {
		lock.lock();
		try {
			return monitoredPorts.size();
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Shutdown this monitor.
	 */
	public void shutdown() {
		lock.lock();
		try {
			if (!isShutdown) {
				isShutdown = true;
				signalled.signalAll();
				for (Event e : monitoredPorts.values()) {
					e.mask = 0;
				}
			}
		} finally {
			lock.unlock();
		}
	}

	/**
	 * A static function that waits on a bunch of ports.
	 * 
	 * @param ports the ports and their events
	 * @return a list of port events that have occurred
	 */
	public static PortEvents await(Map<IOPort, PortEvent> ports, Optional<Timeout> timeout)
			throws InterruptedException {
		if (ports.isEmpty()) {
			return new PortEvents(Collections.emptyMap());
		}
		PortMonitor monitor = new PortMonitor(ports);
		try {
			PortEvents res = monitor.await(timeout);
			return res;
		} finally {
			monitor.shutdown();
		}
	}

	/**
	 * Wait until there is some port that has been signalled.
	 * 
	 * @return a list of ports that have some kind of event pending
	 */
	public PortEvents await(Optional<Timeout> timeout) throws InterruptedException {
		Map<IOPort, PortEvent> res = new HashMap<>();
		Set<IOPort> deletedPorts = new HashSet<>();

		lock.lock();
		try {
			while (true) {
				if (isShutdown) {
					// to prevent deadlock, we'll unregister after we've released the lock
					deletedPorts.addAll(monitoredPorts.keySet());
					monitoredPorts.clear();
					break;
				}

				if (monitoredPorts.isEmpty()) {
					break;
				}

				// since new ports might have been added, we need to reevaluate each port
				for (Map.Entry<IOPort, Event> entry : monitoredPorts.entrySet()) {
					Event e = entry.getValue();
					int events = e.mask & e.pending;
					if (events != 0) {
						res.put(entry.getKey(), new PortEvent(events));
					} else {
						events = e.mask & e.shutdownEvents;
						if (events != 0) {
							res.put(entry.getKey(), new PortEvent(events));
						}
					}
				}
				if (res.isEmpty()) {
					if (timeout.isPresent()) {
						signalled.awaitNanos(timeout.get().timeoutNS());
					} else {
						signalled.await();
					}
				} else {
					break;
				}
			}
			// if we've receive a shutdown event for the events we're interested in then
			// then remove the port
			for (Iterator<Map.Entry<IOPort, Event>> i = monitoredPorts.entrySet().iterator(); i.hasNext();) {
				Map.Entry<IOPort, Event> e = i.next();
				Event ev = e.getValue();
				if ((ev.mask & ev.shutdownEvents) == ev.mask) {
					deletedPorts.add(e.getKey());
					i.remove();
				}
			}
		} finally {
			lock.unlock();
		}
		if (deletedPorts != null) {
			// force deregistration
			for (IOPort p : deletedPorts) {
				p.unregisterMonitor(this, x -> true);
			}
		}
		return new PortEvents(res);
	}

	/**
	 * Monitor the specified port.
	 * 
	 * @param port a port
	 */
	public boolean monitor(IOPort port, int newEvents) {
		return port.registerMonitor(this, (p, shutdownEvents, currentEvents) -> {
			lock.lock();
			try {
				if (isShutdown) {
					return false;
				}
				Event e = monitoredPorts.get(port);
				if (e == null) {
					e = new Event(newEvents, currentEvents, shutdownEvents);
					monitoredPorts.put(port, e);
				} else {
					e.mask |= newEvents;
					e.pending = currentEvents;
					e.shutdownEvents = shutdownEvents;
				}

				if ((e.mask & e.shutdownEvents) == e.mask) {
					monitoredPorts.remove(port);
					return false;
				}

				signalled.signalAll();
				return true;
			} finally {
				lock.unlock();
			}
		});
	}

	/**
	 * Monitor the specified port.
	 * 
	 * @param port a port
	 * @return true if the port was fully removed, false otherwise
	 */
	public void unmonitor(IOPort port, int deleteEvents) {
		port.unregisterMonitor(this, p -> {
			lock.lock();
			try {
				Event e = monitoredPorts.get(port);
				if (e == null) {
					System.err.println("Port " + port + " is not monitored");
					return true;
				}
				e.mask &= ~deleteEvents;
				if (e.mask == 0) {
					monitoredPorts.remove(port);
					return true;
				}
				return false;
			} finally {
				lock.unlock();
			}
		});
	}

	/**
	 * Notify this monitor of a new event. If the event mask becomes 0, then the
	 * this function returns false and the caller must prevent further notifications
	 * of the monitor.
	 * 
	 * @param port     the port
	 * @param mask     the events that are allowed by the port
	 * @param pending  the currently pending port events
	 * @param shutdown 0 if not shutdown
	 * @return false if the port should not be monitored anymore
	 */
	boolean notifyPendingEvents(IOPort port, int shutdownEvents, int pendingEvents) {
		lock.lock();
		try {
			Event e = monitoredPorts.get(port);
			if (e == null) {
				return false;
			}

			e.shutdownEvents = shutdownEvents;
			e.pending = pendingEvents;

			if ((e.pending & e.mask) != 0 || (e.shutdownEvents & e.mask) != 0) {
				signalled.signalAll();
			}
			// the mask tell us which events we're interested in
			// so, if all the events we're interested are shutdown, then
			// the stop reporting events for the port
			if ((e.mask & e.shutdownEvents) == e.mask) {
				return false;
			}
			return true;
		} finally {
			lock.unlock();
		}
	}

}
