package mylang;

import java.util.Optional;

public interface OutputPort {

	/**
	 * True if the port can be written to without blocking. If multiple concurrent
	 * writers exist, then the result of the function is meaningless.
	 * 
	 * @return true if the port can be written without blocking.
	 */
	boolean isWriteable();

	/**
	 * Determine if this port has been shutdown.
	 */
	boolean isShutdown();

	/**
	 * Shutdown the port. No more writes may be performed on the port, but all data
	 * enqueued may be still be read.
	 */
	void shutdown();

	/**
	 * Wait until there is new data available.
	 * 
	 * @param timeout an optional timeout
	 * @return true if new data can be read, false if the port is has been shutdown
	 */
	boolean awaitWritable(Optional<Timeout> timeout);

}