package mylang;

import java.util.LinkedList;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Port<ELEMENT> extends IOPort {

	/** A name for this port */
	private final String name;

	/** A mutex */
	private final Lock lock = new ReentrantLock();

	/** True if write has been shutdown */
	private final AtomicBoolean isShutdown = new AtomicBoolean(false);

	/** The writable condition */
	private final Condition writable;

	/** The readable condition */
	private final Condition readable;

	/** The maximum number of items that may be queued up */
	private final int maxQueueSize;

	/** A queue of items written to the port. */
	private final LinkedList<Optional<ELEMENT>> queue = new LinkedList<>();

	/** The data currently available on the port */
	private Optional<ELEMENT> data = Optional.empty();

	/** State flags */
	private boolean dataIsNew = false;

	/**
	 * Constructor
	 */
	public Port(String name, int maxQueueSize) {
		this.name = name;
		this.maxQueueSize = maxQueueSize;
		this.writable = lock.newCondition();
		this.readable = lock.newCondition();
		updatePendingEvents(false, true, false);
	}

	/**
	 * Get the most recently read data. This is data remains until it is cleared or
	 * a new data is read.
	 * 
	 * @return the data available on the port
	 */
	public final Optional<ELEMENT> get() {
		lock.lock();
		try {
			dataIsNew = false;
			return data;
		} finally {
			lock.unlock();
		}
	}

	/**
	 * True if the value returned by get is new.
	 * 
	 * @return true if the data returned by get() is new.
	 */
	@Override
	public final boolean isNew() {
		lock.lock();
		try {
			return dataIsNew;
		} finally {
			lock.unlock();
		}
	}

	/**
	 * True if the port can be written to without blocking. If multiple concurrent
	 * writers exist, then the result of the function is meaningless.
	 * 
	 * @return true if the port can be written without blocking.
	 */
	@Override
	public final boolean isReadable() {
		lock.lock();
		try {
			return _isReadable();
		} finally {
			lock.unlock();
		}
	}

	/**
	 * True if the port can be written to without blocking. If multiple concurrent
	 * writers exist, then the result of the function is meaningless.
	 * 
	 * @return true if the port can be written without blocking.
	 */
	@Override
	public final boolean isWriteable() {
		lock.lock();
		try {
			return _isWriteable();
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Clear the most recently read data.
	 */
	@Override
	public final void clear() {
		lock.lock();
		try {
			dataIsNew = false;
			data = Optional.empty();
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Determine if this port has been shutdown.
	 */
	@Override
	public final boolean isShutdown() {
		return _isShutdown();
	}

	/**
	 * Determine if this port has been shutdown.
	 */
	private final boolean _isShutdown() {
		return this.isShutdown.get();
	}

	/**
	 * Shutdown the port. No more writes may be performed on the port, but all data
	 * enqueued may be still be read.
	 */
	@Override
	public final void shutdown() {
		lock.lock();
		try {
			if (isShutdown.compareAndSet(false, true)) {
				writable.signalAll();
				if (!_isReadable()) {
					updatePendingEvents(true, false, false);
					// need to wake up the readers
					// and let them know that no new data is coming
					readable.signalAll();
				} else {
					updatePendingEvents(true, false, true);
				}
			}
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Write an item to the port. If the port is not writable, then this method
	 * suspends the current thread.
	 * 
	 * @param data the new data item to write
	 * @return true if written, false if shutdown
	 */
	public boolean write(ELEMENT obj) {
		// do this here, because we might get a NPE if obj is null
		Optional<ELEMENT> data = Optional.of(obj);

		lock.lock();
		try {
			while (!_isWriteable()) {
				if (_isShutdown()) {
					return false;
				}
				writable.awaitUninterruptibly();
			}

			// enqueue the data and notify the reader
			boolean notifyReader = queue.isEmpty();
			queue.add(data);
			if (notifyReader) {
				readable.signal();
			}
			updatePendingEvents(_isShutdown(), _isWriteable(), true);
			return true;
		} finally {
			lock.unlock();
		}
	}

	@Override
	public boolean readNext() {
		return read().isPresent();
	}

	/**
	 * Read the next item from the port. If the port is not readable
	 * 
	 * @return the next item or an empty optional to indicate that the port is
	 *         shutdown
	 */
	public Optional<ELEMENT> read() {
		lock.lock();
		try {
			while (!_isReadable()) {
				if (_isShutdown()) {
					// the queue is empty and no more data will be written
					return Optional.empty();
				}
				readable.awaitUninterruptibly();
			}
			data = queue.removeFirst();
			dataIsNew = true;
			if (!isShutdown()) {
				// I think we just need to signal 1 writer
				writable.signal();
			}

			updatePendingEvents(_isShutdown(), _isWriteable(), _isReadable());
			return data;
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Wait until the port is ready for writing.
	 * 
	 * @param timeout an optional timeout
	 * @return true if new data can be read, false if the port is has been shutdown
	 */
	@Override
	public boolean awaitWritable(Optional<Timeout> timeout) {
		lock.lock();
		try {
			while (!_isWriteable()) {
				if (_isShutdown()) {
					// the queue is empty and no more data will be written
					return false;
				}
				try {
					if (timeout.isPresent()) {
						writable.awaitNanos(timeout.get().timeoutNS());
					} else {
						writable.await();
					}
				} catch (java.lang.InterruptedException e) {
					// FIXME: ignore for now and continue to wait
				}
			}
			return true;
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Wait until there is new data available.
	 * 
	 * @param timeout an optional timeout
	 * @return true if new data can be read, false if the port is has been shutdown
	 */
	@Override
	public boolean awaitReadable(Optional<Timeout> timeout) {
		lock.lock();
		try {
			while (!_isReadable()) {
				if (_isShutdown()) {
					// the queue is empty and no more data will be written
					return false;
				}
				try {
					if (timeout.isPresent()) {
						readable.awaitNanos(timeout.get().timeoutNS());
					} else {
						readable.await();
					}
				} catch (java.lang.InterruptedException e) {
					// FIXME: ignore for now and continue to wait
				}
			}
			return true;
		} finally {
			lock.unlock();
		}
	}

	private final boolean _isWriteable() {
		return !_isShutdown() && queue.size() < maxQueueSize;
	}

	private final boolean _isReadable() {
		return !queue.isEmpty();
	}

	@Override
	public String toString() {
		return name;
	}

}
