package mylang;

public final class Timeout {

	/** The duration in millis */
	private final int timeoutMS;
	private final int timeoutNS;

	public Timeout(int timeoutMs) {
		this.timeoutMS = timeoutMs;
		this.timeoutNS = 1000000 * timeoutMs;
	}

	/**
	 * Get the timeout in millis
	 * 
	 * @return the time out in millis
	 */
	public final int timeoutMS() {
		return timeoutMS;
	}

	/**
	 * Get the timeout in nananos
	 * 
	 * @return the time out in nanos
	 */
	public final long timeoutNS() {
		return timeoutNS;
	}
}
