package mylang;

/** A port event */
public final class PortEvent {
	
	public static final int READ = 1;
	public static final int WRITE = 2;
	public static final int ALL = READ|WRITE;
	
	/** The events to select for */
	final int events;

	public PortEvent(int events) {
		if ((events & ~(READ|WRITE)) != 0) {
			throw new IllegalArgumentException("Invalid event");
		}
		this.events = events;
	}

	public boolean isReadable() {
		return (this.events & READ) != 0;
	}

	public boolean isWritable() {
		return (this.events & WRITE) != 0;
	}

	public boolean isShutdown() {
		return this.events == 0;
	}
}