package mylang;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

public final class PortEvents {

	private final Map<IOPort, PortEvent> events;

	/**
	 * Create new port events.
	 */
	PortEvents() {
		this.events = Collections.emptyMap();
	}

	/**
	 * Create new port events.
	 */
	PortEvents(Map<IOPort, PortEvent> events) {
		this.events = Collections.unmodifiableMap(events);
	}

	/**
	 * The number of ports
	 * 
	 * @return the ports
	 */
	public int size() {
		return events.size();
	}

	/**
	 * The number of ports
	 * 
	 * @return the ports
	 */
	public boolean isEmpty() {
		return events.isEmpty();
	}

	/**
	 * The number of ports
	 * 
	 * @return the ports
	 */
	public Collection<IOPort> ports() {
		return events.keySet();
	}

	/**
	 * Get the events for the specified port.
	 * 
	 * @param port a port
	 * @return the events
	 */
	public PortEvent getEvents(IOPort p) {
		return events.get(p);
	}

	/**
	 * Get the events for the specified port.
	 * 
	 * @param port a port
	 * @return the events
	 */
	public boolean isPortReadable(IOPort p) {
		PortEvent e = events.get(p);
		return e != null && e.isReadable();
	}

	/**
	 * Get the events for the specified port.
	 * 
	 * @param port a port
	 * @return the events
	 */
	public boolean isPortWritable(IOPort p) {
		PortEvent e = events.get(p);
		return e != null && e.isWritable();
	}

}
