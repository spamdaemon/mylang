package mylang;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class IOPort implements InputPort, OutputPort {

	/** A callback interface */
	@FunctionalInterface
	public static interface MonitorCB {
		boolean callback(IOPort port, int shutdownEvents, int pendingEvents);
	}

	/** A callback interface */
	@FunctionalInterface
	public static interface UnmonitorCB {
		boolean callback(IOPort port);
	}

	/** The monitors that observe this port */
	private final Set<PortMonitor> monitors = new HashSet<>();

	/** The pending events: by definition a port must be writable */
	private final AtomicInteger pendingEvents = new AtomicInteger(0);

	/** The events that this port can produce */
	private final AtomicInteger shutdownEvents = new AtomicInteger(0);

	/** Default constructor */
	protected IOPort() {
	}

	/**
	 * Update the pending events
	 */
	protected void updatePendingEvents(boolean shutdown, boolean isWritable, boolean isReadable) {

		boolean notify = false;

		int newShutdownEvents = shutdownEvents.get();
		if (shutdown) {
			if (!isWritable) {
				newShutdownEvents |= PortEvent.WRITE;
			}
			if (!isReadable) {
				newShutdownEvents |= PortEvent.READ;
			}
		}
		notify = shutdownEvents.getAndSet(newShutdownEvents) != newShutdownEvents;

		int events = pendingEvents.get();
		int newEvents = events;
		if (isWritable) {
			newEvents |= PortEvent.WRITE;
		} else {
			newEvents &= ~PortEvent.WRITE;
		}
		if (isReadable) {
			newEvents |= PortEvent.READ;
		} else {
			newEvents &= ~PortEvent.READ;
		}
		notify |= pendingEvents.getAndSet(newEvents) != newEvents;
		if (notify) {
			// need to make a copy to prevent concurrent modification
			synchronized (monitors) {
				for (Iterator<PortMonitor> i = monitors.iterator(); i.hasNext();) {
					PortMonitor m = i.next();
					if (!m.notifyPendingEvents(this, newShutdownEvents, newEvents)) {
						i.remove();
					}
				}
			}
		}
	}

	/**
	 * Register a monitor with this port.
	 * 
	 * @param monitor a port monitor
	 * @param cb      callback that is invoked with the pending events
	 * @return true if the port was registered, false otherwise
	 */
	boolean registerMonitor(PortMonitor monitor, MonitorCB cb) {
		synchronized (monitors) {

			if (cb.callback(this, shutdownEvents.get(), pendingEvents.get())) {
				monitors.add(monitor);
				return true;
			}
			return false;
		}
	}

	/**
	 * Unregister a monitor
	 */
	void unregisterMonitor(PortMonitor monitor, UnmonitorCB cb) {
		synchronized (monitors) {
			if (cb.callback(this)) {
				monitors.remove(monitor);
			}
		}
	}
}