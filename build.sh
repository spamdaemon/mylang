#!/bin/bash

SCRIPT="$(readlink -e "$0")";
SCRIPT_DIR="$(dirname "$SCRIPT")";

cd "${SCRIPT_DIR}";

CPUS="$(fgrep processor /proc/cpuinfo | wc -l)"
CPUS=$((CPUS-2));
if [ $CPUS -lt 1 ]; then
	CPUS=1;
fi;
echo "Building with $CPUS cpus";
make -j"${CPUS}" -l"${CPUS}" "$@";
