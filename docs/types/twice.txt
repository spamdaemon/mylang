defun twice(x:integer[]):integer[]
{
	x.map(lambda(cur:integer) { 2*cur})
}
