#!/bin/bash

declare -a types;

for f in $(find lib-src -name \*TypeConstraint.h); do
    DIR=$(dirname "$f");
    BASE=$(basename "$f" TypeConstraint.h);
    if [ "$BASE" == "TypeConstraint.h" ]; then 
       BASE="";
    fi;
    git mv "${DIR}/${BASE}TypeConstraint.h" "${DIR}/Constrained${BASE}Type.h"
    git mv "${DIR}/${BASE}TypeConstraint.cpp" "${DIR}/Constrained${BASE}Type.cpp"
    if [ -n "${BASE}" ]; then
	types+=("$BASE");
    fi;
done;

# must be the last thing we add!
types+=(""); 

for f in $(find lib-src -name \*.h -o -name \*.cpp); do
    for t in "${types[@]}"; do
	sed -i "s/${t^^}TYPECONSTRAINT/CONSTRAINED${t^^}TYPE/g" "${f}";
	sed -i "s/${t}TypeConstraint/Constrained${t}Type/g" "${f}";
    done;
done;
