# this makefile fragment defines the standard rules
# that are used to compile, link, and run tests

$(OBJECT_DIR)/tests-src/%.o : tests-src/%.cpp  $(GEN_LIB_SOURCE_FILES)
	@mkdir -p $(dir $@)
	$(compile.test.cpp)

$(OBJECT_DIR)/tests-src/%.o : tests-src/%.c $(GEN_LIB_SOURCE_FILES)
	@mkdir -p $(dir $@)
	$(compile.test.c)

$(OBJECT_DIR)/%.o : %.cpp $(GEN_LIB_SOURCE_FILES)
	@mkdir -p $(dir $@)
	$(compile.cpp)

$(OBJECT_DIR)/%.o : %.c $(GEN_LIB_SOURCE_FILES)
	@mkdir -p $(dir $@)
	$(compile.c)

$(OBJECT_DIR)/cppcheck/%.cpp : %.cpp $(GEN_LIB_SOURCE_FILES)
	@mkdir -p $(dir $@)
	@rm -f $@;
	$(check.cpp);
