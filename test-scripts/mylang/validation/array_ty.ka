#!test-support/test-parser

//BEGIN pass constrained arrays
typename EmptyArray = integer[]{min=0;max=0};
typename FixedArray = integer[]{min=1;max=1};
//END

//BEGIN pass default array
typename DefaultArray = integer[]{min=0};

defun op2():integer[] = [1];
defun op1():DefaultArray = op2();
//END

//BEGIN pass minimum size array
typename ArrTy = integer[3:];
//END

//BEGIN pass maximum size array
typename ArrTy = integer[:3];
//END

//BEGIN pass minimum-max size array
typename ArrTy = integer[1:3];
//END

//BEGIN fail invalid bounds
typename ArrTy = integer[2:1];
//END

//BEGIN fail invalid constraint range
typename x = integer[]{min=255;max=0};
//END

//BEGIN fail invalid min constraint
typename x = integer[]{min=0.0};
//END

//BEGIN fail invalid max constraint
typename x = integer[]{max=1.0};
//END


//BEGIN fail unknown constraint
typename x = integer[]{maxx=1};
//END


//BEGIN fail negative lower bound
typename x = integer[]{min=-1};
//END


//BEGIN fail negative upper bound
typename x = integer[]{max=-1};
//END

//BEGIN fail union of arrays
defun f():integer
{
    defun g(a:char[:100], b:integer[:100]) = b.length < 10 ? b : a;
    return 0;
}
//END

//BEGIN pass multi-dimensional array
defun f(x:integer[1][2])
{
    val y:(integer[2])[1] = x;
    return y;
}
//END
