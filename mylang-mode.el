(setq mylang-font-lock-keywords
      (let* (
            ;; define several category of keywords
	     (x-keywords '( "nil" "do"  "typeof" "unit" "with" "safe_cast" "unsafe_cast"
			    "new" "try" "throw" "catch" "if" "else" "for" "import" "assert" "foreach"
			    "log" "return" "val" "var" "wait" "while"
			    "typename" "deftype" "defext" "defun" "defproc" "process" "transform"
			    ))
	     (x-types '("bit" "boolean" "byte" "char" "real" "integer" "string" "void"
			"union" "struct" "tuple" "mutable"))
;;            (x-constants '("ACTIVE" "AGENT" "ALL_SIDES" "ATTACH_BACK"))
;;            (x-events '("at_rot_target" "at_target" "attach"))
;;            (x-functions '("llAbs" "llAcos" "llAddToLandBanList" "llAddToLandPassList"))

            ;; generate regex string for each category of keywords
            (x-keywords-regexp (regexp-opt x-keywords 'words))
            (x-types-regexp (regexp-opt x-types 'words))
;;            (x-constants-regexp (regexp-opt x-constants 'words))
;;            (x-events-regexp (regexp-opt x-events 'words))
;;            (x-functions-regexp (regexp-opt x-functions 'words))
	    )

        `(
          (,x-types-regexp . font-lock-type-face)
;;          (,x-constants-regexp . font-lock-constant-face)
;;          (,x-events-regexp . font-lock-builtin-face)
;;          (,x-functions-regexp . font-lock-function-name-face)
          (,x-keywords-regexp . font-lock-keyword-face)
          ;; note: order above matters, because once colored, that part won't change.
          ;; in general, put longer words first
          )))

;;;###autoload
(define-derived-mode mylang-mode c-mode "mylang mode"
  "Major mode for editing mylang"

  ;; code for syntax highlighting
  (setq font-lock-defaults '((mylang-font-lock-keywords))))

;; add the mode to the `features' list
(provide 'mylang-mode)

;;; mylang-mode.el ends here
