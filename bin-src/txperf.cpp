#include <iostream>
#include <fstream>
#include <filesystem>
#include <map>
#include <optional>
#include <string>
#include <set>
#include <random>
#include <algorithm>
#include <mylang/BigUInt.h>
#include <mylang/names/FQN.h>

// variables we can control
struct PerfOpts
{

// the number of root concepts; those are
// concepts that completely standalone and just
// represent something that can be expressed as a number
  const size_t nRootConcepts = 10;

// the nesting depth for concepts; a concept at a depth
// N will have N member concepts
  const size_t conceptDepth = 5;

// the maximum number of concepts at each level > 0
// the number of concepts at level 0 is given by
// nRootConcepts
  const size_t maxNConcepts = 100;

// the maximum number of representations for a concept
  const size_t maxRepresentations = 5;

// in addition to the default transforms, we can also
// have up N additional transforms per concept
  const size_t nRandomTransforms = 10;

// number of names per directory
  const size_t nNamesPerDirectory = 25;

// the output path
  const ::std::filesystem::path outRoot = ::std::filesystem::path("genout");
};

static const PerfOpts options =
    { .nRootConcepts = 10, .conceptDepth = 5, .maxNConcepts = 100, .maxRepresentations = 5,
        .nRandomTransforms = 10, .nNamesPerDirectory = 25, .outRoot = "genout" };

static const PerfOpts xoptions = { .nRootConcepts = 2, .conceptDepth = 2, .maxNConcepts = 100,
    .maxRepresentations = 5, .nRandomTransforms = 0, .nNamesPerDirectory = 25, .outRoot = "genout" };

using BigUInt = ::mylang::BigUInt;

static BigUInt nChoosek(size_t n, size_t k)
{
  if (k > n)
    return 0;
  if (k * 2 > n)
    k = n - k;
  if (k == 0)
    return 1;

  BigUInt result(n);
  for (size_t i = 2; i <= k; ++i) {
    result = result * BigUInt(n - i + 1);
    result = result / BigUInt(i);
  }
  return result;
}

struct Concept
{
  Concept(size_t xid)
      : id(xid)
  {
  }
  Concept(size_t xid, ::std::set<size_t> xmembers)
      : id(xid), members(::std::move(xmembers))
  {
  }

  friend inline bool operator<(const Concept &a, const Concept &b)
  {
    if (a.id < b.id) {
      return true;
    }
    if (a.id == b.id) {
      return a.members < b.members;
    }
    return false;
  }
  /** name of the concept */
  size_t id;

  /** Other concepts that this may contain */
  ::std::set<size_t> members;
};

struct Representation
{
  Representation(const Concept &xconcept, size_t xcid, size_t xgid, ::std::vector<size_t> xmembers)
      : baseConcept(xconcept), members(::std::move(xmembers)), number(xcid), gid(xgid)
  {
  }

  const Concept &baseConcept;
  ::std::vector<size_t> members; // the name of a representation to use
  size_t number; // the representation number
  size_t gid; // the representation id
};
static mylang::names::FQN representationFQN(const Representation &r)
{
  ::std::vector<::std::string> parts;
  ::std::string p = "R" + ::std::to_string(r.baseConcept.id) + "_";
  size_t i = r.gid;
  while (i > 0) {
    size_t j = i % options.nNamesPerDirectory;
    i = i / options.nNamesPerDirectory;
    parts.emplace_back(p + std::to_string(j));
    p = 'N';
  }
  parts.push_back("ROOT");
  ::std::reverse(parts.begin(), parts.end());
  return mylang::names::FQN::create(parts);
}

static mylang::names::FQN transformFQN(size_t i)
{
  ::std::vector<::std::string> parts;
  char p = 'T';
  while (i > 0) {
    size_t j = i % options.nNamesPerDirectory;
    i = i / options.nNamesPerDirectory;
    parts.emplace_back(p + std::to_string(j));
    p = 'N';
  }
  parts.push_back("ROOT");
  ::std::reverse(parts.begin(), parts.end());
  return mylang::names::FQN::create(parts);
}

static void write(const ::std::filesystem::path &root,
    const ::std::map<size_t, const Representation*> &reps, size_t rid)
{
  const Representation &r = *reps.find(rid)->second;

  auto fqn = representationFQN(r);
  auto f = root / (fqn.fullName("/") + ".ka");
  auto p = f.parent_path();
  ::std::filesystem::create_directories(p);
  ::std::ofstream out(f);

  for (size_t i = 0; i < r.members.size(); ++i) {
    const Representation &xr = *reps.find(r.members.at(i))->second;
    auto m = representationFQN(xr);
    out << "import " << m.fullName() << ';' << ::std::endl;
  }
  out << ::std::endl;
  auto pFqn = fqn.parent();
  ::std::string indent = "";
  if (pFqn) {
    out << "unit " << pFqn->fullName() << " {" << ::std::endl;
    indent += "   ";
  }
  out << indent << "deftype " << fqn.localName() << " = ";
  if (r.members.size() == 0) {
    out << "integer;" << ::std::endl;
  } else {
    out << "struct {" << ::std::endl;
    for (size_t i = 0; i < r.members.size(); ++i) {
      const Representation &xr = *reps.find(r.members.at(i))->second;
      auto m = representationFQN(xr);
      out << indent << "   m" << i << ":" << m.fullName() << ";" << ::std::endl;
    }
    out << indent << "};" << ::std::endl;
  }
  if (pFqn) {
    out << '}' << ::std::endl;
  }
}

struct Transform
{
  Transform(const Representation *xfrom, const Representation *xto)
      : from(xfrom), to(xto)
  {
  }

  friend bool operator<(const Transform &a, const Transform &b)
  {
    if (a.from < b.from) {
      return true;
    }
    if (a.from == b.from) {
      return a.to < b.to;
    }
    return false;
  }
  const Representation *from;
  const Representation *to;
};

static void write(const ::std::filesystem::path &root,
    const ::std::map<size_t, const Representation*> &reps, const size_t tid, const Transform &t)
{
  const auto &from = *t.from;
  const auto &to = *t.to;

  auto fqn = transformFQN(tid);
  auto fromFQN = representationFQN(from);
  auto toFQN = representationFQN(to);

  auto f = root / (fqn.fullName("/") + ".ka");
  auto p = f.parent_path();
  ::std::filesystem::create_directories(p);
  ::std::ofstream out(f);

  out << "import " << fromFQN.fullName() << ";" << ::std::endl;
  out << "import " << toFQN.fullName() << ";" << ::std::endl;

  out << ::std::endl;
  out << "transform " << fqn.fullName() << "(from:" << fromFQN.fullName() << "):"
      << toFQN.fullName() << " {" << ::std::endl;
  out << "   return new " << toFQN.fullName();
  if (to.members.empty()) {
    out << "(safe_cast<integer>(from));" << ::std::endl;
  } else {
    out << "(" << ::std::endl;
    const char *sep = "";
    for (size_t i = 0; i < to.members.size(); ++i) {
      const Representation &xr = *reps.find(to.members.at(i))->second;
      auto outFQN = representationFQN(xr);
      out << "       " << sep << "transform<" << outFQN.fullName() << ">(from.m" << i << ")"
          << ::std::endl;
      sep = ",";
    }
    out << "   );" << ::std::endl;
  }

  out << '}' << ::std::endl;
}

template<class iter, class iterEnd>
static size_t count(iter i, iterEnd e)
{
  size_t n = 0;
  for (auto j = i; j != e; ++j, ++n)
    ;
  return n;
}

template<class iter, class iterEnd>
static iter pickRandom(::std::minstd_rand0 &rndGen, iter i, iterEnd e)
{
  size_t n = count(i, e);

  ::std::uniform_int_distribution<size_t> rnd(0, n - 1);
  if (n == 1) {
    return i;
  }
  auto k = rnd(rndGen);
  for (size_t j = 0; j < k; ++j, ++i)
    ;

  return i;
}

static const Representation* pickRepresentation(::std::minstd_rand0 &rndGen, size_t c,
    const ::std::multimap<size_t, Representation> &representations)
{
  auto begin = representations.lower_bound(c);
  auto end = representations.upper_bound(c);
  auto i = pickRandom(rndGen, begin, end);
  return &i->second;
}

static void mkRepresentations(::std::minstd_rand0 &rndGen,
    const ::std::map<size_t, Concept> &concepts, size_t conceptId, size_t nReps,
    ::std::multimap<size_t, Representation> &representations)
{
  if (representations.find(conceptId) != representations.end()) {
    return;
  }
  const auto &xconcept = concepts.find(conceptId)->second;

  for (size_t cid = 0; cid < nReps; ++cid) {
    ::std::vector<size_t> members;
    for (size_t c : xconcept.members) {
      mkRepresentations(rndGen, concepts, c, nReps, representations);
      const Representation *r = pickRepresentation(rndGen, c, representations);
      members.push_back(r->gid);
    }
    size_t gid = representations.size() + 1;
    representations.emplace(conceptId, Representation(xconcept, cid, gid, ::std::move(members)));
  }
}

static void mkTransforms(::std::minstd_rand0 &rndGen, size_t n,
    const ::std::multimap<size_t, Representation> &repsByConcept, ::std::set<Transform> &transforms)
{

  // build a guaranteed transform chain
  ::std::map<size_t, const Representation*> last;
  {
    ::std::map<size_t, const Representation*> first;
    for (const auto &e : repsByConcept) {
      auto i = last.find(e.first);
      if (i != last.end()) {
        transforms.emplace(i->second, &e.second);
      } else {
        first[e.first] = &e.second;
      }
      last[e.first] = &e.second;
    }
    // generate a transform from the first to the last
    for (const auto &e : first) {
      auto i = last.find(e.first);
      if (i != last.end() && i->second != e.second) {
        transforms.emplace(i->second, e.second);
      }
    }
  }

  // generate random transforms between two members of a concept
  if (n > 0) {
    for (const auto &x : last) {
      for (size_t k = 0; k < n; ++k) {
        auto cid = x.first;
        auto i = pickRepresentation(rndGen, cid, repsByConcept);
        auto j = pickRepresentation(rndGen, cid, repsByConcept);
        if (i != j) {
          transforms.emplace(i, j);
        }
      }
    }
  }
}

static ::std::map<size_t, Concept> makeConcepts(::std::minstd_rand0 &rndGen, size_t nPrimitives,
    size_t max, size_t nLevels)
{

  ::std::map<size_t, Concept> allConcepts;
  size_t next = 1;
  while (allConcepts.size() < nPrimitives) {
    allConcepts.emplace(next, Concept(next));
    ++next;
  }
  ::std::map<size_t, Concept> curConcepts(allConcepts);
  for (size_t level = 1; level <= nLevels; ++level) {
    const ::std::map<size_t, Concept> &concepts = allConcepts;

    ::std::set<::std::set<size_t>> levelConcepts;
    ::std::uniform_int_distribution<size_t> rnd(0, concepts.size() - 1);

    // we use a simple m_choose_m to determine the maximum
    // number of combinations we can expect
    BigUInt n = nChoosek(curConcepts.size(), level);
    //::std::cerr << "Choose  " << level+1 << " from " << concepts.size() << " == " << n << ::std::endl;

    while (levelConcepts.size() < n.min(max)) {
      ::std::set<size_t> rndConcepts;
      while (rndConcepts.size() < level) {
        auto k = rnd(rndGen);
        auto iter = concepts.begin();
        for (size_t j = 0; j < k; ++j, ++iter) {
        }
        rndConcepts.insert(iter->first);
      }
      levelConcepts.insert(::std::move(rndConcepts));
      ++next;
    }
    curConcepts.clear();
    for (const auto &cc : levelConcepts) {
      curConcepts.emplace(next, Concept(next, cc));
      ++next;
    }
    allConcepts.insert(curConcepts.begin(), curConcepts.end());
  }

  return allConcepts;
}

static void printConcepts(const ::std::map<size_t, Concept> &concepts)
{
  for (const auto &c : concepts) {
    ::std::cerr << "concept" << c.first;
    auto k = c.second.members.begin();
    for (size_t i = 0; k != c.second.members.end(); ++i, ++k) {
      ::std::cerr << ::std::endl << "\tm" << i << " concept" << *k;
    }
    ::std::cerr << ::std::endl;
  }
}

int main(int argc, const char **argv)
{
  if (argc != 1) {
    return 1;
  }

  ::std::minstd_rand0 rndGen;

  auto concepts = makeConcepts(rndGen, options.nRootConcepts, options.maxNConcepts,
      options.conceptDepth);
//	printConcepts(concepts);
  ::std::cerr << "Num concepts " << concepts.size() << ::std::endl;

  // generate the representations for each concept
  ::std::multimap<size_t, Representation> representationsByConcept;
  for (auto c : concepts) {
    mkRepresentations(rndGen, concepts, c.first, options.maxRepresentations,
        representationsByConcept);
  }
  ::std::cerr << "Num representations " << representationsByConcept.size() << ::std::endl;
  ::std::set<Transform> transforms;
  mkTransforms(rndGen, options.nRandomTransforms, representationsByConcept, transforms);
  ::std::cerr << "Num transforms " << transforms.size() << ::std::endl;

  ::std::map<size_t, const Representation*> reps;
  for (const auto &e : representationsByConcept) {
    reps.emplace(e.second.gid, &e.second);
  }

  for (const auto &e : reps) {
    write(options.outRoot, reps, e.first);
  }
  size_t tid = 1;
  for (const auto &t : transforms) {
    write(options.outRoot, reps, tid++, t);
  }

  return 0;
}
