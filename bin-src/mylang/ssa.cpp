#include <mylang/api/Parser.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cassert>
#include <mylang/names.h>
#include <mylang/io/Directory.h>
#include <mylang/io/File.h>
#include <mylang/vast/transforms/AddGuards.h>
#include <mylang/vast/transforms/AddDefaultConstructors.h>
#include <mylang/vast/transforms/RegisterProcessEvents.h>
#include <mylang/vast/transforms/InstantiateGenerics.h>
#include <mylang/ethir/vast2ethir.h>
#include <mylang/ethir/prettyPrint.h>
#include <mylang/ethir/Transform.h>
#include <mylang/codegen/model/ethir2model.h>
#include <mylang/ethir/ssa/ToSSA.h>
#include <mylang/ethir/ssa/FromSSA.h>
#include <mylang/filesystem/IterableFileSystem.h>
#include <mylang/filesystem/UnionFS.h>

static bool startsWith(const ::std::string &prefix, ::std::string &arg)
{
  if (arg.find(prefix) == 0) {
    arg.erase(0, prefix.length());
    return true;
  }
  return false;
}

int main(int argc, char **argv)
{
  ::std::vector<const char*> args;
  size_t optLevel = 0;

  for (int i = 1; i < argc; ++i) {
    args.emplace_back(argv[i]);
  }

  int status = 1;
  ::std::set<::mylang::filesystem::IterableFileSystem::Ptr> searchPaths;

  try {
    for (auto i = args.begin(); i != args.end();) {
      ::std::string arg(*i);
      if (arg == "--") {
        break;
      } else if (startsWith("-I", arg)) {
        searchPaths.insert(mylang::filesystem::IterableFileSystem::create(arg));
        i = args.erase(i);
      } else if (arg == "-O") {
        ++optLevel;
        args.erase(i);
      } else {
        ++i;
      }
    }
  } catch (const ::std::exception &e) {
    ::std::cerr << "failed to parse arguments : " << e.what() << ::std::endl;
    return 2;
  }

  if (args.size() < 1 || args.size() > 2) {
    ::std::cerr << "usage [-I <search-path>] [-O]" << argv[0] << " <input-file> [<output-file>]"
        << ::std::endl;
    return 1;
  }

  mylang::api::Parser::Options parserOpts;
  auto fs = mylang::filesystem::UnionFS::create(searchPaths.begin(), searchPaths.end());
  parserOpts.filesystem = fs;
  auto parser = mylang::api::Parser::create(parserOpts);

  auto tree = parser->parse(args.at(0));
  if (tree == nullptr) {
    return 1;
  }

  ::std::unique_ptr<::std::ostream> outfile;

  if (args.size() == 2) {
    outfile = ::std::make_unique<::std::ofstream>(args.at(1));
  }

  ::std::ostream &out = outfile ? *outfile : ::std::cout;

  // instantiate all generics should maybe even be done by validateAST?
  tree = mylang::vast::transforms::InstantiateGenerics::apply(tree);
  tree = mylang::vast::transforms::AddGuards::apply(tree);
  tree = mylang::vast::transforms::AddDefaultConstructors::apply(tree);
  tree = mylang::vast::transforms::RegisterProcessEvents::apply(tree);

  try {
    status = 1;
    auto program = mylang::ethir::vast2ethir(tree);
    if (program) {
      {
        ::std::ofstream fout("source.ir");
        mylang::ethir::prettyPrint(program, fout);
      }

      //  SSA transform
      auto ssa = mylang::ethir::ssa::ToSSA::transform(program);
      if (ssa) {

        {
          ::std::ostringstream sout;
          {
            mylang::ethir::prettyPrint(ssa, sout);
            ::std::ofstream fout("ssa.ir");
            fout << sout.str();
          }
          out << ">>>>> SSA FORM >>>>>" << ::std::endl;
          out << sout.str();
          out << "<<<<< SSA FORM <<<<<" << ::std::endl;
        }
        {
          auto from_ssa = mylang::ethir::ssa::FromSSA::transform(ssa);
          if (from_ssa) {
            ::std::ofstream fout("from_ssa.ir");
            mylang::ethir::prettyPrint(from_ssa, fout);

            mylang::codegen::model::EthirToModel conv;
            auto code_model = conv.transform(program);
            if (code_model) {
              status = 0;
            } else {
              ::std::cerr << "Internal error: could not generate code model" << ::std::endl;
            }
          }
        }
      }
    }

    if (status != 0) {
      ::std::cerr << "Internal error: could not generate program" << ::std::endl;
    }

  } catch (::std::exception &e) {
    ::std::cerr << "Error:" << e.what() << ::std::endl;
  }
  return status;
}

