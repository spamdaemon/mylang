#include <mylang/api/Parser.h>
#include <mylang/filesystem/IterableUnionFS.h>
#include <mylang/transforms/repo/discover.h>
#include <iostream>
#include <iterator>
#include <memory>
#include <string>
#include <vector>

static bool startsWith(const ::std::string &prefix, ::std::string &arg)
{
  if (arg.find(prefix) == 0) {
    arg.erase(0, prefix.length());
    return true;
  }
  return false;
}

int main(int argc, char **argv)
{
  int status;

  ::std::vector<::std::string> args;
  ::std::set<::mylang::filesystem::IterableFileSystem::Ptr> searchPaths;
  bool lenient = false;
  bool resolve = false;

  for (int i = 1; i < argc; ++i) {
    args.emplace_back(argv[i]);
  }

  try {
    for (auto i = args.begin(); i != args.end();) {
      ::std::string arg(*i);
      if (arg == "--") {
        break;
      } else if (arg == "--lenient" || arg == "-l") {
        lenient = true;
        i = args.erase(i);
      } else if (arg == "--resolve" || arg == "-r") {
        resolve = true;
        i = args.erase(i);
      } else if (arg == "-I") {
        i = args.erase(i);
        searchPaths.insert(mylang::filesystem::IterableFileSystem::create(*i));
        i = args.erase(i);
      } else if (startsWith("-I", arg)) {
        searchPaths.insert(mylang::filesystem::IterableFileSystem::create(arg));
        i = args.erase(i);
      } else if (startsWith("-", arg)) {
        ::std::cerr << "Unknown option " << arg << ::std::endl;
        return 2;
      } else
        ++i;
    }
  } catch (const ::std::exception &e) {
    ::std::cerr << "failed to parse arguments : " << e.what() << ::std::endl;
    return 2;
  }
  if (args.size() != 1) {
    ::std::cerr << argv[0] << " [-I <search-path>] <input-file>" << ::std::endl;
    return 1;
  }

  mylang::api::Parser::Options parserOpts;
  auto fs = mylang::filesystem::IterableUnionFS::create(::std::move(searchPaths));
  if (lenient) {
    fs = fs->makeLenient();
  }
  parserOpts.filesystem = fs;
  if (resolve) {
    parserOpts.transforms = mylang::transforms::repo::discoverTransforms(fs, false);
  }
  auto parser = mylang::api::Parser::create(parserOpts);
  status = 1;
  if (parser->lint(args.at(0))) {
    status = 0;
  }
  return status;
}

