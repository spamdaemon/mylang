#include <mylang/api/Parser.h>
#include <mylang/api/Optimizer.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <optional>
#include <string>
#include <cassert>
#include <parser.h>
#include <mylang/names.h>
#include <mylang/io/Directory.h>
#include <mylang/io/File.h>
#include <mylang/vast/ValidationError.h>
#include <mylang/vast/transforms/AddDefaultConstructors.h>
#include <mylang/vast/transforms/AddGuards.h>
#include <mylang/vast/transforms/RegisterProcessEvents.h>
#include <mylang/vast/transforms/InstantiateGenerics.h>
#include <mylang/vast/transforms/ReplaceCasts.h>
#include <mylang/ethir/ethir.h>
#include <mylang/ethir/analysis/TypeAnalysis.h>
#include <mylang/ethir/ssa/SSAProgram.h>
#include <mylang/ethir/ssa/SSAPass.h>
#include <mylang/ethir/ssa/PrunePass.h>
#include <mylang/ethir/ssa/PruneUnreachableCode.h>
#include <mylang/ethir/ssa/ConstFoldPass.h>
#include <mylang/ethir/ssa/ShadowTypingPass.h>
#include <mylang/ethir/ssa/MaintainSSAPass.h>
#include <mylang/ethir/ssa/CSEPass.h>
#include <mylang/ethir/ssa/SimplifyPass.h>
#include <mylang/ethir/ssa/DeriveLiterals.h>
#include <mylang/ethir/ssa/peepholes/peepholes.h>
#include <mylang/ethir/transforms/NormalizeFunction.h>
#include <mylang/ethir/transforms/RewriteBuiltins.h>
#include <mylang/ethir/transforms/NativeTyping.h>
#include <mylang/ethir/transforms/ToLoops.h>
#include <mylang/ethir/transforms/PruneProcessMembers.h>
#include <mylang/ethir/transforms/LoopFusion.h>
#include <mylang/ethir/transforms/InlineFunctions.h>
#include <mylang/ethir/vast2ethir.h>
#include <mylang/ethir/prettyPrint.h>
#include <mylang/codegen/model/ethir2model.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/javagen/ir/ProgramGen.h>
#include <mylang/codegen/javagen/buildtools/maven/POMBuilder.h>
#include <mylang/codegen/javagen/buildtools/bash/ScriptBuilder.h>
#include <mylang/codegen/javagen/runtools/bash/ScriptBuilder.h>
#include <mylang/codegen/typing/NativeJavaTyping.h>
#include <mylang/transforms/TransformBase.h>
#include <mylang/transforms/repo/discover.h>
#include <mylang/filesystem/IterableFileSystem.h>
#include <mylang/filesystem/IterableUnionFS.h>

using SSAProgram = mylang::ethir::ssa::SSAProgram;
static size_t optLevel = 0;
static size_t debugLevel = 0;

enum class Flag
{
  ENABLED, DISABLED, DEFAULT
};

static Flag optInline = Flag::DEFAULT;
static Flag optCSE = Flag::DEFAULT;
static Flag optShadow = Flag::DEFAULT;
static Flag optPeephole = Flag::DEFAULT;

::std::string makeTraceFile(::std::optional<::std::string> folder, const ::std::string &name)
{
  static size_t i = 0;
  ::std::ostringstream out;
  if (folder.has_value()) {
    out << folder.value() << '/';
  }
  out.width(2);
  out.fill('0');
  out << i << "--" << name;
  ++i;
  return out.str();
}

struct OptimizerPass
{
  size_t optLevel;
  Flag enable;
  ::std::unique_ptr<mylang::ethir::ssa::SSAPass> pass;

  bool isEnabled(size_t specifiedOptLevel) const
  {
    if (enable == Flag::ENABLED) {
      return true;
    }
    if (enable == Flag::DISABLED) {
      return false;
    }
    return optLevel <= specifiedOptLevel;
  }
};

static int optLoopPrefix = 0;
static int optLoopCounter = 0;
static void emitIR(SSAProgram &prg, const OptimizerPass *pass, int dbgLevel,
    const std::string &folder)
{
  if (dbgLevel > 1) {
    ::std::string passname;
    if (pass) {
      passname = (typeid(*pass->pass).name());
    } else {
      passname = "pre";
    }
    ::std::ofstream fout(
        makeTraceFile(folder,
            ::std::to_string(optLoopPrefix) + "-optimization-loop-"
                + ::std::to_string(optLoopCounter++) + '-' + passname + ".ir"));
    mylang::ethir::prettyPrint(prg.getSSAForm(), fout);
  }

}

static mylang::ethir::ssa::SimplifyPass::Peepholes getPeepholes()
{
  return mylang::ethir::ssa::SimplifyPass::Peepholes( {
      mylang::ethir::ssa::Peephole::createMapIdentity(),
      mylang::ethir::ssa::Peephole::createCallWrapper(),
      mylang::ethir::ssa::peepholes::BitLikeOps::getPeephole(),
      mylang::ethir::ssa::peepholes::Arrays::getReversePeephole(),
      mylang::ethir::ssa::peepholes::Arrays::getPeephole(),
      mylang::ethir::ssa::peepholes::Optionals::getPeephole(),
      mylang::ethir::ssa::peepholes::Structs::getPeephole(),
      mylang::ethir::ssa::peepholes::Unions::getPeephole(),
      mylang::ethir::ssa::peepholes::Tuples::getPeephole() });
}

static void optimize(SSAProgram &prg, int dbgLevel, const ::std::string &folder,
    bool enableShadowTyping)
{
  ::std::vector<OptimizerPass> passes;
  passes.push_back( { 1, Flag::DEFAULT,
      ::std::make_unique<mylang::ethir::ssa::PruneUnreachableCode>() });
  passes.push_back( { 1, Flag::DEFAULT, ::std::make_unique<mylang::ethir::ssa::PrunePass>() });
  passes.push_back(
      { 1, Flag::DEFAULT, ::std::make_unique<mylang::ethir::ssa::MaintainSSAPass>() });
  passes.push_back( { 1, Flag::DEFAULT, ::std::make_unique<mylang::ethir::ssa::ConstFoldPass>() });
  passes.push_back( { 1, Flag::DEFAULT, ::std::make_unique<mylang::ethir::ssa::DeriveLiterals>() });
  passes.push_back( { 2, optCSE, ::std::make_unique<mylang::ethir::ssa::CSEPass>() });
  passes.push_back(
      { 2, optPeephole, ::std::make_unique<mylang::ethir::ssa::SimplifyPass>(getPeepholes()) });
  if (enableShadowTyping) {
    passes.push_back( { 3, optShadow, ::std::make_unique<mylang::ethir::ssa::ShadowTypingPass>() });
  }

  emitIR(prg, nullptr, dbgLevel, folder);

  // apply the various SSA optimizations
  size_t level = 1;
  while (level <= optLevel) {
    // optimize at the current level
    bool continueOptimize = false;
    for (size_t i = 0; i < passes.size(); ++i) {
      if (passes[i].optLevel == level && passes[i].isEnabled(level)) {
        if (passes[i].pass->transformProgram(prg)) {
          continueOptimize = true;
          emitIR(prg, &passes[i], dbgLevel, folder);
        }
      }
    }
    // if we optimized, then reset and start over at level 1
    // otherwise, go to the next level and finish that
    if (continueOptimize) {
      level = 1;
    } else {
      ++level;
    }
  }
}

struct Context: public mylang::codegen::javagen::ir::Context
{
  Context(int xoptLevel, bool xuseNativeTypes, const ::std::string &xoutputFolder)
      : mylang::codegen::javagen::ir::Context(xoptLevel, xuseNativeTypes),
          outputFolder(xoutputFolder), sourceFolder(xoutputFolder + "/src/main/java"),
          codeGenerated(false)
  {
  }

  ~Context()
  {
  }

  void mkOutputDir() const
  {
    mkdirs(outputFolder);
  }

  void mkdirs(const ::std::string &dir) const
  {
    ::std::string::size_type i = 0;
    for (::std::string::size_type j; (j = dir.find('/', i)) != ::std::string::npos;) {
      auto segment = dir.substr(0, j);
      if (segment.length() > 0) {
        ::mylang::io::Directory::create(segment);
      }
      i = j + 1;
    }

    ::mylang::io::Directory::create(dir);
  }

  void writeClass(const ::std::string &classname, const ::std::string &text, bool haveMain) const
  override
  {
    auto fqn = mylang::names::FQN::parseFQN(classname, '.');
    if (!fqn) {
      throw ::std::invalid_argument("Not a valid classname " + classname);
    }

    if (!codeGenerated) {
      codeGenerated = true;
      mkdirs(sourceFolder);
    }

    ::mylang::io::Directory dir = ::mylang::io::Directory::create(sourceFolder);
    dir.createFile(*mylang::names::Name::create(*fqn), ".java", text, true);

    if (haveMain) {
      pomBuilder.addMainClass(*fqn);
      generateBashRunner(*fqn);
    }
  }

  void generateBashRunner(const mylang::names::FQN &javafqn) const
  {
    ::mylang::codegen::javagen::runtools::bash::ScriptBuilder bashRunScriptBuilder;
    mkdirs(sourceFolder);
    ::mylang::io::Directory dir = ::mylang::io::Directory::create(outputFolder);
    ::std::ostringstream runFile;
    bashRunScriptBuilder.setMainClass(javafqn);
    bashRunScriptBuilder.write(runFile);
    dir.createScriptFile("run-" + javafqn.fullName(".") + ".sh", runFile.str());
  }

  void writeBuildScript()
  {
    ::mylang::codegen::javagen::buildtools::bash::ScriptBuilder bashBuildScriptBuilder;
    mkdirs(sourceFolder);
    ::mylang::io::Directory dir = ::mylang::io::Directory::create(outputFolder);
    ::std::ostringstream buildFile;
    bashBuildScriptBuilder.write(buildFile);
    dir.createScriptFile("build.sh", buildFile.str());
  }

  void writePOM()
  {
    mkdirs(sourceFolder);
    ::mylang::io::Directory dir = ::mylang::io::Directory::create(outputFolder);
    ::std::ostringstream pomFile;
    pomBuilder.write(pomFile);
    dir.createFile("pom.xml", pomFile.str());

#if MAVEN_BUILD_SCRIPT
    ::std::ostringstream buildScript;
    buildScript << "#!/bin/bash" << ::std::endl;
    buildScript << "SCRIPTDIR=\"$(dirname \"$0\")\"" << ::std::endl;
    buildScript << "cd \"${SCRIPTDIR}\"" << ::std::endl;
    buildScript << "mvn compile --offline" << ::std::endl;
    dir.createScriptFile("build.sh", buildScript.str());
#endif
  }

  mutable ::mylang::codegen::javagen::buildtools::maven::POMBuilder pomBuilder;
  const ::std::string outputFolder;
  const ::std::string sourceFolder;
  mutable bool codeGenerated;
};

static mylang::ethir::EProgram optimize(mylang::ethir::EProgram prog,
    const ::std::function<void(SSAProgram&)> &optimizer, int dbgLevel,
    const ::std::string &outputFolder, const std::string &prefix)
{
  ++optLoopPrefix;
  optLoopCounter = 0;

  auto ssaForm = SSAProgram::createSSA(prog);

  if (ssaForm) {
    mylang::ethir::EProgram preopt;
    if (dbgLevel > 0) {
      preopt = ssaForm->getSSAForm();
      ::std::ofstream fout(
          makeTraceFile(outputFolder,
              ::std::to_string(optLoopPrefix) + "-" + prefix + "pre-opt-ssa.ir"));
      mylang::ethir::prettyPrint(preopt, fout);
    }

    optimizer(*ssaForm);

    if (dbgLevel > 0 && preopt != ssaForm->getSSAForm()) {
      ::std::ofstream fout(
          makeTraceFile(outputFolder,
              ::std::to_string(optLoopPrefix) + "-" + prefix + "post-opt-ssa.ir"));
      mylang::ethir::prettyPrint(ssaForm->getSSAForm(), fout);
    }
    auto res = ssaForm->getNonSSAForm();
    if (res) {
      if (dbgLevel > 0) {
        ::std::ofstream fout(
            makeTraceFile(outputFolder,
                ::std::to_string(optLoopPrefix) + "-" + prefix + "post-ssa.ir"));
        mylang::ethir::prettyPrint(res, fout);
      }

      return res;
    } else {
      ::std::cerr << "Failed to convert out of SSA form" << ::std::endl;
    }
  } else {
    ::std::cerr << "Failed to convert to SSA form " << ::std::endl;
  }
  return nullptr;
}

static void pruneWellKnownFunctions(SSAProgram &prg)
{
  auto f = [](
      mylang::ethir::EGlobal g) {return g->kind == mylang::ethir::ir::GlobalValue::EXPORTED;};
  auto pass1 = ::std::make_unique<mylang::ethir::ssa::PrunePass>(f);
  auto pass2 = ::std::make_unique<mylang::ethir::ssa::PruneUnreachableCode>();

  bool continuePrune = true;
  while (continuePrune) {
    continuePrune = pass1->transformProgram(prg);
    continuePrune = continuePrune || pass2->transformProgram(prg);
  }
}

mylang::ethir::EProgram reduceBuiltins(mylang::ethir::EProgram prg)
{
  mylang::ethir::transforms::RewriteBuiltins tx;
  mylang::ethir::transforms::ToLoops toLoops;

  mylang::ethir::TransformResult<mylang::ethir::ENode> res(prg, true);

  for (bool cont = true; cont;) {
    cont = false;
    res = tx.transformNode(res.actual);
    cont = cont || res.isNew;
    res = toLoops.transformNode(res.actual);
    cont = cont || res.isNew;
  }
  return res.actual->self<mylang::ethir::ir::Program>();
}

static bool startsWith(const ::std::string &prefix, ::std::string &arg)
{
  if (arg.find(prefix) == 0) {
    arg.erase(0, prefix.length());
    return true;
  }
  return false;
}

int main(int argc, char **argv)
{
  ::std::vector<const char*> args;

  for (int i = 1; i < argc; ++i) {
    args.emplace_back(argv[i]);
  }

  ::std::set<::mylang::filesystem::IterableFileSystem::Ptr> searchPaths;
  bool lenient = false;
  ::std::string outdir("gen");

  try {
    for (auto i = args.begin(); i != args.end();) {
      ::std::string arg(*i);
      if (arg == "--") {
        break;
      } else if (arg == "-o") {
        i = args.erase(i);
        outdir = *i;
        i = args.erase(i);
      } else if (startsWith("-o", arg)) {
        outdir = arg;
        i = args.erase(i);
      } else if (arg == "-I") {
        i = args.erase(i);
        searchPaths.insert(mylang::filesystem::IterableFileSystem::create(*i));
        i = args.erase(i);
      } else if (startsWith("-I", arg)) {
        searchPaths.insert(mylang::filesystem::IterableFileSystem::create(arg));
        i = args.erase(i);
      } else if (arg == "-g") {
        ++debugLevel;
        i = args.erase(i);
      } else if (arg == "--lenient" || arg == "-l") {
        i = args.erase(i);
        lenient = true;
      } else if (startsWith("-Ono-", arg)) {
        if (arg == "cse") {
          optCSE = Flag::DISABLED;
        } else if (arg == "inline") {
          optInline = Flag::DISABLED;
        } else if (arg == "peephole") {
          optPeephole = Flag::DISABLED;
        } else if (arg == "shadow") {
          optShadow = Flag::DISABLED;
        } else {
          throw ::std::invalid_argument("unknown optimization " + arg + " ignored");
        }
        i = args.erase(i);
      } else if (startsWith("-O", arg)) {
        if (arg == "cse") {
          optCSE = Flag::ENABLED;
        } else if (arg == "inline") {
          optInline = Flag::ENABLED;
        } else if (arg == "peephole") {
          optPeephole = Flag::ENABLED;
        } else if (arg == "shadow") {
          optShadow = Flag::ENABLED;
        } else {
          try {
            int n = ::std::stoi(arg);
            if (n < 0) {
              throw ::std::invalid_argument("Not a valid optimization level");
            }
            optLevel = n;
          } catch (...) {
            throw ::std::invalid_argument("unknown optimization " + arg + " ignored");
          }
        }
        i = args.erase(i);
      } else {
        ++i;
      }
    }
  } catch (const ::std::exception &e) {
    ::std::cerr << "failed to parse arguments : " << e.what() << ::std::endl;
    return 2;
  }

  if (args.size() != 1) {
    ::std::cerr << argv[0]
        << " [-O1] [-O2] [-O3] [-g] [-l] [-I <search-path>] [-o <output-path>] <input-file> "
        << ::std::endl;
    return 1;
  }

  mylang::api::Parser::Options parserOpts;
  auto fs = mylang::filesystem::IterableUnionFS::create(::std::move(searchPaths));
  if (lenient) {
    fs = fs->makeLenient();
  }
  parserOpts.filesystem = fs;
  auto txDB = parserOpts.transforms = mylang::transforms::repo::discoverTransforms(fs);
  auto parser = mylang::api::Parser::create(parserOpts);

  auto tree = parser->parse(args.at(0));
  if (tree == nullptr) {
    return 1;
  }

  // run the parser
  try {

    // instantiate all generics should maybe even be done by validateAST?
    tree = mylang::vast::transforms::InstantiateGenerics::apply(tree);
    tree = mylang::vast::transforms::AddGuards::apply(tree);
    tree = mylang::vast::transforms::ReplaceCasts::apply(tree);
    tree = mylang::vast::transforms::AddDefaultConstructors::apply(tree);
    tree = mylang::vast::transforms::RegisterProcessEvents::apply(tree);

    // resolve any transforms that may be present
  } catch (const mylang::vast::ValidationError &e) {
    ::std::cerr << e.what() << ::std::endl;
  }

  mylang::ethir::EProgram ethir_program;
  try {
    ethir_program = mylang::ethir::vast2ethir(tree);
    if (!ethir_program) {
      ::std::cerr << "Internal error: could not generate ethir program" << ::std::endl;
      return 1;
    }
  } catch (const ::std::exception &ex) {
    ::std::cerr << "Caught an unexpected exception: " << ex.what() << ::std::endl;
    ::std::cerr << "Exiting...";
    return 1;
  }

  try {
    const bool useNativeTypes = optLevel > 2;
    Context ctx(optLevel, useNativeTypes, outdir);
    if (debugLevel > 0) {
      ctx.mkOutputDir();
    }

    if (debugLevel > 0) {
      ::std::ofstream fout(makeTraceFile(outdir, "program.ir"));
      mylang::ethir::prettyPrint(ethir_program, fout);
    }

    mylang::api::Optimizer::Options opts(optLevel);
    if (debugLevel > 0) {
      opts.passLogger = mylang::api::Optimizer::createPassLogger(outdir);
    }
    opts.maxOptimizationLoops = 1000;

    if (optLevel > 2) {
      mylang::codegen::typing::NativeJavaTyping javaTypes;
      opts.nativeTypeConverter = javaTypes;
    }

    auto optimizer = mylang::api::Optimizer::create(opts);
    ethir_program = optimizer->optimize(ethir_program);

    // we need to prune away functions that we've introduced, but are not actually
    // referenced by anyone
    {
      auto prg = optimize(ethir_program, pruneWellKnownFunctions, debugLevel, outdir,
          "prune-well-known-");
      if (prg != ethir_program && debugLevel > 0) {
        {
          ::std::ofstream fout(makeTraceFile(outdir, "pre-prune-well-known.ir"));
          mylang::ethir::prettyPrint(ethir_program, fout);
        }
        {
          ::std::ofstream fout(makeTraceFile(outdir, "post-prune-well-known.ir"));
          mylang::ethir::prettyPrint(prg, fout);
        }
      }
      ethir_program = prg;
    }

    mylang::codegen::model::EthirToModel conv;
    conv.stripComments = debugLevel == 0;

    if (debugLevel > 0) {
      ::std::ofstream fout(makeTraceFile(outdir, "program-pre-codegen.ir"));
      mylang::ethir::prettyPrint(ethir_program, fout);
    }
    auto program = conv.transform(ethir_program);
    if (!program) {
      ::std::cerr << "Internal error: could not generate program model" << ::std::endl;
      return 1;
    }

    ctx.programGen->generate(ctx, program);
    if (ctx.codeGenerated) {
      ctx.writePOM();
      ctx.writeBuildScript();
    } else {
      ::std::cerr << "No code generated" << ::std::endl;
      return 1;
    }

  } catch (::std::exception &e) {
    ::std::cerr << "Error:" << e.what() << ::std::endl;
    return 1;
  }
  return 0;
}

