#include <mylang/api/Parser.h>
#include <idioma/ast/GroupNode.h>
#include <mylang/defs.h>
#include <mylang/io/File.h>
#include <parser.h>
#include <iostream>
#include <sstream>
#include <iterator>
#include <memory>
#include <string>
#include <vector>
#include <filesystem>
#include <iterator>
#include <map>
#include <set>
#include <mylang/transforms/base/TransformDB.h>
#include <mylang/transforms/Type.h>
#include <mylang/ethir/vast2ethir.h>
#include <mylang/ethir/prettyPrint.h>

using TransformDB = mylang::transforms::base::TransformDB;
typedef mylang::transforms::base::TransformDB::Ptr DatabasePtr;

static bool startsWith(const ::std::string &prefix, ::std::string &arg)
{
  if (arg.find(prefix) == 0) {
    arg.erase(0, prefix.length());
    return true;
  }
  return false;
}

static TransformDB::FQN toFQN(const ::std::string &s)
{
  // if we have slashes and . then we treat the name as file
  ::std::optional<TransformDB::FQN> fqn;
  if (s.find('/') != ::std::string::npos) {
    ::std::filesystem::path p(s);
    p = p.lexically_normal();
    p.replace_extension("");
    auto str = p.string();
    if (str.back() == '/') {
      str.pop_back();
    }
    fqn = TransformDB::FQN::parseFQN(str, '/');
  } else {
    fqn = TransformDB::FQN::parseFQN(s);
  }
  if (!fqn) {
    throw ::std::invalid_argument("Not a valid fqn " + s);
  }
  return *fqn;
}

struct Call
{
  mylang::transforms::Type output;
  ::std::vector<mylang::transforms::Type> argTypes;

  inline mylang::transforms::Call getCall() const
  {
    ::std::set<mylang::transforms::Type> inputs;
    inputs.insert(argTypes.begin(), argTypes.end());
    return mylang::transforms::Call(output, ::std::move(inputs));
  }
};
static Call parseCall(::std::string c)
{
  bool opt = false;
  if (*c.rbegin() == '?') {
    opt = true;
    c = c.substr(0, c.size() - 1);
  }
  ::std::vector<mylang::transforms::Type> input;
  ::std::string::size_type i = 0;
  for (::std::string::size_type j; (j = c.find(':', i)) != ::std::string::npos; i = j + 1) {
    input.push_back(mylang::transforms::Type(c.substr(i, j - i)));
  }

  mylang::transforms::Type output(c.substr(i, ::std::string::npos), opt);
  return Call { output, input };
}

struct Command
{
  Command()
  {
  }

  virtual ~Command()
  {
  }

  virtual int run() = 0;
};

struct DbCommand: public Command
{
  DbCommand(DatabasePtr xdb)
      : db(::std::move(xdb))
  {
  }

  ~DbCommand()
  {
  }

  const DatabasePtr db;

};
struct ListCommand: public DbCommand
{

  ListCommand(DatabasePtr xdb, ::std::set<TransformDB::FQN> p)
      : DbCommand(xdb), prefixes(::std::move(p))
  {
  }

  ~ListCommand()
  {
  }

  int run()
  {
    for (const auto &e : db->transforms()) {
      bool ignore = !prefixes.empty();
      if (ignore) {
        for (const auto &prefix : prefixes) {
          if (prefix.isPrefixOf(e.first)) {
            ignore = false;
            break;
          }
        }
      }
      if (ignore) {
        ::std::cout << e.first << ::std::endl;
      }
    }
    return 0;
  }
  ::std::set<TransformDB::FQN> prefixes;

};

struct StatusCommand: public DbCommand
{

  StatusCommand(DatabasePtr xdb, ::std::set<TransformDB::FQN> p)
      : DbCommand(xdb), prefixes(::std::move(p))
  {
  }

  ~StatusCommand()
  {
  }

  int run()
  {
    for (const auto &e : db->status(prefixes)) {
      ::std::cout << e.first << ' ';
      switch (e.second) {
      case TransformDB::Status::UNCHANGED:
        ::std::cout << "unchanged";
        break;
      case TransformDB::Status::ADDED:
        ::std::cout << "added";
        break;
      case TransformDB::Status::REMOVED:
        ::std::cout << "removed";
        break;
      case TransformDB::Status::CHANGED:
        ::std::cout << "changed";
        break;
      }
      ::std::cout << ::std::endl;
    }
    return 0;
  }

  ::std::set<TransformDB::FQN> prefixes;
};

struct SyncCommand: public DbCommand
{

  SyncCommand(DatabasePtr xdb, ::std::set<TransformDB::FQN> p)
      : DbCommand(xdb), prefixes(::std::move(p))
  {
  }

  ~SyncCommand()
  {
  }

  int run()
  {
    db->synchronize(prefixes);
    return 0;
  }
  ::std::set<TransformDB::FQN> prefixes;

};

struct CatCommand: public DbCommand
{

  CatCommand(DatabasePtr xdb, const TransformDB::FQN &xfqn)
      : DbCommand(xdb), fqn(xfqn)
  {
  }

  ~CatCommand()
  {
  }

  int run()
  {
    auto is = db->getFileSystem()->open(fqn, nullptr);
    if (!is) {
      ::std::cerr << "No such transform " << ::std::endl;
      return 1;
    }
    ::std::ostreambuf_iterator out(::std::cout);
    ::std::istreambuf_iterator<char> in(*is), inEnd;
    ::std::copy(in, inEnd, out);
    return 0;
  }

  const TransformDB::FQN fqn;
};

struct FindCommand: public DbCommand
{

  FindCommand(DatabasePtr xdb, const Call &xcall)
      : DbCommand(xdb), call(xcall)
  {
  }

  ~FindCommand()
  {
  }

  int run()
  {
    auto tx = db->findChain(call.getCall(), TransformDB::QUALITY_BEST);
    if (!tx) {
      ::std::cerr << "No chain for call " << call.getCall() << ::std::endl;
      return 1;
    }
    ::std::cerr << tx->toString() << ::std::endl;
    return 0;
  }

  const Call call;
};

struct GenCommand: public DbCommand
{

  GenCommand(DatabasePtr xdb, ::std::map<TransformDB::FQN, Call> xcalls)
      : DbCommand(xdb), compile(false), calls(::std::move(xcalls))
  {
  }

  ~GenCommand()
  {
  }

  static void emitChain(size_t &i, const ::std::string &indent,
      const ::std::map<mylang::transforms::Type, ::std::string> args,
      mylang::transforms::Chain::Ptr chain, ::std::ostream &out)
  {
    ::std::string nextIndent = indent + "  ";

    auto t = chain->getTransform();
    if (t->isIdentity()) {
      out << args.find(chain->getOutputType())->second;
      return;
    }

    ::std::vector<::std::string> vars;
    ::std::vector<::std::string> opts;
    for (auto argTy : t->call.input) {
      auto argChain = chain->getArgChain(*argTy.name);
      if (argChain->getTransform()->isIdentity()) {
        vars.push_back(args.find(argChain->getOutputType())->second);
      } else {
        ::std::string var("tmp" + std::to_string(i++));
        out << indent << "with " << std::endl << nextIndent << var << " = ";
        emitChain(i, nextIndent, args, argChain, out);
        out << ';' << ::std::endl << indent << "do" << ::std::endl;
        if (argChain->getOutputType().optional) {
          vars.push_back("*" + var);
          opts.push_back(var);
        } else {
          vars.push_back(var);
        }
      }
    }
    out << nextIndent;
    if (!opts.empty()) {
      ::std::string sep = "";
      out << "(";
      for (auto v : opts) {
        out << sep << v << ".present";
        sep = " && ";
      }
      out << ") ? ";
    }
    if (!t->call.output.optional && chain->getOutputType().optional) {
      out << '&';
    }
    out << t->name << "(";
    ::std::string sep;
    for (auto v : vars) {
      out << sep;
      sep = ", ";
      out << v;
    }
    out << ")";
    if (!opts.empty()) {
      out << " : nil";
    }
  }

  void collectImports(const mylang::transforms::Chain::Ptr &chain,
      ::std::set<TransformDB::FQN> &imports)
  {
    auto tx = chain->getTransform();
    if (tx->isIdentity()) {
      return;
    }
    imports.insert(toFQN(tx->name));
    for (auto arg : tx->call.input) {
      collectImports(chain->getArgChain(*arg.name), imports);
    }
  }

  int run()
  {
    ::std::set<TransformDB::FQN> imports;
    ::std::map<mylang::transforms::Call, mylang::transforms::Chain::Ptr> chains;
    bool error = false;
    for (const auto &e : calls) {
      const ::mylang::transforms::Call call = e.second.getCall();
      mylang::transforms::Chain::Ptr chain = db->findChain(call, TransformDB::QUALITY_BEST);
      if (chain) {
        imports.insert(toFQN(chain->getOutputType().asNonOptional().getName()));
        for (auto t : call.input) {
          imports.insert(toFQN(t.asNonOptional().getName()));
        }
        collectImports(chain, imports);
        chains.emplace(call, chain);
      } else {
        ::std::cerr << "Failed to find a transform chain for " << call << ::std::endl;
        error = true;
      }
    }
    if (error) {
      return 1;
    }
    ::std::ostringstream out;
    for (const TransformDB::FQN &fqn : imports) {
      out << "import " << fqn.fullName() << ';' << ::std::endl;
    }
    for (const auto &e : calls) {
      out << ::std::endl;
      const TransformDB::FQN &fqn = e.first;
      const Call &call = e.second;

      auto chain = chains[call.getCall()];

      auto parent = fqn.parent();
      ::std::string indent;
      if (parent) {
        out << "unit " << parent->fullName() << " {" << ::std::endl;
        indent = "   ";
      }
      ::std::map<mylang::transforms::Type, ::std::string> args;

      out << indent << "defun export " << fqn.localName() << "(";
      ::std::string sep;
      for (auto &t : call.argTypes) {
        out << sep;
        sep = ", ";
        ::std::string arg("arg" + ::std::to_string(args.size()));
        out << arg << ":" << t.getName();
        args.emplace(t, ::std::move(arg));
      }

      out << "):" << call.output.getName();
      out << " = " << ::std::endl;
      ::std::string bodyIndent(indent + "   ");
#if 0
      out << indent << bodyIndent << "transform<" << call.output.getName() << ">(";
      sep = "";
      for (auto &t : call.input) {
        out << sep << args[t];
        sep = ", ";
      }
      out << ')';
#else
      size_t i = 0;
      emitChain(i, bodyIndent, args, chain, out);
#endif
      out << ';' << ::std::endl;
      if (parent) {
        out << '}' << ::std::endl;
      }

    }
    if (error) {
      return 1;
    }

    // compile the code to ethir
    mylang::api::Parser::Options opts;
    opts.transforms = db;
    try {
      auto parser = mylang::api::Parser::create(opts);
      ::std::istringstream sin(out.str());
      auto dom = parser->parse(sin, { });
      if (!dom) {
        ::std::cerr << "Failed to parse generated code" << ::std::endl;
        return 1;
      }

      if (!compile) {
        ::std::cout << out.str();
        return 0;
      }
      ::std::cerr << out.str();

      auto ethir = mylang::ethir::vast2ethir(dom);
      if (!ethir) {
        ::std::cerr << "Internal error: failed to convert to ethir" << ::std::endl;
      }
      mylang::ethir::prettyPrint(ethir, ::std::cout);

      return 0;
    } catch (const ::std::exception &e) {
      ::std::cerr << "Unexpected error:" << e.what() << ::std::endl;
      return 1;
    }
  }

  bool compile;
  ::std::map<TransformDB::FQN, Call> calls;
};

struct InitCommand: public Command
{

  InitCommand(const ::std::filesystem::path &xdbFile, const ::std::filesystem::path &xsrcDir)
      : Command(), dbFile(xdbFile), srcDir(xsrcDir)
  {
  }

  ~InitCommand()
  {
  }

  int run()
  {

    using namespace mylang::transforms::base;
    auto db = TransformDB::init(dbFile, srcDir);
    if (!db) {
      return 1;
    }
    for (const auto &e : db->transforms()) {
      ::std::cerr << e.first << ::std::endl;
    }
    return 0;
  }

  /** The transform file */
  ::std::filesystem::path dbFile;

  /** The source directory */
  ::std::filesystem::path srcDir;

};

static ::std::unique_ptr<Command> parseInit(::std::vector<::std::string> args,
    ::std::filesystem::path dbFile)
{
  ::std::filesystem::path srcDir(".");
  if (args.size() == 1) {
    srcDir = args.at(0);
  } else if (args.size() > 1) {
    ::std::cerr << "Unexpected number of arguments" << ::std::endl;
    return nullptr;
  }

  return ::std::make_unique<InitCommand>(dbFile, srcDir);
}

static ::std::unique_ptr<ListCommand> parseList(::std::vector<::std::string> args, DatabasePtr db)
{
  ::std::set<TransformDB::FQN> prefixes;
  for (const auto &str : args) {
    prefixes.insert(toFQN(str));
  }
  return ::std::make_unique<ListCommand>(db, ::std::move(prefixes));
}

static ::std::unique_ptr<Command> parseSync(::std::vector<::std::string> args, DatabasePtr db)
{
  ::std::set<TransformDB::FQN> prefixes;
  for (const auto &str : args) {
    prefixes.insert(toFQN(str));
  }
  return ::std::make_unique<SyncCommand>(db, ::std::move(prefixes));
}

static ::std::unique_ptr<Command> parseStatus(::std::vector<::std::string> args, DatabasePtr db)
{
  ::std::set<TransformDB::FQN> prefixes;
  for (const auto &str : args) {
    prefixes.insert(toFQN(str));
  }
  return ::std::make_unique<StatusCommand>(db, ::std::move(prefixes));
}

static ::std::unique_ptr<Command> parseCat(::std::vector<::std::string> args, DatabasePtr db)
{
  if (args.size() != 1) {
    ::std::cerr << "cat: Unexpected number of arguments " << args.size() << ::std::endl;
    return nullptr;
  }
  auto fqn = TransformDB::FQN::parseFQN(args.at(0));
  if (!fqn) {
    ::std::cerr << "cat: invalid fqn " << args.at(0) << ::std::endl;
    return nullptr;
  }
  return ::std::make_unique<CatCommand>(db, *fqn);
}

static ::std::unique_ptr<Command> parseFind(::std::vector<::std::string> args, DatabasePtr db)
{
  if (args.size() != 1) {
    ::std::cerr << "find: Unexpected number of arguments " << args.size() << ::std::endl;
    return nullptr;
  }
  auto call = parseCall(args.at(0));
  return ::std::make_unique<FindCommand>(db, call);
}

static ::std::unique_ptr<Command> parseGen(::std::vector<::std::string> args, DatabasePtr db)
{
  if (args.empty()) {
    ::std::cerr << "gen: missing arguments" << ::std::endl;
    return nullptr;
  }
  ::std::map<TransformDB::FQN, Call> calls;
  for (const ::std::string &arg : args) {
    auto i = arg.find_first_of('=');
    if (i == ::std::string::npos) {
      ::std::cerr << "Not a valid argument " << arg << ::std::endl;
      return nullptr;
    }
    auto fqn = toFQN(arg.substr(0, i));
    auto call = parseCall(arg.substr(i + 1));
    calls.emplace(fqn, call);
  }
  return ::std::make_unique<GenCommand>(db, ::std::move(calls));
}

static ::std::unique_ptr<Command> parseArgs(int argc, const char **argv)
{
  ::std::vector<::std::string> args;
  for (int i = 1; i < argc; ++i) {
    args.emplace_back(argv[i]);
  }

  ::std::filesystem::path db(".db"); // TODO: fix this properly
  TransformDB::Configuration cfg;

  for (auto i = args.begin(); i != args.end();) {
    ::std::string arg(*i);
    if (arg == "--") {
      break;
    } else if (startsWith("--database=", arg)) {
      i = args.erase(i);
      db = arg;
    } else if (arg == "--database") {
      i = args.erase(i);
      db = *i;
      i = args.erase(i);
    } else if (arg == "--sync" || arg == "-s") {
      i = args.erase(i);
      cfg.sync = true;
    } else if (arg == "--verbose" || arg == "-v") {
      i = args.erase(i);
      cfg.verbose = true;
    } else if (startsWith("-", arg)) {
      ::std::cerr << "Unknown option " << arg << ::std::endl;
      return nullptr;
    } else {
      ++i;
    }
  }
  if (args.empty()) {
    ::std::cerr << "Missing command" << ::std::endl;
    return nullptr;
  }

  auto cmd = args[0];
  args.erase(args.begin());
  if (cmd == "init") {
    return parseInit(args, db);
  }
  // any other commands need a database
  auto dbPtr = ::mylang::transforms::base::TransformDB::open(db, cfg);
  if (!dbPtr) {
    return nullptr;
  }
  if (cmd == "list") {
    return parseList(args, dbPtr);
  }
  if (cmd == "sync") {
    return parseSync(args, dbPtr);
  }
  if (cmd == "status") {
    return parseStatus(args, dbPtr);
  }
  if (cmd == "cat") {
    return parseCat(args, dbPtr);
  }
  if (cmd == "find") {
    return parseFind(args, dbPtr);
  }
  if (cmd == "gen") {
    return parseGen(args, dbPtr);
  }
  ::std::cerr << "Unknown command " << cmd << ::std::endl;
  return nullptr;

}

int main(int argc, const char **argv)
{
  ::std::shared_ptr<Command> cmd;
  try {
    cmd = parseArgs(argc, argv);
    if (cmd && cmd->run() == 0) {
      return 0;
    }
  } catch (const ::std::exception &e) {
    ::std::cerr << e.what() << ::std::endl;
  }
  return 1;
}

