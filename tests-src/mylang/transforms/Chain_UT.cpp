#include <mylang/transforms/Call.h>
#include <mylang/transforms/Transform.h>
#include <mylang/transforms/Chain.h>
#include <mylang/transforms/SimpleTxDatabase.h>
#include <cassert>
#include <memory>
#include <iostream>

using namespace mylang::transforms;

static Transform::Ptr makeTransform(const ::std::string &name, unsigned short cost, const Type &out,
    ::std::set<Type> inputs, ::std::set<Call> calls)
{
  return Transform::create(name, Call(out, ::std::move(inputs)), ::std::move(calls),
      Transform::Cost(cost));
}
void utest_create_simple_chain()
{
  Type out("out"), in("in");

  SimpleTxDatabase db;
  db.addTransform(makeTransform("tx1", 10, out, { in }, { }));
  db.addTransform(makeTransform("tx2", 11, out, { in }, { }));

  auto G = db.findChain(Call(out, in), TransformBase::QUALITY_BEST);
  assert(G && "Expected a chain");
  ::std::cerr << __FUNCTION__ << ": " << G->getCost() << ": " << G->toString() << ::std::endl;
  assert(G->getCost() == Transform::Cost(10) && "Cost should have been 10");
}

void utest_with_nested()
{
  Type out("out"), in("in");
  Type w("w"), x("x"), y("y"), z("z");
  Type yx("yx");

  SimpleTxDatabase db;
  db.addTransform(makeTransform("tx1", 1, out, { in }, { Call(w, { x, y, z }) }));
  db.addTransform(makeTransform("tx2", 1000, out, { in }, { }));
  db.addTransform(makeTransform("yx_z_2_w", 1, w, { yx, z }, { }));
  db.addTransform(makeTransform("y_x_2_yx", 1, yx, { y, x }, { }));
  db.addTransform(makeTransform("x_y_z_2_w", 10, w, { x, y, z }, { }));

  {
    auto Gany = db.findChain(Call(out, in), TransformBase::QUALITY_ANY);
    assert(Gany && "Expected a chain");
    ::std::cerr << __FUNCTION__ << " ANY : " << Gany->getCost() << " : " << Gany->toString()
        << ::std::endl;
  }

  {
    auto Gbest = db.findChain(Call(out, in), TransformBase::QUALITY_BEST);
    assert(Gbest && "Expected a chain");
    ::std::cerr << __FUNCTION__ << " BEST: " << Gbest->getCost() << " : " << Gbest->toString()
        << ::std::endl;
    assert(Gbest->getCost() == Transform::Cost(3) && "Cost should have been 3");
  }
}

void utest_partial_transformations()
{
  Type out("out"), mid("mid"), in("in");

  SimpleTxDatabase db;
  db.addTransform(makeTransform("tx1", 1, out, { mid }, { }));
  db.addTransform(makeTransform("tx2", 1, mid.asOptional(), { in }, { }));

#if 0
  {
    auto Gany = db.findChain(Call(out, in),TransformBase::QUALITY_ANY);
    if (Gany) {
      ::std::cerr << __FUNCTION__ << " ANY : " << Gany->getCost() << " : " << Gany->toString()
          << ::std::endl;
    }
    assert(!Gany && "Unexpected chain found");
  }
#endif

  {
    auto Gany = db.findChain(Call(out.asOptional(), in), TransformBase::QUALITY_ANY);
    assert(Gany && "Expected a chain");
    ::std::cerr << __FUNCTION__ << " ANY : " << Gany->getCost() << " : " << Gany->toString()
        << ::std::endl;
  }
}
