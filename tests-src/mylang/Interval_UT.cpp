#include <mylang/Interval.h>
#include <iostream>
#include <cassert>

using namespace mylang;

static void expectUndefined(const ::std::string &key, size_t line,
    const ::std::optional<Interval> &actual)
{
  ::std::cerr << key << ':' << line << ":expected undefined value";
  if (actual.has_value()) {
    ::std::cerr << ", but found " << *actual << ::std::endl;
    assert(false);
  }
  ::std::cerr << ::std::endl;
}

static void expect(const ::std::string &key, size_t line, const Interval &expected,
    const Interval &actual)
{
  ::std::cerr << key << ':' << line << ":expected " << expected;
  if (expected != actual) {
    ::std::cerr << ", but found " << actual << ::std::endl;
    assert(false);
  }
  ::std::cerr << ::std::endl;
}

static void expect(const ::std::string &key, size_t line, const Interval &expected,
    const ::std::optional<Interval> &actual)
{
  if (actual.has_value()) {
    expect(key, line, expected, *actual);
  } else {
    ::std::cerr << key << ':' << line << ":expected " << expected << ", but found nothing"
        << ::std::endl;
    assert(false);
  }
}

void utest_create_interval()
{
  try {
    Interval i(1, -1);
    assert(false);
  } catch (const ::std::invalid_argument&) {
  }
  try {
    Interval i(Integer::MINUS_INFINITY(), Integer::MINUS_INFINITY());
    assert(false);
  } catch (const ::std::invalid_argument&) {
  }
  try {
    Interval i(Integer::PLUS_INFINITY(), Integer::PLUS_INFINITY());
    assert(false);
  } catch (const ::std::invalid_argument&) {
  }
  {
    Interval i(-1, 1);
  }
  expect(__FUNCTION__, __LINE__, Interval(Integer::MINUS_INFINITY(), Integer::PLUS_INFINITY()),
      Interval(std::nullopt, std::nullopt));
  expect(__FUNCTION__, __LINE__, Interval(Integer::MINUS_INFINITY(), Integer(1)),
      Interval(std::nullopt, BigInt(1)));
  expect(__FUNCTION__, __LINE__, Interval(Integer(-1), Integer::PLUS_INFINITY()),
      Interval(BigInt(-1), std::nullopt));
}

void utest_abs_interval()
{
  expect(__FUNCTION__, __LINE__, Interval(0, Integer::PLUS_INFINITY()),
      Interval(Integer::MINUS_INFINITY(), Integer::PLUS_INFINITY()).abs());
  expect(__FUNCTION__, __LINE__, Interval(0, 2), Interval(-2, 1).abs());
  expect(__FUNCTION__, __LINE__, Interval(1, 2), Interval(-2, -1).abs());
  expect(__FUNCTION__, __LINE__, Interval(1, 2), Interval(1, 2).abs());
}

void utest_negate_interval()
{
  expect(__FUNCTION__, __LINE__, Interval(Integer::MINUS_INFINITY(), Integer::PLUS_INFINITY()),
      Interval(Integer::MINUS_INFINITY(), Integer::PLUS_INFINITY()).negate());
  expect(__FUNCTION__, __LINE__, Interval(0, Integer::PLUS_INFINITY()),
      Interval(Integer::MINUS_INFINITY(), 0).negate());
  expect(__FUNCTION__, __LINE__, Interval(0, Integer::PLUS_INFINITY()),
      Interval(Integer::MINUS_INFINITY(), 0).negate());
}

void utest_add_interval()
{
  expect(__FUNCTION__, __LINE__, Interval(Integer::MINUS_INFINITY(), Integer::PLUS_INFINITY()),
      Interval(Integer::MINUS_INFINITY(), Integer::PLUS_INFINITY()).add(
          Interval(Integer::MINUS_INFINITY(), Integer::PLUS_INFINITY())));
  expect(__FUNCTION__, __LINE__, Interval(-1, 1), Interval(-2, -1).add(Interval(1, 2)));
}

void utest_multiply_interval()
{
  auto r1 = Interval(0, ::std::nullopt);
  auto r2 = Interval(8);
  auto r3 = Interval(::std::nullopt, ::std::nullopt);
  auto r4 = Interval(::std::nullopt, 2);

  auto m1_1 = r1.multiply(r1);
  expect(__FUNCTION__, __LINE__, r1, m1_1);

  auto m1_2 = r1.multiply(r2);
  expect(__FUNCTION__, __LINE__, r1, m1_2);
  auto m1_3 = r1.multiply(r3);
  expect(__FUNCTION__, __LINE__, r3, m1_3);
  auto m1_4 = r1.multiply(r4);
  expect(__FUNCTION__, __LINE__, r3, m1_4);

  auto m2_2 = r2.multiply(r2);
  expect(__FUNCTION__, __LINE__, Interval(64, 64), m2_2);
  auto m2_3 = r2.multiply(r3);
  expect(__FUNCTION__, __LINE__, r3, m2_3);
  auto m2_4 = r2.multiply(r4);
  expect(__FUNCTION__, __LINE__, Interval(::std::nullopt, 16), m2_4);

  auto m3_3 = r3.multiply(r3);
  expect(__FUNCTION__, __LINE__, r3, m3_3);
  auto m3_4 = r3.multiply(r4);
  expect(__FUNCTION__, __LINE__, r3, m3_4);

  auto m4_4 = r4.multiply(r4);
  expect(__FUNCTION__, __LINE__, r3, m4_4);
}

void utest_divide_interval()
{
  expectUndefined(__FUNCTION__, __LINE__, Interval().divide(0));

  {
    auto z = Interval(2, 2).divide(Interval(0, ::std::nullopt));
    expect(__FUNCTION__, __LINE__, Interval(0, 2), z);
  }

  {
    auto d0 = Interval(::std::nullopt, std::nullopt).divide(
        Interval(::std::nullopt, ::std::nullopt));
    expect(__FUNCTION__, __LINE__, Interval(::std::nullopt, ::std::nullopt), d0);
  }

  {
    auto x = Interval(-10, -10).divide(Interval(-3, -3));
    expect(__FUNCTION__, __LINE__, Interval(3, 3), x);
  }

  {
    auto x = Interval(-10, -10).divide(Interval(-3, -2));
    expect(__FUNCTION__, __LINE__, Interval(3, 5), x);
  }

  {
    auto x = Interval(-12, -7).divide(Interval(-3, -3));
    expect(__FUNCTION__, __LINE__, Interval(2, 4), x);
  }

  {
    auto x = Interval(-10, 9).divide(Interval(-3, 2));
    expect(__FUNCTION__, __LINE__, Interval(-10, 10), x);
  }

  {
    auto y = Interval(-10, 9).divide(Interval(-3, 0));
    expect(__FUNCTION__, __LINE__, Interval(-9, 10), y);
  }

  {
    auto z = Interval(-10, 9).divide(Interval(0, 3));
    expect(__FUNCTION__, __LINE__, Interval(-10, 9), z);
  }

  {
    int num = 8;
    ::std::cerr << "BLOCK #1" << ::std::endl;

    auto d1 = Interval(num, std::nullopt).divide(Interval(::std::nullopt, ::std::nullopt));
    expect(__FUNCTION__, __LINE__, Interval(::std::nullopt, ::std::nullopt), d1);

    auto d2 = Interval(num, num).divide(Interval(::std::nullopt, ::std::nullopt));
    expect(__FUNCTION__, __LINE__, Interval(-num, num), d2);

    auto d3 = Interval(::std::nullopt, num).divide(Interval(::std::nullopt, ::std::nullopt));
    expect(__FUNCTION__, __LINE__, Interval(::std::nullopt, ::std::nullopt), d3);

    auto d4 = Interval(::std::nullopt, std::nullopt).divide(Interval(num, ::std::nullopt));
    expect(__FUNCTION__, __LINE__, Interval(::std::nullopt, ::std::nullopt), d4);

    auto d5 = Interval(num, std::nullopt).divide(Interval(num, ::std::nullopt));
    expect(__FUNCTION__, __LINE__, Interval(0, ::std::nullopt), d5);

    auto d6 = Interval(num, num).divide(Interval(num, ::std::nullopt));
    expect(__FUNCTION__, __LINE__, Interval(0, 1), d6);

    auto d7 = Interval(::std::nullopt, num).divide(Interval(num, ::std::nullopt));
    expect(__FUNCTION__, __LINE__, Interval(::std::nullopt, 1), d7);

    auto d8 = Interval(::std::nullopt, std::nullopt).divide(Interval(::std::nullopt, num));
    expect(__FUNCTION__, __LINE__, Interval(::std::nullopt, ::std::nullopt), d8);

    auto d9 = Interval(num, std::nullopt).divide(Interval(::std::nullopt, num));
    expect(__FUNCTION__, __LINE__, Interval(std::nullopt, ::std::nullopt), d9);

    auto dA = Interval(num, num).divide(Interval(::std::nullopt, num));
    expect(__FUNCTION__, __LINE__, Interval(-num, num), dA);

    auto dB = Interval(::std::nullopt, num).divide(Interval(::std::nullopt, num));
    expect(__FUNCTION__, __LINE__, Interval(::std::nullopt, ::std::nullopt), dB);

    auto dC = Interval(::std::nullopt, std::nullopt).divide(Interval(num, num));
    expect(__FUNCTION__, __LINE__, Interval(::std::nullopt, ::std::nullopt), dC);

    auto dD = Interval(num, std::nullopt).divide(Interval(num, num));
    expect(__FUNCTION__, __LINE__, Interval(1, ::std::nullopt), dD);

    auto dE = Interval(num, num).divide(Interval(num, num));
    expect(__FUNCTION__, __LINE__, Interval(1, 1), dE);

    auto dF = Interval(::std::nullopt, num).divide(Interval(num, num));
    expect(__FUNCTION__, __LINE__, Interval(::std::nullopt, 1), dF);
  }

  {
    int num = -8;
    ::std::cerr << "Block #2" << ::std::endl;
    auto d1 = Interval(num, std::nullopt).divide(Interval(::std::nullopt, ::std::nullopt));
    expect(__FUNCTION__, __LINE__, Interval(::std::nullopt, ::std::nullopt), d1);

    auto d2 = Interval(num, num).divide(Interval(::std::nullopt, ::std::nullopt));
    expect(__FUNCTION__, __LINE__, Interval(num, -num), d2);

    auto d3 = Interval(::std::nullopt, num).divide(Interval(::std::nullopt, ::std::nullopt));
    expect(__FUNCTION__, __LINE__, Interval(::std::nullopt, ::std::nullopt), d3);

    auto d4 = Interval(::std::nullopt, std::nullopt).divide(Interval(num, ::std::nullopt));
    expect(__FUNCTION__, __LINE__, Interval(::std::nullopt, ::std::nullopt), d4);

    auto d5 = Interval(num, std::nullopt).divide(Interval(num, ::std::nullopt));
    expect(__FUNCTION__, __LINE__, Interval(::std::nullopt, ::std::nullopt), d5);

    auto d6 = Interval(num, num).divide(Interval(num, ::std::nullopt));
    expect(__FUNCTION__, __LINE__, Interval(num, -num), d6);

    auto d7 = Interval(::std::nullopt, num).divide(Interval(num, ::std::nullopt));
    expect(__FUNCTION__, __LINE__, Interval(::std::nullopt, ::std::nullopt), d7);

    auto d8 = Interval(::std::nullopt, std::nullopt).divide(Interval(::std::nullopt, num));
    expect(__FUNCTION__, __LINE__, Interval(::std::nullopt, ::std::nullopt), d8);

    auto d9 = Interval(num, std::nullopt).divide(Interval(::std::nullopt, num));
    expect(__FUNCTION__, __LINE__, Interval(std::nullopt, 1), d9);

    auto dA = Interval(num, num).divide(Interval(::std::nullopt, num));
    expect(__FUNCTION__, __LINE__, Interval(0, 1), dA);

    auto dB = Interval(::std::nullopt, num).divide(Interval(::std::nullopt, num));
    expect(__FUNCTION__, __LINE__, Interval(0, ::std::nullopt), dB);

    auto dC = Interval(::std::nullopt, std::nullopt).divide(Interval(num, num));
    expect(__FUNCTION__, __LINE__, Interval(::std::nullopt, ::std::nullopt), dC);

    auto dD = Interval(num, std::nullopt).divide(Interval(num, num));
    expect(__FUNCTION__, __LINE__, Interval(::std::nullopt, 1), dD);

    auto dE = Interval(num, num).divide(Interval(num, num));
    expect(__FUNCTION__, __LINE__, Interval(1, 1), dE);

    auto dF = Interval(::std::nullopt, num).divide(Interval(num, num));
    expect(__FUNCTION__, __LINE__, Interval(1, ::std::nullopt), dF);
  }
}

void utest_remainder_interval()
{
  try {
    expectUndefined(__FUNCTION__, __LINE__, Interval::INFINITE().remainder(Integer::ZERO()));

    // remainder involving infinities in the numerator
    ::std::cerr << "INFINITE() numerator" << ::std::endl;

    // INFINITE() % INFINITE()
    expect(__FUNCTION__, __LINE__, Interval::INFINITE(),
        Interval::INFINITE().remainder(Interval::INFINITE()));

    // INFINITE() % positive
    expect(__FUNCTION__, __LINE__, Interval::INFINITE(),
        Interval::INFINITE().remainder(Interval::NON_NEGATIVE()));
    expect(__FUNCTION__, __LINE__, Interval::INFINITE(),
        Interval::INFINITE().remainder(Interval::POSITIVE()));

    // INFINITE() % negative
    expect(__FUNCTION__, __LINE__, Interval::INFINITE(),
        Interval::INFINITE().remainder(Interval::NON_POSITIVE()));
    expect(__FUNCTION__, __LINE__, Interval::INFINITE(),
        Interval::INFINITE().remainder(Interval::NEGATIVE()));

    // INFINITE() % finite
    expect(__FUNCTION__, __LINE__, Interval { -9, 9 }, Interval::INFINITE().remainder( { 2, 10 }));

    // remainder involving infinities in the denominator
    ::std::cerr << "INFINITE() denominator" << ::std::endl;

    // INFINITE() % INFINITE() -> already done

    // positive % INFINITE()
    expect(__FUNCTION__, __LINE__, Interval::NON_NEGATIVE(),
        Interval::NON_NEGATIVE().remainder(Interval::INFINITE()));
    expect(__FUNCTION__, __LINE__, Interval::NON_NEGATIVE(),
        Interval::POSITIVE().remainder(Interval::INFINITE()));

    // negative % INFINITE()
    expect(__FUNCTION__, __LINE__, Interval::NON_POSITIVE(),
        Interval::NON_POSITIVE().remainder(Interval::INFINITE()));
    expect(__FUNCTION__, __LINE__, Interval::NON_POSITIVE(),
        Interval::NEGATIVE().remainder(Interval::INFINITE()));

    // finite % INFINITE()
    expect(__FUNCTION__, __LINE__, Interval { -20, 10 },
        Interval(-20, 10).remainder(Interval::INFINITE()));

    expect(__FUNCTION__, __LINE__, Interval(0, 3), Interval(2, 10).remainder(Interval(3, 4)));
    expect(__FUNCTION__, __LINE__, Interval(0, 3), Interval(2, 10).remainder(Interval(-4, -3)));
    expect(__FUNCTION__, __LINE__, Interval(0, 3), Interval(2, 10).remainder(Interval(-3, 4)));

    expect(__FUNCTION__, __LINE__, Interval(-3, 0), Interval(-10, -2).remainder(Interval(3, 4)));
    expect(__FUNCTION__, __LINE__, Interval(-3, 0), Interval(-10, -2).remainder(Interval(-4, -3)));
    expect(__FUNCTION__, __LINE__, Interval(-3, 0), Interval(-10, -2).remainder(Interval(-3, 4)));

    expect(__FUNCTION__, __LINE__, Interval(-3, 2), Interval(-10, 2).remainder(Interval(3, 4)));
    // sign of the denominator doesn't matter:
    // rem(x,y) = x - (x/y)*y  and the sign of y cancels out
    expect(__FUNCTION__, __LINE__, Interval(-3, -3), Interval(-7, -7).remainder(Interval(4, 4)));
    expect(__FUNCTION__, __LINE__, Interval(-3, -3), Interval(-7, -7).remainder(Interval(-4, -4)));
    expect(__FUNCTION__, __LINE__, Interval(0, 3), Interval(3, 4).remainder(Interval(-4, -4)));
    // imprecise
    expect(__FUNCTION__, __LINE__, Interval(0, 3), Interval(4, 4).remainder(Interval(-3, 4)));

    expect(__FUNCTION__, __LINE__, Interval(4, 4), Interval(4, 4).remainder(Interval(5, 10)));
    expect(__FUNCTION__, __LINE__, Interval(0, 4), Interval(4, 4).remainder(Interval(3, 10)));
  } catch (const ::std::exception &e) {
    ::std::cerr << e.what() << ::std::endl;
    exit(1);
  }
}

void utest_mod_interval()
{
  expectUndefined(__FUNCTION__, __LINE__, Interval().mod(0));
  expectUndefined(__FUNCTION__, __LINE__, Interval().mod(Interval(-4, -1)));
  expectUndefined(__FUNCTION__, __LINE__, Interval(-4, -1).mod(Interval(1, 3)));
  expect(__FUNCTION__, __LINE__, Interval(0, 4), Interval(0, 10).remainder(Interval(1, 5)));
}

void utest_pow_interval()
{
  expect(__FUNCTION__, __LINE__, Interval(1), Interval::INFINITE().pow(0));
  expect(__FUNCTION__, __LINE__, Interval(1), Interval::ZERO().pow(0));
  expect(__FUNCTION__, __LINE__, Interval(0, 256), Interval(-2, 4).pow(4));
  expect(__FUNCTION__, __LINE__, Interval(16, 256), Interval(2, 4).pow(4));
  expect(__FUNCTION__, __LINE__, Interval(16, 256), Interval(-4, -2).pow(4));
  expect(__FUNCTION__, __LINE__, Interval(Integer::MINUS_INFINITY(), -8),
      Interval(Integer::MINUS_INFINITY(), -2).pow(3));
  expect(__FUNCTION__, __LINE__, Interval(-8, Integer::PLUS_INFINITY()),
      Interval(-2, Integer::PLUS_INFINITY()).pow(3));
  expect(__FUNCTION__, __LINE__, Interval(-125, -8), Interval(-5, -2).pow(3));

}
