#include <mylang/constraints/ConstrainedUnionType.h>
#include <mylang/constraints/ConstrainedBooleanType.h>
#include <mylang/constraints/ConstrainedVoidType.h>
#include <mylang/expressions/Constant.h>

using namespace mylang::constraints;
using mylang::expressions::Constant;

void utest_void_member()
{
  auto boolTy = ConstrainedBooleanType::create();
  ConstrainedUnionType::Discriminant d("d", boolTy);
  ::std::vector<ConstrainedUnionType::Member> M;
  M.push_back(ConstrainedUnionType::Member(Constant::get(boolTy->type(), "true"), "just", boolTy));
  M.push_back(
      ConstrainedUnionType::Member(Constant::get(boolTy->type(), "false"), "nothing",
          ConstrainedVoidType::create()));

  auto c = ConstrainedUnionType::get(d, M);
}
