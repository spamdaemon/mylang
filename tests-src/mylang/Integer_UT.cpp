#include <mylang/Integer.h>
#include <iostream>
#include <cassert>
#include <optional>

using namespace mylang;

static void expectUndefined(const ::std::string &key, const ::std::optional<Integer> &actual)
{
  if (actual.has_value()) {
    ::std::cerr << key << ":expected undefined value, but found " << *actual << ::std::endl;
    assert(false);
  }
}

static void expect(const ::std::string &key, const Integer &expected, const Integer &actual)
{
  if (expected != actual) {
    ::std::cerr << key << ":expected " << expected << ", but found " << actual << ::std::endl;
    assert(false);
  }
}

static void expect(const ::std::string &key, const Integer &expected,
    const ::std::optional<Integer> &actual)
{
  if (!actual.has_value()) {
    ::std::cerr << key << ":expected " << expected << ", but found nothing" << ::std::endl;
    assert(false);
  } else {
    expect(key, expected, *actual);
  }
}

void utest_should_not_create_invalid_integer()
{
  try {
    Integer i(::std::nullopt, 0);
    assert(false);
  } catch (const ::std::invalid_argument&) {
    // expected
  }

  try {
    Integer i(::std::optional<BigInt>(1), 0);
    assert(false);
  } catch (const ::std::invalid_argument&) {
    // expected
  }
}

void utest_negate_integer()
{
  expect(__FUNCTION__, Integer::PLUS_INFINITY(), Integer::MINUS_INFINITY().negate());
  expect(__FUNCTION__, Integer::MINUS_INFINITY(), Integer::PLUS_INFINITY().negate());
  expect(__FUNCTION__, Integer(0), Integer(0).negate());
  expect(__FUNCTION__, Integer(1), Integer(-1).negate());
  expect(__FUNCTION__, Integer(-1), Integer(1).negate());
}

void utest_add_integer()
{
  expectUndefined(__FUNCTION__, Integer::MINUS_INFINITY() + Integer::PLUS_INFINITY());
  expectUndefined(__FUNCTION__, Integer::PLUS_INFINITY() - Integer::PLUS_INFINITY());
  expect(__FUNCTION__, Integer::PLUS_INFINITY(), Integer::PLUS_INFINITY() + Integer(-1));
  expect(__FUNCTION__, Integer::MINUS_INFINITY(), Integer::MINUS_INFINITY() + Integer(-1));
  expect(__FUNCTION__, Integer(2), Integer(1) + Integer(1));
  expect(__FUNCTION__, Integer(-1), Integer(1) - Integer(2));
}

void utest_multiply_integer()
{
  expect(__FUNCTION__, Integer::MINUS_INFINITY(),
      Integer::MINUS_INFINITY() * Integer::PLUS_INFINITY());
  expect(__FUNCTION__, Integer(0), Integer(0) * Integer::PLUS_INFINITY());
  expect(__FUNCTION__, Integer(0), Integer(0) * Integer::MINUS_INFINITY());
  expect(__FUNCTION__, Integer::PLUS_INFINITY(), Integer(-1) * Integer::MINUS_INFINITY());
  expect(__FUNCTION__, Integer::MINUS_INFINITY(), Integer(1) * Integer::MINUS_INFINITY());
  expect(__FUNCTION__, Integer::MINUS_INFINITY(), Integer(-1) * Integer::PLUS_INFINITY());
  expect(__FUNCTION__, Integer::PLUS_INFINITY(), Integer(1) * Integer::PLUS_INFINITY());
  expect(__FUNCTION__, Integer(-190), Integer(-10) * Integer(19));
}

void utest_divide_integer()
{
  expectUndefined(__FUNCTION__, Integer::MINUS_INFINITY() / Integer::PLUS_INFINITY());
  expectUndefined(__FUNCTION__, Integer::PLUS_INFINITY() / Integer::PLUS_INFINITY());
  expectUndefined(__FUNCTION__, Integer::MINUS_INFINITY() / Integer::MINUS_INFINITY());
  expectUndefined(__FUNCTION__, Integer::PLUS_INFINITY() / Integer(0));
  expect(__FUNCTION__, Integer(0), Integer(1000000) / Integer::PLUS_INFINITY());
  expect(__FUNCTION__, Integer(0), Integer(1000000) / Integer::MINUS_INFINITY());
  expect(__FUNCTION__, Integer::PLUS_INFINITY(), Integer::PLUS_INFINITY() / Integer(10));
  expect(__FUNCTION__, Integer::MINUS_INFINITY(), Integer::PLUS_INFINITY() / Integer(-10));
  expect(__FUNCTION__, Integer(33), Integer(100) / Integer(3));
  expect(__FUNCTION__, Integer(0), Integer(-3) / Integer(4));
  expect(__FUNCTION__, Integer(0), Integer(3) / Integer(-4));
}

void utest_remainder_integer()
{
  expectUndefined(__FUNCTION__, Integer::MINUS_INFINITY().remainder(Integer::PLUS_INFINITY()));
  expectUndefined(__FUNCTION__, Integer::PLUS_INFINITY().remainder(Integer::PLUS_INFINITY()));
  expectUndefined(__FUNCTION__, Integer::MINUS_INFINITY().remainder(Integer::MINUS_INFINITY()));
  expectUndefined(__FUNCTION__, Integer::PLUS_INFINITY().remainder(Integer(0)));
  expectUndefined(__FUNCTION__, Integer::PLUS_INFINITY().remainder(Integer(10)));
  expectUndefined(__FUNCTION__, Integer::PLUS_INFINITY().remainder(Integer(-10)));
  expect(__FUNCTION__, Integer(-999999), Integer(-1000000).remainder(Integer::PLUS_INFINITY()));
  expect(__FUNCTION__, Integer(999999), Integer(1000000).remainder(Integer::MINUS_INFINITY()));
  expect(__FUNCTION__, Integer(1), Integer(100).remainder(Integer(3)));
  expect(__FUNCTION__, Integer(-1), Integer(-100).remainder(Integer(3)));
  expect(__FUNCTION__, Integer(1), Integer(100).remainder(Integer(-3)));
  expect(__FUNCTION__, Integer(0), Integer(0).remainder(Integer(-3)));
  expect(__FUNCTION__, Integer(0), Integer(0).remainder(Integer::MINUS_INFINITY()));
}

void utest_mod_integer()
{
  expectUndefined(__FUNCTION__, Integer::PLUS_INFINITY().mod(Integer::PLUS_INFINITY()));
  expectUndefined(__FUNCTION__, Integer::PLUS_INFINITY().mod(Integer(0)));
  expectUndefined(__FUNCTION__, Integer::PLUS_INFINITY().mod(Integer(10)));
  expectUndefined("Operands to mod must be positive", Integer(-100).mod(Integer(3)));
  expectUndefined("Operands to mod must be positive", Integer(100).mod(Integer(-3)));

  expect(__FUNCTION__, Integer(1000000), Integer(1000000).mod(Integer::PLUS_INFINITY()));
  expect(__FUNCTION__, Integer(1), Integer(100).mod(Integer(3)));
  expect(__FUNCTION__, Integer(0), Integer(0).mod(Integer(3)));
}

void utest_pow_integer()
{
  expect(__FUNCTION__, Integer::PLUS_INFINITY(), Integer::PLUS_INFINITY().pow(3));
  expect(__FUNCTION__, Integer::PLUS_INFINITY(), Integer::MINUS_INFINITY().pow(6));
  expect(__FUNCTION__, Integer::MINUS_INFINITY(), Integer::MINUS_INFINITY().pow(3));
  expect(__FUNCTION__, Integer(8), Integer(2).pow(3));
  expect(__FUNCTION__, Integer(1), Integer::PLUS_INFINITY().pow(0));
  expect(__FUNCTION__, Integer(1), Integer::MINUS_INFINITY().pow(0));
  expect(__FUNCTION__, Integer(1), Integer(1000).pow(0));
  expect(__FUNCTION__, Integer(1), Integer(0).pow(0));
  expect(__FUNCTION__, Integer(0), Integer(0).pow(1));
}

void utest_sign()
{
  assert(0 == Integer(0).sign());
  assert(1 == Integer(1).sign());
  assert(-1 == Integer(-1).sign());
  assert(1 == Integer::PLUS_INFINITY().sign());
  assert(-1 == Integer::MINUS_INFINITY().sign());
}
void utest_compare_integer()
{
  assert(0 == Integer::MINUS_INFINITY().compare(Integer::MINUS_INFINITY()));
  assert(-1 == Integer::MINUS_INFINITY().compare(Integer(-1)));
  assert(0 == Integer::PLUS_INFINITY().compare(Integer::PLUS_INFINITY()));
  assert(1 == Integer::PLUS_INFINITY().compare(Integer(1)));
  assert(1 == Integer::PLUS_INFINITY().compare(Integer::MINUS_INFINITY()));
  assert(1 == Integer(2).compare(Integer(-2)));
  assert(Integer::MINUS_INFINITY() == Integer::MINUS_INFINITY());
  assert(!(Integer::MINUS_INFINITY() != Integer::MINUS_INFINITY()));
}

void utest_abs_integer()
{
  expect(__FUNCTION__, Integer(1), Integer(1).abs());
  expect(__FUNCTION__, Integer(1), Integer(-1).abs());
  expect(__FUNCTION__, Integer(0), Integer(0).abs());
  expect(__FUNCTION__, Integer::PLUS_INFINITY(), Integer::PLUS_INFINITY().abs());
  expect(__FUNCTION__, Integer::PLUS_INFINITY(), Integer::MINUS_INFINITY().abs());
}

void utest_max_integer()
{
  expect(__FUNCTION__, Integer::PLUS_INFINITY(),
      Integer::PLUS_INFINITY().max(Integer::PLUS_INFINITY()));
  expect(__FUNCTION__, Integer::PLUS_INFINITY(),
      Integer::PLUS_INFINITY().max(Integer::MINUS_INFINITY()));
  expect(__FUNCTION__, Integer::PLUS_INFINITY(), Integer::PLUS_INFINITY().max(Integer(1)));
  expect(__FUNCTION__, Integer(2), Integer(1).max(Integer(2)));
  expect(__FUNCTION__, Integer(0), Integer(0).max(Integer::MINUS_INFINITY()));
}

void utest_min_integer()
{
  expect(__FUNCTION__, Integer::PLUS_INFINITY(),
      Integer::PLUS_INFINITY().min(Integer::PLUS_INFINITY()));
  expect(__FUNCTION__, Integer::MINUS_INFINITY(),
      Integer::PLUS_INFINITY().min(Integer::MINUS_INFINITY()));
  expect(__FUNCTION__, Integer(1), Integer::PLUS_INFINITY().min(Integer(1)));
  expect(__FUNCTION__, Integer(1), Integer(1).min(Integer(2)));
  expect(__FUNCTION__, Integer(0), Integer(0).min(Integer::PLUS_INFINITY()));
}

void utest_absmax_integer()
{
  expect(__FUNCTION__, Integer::MINUS_INFINITY(), Integer::MINUS_INFINITY().absMax(Integer(1)));
  expect(__FUNCTION__, Integer::PLUS_INFINITY(),
      Integer::MINUS_INFINITY().absMax(Integer::PLUS_INFINITY()));
  expect(__FUNCTION__, Integer(-2), Integer(-2).absMax(Integer(1)));
  expect(__FUNCTION__, Integer(2), Integer(-2).absMax(Integer(2)));
}

void utest_absmin_integer()
{
  expect(__FUNCTION__, Integer(1), Integer::MINUS_INFINITY().absMin(Integer(1)));
  expect(__FUNCTION__, Integer::MINUS_INFINITY(),
      Integer::MINUS_INFINITY().absMin(Integer::PLUS_INFINITY()));
  expect(__FUNCTION__, Integer(1), Integer(-2).absMin(Integer(1)));
  expect(__FUNCTION__, Integer(-2), Integer(-2).absMin(Integer(2)));
  expect(__FUNCTION__, Integer(0), Integer(-2).absMin(Integer(0)));
  expect(__FUNCTION__, Integer(0), Integer(2).absMin(Integer(0)));
}

void utest_tostring_integer()
{
  assert("-inf" == Integer::MINUS_INFINITY().toString());
  assert("+inf" == Integer::PLUS_INFINITY().toString());
  assert("0" == Integer(0).toString());
  assert("1" == Integer(1).toString());
  assert("-1" == Integer(-1).toString());
}
