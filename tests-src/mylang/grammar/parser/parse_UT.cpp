#include <parser.h>
#include <cassert>

void utest_parse_text()
{
  const char *TEXT = ""
      "unit foo {\n"
      "defun id(x:integer):integer = x;\n"
      "}\n"
      "";
  auto parser = ::mylang::grammar::parser::createParser();
  auto ast = parser->parseText(TEXT, __FUNCTION__);
  assert(ast != nullptr && "Expected a syntactically valid AST");
}

void utest_parse_syntax_error()
{
  const char *TEXT = ""
      "unit foo {\n"
      "id(x:integer):integer = x;\n"
      "}\n"
      "";
  auto parser = ::mylang::grammar::parser::createParser();
  auto ast = parser->parseText(TEXT, __FUNCTION__);
  assert(ast == nullptr && "Expected a syntax error and thus no AST");
}

