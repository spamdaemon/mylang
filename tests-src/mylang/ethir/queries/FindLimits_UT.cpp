#include <mylang/ethir/ir/LiteralInteger.h>
#include <mylang/ethir/ir/OperatorExpression.h>
#include <mylang/ethir/ir/Program.h>
#include <mylang/ethir/ir/StatementBuilder.h>
#include <mylang/ethir/ir/Variable.h>
#include <mylang/ethir/limits/BooleanLimits.h>
#include <mylang/ethir/limits/IntegerLimits.h>
#include <mylang/ethir/limits/Limits.h>
#include <mylang/ethir/Name.h>
#include <mylang/ethir/prettyPrint.h>
#include <mylang/ethir/ProgramBuilder.h>
#include <mylang/ethir/queries/FindLimits.h>
#include <mylang/ethir/ssa/SSAProgram.h>
#include <mylang/ethir/types/IntegerType.h>
#include <iostream>
#include <memory>
#include <utility>

using namespace mylang::ethir;
using namespace mylang::ethir::ir;
using namespace mylang::ethir::queries;
using namespace mylang::ethir::types;

void utest_find_loopvar_limits()
{
  ProgramBuilder setup;

  setup.addFunction([&]
  {
    Name loop1 = Name::create("loop1");
    Name loop2 = Name::create("loop2");
    StatementBuilder b;
    auto zero = b.declareLocalValue(LiteralInteger::create(IntegerType::create(), 0));
    auto one = b.declareLocalValue(LiteralInteger::create(1));
    auto counter = b.declareLocalVariable(Name::create("counter"), zero);
    auto N = b.declareLocalValue(LiteralInteger::create(IntegerType::create(), 10));
    auto sum = b.declareLocalVariable(Name::create("sum"), zero);

    b.appendLoop(loop1, [&](StatementBuilder &b1) {
      auto cond = b1.declareLocalValue(OperatorExpression::OP_LT, {counter,N});
      b1.appendIf(cond, [&](StatementBuilder& b2) {
            auto next_sum = b2.declareLocalValue(OperatorExpression::OP_ADD, {counter,sum});
            auto next = b2.declareLocalValue(OperatorExpression::OP_ADD, {counter,one});
            b2.appendUpdate(counter, next);
            b2.appendUpdate(sum, next_sum);
          }, [&](StatementBuilder& b3) {
            b3.appendBreak(loop1);
          });
    });
    b.appendReturn(sum);
    return b.build();
  });

  auto prg = setup.buildSSA()->getSSAForm();
  prettyPrint(prg, ::std::cout);

  auto bounds = FindLimits::find(prg);

  for (auto e : bounds) {
    ::std::cerr << e.first << ':';
    if (e.second->self<limits::IntegerLimits>()) {
      ::std::cerr << e.second->self<limits::IntegerLimits>()->bounds;
    } else if (e.second->self<limits::BooleanLimits>()) {
      auto lim = e.second->self<limits::BooleanLimits>();
      ::std::cerr << "[ ";
      std::cerr << ::std::endl;
      for (auto ex : lim->iftrue) {
        prettyPrint(ex, ::std::cerr);
        std::cerr << ::std::endl;
      }
      ::std::cerr << " ]";
    } else {
      ::std::cerr << "not-printable";
    }
    std::cerr << ::std::endl;
  }
}
