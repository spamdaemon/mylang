#include <mylang/ethir/queries/FindFreeVariables.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/ir/StatementBuilder.h>
#include <mylang/ethir/types/types.h>
#include <mylang/ethir/prettyPrint.h>
#include <iostream>
#include <memory>
#include <cassert>

using namespace mylang::ethir;
using namespace mylang::ethir::ir;
using namespace mylang::ethir::queries;
using namespace mylang::ethir::types;

void utest_find_free_variables()
{
  auto ty1 = BooleanType::create();

  EVariable v1 = Variable::create(Variable::Scope::FUNCTION_SCOPE, ty1, Name::create("v1"));

  StatementBuilder b;
  auto x = b.declareLocalValue(LiteralBoolean::create(true));
  auto L = b.declareLocalLambda( { ty1 }, [&](StatementBuilder &bb, ::std::vector<EVariable> vars) {
    auto e = bb.declareLocalValue(OperatorExpression::OP_EQ, {vars.at(0),x});
    e = bb.declareLocalValue(OperatorExpression::OP_EQ, {vars.at(0),v1});
    bb.appendReturn(e);
    return e->type;
  });

  auto S = b.build();

  prettyPrint(S, ::std::cerr);

  auto freeVars = FindFreeVariables::find(S);
  ::std::cerr << "Free variables : " << ::std::endl;
  for (auto e : freeVars) {
    ::std::cerr << "  " << e.first.toString() << ::std::endl;
  }
  assert(freeVars.size() == 1 && "Expected 1 free variables");
  assert(freeVars.count(v1->name) == 1 && "Expected v1");
}
