#include <mylang/ethir/types/IntegerType.h>
#include <cassert>

using namespace mylang::ethir;
using namespace mylang::ethir::types;

void utest_inferconstraints()
{
  auto a = IntegerType::create(mylang::Interval::NON_NEGATIVE());
  auto b = IntegerType::create(5, 10);

  EType at, af, bt, bf;
  a->inferConstraints(Type::OP_LT, b, at, af, bt, bf);

  ::std::cerr << "a: " << at->toString() << ", " << af->toString() << ::std::endl;
  ::std::cerr << "b: " << bt->toString() << ", " << bf->toString() << ::std::endl;

  assert(IntegerType::create(0, 9)->isSameType(*at) && "at: unexpected");
  assert(
      IntegerType::create(5, mylang::Integer::PLUS_INFINITY())->isSameType(*af)
          && "at: unexpected");
  assert(IntegerType::create(5, 10)->isSameType(*bt) && "bt: unexpected");
  assert(IntegerType::create(5, 10)->isSameType(*bf) && "bf: unexpected");
}
