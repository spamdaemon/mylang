#include <mylang/ethir/ssa/ToSSA.h>
#include <mylang/ethir/TestSupport.h>
#include <iostream>

using namespace mylang::ethir;
using namespace mylang::ethir::ir;
using namespace mylang::ethir::types;
using namespace mylang::ethir::ssa;

void utest_loop2_toSSA()
{
  ProgramBuilder setup;

  setup.addFunction([&]
  {
    Name loop1 = Name::create("loop1");
    Name loop2 = Name::create("loop2");
    StatementBuilder b;
    auto TRUE = b.declareLocalValue(LiteralBoolean::create(true));
    auto FALSE = b.declareLocalValue(LiteralBoolean::create(false));
    auto out = Variable::create(Variable::Scope::FUNCTION_SCOPE, TRUE->type, Name::create("out"));
    b.declareVariable(out);
    b.appendLoop(loop1, [&](StatementBuilder &b1) {
      b1.appendBlock([&](StatementBuilder& blk1) {
            auto v = Variable::create(Variable::Scope::FUNCTION_SCOPE, TRUE->type,Name::create("local"));
       blk1.declareVariable(v,TRUE);
       blk1.appendLoop(loop2,[&](StatementBuilder& b2) {
         b2.appendBlock([&](StatementBuilder& blk2) {

           blk2.appendIf(TRUE,[&](StatementBuilder& ift) {
             ift.appendBreak(loop2);
           });

           blk2.appendUpdate(v, FALSE);
           blk2.appendBreak(loop2);
         });
       });
       blk1.appendUpdate(out, v);
       blk1.appendBreak(loop1);
     });
    });
    return b.build();
  });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(val (ref (name \"fn_b\") global-scope \"_a\")"
              "(lambda (tag \"lambda_9\")"
              "(block"
              "(val (ref (name \"local_4\") function-scope \"_3\") true)"
              "(val (ref (name \"local_6\") function-scope \"_5\") false)"
              "(val (ref (name \"out_7\") function-scope \"_3\") (no-value))"
              "(loop (name \"loop1_1\")"
              "(block"
              "(val (ref (name \"out_7\"{1}) function-scope \"_3\") (phi (type \"_3\") (ref (name \"out_7\") function-scope \"_3\") (ref (name \"out_7\"{2}) function-scope \"_3\")))"
              "(val (ref (name \"local_8\") function-scope \"_3\") (ref (name \"local_4\") function-scope \"_3\"))"
              "(loop (name \"loop2_2\")"
              "(block"
              "(val (ref (name \"local_8\"{1}) function-scope \"_3\") (phi (type \"_3\") (ref (name \"local_8\") function-scope \"_3\") (ref (name \"local_8\"{2}) function-scope \"_3\")))"
              "(if (ref (name \"local_4\") function-scope \"_3\")"
              "(block"
              "(break (name \"loop2_2\")))"
              "(block))"
              "(val (ref (name \"local_8\"{2}) function-scope \"_3\") (ref (name \"local_6\") function-scope \"_5\"))"
              "(break (name \"loop2_2\"))))"
              "(val (ref (name \"local_8\"{3}) function-scope \"_3\") (phi (type \"_3\") (ref (name \"local_8\"{1}) function-scope \"_3\") (ref (name \"local_8\"{2}) function-scope \"_3\")))"
              "(val (ref (name \"out_7\"{2}) function-scope \"_3\") (ref (name \"local_8\"{3}) function-scope \"_3\"))"
              "(break (name \"loop1_1\")))))))"
              ""));
}

void utest_toSSA()
{
  ProgramBuilder setup;

  setup.addFunction(types::BooleanType::create(), [&]
  {
    StatementBuilder b;
    auto var = b.declareLocalValue(LiteralBoolean::create(true));
    b.appendReturn(var);
    return b.build();
  });

  auto prg = ToSSA::transform(setup.build());

  assert(TestSupport::diff(prg, ""
      "(val (ref (name \"fn_5\") global-scope \"_4\")"
      "(lambda (tag \"lambda_3\")"
      "(block"
      "(val (ref (name \"local_2\") function-scope \"_1\") true)"
      "(return (ref (name \"local_2\") function-scope \"_1\")))))"
      ""));
}

void utest_if_to_ssa_both_branches()
{
  /**
   * Testing a variable update in both branches of an if-statement;
   * We're expecting a phi node after the if-statement
   */
  ProgramBuilder setup;
  auto intTy = types::IntegerType::create();

  setup.addFunction(intTy, { types::BooleanType::create() }, [&](ProgramBuilder::Arguments args) {
    StatementBuilder b;
    auto val = b.declareLocalValue(LiteralInteger::create(intTy,0));
    auto var = b.declareLocalVariable(val);
    b.appendIf(args.at(0),[&](StatementBuilder& bb) {
          auto v = bb.declareLocalValue(LiteralInteger::create(intTy,1));
          bb.appendUpdate(var, v);
        }, [&](StatementBuilder& bb) {
          auto v = bb.declareLocalValue(LiteralInteger::create(intTy,2));
          bb.appendUpdate(var, v);
        });
    b.appendReturn(var);
    return b.build();
  });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_9\") global-scope \"_8\")"
              "(lambda (tag \"lambda_7\")"
              "(parameter (name \"arg_2\") \"_1\")"
              "(block"
              "(val (ref (name \"local_3\") function-scope \"_0\") 0)"
              "(val (ref (name \"local_4\") function-scope \"_0\") (ref (name \"local_3\") function-scope \"_0\"))"
              "(if (ref (name \"arg_2\") function-scope \"_1\")"
              "(block"
              "(val (ref (name \"local_5\") function-scope \"_0\") 1)"
              "(val (ref (name \"local_4\"{1}) function-scope \"_0\") (ref (name \"local_5\") function-scope \"_0\")))"
              "(block"
              "(val (ref (name \"local_6\") function-scope \"_0\") 2)"
              "(val (ref (name \"local_4\"{2}) function-scope \"_0\") (ref (name \"local_6\") function-scope \"_0\"))))"
              "(val (ref (name \"local_4\"{3}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_4\"{1}) function-scope \"_0\") (ref (name \"local_4\"{2}) function-scope \"_0\")))"
              "(return (ref (name \"local_4\"{3}) function-scope \"_0\")))))"
              ""));
}

void utest_if_to_ssa_if_true_branch()
{
  /**
   * Testing a variable update in the if-true branch of an if-statement;
   * We're expecting a phi node after the if-statement
   */
  ProgramBuilder setup;
  auto intTy = types::IntegerType::create();

  setup.addFunction(intTy, { types::BooleanType::create() }, [&](ProgramBuilder::Arguments args) {
    StatementBuilder b;
    auto val = b.declareLocalValue(LiteralInteger::create(intTy,0));
    auto var = b.declareLocalVariable(val);
    b.appendIf(args.at(0),[&](StatementBuilder& bb) {
          auto v = bb.declareLocalValue(LiteralInteger::create(intTy,1));
          bb.appendUpdate(var, v);
        });
    b.appendReturn(var);
    return b.build();
  });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_8\") global-scope \"_7\")"
              "(lambda (tag \"lambda_6\")"
              "(parameter (name \"arg_2\") \"_1\")"
              "(block"
              "(val (ref (name \"local_3\") function-scope \"_0\") 0)"
              "(val (ref (name \"local_4\") function-scope \"_0\") (ref (name \"local_3\") function-scope \"_0\"))"
              "(if (ref (name \"arg_2\") function-scope \"_1\")"
              "(block"
              "(val (ref (name \"local_5\") function-scope \"_0\") 1)"
              "(val (ref (name \"local_4\"{1}) function-scope \"_0\") (ref (name \"local_5\") function-scope \"_0\")))"
              "(block))"
              "(val (ref (name \"local_4\"{2}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_4\") function-scope \"_0\") (ref (name \"local_4\"{1}) function-scope \"_0\")))"
              "(return (ref (name \"local_4\"{2}) function-scope \"_0\")))))"
              ""));
}

void utest_if_to_ssa_if_false_branch()
{
  /**
   * Testing a variable update in the if-false branch of an if-statement;
   * We're expecting a phi node after the if-statement
   */
  ProgramBuilder setup;
  auto intTy = types::IntegerType::create();

  setup.addFunction(intTy, { types::BooleanType::create() }, [&](ProgramBuilder::Arguments args) {
    StatementBuilder b;
    auto val = b.declareLocalValue(LiteralInteger::create(intTy,0));
    auto var = b.declareLocalVariable(val);
    b.appendIf(args.at(0),[&](StatementBuilder&) {},
        [&](StatementBuilder& bb) {
          auto v = bb.declareLocalValue(LiteralInteger::create(intTy,1));
          bb.appendUpdate(var, v);
        });
    b.appendReturn(var);
    return b.build();
  });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_8\") global-scope \"_7\")"
              "(lambda (tag \"lambda_6\")"
              "(parameter (name \"arg_2\") \"_1\")"
              "(block"
              "(val (ref (name \"local_3\") function-scope \"_0\") 0)"
              "(val (ref (name \"local_4\") function-scope \"_0\") (ref (name \"local_3\") function-scope \"_0\"))"
              "(if (ref (name \"arg_2\") function-scope \"_1\")"
              "(block)"
              "(block"
              "(val (ref (name \"local_5\") function-scope \"_0\") 1)"
              "(val (ref (name \"local_4\"{1}) function-scope \"_0\") (ref (name \"local_5\") function-scope \"_0\"))))"
              "(val (ref (name \"local_4\"{2}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_4\") function-scope \"_0\") (ref (name \"local_4\"{1}) function-scope \"_0\")))"
              "(return (ref (name \"local_4\"{2}) function-scope \"_0\")))))"
              ""));
}

void utest_if_to_ssa_with_throw()
{
  /**
   * Testing a variable update in the if-false branch of an if-statement;
   * We're expecting a phi node after the if-statement
   */
  ProgramBuilder setup;
  auto intTy = types::IntegerType::create();

  setup.addFunction(intTy, { types::BooleanType::create() }, [&](ProgramBuilder::Arguments args) {
    StatementBuilder b;
    auto val = b.declareLocalValue(LiteralInteger::create(intTy,0));
    auto var = b.declareLocalVariable(val);
    b.appendIf(args.at(0),
        [&](StatementBuilder& bb) {
          auto v = bb.declareLocalValue(LiteralInteger::create(intTy,1));
          bb.appendUpdate(var, v);
          bb.appendThrow();
        }, [&](StatementBuilder& bb) {
          auto v = bb.declareLocalValue(LiteralInteger::create(intTy,1));
          bb.appendUpdate(var, v);
          bb.appendThrow();
        });
    b.appendReturn(var);
    return b.build();
  });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_9\") global-scope \"_8\")"
              "(lambda (tag \"lambda_7\")"
              "(parameter (name \"arg_2\") \"_1\")"
              "(block"
              "(val (ref (name \"local_3\") function-scope \"_0\") 0)"
              "(val (ref (name \"local_4\") function-scope \"_0\") (ref (name \"local_3\") function-scope \"_0\"))"
              "(if (ref (name \"arg_2\") function-scope \"_1\")"
              "(block"
              "(val (ref (name \"local_5\") function-scope \"_0\") 1)"
              "(val (ref (name \"local_4\"{1}) function-scope \"_0\") (ref (name \"local_5\") function-scope \"_0\"))"
              "(throw))"
              "(block"
              "(val (ref (name \"local_6\") function-scope \"_0\") 1)"
              "(val (ref (name \"local_4\"{2}) function-scope \"_0\") (ref (name \"local_6\") function-scope \"_0\"))"
              "(throw)))"
              "(val (ref (name \"local_4\"{3}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_4\"{1}) function-scope \"_0\") (ref (name \"local_4\"{2}) function-scope \"_0\")))"
              "(return (ref (name \"local_4\"{3}) function-scope \"_0\")))))"
              ""));
}

void utest_try_to_ssa()
{
  /**
   * Testing a variable update in the try and catch blocks
   * We're expecting a phi node after the statement and in the catch block
   */
  ProgramBuilder setup;
  auto intTy = types::IntegerType::create();

  setup.addFunction(intTy, { types::BooleanType::create() }, [&](ProgramBuilder::Arguments args) {
    StatementBuilder b;
    auto val = b.declareLocalValue(LiteralInteger::create(intTy,0));
    auto var = b.declareLocalVariable(val);
    b.appendTryCatch([&](StatementBuilder& bb) {
          bb.appendIf(args.at(0), [&](StatementBuilder& bbb) {
                auto v = bbb.declareLocalValue(LiteralInteger::create(intTy,1));
                bbb.appendUpdate(var, v);
                bbb.appendThrow();
              });
          auto v = bb.declareLocalValue(LiteralInteger::create(intTy,2));
          bb.appendUpdate(var, v);
        },
        [&](StatementBuilder& bb) {
          auto v = bb.declareLocalValue(LiteralInteger::create(intTy,3));
          bb.appendUpdate(var, v);
        });
    b.appendReturn(var);
    return b.build();
  });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_a\") global-scope \"_9\")"
              "(lambda (tag \"lambda_8\")"
              "(parameter (name \"arg_2\") \"_1\")"
              "(block"
              "(val (ref (name \"local_3\") function-scope \"_0\") 0)"
              "(val (ref (name \"local_4\") function-scope \"_0\") (ref (name \"local_3\") function-scope \"_0\"))"
              "(try"
              "(block"
              "(if (ref (name \"arg_2\") function-scope \"_1\")"
              "(block"
              "(val (ref (name \"local_5\") function-scope \"_0\") 1)"
              "(val (ref (name \"local_4\"{1}) function-scope \"_0\") (ref (name \"local_5\") function-scope \"_0\"))"
              "(throw))"
              "(block))"
              "(val (ref (name \"local_4\"{2}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_4\") function-scope \"_0\") (ref (name \"local_4\"{1}) function-scope \"_0\")))"
              "(val (ref (name \"local_6\") function-scope \"_0\") 2)"
              "(val (ref (name \"local_4\"{3}) function-scope \"_0\") (ref (name \"local_6\") function-scope \"_0\")))"
              "(block"
              "(val (ref (name \"local_7\") function-scope \"_0\") 3)"
              "(val (ref (name \"local_4\"{4}) function-scope \"_0\") (ref (name \"local_7\") function-scope \"_0\"))))"
              "(val (ref (name \"local_4\"{5}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_4\"{3}) function-scope \"_0\") (ref (name \"local_4\"{4}) function-scope \"_0\")))"
              "(return (ref (name \"local_4\"{5}) function-scope \"_0\")))))"
              ""));
}

void utest_try_to_ssa_update_in_try()
{
  /**
   * Testing a variable update in the try-block only.
   * We're expecting a phi node after the statement and in the catch block
   */
  ProgramBuilder setup;
  auto intTy = types::IntegerType::create();

  setup.addFunction(intTy, { types::BooleanType::create() }, [&](ProgramBuilder::Arguments args) {
    StatementBuilder b;
    auto val = b.declareLocalValue(LiteralInteger::create(intTy,0));
    auto var = b.declareLocalVariable(val);
    b.appendTryCatch([&](StatementBuilder& bb) {
          bb.appendIf(args.at(0), [&](StatementBuilder& bbb) {
                auto vv = bb.declareLocalValue(LiteralInteger::create(intTy,2));
                bbb.appendUpdate(var, vv);
                bbb.appendThrow();
              });
          auto v = bb.declareLocalValue(LiteralInteger::create(intTy,1));
          bb.appendUpdate(var, v);
        },
        [&](StatementBuilder& ) {
        });
    b.appendReturn(var);
    return b.build();
  });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_9\") global-scope \"_8\")"
              "(lambda (tag \"lambda_7\")"
              "(parameter (name \"arg_2\") \"_1\")"
              "(block"
              "(val (ref (name \"local_3\") function-scope \"_0\") 0)"
              "(val (ref (name \"local_4\") function-scope \"_0\") (ref (name \"local_3\") function-scope \"_0\"))"
              "(try"
              "(block"
              "(val (ref (name \"local_5\") function-scope \"_0\") 2)"
              "(if (ref (name \"arg_2\") function-scope \"_1\")"
              "(block"
              "(val (ref (name \"local_4\"{1}) function-scope \"_0\") (ref (name \"local_5\") function-scope \"_0\"))"
              "(throw))"
              "(block))"
              "(val (ref (name \"local_4\"{2}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_4\") function-scope \"_0\") (ref (name \"local_4\"{1}) function-scope \"_0\")))"
              "(val (ref (name \"local_6\") function-scope \"_0\") 1)"
              "(val (ref (name \"local_4\"{3}) function-scope \"_0\") (ref (name \"local_6\") function-scope \"_0\")))"
              "(block))"
              "(return (ref (name \"local_4\"{3}) function-scope \"_0\")))))"
              ""));
}

void utest_try_to_ssa_update_in_catch()
{
  /**
   * Testing a variable update in a catch-block only.
   * We're expecting a phi node after the statement
   */
  ProgramBuilder setup;
  auto intTy = types::IntegerType::create();

  setup.addFunction(intTy, { types::BooleanType::create() }, [&](ProgramBuilder::Arguments args) {
    StatementBuilder b;
    auto val = b.declareLocalValue(LiteralInteger::create(intTy,0));
    auto var = b.declareLocalVariable(val);
    b.appendTryCatch([&](StatementBuilder& bb) {
          bb.appendIf(args.at(0), [&](StatementBuilder& bbb) {
                bbb.appendThrow();
              });
        },
        [&](StatementBuilder& bb) {
          auto v = bb.declareLocalValue(LiteralInteger::create(intTy,2));
          bb.appendUpdate(var, v);
        });
    b.appendReturn(var);
    return b.build();
  });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_8\") global-scope \"_7\")"
              "(lambda (tag \"lambda_6\")"
              "(parameter (name \"arg_2\") \"_1\")"
              "(block"
              "(val (ref (name \"local_3\") function-scope \"_0\") 0)"
              "(val (ref (name \"local_4\") function-scope \"_0\") (ref (name \"local_3\") function-scope \"_0\"))"
              "(try"
              "(block"
              "(if (ref (name \"arg_2\") function-scope \"_1\")"
              "(block"
              "(throw))"
              "(block)))"
              "(block"
              "(val (ref (name \"local_5\") function-scope \"_0\") 2)"
              "(val (ref (name \"local_4\"{1}) function-scope \"_0\") (ref (name \"local_5\") function-scope \"_0\"))))"
              "(val (ref (name \"local_4\"{2}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_4\") function-scope \"_0\") (ref (name \"local_4\"{1}) function-scope \"_0\")))"
              "(return (ref (name \"local_4\"{2}) function-scope \"_0\")))))"
              ""));
}

void utest_foreach_to_ssa_update_in_loop()
{
  /**
   * Testing a variable update in a foreach loop.
   * We're expecting a phi node after the statement and as the first statement
   * in the loop's body
   */
  ProgramBuilder setup;
  auto intTy = types::IntegerType::create();
  auto arrTy = types::ArrayType::get(types::BooleanType::create(), mylang::BigInt(0),
      mylang::BigInt(1));

  setup.addFunction(intTy, { arrTy },
      [&](
          ProgramBuilder::Arguments args) {
            StatementBuilder b;
            auto val = b.declareLocalValue(LiteralInteger::create(intTy,0));
            auto var = b.declareLocalVariable(val);
            auto loopVar = ir::Variable::create(ir::Variable::Scope::FUNCTION_SCOPE,arrTy->element,Name::create("i"));

            b.appendForEach(Name::create("loop"), loopVar, args.at(0), [&](StatementBuilder& bb) {
                  auto v = bb.declareLocalValue(LiteralInteger::create(intTy,2));
                  bb.appendUpdate(var, v);
                });
            b.appendReturn(var);
            return b.build();
          });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_e\") global-scope \"_d\")"
              "(lambda (tag \"lambda_c\")"
              "(parameter (name \"arg_6\") \"_5\")"
              "(block"
              "(val (ref (name \"local_7\") function-scope \"_0\") 0)"
              "(val (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_7\") function-scope \"_0\"))"
              "(for (name \"loop_a\") (ref (name \"arg_6\") function-scope \"_5\")"
              "(block"
              "(val (ref (name \"local_8\"{1}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_8\"{2}) function-scope \"_0\")))"
              "(val (ref (name \"local_b\") function-scope \"_0\") 2)"
              "(val (ref (name \"local_8\"{2}) function-scope \"_0\") (ref (name \"local_b\") function-scope \"_0\"))))"
              "(val (ref (name \"local_8\"{3}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_8\"{2}) function-scope \"_0\")))"
              "(return (ref (name \"local_8\"{3}) function-scope \"_0\")))))"
              ""));
}

void utest_foreach_to_ssa_break()
{
  /**
   * Testing a variable update in a foreach loop with an early loop termination
   * We're expecting a phi node after the statement and as the first statement
   * in the loop's body
   */
  ProgramBuilder setup;
  auto intTy = types::IntegerType::create();
  auto arrTy = types::ArrayType::get(types::BooleanType::create(), mylang::BigInt(0),
      mylang::BigInt(1));

  setup.addFunction(intTy, { arrTy },
      [&](
          ProgramBuilder::Arguments args) {
            StatementBuilder b;
            auto val = b.declareLocalValue(LiteralInteger::create(intTy,0));
            auto var = b.declareLocalVariable(val);
            auto loopVar = ir::Variable::create(ir::Variable::Scope::FUNCTION_SCOPE,arrTy->element,Name::create("i"));
            auto loopName = Name::create("loop");
            b.appendForEach(loopName, loopVar, args.at(0), [&](StatementBuilder& bb) {
                  auto v = bb.declareLocalValue(LiteralInteger::create(intTy,2));
                  bb.appendUpdate(var, v);
                  bb.appendIf(loopVar,[&](StatementBuilder&bbb) {bbb.appendBreak(loopName);});
                  v = bb.declareLocalValue(LiteralInteger::create(intTy,3));
                  bb.appendUpdate(var, v);
                });
            b.appendReturn(var);
            return b.build();
          });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_f\") global-scope \"_e\")"
              "(lambda (tag \"lambda_d\")"
              "(parameter (name \"arg_6\") \"_5\")"
              "(block"
              "(val (ref (name \"local_7\") function-scope \"_0\") 0)"
              "(val (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_7\") function-scope \"_0\"))"
              "(for (name \"loop_a\") (ref (name \"arg_6\") function-scope \"_5\")"
              "(block"
              "(val (ref (name \"local_8\"{1}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_8\"{3}) function-scope \"_0\")))"
              "(val (ref (name \"local_b\") function-scope \"_0\") 2)"
              "(val (ref (name \"local_8\"{2}) function-scope \"_0\") (ref (name \"local_b\") function-scope \"_0\"))"
              "(if (ref (name \"i_9\") function-scope \"_1\")"
              "(block"
              "(break (name \"loop_a\")))"
              "(block))"
              "(val (ref (name \"local_c\") function-scope \"_0\") 3)"
              "(val (ref (name \"local_8\"{3}) function-scope \"_0\") (ref (name \"local_c\") function-scope \"_0\"))))"
              "(val (ref (name \"local_8\"{4}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_8\"{2}) function-scope \"_0\") (ref (name \"local_8\"{3}) function-scope \"_0\")))"
              "(return (ref (name \"local_8\"{4}) function-scope \"_0\")))))"
              ""));
}

void utest_foreach_to_ssa_continue()
{
  /**
   * Testing a variable update in a foreach loop with an early continuation..
   * We're expecting a phi node after the statement and as the first statement
   * in the loop's body
   */
  ProgramBuilder setup;
  auto intTy = types::IntegerType::create();
  auto arrTy = types::ArrayType::get(types::BooleanType::create(), mylang::BigInt(0),
      mylang::BigInt(1));

  setup.addFunction(intTy, { arrTy },
      [&](
          ProgramBuilder::Arguments args) {
            StatementBuilder b;
            auto val = b.declareLocalValue(LiteralInteger::create(intTy,0));
            auto var = b.declareLocalVariable(val);
            auto loopVar = ir::Variable::create(ir::Variable::Scope::FUNCTION_SCOPE,arrTy->element,Name::create("i"));
            auto loopName = Name::create("loop");
            b.appendForEach(loopName, loopVar, args.at(0), [&](StatementBuilder& bb) {
                  auto v = bb.declareLocalValue(LiteralInteger::create(intTy,2));
                  bb.appendUpdate(var, v);
                  bb.appendIf(loopVar,[&](StatementBuilder&bbb) {bbb.appendContinue(loopName);});
                  v = bb.declareLocalValue(LiteralInteger::create(intTy,3));
                  bb.appendUpdate(var, v);
                });
            b.appendReturn(var);
            return b.build();
          });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_f\") global-scope \"_e\")"
              "(lambda (tag \"lambda_d\")"
              "(parameter (name \"arg_6\") \"_5\")"
              "(block"
              "(val (ref (name \"local_7\") function-scope \"_0\") 0)"
              "(val (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_7\") function-scope \"_0\"))"
              "(for (name \"loop_a\") (ref (name \"arg_6\") function-scope \"_5\")"
              "(block"
              "(val (ref (name \"local_8\"{1}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_8\"{2}) function-scope \"_0\") (ref (name \"local_8\"{3}) function-scope \"_0\")))"
              "(val (ref (name \"local_b\") function-scope \"_0\") 2)"
              "(val (ref (name \"local_8\"{2}) function-scope \"_0\") (ref (name \"local_b\") function-scope \"_0\"))"
              "(if (ref (name \"i_9\") function-scope \"_1\")"
              "(block"
              "(continue (name \"loop_a\")))"
              "(block))"
              "(val (ref (name \"local_c\") function-scope \"_0\") 3)"
              "(val (ref (name \"local_8\"{3}) function-scope \"_0\") (ref (name \"local_c\") function-scope \"_0\"))))"
              "(val (ref (name \"local_8\"{4}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_8\"{3}) function-scope \"_0\")))"
              "(return (ref (name \"local_8\"{4}) function-scope \"_0\")))))"
              ""));
}

void utest_foreach_to_ssa_abort()
{
  /**
   * Testing a variable update in a foreach loop with an early continuation..
   * We're expecting a phi node after the statement and as the first statement
   * in the loop's body
   */
  ProgramBuilder setup;
  auto intTy = types::IntegerType::create();
  auto arrTy = types::ArrayType::get(types::BooleanType::create(), mylang::BigInt(0),
      mylang::BigInt(1));

  setup.addFunction(intTy, { arrTy },
      [&](
          ProgramBuilder::Arguments args) {
            StatementBuilder b;
            auto val = b.declareLocalValue(LiteralInteger::create(intTy,0));
            auto var = b.declareLocalVariable(val);
            auto loopVar = ir::Variable::create(ir::Variable::Scope::FUNCTION_SCOPE,arrTy->element,Name::create("i"));
            auto loopName = Name::create("loop");
            b.appendForEach(loopName, loopVar, args.at(0), [&](StatementBuilder& bb) {
                  auto v = bb.declareLocalValue(LiteralInteger::create(intTy,2));
                  bb.appendUpdate(var, v);
                  bb.appendAbort("");
                });
            b.appendReturn(var);
            return b.build();
          });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_13\") global-scope \"_12\")"
              "(lambda (tag \"lambda_11\")"
              "(parameter (name \"arg_6\") \"_5\")"
              "(block"
              "(val (ref (name \"local_7\") function-scope \"_0\") 0)"
              "(val (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_7\") function-scope \"_0\"))"
              "(for (name \"loop_a\") (ref (name \"arg_6\") function-scope \"_5\")"
              "(block"
              "(val (ref (name \"local_8\"{1}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_8\"{2}) function-scope \"_0\")))"
              "(val (ref (name \"local_b\") function-scope \"_0\") 2)"
              "(val (ref (name \"local_8\"{2}) function-scope \"_0\") (ref (name \"local_b\") function-scope \"_0\"))"
              "(val (ref (name \"local_10\") function-scope \"_c\") \"\")"
              "(abort (ref (name \"local_10\") function-scope \"_c\"))))"
              "(val (ref (name \"local_8\"{3}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_8\"{2}) function-scope \"_0\")))"
              "(return (ref (name \"local_8\"{3}) function-scope \"_0\")))))"
              ""));
}

void utest_foreach_loop_with_exception()
{
  /**
   * Throwing an exception from a loop body.
   * There should only be a phi node at the beginnig of the loop and after.
   */
  ProgramBuilder setup;
  auto intTy = types::IntegerType::create();
  auto arrTy = types::ArrayType::get(types::BooleanType::create(), mylang::BigInt(0),
      mylang::BigInt(1));

  setup.addFunction(intTy, { arrTy },
      [&](
          ProgramBuilder::Arguments args) {
            StatementBuilder b;
            auto val = b.declareLocalValue(LiteralInteger::create(intTy,0));
            auto var = b.declareLocalVariable(val);
            auto loopVar = ir::Variable::create(ir::Variable::Scope::FUNCTION_SCOPE,arrTy->element,Name::create("i"));
            auto loopName = Name::create("loop");
            b.appendForEach(loopName, loopVar, args.at(0), [&](StatementBuilder& bb) {
                  auto v = bb.declareLocalValue(LiteralInteger::create(intTy,2));
                  bb.appendUpdate(var, v);
                  bb.appendThrow();
                });
            b.appendReturn(var);
            return b.build();
          });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_e\") global-scope \"_d\")"
              "(lambda (tag \"lambda_c\")"
              "(parameter (name \"arg_6\") \"_5\")"
              "(block"
              "(val (ref (name \"local_7\") function-scope \"_0\") 0)"
              "(val (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_7\") function-scope \"_0\"))"
              "(for (name \"loop_a\") (ref (name \"arg_6\") function-scope \"_5\")"
              "(block"
              "(val (ref (name \"local_8\"{1}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_8\"{2}) function-scope \"_0\")))"
              "(val (ref (name \"local_b\") function-scope \"_0\") 2)"
              "(val (ref (name \"local_8\"{2}) function-scope \"_0\") (ref (name \"local_b\") function-scope \"_0\"))"
              "(throw)))"
              "(val (ref (name \"local_8\"{3}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_8\"{2}) function-scope \"_0\")))"
              "(return (ref (name \"local_8\"{3}) function-scope \"_0\")))))"
              ""));
}

void utest_foreach_loop_nested()
{
  /**
   * Throwing an exception from a loop body.
   * There should only be a phi node at the beginnig of the loop and after.
   */
  ProgramBuilder setup;
  auto intTy = types::IntegerType::create();
  auto arrTy = types::ArrayType::get(types::IntegerType::create(), mylang::BigInt(0),
      mylang::BigInt(1));

  setup.addFunction(intTy, { arrTy },
      [&](
          ProgramBuilder::Arguments args) {
            StatementBuilder b;
            auto val = b.declareLocalValue(LiteralInteger::create(intTy,0));
            auto var = b.declareLocalVariable(val);
            auto outer_loopVar = ir::Variable::create(ir::Variable::Scope::FUNCTION_SCOPE,arrTy->element,Name::create("i"));
            auto outer_loopName = Name::create("loop");
            auto inner_loopVar = ir::Variable::create(ir::Variable::Scope::FUNCTION_SCOPE,arrTy->element,Name::create("i"));
            auto inner_loopName = Name::create("loop");
            b.appendForEach(outer_loopName, outer_loopVar, args.at(0), [&](StatementBuilder& bb) {
                  bb.appendForEach(inner_loopName, inner_loopVar, args.at(0), [&](StatementBuilder& bbb) {
                        auto v = bbb.declareLocalValue(OperatorExpression::create(OperatorExpression::OP_ADD,var,inner_loopVar));
                        bbb.appendUpdate(var, v);
                      });
                });
            b.appendReturn(var);
            return b.build();
          });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_11\") global-scope \"_10\")"
              "(lambda (tag \"lambda_f\")"
              "(parameter (name \"arg_6\") \"_5\")"
              "(block"
              "(val (ref (name \"local_7\") function-scope \"_0\") 0)"
              "(val (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_7\") function-scope \"_0\"))"
              "(for (name \"loop_a\") (ref (name \"arg_6\") function-scope \"_5\")"
              "(block"
              "(val (ref (name \"local_8\"{1}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_8\"{4}) function-scope \"_0\")))"
              "(for (name \"loop_c\") (ref (name \"arg_6\") function-scope \"_5\")"
              "(block"
              "(val (ref (name \"local_8\"{2}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_8\"{1}) function-scope \"_0\") (ref (name \"local_8\"{3}) function-scope \"_0\")))"
              "(val (ref (name \"local_e\") function-scope \"_d\") (apply-operator add (type \"_d\") (ref (name \"local_8\"{2}) function-scope \"_0\") (ref (name \"i_b\") function-scope \"_1\")))"
              "(val (ref (name \"local_8\"{3}) function-scope \"_0\") (ref (name \"local_e\") function-scope \"_d\"))))"
              "(val (ref (name \"local_8\"{4}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_8\"{1}) function-scope \"_0\") (ref (name \"local_8\"{3}) function-scope \"_0\")))))"
              "(val (ref (name \"local_8\"{5}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_8\"{4}) function-scope \"_0\")))"
              "(return (ref (name \"local_8\"{5}) function-scope \"_0\")))))"
              ""));
}

void utest_foreach_loop_no_update()
{
  /**
   * A for loop that doesn't do anything but a variable is declared before
   * and updated after the loop.
   */
  ProgramBuilder setup;
  auto intTy = types::IntegerType::create();
  auto arrTy = types::ArrayType::get(types::IntegerType::create(), mylang::BigInt(0),
      mylang::BigInt(1));

  setup.addFunction(intTy, { arrTy },
      [&](
          ProgramBuilder::Arguments args) {
            StatementBuilder b;
            auto val = b.declareLocalValue(LiteralInteger::create(intTy,0));
            auto var = b.declareLocalVariable(val);
            auto loopVar = ir::Variable::create(ir::Variable::Scope::FUNCTION_SCOPE,arrTy->element,Name::create("i"));
            auto loopName = Name::create("loop");
            b.appendForEach(loopName, loopVar, args.at(0), [&](StatementBuilder&) {
                });
            auto v = b.declareLocalValue(OperatorExpression::create(OperatorExpression::OP_ADD,var,val));
            b.appendUpdate(var, v);
            b.appendReturn(var);
            return b.build();
          });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_f\") global-scope \"_e\")"
              "(lambda (tag \"lambda_d\")"
              "(parameter (name \"arg_6\") \"_5\")"
              "(block"
              "(val (ref (name \"local_7\") function-scope \"_0\") 0)"
              "(val (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_7\") function-scope \"_0\"))"
              "(for (name \"loop_a\") (ref (name \"arg_6\") function-scope \"_5\")"
              "(block))"
              "(val (ref (name \"local_c\") function-scope \"_b\") (apply-operator add (type \"_b\") (ref (name \"local_8\") function-scope \"_0\") (ref (name \"local_7\") function-scope \"_0\")))"
              "(val (ref (name \"local_8\"{1}) function-scope \"_0\") (ref (name \"local_c\") function-scope \"_b\"))"
              "(return (ref (name \"local_8\"{1}) function-scope \"_0\")))))"
              ""));
}

void utest_generic_loop_no_update()
{
  /**
   * A for loop that doesn't do anything but a variable is declared before
   * and updated after the loop.
   */
  ProgramBuilder setup;
  auto intTy = types::IntegerType::create();

  setup.addFunction(intTy, [&]
  {
    StatementBuilder b;
    auto val = b.declareLocalValue(LiteralInteger::create(intTy, 0));
    auto var = b.declareLocalVariable(val);
    auto loopName = Name::create("loop");
    b.appendLoop(loopName, [&](StatementBuilder &bb) {
      bb.appendBreak(loopName);
    });
    auto v = b.declareLocalValue(OperatorExpression::create(OperatorExpression::OP_ADD, var, val));
    b.appendUpdate(var, v);
    b.appendReturn(var);
    return b.build();
  });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(val (ref (name \"fn_8\") global-scope \"_7\")"
              "(lambda (tag \"lambda_6\")"
              "(block"
              "(val (ref (name \"local_1\") function-scope \"_0\") 0)"
              "(val (ref (name \"local_2\") function-scope \"_0\") (ref (name \"local_1\") function-scope \"_0\"))"
              "(loop (name \"loop_3\")"
              "(block"
              "(break (name \"loop_3\"))))"
              "(val (ref (name \"local_5\") function-scope \"_4\") (apply-operator add (type \"_4\") (ref (name \"local_2\") function-scope \"_0\") (ref (name \"local_1\") function-scope \"_0\")))"
              "(val (ref (name \"local_2\"{1}) function-scope \"_0\") (ref (name \"local_5\") function-scope \"_4\"))"
              "(return (ref (name \"local_2\"{1}) function-scope \"_0\")))))"
              ""));
}

void utest_generic_loop_with_break()
{
  /**
   * A generic loop with a break. Expecting a phi statement in the loop and after the loop.
   */
  ProgramBuilder setup;
  auto intTy = types::IntegerType::create();
  auto cond = types::BooleanType::create();

  setup.addFunction(intTy, { cond }, [&](ProgramBuilder::Arguments args) {
    StatementBuilder b;
    auto val = b.declareLocalValue(LiteralInteger::create(intTy,0));
    auto var = b.declareLocalVariable(val);
    auto loopName = Name::create("loop");
    b.appendLoop(loopName, [&](StatementBuilder& bb) {
          auto v = bb.declareLocalValue(LiteralInteger::create(intTy,1));
          v = bb.declareLocalValue(OperatorExpression::create(OperatorExpression::OP_ADD,var,v));
          bb.appendUpdate(var, v);
          bb.appendIf(args.at(0),[&](StatementBuilder& bbb) {bbb.appendBreak(loopName);});
        });
    b.appendReturn(var);
    return b.build();
  });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_b\") global-scope \"_a\")"
              "(lambda (tag \"lambda_9\")"
              "(parameter (name \"arg_2\") \"_1\")"
              "(block"
              "(val (ref (name \"local_3\") function-scope \"_0\") 0)"
              "(val (ref (name \"local_4\") function-scope \"_0\") (ref (name \"local_3\") function-scope \"_0\"))"
              "(loop (name \"loop_5\")"
              "(block"
              "(val (ref (name \"local_4\"{1}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_4\") function-scope \"_0\") (ref (name \"local_4\"{2}) function-scope \"_0\")))"
              "(val (ref (name \"local_6\") function-scope \"_0\") 1)"
              "(val (ref (name \"local_8\") function-scope \"_7\") (apply-operator add (type \"_7\") (ref (name \"local_4\"{1}) function-scope \"_0\") (ref (name \"local_6\") function-scope \"_0\")))"
              "(val (ref (name \"local_4\"{2}) function-scope \"_0\") (ref (name \"local_8\") function-scope \"_7\"))"
              "(if (ref (name \"arg_2\") function-scope \"_1\")"
              "(block"
              "(break (name \"loop_5\")))"
              "(block))))"
              "(return (ref (name \"local_4\"{2}) function-scope \"_0\")))))"
              ""));
}

void utest_generic_loop_without_break()
{
  /**
   * A generic loop with a break. Expecting a phi statement in the loop and after the loop.
   */
  ProgramBuilder setup;
  auto intTy = types::IntegerType::create();
  auto cond = types::BooleanType::create();

  setup.addFunction(intTy, { cond }, [&](ProgramBuilder::Arguments) {
    StatementBuilder b;
    auto val = b.declareLocalValue(LiteralInteger::create(intTy,0));
    auto var = b.declareLocalVariable(val);
    auto loopName = Name::create("loop");
    b.appendLoop(loopName, [&](StatementBuilder& bb) {
          auto v = bb.declareLocalValue(LiteralInteger::create(intTy,1));
          v = bb.declareLocalValue(OperatorExpression::create(OperatorExpression::OP_ADD,var,v));
          bb.appendUpdate(var, v);
        });
    // the return statement is unreachable
      b.appendReturn(var);
      return b.build();
    });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_b\") global-scope \"_a\")"
              "(lambda (tag \"lambda_9\")"
              "(parameter (name \"arg_2\") \"_1\")"
              "(block"
              "(val (ref (name \"local_3\") function-scope \"_0\") 0)"
              "(val (ref (name \"local_4\") function-scope \"_0\") (ref (name \"local_3\") function-scope \"_0\"))"
              "(loop (name \"loop_5\")"
              "(block"
              "(val (ref (name \"local_4\"{1}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_4\") function-scope \"_0\") (ref (name \"local_4\"{2}) function-scope \"_0\")))"
              "(val (ref (name \"local_6\") function-scope \"_0\") 1)"
              "(val (ref (name \"local_8\") function-scope \"_7\") (apply-operator add (type \"_7\") (ref (name \"local_4\"{1}) function-scope \"_0\") (ref (name \"local_6\") function-scope \"_0\")))"
              "(val (ref (name \"local_4\"{2}) function-scope \"_0\") (ref (name \"local_8\") function-scope \"_7\"))))"
              "(return (ref (name \"local_4\") function-scope \"_0\")))))"
              ""));
}

void utest_lambda()
{
  /**
   * A generic loop with a break. Expecting a phi statement in the loop and after the loop.
   */
  ProgramBuilder setup;
  auto intTy = types::IntegerType::create();
  auto cond = types::BooleanType::create();

  setup.addFunction(intTy, { cond },
      [&](
          ProgramBuilder::Arguments args) {
            StatementBuilder b;
            auto val1 = b.declareLocalValue(LiteralInteger::create(intTy,1));
            auto val2 = b.declareLocalValue(LiteralInteger::create(intTy,2));
            auto var = b.declareLocalValue(intTy);
            b.appendIf(args.at(0),[&](StatementBuilder& bb) {
                  bb.appendUpdate(var, val1);
                }, [&](StatementBuilder& bb) {
                  bb.appendUpdate(var, val2);
                });

            auto fn = b.declareLocalLambda( {},[&](StatementBuilder::LambdaParameters params) {
                  StatementBuilder bb;
                  auto ret = bb.declareLocalValue(var);
                  bb.appendReturn(ret);
                  return ir::LambdaExpression::create(params, ret->type, bb.build());
                });
            auto ret = b.declareLocalValue(ir::OperatorExpression::create(ir::OperatorExpression::OP_CALL,fn));
            b.appendReturn(ret);
            return b.build();
          });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_d\") global-scope \"_c\")"
              "(lambda (tag \"lambda_b\")"
              "(parameter (name \"arg_2\") \"_1\")"
              "(block"
              "(val (ref (name \"local_3\") function-scope \"_0\") 1)"
              "(val (ref (name \"local_4\") function-scope \"_0\") 2)"
              "(if (ref (name \"arg_2\") function-scope \"_1\")"
              "(block"
              "(val (ref (name \"local_5\"{1}) function-scope \"_0\") (ref (name \"local_3\") function-scope \"_0\")))"
              "(block"
              "(val (ref (name \"local_5\"{2}) function-scope \"_0\") (ref (name \"local_4\") function-scope \"_0\"))))"
              "(val (ref (name \"local_5\"{3}) function-scope \"_0\") (phi (type \"_0\") (ref (name \"local_5\"{1}) function-scope \"_0\") (ref (name \"local_5\"{2}) function-scope \"_0\")))"
              "(val (ref (name \"local_9\") function-scope \"_8\") (lambda (tag \"lambda_7\")"
              "(block"
              "(val (ref (name \"local_6\") function-scope \"_0\") (ref (name \"local_5\"{3}) function-scope \"_0\"))"
              "(return (ref (name \"local_6\") function-scope \"_0\")))))"
              "(val (ref (name \"local_a\") function-scope \"_0\") (apply-operator call (type \"_0\") (ref (name \"local_9\") function-scope \"_8\")))"
              "(return (ref (name \"local_a\") function-scope \"_0\")))))"
              ""));
}
void utest_loop_toSSA()
{
  ProgramBuilder setup;

  setup.addFunction(types::BooleanType::create(), [&]
  {
    Name loopName = Name::create("loop");
    Name loopName1 = Name::create("loop1");
    StatementBuilder b;
    auto TRUE = b.declareLocalValue(LiteralBoolean::create(true));
    auto FALSE = b.declareLocalValue(LiteralBoolean::create(false));
    auto var2 = b.declareLocalVariable(TRUE->type);
    auto out = b.declareLocalVariable(TRUE->type);

    b.appendUpdate(out, FALSE);
    b.appendLoop(loopName1, [&](StatementBuilder &bb) {
      bb.appendIf(TRUE,[&](StatementBuilder& ift) {
            ift.appendBreak(loopName1);
          });
      bb.appendUpdate(out, var2);
      bb.appendBreak(loopName1);
    });

    b.appendReturn(out);
    return b.build();
  });

  auto prg = ToSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(val (ref (name \"fn_b\") global-scope \"_a\")"
              "(lambda (tag \"lambda_9\")"
              "(block"
              "(val (ref (name \"local_4\") function-scope \"_3\") true)"
              "(val (ref (name \"local_6\") function-scope \"_5\") false)"
              "(val (ref (name \"local_7\") function-scope \"_3\") (no-value))"
              "(val (ref (name \"local_8\") function-scope \"_3\") (no-value))"
              "(val (ref (name \"local_8\"{1}) function-scope \"_3\") (ref (name \"local_6\") function-scope \"_5\"))"
              "(loop (name \"loop1_2\")"
              "(block"
              "(val (ref (name \"local_8\"{2}) function-scope \"_3\") (phi (type \"_3\") (ref (name \"local_8\"{1}) function-scope \"_3\") (ref (name \"local_8\"{3}) function-scope \"_3\")))"
              "(if (ref (name \"local_4\") function-scope \"_3\")"
              "(block"
              "(break (name \"loop1_2\")))"
              "(block))"
              "(val (ref (name \"local_8\"{3}) function-scope \"_3\") (ref (name \"local_7\") function-scope \"_3\"))"
              "(break (name \"loop1_2\"))))"
              "(val (ref (name \"local_8\"{4}) function-scope \"_3\") (phi (type \"_3\") (ref (name \"local_8\"{2}) function-scope \"_3\") (ref (name \"local_8\"{3}) function-scope \"_3\")))"
              "(return (ref (name \"local_8\"{4}) function-scope \"_3\")))))"
              ""));
}
