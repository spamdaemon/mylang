#include <mylang/ethir/ssa/SSATestSupport.h>
#include <mylang/ethir/ssa/peepholes/Optionals.h>
#include <mylang/ethir/ssa/peepholes/Arrays.h>
#include <mylang/ethir/ssa/peepholes/Structs.h>
#include <mylang/ethir/ssa/peepholes/Tuples.h>
#include <mylang/ethir/ssa/peepholes/Unions.h>
#include <iostream>

using namespace mylang::ethir;
using namespace mylang::ethir::ir;
using namespace mylang::ethir::types;
using namespace mylang::ethir::ssa;

void utest_map_array_identity()
{
  // apply the identity function to an array
  auto P = Peephole::createMapIdentity();
  auto intTy = IntegerType::create();
  auto arrTy = ArrayType::get(IntegerType::create(), mylang::Interval::NON_NEGATIVE());

  assert(
      SSATestSupport::checkBlock(P, [&](StatementBuilder &b) {
        auto arr = SSATestSupport::newGlobal(arrTy,"arr");
        auto fn = b.declareLocalValue(LambdaExpression::createIdentity(intTy));
        b.declareLocalValue(OperatorExpression::OP_MAP_ARRAY, {arr,fn});

      },
          ""
              "(block"
              "(val (ref (name \"local_b\") function-scope \"_9\") (lambda (tag \"identity_a\")"
              "(parameter (name \"arg_8\") \"_0\")"
              "(block"
              "(return (ref (name \"arg_8\") function-scope \"_0\")))))"
              "(val (ref (name \"local_10\") function-scope \"_f\") (ref (name \"arr_7\") global-scope \"_5\")))"
              ""));
}

void utest_map_opt_identity()
{
  // apply the identity function to an optional
  auto P = Peephole::createMapIdentity();
  auto intTy = IntegerType::create();
  auto optTy = OptType::get(IntegerType::create());

  assert(
      SSATestSupport::checkBlock(P, [&](StatementBuilder &b) {
        auto opt = SSATestSupport::newGlobal(optTy,"opt");
        auto fn = b.declareLocalValue(LambdaExpression::createIdentity(intTy));
        b.declareLocalValue(OperatorExpression::OP_MAP_OPTIONAL, {opt,fn});

      },
          ""
              "(block"
              "(val (ref (name \"local_8\") function-scope \"_6\") (lambda (tag \"identity_7\")"
              "(parameter (name \"arg_5\") \"_0\")"
              "(block"
              "(return (ref (name \"arg_5\") function-scope \"_0\")))))"
              "(val (ref (name \"local_a\") function-scope \"_9\") (ref (name \"opt_4\") global-scope \"_2\")))"
              ""));
}

void utest_call_identity()
{
  // call a function that returns one of its arguments
  auto P = Peephole::createCallWrapper();
  auto intTy = IntegerType::create();

  assert(
      SSATestSupport::checkBlock(P,
          [&](
              StatementBuilder &b) {
                auto arg0 = SSATestSupport::newGlobal(IntegerType::create(),"xxx");
                auto arg1 = SSATestSupport::newGlobal(BooleanType::create(),"yyy");
                auto fn = b.declareLocalLambda( {arg0->type,arg1->type},[&](StatementBuilder& bb, ::std::vector<EVariable> args) {
                      bb.appendReturn(args.at(1));
                      return arg1->type;
                    });
                b.declareLocalValue(OperatorExpression::OP_CALL, {fn, arg0,arg1});
              },
          ""
              "(block"
              "(val (ref (name \"local_a\") function-scope \"_9\") (lambda (tag \"lambda_8\")"
              "(parameter (name \"arg0_6\") \"_2\")"
              "(parameter (name \"arg1_7\") \"_4\")"
              "(block"
              "(return (ref (name \"arg1_7\") function-scope \"_4\")))))"
              "(val (ref (name \"local_b\") function-scope \"_4\") (ref (name \"yyy_5\") global-scope \"_4\")))"
              ""));
}

void utest_call_constant()
{
  // call a function that returns one of its arguments
  auto P = Peephole::createCallWrapper();
  auto intTy = IntegerType::create();

  assert(
      SSATestSupport::checkBlock(P,
          [&](
              StatementBuilder &b) {
                auto arg0 = SSATestSupport::newGlobal(IntegerType::create(),"xxx");
                auto arg1 = SSATestSupport::newGlobal(BooleanType::create(),"yyy");
                auto zzz = SSATestSupport::newGlobal(CharType::create(),"zzz");
                auto fn = b.declareLocalLambda( {arg0->type,arg1->type},[&](StatementBuilder& bb, ::std::vector<EVariable>) {
                      bb.appendReturn(zzz);
                      return zzz->type;
                    });
                b.declareLocalValue(OperatorExpression::OP_CALL, {fn, arg0,arg1});
              },
          ""
              "(block"
              "(val (ref (name \"local_c\") function-scope \"_b\") (lambda (tag \"lambda_a\")"
              "(parameter (name \"arg0_8\") \"_2\")"
              "(parameter (name \"arg1_9\") \"_4\")"
              "(block"
              "(return (ref (name \"zzz_7\") global-scope \"_6\")))))"
              "(val (ref (name \"local_d\") function-scope \"_6\") (ref (name \"zzz_7\") global-scope \"_6\")))"
              ""));
}

void utest_call_optionals()
{
  auto P = peepholes::Optionals::getPeephole();
  auto optTy = OptType::get(IntegerType::create());

  assert(
      SSATestSupport::checkBlock(P,
          [&](
              StatementBuilder &b) {
                auto arg0 = SSATestSupport::newGlobal(optTy->element,"xxx");
                auto mapFN = SSATestSupport::newGlobal(FunctionType::get(PrimitiveType::getBoolean(), {optTy->element}));
                auto optValue = b.declareLocalValue(optTy,OperatorExpression::OP_NEW, {arg0});
                auto nilValue = b.declareLocalValue(optTy,OperatorExpression::OP_NEW, {});
                b.comment("present_new");
                auto is_present = b.declareLocalValue(OperatorExpression::OP_IS_PRESENT, {optValue});
                b.comment("get_new");
                auto value = b.declareLocalValue(OperatorExpression::OP_GET, {optValue});
                b.comment("present_nil");
                auto is_not_present = b.declareLocalValue(OperatorExpression::OP_IS_PRESENT, {nilValue});
                b.comment("get_nil unoptimized");
                auto not_optimzed = b.declareLocalValue(OperatorExpression::OP_GET, {nilValue});
                b.comment("map_nil");
                auto map_nil = b.declareLocalValue(OperatorExpression::OP_MAP_OPTIONAL, {nilValue,mapFN});
                b.comment("map_new");
                auto map_new = b.declareLocalValue(OperatorExpression::OP_MAP_OPTIONAL, {optValue,mapFN});
              },
          ""
              "(block"
              "(val (ref (name \"local_7\") function-scope \"_1\") (apply-operator new (type \"_1\") (ref (name \"xxx_3\") global-scope \"_0\")))"
              "(val (ref (name \"local_8\") function-scope \"_1\") (apply-operator new (type \"_1\")))"
              "(comment \"present_new\")"
              "(val (ref (name \"local_a\") function-scope \"_9\") true)"
              "(comment \"get_new\")"
              "(val (ref (name \"local_b\") function-scope \"_0\") (ref (name \"xxx_3\") global-scope \"_0\"))"
              "(comment \"present_nil\")"
              "(val (ref (name \"local_d\") function-scope \"_c\") false)"
              "(comment \"get_nil unoptimized\")"
              "(val (ref (name \"local_e\") function-scope \"_0\") (apply-operator get (type \"_0\") (ref (name \"local_8\") function-scope \"_1\")))"
              "(comment \"map_nil\")"
              "(val (ref (name \"local_10\") function-scope \"_f\") (apply-operator new (type \"_f\")))"
              "(comment \"map_new\")"
              "(val (ref (name \"let_18\") function-scope \"_4\") (apply-operator call (type \"_4\") (ref (name \"tmp_6\") global-scope \"_5\") (ref (name \"xxx_3\") global-scope \"_0\")))"
              "(val (ref (name \"local_12\") function-scope \"_11\") (apply-operator new (type \"_11\") (ref (name \"let_18\") function-scope \"_4\"))))"
              ""));

}

void utest_call_arrays()
{
  auto P = peepholes::Arrays::getPeephole();
  auto arrTy = ArrayType::get(IntegerType::create(), mylang::Interval::NON_NEGATIVE());

  assert(
      SSATestSupport::checkBlock(P,
          [&](StatementBuilder &b) {
            OperatorExpression::Arguments args;
            for (int i=0;i<11;++i) {
              args.push_back(SSATestSupport::newGlobal(arrTy->element,"xxx"));
            }
            auto array = b.declareLocalValue(arrTy,OperatorExpression::OP_NEW, args);
            // this is optimized
              {
                b.comment("size_new");
                auto size = b.declareLocalValue(OperatorExpression::OP_SIZE, {array});
              }
              // this is optimized
              {
                b.comment("index_new");
                auto i = b.declareLocalValue(LiteralInteger::create(arrTy->indexType.value(),1));
                auto op = b.declareLocalValue(arrTy->element,OperatorExpression::OP_INDEX, {array,i});
              }
              // this is unoptimized
              {
                b.comment("not optimized");
                auto i = b.declareLocalValue(LiteralInteger::create(arrTy->indexType.value(),3));
                auto op = b.declareLocalValue(arrTy->element,OperatorExpression::OP_INDEX, {array,i});
              }
              // this is optimized
              {
                b.comment("subrange_subrange");
                auto jstart = b.declareLocalValue(LiteralInteger::create(arrTy->indexType.value(),1));
                auto jstop = b.declareLocalValue(LiteralInteger::create(arrTy->indexType.value(),8));
                auto subj = b.declareLocalValue(arrTy,OperatorExpression::OP_SUBRANGE, {array,jstart,jstop});
                auto istart = b.declareLocalValue(LiteralInteger::create(arrTy->indexType.value(),2));
                auto istop = b.declareLocalValue(LiteralInteger::create(arrTy->indexType.value(),3));
                auto subi = b.declareLocalValue(arrTy,OperatorExpression::OP_SUBRANGE, {subj,istart,istop});
              }

              // this is optimized
              {
                b.comment("index_subrange");
                auto jstart = b.declareLocalValue(LiteralInteger::create(arrTy->indexType.value(),1));
                auto jstop = b.declareLocalValue(LiteralInteger::create(arrTy->indexType.value(),8));
                auto subj = b.declareLocalValue(arrTy,OperatorExpression::OP_SUBRANGE, {array,jstart,jstop});
                auto i = b.declareLocalValue(LiteralInteger::create(arrTy->indexType.value(),2));
                auto subi = b.declareLocalValue(arrTy->element,OperatorExpression::OP_INDEX, {subj,i});
              }

              // this is optimized
              {
                b.comment("size_subrange");
                auto jstart = b.declareLocalValue(LiteralInteger::create(arrTy->indexType.value(),1));
                auto jstop = b.declareLocalValue(LiteralInteger::create(arrTy->indexType.value(),8));
                auto subj = b.declareLocalValue(arrTy,OperatorExpression::OP_SUBRANGE, {array,jstart,jstop});
                auto subi = b.declareLocalValue(arrTy->lengthType,OperatorExpression::OP_SIZE, {subj});
              }
            },
          ""
              "(block"
              "(val (ref (name \"local_11\") function-scope \"_4\") (apply-operator new (type \"_4\") (ref (name \"xxx_6\") global-scope \"_0\") (ref (name \"xxx_7\") global-scope \"_0\") (ref (name \"xxx_8\") global-scope \"_0\") (ref (name \"xxx_9\") global-scope \"_0\") (ref (name \"xxx_a\") global-scope \"_0\") (ref (name \"xxx_b\") global-scope \"_0\") (ref (name \"xxx_c\") global-scope \"_0\") (ref (name \"xxx_d\") global-scope \"_0\") (ref (name \"xxx_e\") global-scope \"_0\") (ref (name \"xxx_f\") global-scope \"_0\") (ref (name \"xxx_10\") global-scope \"_0\")))"
              "(comment \"size_new\")"
              "(val (ref (name \"local_12\") function-scope \"_1\") 11)"
              "(comment \"index_new\")"
              "(val (ref (name \"local_13\") function-scope \"_2\") 1)"
              "(val (ref (name \"local_14\") function-scope \"_0\") (ref (name \"xxx_7\") global-scope \"_0\"))"
              "(comment \"not optimized\")"
              "(val (ref (name \"local_15\") function-scope \"_2\") 3)"
              "(val (ref (name \"local_16\") function-scope \"_0\") (ref (name \"xxx_9\") global-scope \"_0\"))"
              "(comment \"subrange_subrange\")"
              "(val (ref (name \"local_17\") function-scope \"_2\") 1)"
              "(val (ref (name \"local_18\") function-scope \"_2\") 8)"
              "(val (ref (name \"local_19\") function-scope \"_4\") (apply-operator subrange (type \"_4\") (ref (name \"local_11\") function-scope \"_4\") (ref (name \"local_17\") function-scope \"_2\") (ref (name \"local_18\") function-scope \"_2\")))"
              "(val (ref (name \"local_1a\") function-scope \"_2\") 2)"
              "(val (ref (name \"local_1b\") function-scope \"_2\") 3)"
              "(val (ref (name \"let_29\") function-scope \"_3\") 3)"
              "(val (ref (name \"let_2a\") function-scope \"_3\") 4)"
              "(val (ref (name \"local_1c\") function-scope \"_4\") (apply-operator subrange (type \"_4\") (ref (name \"local_11\") function-scope \"_4\") (ref (name \"let_29\") function-scope \"_3\") (ref (name \"let_2a\") function-scope \"_3\")))"
              "(comment \"index_subrange\")"
              "(val (ref (name \"local_1d\") function-scope \"_2\") 1)"
              "(val (ref (name \"local_1e\") function-scope \"_2\") 8)"
              "(val (ref (name \"local_1f\") function-scope \"_4\") (apply-operator subrange (type \"_4\") (ref (name \"local_11\") function-scope \"_4\") (ref (name \"local_1d\") function-scope \"_2\") (ref (name \"local_1e\") function-scope \"_2\")))"
              "(val (ref (name \"local_20\") function-scope \"_2\") 2)"
              "(val (ref (name \"let_2b\") function-scope \"_2\") 3)"
              "(val (ref (name \"local_21\") function-scope \"_0\") (apply-operator index (type \"_0\") (ref (name \"local_11\") function-scope \"_4\") (ref (name \"let_2b\") function-scope \"_2\")))"
              "(comment \"size_subrange\")"
              "(val (ref (name \"local_22\") function-scope \"_2\") 1)"
              "(val (ref (name \"local_23\") function-scope \"_2\") 8)"
              "(val (ref (name \"local_24\") function-scope \"_4\") (apply-operator subrange (type \"_4\") (ref (name \"local_11\") function-scope \"_4\") (ref (name \"local_22\") function-scope \"_2\") (ref (name \"local_23\") function-scope \"_2\")))"
              "(val (ref (name \"local_25\") function-scope \"_1\") 7))"
              ""));

}

void utest_call_structs()
{
  auto P = peepholes::Structs::getPeephole();

  ::std::vector<StructType::Member> members;
  members.emplace_back("a", PrimitiveType::getBoolean());
  members.emplace_back("b", PrimitiveType::getChar());
  auto sTy = StructType::get(members);

  assert(
      SSATestSupport::checkBlock(P,
          [&](
              StatementBuilder &b) {
                OperatorExpression::Arguments args;
                args.push_back(SSATestSupport::newGlobal(members.at(0).type,"xxx"));
                args.push_back(SSATestSupport::newGlobal(members.at(1).type,"xxx"));
                auto obj = b.declareLocalValue(sTy,OperatorExpression::OP_NEW, args);
                b.comment("getmember_new");
                auto size = b.declareLocalValue(GetMember::create(members.at(1).type, obj, members.at(1).name));
              },
          ""
              "(block"
              "(val (ref (name \"local_6\") function-scope \"_2\") (apply-operator new (type \"_2\") (ref (name \"xxx_4\") global-scope \"_0\") (ref (name \"xxx_5\") global-scope \"_1\")))"
              "(comment \"getmember_new\")"
              "(val (ref (name \"local_7\") function-scope \"_1\") (ref (name \"xxx_5\") global-scope \"_1\")))"
              ""));

}

void utest_call_tuples()
{
  auto P = peepholes::Tuples::getPeephole();

  auto tTy = TupleType::get( { PrimitiveType::getBoolean(), PrimitiveType::getChar() });

  assert(
      SSATestSupport::checkBlock(P, [&](StatementBuilder &b) {
        OperatorExpression::Arguments args;
        args.push_back(SSATestSupport::newGlobal(tTy->types.at(0),"xxx"));
        args.push_back(SSATestSupport::newGlobal(tTy->types.at(1),"xxx"));
        auto obj = b.declareLocalValue(tTy,OperatorExpression::OP_NEW, args);
        b.comment("gettuplemember_new");
        auto size = b.declareLocalValue(GetMember::create(args.at(1)->type,obj,1));
      },
          ""
              "(block"
              "(val (ref (name \"local_6\") function-scope \"_2\") (apply-operator new (type \"_2\") (ref (name \"xxx_4\") global-scope \"_0\") (ref (name \"xxx_5\") global-scope \"_1\")))"
              "(comment \"gettuplemember_new\")"
              "(val (ref (name \"local_7\") function-scope \"_1\") (ref (name \"xxx_5\") global-scope \"_1\")))"
              ""));

}

void utest_call_unions()
{
  auto P = peepholes::Unions::getPeephole();

  ::std::vector<UnionType::Member> members;
  auto discriminantType = IntegerType::create();
  UnionType::Discriminant D("d", discriminantType);

  members.emplace_back(LiteralInteger::create(discriminantType, 17), "a",
      PrimitiveType::getBoolean());
  members.emplace_back(LiteralInteger::create(discriminantType, 31), "b", PrimitiveType::getChar());
  auto uTy = UnionType::get(D, members);

  assert(
      SSATestSupport::checkBlock(P, [&](StatementBuilder &b) {
        OperatorExpression::Arguments args;
        args.push_back(SSATestSupport::newGlobal(members.at(0).type,"xxx"));
        args.push_back(SSATestSupport::newGlobal(members.at(1).type,"xxx"));
        auto obj = b.declareLocalValue(NewUnion::create(uTy,members.at(1).name,args.at(1)));

        {
          b.comment("getmember_new");
          b.declareLocalValue(GetMember::create(members.at(1).type, obj, members.at(1).name));
        }

        {
          b.comment("getdiscriminant_new");
          b.declareLocalValue(GetDiscriminant::create(discriminantType, obj));
        }

      },
          ""
              "(block"
              "(val (ref (name \"local_7\") function-scope \"_3\") (new-union b (ref (name \"xxx_6\") global-scope \"_2\")))"
              "(comment \"getmember_new\")"
              "(val (ref (name \"local_8\") function-scope \"_2\") (ref (name \"xxx_6\") global-scope \"_2\"))"
              "(comment \"getdiscriminant_new\")"
              "(val (ref (name \"local_9\") function-scope \"_0\") 31))"
              ""));

}

