#include <mylang/ethir/ssa/FromSSA.h>
#include <mylang/ethir/TestSupport.h>
#include <iostream>

using namespace mylang::ethir;
using namespace mylang::ethir::ir;
using namespace mylang::ethir::types;
using namespace mylang::ethir::ssa;

static Phi::VariablePtr newVersion(const Phi::VariablePtr &v)
{
  auto name = v->name.newVersion();
  return Variable::create(v->scope, v->type, name);
}

void utest_block_fromSSA()
{
  /**
   * two nested blocks, where a value is defined
   * in a nested block and used in the enclosing block.
   * the variable has version > 0, which can happen
   * during optimization
   */
  ProgramBuilder setup;

  auto intTy = types::IntegerType::create();

  setup.addFunction(intTy, [&]
  {
    StatementBuilder b;

    auto var = newVersion(Variable::create(Variable::Scope::FUNCTION_SCOPE, intTy, Name::create("x")));

    b.appendBlock([&](StatementBuilder &bb) {
      bb.declareValue(var,LiteralInteger::create(intTy,1));
    });
    b.appendReturn(var);
    return b.build();
  });

  auto prg = FromSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(val (ref (name \"fn_4\") global-scope \"_3\")"
              "(lambda (tag \"lambda_2\")"
              "(block"
              "(val (ref (name \"x_1\") function-scope \"_0\"))"
              "(val (ref (name \"from_ssa_5\") function-scope \"_0\") 1)"
              "(update-local-var (ref (name \"x_1\") function-scope \"_0\") (ref (name \"from_ssa_5\") function-scope \"_0\"))"
              "(return (ref (name \"x_1\") function-scope \"_0\")))))"
              ""));
}

void utest_if_fromSSA()
{
  ProgramBuilder setup;

  auto intTy = types::IntegerType::create();

  setup.addFunction(intTy, { types::BooleanType::create() }, [&](ProgramBuilder::Arguments args) {
    StatementBuilder b;

    auto val = b.declareLocalValue(LiteralInteger::create(intTy,0));
    auto var = b.declareLocalValue(val);
    Phi::VariablePtr phi1,phi2,phi3;

    b.appendIf(args.at(0),[&](StatementBuilder& bb) {
          auto v = bb.declareLocalValue(LiteralInteger::create(intTy,1));
          phi1 = bb.declareValue(phi1=newVersion(var),v);
        }, [&](StatementBuilder& bb) {
          auto v = bb.declareLocalValue(LiteralInteger::create(intTy,2));
          phi2 = bb.declareValue(newVersion(var),v);
        });
    phi3= b.declareValue(newVersion(var),Phi::create( {phi1,phi2}));
    b.appendReturn(phi3);
    return b.build();
  });

  auto prg = FromSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_9\") global-scope \"_8\")"
              "(lambda (tag \"lambda_7\")"
              "(parameter (name \"arg_2\") \"_1\")"
              "(block"
              "(val (ref (name \"local_3\") function-scope \"_0\") 0)"
              "(var (ref (name \"local_4\") function-scope \"_0\") (ref (name \"local_3\") function-scope \"_0\"))"
              "(if (ref (name \"arg_2\") function-scope \"_1\")"
              "(block"
              "(val (ref (name \"local_5\") function-scope \"_0\") 1)"
              "(update-local-var (ref (name \"local_4\") function-scope \"_0\") (ref (name \"local_5\") function-scope \"_0\")))"
              "(block"
              "(val (ref (name \"local_6\") function-scope \"_0\") 2)"
              "(update-local-var (ref (name \"local_4\") function-scope \"_0\") (ref (name \"local_6\") function-scope \"_0\"))))"
              "(return (ref (name \"local_4\") function-scope \"_0\")))))"
              ""));
}

void utest_nested_if_fromSSA()
{
  /* test a simple if statement with updates in each branch and use of the variable after the if-statement */
  ProgramBuilder setup;

  auto intTy = types::IntegerType::create();

  setup.addFunction(intTy, { types::BooleanType::create() },
      [&](
          ProgramBuilder::Arguments args) {
            StatementBuilder b;
            auto val = b.declareLocalValue(LiteralInteger::create(intTy,0));

            b.appendIf(args.at(0),[&](StatementBuilder& bb) {
                  auto var = Variable::create(Variable::Scope::FUNCTION_SCOPE, val->type, Name::create("x"));
                  Phi::VariablePtr phi1,phi2,phi3;

                  bb.appendIf(args.at(0),[&](StatementBuilder& bbb) {
                        auto v = bbb.declareLocalValue(LiteralInteger::create(intTy,1));
                        phi1 = bbb.declareValue(phi1=newVersion(var),v);
                      }, [&](StatementBuilder& bbb) {
                        auto v = bbb.declareLocalValue(LiteralInteger::create(intTy,2));
                        phi2 = bbb.declareValue(newVersion(var),v);
                      });
                  phi3= bb.declareValue(newVersion(var),Phi::create( {phi1,phi2}));
                  bb.appendReturn(phi3);
                });
            b.appendReturn(val);
            return b.build();
          });

  auto prg = FromSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_9\") global-scope \"_8\")"
              "(lambda (tag \"lambda_7\")"
              "(parameter (name \"arg_2\") \"_1\")"
              "(block"
              "(val (ref (name \"local_3\") function-scope \"_0\") 0)"
              "(if (ref (name \"arg_2\") function-scope \"_1\")"
              "(block"
              "(val (ref (name \"x_4\") function-scope \"_0\"))"
              "(if (ref (name \"arg_2\") function-scope \"_1\")"
              "(block"
              "(val (ref (name \"local_5\") function-scope \"_0\") 1)"
              "(update-local-var (ref (name \"x_4\") function-scope \"_0\") (ref (name \"local_5\") function-scope \"_0\")))"
              "(block"
              "(val (ref (name \"local_6\") function-scope \"_0\") 2)"
              "(update-local-var (ref (name \"x_4\") function-scope \"_0\") (ref (name \"local_6\") function-scope \"_0\"))))"
              "(return (ref (name \"x_4\") function-scope \"_0\")))"
              "(block))"
              "(return (ref (name \"local_3\") function-scope \"_0\")))))"
              ""));
}

void utest_try_fromSSA_update_in_try()
{
  /* test a simple if statement with updates in each branch and use of the variable after the if-statement */
  ProgramBuilder setup;

  auto intTy = types::IntegerType::create();

  setup.addFunction(intTy, { types::BooleanType::create() },
      [&](
          ProgramBuilder::Arguments args) {
            StatementBuilder b;
            auto val = b.declareLocalValue(LiteralInteger::create(intTy,0));

            b.appendTryCatch([&](StatementBuilder& bb) {
                  auto var = Variable::create(Variable::Scope::FUNCTION_SCOPE, val->type, Name::create("x"));
                  Phi::VariablePtr phi1,phi2,phi3;

                  bb.appendIf(args.at(0),[&](StatementBuilder& bbb) {
                        auto v = bbb.declareLocalValue(LiteralInteger::create(intTy,1));
                        phi1 = bbb.declareValue(phi1=newVersion(var),v);
                      }, [&](StatementBuilder& bbb) {
                        auto v = bbb.declareLocalValue(LiteralInteger::create(intTy,2));
                        phi2 = bbb.declareValue(newVersion(var),v);
                      });
                  phi3= bb.declareValue(newVersion(var),Phi::create( {phi1,phi2}));
                  bb.appendReturn(phi3);
                },[&](StatementBuilder& ) {});
            b.appendReturn(val);
            return b.build();
          });

  auto prg = FromSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(export (ref (name \"fn_9\") global-scope \"_8\")"
              "(lambda (tag \"lambda_7\")"
              "(parameter (name \"arg_2\") \"_1\")"
              "(block"
              "(val (ref (name \"local_3\") function-scope \"_0\") 0)"
              "(try"
              "(block"
              "(val (ref (name \"x_4\") function-scope \"_0\"))"
              "(if (ref (name \"arg_2\") function-scope \"_1\")"
              "(block"
              "(val (ref (name \"local_5\") function-scope \"_0\") 1)"
              "(update-local-var (ref (name \"x_4\") function-scope \"_0\") (ref (name \"local_5\") function-scope \"_0\")))"
              "(block"
              "(val (ref (name \"local_6\") function-scope \"_0\") 2)"
              "(update-local-var (ref (name \"x_4\") function-scope \"_0\") (ref (name \"local_6\") function-scope \"_0\"))))"
              "(return (ref (name \"x_4\") function-scope \"_0\")))"
              "(block))"
              "(return (ref (name \"local_3\") function-scope \"_0\")))))"
              ""));
}

void utest_try_fromSSA()
{
  /**
   * Test a try catch block, where a variable is updated in both blocks
   */
  ProgramBuilder setup;

  auto intTy = types::IntegerType::create();
  auto var = Variable::create(Variable::Scope::FUNCTION_SCOPE, intTy, Name::create("x"));

  setup.addFunction([&]() {
    StatementBuilder b;

    Phi::VariablePtr phi1,phi2,phi3;

    b.appendTryCatch([&](StatementBuilder& bb) {
          auto v = bb.declareLocalValue(LiteralInteger::create(intTy,1));
          phi1 = bb.declareValue(phi1=newVersion(var),v);
        }, [&](StatementBuilder& bb) {
          auto v = bb.declareLocalValue(LiteralInteger::create(intTy,2));
          phi2 = bb.declareValue(newVersion(var),v);
        });
    return b.build();
  });

  auto prg = FromSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(val (ref (name \"fn_7\") global-scope \"_6\")"
              "(lambda (tag \"lambda_5\")"
              "(block"
              "(var (ref (name \"x_1\") function-scope \"_0\"))"
              "(try"
              "(block"
              "(val (ref (name \"local_3\") function-scope \"_0\") 1)"
              "(update-local-var (ref (name \"x_1\") function-scope \"_0\") (ref (name \"local_3\") function-scope \"_0\")))"
              "(block"
              "(val (ref (name \"local_4\") function-scope \"_0\") 2)"
              "(update-local-var (ref (name \"x_1\") function-scope \"_0\") (ref (name \"local_4\") function-scope \"_0\")))))))"
              ""));
}

void utest_multiple_loops()
{
  /**
   * Test a try catch block, where a variable is updated in both blocks
   */
  ProgramBuilder setup;

  auto intTy = types::IntegerType::create();
  auto var = Variable::create(Variable::Scope::FUNCTION_SCOPE, intTy, Name::create("x"));

  setup.addFunction([&]() {
    StatementBuilder b;

    // variable for the phi we need
      Phi::VariablePtr phi1,phi2,phi3;

      auto loopName = Name::create("loop");
      auto loopName1 = Name::create("loop1");
      b.appendLoop(loopName, [&](StatementBuilder &bb) {
            bb.declareValue(phi1=newVersion(var),LiteralInteger::create(intTy,2));
            bb.appendBreak(loopName);
          });
      // we need to introduce the variable in the scope outside the loop, otherwise
      // we loose track of the variables
      b.appendLoop(loopName1,[&](StatementBuilder& bb) {
            bb.declareValue(phi2=newVersion(var),Phi::create(phi1));
            auto tmp = bb.declareLocalValue(phi2);
            bb.declareValue(phi3=newVersion(phi2),tmp);
          });
      return b.build();
    });

  auto prg = FromSSA::transform(setup.build());

  assert(
      TestSupport::diff(prg,
          ""
              "(val (ref (name \"fn_8\") global-scope \"_7\")"
              "(lambda (tag \"lambda_6\")"
              "(block"
              "(var (ref (name \"x_1\") function-scope \"_0\"))"
              "(loop (name \"loop_3\")"
              "(block"
              "(val (ref (name \"from_ssa_9\") function-scope \"_0\") 2)"
              "(update-local-var (ref (name \"x_1\") function-scope \"_0\") (ref (name \"from_ssa_9\") function-scope \"_0\"))"
              "(break (name \"loop_3\"))))"
              "(loop (name \"loop1_4\")"
              "(block"
              "(val (ref (name \"local_5\") function-scope \"_0\") (ref (name \"x_1\") function-scope \"_0\"))"
              "(update-local-var (ref (name \"x_1\") function-scope \"_0\") (ref (name \"local_5\") function-scope \"_0\")))))))"
              ""));
}

