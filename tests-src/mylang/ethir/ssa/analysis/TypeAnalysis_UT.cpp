#include <mylang/ethir/analysis/TypeAnalysis.h>
#include <mylang/ethir/ir/LiteralInteger.h>
#include <mylang/ethir/ir/OperatorExpression.h>
#include <mylang/ethir/ir/Program.h>
#include <mylang/ethir/ir/StatementBuilder.h>
#include <mylang/ethir/ir/Variable.h>
#include <mylang/ethir/limits/BooleanLimits.h>
#include <mylang/ethir/limits/IntegerLimits.h>
#include <mylang/ethir/limits/Limits.h>
#include <mylang/ethir/Name.h>
#include <mylang/ethir/prettyPrint.h>
#include <mylang/ethir/ProgramBuilder.h>
#include <mylang/ethir/ssa/SSAProgram.h>
#include <mylang/ethir/ssa/MaintainSSAPass.h>
#include <mylang/ethir/ssa/PruneUnreachableCode.h>
#include <mylang/ethir/ssa/PrunePass.h>
#include <mylang/ethir/types/IntegerType.h>
#include <iostream>
#include <memory>
#include <utility>

using namespace mylang::ethir;
using namespace mylang::ethir::analysis;
using namespace mylang::ethir::ir;
using namespace mylang::ethir::types;
using namespace mylang::ethir::ssa;

void utest_basic_type_analysis()
{
  ProgramBuilder setup;

  setup.addFunction([&]
  {
    Name loop1 = Name::create("loop1");
    Name loop2 = Name::create("loop2");
    StatementBuilder b;
    auto zero = b.declareLocalValue(LiteralInteger::create(IntegerType::create(), 0));
    auto one = b.declareLocalValue(LiteralInteger::create(1));
    auto counter = b.declareLocalVariable(Name::create("counter"), zero);
    auto N = b.declareLocalValue(LiteralInteger::create(IntegerType::create(), 10));
    auto sum = b.declareLocalVariable(Name::create("sum"), zero);

    b.appendLoop(loop1, [&](StatementBuilder &b1) {
      auto cond = b1.declareLocalValue(OperatorExpression::OP_LT, {counter,N});
      b1.appendIf(cond, [&](StatementBuilder& b2) {
            auto next_sum = b2.declareLocalValue(OperatorExpression::OP_ADD, {counter,sum});
            auto next = b2.declareLocalValue(OperatorExpression::OP_ADD, {counter,one});
            b2.appendUpdate(counter, next);
            b2.appendUpdate(sum, next_sum);
          }, [&](StatementBuilder& b3) {
            b3.appendBreak(loop1);
          });
    });
    b.appendReturn(sum);
    return b.build();
  });

  auto ssaPrg = setup.buildSSA();
  PruneUnreachableCode().transformProgram(*ssaPrg);
  MaintainSSAPass().transformProgram(*ssaPrg);
  auto prg = ssaPrg->getSSAForm();

  prettyPrint(prg, ::std::cout);

  auto types = TypeAnalysis::get(prg);

  ::std::cerr << "Number of changed variables " << types.size() << ::std::endl;
}
