#include <mylang/ethir/ssa/ConstFoldPass.h>
#include <mylang/ethir/TestSupport.h>
#include <mylang/ethir/ssa/SSATestSupport.h>
#include <iostream>

using namespace mylang::ethir;
using namespace mylang::ethir::ir;
using namespace mylang::ethir::types;
using namespace mylang::ethir::ssa;

static Phi::VariablePtr newVersion(const Phi::VariablePtr &v)
{
  auto name = v->name.newVersion();
  return Variable::create(v->scope, v->type, name);
}

void utest_constfold_if_false_condition()
{
  ProgramBuilder setup;

  auto intTy = types::IntegerType::create();

  setup.addFunction(intTy, [&]
  {
    StatementBuilder b;

    auto val = b.declareLocalValue(LiteralBoolean::create(false));
    auto var = Variable::create(Variable::Scope::FUNCTION_SCOPE, types::IntegerType::create(), Name::create("tmp"));

    Phi::VariablePtr phi1, phi2, phi3;

    b.appendIf(val, [&](StatementBuilder &bb) {
      auto v = bb.declareLocalValue(LiteralInteger::create(intTy,1));
      phi1 = bb.declareValue(phi1=newVersion(var),v);
      bb.appendAbort("error");
    }, [&](StatementBuilder &bb) {
      auto v = bb.declareLocalValue(LiteralInteger::create(intTy,2));
      phi2 = bb.declareValue(newVersion(var),v);
    });
    phi3 = b.declareValue(newVersion(var), Phi::create( { phi1, phi2 }));
    b.appendReturn(phi3);
    return b.build();
  });
  ConstFoldPass pass;
  auto ssa = setup.buildSSA();
  pass.transformProgram(*ssa);

  assert(
      TestSupport::diff(ssa->getSSAForm(),
          ""
              "(val (ref (name \"fn_e\") global-scope \"_d\")"
              "(lambda (tag \"lambda_c\")"
              "(block"
              "(val (ref (name \"local_2\") function-scope \"_1\") (ref (name \"literal_f\") global-scope \"_1\"))"
              "(val (ref (name \"local_b\") function-scope \"_0\") (ref (name \"literal_10\") global-scope \"_0\"))"
              "(val (ref (name \"tmp_3\"{2}) function-scope \"_4\") (ref (name \"literal_10\") global-scope \"_0\"))"
              "(val (ref (name \"tmp_3\"{3}) function-scope \"_4\") (ref (name \"tmp_3\"{2}) function-scope \"_4\"))"
              "(return (ref (name \"tmp_3\"{3}) function-scope \"_4\")))))"
              "(val (ref (name \"literal_f\") global-scope \"_1\")"
              "false)"
              "(val (ref (name \"literal_10\") global-scope \"_0\")"
              "2)"
              ""));
}

void utest_constfold_if_true_condition()
{
  ProgramBuilder setup;

  auto intTy = types::IntegerType::create();

  setup.addFunction(intTy, [&]
  {
    StatementBuilder b;

    auto val = b.declareLocalValue(LiteralBoolean::create(true));
    auto var = Variable::create(Variable::Scope::FUNCTION_SCOPE, types::IntegerType::create(), Name::create("tmp"));

    Phi::VariablePtr phi1, phi2, phi3;

    b.appendIf(val, [&](StatementBuilder &bb) {
      auto v = bb.declareLocalValue(LiteralInteger::create(intTy,1));
      phi1 = bb.declareValue(phi1=newVersion(var),v);
      bb.appendAbort("error");
    }, [&](StatementBuilder &bb) {
      auto v = bb.declareLocalValue(LiteralInteger::create(intTy,2));
      phi2 = bb.declareValue(newVersion(var),v);
    });
    phi3 = b.declareValue(newVersion(var), Phi::create( { phi1, phi2 }));
    b.appendReturn(phi3);
    return b.build();
  });
  ConstFoldPass pass;
  auto ssa = setup.buildSSA();
  pass.transformProgram(*ssa);

  assert(
      TestSupport::diff(ssa->getSSAForm(),
          ""
              "(val (ref (name \"fn_e\") global-scope \"_d\")"
              "(lambda (tag \"lambda_c\")"
              "(block"
              "(val (ref (name \"local_2\") function-scope \"_1\") (ref (name \"literal_f\") global-scope \"_1\"))"
              "(val (ref (name \"local_5\") function-scope \"_0\") (ref (name \"literal_10\") global-scope \"_0\"))"
              "(val (ref (name \"tmp_3\"{1}) function-scope \"_4\") (ref (name \"literal_10\") global-scope \"_0\"))"
              "(val (ref (name \"local_a\") function-scope \"_6\") (ref (name \"literal_11\") global-scope \"_6\"))"
              "(abort (ref (name \"literal_11\") global-scope \"_6\")))))"
              "(val (ref (name \"literal_f\") global-scope \"_1\")"
              "true)"
              "(val (ref (name \"literal_10\") global-scope \"_0\")"
              "1)"
              "(val (ref (name \"literal_11\") global-scope \"_6\")"
              "\"error\")"
              ""));
}

void utest_constfold_new_optional()
{
  ProgramBuilder setup;

  auto ity = types::IntegerType::create(0, 10);
  auto optTy = types::OptType::get(ity);

  setup.addFunction(optTy, [&]
  {
    StatementBuilder b;

    auto lit = b.declareLocalValue(LiteralInteger::create(ity, 1));
    auto opt = b.declareLocalValue(OperatorExpression::create(optTy, OperatorExpression::OP_NEW, lit));
    b.appendReturn(opt);
    return b.build();
  });
  ConstFoldPass pass;
  auto ssa = setup.buildSSA();
  pass.transformProgram(*ssa);

  assert(
      TestSupport::diff(ssa->getSSAForm(),
          ""
              "(val (ref (name \"fn_6\") global-scope \"_5\")"
              "(lambda (tag \"lambda_4\")"
              "(block"
              "(val (ref (name \"local_2\") function-scope \"_0\") (ref (name \"literal_7\") global-scope \"_0\"))"
              "(val (ref (name \"local_3\") function-scope \"_1\") (ref (name \"literal_8\") global-scope \"_1\"))"
              "(return (ref (name \"literal_8\") global-scope \"_1\")))))"
              "(val (ref (name \"literal_7\") global-scope \"_0\")"
              "1)"
              "(val (ref (name \"literal_8\") global-scope \"_1\")"
              "(literal-optional \"_1\" 1))"
              ""));
}

void utest_constfold_to_string()
{
  ProgramBuilder setup;

  setup.addFunction([&]
  {
    StatementBuilder b;

    auto bit_val_on = b.declareLocalValue(LiteralBit::create(true));
    auto bit_val_off = b.declareLocalValue(LiteralBit::create(false));
    auto bool_val_true = b.declareLocalValue(LiteralBoolean::create(true));
    auto bool_val_false = b.declareLocalValue(LiteralBoolean::create(false));
    auto byte_val_0x80 = b.declareLocalValue(LiteralByte::create(0x80));
    auto char_val_a = b.declareLocalValue(LiteralChar::create('a'));
    auto int_val_minus_one = b.declareLocalValue(LiteralInteger::create(-1));
    auto int_val_limit = b.declareLocalValue(LiteralInteger::create(mylang::BigInt::parse("18446744073709551616")));
    auto real_val_1_1 = b.declareLocalValue(LiteralReal::create(1.1));
    auto str_val = b.declareLocalValue(LiteralString::create("Hello, World!"));

    b.declareLocalValue(OperatorExpression::create(OperatorExpression::OP_TO_STRING, bit_val_on));
    b.declareLocalValue(OperatorExpression::create(OperatorExpression::OP_TO_STRING, bit_val_off));
    b.declareLocalValue(
        OperatorExpression::create(OperatorExpression::OP_TO_STRING, bool_val_true));
    b.declareLocalValue(
        OperatorExpression::create(OperatorExpression::OP_TO_STRING, bool_val_false));
    b.declareLocalValue(
        OperatorExpression::create(OperatorExpression::OP_TO_STRING, byte_val_0x80));
    b.declareLocalValue(OperatorExpression::create(OperatorExpression::OP_TO_STRING, char_val_a));
    b.declareLocalValue(
        OperatorExpression::create(OperatorExpression::OP_TO_STRING, int_val_minus_one));
    b.declareLocalValue(
        OperatorExpression::create(OperatorExpression::OP_TO_STRING, int_val_limit));
    b.declareLocalValue(OperatorExpression::create(OperatorExpression::OP_TO_STRING, real_val_1_1));
    b.declareLocalValue(OperatorExpression::create(OperatorExpression::OP_TO_STRING, str_val));

    return b.build();
  });
  ConstFoldPass pass;
  auto ssa = setup.buildSSA();
  pass.transformProgram(*ssa);

  assert(
      TestSupport::diff(ssa->getSSAForm(),
          ""
              "(val (ref (name \"fn_4c\") global-scope \"_4b\")"
              "(lambda (tag \"lambda_4a\")"
              "(block"
              "(val (ref (name \"local_2\") function-scope \"_1\") (ref (name \"literal_4d\") global-scope \"_1\"))"
              "(val (ref (name \"local_4\") function-scope \"_3\") (ref (name \"literal_4e\") global-scope \"_3\"))"
              "(val (ref (name \"local_6\") function-scope \"_5\") (ref (name \"literal_4f\") global-scope \"_5\"))"
              "(val (ref (name \"local_8\") function-scope \"_7\") (ref (name \"literal_50\") global-scope \"_7\"))"
              "(val (ref (name \"local_a\") function-scope \"_9\") (ref (name \"literal_51\") global-scope \"_9\"))"
              "(val (ref (name \"local_c\") function-scope \"_b\") (ref (name \"literal_52\") global-scope \"_b\"))"
              "(val (ref (name \"local_e\") function-scope \"_d\") (ref (name \"literal_53\") global-scope \"_d\"))"
              "(val (ref (name \"local_10\") function-scope \"_f\") (ref (name \"literal_54\") global-scope \"_f\"))"
              "(val (ref (name \"local_12\") function-scope \"_11\") (ref (name \"literal_55\") global-scope \"_11\"))"
              "(val (ref (name \"local_17\") function-scope \"_13\") (ref (name \"literal_56\") global-scope \"_13\"))"
              "(val (ref (name \"local_1c\") function-scope \"_18\") (ref (name \"literal_5b\") global-scope \"_57\"))"
              "(val (ref (name \"local_21\") function-scope \"_1d\") (ref (name \"literal_60\") global-scope \"_5c\"))"
              "(val (ref (name \"local_26\") function-scope \"_22\") (ref (name \"literal_65\") global-scope \"_61\"))"
              "(val (ref (name \"local_2b\") function-scope \"_27\") (ref (name \"literal_6a\") global-scope \"_66\"))"
              "(val (ref (name \"local_30\") function-scope \"_2c\") (ref (name \"literal_6f\") global-scope \"_6b\"))"
              "(val (ref (name \"local_35\") function-scope \"_31\") (ref (name \"literal_74\") global-scope \"_70\"))"
              "(val (ref (name \"local_3a\") function-scope \"_36\") (ref (name \"literal_79\") global-scope \"_75\"))"
              "(val (ref (name \"local_3f\") function-scope \"_3b\") (ref (name \"literal_7e\") global-scope \"_7a\"))"
              "(val (ref (name \"local_44\") function-scope \"_40\") (ref (name \"literal_83\") global-scope \"_7f\"))"
              "(val (ref (name \"local_49\") function-scope \"_45\") (ref (name \"literal_56\") global-scope \"_13\")))))"
              "(val (ref (name \"literal_4d\") global-scope \"_1\")"
              "1)"
              "(val (ref (name \"literal_4e\") global-scope \"_3\")"
              "0)"
              "(val (ref (name \"literal_4f\") global-scope \"_5\")"
              "true)"
              "(val (ref (name \"literal_50\") global-scope \"_7\")"
              "false)"
              "(val (ref (name \"literal_51\") global-scope \"_9\")"
              "128)"
              "(val (ref (name \"literal_52\") global-scope \"_b\")"
              "'a')"
              "(val (ref (name \"literal_53\") global-scope \"_d\")"
              "-1)"
              "(val (ref (name \"literal_54\") global-scope \"_f\")"
              "18446744073709551616)"
              "(val (ref (name \"literal_55\") global-scope \"_11\")"
              "0x1.199999999999ap+0)"
              "(val (ref (name \"literal_56\") global-scope \"_13\")"
              "\"Hello, World!\")"
              "(val (ref (name \"literal_5b\") global-scope \"_57\")"
              "\"49\")"
              "(val (ref (name \"literal_60\") global-scope \"_5c\")"
              "\"48\")"
              "(val (ref (name \"literal_65\") global-scope \"_61\")"
              "\"true\")"
              "(val (ref (name \"literal_6a\") global-scope \"_66\")"
              "\"false\")"
              "(val (ref (name \"literal_6f\") global-scope \"_6b\")"
              "\"80\")"
              "(val (ref (name \"literal_74\") global-scope \"_70\")"
              "\"a\")"
              "(val (ref (name \"literal_79\") global-scope \"_75\")"
              "\"-1\")"
              "(val (ref (name \"literal_7e\") global-scope \"_7a\")"
              "\"18446744073709551616\")"
              "(val (ref (name \"literal_83\") global-scope \"_7f\")"
              "\"0x1.199999999999ap+0\")"
              ""));
}

void utest_string_to_chars()
{
  ProgramBuilder setup;

  setup.addFunction([&]
  {
    StatementBuilder b;

    auto str_val = b.declareLocalValue(LiteralString::create("Hello, World!"));
    b.declareLocalValue(OperatorExpression::create(OperatorExpression::OP_STRING_TO_CHARS, str_val));

    return b.build();
  });
  ConstFoldPass pass;
  auto ssa = setup.buildSSA();
  pass.transformProgram(*ssa);

  assert(
      TestSupport::diff(ssa->getSSAForm(),
          ""
              "(val (ref (name \"fn_d\") global-scope \"_c\")"
              "(lambda (tag \"lambda_b\")"
              "(block"
              "(val (ref (name \"local_5\") function-scope \"_1\") (ref (name \"literal_e\") global-scope \"_1\"))"
              "(val (ref (name \"local_a\") function-scope \"_9\") (ref (name \"literal_f\") global-scope \"_9\")))))"
              "(val (ref (name \"literal_e\") global-scope \"_1\")"
              "\"Hello, World!\")"
              "(val (ref (name \"literal_f\") global-scope \"_9\")"
              "(literal-array \"_9\" 'H' 'e' 'l' 'l' 'o' ',' ' ' 'W' 'o' 'r' 'l' 'd' '!'))"
              ""));
}

void utest_clamp()
{
  ProgramBuilder setup;

  setup.addFunction([&]
  {
    StatementBuilder b;

    auto upper = b.declareLocalValue(LiteralInteger::create(10));
    auto lower = b.declareLocalValue(LiteralInteger::create(-10));
    auto ity = IntegerType::create(-1, 1);

    auto v1 = b.declareLocalValue(OperatorExpression::create(ity, OperatorExpression::OP_CLAMP, upper));
    auto v2 = b.declareLocalValue(
        OperatorExpression::create(ity, OperatorExpression::OP_CLAMP, lower));

    auto res = b.declareLocalValue(OperatorExpression::create(OperatorExpression::OP_EQ, v1, v2));
    b.appendReturn(res);
    return b.build();
  });

  ConstFoldPass pass;
  auto ssa = setup.buildSSA();
  pass.transformProgram(*ssa);

  assert(
      TestSupport::diff(ssa->getSSAForm(),
          ""
              "(val (ref (name \"fn_c\") global-scope \"_b\")"
              "(lambda (tag \"lambda_a\")"
              "(block"
              "(val (ref (name \"local_2\") function-scope \"_1\") (ref (name \"literal_d\") global-scope \"_1\"))"
              "(val (ref (name \"local_4\") function-scope \"_3\") (ref (name \"literal_e\") global-scope \"_3\"))"
              "(val (ref (name \"local_6\") function-scope \"_5\") (ref (name \"literal_f\") global-scope \"_5\"))"
              "(val (ref (name \"local_7\") function-scope \"_5\") (ref (name \"literal_10\") global-scope \"_5\"))"
              "(val (ref (name \"local_9\") function-scope \"_8\") (ref (name \"literal_12\") global-scope \"_11\"))"
              "(return (ref (name \"literal_12\") global-scope \"_11\")))))"
              "(val (ref (name \"literal_d\") global-scope \"_1\")"
              "10)"
              "(val (ref (name \"literal_e\") global-scope \"_3\")"
              "-10)"
              "(val (ref (name \"literal_f\") global-scope \"_5\")"
              "1)"
              "(val (ref (name \"literal_10\") global-scope \"_5\")"
              "-1)"
              "(val (ref (name \"literal_12\") global-scope \"_11\")"
              "false)"
              ""));
}

void utest_wrap()
{
  ProgramBuilder setup;

  setup.addFunction([&]
  {
    StatementBuilder b;

    auto upper = b.declareLocalValue(LiteralInteger::create(12));
    auto lower = b.declareLocalValue(LiteralInteger::create(-9));
    auto ity = IntegerType::create(2, 8);

    // v1,v2 ==> 6
    auto v1 = b.declareLocalValue(OperatorExpression::create(ity, OperatorExpression::OP_WRAP, upper));
    auto v2 = b.declareLocalValue(
        OperatorExpression::create(ity, OperatorExpression::OP_WRAP, lower));

    auto res = b.declareLocalValue(OperatorExpression::create(OperatorExpression::OP_EQ, v1, v2));
    b.appendReturn(res);
    return b.build();
  });

  ConstFoldPass pass;
  auto ssa = setup.buildSSA();
  pass.transformProgram(*ssa);

  assert(
      TestSupport::diff(ssa->getSSAForm(),
          ""
              "(val (ref (name \"fn_c\") global-scope \"_b\")"
              "(lambda (tag \"lambda_a\")"
              "(block"
              "(val (ref (name \"local_2\") function-scope \"_1\") (ref (name \"literal_d\") global-scope \"_1\"))"
              "(val (ref (name \"local_4\") function-scope \"_3\") (ref (name \"literal_e\") global-scope \"_3\"))"
              "(val (ref (name \"local_6\") function-scope \"_5\") (ref (name \"literal_f\") global-scope \"_5\"))"
              "(val (ref (name \"local_7\") function-scope \"_5\") (ref (name \"literal_f\") global-scope \"_5\"))"
              "(val (ref (name \"local_9\") function-scope \"_8\") (ref (name \"literal_11\") global-scope \"_10\"))"
              "(return (ref (name \"literal_11\") global-scope \"_10\")))))"
              "(val (ref (name \"literal_d\") global-scope \"_1\")"
              "12)"
              "(val (ref (name \"literal_e\") global-scope \"_3\")"
              "-9)"
              "(val (ref (name \"literal_f\") global-scope \"_5\")"
              "5)"
              "(val (ref (name \"literal_11\") global-scope \"_10\")"
              "true)"
              ""));
}

void utest_propagate_if_condition()
{
  auto intTy = IntegerType::create();
  auto optTy = OptType::get(intTy);
  ConstFoldPass P;

  assert(
      SSATestSupport::checkBlock(P, [&](StatementBuilder &b) {
        auto opt = SSATestSupport::newGlobal(optTy,"opt");
        auto c1 = b.declareLocalValue(OperatorExpression::OP_IS_PRESENT, {opt});
        b.appendIf(c1,[&](StatementBuilder &t) {
              auto c2 = t.declareLocalValue(c1);
            },[&](StatementBuilder &f) {
              auto c2 = f.declareLocalValue(c1);
            });
      },
          ""
              "(block"
              "(val (ref (name \"local_5\") function-scope \"_4\") (apply-operator is-present (type \"_4\") (ref (name \"opt_3\") global-scope \"_1\")))"
              "(if (ref (name \"local_5\") function-scope \"_4\")"
              "(block"
              "(val (ref (name \"local_6\") function-scope \"_4\") (ref (name \"literal_c\") global-scope \"_b\")))"
              "(block"
              "(val (ref (name \"local_7\") function-scope \"_4\") (ref (name \"literal_e\") global-scope \"_d\")))))"
              ""));
}

