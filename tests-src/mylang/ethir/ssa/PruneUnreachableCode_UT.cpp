#include <mylang/ethir/ssa/SSATestSupport.h>
#include <mylang/ethir/ssa/PruneUnreachableCode.h>
#include <mylang/ethir/types/types.h>
#include <iostream>

using namespace mylang;
using namespace mylang::ethir;
using namespace mylang::ethir::ir;
using namespace mylang::ethir::types;
using namespace mylang::ethir::ssa;

void utest_prune_unreachable_code()
{
  // apply the identity function to an array
  PruneUnreachableCode P;

  auto intTy = IntegerType::create();
  auto arrTy = ArrayType::get(IntegerType::create(), Interval::NON_NEGATIVE());

  assert(SSATestSupport::checkBlock(P, [&](StatementBuilder &b0) {
    b0.appendBlock([&](StatementBuilder& b1) {
          b1.appendBlock([&](StatementBuilder& b2) {
                Name loop = Name::create("loop");
                auto brk = BreakStatement::create(loop);
                b2.appendLoop(loop, [&](StatementBuilder& b3) {
                      b3.appendBlock([&](StatementBuilder& b4) {
                            auto cond = b4.declareLocalValue(LiteralBoolean::create(true));
                            b4.appendIf(cond,brk,brk);
                          });
                      b3.appendBreak(loop);
                    });

              });
        });
  }, ""
      "(block"
      "(loop (name \"loop_7\")"
      "(block"
      "(val (ref (name \"local_9\") function-scope \"_8\") true)"
      "(if (ref (name \"local_9\") function-scope \"_8\")"
      "(block"
      "(break (name \"loop_7\")))"
      "(block"
      "(break (name \"loop_7\")))))))"
      ""));
}

void utest_prune_code_after_infinite_loop()
{
  // apply the identity function to an array
  PruneUnreachableCode P;

  auto intTy = IntegerType::create();
  auto arrTy = ArrayType::get(IntegerType::create(), Interval::NON_NEGATIVE());

  assert(SSATestSupport::checkBlock(P, [&](StatementBuilder &b0) {
    b0.appendBlock([&](StatementBuilder& b1) {
          b1.appendBlock([&](StatementBuilder& b2) {
                Name loop = Name::create("loop");
                b2.appendLoop(loop, [&](StatementBuilder&) {});
                b2.declareLocalValue(LiteralBoolean::create(true));
              });
          b1.declareLocalValue(LiteralBoolean::create(true));
        });
    b0.declareLocalValue(LiteralBoolean::create(true));
  }, ""
      "(block"
      "(loop (name \"loop_7\")"
      "(block)))"
      ""));
}

void utest_prune_code_with_local_lambda()
{
  // apply the identity function to an array
  PruneUnreachableCode P;

  auto intTy = IntegerType::create();
  auto arrTy = ArrayType::get(IntegerType::create(), Interval::NON_NEGATIVE());

  assert(
      SSATestSupport::checkBlock(P,
          [&](
              StatementBuilder &b0) {
                auto v0 = b0.declareLocalValue(LiteralInteger::create(0));
                auto cond = b0.declareLocalValue(LiteralBoolean::create(true));
                b0.appendIf(cond, [&] (StatementBuilder& t) {
                      t.appendBlock([&](StatementBuilder& b1) {
                            EVariable v1;
                            b1.appendBlock([&](StatementBuilder& b2) {
                                  b2.declareLocalLambda( {},[&](StatementBuilder& b3, ::std::vector<EVariable>) {
                                        b3.appendReturn(v0);
                                        return v0->type;
                                      });
                                  v1=b2.declareLocalValue(v0);
                                });
                            b1.declareLocalValue(v1);
                          });
                    },[&] (StatementBuilder& f) {
                      f.appendBlock(EStatement(nullptr));
                    });
              },
          ""
              "(block"
              "(val (ref (name \"local_8\") function-scope \"_7\") 0)"
              "(val (ref (name \"local_a\") function-scope \"_9\") true)"
              "(if (ref (name \"local_a\") function-scope \"_9\")"
              "(block"
              "(val (ref (name \"local_d\") function-scope \"_c\") (lambda (tag \"lambda_b\")"
              "(block"
              "(return (ref (name \"local_8\") function-scope \"_7\")))))"
              "(val (ref (name \"local_e\") function-scope \"_7\") (ref (name \"local_8\") function-scope \"_7\"))"
              "(val (ref (name \"local_f\") function-scope \"_7\") (ref (name \"local_e\") function-scope \"_7\")))"
              "(block)))"
              ""));
}

