#include <mylang/ethir/ssa/SSATestSupport.h>
#include <mylang/ethir/ssa/CSEPass.h>
#include <mylang/ethir/types/types.h>
#include <iostream>

using namespace mylang::ethir;
using namespace mylang::ethir::ir;
using namespace mylang::ethir::types;
using namespace mylang::ethir::ssa;

void utest_cse_without_sideeffect()
{
  // apply the identity function to an array
  CSEPass P(false);

  auto intTy = IntegerType::create();
  auto arrTy = ArrayType::get(IntegerType::create(), mylang::Interval::NON_NEGATIVE());

  assert(
      SSATestSupport::checkBlock(P, [&](StatementBuilder &b) {
        auto arr = SSATestSupport::newGlobal(arrTy,"arr");
        auto lhs=b.declareLocalValue(OperatorExpression::OP_SIZE, {arr});
        auto rhs=b.declareLocalValue(OperatorExpression::OP_SIZE, {arr});
        auto sum = b.declareLocalValue(OperatorExpression::OP_ADD, {lhs,rhs});
      },
          ""
              "(block"
              "(val (ref (name \"local_8\") function-scope \"_2\") (apply-operator length (type \"_2\") (ref (name \"arr_7\") global-scope \"_5\")))"
              "(val (ref (name \"local_9\") function-scope \"_2\") (ref (name \"local_8\") function-scope \"_2\"))"
              "(val (ref (name \"local_b\") function-scope \"_a\") (apply-operator add (type \"_a\") (ref (name \"local_8\") function-scope \"_2\") (ref (name \"local_9\") function-scope \"_2\"))))"
              ""));
}

void utest_cse_with_sideeffect()
{
  // apply the identity function to an array
  CSEPass P(false);

  auto intTy = IntegerType::create();

  assert(
      SSATestSupport::checkBlock(P, [&](StatementBuilder &b) {
        auto port = SSATestSupport::newGlobal(InputType::get(intTy),"in");
        auto lhs=b.declareLocalValue(OperatorExpression::OP_IS_PORT_READABLE, {port});
        auto rhs=b.declareLocalValue(OperatorExpression::OP_IS_PORT_READABLE, {port});
        auto readadble = b.declareLocalValue(OperatorExpression::OP_OR, {lhs,rhs});
      },
          ""
              "(block"
              "(val (ref (name \"local_5\") function-scope \"_4\") (apply-operator is-readable (type \"_4\") (ref (name \"in_3\") global-scope \"_2\")))"
              "(val (ref (name \"local_7\") function-scope \"_6\") (apply-operator is-readable (type \"_6\") (ref (name \"in_3\") global-scope \"_2\")))"
              "(val (ref (name \"local_8\") function-scope \"_4\") (apply-operator or (type \"_4\") (ref (name \"local_5\") function-scope \"_4\") (ref (name \"local_7\") function-scope \"_6\"))))"
              ""));
}

void utest_nested_if()
{
  // apply the identity function to an array
  CSEPass P(false);

  auto intTy = IntegerType::create();
  auto optTy = OptType::get(intTy);

  assert(
      SSATestSupport::checkBlock(P, [&](StatementBuilder &b) {
        auto opt = SSATestSupport::newGlobal(optTy,"opt");
        auto c1 = b.declareLocalValue(OperatorExpression::OP_IS_PRESENT, {opt});
        b.appendIf(c1,[&](StatementBuilder &t) {
              auto c2 = t.declareLocalValue(OperatorExpression::OP_IS_PRESENT, {opt});
            },[&](StatementBuilder &f) {
              auto c2 = f.declareLocalValue(OperatorExpression::OP_IS_PRESENT, {opt});
            });
      },
          ""
              "(block"
              "(val (ref (name \"local_5\") function-scope \"_4\") (apply-operator is-present (type \"_4\") (ref (name \"opt_3\") global-scope \"_1\")))"
              "(if (ref (name \"local_5\") function-scope \"_4\")"
              "(block"
              "(val (ref (name \"local_7\") function-scope \"_6\") (ref (name \"local_5\") function-scope \"_6\")))"
              "(block"
              "(val (ref (name \"local_9\") function-scope \"_8\") (ref (name \"local_5\") function-scope \"_8\")))))"
              ""));
}

