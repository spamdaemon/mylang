#include <mylang/ethir/ethir.h>
#include <mylang/ethir/ir/Declaration.h>
#include <mylang/ethir/ir/LiteralInteger.h>
#include <mylang/ethir/ir/Loop.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/ir/OperatorExpression.h>
#include <mylang/ethir/ir/SkipStatement.h>
#include <mylang/ethir/ir/StatementBuilder.h>
#include <mylang/ethir/ir/Variable.h>
#include <mylang/ethir/ir/YieldStatement.h>
#include <mylang/ethir/Name.h>
#include <mylang/ethir/prettyPrint.h>
#include <mylang/ethir/types/ArrayType.h>
#include <mylang/ethir/types/IntegerType.h>
#include <mylang/ethir/types/types.h>
#include <mylang/ethir/prettyPrint.h>
#include <iostream>
#include <memory>
#include <utility>

using namespace mylang::ethir;
using namespace mylang::ethir::ir;
using namespace mylang::ethir::types;

void xuxtest_empty_loop()
{
  Loop::Expressions inits;

  auto iTy = IntegerType::create();
  auto arrTy = ArrayType::get(iTy, 0);

  auto array = Variable::createLocal(arrTy);

  auto theLoop = Loop::create(IterateArrayStep::createDefault(false, array));

  ::std::cerr << "The Loop expression" << ::std::endl;
  prettyPrint(theLoop, ::std::cerr);
  ::std::cerr << ::std::endl;
  StatementBuilder b;

  theLoop->toStatements([&](EVariable v, StatementBuilder &sb1) {
    auto str = sb1.declareLocalValue(OperatorExpression::OP_TO_STRING, {v});
    sb1.declareLocalValue(OperatorExpression::OP_LOG, {str});
  }, b);
  auto loopStmt = b.build();

  ::std::cerr << "The Loop expression as a statement" << ::std::endl;
  prettyPrint(loopStmt, ::std::cerr);
  ::std::cerr << ::std::endl;

}
void utest_loop_with_a_single_statement()
{
  auto iTy = IntegerType::create();
  auto arrTy = ArrayType::get(iTy, 1);
  auto array = ir::Variable::create(ir::Variable::Scope::FUNCTION_SCOPE, arrTy,
      Name::create("arr"));

  auto theLoop = Loop::create(IterateArrayStep::createDefault(false, array));
  ::std::cerr << "The Loop expression" << ::std::endl;
  prettyPrint(theLoop, ::std::cerr);
  ::std::cerr << ::std::endl;

  StatementBuilder sb;
  theLoop->toStatements([&](EVariable v, StatementBuilder &sb1) {
    auto str = sb1.declareLocalValue(OperatorExpression::OP_TO_STRING, {v});
    sb1.declareLocalValue(OperatorExpression::OP_LOG, {str});
    return sb1.build();
  }, sb);
  auto loopStmt = sb.build();

  ::std::cerr << "The Loop expression as a statement" << ::std::endl;
  prettyPrint(loopStmt, ::std::cerr);
  ::std::cerr << ::std::endl;

}
