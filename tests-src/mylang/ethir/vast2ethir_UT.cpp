#include <mylang/Parser.h>
#include <mylang/ethir/vast2ethir.h>
#include <mylang/ethir/prettyPrint.h>
#include <iostream>
#include <cassert>

using namespace mylang;
using namespace mylang::ethir;

void utest_toethir()
{
  const char *TEXT = ""
      "unit foo {\n"
      "defun id(x:integer):integer = x;\n"
      "}\n"
      "";

  auto eth = vast2ethir(parseText(TEXT));
  assert(eth && "ETH conversion failed");

  ::std::cerr << prettyPrint(eth) << ::std::endl;
}

