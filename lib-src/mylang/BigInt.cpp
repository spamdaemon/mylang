#include <mylang/BigInt.h>
#include <mylang/BigUInt.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <limits>

namespace mylang {

  BigInt::BigInt(const BigInt &other)
  {
    ::mpz_init_set(value, other.value);
  }

  BigInt& BigInt::operator=(const BigInt &other)
  {
    ::mpz_set(value, other.value);
    return *this;
  }

  BigInt::BigInt()
  {
    ::mpz_init(value);
  }

  BigInt::BigInt(long long int xvalue)
  {
    ::mpz_init_set_si(value, xvalue);
  }

  BigInt::BigInt(const BigUInt &xvalue)
  {
    ::mpz_init_set_ui(value, xvalue.unsignedValue());
  }

  BigInt::~BigInt()
  {
    ::mpz_clear(value);
  }

  bool BigInt::isSigned8() const
  {
    BigInt mx(::std::numeric_limits<::std::int8_t>::max());
    BigInt mn(::std::numeric_limits<::std::int8_t>::min());
    return mn <= *this && *this <= mx;
  }

  bool BigInt::isUnsigned8() const
  {
    BigInt mx(::std::numeric_limits<::std::uint8_t>::max());
    BigInt mn(::std::numeric_limits<::std::uint8_t>::min());
    return mn <= *this && *this <= mx;
  }

  bool BigInt::isSigned16() const
  {
    BigInt mx(::std::numeric_limits<::std::int16_t>::max());
    BigInt mn(::std::numeric_limits<::std::int16_t>::min());
    return mn <= *this && *this <= mx;
  }

  bool BigInt::isUnsigned16() const
  {
    BigInt mx(::std::numeric_limits<::std::uint16_t>::max());
    BigInt mn(::std::numeric_limits<::std::uint16_t>::min());
    return mn <= *this && *this <= mx;
  }

  bool BigInt::isSigned32() const
  {
    BigInt mx(::std::numeric_limits<::std::int32_t>::max());
    BigInt mn(::std::numeric_limits<::std::int32_t>::min());
    return mn <= *this && *this <= mx;
  }

  bool BigInt::isUnsigned32() const
  {
    BigInt mx(::std::numeric_limits<::std::uint32_t>::max());
    BigInt mn(::std::numeric_limits<::std::uint32_t>::min());
    return mn <= *this && *this <= mx;
  }

  bool BigInt::isSigned64() const
  {
    BigInt mx(::std::numeric_limits<::std::int64_t>::max());
    BigInt mn(::std::numeric_limits<::std::int64_t>::min());
    return mn <= *this && *this <= mx;
  }

  bool BigInt::isUnsigned64() const
  {
    BigInt mx(::std::numeric_limits<::std::uint64_t>::max());
    BigInt mn(::std::numeric_limits<::std::uint64_t>::min());
    return mn <= *this && *this <= mx;
  }

  long long int BigInt::signedValue() const
  {
    return ::mpz_get_si(value);
  }

  unsigned long long int BigInt::unsignedValue() const
  {
    return ::mpz_get_ui(value);
  }

  BigInt BigInt::signedValueOf(long long int value)
  {
    return BigInt(value);
  }

  BigInt BigInt::unsignedValueOf(unsigned long long int value)
  {
    return BigInt(value);
  }

  BigInt BigInt::parse(const ::std::string &str)
  {
    BigInt res;
    if (::mpz_set_str(res.value, str.c_str(), 10) != 0) {
      throw ::std::invalid_argument("Not a valid integer " + str);
    }
    return res;
  }

  BigInt BigInt::parseOctal(const ::std::string &str)
  {
    BigInt res;
    if (::mpz_set_str(res.value, str.c_str(), 8) != 0) {
      throw ::std::invalid_argument("Not a valid integer " + str);
    }
    return res;
  }

  BigInt BigInt::parseHex(const ::std::string &str)
  {
    BigInt res;
    if (::mpz_set_str(res.value, str.c_str(), 16) != 0) {
      throw ::std::invalid_argument("Not a valid integer " + str);
    }
    return res;
  }

  BigInt BigInt::parseBinary(const ::std::string &str)
  {
    BigInt res;
    if (::mpz_set_str(res.value, str.c_str(), 2) != 0) {
      throw ::std::invalid_argument("Not a valid integer " + str);
    }
    return res;
  }

  BigInt BigInt::negate() const
  {
    BigInt res;
    ::mpz_neg(res.value, value);
    return res;
  }

  BigInt BigInt::add(const BigInt &op2) const
  {
    BigInt res;
    ::mpz_add(res.value, value, op2.value);
    return res;
  }

  BigInt BigInt::subtract(const BigInt &op2) const
  {
    BigInt res;
    ::mpz_sub(res.value, value, op2.value);
    return res;
  }

  BigInt BigInt::multiply(const BigInt &op2) const
  {
    BigInt res;
    ::mpz_mul(res.value, value, op2.value);
    return res;
  }

  BigInt BigInt::divide(const BigInt &op2) const
  {
    BigInt res;
    ::mpz_tdiv_q(res.value, value, op2.value);
    return res;
  }

  BigInt BigInt::remainder(const BigInt &op2) const
  {
    BigInt res;
    ::mpz_tdiv_r(res.value, value, op2.value);
    return res;
  }

  BigInt BigInt::mod(const BigInt &op2) const
  {
    BigInt res;
    ::mpz_mod(res.value, value, op2.value);
    return res;
  }

  BigInt BigInt::min(const BigInt &op2) const
  {
    return *this < op2 ? *this : op2;
  }

  BigInt BigInt::max(const BigInt &op2) const
  {
    return *this > op2 ? *this : op2;
  }

  BigInt BigInt::pow(::std::uint32_t op2) const
  {
    BigInt res;
    ::mpz_pow_ui(res.value, value, op2);
    return res;
  }

  int BigInt::sgn() const
  {
    return mpz_sgn(value);
  }

  BigInt BigInt::abs() const
  {
    BigInt res;
    ::mpz_abs(res.value, value);
    return res;
  }

  int BigInt::compare(const BigInt &op2) const
  {
    return ::mpz_cmp(value, op2.value);
  }

  ::std::ostream& BigInt::write(::std::ostream &out) const
  {
    return out << toString();
  }

  ::std::string BigInt::toString() const
  {
    char *str = ::mpz_get_str(NULL, 10, value);
    ::std::string res(str);
    free(str);
    return res;
  }

}
