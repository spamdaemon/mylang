#ifndef CLASS_MYLANG_INTERVAL_H
#define CLASS_MYLANG_INTERVAL_H

#ifndef CLASS_MYLANG_INTEGER_H
#include <mylang/Integer.h>
#endif

#include <functional>
#include <optional>
#include <iosfwd>
#include <string>
#include <initializer_list>
#include <cstdint>

namespace mylang {

  /**
   * A generate representation of arbitrary Intervals with support for infinity.
   *  */
  class Interval
  {
    /**
     * The endpoint of an interval is an integer
     */
  public:
    using EndPoint = Integer;

    /** The interval from MINUS_INFINITY to PLUS_INFINITY */
  public:
    static const Interval& INFINITE();

    /** The positive interval is the interval {1,PLUS_INFINITY}*/
  public:
    static const Interval& POSITIVE();

    /** The negative interval is the interval {MINUS_INFINITY,-1}*/
  public:
    static const Interval& NEGATIVE();

    /** The non-positive interval is the interval {MINUS_INFINITY,0}*/
  public:
    static const Interval& NON_POSITIVE();

    /** The non-negative interval is the interval {0,PLUS_INFINITY}*/
  public:
    static const Interval& NON_NEGATIVE();

    /** The interval that contains only zero */
  public:
    static const Interval& ZERO();

    /** The interval that contains only zero */
  public:
    static const Interval& MINUS_ONE();

    /** The interval that contains only zero */
  public:
    static const Interval& PLUS_ONE();

    /** Copy constructor */
  public:
    Interval(const Interval&) = default;

    /** Copy operator */
  public:
    Interval& operator=(const Interval&) = default;

    /** Default constructor for the interval (-inf;+inf) */
  public:
    Interval();

    /**
     * Create an Interval that contains all numbers between l and r, including l and r.
     * @param l the left side of the interval (min)
     * @param r the right side of the interval (max)
     * @throws ::std::invalid_argument if l.sign()==r.sign() && l.isInfinite() && r.isInfinite()
     */
  public:
    Interval(const Integer &l, const Integer &r);

    /**
     * Create an Interval that contains all numbers between l and r, including l and r.
     * @param l the left side of the interval (min)
     * @param r the right side of the interval (max)
     * @throws ::std::invalid_argument if l.sign()==r.sign() && l.isInfinite() && r.isInfinite()
     */
  public:
    template<class T, class U>
    inline Interval(const T &l, const U &r)
        : Interval(toLeft(l), toRight(r))
    {
    }

    /**
     * Create an Interval that contains a single number.
     * @param value the value
     * @throws ::std::invalid_argument if value.isInfinite()
     */
  public:
    Interval(const Integer &value);

    /**
     * Create an Interval that contains a single number.
     */
  public:
    template<class T>
    Interval(const T &value)
        : Interval(Integer(value))
    {
    }

    /**
     * Destructor.
     */
  public:
    ~Interval() = default;

    /**
     * Create an interval from an initializer list.
     * If the set of integers describes either the interval (+inf,+inf) or (-inf,-inf),
     * then this function returns nothing.
     * @param x an integer
     * @param values
     * @return an interval or nothing on error
     */
  public:
    static ::std::optional<Interval> makeInterval(const Integer &x,
        const ::std::initializer_list<Integer> &list);

    /**
     * The size of the interval is the number of integers it contains.
     * @return the number of integers contained in this interval.
     */
  public:
    Integer size() const;

    /**
     * Get the value. If the value is infinity, nothing is returned.
     * @return the value
     */
  public:
    inline const Integer& min() const
    {
      return _min;
    }

    /**
     * Get the value. If the value is infinity, nothing is returned.
     * @return the value
     */
  public:
    inline const Integer& max() const
    {
      return _max;
    }

    /**
     * Determine if this interval fully contains that interval.
     * @param that an interval
     * @return true if every value in that interval is also in this interval
     */
  public:
    bool contains(const Interval &that) const;

    /**
     * Determine if this interval contains a multiple of the
     * specified number.
     *
     * @param n a number
     * @return true if the number is contained in this interval
     */
  public:
    inline bool containsMultiplesOf(const Integer &n) const
    {
      return multiplesOf(n).has_value();
    }

    /**
     * The determine the interval that contains multiples of the given number.
     *
     * If n==0, the interval ZERO is returned if this interval contains 0.
     *
     * @param n a number
     * @return the sub-interval that contains all multiples of n
     */
  public:
    ::std::optional<Interval> multiplesOf(const Integer &n) const;

    /**
     * Determine if this interval contains the number 0.
     * @return true if the number is contained in this interval
     */
  public:
    inline bool contains0() const
    {
      return _min.sign() <= 0 && 0 <= _max.sign();
    }

    /**
     * Determine if this interval is the number zero. The number 0 is
     * special, because it appears in several contexts.
     * @return true if this interval is Interval(0,0)
     */
  public:
    inline bool is0() const
    {
      return _min.sign() == 0 && _max.sign() == 0;
    }

    /** Is this interval is negative.
     * @return true if this interval is entirly positive
     */
  public:
    inline bool isNegative() const
    {
      return _max.sign() < 0;
    }

    /** Is this interval is positive.
     * @return true if this interval is entirely positive
     */
  public:
    inline bool isPositive() const
    {
      return _max.sign() > 0;
    }

    /**
     * Determine if this Interval is a finite Interval.
     * A finite interval is not bounded by either -inf or +inf
     * @return true if this Interval is finite
     */
  public:
    inline bool isFinite() const
    {
      return _min.isFinite() && _max.isFinite();
    }

    /**
     * Determine if this Interval is either positive or negative infinity.
     * @return true if this Interval is infinity
     */
  public:
    inline bool isInfinite() const
    {
      return !isFinite();
    }

    /**
     * Determine if this interval is fixed.
     * An interval is fixed if _min==_max. A fixed interval
     * implies isFinite()
     * @return if this interval contains a single integer number
     */
  public:
    inline bool isFixed() const
    {
      return _min == _max;
    }

    /**
     * Comparing.
     */
  public:
    bool equals(const Interval &op2) const;

    /**
     * Insert 0 into this interval.
     * @return a new interval that contains 0
     */
  public:
    Interval insert0() const;

    /**
     * Negate this Interval.
     * @return a new Interval
     */
  public:
    Interval negate() const;

    /**
     * Get the absolute value of this Interval */
  public:
    Interval abs() const;

    /**
     * Add two Intervals. If the two Intervals are infinite with
     * opposite signs, then addition is not defined.
     * @param y an Interval
     * @return a new Interval or nothing if both operands are infinite with opposite signs.
     */
  public:
    Interval add(const Interval &op2) const;

    /**
     * Subtract one Interval from another. If the two Intervals are infinite
     * and have the same sign, then subtraction is not defined.
     *
     * Subtraction is equivalent to
     * <code>add(op2.negate())</code>.
     *
     * @param y an Interval
     * @return a new Interval or nothing if both operands are infinite with the same sign.
     */
  public:
    inline Interval subtract(const Interval &op2) const
    {
      return add(op2.negate());
    }

    /**
     * Multiply two Intervals.
     * @param y an Interval
     * @return a new Interval
     */
  public:
    Interval multiply(const Interval &op2) const;

    /**
     * Divide two Intervals.
     *
     * If the denominator is 0 or both Intervals are infinite, then the result is undefined.
     * If the numerator is infinite, the result is infinite, and if the denomiator
     * is infinte, the result is 0.
     * @param y an Interval
     * @return a new Interval or Nothing if denominator is 0, or both operands are infinite.
     */
  public:
    ::std::optional<Interval> divide(const Interval &op2) const;

    /**
     * Compute the remainder of the division of two Intervals.
     * The remainder is defined as
     * <code>
     * a - (a/b)*b;
     * </code>
     * For example:
     *  * rem(-21,4) = -21 - (-21/4)*4 = -21 -  (-5) * 4 = -21 - (-20) = -1
     *  * rem(-21,-4) = -21 - (-21/-4)*(-4) = -21 -  (5) * (-4) = (-20) - (-20) = -1
     *  * rem(-21,inf) = -21 - (-21/inf) * inf = -21 - 0 * inf = -21
     * @param y an Interval
     * @return a new Interval or Nothing if denominator is 0
     */
  public:
    ::std::optional<Interval> remainder(const Interval &op2) const;

    /**
     * Compute the modulus of the division of two Intervals. Both, the
     * numerator and the denominator must be positive.
     * @param y an Interval
     * @return a new Interval or Nothing if denominator is 0 or any of the operands
     * is negative.
     */
  public:
    ::std::optional<Interval> mod(const Interval &op2) const;

    /**
     * Compute the power of this Interval raised to the given Interval.
     * Any interval raised to 0 yields the interval 1.
     * @param y an Interval to raise
     * @return a new Interval
     */
  public:
    Interval pow(::std::uint32_t op2) const;

    /**
     * Compute the pairwise minimum of two intervals.
     * Applies the minimum function to both endpoints of the
     * interval.
     * @param that interval
     * @return an interval
     */
  public:
    Interval pairwiseMin(const Interval &that) const;

    /**
     * Compute the pairwise maximum of two intervals.
     * Applies the maximum function to both endpoints of the
     * interval.
     * @param that interval
     * @return an interval
     */
  public:
    Interval pairwiseMax(const Interval &that) const;

    /**
     * Determine the operands whose abs value is minimal.
     * Basically, this function implements <code>x.abs().compare(y.abs())<0 ? x : y</code>
     * If the abs values are the same, then max() is used to determine the result value, e.g.,
     * given x=5,y-5, x.absMin(y) yields y
     *
     * @param y an Interval
     * @return *this or y
     */
  public:
    Interval absMin(const Interval &op2) const;

    /**
     * Determine the operands whose abs value is maximal.
     * Basically, this function implements <code>x.abs().compare(y.abs())>0 ? x : y</code>.
     * If the abs values are the same, then max() is used to determine the result value, e.g.,
     * given x=5,y-5, x. Max(y) yields x. This condition guarantees that x.absMax(y) == x.absMin(y) iff
     * x==y.
     * @param y an Interval
     * @return *this or y
     */
  public:
    Interval absMax(const Interval &op2) const;

    /**
     * Combine this interval with a second interval.
     * @param that an interval
     * @return an interval that contains all numbers in this
     * interval and that interval
     */
  public:
    Interval unionWith(const Interval &that) const;

    /**
     * Union two intervals.
     * @param a an interval
     * @param b an interval
     * @return an interval
     */
  public:
    static ::std::optional<Interval> unionOf(const ::std::optional<Interval> &a,
        const ::std::optional<Interval> &b);

    /**
     * Intersect this interval with a second interval.
     * @param that an interval
     * @return an interval or nothing if the intersection is empty.
     */
  public:
    ::std::optional<Interval> intersectWith(const Interval &that) const;

    /**
     * Test if this this and that interval intersect.
     * @param that an interval
     * @return true if they two intervals intersect
     */
  public:
    inline bool intersects(const Interval &that) const
    {
      return intersectWith(that).has_value();
    }

    /**
     * Intersection of two intervals.
     * @param a an interval
     * @param b an interval
     * @return an interval or nothing if the intersection would be empty
     */
  public:
    static ::std::optional<Interval> intersectionOf(const ::std::optional<Interval> &a,
        const ::std::optional<Interval> &b);

    /**
     * Join this interval with the specified interval.
     * This function unions this interval with the specified interval
     * if they intersect or are adjacent to each other.
     * As an example, The intervals (2,4) and (5,6) can be joined, but
     * the intervals (2,3) and (5,6) cannot be joined.
     * @param that an interval
     * @return an interval or nothing
     */
  public:
    ::std::optional<Interval> joinWith(const Interval &that) const;

    /**
     * Write an int to a stream
     * @param out a stream
     * @return the output stream
     */
  public:
    ::std::ostream& write(::std::ostream &out) const;

    /**
     * Get the string representation.
     * @return a string
     */
  public:
    ::std::string toString() const;

    /** The operators */
  public:
    inline friend Interval operator+(const Interval &x, const Interval &y)
    {
      return x.add(y);
    }

    inline friend Interval operator-(const Interval &x, const Interval &y)
    {
      return x.subtract(y);
    }

    inline friend Interval operator*(const Interval &x, const Interval &y)
    {
      return x.multiply(y);
    }

    inline friend ::std::optional<Interval> operator/(const Interval &x, const Interval &y)
    {
      return x.divide(y);
    }

    inline friend ::std::optional<Interval> operator%(const Interval &x, const Interval &y)
    {
      return x.mod(y);
    }

    inline friend Interval operator-(const Interval &x)
    {
      return x.negate();
    }

    inline friend bool operator==(const Interval &x, const Interval &y)
    {
      return x.equals(y);
    }

    inline friend bool operator!=(const Interval &x, const Interval &y)
    {
      return !x.equals(y);
    }

    inline friend ::std::ostream& operator<<(::std::ostream &s, const Interval &x)
    {
      return x.write(s);
    }

    /**
     * toLeft is a helper the create the min value
     */
  private:
    template<class T>
    static inline Integer toLeft(const T &t)
    {
      return Integer(t);
    }

    template<class T>
    static inline Integer toLeft(const ::std::optional<T> &t)
    {
      if (t) {
        return Integer(*t);
      } else {
        return Integer::MINUS_INFINITY();
      }
    }
    static inline Integer toLeft(const ::std::nullopt_t&)
    {
      return Integer::MINUS_INFINITY();
    }

    /**
     * toRight is a helper the create the max value
     */
  private:
    template<class T>
    static inline Integer toRight(const T &t)
    {
      return Integer(t);
    }
    template<class T>
    static inline Integer toRight(const ::std::optional<T> &t)
    {
      if (t) {
        return Integer(*t);
      } else {
        return Integer::PLUS_INFINITY();
      }
    }
    static inline Integer toRight(const ::std::nullopt_t&)
    {
      return Integer::PLUS_INFINITY();
    }

    /** The Interval value or nothing if infinity */
  private:
    Integer _min, _max;
  }
  ;

}
#endif
