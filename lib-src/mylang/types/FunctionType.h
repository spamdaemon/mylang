#ifndef CLASS_MYLANG_TYPES_FUNCTIONTYPE_H
#define CLASS_MYLANG_TYPES_FUNCTIONTYPE_H

#ifndef CLASS_MYLANG_TYPES_TYPE_H
#include <mylang/types/Type.h>
#endif
#include <memory>
#include <vector>

namespace mylang {
  namespace types {

    /** A type representing an error */
    class FunctionType: public Type
    {

    private:
      FunctionType(const ::std::shared_ptr<const Type> &ret,
          const ::std::vector<Type::Ptr> &xparameters);

    public:
      ~FunctionType();

      /** Two primitives are the same if their type ids are the same  */
    public:
      Ptr normalize(TypeSet &ts) const;

      void accept(TypeVisitor &v) const;

      bool isSameType(const Type &other) const final;

      ::std::string toString() const final;

      bool isConcrete() const override;

      bool canSafeCastFrom(const Type &t) const override;

      bool canCastFrom(const Type &t) const override;

      /**
       * Check if the specified parameters match this signature.
       * @param params a parameters object
       */
    public:
      bool matchParameters(const ::std::vector<Type::Ptr> &params) const;

      /**
       * Create a function type.
       * @param res the return type
       * @param parameters the positional parameters
       */
    public:
      static ::std::shared_ptr<const FunctionType> get(const Type::Ptr &ret,
          const ::std::vector<Type::Ptr> &xparameters);

      /** The return type */
    public:
      const Type::Ptr returnType;

      /** The return type */
    public:
      const ::std::vector<Type::Ptr> parameters;
    };
  }
}
#endif
