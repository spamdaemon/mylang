#ifndef CLASS_MYLANG_TYPES_INTEGERTYPE_H
#define CLASS_MYLANG_TYPES_INTEGERTYPE_H

#ifndef CLASS_MYLANG_TYPES_NUMERICTYPE_H
#include <mylang/types/NumericType.h>
#endif

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

namespace mylang {
  namespace types {

    /** The baseclass for all types */
    class IntegerType: public NumericType
    {

    private:
      IntegerType();

    public:
      ~IntegerType();

      /**
       * Get the bit type
       * @return a primitive type for a INTEGER
       */
    public:
      static ::std::shared_ptr<const IntegerType> create();

    public:
      void accept(TypeVisitor &v) const;

      bool isSameType(const Type &other) const final;

      ::std::string toString() const final;
    };
  }
}
#endif
