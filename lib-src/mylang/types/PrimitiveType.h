#ifndef CLASS_MYLANG_TYPES_PRIMITIVETYPE_H
#define CLASS_MYLANG_TYPES_PRIMITIVETYPE_H

#ifndef CLASS_MYLANG_TYPES_TYPE_H
#include <mylang/types/Type.h>
#endif
#include <memory>

namespace mylang {
  namespace types {

    /** The baseclass for all types */
    class PrimitiveType: public Type
    {

    protected:
      PrimitiveType();

    public:
      ~PrimitiveType();

      Ptr normalize(TypeSet &ts) const;

      /**
       * Get the bit type
       * @return a primitive type for a bit
       */
    public:
      static ::std::shared_ptr<const PrimitiveType> getBit();

      /**
       * Get the bit type
       * @return a primitive type for a boolean
       */
    public:
      static ::std::shared_ptr<const PrimitiveType> getBoolean();

      /**
       * Get the byte type
       * @return a primitive type for a byte
       */
    public:
      static ::std::shared_ptr<const PrimitiveType> getByte();

      /**
       * Get the bit type
       * @return a primitive type for a char
       */
    public:
      static ::std::shared_ptr<const PrimitiveType> getChar();

      /**
       * Get the bit type
       * @return a primitive type for a real
       */
    public:
      static ::std::shared_ptr<const PrimitiveType> getReal();

      /**
       * Get the bit type
       * @return a primitive type for a integer
       */
    public:
      static ::std::shared_ptr<const PrimitiveType> getInteger();

      /**
       * Get the bit type
       * @return a primitive type for a string
       */
    public:
      static ::std::shared_ptr<const PrimitiveType> getString();

      /**
       * Get the void type
       * @return a primitive type for a string
       */
    public:
      static ::std::shared_ptr<const PrimitiveType> getVoid();
    };
  }
}
#endif
