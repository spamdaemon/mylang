#include <mylang/types/StringType.h>
#include <mylang/types/TypeVisitor.h>

namespace mylang {
  namespace types {

    StringType::StringType()
    {
    }

    StringType::~StringType()
    {
    }

    void StringType::accept(TypeVisitor &v) const
    {
      v.visit(::std::dynamic_pointer_cast<const StringType>(self()));
    }

    bool StringType::isSameType(const Type &other) const
    {
      return dynamic_cast<const StringType*>(&other) != nullptr;
    }

    ::std::string StringType::toString() const
    {
      return "string";
    }

    ::std::shared_ptr<const StringType> StringType::create()
    {
      struct Impl: public StringType
      {
        ~Impl()
        {
        }
      };

      return ::std::make_shared<Impl>();
    }

  }
}
