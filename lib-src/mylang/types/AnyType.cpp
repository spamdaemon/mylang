#include <mylang/types/AnyType.h>
#include <mylang/types/TypeVisitor.h>
#include <mylang/types/TypeSet.h>
#include <cassert>

namespace mylang {
  namespace types {

    AnyType::AnyType()
    {
    }

    AnyType::~AnyType()
    {
    }

    bool AnyType::isConcrete() const
    {
      return false;
    }

    bool AnyType::isAnyType() const
    {
      return true;
    }

    ::std::string AnyType::toString() const
    {
      return "*";
    }

    Type::Ptr AnyType::normalize(TypeSet &ts) const
    {
      return ts.add(self());
    }

    bool AnyType::isSameType(const Type &other) const
    {
      return dynamic_cast<const AnyType*>(&other) != nullptr;
    }

    void AnyType::accept(TypeVisitor &v) const
    {
      v.visit(::std::dynamic_pointer_cast<const AnyType>(self()));
    }

    ::std::shared_ptr<const AnyType> AnyType::get()
    {
      struct Impl: public AnyType
      {
        Impl()
        {
        }

        ~Impl()
        {
        }

      };
      return ::std::make_shared<const Impl>();
    }
  }
}
