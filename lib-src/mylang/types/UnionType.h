#ifndef CLASS_MYLANG_TYPES_UNIONTYPE_H
#define CLASS_MYLANG_TYPES_UNIONTYPE_H

#ifndef CLASS_MYLANG_TYPES_OPTTYPE_H
#include <mylang/types/OptType.h>
#endif

#ifndef CLASS_MYLANG_EXPRESSIONS_CONSTANT_H
#include <mylang/expressions/Constant.h>
#endif
#include <memory>
#include <map>
#include <vector>

namespace mylang {
  namespace types {

    /** A type representing an error */
    class UnionType: public Type
    {

    public:
      struct Discriminant
      {

        Discriminant(const ::std::string &xname, const Type::Ptr &xtype);

        /** The name of the parameter */
        const ::std::string name;

        /** The parameter type */
        const Type::Ptr type;
      };

    public:
      struct Member
      {

        Member(const mylang::expressions::Constant::Ptr &discriminant, const ::std::string &xname,
            const TypePtr &xtype);

        /** The value of the discriminant for this member */
        const mylang::expressions::Constant::Ptr discriminant;

        /** The name of the parameter */
        const ::std::string name;

        /** The parameter type */
        const TypePtr type;
      };

    private:
      UnionType(const Discriminant &xdiscriminant, const ::std::vector<Member> &members);

    public:
      ~UnionType();

      /** Two primitives are the same if their type ids are the same  */
    public:
      Ptr normalize(TypeSet &ts) const;

      void accept(TypeVisitor &v) const;

      bool isSameType(const Type &other) const final;

      ::std::string toString() const final;

      /**
       * Get a structure
       * @param members
       */
    public:
      static ::std::shared_ptr<const UnionType> get(const Discriminant &xdiscriminant,
          const ::std::vector<Member> &xmembers);

      /**
       * Get the type of the subfield with the specified name.
       * @param name a subfield
       * @return the type of the subfield or nullptr if not found
       */
    public:
      TypePtr getMemberType(const ::std::string &field) const;

      TypePtr getMemberType(const mylang::expressions::Constant &discriminantValue) const;

      /** The discriminant member */
    public:
      const Discriminant discriminant;

      /** The members */
    public:
      const ::std::vector<Member> members;

      /** The indexed members */
    private:
      ::std::map<::std::string, const Member> _indexedMembers;
    };
  }
}
#endif
