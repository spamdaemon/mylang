#ifndef CLASS_MYLANG_TYPES_ANYTYPE_H
#define CLASS_MYLANG_TYPES_ANYTYPE_H

#ifndef CLASS_MYLANG_TYPES_TYPE_H
#include <mylang/types/Type.h>
#endif
#include <memory>

namespace mylang {
  namespace types {

    class AnyType: public Type
    {

    private:
      AnyType();

    public:
      ~AnyType();

    public:
      static ::std::shared_ptr<const AnyType> get();

    public:
      Ptr normalize(TypeSet &ts) const override;

      void accept(TypeVisitor &v) const override;

      ::std::string toString() const final;

      bool isConcrete() const override;

      bool isAnyType() const override;

      bool isSameType(const Type &other) const override;
    };
  }
}
#endif
