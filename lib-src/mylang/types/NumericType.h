#ifndef CLASS_MYLANG_TYPES_NUMERICTYPE_H
#define CLASS_MYLANG_TYPES_NUMERICTYPE_H

#ifndef CLASS_MYLANG_TYPES_PRIMITIVETYPE_H
#include <mylang/types/PrimitiveType.h>
#endif

namespace mylang {
  namespace types {

    class NumericType: public PrimitiveType
    {

    protected:
      NumericType();

    public:
      ~NumericType();
    };
  }
}
#endif
