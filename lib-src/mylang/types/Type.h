#ifndef CLASS_MYLANG_TYPES_TYPE_H
#define CLASS_MYLANG_TYPES_TYPE_H
#include <memory>
#include <string>
#include <map>
#include <iostream>

#ifndef CLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif

namespace mylang {
  namespace functions {
    class Function;

    class FunctionArgument;
  }

  namespace types {
    class TypeSet;

    /** The type visitor */
    class TypeVisitor;

    /** The baseclass for all types */
    class Type: public ::std::enable_shared_from_this<Type>
    {

      /** A type pointer */
    public:
      typedef ::std::shared_ptr<const Type> Ptr;

      /** Create new type instance */
    protected:
      Type();

      /** Create new type instance with a name.
       * @param xid an optional id */
    protected:
      Type(const ::mylang::names::Name::Ptr &xid);

    public:
      virtual ~Type() = 0;

      /**
       * Accept the specified type visitor
       * @param v a visitor
       */
    public:
      virtual void accept(TypeVisitor &v) const = 0;

      /**
       * Get this type.
       * @return this type instance
       */
    public:
      template<class T = Type>
      inline ::std::shared_ptr<const T> self() const
      {
        return ::std::dynamic_pointer_cast<const T>(shared_from_this());
      }

      /**
       * Can the specified type object be safely cast to this type.
       * @param from a type from which to cast
       * @return true if a cast between two type is safe, false otherwise
       */
    public:
      virtual bool canSafeCastFrom(const Type &from) const;

      /**
       * Can the specified type object be cast to this type. The cast
       * may be safe or unsafe, but there is a way to convert between the types.
       * @param from a type from which to cast
       * @return true if it possible to cast from to this type
       */
    public:
      virtual bool canCastFrom(const Type &from) const;

      /**
       * Determine if this type is the same as the specified type
       * @param other another type
       * @return true if the two types are the same
       */
    public:
      virtual bool isSameType(const Type &other) const = 0;

      /**
       * Get a string representation of this type. The representation is undefined and is primarily for debugging purposes.
       * @return a string representation
       */
    public:
      virtual ::std::string toString() const = 0;

      /**
       * Determine if this type is concrete.  A type that references the AnyType, is
       * not not concrete.
       * @return true by default
       */
    public:
      virtual bool isConcrete() const;

      /**
       * Is this the AnyType. This helper function to be make it easier to check for AnyType.
       * @return false
       */
    public:
      virtual bool isAnyType() const;

      /**
       * Get the unique name for this type
       * @return a unique name for this type
       */
    public:
      ::mylang::names::Name::Ptr name() const;

      /**
       * Add a normalized copy of this type to the typeset.
       * @param ts a typeset
       * @return a instance of this type that is fully normalized
       */
    public:
      virtual Ptr normalize(TypeSet &ts) const = 0;

      /**
       * Determine the union of the two types.
       * @param t a union
       * @return a instance of this type that is fully normalized
       */
    public:
      Ptr unionWith(const Ptr &t) const;

      /**
       * Print the specified type.
       * @param out an output stream
       * @param type a type pointer
       * @return out
       */
    public:
      friend inline ::std::ostream& operator<<(::std::ostream &out, const Ptr &type)
      {
        if (type) {
          out << type->toString();
        }
        return out;
      }

      /** A unique id for this type; the id is NOT used to determine if two types are the same */
    private:
      mutable ::mylang::names::Name::Ptr _id;
    };
  }
}
#endif
