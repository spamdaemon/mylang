#ifndef CLASS_MYLANG_TYPES_PROCESSTYPE_H
#define CLASS_MYLANG_TYPES_PROCESSTYPE_H

#ifndef CLASS_MYLANG_TYPES_TYPE_H
#include <mylang/types/Type.h>
#endif

#ifndef CLASS_MYLANG_TYPES_INPUTTYPE_H
#include <mylang/types/InputType.h>
#endif

#ifndef CLASS_MYLANG_TYPES_OUTPUTTYPE_H
#include <mylang/types/OutputType.h>
#endif
#include <map>
#include <memory>

namespace mylang {
  namespace types {

    /** A type representing an error */
    class ProcessType: public Type
    {
    public:
      typedef ::std::map<::std::string, ::std::shared_ptr<const InputType>> Inputs;
      typedef ::std::map<::std::string, ::std::shared_ptr<const OutputType>> Outputs;

    private:
      ProcessType(Inputs in, Outputs out);

    public:
      ~ProcessType();

    public:
      static ::std::shared_ptr<const ProcessType> get(Inputs in, Outputs out);

    public:
      Ptr normalize(TypeSet &ts) const override;

      void accept(TypeVisitor &v) const override;

      ::std::string toString() const final;

      bool isSameType(const Type &other) const override;

    public:
      const Inputs inputs;
      const Outputs outputs;
    };
  }
}
#endif
