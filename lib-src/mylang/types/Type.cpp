#include <mylang/types/Type.h>
#include <mylang/types/AnyType.h>

namespace mylang {
  namespace types {
    Type::Type()
    {
    }

    Type::Type(const ::mylang::names::Name::Ptr &xid)
        : _id(xid)
    {
    }

    Type::~Type()
    {
    }

    bool Type::isAnyType() const
    {
      return false;
    }

    bool Type::isConcrete() const
    {
      return true;
    }

    bool Type::canSafeCastFrom(const Type &from) const
    {
      return from.isAnyType() || isSameType(from);
    }

    bool Type::canCastFrom(const Type &from) const
    {
      return canSafeCastFrom(from);
    }

    ::mylang::names::Name::Ptr Type::name() const
    {
      if (!_id) {
        _id = mylang::names::Name::create();
      }
      return _id;
    }

    Type::Ptr Type::unionWith(const Ptr &t) const
    {
      if (canSafeCastFrom(*t)) {
        return self();
      }
      if (t->canSafeCastFrom(*this)) {
        return t;
      }
      return nullptr;
    }

  }
}
