#ifndef CLASS_MYLANG_TYPES_TYPEVISITOR_H
#define CLASS_MYLANG_TYPES_TYPEVISITOR_H
#include <mylang/types/types.h>

namespace mylang {
  namespace types {

    /** A type visitor */
    class TypeVisitor
    {

      /** The constructor */
    protected:
      TypeVisitor();

      /** Destructor */
    public:
      virtual ~TypeVisitor() = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const AnyType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const BitType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const BooleanType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const ByteType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const CharType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const RealType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const IntegerType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const StringType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const VoidType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const GenericType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const ArrayType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const UnionType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const StructType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const TupleType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const FunctionType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const MutableType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const OptType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const NamedType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const InputType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const OutputType> &type) = 0;

      /**
       * Visit the specified type
       * @param p a primitive type
       */
    public:
      virtual void visit(const ::std::shared_ptr<const ProcessType> &type) = 0;

    };
  }
}
#endif 
