#ifndef CLASS_MYLANG_TYPES_EXPORTABLETYPECHECKER_H
#define CLASS_MYLANG_TYPES_EXPORTABLETYPECHECKER_H

#include <mylang/defs.h>
#include <mylang/types/FunctionType.h>

namespace mylang::types {

  /**
   * The typechecker ensures that a type does not infinitely recurse.
   */
  class ExportableTypeChecker
  {
    ~ExportableTypeChecker() = delete;

    /**
     * A pointer to a type.
     *
     * @param type a type
     * @return true if the type is exportable
     */
  public:
    static bool isExportable(const TypePtr &type);

    /**
     * A pointer to a function type.
     *
     * Function types are not exportable types by default.
     *
     * @param type a function type
     * @return true if the function only references exportable types.
     */
  public:
    static bool isExportableFunction(
        const ::std::shared_ptr<const ::mylang::types::FunctionType> &type);

  };
}
#endif
