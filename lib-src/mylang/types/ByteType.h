#ifndef CLASS_MYLANG_TYPES_BYTETYPE_H
#define CLASS_MYLANG_TYPES_BYTETYPE_H

#ifndef CLASS_MYLANG_TYPES_PRIMITIVETYPE_H
#include <mylang/types/PrimitiveType.h>
#endif

namespace mylang {
  namespace types {

    /** The baseclass for all types */
    class ByteType: public PrimitiveType
    {

    private:
      ByteType();

    public:
      ~ByteType();

      /**
       * Get the bit type
       * @return a primitive type for a INTEGER
       */
    public:
      static ::std::shared_ptr<const ByteType> create();

    public:
      void accept(TypeVisitor &v) const;

      bool isSameType(const Type &other) const final;

      ::std::string toString() const final;
    };
  }
}
#endif
