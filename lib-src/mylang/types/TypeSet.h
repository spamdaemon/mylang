#ifndef CLASS_MYLANG_TYPES_TYPESET_H
#define CLASS_MYLANG_TYPES_TYPESET_H

#ifndef CLASS_MYLANG_TYPES_TYPE_H
#include <mylang/types/Type.h>
#endif

#ifndef CLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif
#include <vector>
#include <functional>

namespace mylang {
  namespace types {

    class TypeSet
    {
      /** The pair */

      /** Constructor */
    public:
      TypeSet();

      /** Destructor */
    public:
      virtual ~TypeSet();

      /**
       * Add a new type to this set. If the type already exists, then
       * the existing type is returned.
       * @param type a type
       * @return the type or the type that already exists
       */
    public:
      Type::Ptr add(const Type::Ptr &type);

      /**
       * Get each vector.
       * @return a vector types
       */
    public:
      const ::std::vector<Type::Ptr>& types() const
      {
        return _types;
      }

      /**
       * Filter the types in this set.
       * @param f a filter function
       * @return a vector types
       */
    public:
      template<class T>
      ::std::vector<::std::shared_ptr<const T>> filter(
          ::std::function<::std::shared_ptr<const T>(const Type::Ptr&)> f) const
      {
        ::std::vector<::std::shared_ptr<const T>> res;
        for (auto t : _types) {
          auto tmp = f(t);
          if (tmp) {
            res.push_back(tmp);
          }
        }
        return res;
      }

      /**
       * Filter the types by typename.
       */
      template<class T>
      ::std::vector<::std::shared_ptr<const T>> filter() const
      {
        ::std::vector<::std::shared_ptr<const T>> res;
        for (auto t : _types) {
          auto tmp = ::std::dynamic_pointer_cast<const T>(t);
          if (tmp) {
            res.push_back(tmp);
          }
        }
        return res;
      }

      /** The list of types */
    private:
      ::std::vector<Type::Ptr> _types;
    };
  }
}
#endif
