#ifndef CLASS_MYLANG_TYPES_OUTPUTTYPE_H
#define CLASS_MYLANG_TYPES_OUTPUTTYPE_H

#ifndef CLASS_MYLANG_TYPES_TYPE_H
#include <mylang/types/Type.h>
#endif
#include <memory>

namespace mylang {
  namespace types {

    /** A type representing an error */
    class OutputType: public Type
    {

    private:
      OutputType(::std::shared_ptr<const Type> xelement);

    public:
      ~OutputType();

    public:
      static ::std::shared_ptr<const OutputType> get(::std::shared_ptr<const Type> xelement);

    public:
      Ptr normalize(TypeSet &ts) const override;

      void accept(TypeVisitor &v) const override;

      ::std::string toString() const final;

      bool isSameType(const Type &other) const override;

      /** The element type */
    public:
      const ::std::shared_ptr<const Type> element;
    };
  }
}
#endif
