#include <mylang/types/InputType.h>
#include <mylang/types/TypeVisitor.h>
#include <mylang/types/TypeSet.h>
#include <cassert>

namespace mylang {
  namespace types {

    InputType::InputType(::std::shared_ptr<const Type> xelement)
        : element(xelement)
    {
      if (!xelement) {
        throw ::std::invalid_argument("Null element");
      }
      if (!xelement->isConcrete()) {
        throw ::std::invalid_argument("Element is not concrete " + xelement->toString());
      }
    }

    InputType::~InputType()
    {
    }

    ::std::string InputType::toString() const
    {
      return "=>" + element->toString();
    }

    Type::Ptr InputType::normalize(TypeSet &ts) const
    {
      return ts.add(get(element->normalize(ts)));
    }

    bool InputType::isSameType(const Type &other) const
    {
      auto that = dynamic_cast<const InputType*>(&other);
      if (!that) {
        return false;
      }
      return element->isSameType(*that->element);
    }

    void InputType::accept(TypeVisitor &v) const
    {
      v.visit(::std::dynamic_pointer_cast<const InputType>(self()));
    }

    ::std::shared_ptr<const InputType> InputType::get(::std::shared_ptr<const Type> xelement)
    {
      struct Impl: public InputType
      {
        Impl(::std::shared_ptr<const Type> t)
            : InputType(t)
        {
        }

        ~Impl()
        {
        }

      };
      return ::std::make_shared<const Impl>(xelement);
    }
  }
}
