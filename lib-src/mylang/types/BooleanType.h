#ifndef CLASS_MYLANG_TYPES_BOOLEANTYPE_H
#define CLASS_MYLANG_TYPES_BOOLEANTYPE_H

#ifndef CLASS_MYLANG_TYPES_PRIMITIVETYPE_H
#include <mylang/types/PrimitiveType.h>
#endif

namespace mylang {
  namespace types {

    /** The baseclass for all types */
    class BooleanType: public PrimitiveType
    {

    private:
      BooleanType();

    public:
      ~BooleanType();

      /**
       * Get the bit type
       * @return a primitive type for a INTEGER
       */
    public:
      static ::std::shared_ptr<const BooleanType> create();

    public:
      void accept(TypeVisitor &v) const;

      bool isSameType(const Type &other) const final;

      ::std::string toString() const final;
    };
  }
}
#endif
