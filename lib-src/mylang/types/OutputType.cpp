#include <mylang/types/OutputType.h>
#include <mylang/types/TypeVisitor.h>
#include <mylang/types/TypeSet.h>
#include <cassert>

namespace mylang {
  namespace types {

    OutputType::OutputType(::std::shared_ptr<const Type> xelement)
        : element(xelement)
    {
      if (!xelement) {
        throw ::std::invalid_argument("Null element");
      }
      if (!xelement->isConcrete()) {
        throw ::std::invalid_argument("Element is not concrete " + xelement->toString());
      }
    }

    OutputType::~OutputType()
    {
    }

    ::std::string OutputType::toString() const
    {
      return "<=" + element->toString();
    }

    Type::Ptr OutputType::normalize(TypeSet &ts) const
    {
      return ts.add(get(element->normalize(ts)));
    }

    bool OutputType::isSameType(const Type &other) const
    {
      auto that = dynamic_cast<const OutputType*>(&other);
      if (!that) {
        return false;
      }
      return element->isSameType(*that->element);
    }

    void OutputType::accept(TypeVisitor &v) const
    {
      v.visit(::std::dynamic_pointer_cast<const OutputType>(self()));
    }

    ::std::shared_ptr<const OutputType> OutputType::get(::std::shared_ptr<const Type> xelement)
    {
      struct Impl: public OutputType
      {
        Impl(::std::shared_ptr<const Type> t)
            : OutputType(t)
        {
        }

        ~Impl()
        {
        }

      };
      return ::std::make_shared<const Impl>(xelement);
    }
  }
}
