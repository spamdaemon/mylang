#include <mylang/types/ByteType.h>
#include <mylang/types/TypeVisitor.h>

namespace mylang {
  namespace types {

    ByteType::ByteType()
    {
    }

    ByteType::~ByteType()
    {
    }

    void ByteType::accept(TypeVisitor &v) const
    {
      v.visit(::std::dynamic_pointer_cast<const ByteType>(self()));
    }

    bool ByteType::isSameType(const Type &other) const
    {
      return dynamic_cast<const ByteType*>(&other) != nullptr;
    }

    ::std::string ByteType::toString() const
    {
      return "byte";
    }

    ::std::shared_ptr<const ByteType> ByteType::create()
    {
      struct Impl: public ByteType
      {
        ~Impl()
        {
        }
      };

      return ::std::make_shared<Impl>();
    }

  }
}
