#include <mylang/types/PrimitiveType.h>
#include <mylang/types/TypeVisitor.h>
#include <mylang/types/BitType.h>
#include <mylang/types/BooleanType.h>
#include <mylang/types/ByteType.h>
#include <mylang/types/CharType.h>
#include <mylang/types/RealType.h>
#include <mylang/types/IntegerType.h>
#include <mylang/types/StringType.h>
#include <mylang/types/TypeSet.h>

namespace mylang {
  namespace types {

    PrimitiveType::PrimitiveType()
    {
    }

    PrimitiveType::~PrimitiveType()
    {
    }

    Type::Ptr PrimitiveType::normalize(TypeSet &ts) const
    {
      return ts.add(self());
    }

    ::std::shared_ptr<const PrimitiveType> PrimitiveType::getBit()
    {
      return BitType::create();
    }

    ::std::shared_ptr<const PrimitiveType> PrimitiveType::getBoolean()
    {
      return BooleanType::create();
    }

    ::std::shared_ptr<const PrimitiveType> PrimitiveType::getByte()
    {
      return ByteType::create();
    }

    ::std::shared_ptr<const PrimitiveType> PrimitiveType::getChar()
    {
      return CharType::create();
    }

    ::std::shared_ptr<const PrimitiveType> PrimitiveType::getReal()
    {
      return RealType::create();
    }

    ::std::shared_ptr<const PrimitiveType> PrimitiveType::getInteger()
    {
      return IntegerType::create();
    }

    ::std::shared_ptr<const PrimitiveType> PrimitiveType::getString()
    {
      return StringType::create();
    }

    ::std::shared_ptr<const PrimitiveType> PrimitiveType::getVoid()
    {
      return VoidType::create();
    }

  }
}
