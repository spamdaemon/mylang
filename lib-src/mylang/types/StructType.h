#ifndef CLASS_MYLANG_TYPES_STRUCTTYPE_H
#define CLASS_MYLANG_TYPES_STRUCTTYPE_H

#ifndef CLASS_MYLANG_TYPES_TYPE_H
#include <mylang/types/Type.h>
#endif
#include <memory>
#include <map>
#include <vector>

namespace mylang {
  namespace types {

    /** A type representing an error */
    class StructType: public Type
    {

    public:
      struct Member
      {

        Member(const ::std::string &xname, const ::std::shared_ptr<const Type> &xtype);

        /** The name of the parameter */
        const ::std::string name;

        /** The parameter type */
        const ::std::shared_ptr<const Type> type;
      };

    private:
      StructType(const ::std::vector<Member> &members);

    public:
      ~StructType();

      /** Two primitives are the same if their type ids are the same  */
    public:
      Ptr normalize(TypeSet &ts) const;

      void accept(TypeVisitor &v) const;

      bool isSameType(const Type &other) const final;

      ::std::string toString() const final;

      bool canSafeCastFrom(const Type &t) const override;

      bool canCastFrom(const Type &t) const override;

      /**
       * Get a structure
       * @param members
       */
    public:
      static ::std::shared_ptr<const StructType> get(const ::std::vector<Member> &xmembers);

      /**
       * Get the type of the subfield with the specified name.
       * @param name a subfield
       * @return the type of the subfield or nullptr if not found
       */
    public:
      ::std::shared_ptr<const Type> getMemberType(const ::std::string &field) const;

      /** The members */
    public:
      const ::std::vector<Member> members;

      /** The indexed members */
    private:
      ::std::map<::std::string, const Member> _indexedMembers;
    };
  }
}
#endif
