#ifndef CLASS_MYLANG_TYPES_TUPLETYPE_H
#define CLASS_MYLANG_TYPES_TUPLETYPE_H

#ifndef CLASS_MYLANG_TYPES_TYPE_H
#include <mylang/types/Type.h>
#endif
#include <memory>
#include <vector>

namespace mylang {
  namespace types {

    /** A type representing an error */
    class TupleType: public Type
    {

    public:
      struct Member
      {
        Member(const ::std::string &xname, const ::std::shared_ptr<const Type> &xtype);

        /** The name of the parameter */
        const ::std::string name;

        /** The parameter type */
        const ::std::shared_ptr<const Type> type;
      };

    private:
      TupleType(const ::std::vector<Ptr> &tuple);

    public:
      ~TupleType();

      /** Two primitives are the same if their type ids are the same  */
    public:
      Ptr normalize(TypeSet &ts) const override;

      void accept(TypeVisitor &v) const override;

      bool isSameType(const Type &other) const final override;

      ::std::string toString() const final override;

      bool canSafeCastFrom(const Type &t) const override;

      bool canCastFrom(const Type &t) const override;

      bool isConcrete() const override;

      /**
       * Create a tuple that is the merge of the specified tuples
       * @param tuples a list of types
       * @return a tuple
       */
    public:
      static ::std::shared_ptr<const TupleType> merge(
          const ::std::vector<::std::shared_ptr<const TupleType>> &xtuples);
      /**
       * Get a Tupleure
       * @param tuple
       */
    public:
      static ::std::shared_ptr<const TupleType> get(const ::std::vector<Ptr> &xtuple);

      /** The types of the tuple */
    public:
      const ::std::vector<Ptr> types;

      /** The tuple (each type can be accessed using a unique name for convenience) */
    public:
      const ::std::vector<Member> tuple;
    };
  }
}
#endif
