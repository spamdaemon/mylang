#ifndef CLASS_MYLANG_TYPES_SERIALIZABLETYPECHECKER_H
#define CLASS_MYLANG_TYPES_SERIALIZABLETYPECHECKER_H

#include <mylang/defs.h>

namespace mylang::types {

  /**
   * The typechecker ensures that a type does not infinitely recurse.
   */
  class SerializableTypeChecker
  {
    ~SerializableTypeChecker() = delete;

    /**
     * A pointer to a type.
     *
     * @param type a type
     * @return true if the type is Serializable
     */
  public:
    static bool isSerializable(const TypePtr &type);

  };
}
#endif
