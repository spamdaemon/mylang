#include <mylang/types/VoidType.h>
#include <mylang/types/TypeVisitor.h>

namespace mylang {
  namespace types {

    VoidType::VoidType()
    {
    }

    VoidType::~VoidType()
    {
    }

    bool VoidType::isSameType(const Type &other) const
    {
      return dynamic_cast<const VoidType*>(&other) != nullptr;
    }

    ::std::string VoidType::toString() const
    {
      return "void";
    }

    void VoidType::accept(TypeVisitor &v) const
    {
      v.visit(::std::dynamic_pointer_cast<const VoidType>(self()));
    }

    ::std::shared_ptr<const VoidType> VoidType::create()
    {
      struct Impl: public VoidType
      {
        Impl()
            : VoidType()
        {
        }

        ~Impl()
        {
        }
      };
      return ::std::make_shared<Impl>();
    }
  }
}
