#include <mylang/types/ProcessType.h>
#include <mylang/types/TypeVisitor.h>
#include <mylang/types/TypeSet.h>
#include <cassert>

namespace mylang {
  namespace types {

    ProcessType::ProcessType(Inputs in, Outputs out)
        : inputs(::std::move(in)), outputs(::std::move(out))
    {
      for (auto i : inputs) {
        if (i.second == nullptr) {
          throw ::std::invalid_argument("null value for input " + i.first);
        }
      }
      for (auto i : outputs) {
        if (i.second == nullptr) {
          throw ::std::invalid_argument("null value for output " + i.first);
        }
        if (inputs.count(i.first) != 0) {
          throw ::std::invalid_argument("duplicate variable " + i.first);
        }
      }

    }

    ProcessType::~ProcessType()
    {
    }

    ::std::string ProcessType::toString() const
    {
      return "process";
    }

    Type::Ptr ProcessType::normalize(TypeSet &ts) const
    {
      Inputs in;
      Outputs out;

      for (auto i : inputs) {
        in[i.first] = i.second->normalize(ts)->self<InputType>();
      }
      for (auto i : outputs) {
        out[i.first] = i.second->normalize(ts)->self<OutputType>();
      }
      return ts.add(get(in, out));
    }

    bool ProcessType::isSameType(const Type &other) const
    {
      auto that = dynamic_cast<const ProcessType*>(&other);
      if (!that || that->inputs.size() != inputs.size() || that->outputs.size() != outputs.size()) {
        return false;
      }
      for (auto i : that->inputs) {
        auto tmp = inputs.find(i.first);
        if (tmp == inputs.end() || !tmp->second->isSameType(*i.second)) {
          return false;
        }
      }
      for (auto i : that->outputs) {
        auto tmp = outputs.find(i.first);
        if (tmp == outputs.end() || !tmp->second->isSameType(*i.second)) {
          return false;
        }
      }
      return true;
    }

    void ProcessType::accept(TypeVisitor &v) const
    {
      v.visit(::std::dynamic_pointer_cast<const ProcessType>(self()));
    }

    ::std::shared_ptr<const ProcessType> ProcessType::get(Inputs in, Outputs out)
    {
      struct Impl: public ProcessType
      {
        Impl(Inputs in, Outputs out)
            : ProcessType(::std::move(in), ::std::move(out))
        {
        }

        ~Impl()
        {
        }

      };
      return ::std::make_shared<const Impl>(::std::move(in), ::std::move(out));
    }
  }
}
