#include <mylang/types/ArrayType.h>
#include <mylang/types/TypeVisitor.h>
#include <mylang/types/TypeSet.h>
#include <cstddef>
#include <string>
#include <vector>
#include <cassert>
#include <utility>

namespace mylang {
  namespace types {

    ArrayType::ArrayType(const Ptr &xelement)
        : element(xelement)
    {
      if (!xelement) {
        throw ::std::invalid_argument("Null element");
      }
      if (!xelement->isAnyType() && !xelement->isConcrete()) {
        throw ::std::invalid_argument("Element is not concrete " + xelement->toString());
      }
    }

    ArrayType::~ArrayType()
    {
    }

    size_t ArrayType::dimensionality() const
    {
      size_t dim = 0;
      auto c = self<ArrayType>();
      while (c) {
        ++dim;
        c = c->element->self<ArrayType>();
      }
      return dim;
    }

    Type::Ptr ArrayType::leafElement() const
    {
      Ptr res;
      auto c = self<ArrayType>();
      while (c) {
        res = c->element;
        c = res->self<ArrayType>();
      }
      assert(res);
      return res;
    }

    Type::Ptr ArrayType::normalize(TypeSet &ts) const
    {
      auto e = element->normalize(ts);
      return ArrayType::get(e);
    }

    void ArrayType::accept(TypeVisitor &v) const
    {
      v.visit(::std::dynamic_pointer_cast<const ArrayType>(self()));
    }

    bool ArrayType::isConcrete() const
    {
      return element->isConcrete();
    }

    Type::Ptr ArrayType::flatten() const
    {
      auto arr = ::std::dynamic_pointer_cast<const ArrayType>(element);
      if (arr) {
        return arr->flatten();
      } else {
        return element;
      }
    }

    ::std::string ArrayType::toString() const
    {
      ::std::string res;
      res += element->toString();
      res += "[]";
      return res;
    }

    bool ArrayType::isSameType(const Type &other) const
    {
      auto that = dynamic_cast<const ArrayType*>(&other);
      if (!that) {
        return false;
      }
      return element->isSameType(*that->element);
    }

    bool ArrayType::canSafeCastFrom(const Type &from) const
    {
      auto other = dynamic_cast<const ArrayType*>(&from);
      if (!other) {
        return Type::canSafeCastFrom(from);
      }
      return element->canSafeCastFrom(*other->element);
    }

    bool ArrayType::canCastFrom(const Type &from) const
    {
      auto other = dynamic_cast<const ArrayType*>(&from);
      if (!other) {
        return Type::canCastFrom(from);
      }
      return element->canCastFrom(*other->element);
    }

    ::std::shared_ptr<const ArrayType> ArrayType::copy(
        const ::std::shared_ptr<const Type> &newElement) const
    {
      return get(newElement);
    }

    ::std::shared_ptr<const ArrayType> ArrayType::get(::std::shared_ptr<const Type> xelement)
    {
      struct Impl: public ArrayType
      {
        Impl(::std::shared_ptr<const Type> t)
            : ArrayType(t)
        {
        }

        ~Impl()
        {
        }
      };
      return ::std::make_shared<const Impl>(xelement);
    }
  }
}
