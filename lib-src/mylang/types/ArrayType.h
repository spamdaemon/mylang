#ifndef CLASS_MYLANG_TYPES_ARRAYTYPE_H
#define CLASS_MYLANG_TYPES_ARRAYTYPE_H

#ifndef CLASS_MYLANG_TYPES_TYPE_H
#include <mylang/types/Type.h>
#endif

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif
#include <memory>

namespace mylang {
  namespace types {

    /** A type representing an error */
    class ArrayType: public Type
    {
    private:
      ArrayType(const Ptr &xelement);

    public:
      ~ArrayType();

      /**
       * Follow the element type until an element is found that is not a optional
       * @return an element type (maybe nullptr!)
       */
    public:
      Type::Ptr flatten() const;

      /**
       * Create a new array with the specified element type. The element
       * type may an any element, otherwise be concrete.
       * @param xelement the element
       */
    public:
      static ::std::shared_ptr<const ArrayType> get(::std::shared_ptr<const Type> xelement);

      bool canSafeCastFrom(const Type &t) const override;

      bool canCastFrom(const Type &t) const override;

      bool isConcrete() const override;

      Ptr normalize(TypeSet &ts) const;

      void accept(TypeVisitor &v) const override;

      ::std::string toString() const override;

      bool isSameType(const Type &other) const override;

      /**
       * Determine the dimension of this array. If the element is also an array
       * then the dimension is increased by 1.
       */
    public:
      size_t dimensionality() const;

      /**
       * Get the leaf element.
       */
    public:
      Ptr leafElement() const;

      /**
       * Create a new instanceof of this array, but change the return type
       * @param element the new element type
       */
    public:
      virtual ::std::shared_ptr<const ArrayType> copy(const Type::Ptr &newElement) const;

      /** The element type (never null) */
    public:
      const Type::Ptr element;
    };
  }
}
#endif
