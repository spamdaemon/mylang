#include <mylang/types/SerializableTypeChecker.h>
#include <mylang/types/TypeVisitor.h>
#include <mylang/defs.h>
#include <mylang/types/AnyType.h>
#include <mylang/types/ArrayType.h>
#include <mylang/types/BitType.h>
#include <mylang/types/BooleanType.h>
#include <mylang/types/ByteType.h>
#include <mylang/types/CharType.h>
#include <mylang/types/FunctionType.h>
#include <mylang/types/GenericType.h>
#include <mylang/types/InputType.h>
#include <mylang/types/IntegerType.h>
#include <mylang/types/MutableType.h>
#include <mylang/types/NamedType.h>
#include <mylang/types/OptType.h>
#include <mylang/types/OutputType.h>
#include <mylang/types/ProcessType.h>
#include <mylang/types/RealType.h>
#include <mylang/types/StringType.h>
#include <mylang/types/StructType.h>
#include <mylang/types/TupleType.h>
#include <mylang/types/UnionType.h>
#include <mylang/types/VoidType.h>
#include <memory>
#include <optional>
#include <set>

namespace mylang::types {

  bool SerializableTypeChecker::isSerializable(const TypePtr &type)
  {
    using namespace mylang::types;

    struct Checker: public TypeVisitor
    {
      Checker()
      {
      }

      ~Checker()
      {
      }

      ::std::optional<bool> check(const TypePtr &type)
      {
        if (types.insert(type).second) {
          type->accept(*this);
          return ok;
        }
        // we don't know yet if the type is serializable or not
        return ::std::nullopt;
      }

      void visit(const ::std::shared_ptr<const AnyType>&)
      override
      {
        ok = false;
      }

      void visit(const ::std::shared_ptr<const BitType>&)
      override
      {
        ok = true;
      }

      void visit(const ::std::shared_ptr<const BooleanType>&)
      override
      {
        ok = true;
      }

      void visit(const ::std::shared_ptr<const ByteType>&)
      override
      {
        ok = true;
      }

      void visit(const ::std::shared_ptr<const CharType>&)
      override
      {
        ok = true;
      }

      void visit(const ::std::shared_ptr<const RealType>&)
      override
      {
        ok = true;
      }

      void visit(const ::std::shared_ptr<const IntegerType>&)
      override
      {
        ok = true;
      }

      void visit(const ::std::shared_ptr<const StringType>&)
      override
      {
        ok = true;
      }

      void visit(const ::std::shared_ptr<const ArrayType> &arrTy)
      override
      {
        ok = check(arrTy->element);
      }

      void visit(const ::std::shared_ptr<const VoidType>&)
      override
      {
        ok = false;
      }

      void visit(const ::std::shared_ptr<const GenericType>&)
      override
      {
        ok = false;
      }

      void visit(const ::std::shared_ptr<const TupleType> &ty)
      override
      {
        for (const auto &t : ty->types) {
          if (check(t) == false) {
            return;
          }
        }
        ok = true;
      }

      void visit(const ::std::shared_ptr<const StructType> &ty)
      override
      {
        for (const auto &m : ty->members) {
          if (check(m.type) == false) {
            return;
          }
        }
        ok = true;
      }

      void visit(const ::std::shared_ptr<const UnionType> &ty)
      {
        if (check(ty->discriminant.type) == false) {
          return;
        }
        for (const auto &m : ty->members) {
          if (check(m.type) == false) {
            return;
          }
        }
        ok = true;
      }

      void visit(const ::std::shared_ptr<const FunctionType>&)
      override
      {
        ok = false;
      }

      void visit(const ::std::shared_ptr<const OptType> &ty)
      override
      {
        ok = check(ty->element);
      }

      void visit(const ::std::shared_ptr<const MutableType>&)
      override
      {
        ok = false;
      }

      void visit(const ::std::shared_ptr<const NamedType> &t)
      override
      {
        ok = check(t->resolve());
      }

      void visit(const ::std::shared_ptr<const InputType>&)
      override
      {
        ok = false;
      }

      void visit(const ::std::shared_ptr<const OutputType>&)
      override
      {
        ok = false;
      }

      void visit(const ::std::shared_ptr<const ProcessType>&)
      override
      {
        ok = false;
      }

      ::std::optional<bool> ok;
      ::std::set<TypePtr> types;
    };

    Checker ch;
    type->accept(ch);
    return ch.ok != false;
  }
}

