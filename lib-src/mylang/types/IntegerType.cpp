#include <mylang/types/IntegerType.h>
#include <mylang/types/TypeVisitor.h>

namespace mylang {
  namespace types {

    IntegerType::IntegerType()
    {
    }

    IntegerType::~IntegerType()
    {
    }

    void IntegerType::accept(TypeVisitor &v) const
    {
      v.visit(::std::dynamic_pointer_cast<const IntegerType>(self()));
    }

    bool IntegerType::isSameType(const Type &other) const
    {
      if (this == &other) {
        return true;
      }

      return dynamic_cast<const IntegerType*>(&other) != nullptr;
    }

    ::std::string IntegerType::toString() const
    {
      ::std::string res;
      res += "integer";
      return res;
    }

    ::std::shared_ptr<const IntegerType> IntegerType::create()
    {
      struct Impl: public IntegerType
      {
        Impl()
            : IntegerType()
        {
        }

        ~Impl()
        {
        }
      };

      return ::std::make_shared<Impl>();
    }

  }
}
