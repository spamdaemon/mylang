#include <mylang/codegen/javagen/buildtools/maven/POMBuilder.h>
#include <iostream>

namespace mylang::codegen::javagen::buildtools::maven {

  POMBuilder::POMBuilder()
      : artifactId("none"), artifactGroup("none"), artifactVersion("1.0-SNAPSHOT")
  {

  }

  POMBuilder& POMBuilder::setArtifactId(::std::string id)
  {
    artifactId = ::std::move(id);
    return *this;
  }

  POMBuilder& POMBuilder::setArtifactGroup(::std::string group)
  {
    artifactGroup = ::std::move(group);
    return *this;
  }

  POMBuilder& POMBuilder::setArtifactVersion(::std::string version)
  {
    artifactVersion = ::std::move(version);
    return *this;
  }

  POMBuilder& POMBuilder::addMainClass(const ::mylang::names::FQN &classname)
  {
    mainClasses.insert(classname.fullName("."));
    return *this;
  }

  void POMBuilder::write(::std::ostream &pomFile) const
  {
    pomFile
        << R"delim(<?xml version="1.0" encoding="utf-8" ?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>none</groupId>
  <artifactId>none</artifactId>
  <version>1.0-SNAPSHOT</version>
  <properties>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
  </properties>
)delim";
    if (!mainClasses.empty()) {
      pomFile
          << R"delim( 	
  <build>
    <plugins>
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>appassembler-maven-plugin</artifactId>
        <version>1.10</version>
        <executions>
          <execution>
            <id>generate-programs</id>
            <phase>package</phase>
             <goals>
              <goal>assemble</goal>
             </goals>
            <configuration>
              <programs>
)delim";
      for (auto p : mainClasses) {
        pomFile << "                <program>" << ::std::endl;
        pomFile << "                  <mainClass>" << p << "</mainClass>" << ::std::endl;
        pomFile << "                  <id>" << p << "</id>" << ::std::endl;
        pomFile << "                </program>" << ::std::endl;
      }
      pomFile
          << R"delim( 	
              </programs>
            </configuration>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
)delim";
    }
    pomFile << R"delim( 	
</project>
)delim";
  }
}
