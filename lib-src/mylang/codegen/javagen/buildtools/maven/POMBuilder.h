#ifndef MYLANG_CODEGEN_JAVEGEN_BUILDTOOLS_MAVEN_POMBUILDER_H
#define MYLANG_CODEGEN_JAVEGEN_BUILDTOOLS_MAVEN_POMBUILDER_H

#ifndef MYLANG_NAMES_FQN_H
#include <mylang/names/FQN.h>
#endif

#include <iosfwd>
#include <set>

namespace mylang::codegen::javagen::buildtools::maven {

  /**
   * A builder for a POM file.
   *
   */
  struct POMBuilder
  {
    /**
     * Default constructor.
     * The artifact will be "none" and the version "1.0-SNAPSHOT"
     */
  public:
    POMBuilder();

    /**
     * Set the artifact id.
     * @param id
     * @return this builder
     */
  public:
    POMBuilder& setArtifactId(::std::string id);

    /**
     * Set the artifact group.
     * @param id
     * @return this builder
     */
  public:
    POMBuilder& setArtifactGroup(::std::string group);

    /**
     * Set the artifact version.
     * @param id
     * @return this builder
     */
  public:
    POMBuilder& setArtifactVersion(::std::string version);

    /**
     * Add a class for a main program.
     * @param fqn an FQN
     * @return this builder
     */
  public:
    POMBuilder& addMainClass(const ::mylang::names::FQN &classname);

    /**
     * Write this POM to the specified stream.
     * @param out a stream
     */
  public:
    void write(::std::ostream &out) const;

    /** The main classes */
  private:
    ::std::set<::std::string> mainClasses;

    /** The POM artifact id */
  private:
    ::std::string artifactId;

    /** The artifact group */
  private:
    ::std::string artifactGroup;

    /** The artifact version */
  private:
    ::std::string artifactVersion;
  };
}

#endif
