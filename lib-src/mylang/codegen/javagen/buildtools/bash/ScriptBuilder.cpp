#include <mylang/codegen/javagen/buildtools/bash/ScriptBuilder.h>
#include <iostream>

namespace mylang::codegen::javagen::buildtools::bash {

  namespace {
    static void bashHeader(::std::ostream &out)
    {
      out
          << R"delim(#!/bin/bash
		SCRIPT="$(readlink -f "$0")"
		SCRIPT_DIR="$(dirname "$0")"
)delim";

    }
  }

  ScriptBuilder::ScriptBuilder()
  {

  }

  void ScriptBuilder::write(::std::ostream &buildFile) const
  {
    bashHeader(buildFile);
    buildFile
        << R"delim(
JAVA_LANG=8;
cd "${SCRIPT_DIR}";
SOURCES=$(find src/main/java -name \*.java);
DEST=target/classes;
mkdir -p "${DEST}";
# distinguish between OpenJDK and the JDK
TARGET=--target;
SOURCE=--source;
if javac -version &>/dev/null; then
  TARGET=-target;
  SOURCE=-source;
fi;

exec javac \
  ${TARGET} "${JAVA_LANG}" \
  ${SOURCE} "${JAVA_LANG}" \
  -Xlint:unchecked \
  -Xlint:deprecation \
  -d "${DEST}" \
  -sourcepath src/main/java \
  ${SOURCES};
)delim";

  }

}

