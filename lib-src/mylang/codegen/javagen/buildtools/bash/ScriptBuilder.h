#ifndef MYLANG_CODEGEN_JAVEGEN_BUILDTOOLS_BASH_SCRIPTBUILDER_H
#define MYLANG_CODEGEN_JAVEGEN_BUILDTOOLS_BASH_SCRIPTBUILDER_H

#ifndef MYLANG_NAMES_FQN_H
#include <mylang/names/FQN.h>
#endif

#include <iosfwd>
#include <set>

namespace mylang::codegen::javagen::buildtools::bash {

  /**
   * A builder for a POM file.
   *
   */
  struct ScriptBuilder
  {
    /**
     * Default constructor.
     * The artifact will be "none" and the version "1.0-SNAPSHOT"
     */
  public:
    ScriptBuilder();

    /**
     * Write this POM to the specified stream.
     * @param out a stream
     */
  public:
    void write(::std::ostream &out) const;
  };
}

#endif
