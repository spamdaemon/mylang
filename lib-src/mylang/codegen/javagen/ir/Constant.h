#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_CONSTANT_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_CONSTANT_H

#include <memory>
#include <string>
#include <iostream>
#include <optional>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        template<class T>
        class Constant
        {
        public:
          template<class U>
          Constant(U xvalue)
              : value(xvalue)
          {
          }

        public:
          friend ::std::ostream& operator<<(::std::ostream &out, const Constant &c)
          {
            c.print(out);
            return out;
          }

        private:
          void print(::std::ostream &out) const
          {
            out << value;
          }

          T value;
        };

        template<>
        void Constant<::std::string>::print(::std::ostream &out) const;

        template<>
        void Constant<char>::print(::std::ostream &out) const;

      }
    }
  }
}
#endif
