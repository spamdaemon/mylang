#include <mylang/codegen/javagen/ir/VariableName.h>
#include <mylang/names/Name.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        static ::std::string make_fqn(const ::std::string &xpackagename,
            const ::std::string &xclassname, ::std::string xsimpleName)
        {
          if (xpackagename.length() == 0) {
            return xsimpleName;
          } else {
            return xpackagename + '.' + xclassname + '.' + xsimpleName;
          }
        }

        VariableName::VariableName(::std::string xpackagename, ::std::string xclassname,
            ::std::string xsimpleName)
            : fqn(make_fqn(xpackagename, xclassname, xsimpleName)), packageName(xpackagename),
                className(xclassname), simpleName(xsimpleName)
        {
        }

        VariableName::VariableName(::std::string xsimpleName)
            : fqn(xsimpleName), simpleName(xsimpleName)
        {
        }

        VariableName::~VariableName()
        {
        }

        VariableName VariableName::unique()
        {
          auto n = ::mylang::names::Name::create("v");
          return VariableName(n->uniqueLocalName());
        }
      }
    }
  }
}
