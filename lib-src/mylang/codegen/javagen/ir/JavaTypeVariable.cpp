#include <mylang/codegen/javagen/ir/JavaTypeVariable.h>
#include <mylang/codegen/javagen/ir/JavaType.h>
#include <mylang/codegen/CodeStream.h>

namespace mylang::codegen::javagen::ir {
  JavaTypeVariable::JavaTypeVariable(VariableName xn, JavaTypePtr t)
      : name(xn), type(t)
  {
    assert(t && "missing type");
  }

  ::std::string JavaTypeVariable::withExpr(const JavaTypePtr &exprTy, const ::std::string &expr,
      const ::std::function<::std::pair<JavaTypePtr, ::std::string>(const JavaTypeVariable&)> &fn)
  {

    JavaTypePtr boxedExprTy = exprTy->getBoxedType();
    JavaTypeVariable from(VariableName::unique(), exprTy);
    VariableName fromBoxed = VariableName::unique();

    auto ret = fn(from);
    auto toTy = ret.first;
    auto boxedRetTy = toTy->getBoxedType();

    CodeStream out;
    out << "((new java.util.function.Function<" << boxedExprTy->name << ", " << boxedRetTy->name
        << ">() ";
    out.brace(
        [&]
        {
          out << "public final " << boxedRetTy->name << " apply(" << boxedExprTy->name << ' '
              << fromBoxed << ") ";
          out.brace(
              [&]
              {
                out << "final " << exprTy->name << " " << from << " = " << exprTy->unbox(fromBoxed)
                    << ";" << nl;
                out << "return " << toTy->box(ret.second) << ";" << nl;
              });
        });
    out << ").apply(" << exprTy->box(expr) << "))";

    return toTy->unbox(out.getText());
  }

}
