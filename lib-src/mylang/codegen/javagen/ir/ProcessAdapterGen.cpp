#include <mylang/codegen/javagen/ir/ProcessAdapterGen.h>
#include <mylang/codegen/javagen/ir/PortAdapterGen.h>
#include <mylang/codegen/javagen/ir/LoggerGen.h>
#include <mylang/codegen/javagen/ir/types/OutputType.h>
#include <mylang/codegen/javagen/ir/ProcessGen.h>
#include <mylang/codegen/model/types/ArrayType.h>
#include <mylang/codegen/model/types/ProcessType.h>
#include <mylang/codegen/model/types/StringType.h>
#include <mylang/codegen/model/types/InputType.h>
#include <mylang/codegen/model/types/InputOutputType.h>
#include <mylang/codegen/model/types/OutputType.h>
#include <mylang/codegen/model/ir/ProcessConstructor.h>
#include <vector>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        namespace {

          struct PortAdapter
          {
            ::std::string portName;
            JavaType::Ptr portType;

            JavaType::Ptr nativeStreamType;
            ::std::string nativeStreamExpr;
            JavaType::Ptr adapterType;
          };

          struct Adapter: public JavaType
          {
            Adapter(Typename xname, model::ir::ProcessDecl::ProcessDeclPtr process,
                model::ir::ProcessConstructor::ProcessConstructorPtr ctor,
                ::std::vector<PortAdapter> xportAdapters)
                : JavaType(xname, nullptr), model(process), constructor(ctor),
                    portAdapters(xportAdapters)
            {
            }

            ~Adapter()
            {
            }

            bool isObject() const override final
            {
              return true;
            }

            bool writeClass(const Context &ctx) const override final
            {
              CodeStream out;
              if (generate(ctx, out)) {
                ctx.writeClass(name.fqn, out.getText(), true);
                return true;
              }
              return false;
            }
            ::std::string genNewInstance(const ::std::vector<Variable>&) const override final
            {
              return unexpected("constructor");
            }

            bool generate(const Context &ctx, CodeStream &out) const override final
            {
              JavaType::Ptr stdinAdapter;
              JavaType::Ptr stdoutAdapter;
              auto procGen = ctx.processGen;
              auto procTy = procGen->getProcessType(ctx,
                  model->type->self<model::types::ProcessType>());
              JavaType::Variable procVar(VariableName("proc"), procTy);

              auto procName = ctx.getProcessName(model->name);

              if (name.packageName.has_value()) {
                out << "package " << name.packageName.value() << ";" << nl();
              }
              out.brace("public final class " + name.simpleName, [&]
              {
                out.function("public static void main(String[] argv)", [&]
                {
                  out << procName << ' ' << procVar << " = ";
                  if (constructor && constructor->parameters.size() == 1) {
                    auto argTy = ctx.getType(constructor->parameters.at(0)->variable->type);
                    JavaType::Variable v(VariableName("argv"), argTy);
                    out << procGen->genNewInstance(ctx, procTy, procName, { v }) << ";" << nl();
                  } else {
                    out << procGen->genNewInstance(ctx, procTy, procName, { }) << ";" << nl();
                  }
                  ::std::vector<JavaType::Variable> autoclose;

                  for (const auto &e : portAdapters) {
                    out << nl();
                    JavaType::Variable portVar(e.portName, e.portType);
                    JavaType::Variable streamVar(e.portName + "_stream", e.nativeStreamType);
                    JavaType::Variable adapterVar(e.portName + "_adapter", e.adapterType);
                    out << "final " << e.portType->name << " " << portVar << " = " << procTy->genGetMember(e.portType,procVar,e.portName) << ';' << nl();
                    out << "final " << e.nativeStreamType->name << " " << streamVar << " = "
                        << e.nativeStreamExpr << ';' << nl();
                    out << "final " << e.adapterType->name << " " << adapterVar << " = "
                        << e.adapterType->genNewInstance( { portVar, streamVar }) << ';' << nl();
                    if (e.portType->self<types::OutputType>()) {
                      autoclose.push_back(adapterVar);
                    }
                  }

                  out << "// wait for the process to finish" << nl();
                  out.brace("try", procVar + ".join();");
                  out.brace("catch(java.lang.InterruptedException e)", "// ignore");
                  out.newlines(2);
                  out << "// close inputs to the process" << nl();
                  out << "int exitCode=0;" << nl();
                  for (auto adapterVar : autoclose) {
                    out.brace("try", adapterVar + ".close();");
                    out.brace("catch(java.io.IOException e)",
                        [&]
                        {
                          out << "exitCode=1;" << nl();
                          ctx.loggerGen->genEmitLog("error",
                              "Failed to stop adapter: " + adapterVar, out);
                        });
                  }
                  for (auto adapterVar : autoclose) {
                    out.brace("if (!" + ctx.portAdapterGen->getWaitAdapter(ctx, adapterVar) + ")",
                        [&]
                        {
                          out << "exitCode=1;" << nl();
                          ctx.loggerGen->genEmitLog("error",
                              "Could not wait for adapter or adapter failed: " + adapterVar, out);
                        });
                  }
                  out.newline();
                  out << "java.lang.System.exit(exitCode);";
                });
              });
              return true;
            }

            const model::ir::ProcessDecl::ProcessDeclPtr model;
            const model::ir::ProcessConstructor::ProcessConstructorPtr constructor;
            const ::std::vector<PortAdapter> portAdapters;
          };
        }

        ProcessAdapterGen::ProcessAdapterGen()
        {
        }
        ProcessAdapterGen::~ProcessAdapterGen()
        {
        }

        JavaType::Ptr ProcessAdapterGen::getAdapter(const Context &ctx,
            model::ir::ProcessDecl::ProcessDeclPtr process) const
        {
          auto name = ctx.getTypename(::std::nullopt, "Adapter" + process->name->uniqueLocalName());
          auto ty = ctx.findType(name);
          if (ty) {
            return ty;
          }
          auto argType = model::types::ArrayType::get(model::types::StringType::create(),
              Integer::ZERO(), Integer::PLUS_INFINITY());

          bool constructible = process->constructors.empty();
          model::ir::ProcessConstructor::ProcessConstructorPtr useCtor;

          for (auto ctor : process->constructors) {
            if (ctor->parameters.size() == 1) {
              if (ctor->parameters.at(0)->variable->type->isSameType(*argType)) {
                useCtor = ctor;
                break;
              }
            } else if (ctor->parameters.empty()) {
              constructible = true;
            }
          }
          if (!constructible) {
            // no useable constructor
            return nullptr;
          }

          PortAdapter inputPort;
          PortAdapter outputPort;
          ::std::vector<PortAdapter> portAdapters;
          for (auto p : process->publicPorts) {
            auto pty = p->type;
            auto ity = pty->self<model::types::InputType>();
            if (ity) {
              if (!inputPort.portName.empty()) {
                return nullptr; // too many input ports
              }
              inputPort.adapterType = ctx.portAdapterGen->getAdapter(ctx, ity);
              if (nullptr == inputPort.adapterType) {
                return nullptr;
              }
              inputPort.portName = p->publicName;
              inputPort.portType = ctx.getType(ity->getPeer());
              inputPort.nativeStreamExpr = "java.lang.System.in";
              inputPort.nativeStreamType = ctx.getBuiltinType("java.io.InputStream", nullptr);
              portAdapters.push_back(inputPort);
            } else {
              auto oty = pty->self<model::types::OutputType>();
              if (oty) {
                if (!outputPort.portName.empty()) {
                  return nullptr; // too many output ports
                }
                outputPort.adapterType = ctx.portAdapterGen->getAdapter(ctx, oty);
                if (nullptr == outputPort.adapterType) {
                  return nullptr;
                }
                outputPort.portName = p->publicName;
                outputPort.portType = ctx.getType(oty->getPeer());
                outputPort.nativeStreamExpr = "java.lang.System.out";
                outputPort.nativeStreamType = ctx.getBuiltinType("java.io.OutputStream", nullptr);
                portAdapters.push_back(outputPort);
              }
            }
          }
          return ctx.registerType(::std::make_shared<Adapter>(name, process, useCtor, portAdapters));
        }

      }
    }
  }
}
