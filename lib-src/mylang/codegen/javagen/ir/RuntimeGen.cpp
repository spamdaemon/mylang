#include <mylang/codegen/javagen/ir/RuntimeGen.h>
#include <mylang/codegen/javagen/ir/Context.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        RuntimeGen::RuntimeGen()
        {
        }
        RuntimeGen::~RuntimeGen()
        {
        }

        void RuntimeGen::generate(const Context &context) const
        {
          CodeStream out;

          auto ty = context.getTypename(::std::nullopt, "Runtime");

          ::std::map<::std::string, ::std::string> replacements;
          replacements["runtime.package"] = ty.packageName.value();
          replacements["runtime.class"] = ty.simpleName;
          out.insertTemplateFile("templates/codegen/javagen/ir/Runtime.java", replacements);
        }
      }
    }
  }
}
