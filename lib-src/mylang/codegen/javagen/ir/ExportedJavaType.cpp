#include <mylang/codegen/javagen/ir/ExportedJavaType.h>
#include <mylang/codegen/javagen/ir/JavaType.h>
#include <mylang/codegen/javagen/ir/ir.h>
#include <mylang/codegen/javagen/ir/Typename.h>
#include <mylang/codegen/javagen/ir/VariableName.h>
#include <algorithm>
#include <functional>
#include <memory>
#include <string>
#include <mylang/codegen/CodeStream.h>
#include <mylang/codegen/javagen/ir/Context.h>

namespace mylang::codegen::javagen::ir {

  ExportedJavaType::BidiMap ExportedJavaType::BidiMap::identity()
  {
    return {
      [&](const VariableName& n) {return n.fqn;}, //
      [&](const VariableName& n) {return n.fqn;}
    };
  }

  ExportedJavaType::ExportedJavaType(const Typename &n)
      : Type(n)
  {
  }

  ExportedJavaType::~ExportedJavaType()
  {
  }

  ExportedJavaType::Ptr ExportedJavaType::create(const Typename &name, JavaTypePtr bt,
      ::std::function<::std::string(const VariableName&)> toBT,
      ::std::function<::std::string(const VariableName&)> fromBT)
  {
    struct Impl: public ExportedJavaType
    {
      Impl(const Typename &xname, JavaTypePtr bt,
          ::std::function<::std::string(const VariableName&)> toBT,
          ::std::function<::std::string(const VariableName&)> fromBT)
          : ExportedJavaType(xname), javaType(::std::move(bt)), toBacking(toBT), fromBacking(fromBT)
      {
      }

      ~Impl()
      {
      }
      JavaTypePtr backingType() const override final
      {
        return javaType;
      }
      ::std::string toBackingType(const VariableName &v) const override final
      {
        return toBacking(v);
      }

      ::std::string fromBackingType(const VariableName &v) const override final
      {
        return fromBacking(v);
      }
      bool isObject() const
      {
        return javaType->isObject();
      }
      bool writeClass(const Context&) const
      {
        // since we're passing the impl type through, we don't have to do anything
        return false;
      }

      bool generate(const Context&, CodeStream&) const
      {
        return false;
      }

      const JavaTypePtr javaType;
      const ::std::function<::std::string(const VariableName&)> toBacking;
      const ::std::function<::std::string(const VariableName&)> fromBacking;
    };

    return ::std::make_shared<Impl>(name, ::std::move(bt), toBT, fromBT);
  }

  ExportedJavaType::Ptr ExportedJavaType::createIdentity(JavaType::Ptr type)
  {
    struct Impl: public ExportedJavaType
    {
      Impl(JavaType::Ptr impl)
          : ExportedJavaType(impl->name), javaType(::std::move(impl))
      {
      }

      ~Impl()
      {
      }
      JavaTypePtr backingType() const override final
      {
        return javaType;
      }
      ::std::string toBackingType(const VariableName &v) const override final
      {
        return v.simpleName;
      }

      ::std::string fromBackingType(const VariableName &v) const override final
      {
        return v.simpleName;
      }
      bool isObject() const
      {
        return javaType->isObject();
      }
      bool writeClass(const Context&) const
      {
        // since we're passing the impl type through, we don't have to do anything
        return false;
      }

      bool generate(const Context&, CodeStream&) const
      {
        return false;
      }
      const JavaType::Ptr javaType;
    };

    return ::std::make_shared<Impl>(::std::move(type));
  }

  ::std::string ExportedJavaType::allocateArray(const ::std::string &n) const
  {
    return "new " + name + "[" + n + "]";
  }

}
