#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_EXPORTEDJAVATYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_EXPORTEDJAVATYPE_H
#include <iostream>
#include <memory>
#include <string>

#ifndef FILE_MYLANG_CODEGEN_JAVAGEN_IR_H
#include <mylang/codegen/javagen/ir/ir.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPE_H
#include <mylang/codegen/javagen/ir/Type.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPENAME_H
#include <mylang/codegen/javagen/ir/Typename.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_VARIABLENAME_H
#include <mylang/codegen/javagen/ir/VariableName.h>
#endif

#include <cassert>
#include <functional>

namespace mylang::codegen::javagen::ir {

  /**
   * Instances of this type can be used to when interfacing with the outside world.
   */
  class ExportedJavaType: public Type
  {
    /** A type pointer */
  public:
    using Ptr = ::std::shared_ptr<const ExportedJavaType>;

    struct Variable
    {
    public:
      Variable(VariableName xn, Ptr t)
          : name(xn), type(t)
      {
        assert(t && "missing type");
      }

      friend ::std::ostream& operator<<(::std::ostream &out, const Variable &v)
      {
        return out << v.name.fqn;
      }
      friend ::std::string operator+(const ::std::string &s, const Variable &v)
      {
        return s + v.name;
      }
      friend ::std::string operator+(const Variable &v, const ::std::string &s)
      {
        return v.name + s;
      }
      friend ::std::string operator+(char ch, const Variable &v)
      {
        return ch + v.name;
      }
      friend ::std::string operator+(const Variable &v, char ch)
      {
        return v.name + ch;
      }

      /** The fully qualified name of the variable */
    public:
      const VariableName name;

      /* The type of the variable */
    public:
      const Ptr type;
    };

    struct BidiMap
    {
      ::std::function<::std::string(const VariableName&)> exportToBackingType;
      ::std::function<::std::string(const VariableName&)> backingTypeToExport;

      static BidiMap identity();
    };

    /**
     * Create a new simple java type
     */
  public:
    ExportedJavaType(const Typename &n);

  public:
    virtual ~ExportedJavaType();

    /**
     * Create an exported java type where both the exported and backing type
     * are the same type.
     * @param type backing type
     * @return a type
     */
  public:
    static Ptr createIdentity(JavaTypePtr backingType);

    /**
     * Create a simple exported type.
     * @param name the name of the exported type
     * @param backingType the backing type
     * @param toBackingType a function to convert to the backing type
     * @param fromBackingType a function to convert from the backing type
     * @return a type
     */
  public:
    static Ptr create(const Typename &name, JavaTypePtr backingType,
        ::std::function<::std::string(const VariableName&)> toBackingType,
        ::std::function<::std::string(const VariableName&)> fromBackingType);

    /**
     * Get this type.
     * @return this type instance
     */
  public:
    template<class T = ExportedJavaType>
    inline ::std::shared_ptr<const T> self() const
    {
      return ::std::dynamic_pointer_cast<const T>(shared_from_this());
    }

    /**
     * Allocate an array of a given size of this type.
     * @param n an expression for a size
     * @return an expression that allocates an array of size n
     */
  public:
    virtual ::std::string allocateArray(const ::std::string &n) const;

    /**
     * Get the java type that backs this exported type.
     *
     * The backing type is the type used internally.
     * @return a java type
     */
  public:
    virtual JavaTypePtr backingType() const = 0;

    /**
     * Get an expression that converts a value of an exported type to its internal type.
     * @param v a variable
     * @return a string expression of the backing type
     */
  public:
    virtual ::std::string toBackingType(const VariableName &v) const = 0;

    /**
     * Get an expression that converts a value from the backing type.
     * @param v a variable
     * @return a string expresion of the external type
     */
  public:
    virtual ::std::string fromBackingType(const VariableName &v) const = 0;

  };
}
#endif
