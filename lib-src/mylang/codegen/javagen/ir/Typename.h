#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPENAME_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPENAME_H

#ifndef FILE_MYLANG_CODEGEN_MODEL_H
#include <mylang/codegen/model/model.h>
#endif

#include <memory>
#include <string>
#include <optional>
#include <iostream>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        class Context;

        /**
         * A java type
         */
        class Typename
        {
          /** The tag that indicates the origin of the typename */
        public:
          enum Tag
          {
            TAG_NONE,    // tag is not set
            TAG_OPTIONAL,
            TAG_BIT,
            TAG_BOOLEAN,
            TAG_BYTE
          };

          friend class Context;

          /**
           * Create a new simple java type
           */
        private:
          Typename(::std::optional<::std::string> packagename, ::std::string simpleName, Tag tag =
              TAG_NONE);

          /**
           * Create a new simple java type
           */
        private:
          Typename(::std::string simpleName, Tag tag = TAG_NONE);

        public:
          ~Typename();

          /**
           * Create a new typename by changing the tag
           * @param newTag the new tag
           */
        public:
          Typename retag(Tag newTag) const;

          /**
           * Compare two typenames for equality based on their fqn
           */
        public:
          friend inline bool operator==(const Typename &a, const Typename &b)
          {
            return a.tag == b.tag && a.fqn == b.fqn;
          }

          /**
           * Compare two typenames for equality based on their fqn
           */
        public:
          friend inline bool operator!=(const Typename &a, const Typename &b)
          {
            return !(a == b);
          }

          /**
           * Ordering of typenames based on their fqn
           */
        public:
          friend inline bool operator<(const Typename &a, const Typename &b)
          {
            return a.tag < b.tag || (a.tag == b.tag && a.fqn < b.fqn);
          }

          /**
           * Ordering of typenames based on their fqn
           */
        public:
          friend inline ::std::ostream& operator<<(::std::ostream &out, const Typename &b)
          {
            return out << b.fqn;
          }

          /**
           * Ordering of typenames based on their fqn
           */
        public:
          friend inline ::std::string operator+(const ::std::string &left, const Typename &right)
          {
            return left + right.fqn;
          }

          /**
           * Ordering of typenames based on their fqn
           */
        public:
          friend inline ::std::string operator+(const Typename &left, const ::std::string &right)
          {
            return left.fqn + right;
          }

          /** A tag to distinguish typenames that are actually the same, but have different purposes */
        public:
          const Tag tag;

          /**
           * The fully qualified name of the type. If this is a builtin type, then
           * this is the same as the simple name.
           */
        public:
          const ::std::string fqn;

          /**
           * The fully qualified package name.
           */
        public:
          const ::std::optional<::std::string> packageName;

          /**
           * The simple name.
           */
        public:
          const ::std::string simpleName;
        };
      }
    }
  }
}
#endif
