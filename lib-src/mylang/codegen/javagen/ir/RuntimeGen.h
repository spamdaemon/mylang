#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_RUNTIMEGEN_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_RUNTIMEGEN_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_CONTEXT_H
#include <mylang/codegen/javagen/ir/Context.h>
#endif

#include <memory>
#include <string>
#include <vector>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        /** A generator for all top-level functions */
        class RuntimeGen
        {
          /** Constructor */
        public:
          RuntimeGen();

          /** Destructor */
        public:
          virtual ~RuntimeGen();

          /**
           * Generate the runtime files.
           * @param context a context
           */
        public:
          virtual void generate(const Context &context) const;

        };
      }
    }
  }
}
#endif
