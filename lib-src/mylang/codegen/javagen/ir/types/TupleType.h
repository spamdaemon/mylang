#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TUPLETYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TUPLETYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

#ifndef CLASS_MYLANG_BIGUINT_H
#include <mylang/BigUInt.h>
#endif

#include <mylang/codegen/model/types/TupleType.h>
#include <mylang/codegen/model/ir/LiteralTuple.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class TupleType: public virtual JavaType
          {
            /**
             * Create a new simple java type
             */
          public:
            TupleType();

          public:
            virtual ~TupleType();

            /**
             * Generate a constructor for a literal.
             * @param literal a literal
             */
          public:
            virtual ::std::string genLiteral(const Context &ctx,
                const model::ir::LiteralTuple::LiteralTuplePtr &literal) const = 0;

            /**
             * Generate an equality expression
             * @param lhs
             * @param rhs
             * @retutrn an equality expression lhs==rhs
             */
          public:
            virtual ::std::string genGetMember(const Ptr &ty, const Variable &str,
                const BigUInt &index) const = 0;

            /**
             * Create a bit type
             */
          public:
            static ::std::shared_ptr<const TupleType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::TupleType> model);
          };
        }
      }
    }
  }
}
#endif
