#include <mylang/codegen/javagen/ir/types/NativeIntegerType.h>
#include <mylang/codegen/javagen/ir/types/ArrayType.h>
#include <mylang/codegen/model/ir/LiteralInteger.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/javagen/ir/exports/PrimitiveExportedType.h>
#include <mylang/codegen/javagen/ir/exports/PrimitiveNamedType.h>
#include <limits>
#include <mylang/BigInt.h>
#include <mylang/codegen/CodeStream.h>
#include <mylang/codegen/javagen/ir/ir.h>
#include <mylang/codegen/javagen/ir/JavaType.h>
#include <mylang/codegen/javagen/ir/Typename.h>
#include <mylang/codegen/javagen/ir/VariableName.h>
#include <mylang/codegen/model/ir/Node.h>
#include <mylang/codegen/model/model.h>
#include <mylang/codegen/model/types/IntegerType.h>
#include <mylang/Integer.h>
#include <mylang/Interval.h>
#include <mylang/names/Name.h>
#include <cassert>
#include <map>
#include <memory>
#include <optional>
#include <stdexcept>
#include <string>
#include <vector>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          namespace {
            template<class T>
            static bool matches_type(const Interval &r)
            {
              const Integer min(::std::numeric_limits<T>::min());
              const Integer max(::std::numeric_limits<T>::max());
              if (r.min() < min) {
                return false;
              }
              if (r.max() > max) {
                return false;
              }
              return true;
            }

            static ::std::optional<::std::string> getNativeType(
                const model::types::IntegerType &model)
            {
              if (!model.range.isFinite()) {
                return ::std::nullopt;
              }
              if (matches_type<::std::int8_t>(model.range)) {
                return "byte";
              } else if (matches_type<::std::int16_t>(model.range)
                  || matches_type<::std::uint8_t>(model.range)) {
                return "short";
              } else if (matches_type<::std::int32_t>(model.range)
                  || matches_type<::std::uint16_t>(model.range)) {
                return "int";
              } else if (matches_type<::std::int64_t>(model.range)
                  || matches_type<::std::uint32_t>(model.range)) {
                return "long";
              } else {
                // does not fit into 64bits
                return ::std::nullopt;
              }
            }

          }

          NativeIntegerType::NativeIntegerType()

          {
          }

          NativeIntegerType::~NativeIntegerType()
          {
          }

          ::std::shared_ptr<const NativeIntegerType> NativeIntegerType::create(const Context &ctx,
              ::std::shared_ptr<const model::types::IntegerType> model)
          {
            auto ty = tryCreate(ctx, model);
            if (ty) {
              return ty;
            }
            throw new ::std::invalid_argument("Type is too large to fit into a native type");
          }

          ::std::shared_ptr<const NativeIntegerType> NativeIntegerType::tryCreate(
              const Context &ctx, ::std::shared_ptr<const model::types::IntegerType> model)
          {
            struct Impl: public NativeIntegerType
            {
              Impl(const ::std::string &nativeTypeName, const Context &ctx,
                  ::std::shared_ptr<const model::types::IntegerType> type)
                  : JavaType(ctx.getBuiltinTypename(nativeTypeName), type),
                      support(ctx.getTypename(::std::nullopt, type->name()->uniqueLocalName()))
              {
              }

              ~Impl()
              {
              }
              int typeSizeBytes() const
              {
                if (name.simpleName == "byte") {
                  return 1;
                }
                if (name.simpleName == "short") {
                  return 2;
                }
                if (name.simpleName == "int") {
                  return 4;
                }
                assert(name.simpleName == "long");
                return 8;
              }

              Ptr getBoxedType() const
              {
                const char *n;
                switch (typeSizeBytes()) {
                case 1:
                  n = "java.lang.Byte";
                  break;
                case 2:
                  n = "java.lang.Short";
                  break;
                case 4:
                  n = "java.lang.Integer";
                  break;
                default:
                  n = "java.lang.Long";
                  break;
                }
                return Context::getBuiltinType(Context::getBuiltinTypename(n), optModel);
              }

              ::std::string box(const ::std::string &expr) const
              {
                ::std::string prefix;
                switch (typeSizeBytes()) {
                case 1:
                  prefix = "java.lang.Byte.valueOf";
                  break;
                case 2:
                  prefix = "java.lang.Short.valueOf";
                  break;
                case 4:
                  prefix = "java.lang.Integer.valueOf";
                  break;
                default:
                  prefix = "java.lang.Long.valueOf";
                  break;
                }
                return prefix + "(" + expr + ")";
              }

              ::std::string unbox(const ::std::string &expr)
              {
                ::std::string suffix;
                switch (typeSizeBytes()) {
                case 1:
                  suffix = "byteValue()";
                  break;
                case 2:
                  suffix = "shortValue()";
                  break;
                case 4:
                  suffix = "intValue()";
                  break;
                default:
                  suffix = "longValue()";
                  break;
                }
                return "((" + expr + ")." + suffix + ")";
              }

              ExportedJavaTypePtr exportType(const Context&) const override final
              {
                return ::std::make_shared<exports::PrimitiveExportedType>(self());
              }

              ExportedJavaTypePtr exportTypeAs(const Context &c, const Typename &as,
                  const NamedTypePtr &ty) const override final
              {
                return ::std::make_shared<exports::PrimitiveNamedType>(c, as, ty);
              }

              bool isObject() const override final
              {
                return false;
              }

              bool writeClass(const Context &ctx) const override final
              {
                writeSupportClass(ctx);
                return false;
              }

              void writeSupportClass(const Context &ctx) const
              {
                CodeStream out;
                ::std::map<::std::string, ::std::string> replacements;
                replacements["support.package"] = support.packageName.value();
                replacements["support.class"] = support.simpleName;
                replacements["type.name"] = name.simpleName;
                replacements["type.size.bytes"] = ::std::to_string(typeSizeBytes());

                out.insertTemplateFile("templates/codegen/javagen/ir/NativeIntegerSupport.java",
                    replacements);

                ctx.writeClass(support.fqn, out.getText());
              }

              ::std::string toNativeString(const ::std::string &expr) const override final
              {
                return "java.lang.String.valueOf(" + expr + ")";
              }

              bool generate(const Context&, CodeStream&) const override final
              {
                return false;
              }
              std::string createFromInt32(const ::std::string &expr) const override final
              {
                if (name.simpleName != "int") {
                  return "(" + name.simpleName + ")(" + expr + ")";
                } else {
                  return expr;
                }
              }

              std::string createFromInt64(const ::std::string &expr) const override final
              {
                if (name.simpleName != "long") {
                  return "(" + name.simpleName + ")(" + expr + ")";
                } else {
                  return expr;
                }
              }

              ::std::string getInt8(const JavaType::Variable &var) const
              {
                if (name.simpleName != "byte") {
                  return "(byte)(" + var + ")";
                } else {
                  return var.name;
                }
              }

              ::std::string getInt16(const JavaType::Variable &var) const
              {
                if (name.simpleName != "short") {
                  return "(short)(" + var + ")";
                } else {
                  return var.name;
                }
              }

              ::std::string getInt32(const JavaType::Variable &var) const
              {
                if (name.simpleName != "int") {
                  return "(int)(" + var + ")";
                } else {
                  return var.name;
                }
              }

              ::std::string getInt64(const JavaType::Variable &var) const
              {
                if (name.simpleName != "long") {
                  return "(long)(" + var + ")";
                } else {
                  return var.name;
                }
              }

              ::std::string genFromBits(const JavaType::Variable &var) const override final
              {
                auto arrTy = var.type->self<ArrayType>();
                return support + ".bits2integer(" + arrTy->toNative(var) + ")";
              }

              ::std::string genToBits(const JavaType::Ptr &resTy,
                  const JavaType::Variable &var) const
                  override final
              {
                auto arrTy = resTy->self<ArrayType>();
                return arrTy->fromNative(support + ".integer2bits(" + var + ")");
              }
              ::std::string genFromBytes(const JavaType::Variable &var) const override final
              {
                auto arrTy = var.type->self<ArrayType>();
                return support + ".bytes2integer(" + arrTy->toNative(var) + ")";
              }
              ::std::string genToBytes(const JavaType::Ptr &resTy,
                  const JavaType::Variable &var) const override final
              {
                auto arrTy = resTy->self<ArrayType>();
                return arrTy->fromNative(support + ".integer2bytes(" + var + ")");
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
              {
                if (args.empty()) {
                  if (name.simpleName == "long") {
                    return "0L";
                  } else if (name.simpleName == "int") {
                    return "0";
                  } else {
                    return "(" + name.simpleName + ")0";
                  }
                }
                return "(" + name.simpleName + ")(" + args.at(0) + ")";
              }

              ::std::string genLiteral(const model::MLiteral &literal) const override final
              {
                auto lit = literal->self<model::ir::LiteralInteger>();
                if (!lit->value.isSigned64()) {
                  throw ::std::runtime_error(
                      "Integer too large for native integer " + name + ": "
                          + lit->value.toString());
                }
                if (name.simpleName == "long") {
                  return lit->value.toString() + "L";
                } else if (name.simpleName == "int") {
                  return lit->value.toString();
                } else {
                  return "(" + name.simpleName + ")" + lit->value.toString();
                }
              }

              ::std::string genToLiteral(const Ptr&, const Variable &var) const override final
              {
                return "java.lang.String.valueOf(" + var + ")";
              }

              ::std::string genEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + " == " + rhs;
              }
              ::std::string genNEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + " != " + rhs;
              }

              ::std::string genLT(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + " < " + rhs;
              }

              ::std::string genLTE(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + " <= " + rhs;
              }

              ::std::string genNegate(const Ptr&, const Variable &op) const override final
              {
                ::std::string cast;
                if (op.type->name != name) {
                  cast = "(" + name.simpleName + ")";
                }
                return cast + "(-" + op + ")";
              }

              ::std::string genAdd(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                ::std::string cast;
                if (true || lhs.type->name != name || rhs.type->name != name) {
                  cast = "(" + name.simpleName + ")";
                }
                return cast + "(" + lhs + " + " + rhs + ")";
              }

              ::std::string genSub(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                ::std::string cast;
                if (true || lhs.type->name != name || rhs.type->name != name) {
                  cast = "(" + name.simpleName + ")";
                }
                return cast + "(" + lhs + " - " + rhs + ")";
              }

              ::std::string genMul(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                ::std::string cast;
                if (true || lhs.type->name != name || rhs.type->name != name) {
                  cast = "(" + name.simpleName + ")";
                }
                return cast + "(" + lhs + " * " + rhs + ")";
              }

              ::std::string genDiv(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                ::std::string cast;
                if (true || lhs.type->name != name || rhs.type->name != name) {
                  cast = "(" + name.simpleName + ")";
                }
                return cast + "(" + lhs + " / " + rhs + ")";
              }

              ::std::string genMod(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                // TODO: is this correct??
                return "(" + name.simpleName + ")" + "java.lang.Long.remainderUnsigned(" + lhs
                    + ", " + rhs + ")";
              }

              ::std::string genRem(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                ::std::string cast;
                if (true || lhs.type->name != name || rhs.type->name != name) {
                  cast = "(" + name.simpleName + ")";
                }
                return cast + "(" + lhs + " % " + rhs + ")";
              }

              ::std::string genCheckedCast(const Variable &op) const
              {
                auto ity = op.type->self<IntegerType>();
                if (ity) {
                  if (name.simpleName == "byte") {
                    return ity->getInt8(op);
                  }
                  if (name.simpleName == "short") {
                    return ity->getInt16(op);
                  }
                  if (name.simpleName == "int") {
                    return ity->getInt32(op);
                  }
                  if (name.simpleName == "long") {
                    return ity->getInt64(op);
                  }
                }
                return unexpectedCheckedCast(op);
              }
              ::std::string genUncheckedCast(const Variable &op) const
              {
                auto ity = op.type->self<IntegerType>();

                if (ity) {
                  if (name.simpleName == "byte") {
                    return ity->getInt8(op);
                  }
                  if (name.simpleName == "short") {
                    return ity->getInt16(op);
                  }
                  if (name.simpleName == "int") {
                    return ity->getInt32(op);
                  }
                  if (name.simpleName == "long") {
                    return ity->getInt64(op);
                  }
                }
                return unexpectedUncheckedCast(op);
              }

            private:
              const Typename support;

            };
            auto nativeTypeName = getNativeType(*model);
            if (nativeTypeName) {
              return ::std::make_shared<Impl>(*nativeTypeName, ctx, model);
            } else {
              return nullptr;
            }
          }

        }
      }
    }
  }
}
