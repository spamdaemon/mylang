#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_MUTABLETYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_MUTABLETYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_WRAPPERLIKETYPE_H
#include <mylang/codegen/javagen/ir/types/WrapperLikeType.h>
#endif

#include <mylang/codegen/model/types/MutableType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class MutableType: public virtual JavaType, public virtual WrapperLikeType
          {
            /**
             * Create a new simple java type
             */
          public:
            MutableType();

          public:
            virtual ~MutableType();

            /**
             * Generate an equality expression
             * @param lhs
             * @param rhs
             * @retutrn an equality expression lhs==rhs
             */
          public:
            virtual ::std::string genSetValue(const Variable &obj, const Variable &value) const = 0;

            /**
             * Create a bit type
             */
          public:
            static ::std::shared_ptr<const MutableType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::MutableType> model);
          };
        }
      }
    }
  }
}
#endif
