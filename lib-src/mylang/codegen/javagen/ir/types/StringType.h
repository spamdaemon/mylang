#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_STRINGTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_STRINGTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PRIMITIVELIKETYPE_H
#include <mylang/codegen/javagen/ir/types/PrimitiveLikeType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_ARRAYLIKETYPE_H
#include <mylang/codegen/javagen/ir/types/SequenceType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TONATIVESTRINGTYPE_H
#include <mylang/codegen/javagen/ir/types/ToNativeStringType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_NATIVETYPE_H
#include <mylang/codegen/javagen/ir/types/NativeType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TOBITSTYPE_H
#include <mylang/codegen/javagen/ir/types/ToBitsType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TOBYTESTYPE_H
#include <mylang/codegen/javagen/ir/types/ToBytesType.h>
#endif

#include <mylang/codegen/model/types/StringType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class StringType: public virtual JavaType, public virtual SequenceType,
              public virtual PrimitiveLikeType, public virtual ToNativeStringType,
              public virtual NativeType, public virtual ToBitsType, public virtual ToBytesType
          {
            /**
             * Create a new simple java type
             */
          public:
            StringType();

          public:
            virtual ~StringType();

            /**
             * Generate an equality expression
             * @param lhs
             */
          public:
            virtual ::std::string genToCharArray(const Ptr &ty, const Variable &opt) const = 0;

          public:
            static ::std::shared_ptr<const StringType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::StringType> model);
          };
        }
      }
    }
  }
}
#endif
