#include <mylang/codegen/javagen/ir/types/BooleanType.h>
#include <mylang/codegen/model/ir/LiteralBoolean.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/CodeStream.h>
#include <mylang/codegen/javagen/ir/JavaType.h>
#include <mylang/codegen/javagen/ir/Typename.h>
#include <mylang/codegen/javagen/ir/VariableName.h>
#include <mylang/codegen/model/ir/Node.h>
#include <mylang/codegen/model/model.h>
#include <mylang/codegen/model/types/BooleanType.h>
#include <mylang/codegen/javagen/ir/exports/PrimitiveExportedType.h>
#include <mylang/codegen/javagen/ir/exports/PrimitiveNamedType.h>
#include <memory>
#include <string>
#include <vector>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          BooleanType::BooleanType()

          {
          }

          BooleanType::~BooleanType()
          {
          }

          ::std::shared_ptr<const BooleanType> BooleanType::create(const Context &ctx,
              ::std::shared_ptr<const model::types::BooleanType> model)
          {
            struct Impl: public BooleanType
            {
              Impl(const Context &ctx, model::MType m)
                  : JavaType(ctx.getBuiltinTypename("boolean").retag(Typename::TAG_BOOLEAN), m),
                      NativeType("boolean")
              {
              }

              ~Impl()
              {
              }

              Ptr getBoxedType() const
              {
                return Context::getBuiltinType(Context::getBuiltinTypename("java.lang.Boolean"),
                    optModel);
              }

              ::std::string box(const ::std::string &expr) const
              {
                return "java.lang.Boolean.valueOf(" + expr + ")";
              }

              ::std::string unbox(const ::std::string &expr)
              {
                return "((" + expr + ").booleanValue())";
              }

              ExportedJavaTypePtr exportType(const Context&) const override final
              {
                return ::std::make_shared<exports::PrimitiveExportedType>(self());
              }

              ExportedJavaTypePtr exportTypeAs(const Context &c, const Typename &as,
                  const NamedTypePtr &ty) const override final
              {
                return ::std::make_shared<exports::PrimitiveNamedType>(c, as, ty);
              }

              ::std::string genZero() const
              {
                return "false";
              }

              bool isObject() const override final
              {
                return false;
              }

              bool writeClass(const Context&) const override final
              {
                return false;
              }

              bool generate(const Context&, CodeStream&) const override final
              {
                return false;
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
              {
                // default
                if (args.empty()) {
                  return "false";
                }
                return args.at(0).name;
              }

              ::std::string toNative(const ::std::string &expr) const override final
              {
                return expr;
              }
              ::std::string fromNative(const ::std::string &expr) const override final
              {
                return expr;
              }
              ::std::string toNativeString(const ::std::string &expr) const override final
              {
                return "(" + expr + ") ? \"true\" : \"false\"";
              }

              ::std::string genLiteral(bool value) const override final
              {
                return value ? "true" : "false";
              }

              ::std::string genLiteral(const model::MLiteral &literal) const override final
              {
                return genLiteral(literal->self<model::ir::LiteralBoolean>()->value);
              }

              ::std::string genToLiteral(const Ptr&, const Variable &var) const override final
              {
                return "java.lang.String.valueOf(" + var + ")";
              }

              ::std::string genEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + " == " + rhs;
              }
              ::std::string genNEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + " != " + rhs;
              }

              ::std::string genLT(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                /*
                 * true < true -> false
                 * true < false -> false
                 * false < true -> true
                 * false < false -> false
                 */
                return '(' + rhs + " && !" + lhs + ')';
              }

              ::std::string genLTE(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                /*
                 * true < true -> true
                 * true < false -> false
                 * false < true -> true
                 * false < false -> true
                 */
                return '(' + rhs + " || !" + lhs + ')';
              }

              ::std::string genNot(const Ptr&, const Variable &op) const override final
              {
                return '!' + op;
              }

              ::std::string genAnd(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + " && " + rhs;
              }
              ::std::string genOr(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + " || " + rhs;
              }

              ::std::string genXor(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + " ^ " + rhs;
              }

            };

            return ::std::make_shared<Impl>(ctx, model);
          }
        }
      }
    }
  }
}
