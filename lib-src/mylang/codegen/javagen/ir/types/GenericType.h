#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_GENERICTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_GENERICTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PRIMITIVETYPE_H
#include <mylang/codegen/javagen/ir/types/PrimitiveType.h>
#endif

#include <mylang/codegen/model/types/GenericType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class GenericType: public virtual JavaType
          {
            /**
             * Create a new simple java type
             */
          public:
            GenericType();

          public:
            ~GenericType();

            /**
             * Create a bit type
             */
          public:
            static ::std::shared_ptr<const GenericType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::GenericType> model);
          };
        }
      }
    }
  }
}
#endif
