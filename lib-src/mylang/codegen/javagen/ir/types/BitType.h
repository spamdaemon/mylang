#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_BITTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_BITTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_BOOLEANLIKETYPE_H
#include <mylang/codegen/javagen/ir/types/BitLikeType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PRIMITIVETYPE_H
#include <mylang/codegen/javagen/ir/types/PrimitiveType.h>
#endif

#include <mylang/codegen/model/types/BitType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class BitType: public virtual JavaType, public virtual BitLikeType,
              public virtual PrimitiveType
          {
            /**
             * Create a new simple java type
             */
          public:
            BitType();

          public:
            ~BitType();

            /**
             * Create a bit type
             */
          public:
            static ::std::shared_ptr<const BitType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::BitType> model);
          };
        }
      }
    }
  }
}
#endif
