#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_REALTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_REALTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_NUMERICTYPE_H
#include <mylang/codegen/javagen/ir/types/NumericType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PRIMITIVETYPE_H
#include <mylang/codegen/javagen/ir/types/PrimitiveType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TONATIVESTRINGTYPE_H
#include <mylang/codegen/javagen/ir/types/ToNativeStringType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TOBITSTYPE_H
#include <mylang/codegen/javagen/ir/types/ToBitsType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TOBYTESTYPE_H
#include <mylang/codegen/javagen/ir/types/ToBytesType.h>
#endif

#include <mylang/codegen/model/types/RealType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class RealType: public virtual JavaType, public virtual NumericType,
              public virtual PrimitiveType, public virtual ToNativeStringType,
              public virtual ToBitsType, public virtual ToBytesType
          {
            /**
             * Create a new simple java type
             */
          public:
            RealType();

          public:
            ~RealType();

            /**
             * Create a bit type
             */
          public:
            static ::std::shared_ptr<const RealType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::RealType> model);
          };
        }
      }
    }
  }
}
#endif
