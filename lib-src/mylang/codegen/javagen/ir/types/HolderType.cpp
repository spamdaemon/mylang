#include <mylang/codegen/javagen/ir/types/HolderType.h>
#include <mylang/codegen/model/types/Type.h>
#include <mylang/codegen/javagen/ir/Context.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          HolderType::HolderType()

          {
          }

          HolderType::~HolderType()
          {
          }

          ::std::shared_ptr<const HolderType> HolderType::create(const Context &ctx,
              mylang::codegen::model::MType element)
          {
            struct Impl: public HolderType
            {
              Impl(const Context &ctx, mylang::codegen::model::MType xelement)
                  :
                      JavaType(ctx.getTypename("holder", xelement->name()->uniqueLocalName()),
                          nullptr), element(ctx.getType(xelement)->name)
              {
              }

              ~Impl()
              {
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                CodeStream out;
                bool generated = generate(ctx, out);
                if (generated) {
                  ctx.writeClass(name.fqn, out.getText());
                }
                return generated;
              }

              bool generate(const Context&, CodeStream &out) const override final
              {
                if (name.packageName.has_value()) {
                  out << "package " << name.packageName.value() << ";" << nl();
                }
                out << "public class " << name.simpleName << ' ';
                out.brace([&]
                {
                  out << "public final " << element << " value;" << nl();
                  out << "public " << name.simpleName << "(" << element << " value)";
                  out.brace([&]
                  {
                    out << "this.value = value;";
                  });
                }) << nl();
                return true;
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
              {
                return "new " + name + '(' + args.at(0) + ')';
              }

              ::std::string genNewInstance(const ::std::string &expr) const override final
              {
                return "new " + name + '(' + expr + ')';
              }

              ::std::string genGet(const Ptr&, const Variable &holder) const override final
              {
                return holder + ".value";
              }

              ::std::string genGet(const ::std::string &expr) const override final
              {
                return "(" + expr + ").value";
              }

              Typename element;
            }
            ;
            ::std::shared_ptr<const HolderType> holder = ::std::make_shared<Impl>(ctx, element);
            return ctx.registerType(holder)->self<const HolderType>();
          }
        }
      }
    }
  }
}
