#include <mylang/codegen/javagen/ir/types/VoidType.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/CodeStream.h>
#include <mylang/codegen/javagen/ir/JavaType.h>
#include <mylang/codegen/javagen/ir/Typename.h>
#include <mylang/codegen/model/model.h>
#include <mylang/codegen/model/types/VoidType.h>
#include <mylang/codegen/javagen/ir/exports/PrimitiveExportedType.h>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          VoidType::VoidType()

          {
          }

          VoidType::~VoidType()
          {
          }

          ::std::shared_ptr<const VoidType> VoidType::create(const Context &ctx,
              ::std::shared_ptr<const model::types::VoidType> model)
          {
            struct Impl: public VoidType
            {
              Impl(const Context &ctx, model::MType m)
                  : JavaType(ctx.getBuiltinTypename("void"), m)
              {
              }

              ~Impl()
              {
              }

              Ptr getBoxedType() const
              {
                return nullptr;
              }

              ::std::string box(const ::std::string &expr) const
              {
                throw ::std::runtime_error("Cannot unbox a void");
              }

              ::std::string unbox(const ::std::string &expr)
              {
                throw ::std::runtime_error("Cannot unbox a void");
              }

              ExportedJavaTypePtr exportType(const Context&) const override final
              {
                return ::std::make_shared<exports::PrimitiveExportedType>(self());
              }

              bool isObject() const override final
              {
                return false;
              }

              bool writeClass(const Context&) const override final
              {
                return false;
              }

              bool generate(const Context&, CodeStream&) const override final
              {
                return false;
              }

              ::std::string genNewInstance(const ::std::vector<Variable>&) const override final
              {
                throw ::std::runtime_error("Not supported");
              }

            };

            return ::std::make_shared<Impl>(ctx, model);
          }

        }
      }
    }
  }
}
