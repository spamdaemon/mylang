#include <mylang/codegen/javagen/ir/types/StructType.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/javagen/ir/BlockGen.h>
#include <mylang/codegen/javagen/ir/exports/ExportedNamedStructType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          StructType::StructType()

          {
          }

          StructType::~StructType()
          {
          }

          ::std::shared_ptr<const StructType> StructType::create(const Context &ctx,
              ::std::shared_ptr<const model::types::StructType> model)
          {
            struct Impl: public StructType
            {
              ::std::vector<::std::string> memberNames;
              ::std::vector<JavaType::Ptr> memberTypes;

              Impl(const Context &ctx, ::std::shared_ptr<const model::types::StructType> type)
                  :
                      JavaType(
                          ctx.getTypename(std::nullopt,
                              "struct_" + type->name()->uniqueLocalName()), type)
              {
                for (auto m : type->members) {
                  memberNames.push_back(m.name);
                  memberTypes.push_back(ctx.getType(m.type));
                }

              }

              ~Impl()
              {
              }

              ExportedJavaTypePtr exportTypeAs(const Context &c, const Typename &as,
                  const NamedTypePtr &ty) const override final
              {
                return ::std::make_shared<exports::ExportedNamedStructType>(c, as, ty);
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                CodeStream out;
                bool generated = generate(ctx, out);
                if (generated) {
                  ctx.writeClass(name.fqn, out.getText());
                }
                return generated;
              }

              bool generate(const Context&, CodeStream &out) const override final
              {
                if (name.packageName.has_value()) {
                  out << "package " << name.packageName.value() << ";" << nl();
                }
                out << "public class " << name.simpleName << ' ';
                out.brace(
                    [&]
                    {
                      for (size_t i = 0; i < memberNames.size(); ++i) {
                        out << "public final " << memberTypes.at(i)->name << " _"
                            << memberNames.at(i) << ';' << nl();
                      }
                      // constructor
                      out << "public " << name.simpleName;
                      out.parens(memberNames, ", ", [&](size_t i, const ::std::string&) {
                        out << memberTypes.at(i)->name << " _" << memberNames.at(i);
                      });
                      out.brace([&]
                      {
                        for (auto m : memberNames) {
                          out << "this._" << m << " = _" << m << ';' << nl();
                        }
                      });
                      out << ';';
                    }) << nl();
                return true;
              }

              ::std::string genGetMember(const Ptr&, const Variable &str,
                  const ::std::string &member) const
                  override final
              {
                return str + "._" + member;
              }

              ::std::string genLiteral(const Context &ctx,
                  const model::ir::LiteralStruct::LiteralStructPtr &literal) const override final
              {
                ::std::string res = "new " + name.fqn + '(';
                const char *sep = "";
                for (auto v : literal->members) {
                  CodeStream out;
                  ctx.blockGen->generate(ctx, v, out);
                  res += sep;
                  res = res + out.getText();
                  sep = ", ";
                }
                return res + ')';
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
              {
                ::std::string res = "new " + name.fqn + '(';
                const char *sep = "";
                for (auto v : args) {
                  res += sep;
                  res = res + v;
                  sep = ", ";
                }
                return res + ')';
              }
            };

            return ::std::make_shared<Impl>(ctx, model);
          }
        }
      }
    }
  }
}
