#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_VOIDTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_VOIDTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

#include <mylang/codegen/model/types/VoidType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class VoidType: public virtual JavaType
          {
            /**
             * Create a new simple java type
             */
          public:
            VoidType();

          public:
            ~VoidType();

            /**
             * Create a bit type
             */
          public:
            static ::std::shared_ptr<const VoidType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::VoidType> model);
          };
        }
      }
    }
  }
}
#endif
