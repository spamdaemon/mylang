#include <mylang/codegen/javagen/ir/types/IntegerType.h>
#include <mylang/codegen/javagen/ir/types/NativeIntegerType.h>
#include <mylang/codegen/javagen/ir/types/ArrayType.h>
#include <mylang/codegen/model/ir/LiteralInteger.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/javagen/ir/exports/ExportedTypeView.h>
#include <mylang/codegen/javagen/ir/exports/PrimitiveExportedType.h>
#include <mylang/codegen/javagen/ir/exports/PrimitiveNamedType.h>
#include <mylang/BigInt.h>
#include <mylang/codegen/CodeStream.h>
#include <mylang/codegen/javagen/ir/ir.h>
#include <mylang/codegen/javagen/ir/JavaType.h>
#include <mylang/codegen/javagen/ir/Typename.h>
#include <mylang/codegen/javagen/ir/VariableName.h>
#include <mylang/codegen/model/ir/Node.h>
#include <mylang/codegen/model/model.h>
#include <mylang/codegen/model/types/IntegerType.h>
#include <mylang/names/Name.h>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <vector>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {
          IntegerType::IntegerType()

          {
          }

          IntegerType::~IntegerType()
          {
          }

          ::std::shared_ptr<const IntegerType> IntegerType::create(const Context &ctx,
              ::std::shared_ptr<const model::types::IntegerType> model)
          {
            struct Impl: public IntegerType
            {
              Impl(const Context &ctx, ::std::shared_ptr<const model::types::IntegerType> type)
                  : JavaType(ctx.getBuiltinTypename("java.math.BigInteger"), type),
                      support(ctx.getTypename(::std::nullopt, type->name()->uniqueLocalName())),
                      nativeType(NativeIntegerType::tryCreate(ctx, type))
              {
              }

              ~Impl()
              {
              }

              ExportedJavaTypePtr exportType(const Context&) const override final
              {
                if (nativeType) {
                  return ::std::make_shared<exports::ExportedTypeView>(nativeType->name, self(), //
                      exports::ExportedTypeView::BidiMap { //
                      [&](const VariableName &v) {
                        return name +".valueOf("+v+")";
                      }, //
                      [&](const VariableName &v)
                      {
                        return v + '.' + nativeType->name.simpleName + "ValueExact()";
                      } });
                }
                return ::std::make_shared<exports::PrimitiveExportedType>(self());
              }

              ExportedJavaTypePtr exportTypeAs(const Context &c, const Typename &as,
                  const NamedTypePtr &ty) const override final
              {
                return ::std::make_shared<exports::PrimitiveNamedType>(c, as, ty);
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                writeSupportClass(ctx);
                return false;
              }

              void writeSupportClass(const Context &ctx) const
              {
                CodeStream out;
                ::std::map<::std::string, ::std::string> replacements;
                replacements["support.package"] = support.packageName.value();
                replacements["support.class"] = support.simpleName;
                out.insertTemplateFile("templates/codegen/javagen/ir/IntegerSupport.java",
                    replacements);

                ctx.writeClass(support.fqn, out.getText());
              }

              ::std::string toNativeString(const ::std::string &expr) const override final
              {
                return "(" + expr + ").toString()";
              }

              bool generate(const Context&, CodeStream&) const override final
              {
                return false;
              }
              std::string createFromInt32(const ::std::string &expr) const override final
              {
                return name + ".valueOf(" + expr + ")";
              }

              std::string createFromInt64(const ::std::string &expr) const override final
              {
                return name + ".valueOf(" + expr + ")";
              }

              ::std::string getInt8(const JavaType::Variable &var) const
              {
                return "(byte)(" + var + ".intValueExact())";
              }

              ::std::string getInt16(const JavaType::Variable &var) const
              {
                return "(short)(" + var + ".intValueExact())";
              }

              ::std::string getInt32(const JavaType::Variable &var) const
              {
                return var + ".intValueExact()";
              }

              ::std::string getInt64(const JavaType::Variable &var) const
              {
                return var + ".longValueExact()";
              }

              ::std::string genFromBits(const JavaType::Variable &var) const override final
              {
                auto arrTy = var.type->self<ArrayType>();
                return support + ".bits2integer(" + arrTy->toNative(var) + ")";
              }

              ::std::string genToBits(const JavaType::Ptr &resTy,
                  const JavaType::Variable &var) const
                  override final
              {
                auto arrTy = resTy->self<ArrayType>();
                return arrTy->fromNative(support + ".integer2bits(" + var + ")");
              }
              ::std::string genFromBytes(const JavaType::Variable &var) const override final
              {
                auto arrTy = var.type->self<ArrayType>();
                return support + ".bytes2integer(" + arrTy->toNative(var) + ")";
              }
              ::std::string genToBytes(const JavaType::Ptr &resTy,
                  const JavaType::Variable &var) const override final
              {
                auto arrTy = resTy->self<ArrayType>();
                return arrTy->fromNative(support + ".integer2bytes(" + var + ")");
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
              {
                // default
                if (args.empty()) {
                  return "java.math.BigInteger.ZERO";
                }
                return args.at(0).name;
              }

              ::std::string genLiteral(const model::MLiteral &literal) const override final
              {
                auto lit = literal->self<model::ir::LiteralInteger>();
                if (lit->value == 0) {
                  return "java.math.BigInteger.ZERO";
                }
                if (lit->value == 1) {
                  return "java.math.BigInteger.ONE";
                }
                if (lit->value == 10) {
                  return "java.math.BigInteger.TEN";
                }

                if (lit->value.isSigned32()) {
                  return "java.math.BigInteger.valueOf(" + lit->value.toString() + ")";
                } else if (lit->value.isSigned64()) {
                  return "java.math.BigInteger.valueOf(" + lit->value.toString() + "L)";
                } else {
                  return "new java.math.BigInteger(\"" + lit->value.toString() + "\")";
                }
              }

              ::std::string genToLiteral(const Ptr&, const Variable &var) const override final
              {
                return var + ".toString()";
              }

              ::std::string genEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".equals(" + rhs + ')';
              }
              ::std::string genNEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return "!" + lhs + ".equals(" + rhs + ')';
              }

              ::std::string genLT(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".compareTo(" + rhs + ")<0";
              }

              ::std::string genLTE(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".compareTo(" + rhs + ")<=0";
              }

              ::std::string genNegate(const Ptr&, const Variable &op) const override final
              {
                return op + ".negate()";
              }

              ::std::string genAdd(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".add(" + rhs + ')';
              }

              ::std::string genSub(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".subtract(" + rhs + ')';
              }

              ::std::string genMul(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".multiply(" + rhs + ')';
              }

              ::std::string genDiv(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".divide(" + rhs + ')';
              }

              ::std::string genMod(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".mod(" + rhs + ')';
              }

              ::std::string genRem(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".remainder(" + rhs + ')';
              }

              ::std::string genCheckedCast(const Variable &op) const
              {
                auto ity = op.type->self<NativeIntegerType>();
                if (ity) {
                  if (ity->name.simpleName == "byte") {
                    return createFromInt32(ity->getInt8(op));
                  }
                  if (ity->name.simpleName == "short") {
                    return createFromInt32(ity->getInt16(op));
                  }
                  if (ity->name.simpleName == "int") {
                    return createFromInt32(ity->getInt32(op));
                  }
                  if (ity->name.simpleName == "long") {
                    return createFromInt64(ity->getInt64(op));
                  }
                }
                return unexpectedCheckedCast(op);
              }
              ::std::string genUncheckedCast(const Variable &op) const
              {
                auto ity = op.type->self<NativeIntegerType>();
                if (ity) {
                  if (ity->name.simpleName == "byte") {
                    return createFromInt32(ity->getInt8(op));
                  }
                  if (ity->name.simpleName == "short") {
                    return createFromInt32(ity->getInt16(op));
                  }
                  if (ity->name.simpleName == "int") {
                    return createFromInt32(ity->getInt32(op));
                  }
                  if (ity->name.simpleName == "long") {
                    return createFromInt64(ity->getInt64(op));
                  }
                }
                return unexpectedUncheckedCast(op);
              }

            private:
              const Typename support;
            private:
              const ::std::shared_ptr<const NativeIntegerType> nativeType;
            };

            return ::std::make_shared<Impl>(ctx, model);
          }

        }
      }
    }
  }
}
