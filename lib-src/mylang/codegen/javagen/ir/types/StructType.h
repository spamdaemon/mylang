#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_STRUCTTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_STRUCTTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_STRUCTLIKETYPE_H
#include <mylang/codegen/javagen/ir/types/StructLikeType.h>
#endif

#include <mylang/codegen/model/types/StructType.h>
#include <mylang/codegen/model/ir/LiteralStruct.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class StructType: public virtual JavaType, public virtual StructLikeType
          {
            /**
             * Create a new simple java type
             */
          public:
            StructType();

          public:
            ~StructType();

            /**
             * Generate a constructor for a literal.
             * @param literal a literal
             */
          public:
            virtual ::std::string genLiteral(const Context &ctx,
                const model::ir::LiteralStruct::LiteralStructPtr &literal) const = 0;

            /**
             * Create a bit type
             */
          public:
            static ::std::shared_ptr<const StructType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::StructType> model);

          };
        }
      }
    }
  }
}
#endif
