#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_CHARTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_CHARTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PRIMITIVETYPE_H
#include <mylang/codegen/javagen/ir/types/PrimitiveType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TONATIVESTRINGTYPE_H
#include <mylang/codegen/javagen/ir/types/ToNativeStringType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_NATIVETYPE_H
#include <mylang/codegen/javagen/ir/types/NativeType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TOBITSTYPE_H
#include <mylang/codegen/javagen/ir/types/ToBitsType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TOBYTESTYPE_H
#include <mylang/codegen/javagen/ir/types/ToBytesType.h>
#endif

#include <mylang/codegen/model/types/CharType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class CharType: public virtual JavaType, public virtual PrimitiveType,
              public virtual ToNativeStringType, public virtual NativeType,
              public virtual ToBitsType, public virtual ToBytesType
          {
            /**
             * Create a new simple java type
             */
          public:
            CharType();

          public:
            ~CharType();

            /**
             * Create a bit type
             */
          public:
            static ::std::shared_ptr<const CharType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::CharType> model);
          };
        }
      }
    }
  }
}
#endif
