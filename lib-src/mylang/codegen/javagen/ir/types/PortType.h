#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PORTTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PORTTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_COMPARABLETYPE_H
#include <mylang/codegen/javagen/ir/types/ComparableType.h>
#endif

#include <vector>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class PortType: public virtual ComparableType
          {
          public:
            virtual ~PortType() = 0;

            /**
             * Get the the generatic port for the specified input or output port.
             * @return a function to get what is similar to file descriptor.
             */
          public:
            virtual ::std::string genGetDescriptor(const Context &context,
                JavaType::Variable port) const = 0;

          };
        }
      }
    }
  }
}
#endif
