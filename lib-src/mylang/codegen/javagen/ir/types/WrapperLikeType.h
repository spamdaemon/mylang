#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_WRAPPERLIKETYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_WRAPPERLIKETYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class WrapperLikeType
          {

          public:
            virtual ~WrapperLikeType() = 0;

            /**
             * Generate an equality expression
             * @param lhs
             */
          public:
            virtual ::std::string genGet(const JavaType::Ptr &ty,
                const JavaType::Variable &opt) const = 0;

          };
        }
      }
    }
  }
}
#endif
