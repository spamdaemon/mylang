#include <mylang/codegen/javagen/ir/types/CharType.h>
#include <mylang/codegen/model/ir/LiteralChar.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/javagen/ir/exports/PrimitiveExportedType.h>
#include <mylang/codegen/javagen/ir/exports/PrimitiveNamedType.h>
#include <mylang/text/AsciiTable.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          CharType::CharType()

          {
          }

          CharType::~CharType()
          {
          }

          ::std::shared_ptr<const CharType> CharType::create(const Context &ctx,
              ::std::shared_ptr<const model::types::CharType> model)
          {
            struct Impl: public CharType
            {
              Impl(const Context &ctx, model::MType type)
                  : JavaType(ctx.getBuiltinTypename("char"), type), NativeType("char"),
                      support(ctx.getTypename(::std::nullopt, type->name()->uniqueLocalName()))
              {
              }

              ~Impl()
              {
              }

              Ptr getBoxedType() const
              {
                return Context::getBuiltinType(Context::getBuiltinTypename("java.lang.Character"),
                    optModel);
              }

              ::std::string box(const ::std::string &expr) const
              {
                return "java.lang.Character.valueOf(" + expr + ")";
              }

              ::std::string unbox(const ::std::string &expr)
              {
                return "((" + expr + ").charValue())";
              }

              ExportedJavaTypePtr exportType(const Context&) const override final
              {
                return ::std::make_shared<exports::PrimitiveExportedType>(self());
              }

              ExportedJavaTypePtr exportTypeAs(const Context &c, const Typename &as,
                  const NamedTypePtr &ty) const override final
              {
                return ::std::make_shared<exports::PrimitiveNamedType>(c, as, ty);
              }

              ::std::string genZero() const
              {
                return "'\\0'";
              }

              bool isObject() const override final
              {
                return false;
              }

              bool writeClass(const Context &ctx) const override final
              {
                writeSupportClass(ctx);
                return false;
              }

              void writeSupportClass(const Context &ctx) const
              {
                CodeStream out;
                ::std::map<::std::string, ::std::string> replacements;
                replacements["support.package"] = support.packageName.value();
                replacements["support.class"] = support.simpleName;
                out.insertTemplateFile("templates/codegen/javagen/ir/CharacterSupport.java",
                    replacements);

                ctx.writeClass(support.fqn, out.getText());
              }

              bool generate(const Context&, CodeStream&) const override final
              {
                return false;
              }
              ::std::string toNative(const ::std::string &expr) const override final
              {
                return expr;
              }
              ::std::string fromNative(const ::std::string &expr) const override final
              {
                return expr;
              }
              ::std::string toNativeString(const ::std::string &expr) const override final
              {
                return "java.lang.String.valueOf(" + expr + ")";
              }

              ::std::string genFromBits(const JavaType::Variable &var) const override final
              {
                // TODO
                return "";
              }

              ::std::string genToBits(const JavaType::Ptr &resTy,
                  const JavaType::Variable &var) const
                  override final
              {
                // TODO
                return "";
              }
              ::std::string genFromBytes(const JavaType::Variable &var) const override final
              {
                return support + ".bytes2char(" + var + ")";
              }
              ::std::string genToBytes(const JavaType::Ptr&, const JavaType::Variable &var) const
              override final
              {
                return support + ".char2bytes(" + var + ")";
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
              {
                // default
                if (args.empty()) {
                  return "((char)0)";
                }
                return args.at(0).name;
              }

              ::std::string genLiteral(const model::MLiteral &literal) const override final
              {
                auto lit = literal->self<model::ir::LiteralChar>();
                return "'"
                    + mylang::text::AsciiTable::escapeText(mylang::text::AsciiTable::JAVA,
                        lit->value) + "'";
              }

              ::std::string genToLiteral(const Ptr&, const Variable &var) const override final
              {
                return "java.lang.String.valueOf(" + var + ")";
              }

              ::std::string genEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + " == " + rhs;
              }
              ::std::string genNEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + " != " + rhs;
              }

              ::std::string genLT(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + " < " + rhs;
              }

              ::std::string genLTE(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + " <= " + rhs;
              }

              const Typename support;
            };

            return ::std::make_shared<Impl>(ctx, model);
          }

        }
      }
    }
  }
}
