#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PRIMITIVETYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PRIMITIVETYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PRIMITIVELIKETYPE_H
#include <mylang/codegen/javagen/ir/types/PrimitiveLikeType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class PrimitiveType: public virtual PrimitiveLikeType
          {
          public:
            virtual ~PrimitiveType();

          public:
          };
        }
      }
    }
  }
}
#endif
