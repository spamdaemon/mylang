#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_NAMEDTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_NAMEDTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

#include <mylang/codegen/model/types/NamedType.h>
#include <mylang/codegen/model/ir/LiteralNamedType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class NamedType: public virtual JavaType
          {
            /**
             * Create a new simple java type
             */
          public:
            NamedType();

          public:
            virtual ~NamedType();

          public:
            static ::std::shared_ptr<const NamedType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::NamedType> model);

            /**
             * Generate a constructor for a literal.
             * @param literal a literal
             */
          public:
            virtual ::std::string genLiteral(const Context &ctx,
                const model::ir::LiteralNamedType::LiteralNamedTypePtr &literal) const = 0;

            /**
             * Determine if this is a reference type or the actual implementation type.
             */
          public:
            virtual bool isReference() const = 0;

            /**
             * Get the base of the object.
             * @param lhs
             * @param rhs
             * @retutrn an equality expression lhs==rhs
             */
          public:
            virtual ::std::string genCastToBaseType(const Variable &str) const = 0;

            /**
             * Get the root type.
             * @param lhs
             * @param rhs
             * @retutrn an equality expression lhs==rhs
             */
          public:
            virtual ::std::string genCastToRootType(const Variable &str) const = 0;
          };
        }
      }
    }
  }
}
#endif
