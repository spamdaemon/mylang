#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_INPUTTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_INPUTTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PORTTYPE_H
#include <mylang/codegen/javagen/ir/types/PortType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_WRAPPERLIKETYPE_H
#include <mylang/codegen/javagen/ir/types/WrapperLikeType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class InputType: public virtual JavaType, public virtual PortType, public WrapperLikeType
          {
          public:
            InputType();

          public:
            ~InputType();

            /**
             * Generate an equality expression
             * @param lhs
             */
          public:
            virtual ::std::string genIsInputNew(const Ptr &ty, const Variable &opt) const = 0;

            /**
             * Generate an equality expression
             * @param lhs
             */
          public:
            virtual ::std::string genIsReadable(const Ptr &ty, const Variable &opt) const = 0;

            /**
             * Generate a clear port statement
             * @param port the port to clear
             */
          public:
            virtual ::std::string genClearPort(const Variable &port) const = 0;

            /**
             * Generate a read port expression.
             * @param port the port to read from
             */
          public:
            virtual ::std::string genReadPort(const Ptr &ty, const Variable &port) const = 0;

            /**
             * Generate an equality expression
             * @param lhs
             */
          public:
            virtual ::std::string genIsInputClosed(const JavaType::Ptr &ty,
                const JavaType::Variable &opt) const = 0;

            /**
             * Generate a clear port statement
             * @param port the port to clear
             */
          public:
            virtual ::std::string genCloseInput(const JavaType::Variable &port) const = 0;

          };
        }
      }
    }
  }
}
#endif
