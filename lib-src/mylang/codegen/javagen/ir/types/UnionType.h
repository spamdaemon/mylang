#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_UNIONTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_UNIONTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_STRUCTLIKETYPE_H
#include <mylang/codegen/javagen/ir/types/StructLikeType.h>
#endif

#include <mylang/codegen/model/types/UnionType.h>
#include <mylang/codegen/model/ir/LiteralUnion.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class UnionType: public virtual JavaType, public virtual StructLikeType
          {
            /**
             * Create a new simple java type
             */
          public:
            UnionType();

          public:
            virtual ~UnionType();

            /**
             * Generate a constructor for a literal.
             * @param literal a literal
             */
          public:
            virtual ::std::string genLiteral(const Context &ctx,
                const model::ir::LiteralUnion::LiteralUnionPtr &literal) const = 0;

            /**
             * Create a new instance of a union type.
             * @param member the name of the member to be initialized
             * @param value the value associated with the slot
             */
          public:
            virtual ::std::string genNewInstance(const ::std::string &member,
                const Variable &value) const = 0;

            /**
             * Get the discriminant.
             */
          public:
            virtual ::std::string genGetDiscriminant(const JavaType::Ptr &ty,
                const JavaType::Variable &str) const = 0;

            /**
             * Create a bit type
             */
          public:
            static ::std::shared_ptr<const UnionType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::UnionType> model);
          };
        }
      }
    }
  }
}
#endif
