#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PROCESSTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PROCESSTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_COMPARABLETYPE_H
#include <mylang/codegen/javagen/ir/types/ComparableType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_StructLikeType_H
#include <mylang/codegen/javagen/ir/types/StructLikeType.h>
#endif

#include <vector>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class ProcessType: public virtual JavaType, public virtual ComparableType,
              public virtual StructLikeType
          {
          public:
            ProcessType();

          public:
            virtual ~ProcessType() =0;
          };
        }
      }
    }
  }
}
#endif
