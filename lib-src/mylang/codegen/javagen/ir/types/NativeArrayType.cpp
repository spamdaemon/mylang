#include <mylang/codegen/javagen/ir/types/NativeArrayType.h>
#include <mylang/codegen/javagen/ir/types/IntegerType.h>
#include <mylang/codegen/javagen/ir/BlockGen.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/javagen/ir/exports/ExportedArrayType.h>
#include <mylang/codegen/javagen/ir/exports/PrimitiveNamedType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          NativeArrayType::NativeArrayType()
          {
          }

          NativeArrayType::~NativeArrayType()
          {
          }

          ::std::shared_ptr<const NativeArrayType> NativeArrayType::create(const Context &ctx,
              ::std::shared_ptr<const model::types::ArrayType> model)
          {
            struct Impl: public NativeArrayType
            {
              Impl(const Context &ctx, ::std::shared_ptr<const model::types::ArrayType> model,
                  const Typename &nativeArrayType)
                  : JavaType(nativeArrayType, model), NativeType(nativeArrayType.fqn),
                      element(ctx.getType(model->element)->name),
                      support(
                          ctx.getTypename(::std::nullopt,
                              "support_" + model->element->name()->uniqueLocalName()))
              {
              }

              ~Impl()
              {
              }

              ExportedJavaTypePtr exportType(const Context &c) const override final
              {
                return ::std::make_shared<exports::ExportedArrayType>(c, self<NativeArrayType>());
              }

              ExportedJavaTypePtr exportTypeAs(const Context &c, const Typename &as,
                  const NamedTypePtr &ty) const override final
              {
                return ::std::make_shared<exports::PrimitiveNamedType>(c, as, ty);
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                writeSupportClass(ctx);
                return false;
              }

              bool generate(const Context&, CodeStream&) const override final
              {
                return false;
              }

              void writeSupportClass(const Context &ctx) const
              {
                CodeStream out;
                ::std::map<::std::string, ::std::string> replacements;
                replacements["support.package"] = support.packageName.value();
                replacements["support.class"] = support.simpleName;
                replacements["element.type"] = element.fqn;

                out.insertTemplateFile("templates/codegen/javagen/ir/ArraySupport.java",
                    replacements);

                ctx.writeClass(support.fqn, out.getText());
              }

              ::std::string genLiteral(const Context &ctx,
                  const model::ir::LiteralArray::LiteralArrayPtr &literal) const override final
              {
                ::std::string res;
                res = "new " + name.fqn + '{';
                const char *sep = "";
                for (auto v : literal->elements) {
                  CodeStream out;
                  ctx.blockGen->generate(ctx, v, out);
                  res += sep;
                  res = res + out.getText();
                  sep = ", ";
                }
                res += '}';
                return res;
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
              {
                ::std::string res;
                res = "new " + name.fqn + '{';
                const char *sep = "";
                for (auto v : args) {
                  res += sep;
                  res = res + v;
                  sep = ", ";
                }
                res += '}';
                return res;
              }

              ::std::string genAllocate(const ::std::string &len) const override final
              {
                return "new " + element + " [ " + len + "]";
              }

              ::std::string fromNative(const ::std::string &expr) const override final

              {
                return expr;
              }

              ::std::string toNative(const ::std::string &expr) const override final

              {
                return expr;
              }

              ::std::string genLength(const ::std::shared_ptr<const IntegerType> &ty,
                  const Variable &arr) const override final
              {
                return ty->createFromInt32(arr + ".length");
              }

              ::std::string genIndex(const Ptr&, const Variable &arr, const Variable &index) const
              override final
              {
                auto ity = index.type->self<IntegerType>();
                return arr + "[" + ity->getInt32(index) + ']';
              }

              ::std::string genSubrange(const Ptr&, const Variable &arr, const Variable &start,
                  const Variable &end) const override final
              {
                auto ity = start.type->self<IntegerType>();
                auto jty = end.type->self<IntegerType>();
                return "java.util.Arrays.copyOfRange(" + arr + ", " + ity->getInt32(start) + ", "
                    + jty->getInt32(end) + ")";
              }

              ::std::string genConcatenate(const Ptr&, const Variable &lhs,
                  const Variable &rhs) const override final
              {
                return support + ".concatenate(" + lhs + "," + rhs + ")";
              }

              const Typename element;
              const Typename support;
            };
            auto elementModel = model->element;
            auto element = ctx.getType(elementModel);
            return ::std::make_shared<Impl>(ctx, model, ctx.getArrayTypename(element->name));
          }

        }
      }
    }
  }
}
