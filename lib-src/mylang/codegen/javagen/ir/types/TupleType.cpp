#include <mylang/codegen/javagen/ir/types/TupleType.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/javagen/ir/BlockGen.h>
#include <mylang/codegen/javagen/ir/exports/ExportedNamedTupleType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          TupleType::TupleType()

          {
          }

          TupleType::~TupleType()
          {
          }

          ::std::shared_ptr<const TupleType> TupleType::create(const Context &ctx,
              ::std::shared_ptr<const model::types::TupleType> model)
          {
            struct Impl: public TupleType
            {
              ::std::vector<JavaType::Ptr> memberTypes;

              Impl(const Context &ctx, ::std::shared_ptr<const model::types::TupleType> type)
                  :
                      JavaType(
                          ctx.getTypename(std::nullopt, "tuple_" + type->name()->uniqueLocalName()),
                          type)
              {
                for (auto m : type->types) {
                  memberTypes.push_back(ctx.getType(m));
                }

              }

              ~Impl()
              {
              }

              ExportedJavaTypePtr exportTypeAs(const Context &c, const Typename &as,
                  const NamedTypePtr &ty) const override final
              {
                return ::std::make_shared<exports::ExportedNamedTupleType>(c, as, ty);
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                CodeStream out;
                bool generated = generate(ctx, out);
                if (generated) {
                  ctx.writeClass(name.fqn, out.getText());
                }
                return generated;
              }

              bool generate(const Context&, CodeStream &out) const override final
              {
                if (name.packageName.has_value()) {
                  out << "package " << name.packageName.value() << ";" << nl();
                }
                out << "public class " << name.simpleName << ' ';
                out.brace([&]
                {
                  for (size_t i = 0; i < memberTypes.size(); ++i) {
                    out << "public final " << memberTypes.at(i)->name << " _" << i << ';' << nl();
                  }
                  // constructor
                  out << "public " << name.simpleName;
                  out.parens(memberTypes, ", ", [&](size_t i, const JavaType::Ptr&) {
                    out << memberTypes.at(i)->name << " _" << i;
                  });
                  out.brace([&]
                  {
                    for (size_t i = 0; i < memberTypes.size(); ++i) {
                      out << "this._" << i << " = _" << i << ';' << nl();
                    }
                  });
                  out << ';';
                }) << nl();
                return true;
              }

              ::std::string genGetMember(const Ptr&, const Variable &str,
                  const BigUInt &index) const
                  override final
              {
                return str + "._" + index.toString();
              }

              ::std::string genLiteral(const Context &ctx,
                  const model::ir::LiteralTuple::LiteralTuplePtr &literal) const override final
              {
                ::std::string res = "new " + name.fqn + '(';
                const char *sep = "";
                for (auto v : literal->elements) {
                  CodeStream out;
                  ctx.blockGen->generate(ctx, v, out);
                  res += sep;
                  res = res + out.getText();
                  sep = ", ";
                }
                return res + ')';
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
              {
                ::std::string res = "new " + name.fqn + '(';
                const char *sep = "";
                for (auto v : args) {
                  res += sep;
                  res = res + v;
                  sep = ", ";
                }
                return res + ')';
              }
            };

            return ::std::make_shared<Impl>(ctx, model);
          }
        }
      }
    }
  }
}
