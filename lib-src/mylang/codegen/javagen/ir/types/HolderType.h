#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_HOLDERTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_HOLDERTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_WRAPPERLIKETYPE_H
#include <mylang/codegen/javagen/ir/types/WrapperLikeType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {
          /**
           * A numeric type for integers and floats
           */
          class HolderType: public virtual JavaType, public virtual WrapperLikeType
          {
            /**
             * Create a new simple java type
             */
          public:
            HolderType();

          public:
            virtual ~HolderType();

            /**
             * Generate an equality expression
             * @param lhs
             */
          public:
            virtual ::std::string genNewInstance(const ::std::string &expr) const = 0;

            /**
             * Generate an equality expression
             * @param lhs
             */
          public:
            virtual ::std::string genGet(const ::std::string &expr) const = 0;

            /**
             * Create a bit type
             */
          public:
            static ::std::shared_ptr<const HolderType> create(const Context &ctx,
                mylang::codegen::model::MType element);
          };
        }
      }
    }
  }
}
#endif
