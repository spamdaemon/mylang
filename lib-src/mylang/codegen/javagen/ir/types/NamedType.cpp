#include <mylang/codegen/CodeStream.h>
#include <mylang/codegen/javagen/ir/BlockGen.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/javagen/ir/JavaType.h>
#include <mylang/codegen/javagen/ir/types/NamedType.h>
#include <mylang/codegen/javagen/ir/Typename.h>
#include <mylang/codegen/model/ir/LiteralNamedType.h>
#include <mylang/codegen/model/types/NamedType.h>
#include <mylang/codegen/javagen/ir/ExportedJavaType.h>
#include <mylang/codegen/javagen/ir/exports/PrimitiveNamedType.h>
#include <optional>
#include <string>
#include <vector>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          NamedType::NamedType()

          {
          }

          NamedType::~NamedType()
          {
          }

          ::std::shared_ptr<const NamedType> NamedType::create(const Context &ctx,
              ::std::shared_ptr<const model::types::NamedType> model)
          {
            // the root impl is a named type whose base type is not a named type
            struct RootImpl: public NamedType
            {
              RootImpl(const Context&, ::std::shared_ptr<const model::types::NamedType> m,
                  Typename xname, JavaType::Ptr root)
                  : JavaType(xname, m), roottype(root)
              {
              }

              ~RootImpl()
              {
              }

              ExportedJavaTypePtr exportType(const Context &c) const override final
              {
                auto fqn = Context::getBuiltinTypename(optModel->name()->fullName("."));

                ExportedJavaTypePtr impl = roottype->exportTypeAs(c, fqn, self<NamedType>());
                if (!impl) {
                  impl = ExportedJavaType::createIdentity(self());
                }
                return c.registerType(impl);
              }

              bool isObject() const override final
              {
                return true;
              }

              bool isReference() const override final
              {
                return false;
              }
              bool writeClass(const Context &ctx) const override final
              {
                CodeStream out;
                bool generated = generate(ctx, out);
                if (generated) {
                  ctx.writeClass(name.fqn, out.getText());
                }
                return generated;
              }

              bool generate(const Context&, CodeStream &out) const override final
              {
                if (name.packageName.has_value()) {
                  out << "package " << name.packageName.value() << ";" << nl();
                }

                out << "public class " << name.simpleName << ' ';
                out.brace([&]
                {
                  out << "public final " << roottype->name << " value;" << nl();
                  out << "public " << name.simpleName << "(" << roottype->name << " value)";
                  out.brace([&]
                  {
                    out << "this.value = value;";
                  }) << nl();
                  out << "public " << name.simpleName << "(" << name.simpleName << " obj)";
                  out.brace([&]
                  {
                    out << "this.value=obj.value;";
                  });
                }) << nl();
                return true;
              }

              ::std::string genLiteral(const Context &ctx,
                  const model::ir::LiteralNamedType::LiteralNamedTypePtr &literal) const
                  override final
              {
                CodeStream out;
                ctx.blockGen->generate(ctx, literal->value, out);
                return "new " + name + '(' + out.getText() + ')';
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
              {
                return "new " + name + '(' + args.at(0) + ')';
              }

              ::std::string genCastToBaseType(const Variable &var) const override final
              {
                return var + ".value";
              }

              ::std::string genCastToRootType(const Variable &var) const override final
              {
                return var + ".value";
              }

              JavaType::Ptr roottype;
            };
            struct BaseImpl: public NamedType
            {
              BaseImpl(const Context&, ::std::shared_ptr<const model::types::NamedType> m,
                  Typename xname, NamedTypePtr base, ::std::shared_ptr<const RootImpl> root)
                  : JavaType(xname, m), basetype(base), roottype(root)
              {
              }

              ~BaseImpl()
              {
              }

              ExportedJavaTypePtr exportType(const Context &c) const override final
              {
                auto fqn = Context::getBuiltinTypename(optModel->name()->fullName("."));
                return ::std::make_shared<exports::PrimitiveNamedType>(c, fqn, self<NamedType>());
              }

              bool isReference() const override final
              {
                return false;
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                CodeStream out;
                bool generated = generate(ctx, out);
                if (generated) {
                  ctx.writeClass(name.fqn, out.getText());
                }
                return generated;
              }

              bool generate(const Context&, CodeStream &out) const override final
              {
                if (name.packageName.has_value()) {
                  out << "package " << name.packageName.value() << ";" << nl();
                }

                out << "public class " << name.simpleName << " extends " << basetype->name;
                out.brace(
                    [&]
                    {
                      out << "public " << name.simpleName << "(" << roottype->roottype->name
                          << " value)";
                      out.brace([&]
                      {
                        out << "super(value);";
                      }) << nl();
                      out << "public " << name.simpleName << "(" << roottype->name << " obj)";
                      out.brace([&]
                      {
                        out << "super(obj);";
                      });
                    }) << nl();
                return true;
              }

              ::std::string genLiteral(const Context &ctx,
                  const model::ir::LiteralNamedType::LiteralNamedTypePtr &literal) const
                  override final
              {
                CodeStream out;
                ctx.blockGen->generate(ctx, literal->value, out);
                return "new " + name + '(' + out.getText() + ')';
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
              {
                return "new " + name + '(' + args.at(0) + ')';
              }

              ::std::string genCastToBaseType(const Variable &var) const override final
              {
                return "(" + basetype->name + ")" + var;
              }

              ::std::string genCastToRootType(const Variable &var) const override final
              {
                return var + ".value";
              }
              NamedTypePtr basetype;
              ::std::shared_ptr<const RootImpl> roottype;
            };

            struct RefImpl: public NamedType
            {
              RefImpl(const Context&, ::std::shared_ptr<const model::types::NamedType> m,
                  Typename xname)
                  : JavaType(xname, m)
              {
              }

              ~RefImpl()
              {
              }

              ExportedJavaTypePtr exportType(const Context&) const override final
              {
                // FIXME: implement this properly
                return ExportedJavaType::createIdentity(self());
              }

              bool isReference() const override final
              {
                return true;
              }
              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                CodeStream out;
                bool generated = generate(ctx, out);
                if (generated) {
                  ctx.writeClass(name.fqn, out.getText());
                }
                return generated;
              }

              bool generate(const Context&, CodeStream&) const override final
              {
                return false;
              }

              ::std::string genLiteral(const Context &ctx,
                  const model::ir::LiteralNamedType::LiteralNamedTypePtr &literal) const
                  override final
              {
                return impl->genLiteral(ctx, literal);
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
              {
                return impl->genNewInstance(args);
              }

              ::std::string genCastToBaseType(const Variable &var) const override final
              {
                return impl->genCastToBaseType(var);
              }

              ::std::string genCastToRootType(const Variable &var) const override final
              {
                return impl->genCastToRootType(var);
              }

              ::std::shared_ptr<const NamedType> impl;
            };

            auto xname = ctx.getTypename(std::nullopt, model->name()->uniqueFullName("."));
            auto ref = ::std::make_shared<RefImpl>(ctx, model, xname);
            auto xref = ctx.registerType(ref);
            if (xref != ref) {
              // we're in a recursive call, so just return immediately
              return xref->self<NamedType>();
            }

            auto implTy = ctx.getType(model->resolve());
            auto rootTy = implTy->self<RootImpl>();
            if (rootTy) {
              ref->impl = ::std::make_shared<BaseImpl>(ctx, model, xname, rootTy, rootTy);
            } else {
              auto baseTy = implTy->self<BaseImpl>();
              if (baseTy) {
                ref->impl = ::std::make_shared<BaseImpl>(ctx, model, xname, baseTy,
                    baseTy->roottype);
              }
              ref->impl = ::std::make_shared<RootImpl>(ctx, model, xname, implTy);
            }
            return ref->impl;
          }

        }
      }
    }
  }
}
