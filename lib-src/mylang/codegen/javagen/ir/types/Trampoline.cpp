#include <mylang/codegen/javagen/ir/types/Trampoline.h>
#include <mylang/codegen/javagen/ir/types/FunctionType.h>
#include <mylang/codegen/model/types/Type.h>
#include <mylang/codegen/javagen/ir/Context.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          Trampoline::Trampoline()

          {
          }

          Trampoline::~Trampoline()
          {
          }

          ::std::shared_ptr<const Trampoline> Trampoline::create(const Context &ctx,
              ::std::shared_ptr<const model::types::FunctionType> element)
          {
            auto name = ctx.getTypename(::std::nullopt,
                "trampoline" + element->name()->uniqueLocalName());
            auto ty = ctx.findType(name);
            if (ty) {
              return ty->self<Trampoline>();
            }

            struct Impl: public Trampoline
            {
              Impl(const Context &ctx, ::std::shared_ptr<const model::types::FunctionType> xelement)
                  :
                      JavaType(ctx.getTypename("trampoline", xelement->name()->uniqueLocalName()),
                          nullptr), element(ctx.getType(xelement)->self<types::FunctionType>()),
                      isvoid(ctx.getType(xelement->returnType)->name.fqn == "void")
              {
              }

              ~Impl()
              {
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                CodeStream out;
                bool generated = generate(ctx, out);
                if (generated) {
                  ctx.writeClass(name.fqn, out.getText());
                }
                return generated;
              }

              bool generate(const Context &ctx, CodeStream &out) const override final
              {
                if (name.packageName.has_value()) {
                  out << "package " << name.packageName.value() << ";" << nl();
                }
                out << "public class " << name.simpleName << " implements " << element->name << ' ';
                Variable elemVar(VariableName("value"), element);

                out.brace([&]
                {
                  out << "public " << elemVar.type->name << " " << elemVar << "=null;" << nl();
                  out << "public " << name.simpleName << "() {}" << nl();

                  auto args = element->genSignature(ctx, out);
                  out.brace([&]
                  {
                    if (!isvoid) {
                      out << "return ";
                    }
                    out << element->genCall(elemVar, args) << ";";
                  });
                }) << nl();
                return true;
              }

              ::std::string genNewInstance(const ::std::vector<Variable>&) const override final
              {
                return "new " + name + "()";
              }

              ::std::string genSet(const Variable &holder, const ::std::string &expr) const
              override final
              {
                return holder + ".value = " + expr;
              }

              const ::std::shared_ptr<const types::FunctionType> element;
              const bool isvoid;
            }
            ;
            ::std::shared_ptr<const Trampoline> trampoline = ::std::make_shared<Impl>(ctx, element);
            return ctx.registerType(trampoline)->self<Trampoline>();
          }
        }
      }
    }
  }
}
