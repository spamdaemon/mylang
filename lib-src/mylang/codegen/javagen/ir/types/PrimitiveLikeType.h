#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PRIMITIVELIKETYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PRIMITIVELIKETYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_ORDERABLETYPE_H
#include <mylang/codegen/javagen/ir/types/OrderableType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A type that is primitive in the sense that is supports a lot of the same
           * functions as a primitive.
           */
          class PrimitiveLikeType: public virtual OrderableType
          {
          public:
            virtual ~PrimitiveLikeType();

            /**
             * Generate a constructor for a literal.
             * @param literal a literal
             */
          public:
            virtual ::std::string genLiteral(const model::MLiteral &literal) const = 0;

            /**
             * Generate a toLiteral string
             */
          public:
            virtual ::std::string genToLiteral(const JavaType::Ptr &ty,
                const JavaType::Variable &var) const =0;

          };
        }
      }
    }
  }
}
#endif
