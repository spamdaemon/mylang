#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_BYTETYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_BYTETYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_BOOLEANLIKETYPE_H
#include <mylang/codegen/javagen/ir/types/BitLikeType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PRIMITIVETYPE_H
#include <mylang/codegen/javagen/ir/types/PrimitiveType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_INTEGERTYPE_H
#include <mylang/codegen/javagen/ir/types/IntegerType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TONATIVESTRINGTYPE_H
#include <mylang/codegen/javagen/ir/types/ToNativeStringType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_NATIVETYPE_H
#include <mylang/codegen/javagen/ir/types/NativeType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TOBITSTYPE_H
#include <mylang/codegen/javagen/ir/types/ToBitsType.h>
#endif

#include <mylang/codegen/model/types/ByteType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class ByteType: public virtual JavaType, public virtual BitLikeType,
              public virtual PrimitiveType, public virtual ToNativeStringType,
              public virtual NativeType, public virtual ToBitsType
          {
            /**
             * Create a new simple java type
             */
          public:
            ByteType();

          public:
            ~ByteType();

            /**
             * Get the value of a byte as an unsigned integer
             * @param var a byte variable
             */
          public:
            virtual ::std::string genGetUnsignedInteger(
                const ::std::shared_ptr<const IntegerType> &resTy, const Variable &var) const = 0;

            /**
             * Get the value of a byte as an unsigned integer
             * @param var a byte variable
             */
          public:
            virtual ::std::string genGetSignedInteger(
                const ::std::shared_ptr<const IntegerType> &resTy, const Variable &var) const = 0;

            /**
             * Create a byte from a native byte.
             * @param expr an expression
             */
          public:
            virtual ::std::string fromNative(const ::std::string &expr) const = 0;

            /**
             * Create a native byte from a byte.
             * @param expr an expression
             */
          public:
            virtual ::std::string toNative(const ::std::string &expr) const = 0;

            /**
             * Create a bit type
             */
          public:
            static ::std::shared_ptr<const ByteType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::ByteType> model);
          };
        }
      }
    }
  }
}
#endif
