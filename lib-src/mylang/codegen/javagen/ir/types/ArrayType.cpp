#include <mylang/codegen/CodeStream.h>
#include <mylang/codegen/javagen/ir/BlockGen.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/javagen/ir/JavaType.h>
#include <mylang/codegen/javagen/ir/types/ArrayType.h>
#include <mylang/codegen/javagen/ir/types/IntegerType.h>
#include <mylang/codegen/javagen/ir/Typename.h>
#include <mylang/codegen/javagen/ir/ExportedJavaType.h>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <vector>
#include <mylang/codegen/model/ir/LiteralArray.h>
#include <mylang/codegen/model/types/ArrayType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          ArrayType::ArrayType()
          {
          }

          ArrayType::~ArrayType()
          {
          }

          ::std::shared_ptr<const ArrayType> ArrayType::create(const Context &ctx,
              ::std::shared_ptr<const model::types::ArrayType> model)
          {
            struct Impl: public ArrayType
            {
              Impl(const Context &ctx, ::std::shared_ptr<const model::types::ArrayType> m,
                  const Typename &elem)
                  : JavaType(ctx.getTypename(::std::nullopt, "array_" + elem.simpleName), m),
                      NativeType(elem + "[]"), element(elem)
              {
              }

              ~Impl()
              {
              }

              ExportedJavaTypePtr exportType(const Context&) const override final
              {
                return ExportedJavaType::createIdentity(self());
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                CodeStream out;
                bool generated = generate(ctx, out);
                if (generated) {
                  ctx.writeClass(name.fqn, out.getText());
                }
                return generated;
              }

              bool generate(const Context&, CodeStream &out) const override final
              {
                ::std::map<::std::string, ::std::string> replacements;
                replacements["array.package"] = name.packageName.value();
                replacements["array.class"] = name.simpleName;
                replacements["element.type"] = element.fqn;
                out.insertTemplateFile("templates/codegen/javagen/ir/ArrayType.java", replacements);
                return true;
              }

              ::std::string genLiteral(const Context &ctx,
                  const model::ir::LiteralArray::LiteralArrayPtr &literal) const override final
              {
                ::std::string arr;
                arr = "new " + element + "[] {";
                const char *sep = "";
                for (auto v : literal->elements) {
                  arr += sep;
                  CodeStream out;
                  ctx.blockGen->generate(ctx, v, out);
                  arr = arr + out.getText();
                  sep = ", ";
                }
                arr += '}';
                return name + ".create(" + arr + ")";
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
              {
                ::std::string arr;
                arr = "new " + element + "[] {";
                const char *sep = "";
                for (auto v : args) {
                  arr += sep;
                  arr = arr + v;
                  sep = ", ";
                }
                arr += '}';
                return name + ".create(" + arr + ")";
              }

              ::std::string fromNative(const ::std::string &expr) const
              {
                return name + ".create(" + expr + ")";
              }

              ::std::string toNative(const ::std::string &expr) const
              {
                return "(" + expr + ").toArray()";
              }

              ::std::string genLength(const ::std::shared_ptr<const IntegerType> &ty,
                  const Variable &arr) const override final
              {
                return ty->createFromInt32(arr + ".length()");
              }

              ::std::string genIndex(const Ptr&, const Variable &arr, const Variable &index) const
              override final
              {
                auto ity = index.type->self<IntegerType>();
                return arr + ".get(" + ity->getInt32(index) + ')';
              }

              ::std::string genSubrange(const Ptr&, const Variable &arr, const Variable &start,
                  const Variable &end) const override final
              {
                auto ity = start.type->self<IntegerType>();
                auto jty = end.type->self<IntegerType>();
                return arr + ".subrange(" + ity->getInt32(start) + ", " + jty->getInt32(end) + ")";
              }

              ::std::string genConcatenate(const Ptr&, const Variable &lhs,
                  const Variable &rhs) const override final
              {
                return name + ".create(" + lhs + ", " + rhs + ")";
              }

              const Typename element;
            };
            auto element = ctx.getType(model->element);
            return ::std::make_shared<Impl>(ctx, model, element->name);
          }

        }
      }
    }
  }
}
