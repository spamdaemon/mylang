#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_OPTTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_OPTTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_WRAPPERLIKETYPE_H
#include <mylang/codegen/javagen/ir/types/WrapperLikeType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_BOOLEANTYPE_H
#include <mylang/codegen/javagen/ir/types/BooleanType.h>
#endif

#include <mylang/codegen/model/types/OptType.h>
#include <mylang/codegen/model/ir/LiteralOptional.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class OptType: public virtual JavaType, public virtual WrapperLikeType
          {
            /**
             * Create a new simple java type
             */
          public:
            OptType();

          public:
            virtual ~OptType();

            /**
             * Generate a constructor for a literal.
             * @param literal a literal
             */
          public:
            virtual ::std::string genLiteral(const Context &ctx,
                const model::ir::LiteralOptional::LiteralOptionalPtr &literal) const = 0;

            /**
             * Generate an equality expression
             * @param lhs
             */
          public:
            virtual ::std::string genIsPresent(const ::std::shared_ptr<const BooleanType> &ty,
                const Variable &opt) const = 0;

            /**
             * Generate an equality expression
             * @param lhs
             */
          public:
            virtual ::std::string genIsPresent(const ::std::shared_ptr<const BooleanType> &ty,
                const ::std::string &expr) const = 0;

            /**
             * Generate an equality expression
             * @param lhs
             */
          public:
            virtual ::std::string genNewInstance(const ::std::string &expr) const = 0;

            /**
             * Generate an equality expression
             * @param lhs
             */
          public:
            virtual ::std::string genNIL() const = 0;

            /**
             * Create a bit type
             */
          public:
            static ::std::shared_ptr<const OptType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::OptType> model);
          };
        }
      }
    }
  }
}
#endif
