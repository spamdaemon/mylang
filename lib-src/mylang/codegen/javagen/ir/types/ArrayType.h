#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_ARRAYTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_ARRAYTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_SEQUENCETYPE_H
#include <mylang/codegen/javagen/ir/types/SequenceType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_NATIVETYPE_H
#include <mylang/codegen/javagen/ir/types/NativeType.h>
#endif

#include <mylang/codegen/model/types/ArrayType.h>
#include <mylang/codegen/model/ir/LiteralArray.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class ArrayType: public virtual JavaType, public virtual SequenceType,
              public virtual NativeType
          {
            /**
             * Create a new simple java type
             */
          public:
            ArrayType();

          public:
            virtual ~ArrayType();

            /**
             * Generate a constructor for a literal.
             * @param literal a literal
             */
          public:
            virtual ::std::string genLiteral(const Context &ctx,
                const model::ir::LiteralArray::LiteralArrayPtr &literal) const = 0;

            /**
             * Create a bit type
             */
          public:
            static ::std::shared_ptr<const ArrayType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::ArrayType> model);
          };
        }
      }
    }
  }
}
#endif
