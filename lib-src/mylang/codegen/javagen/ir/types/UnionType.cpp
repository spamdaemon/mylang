#include <mylang/codegen/CodeStream.h>
#include <mylang/codegen/javagen/ir/BlockGen.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/javagen/ir/types/OptType.h>
#include <mylang/codegen/javagen/ir/types/HolderType.h>
#include <mylang/codegen/javagen/ir/types/UnionType.h>
#include <mylang/codegen/javagen/ir/Typename.h>
#include <mylang/codegen/javagen/ir/exports/ExportedNamedUnionType.h>
#include <mylang/codegen/model/ir/Literal.h>
#include <mylang/codegen/model/types/OptType.h>
#include <mylang/names/Name.h>
#include <stddef.h>
#include <functional>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <vector>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          UnionType::UnionType()

          {
          }

          UnionType::~UnionType()
          {
          }

          ::std::shared_ptr<const UnionType> UnionType::create(const Context &ctx,
              ::std::shared_ptr<const model::types::UnionType> model)
          {
            struct Impl: public UnionType
            {
              Impl(const Context &ctx, ::std::shared_ptr<const model::types::UnionType> type)
                  :
                      JavaType(
                          ctx.getTypename(std::nullopt, "union_" + type->name()->uniqueLocalName()),
                          type), model(type),
                      discriminant(ctx.getType(type->discriminant.type)->name)
              {
                for (size_t i = 0; i < type->members.size(); ++i) {
                  members[type->members.at(i).name] = i;
                }
              }

              ~Impl()
              {
              }

              ExportedJavaTypePtr exportTypeAs(const Context &c, const Typename &as,
                  const NamedTypePtr &ty) const override final
              {
                return ::std::make_shared<exports::ExportedNamedUnionType>(c, as, ty);
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                CodeStream out;
                bool generated = generate(ctx, out);
                if (generated) {
                  ctx.writeClass(name.fqn, out.getText());
                }
                return generated;
              }

              bool generate(const Context &ctx, CodeStream &out) const override final
              {
                if (name.packageName.has_value()) {
                  out << "package " << name.packageName.value() << ";" << nl();
                }
                out << "public class " << name.simpleName << ' ';
                out.brace([&]
                {
                  out << "public final " << discriminant << " discriminant;" << nl();
                  out << "private final java.lang.Object value;" << nl();
                  out << "private " << name.simpleName << "(" << discriminant <<" d, java.lang.Object value) { this.discriminant = d; this.value=value; }" << nl();

                // create an inner class for each member type
                  for (size_t i = 0; i < model->members.size(); ++i) {
                    auto memberType = ctx.getType(model->members.at(i).type)->name;
                    auto elemTy = ctx.getType(model->members.at(i).type);
                    auto variant = HolderType::create(ctx, model->members.at(i).type);

                    ::std::string d("discriminant_" + ::std::to_string(i));
                    // variant class definition
                    out << "private static final " << discriminant << ' ' << d << " = ";
                    ctx.blockGen->generate(ctx, model->members.at(i).discriminant, out);
                    out << ";" << nl();

                    // new instance for variant
                    out << "public static " << name.simpleName << " create" << i << "("
                        << memberType << " value)";
                    out.brace(
                        [&]
                        {
                          out << "return new " << name.simpleName << "(" << d << ", "
                              << variant->genNewInstance("value") << ");";
                        }) << nl();

                    // getter for the variant

                    out << "public final " << elemTy->name << " get" << i << "()";
                    out.brace(
                        [&]
                        {
                          out << "return " << variant->genGet("(" + variant->name + ")(this.value)")
                              << ";";
                        }) << nl();
                  }
                });
                return true;
              }

              ::std::string genLiteral(const Context &ctx,
                  const model::ir::LiteralUnion::LiteralUnionPtr &literal) const override final
              {
                auto i = members.find(literal->member);
                if (i == members.end()) {
                  throw ::std::runtime_error("No such member");
                }
                CodeStream out;
                ctx.blockGen->generate(ctx, literal->value, out);

                return name + ".create" + ::std::to_string(i->second) + "(" + out.getText() + ")";
              }

              ::std::string genNewInstance(const ::std::string &member, const Variable &value) const
              override final
              {
                auto i = members.find(member);
                if (i == members.end()) {
                  throw ::std::runtime_error("No such member");
                }

                return name + ".create" + ::std::to_string(i->second) + "(" + value + ")";
              }

              ::std::string genGetDiscriminant(const Ptr&, const Variable &str) const
              override final
              {
                return str + ".discriminant";
              }

              ::std::string genGetMember(const Ptr&, const Variable &str,
                  const ::std::string &member) const override final
              {
                auto i = members.find(member);
                if (i == members.end()) {
                  throw ::std::runtime_error("No such member");
                }
                return str + ".get" + ::std::to_string(i->second) + "()";
              }

              ::std::string genNewInstance(const ::std::vector<Variable>&) const override final
              {
                return unexpected("direct construction of union");
              }

              ::std::shared_ptr<const model::types::UnionType> model;
              const Typename discriminant;
              ::std::map<::std::string, size_t> members;
            };

            return ::std::make_shared<Impl>(ctx, model);
          }
        }
      }
    }
  }
}
