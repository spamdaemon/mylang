#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_FUNCTIONTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_FUNCTIONTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_ORDERABLETYPE_H
#include <mylang/codegen/javagen/ir/types/OrderableType.h>
#endif

#include <mylang/codegen/model/types/FunctionType.h>
#include <string>
#include <vector>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class FunctionType: public virtual JavaType, public virtual OrderableType
          {
            /**
             * Create a new simple java type
             */
          public:
            FunctionType();

          public:
            virtual ~FunctionType();

            /**
             * Generate an equality expression
             * @param lhs
             */
          public:
            virtual ::std::string genCall(const Variable &fn,
                const ::std::vector<Variable> &args) const = 0;

            /**
             * Generate the function prototype. There will be no trailing semicolon or brace.
             * @param out an putput stream
             * @return the names of the arguments
             */
          public:
            virtual ::std::vector<Variable> genSignature(const Context &ctx,
                CodeStream &out) const = 0;

            /**
             * Create a bit type
             */
          public:
            static ::std::shared_ptr<const FunctionType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::FunctionType> model);
          };
        }
      }
    }
  }
}
#endif
