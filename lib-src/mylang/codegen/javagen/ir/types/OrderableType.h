#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_ORDERABLETYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_ORDERABLETYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_COMPARABLETYPE_H
#include <mylang/codegen/javagen/ir/types/ComparableType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class OrderableType: public virtual ComparableType
          {
          public:
            virtual ~OrderableType();

            /**
             * Generate an equality expression
             * @param lhs
             * @param rhs
             * @retutrn the expression lhs < rhs
             */
          public:
            virtual ::std::string genLT(const JavaType::Ptr &ty, const JavaType::Variable &lhs,
                const JavaType::Variable &rhs) const = 0;

            /**
             * Generate an equality expression
             * @param lhs
             * @param rhs
             * @retutrn the expression lhs <= rhs
             */
          public:
            virtual ::std::string genLTE(const JavaType::Ptr &ty, const JavaType::Variable &lhs,
                const JavaType::Variable &rhs) const = 0;
          };
        }
      }
    }
  }
}
#endif
