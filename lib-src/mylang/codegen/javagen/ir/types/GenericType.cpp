#include <mylang/codegen/javagen/ir/types/GenericType.h>
#include <mylang/codegen/javagen/ir/Context.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          GenericType::GenericType()

          {
          }

          GenericType::~GenericType()
          {
          }

          ::std::shared_ptr<const GenericType> GenericType::create(const Context &ctx,
              ::std::shared_ptr<const model::types::GenericType> model)
          {
            struct Impl: public GenericType
            {
              Impl(const Context &ctx, model::MType m)
                  : JavaType(ctx.getBuiltinTypename("java.lang.Object"), m)
              {
              }

              ~Impl()
              {
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                CodeStream out;
                bool generated = generate(ctx, out);
                if (generated) {
                  ctx.writeClass(name.fqn, out.getText());
                }
                return generated;
              }

              bool generate(const Context&, CodeStream&) const override final
              {
                return false;
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
              {
                return args.at(0).name;
              }

            };

            return ::std::make_shared<Impl>(ctx, model);
          }

        }
      }
    }
  }
}
