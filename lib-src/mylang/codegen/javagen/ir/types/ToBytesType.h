#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TOBYTESTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TOBYTESTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class ToBytesType
          {
          public:
            virtual ~ToBytesType();

          public:
            virtual ::std::string genFromBytes(const JavaType::Variable &var) const = 0;
            virtual ::std::string genToBytes(const JavaType::Ptr &resTy,
                const JavaType::Variable &var) const = 0;
          };
        }
      }
    }
  }
}
#endif
