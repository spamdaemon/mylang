#include <mylang/codegen/javagen/ir/types/MutableType.h>
#include <mylang/codegen/javagen/ir/Context.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          MutableType::MutableType()

          {
          }

          MutableType::~MutableType()
          {
          }

          ::std::shared_ptr<const MutableType> MutableType::create(const Context &ctx,
              ::std::shared_ptr<const model::types::MutableType> model)
          {
            struct Impl: public MutableType
            {
              Impl(const Context &ctx, ::std::shared_ptr<const model::types::MutableType> xmodel)
                  :
                      JavaType(
                          ctx.getTypename(std::nullopt, "ptr_" + xmodel->name()->uniqueLocalName()),
                          xmodel), element(ctx.getType(xmodel->element)->name)
              {
              }

              ~Impl()
              {
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                CodeStream out;
                bool generated = generate(ctx, out);
                if (generated) {
                  ctx.writeClass(name.fqn, out.getText());
                }
                return generated;
              }

              bool generate(const Context&, CodeStream &out) const override final
              {
                if (name.packageName.has_value()) {
                  out << "package " << name.packageName.value() << ";" << nl();
                }
                out << "public class " << name.simpleName << ' ';
                out.brace([&]
                {
                  out << "public " << element << " value;" << nl();
                  out << "public " << name.simpleName << "(" << element << " value)";
                  out.brace([&]
                  {
                    out << "this.value = value;";
                  });
                }) << nl();
                return true;
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
              {
                return "new " + name.fqn + '(' + args.at(0) + ')';
              }

              ::std::string genGet(const Ptr&, const Variable &opt) const override final
              {
                return opt + ".value";
              }
              ::std::string genSetValue(const Variable &obj, const Variable &value) const
              override final
              {
                return obj + ".value = " + value;
              }
              Typename element;
            };

            return ::std::make_shared<Impl>(ctx, model);
          }
        }
      }
    }
  }
}
