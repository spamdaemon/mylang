#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_NUMERICTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_NUMERICTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class NumericType
          {

          public:
            virtual ~NumericType() = 0;

            /**
             * Generate an equality expression
             * @param lhs
             * @param rhs
             * @retutrn an equality expression lhs==rhs
             */
          public:
            virtual ::std::string genNegate(const JavaType::Ptr &ty,
                const JavaType::Variable &op) const = 0;

            /**
             * Generate an inequality expression
             * @param lhs
             * @param rhs
             * @retutrn an inequality expression lhs==rhs
             */
          public:
            virtual ::std::string genAdd(const JavaType::Ptr &ty, const JavaType::Variable &lhs,
                const JavaType::Variable &rhs) const = 0;

            /**
             * Generate an equality expression
             * @param lhs
             * @param rhs
             * @retutrn the expression lhs < rhs
             */
          public:
            virtual ::std::string genSub(const JavaType::Ptr &ty, const JavaType::Variable &lhs,
                const JavaType::Variable &rhs) const = 0;

            /**
             * Generate an equality expression
             * @param lhs
             * @param rhs
             * @retutrn the expression lhs <= rhs
             */
          public:
            virtual ::std::string genMul(const JavaType::Ptr &ty, const JavaType::Variable &lhs,
                const JavaType::Variable &rhs) const = 0;

            /**
             * Generate an equality expression
             * @param lhs
             * @param rhs
             * @retutrn the expression lhs < rhs
             */
          public:
            virtual ::std::string genDiv(const JavaType::Ptr &ty, const JavaType::Variable &lhs,
                const JavaType::Variable &rhs) const = 0;

            /**
             * Generate an equality expression
             * @param lhs
             * @param rhs
             * @retutrn the expression lhs <= rhs
             */
          public:
            virtual ::std::string genMod(const JavaType::Ptr &ty, const JavaType::Variable &lhs,
                const JavaType::Variable &rhs) const = 0;

            /**
             * Generate an equality expression
             * @param lhs
             * @param rhs
             * @retutrn the expression lhs <= rhs
             */
          public:
            virtual ::std::string genRem(const JavaType::Ptr &ty, const JavaType::Variable &lhs,
                const JavaType::Variable &rhs) const = 0;
          };
        }
      }
    }
  }
}
#endif
