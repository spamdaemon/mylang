#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_BITLIKETYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_BITLIKETYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class BitLikeType
          {
          public:
            virtual ~BitLikeType() = 0;

            /**
             * Generate an equality expression
             * @param lhs
             * @param rhs
             * @retutrn an equality expression lhs==rhs
             */
          public:
            virtual ::std::string genNot(const JavaType::Ptr &ty,
                const JavaType::Variable &op) const = 0;

            /**
             * Generate an equality expression
             * @param lhs
             * @param rhs
             * @retutrn an equality expression lhs==rhs
             */
          public:
            virtual ::std::string genAnd(const JavaType::Ptr &ty, const JavaType::Variable &lhs,
                const JavaType::Variable &rhs) const = 0;

            /**
             * Generate an equality expression
             * @param lhs
             * @param rhs
             * @retutrn an equality expression lhs==rhs
             */
          public:
            virtual ::std::string genOr(const JavaType::Ptr &ty, const JavaType::Variable &lhs,
                const JavaType::Variable &rhs) const = 0;

            /**
             * Generate an equality expression
             * @param lhs
             * @param rhs
             * @retutrn an equality expression lhs==rhs
             */
          public:
            virtual ::std::string genXor(const JavaType::Ptr &ty, const JavaType::Variable &lhs,
                const JavaType::Variable &rhs) const = 0;
          };
        }
      }
    }
  }
}
#endif
