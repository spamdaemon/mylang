#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_NATIVEINTEGERTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_NATIVEINTEGERTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_INTEGERTYPE_H
#include <mylang/codegen/javagen/ir/types/IntegerType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class NativeIntegerType: public IntegerType
          {
            /**
             * Create a new simple java type
             */
          public:
            NativeIntegerType();

          public:
            ~NativeIntegerType();

            /**
             * An integer that fits into a native type.
             * @return an integer type that is one of byte, short, int, or long
             * @throws ::std::invalid_argument if the model does not fit
             */
          public:
            static ::std::shared_ptr<const NativeIntegerType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::IntegerType> model);

            /**
             * An integer that fits into a native type.
             * @return an integer type that is one of byte, short, int, or long, or nullptr
             */
          public:
            static ::std::shared_ptr<const NativeIntegerType> tryCreate(const Context &ctx,
                ::std::shared_ptr<const model::types::IntegerType> model);

          };
        }
      }
    }
  }
}
#endif
