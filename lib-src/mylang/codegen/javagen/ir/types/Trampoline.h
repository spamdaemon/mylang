#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TRAMPOLINE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TRAMPOLINE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif
#include <mylang/codegen/model/types/FunctionType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {
          /**
           * A numeric type for integers and floats
           */
          class Trampoline: public virtual JavaType
          {
            /**
             * Create a new simple java type
             */
          public:
            Trampoline();

          public:
            virtual ~Trampoline();

            /**
             * Generate an equality expression
             * @param lhs
             */
          public:
            virtual ::std::string genSet(const Variable &self, const ::std::string &expr) const = 0;

            /**
             * Create a bit type
             */
          public:
            static ::std::shared_ptr<const Trampoline> create(const Context &ctx,
                ::std::shared_ptr<const model::types::FunctionType> element);
          };
        }
      }
    }
  }
}
#endif
