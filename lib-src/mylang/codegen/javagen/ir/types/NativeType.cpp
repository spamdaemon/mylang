#include <mylang/codegen/javagen/ir/types/NativeType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          NativeType::NativeType(const ::std::string &xnativeType)
              : nativeType(xnativeType)
          {
          }

          NativeType::~NativeType()
          {
          }

          ::std::string NativeType::toNative(const Variable &expr) const
          {
            return toNative(expr.name.fqn);
          }

        }
      }
    }
  }
}
