#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_SEQUENCETYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_SEQUENCETYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_INTEGERTYPE_H
#include <mylang/codegen/javagen/ir/types/IntegerType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class SequenceType
          {
          public:
            virtual ~SequenceType() = 0;

            /**
             * Generate an equality expression
             * @param lhs
             * @param rhs
             * @retutrn an equality expression lhs==rhs
             */
          public:
            virtual ::std::string genLength(const ::std::shared_ptr<const IntegerType> &ty,
                const JavaType::Variable &arr) const = 0;

            /**
             * Generate an equality expression
             * @param lhs
             * @param rhs
             * @return an equality expression lhs==rhs
             */
          public:
            virtual ::std::string genIndex(const JavaType::Ptr &ty, const JavaType::Variable &arr,
                const JavaType::Variable &index) const = 0;

            /**
             * Generate an inequality expression
             * @param lhs
             * @param rhs
             * @return an inequality expression lhs==rhs
             */
          public:
            virtual ::std::string genSubrange(const JavaType::Ptr &ty,
                const JavaType::Variable &arr, const JavaType::Variable &start,
                const JavaType::Variable &end) const = 0;

            /**
             * Generate an inequality expression
             * @param lhs
             * @param rhs
             * @return an inequality expression lhs==rhs
             */
          public:
            virtual ::std::string genConcatenate(const JavaType::Ptr &ty,
                const JavaType::Variable &lhs, const JavaType::Variable &rhs) const = 0;
          };
        }
      }
    }
  }
}
#endif
