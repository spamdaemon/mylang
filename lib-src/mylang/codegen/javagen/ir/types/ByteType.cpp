#include <mylang/codegen/javagen/ir/types/ByteType.h>
#include <mylang/codegen/model/ir/LiteralByte.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/javagen/ir/exports/PrimitiveExportedType.h>
#include <mylang/codegen/javagen/ir/exports/PrimitiveNamedType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          ByteType::ByteType()

          {
          }

          ByteType::~ByteType()
          {
          }

          ::std::shared_ptr<const ByteType> ByteType::create(const Context &ctx,
              ::std::shared_ptr<const model::types::ByteType> model)
          {
            struct Impl: public ByteType
            {
              Impl(const Context &ctx, model::MType m)
                  : JavaType(ctx.getBuiltinTypename("byte").retag(Typename::TAG_BYTE), m),
                      NativeType("byte")
              {
              }

              ~Impl()
              {
              }

              Ptr getBoxedType() const
              {
                return Context::getBuiltinType(Context::getBuiltinTypename("java.lang.Byte"),
                    optModel);
              }

              ::std::string box(const ::std::string &expr) const
              {
                return "java.lang.Byte.valueOf(" + expr + ")";
              }

              ::std::string unbox(const ::std::string &expr)
              {
                return "((" + expr + ").byteValue())";
              }

              ExportedJavaTypePtr exportType(const Context&) const override final
              {
                return ::std::make_shared<exports::PrimitiveExportedType>(self());
              }

              ExportedJavaTypePtr exportTypeAs(const Context &c, const Typename &as,
                  const NamedTypePtr &ty) const override final
              {
                return ::std::make_shared<exports::PrimitiveNamedType>(c, as, ty);
              }

              bool isObject() const override final
              {
                return false;
              }

              bool writeClass(const Context&) const override final
              {
                return false;
              }

              bool generate(const Context&, CodeStream&) const override final
              {
                return false;
              }
              ::std::string toNative(const ::std::string &expr) const override final
              {
                return expr;
              }
              ::std::string fromNative(const ::std::string &expr) const override final
              {
                return expr;
              }
              ::std::string toNativeString(const ::std::string &expr) const override final
              {
                return "java.lang.String.format(\"%02x\"," + expr + ")";
              }

              ::std::string genFromBits(const JavaType::Variable &var) const override final
              {
                // TODO
                return "";
              }

              ::std::string genToBits(const JavaType::Ptr &resTy,
                  const JavaType::Variable &var) const
                  override final
              {
                // TODO
                return "";
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
              {
                // default
                if (args.empty()) {
                  return "((byte)0)";
                }
                auto ity = args.at(0).type->self<IntegerType>();
                if (ity) {
                  return ity->getInt8(args.at(0));
                }
                return args.at(0).name;
              }

              ::std::string genGetUnsignedInteger(const ::std::shared_ptr<const IntegerType> &resTy,
                  const Variable &var) const override final
              {
                return resTy->createFromInt32(var + " & 0x0ff");
              }

              ::std::string genGetSignedInteger(const ::std::shared_ptr<const IntegerType> &resTy,
                  const Variable &var) const override final
              {
                return resTy->createFromInt32(var.name);
              }

              ::std::string genLiteral(const model::MLiteral &literal) const override final
              {
                auto lit = literal->self<model::ir::LiteralByte>();
                return "((byte)" + ::std::to_string((unsigned int) lit->value) + ")";
              }

              ::std::string genToLiteral(const Ptr&, const Variable &var) const override final
              {
                return "java.lang.String.format(\"%02x\"," + var + ")";
              }

              ::std::string genEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + " == " + rhs;
              }
              ::std::string genNEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + " != " + rhs;
              }

              ::std::string genLT(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return "(" + lhs + "&0xff) < (" + rhs + " & 0x0ff)";
              }

              ::std::string genLTE(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return "(" + lhs + "&0xff) <= (" + rhs + " & 0x0ff)";
              }

              ::std::string genNot(const Ptr&, const Variable &op) const override final
              {
                return "(byte)(~" + op + ")";
              }

              ::std::string genAnd(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return "(byte)(" + lhs + " & " + rhs + ")";
              }
              ::std::string genOr(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return "(byte)(" + lhs + " | " + rhs + ")";
              }

              ::std::string genXor(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return "(byte)(" + lhs + " ^ " + rhs + ")";
              }

            };

            return ::std::make_shared<Impl>(ctx, model);
          }

        }
      }
    }
  }
}
