#include <mylang/codegen/javagen/ir/types/OptType.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/javagen/ir/BlockGen.h>
#include <mylang/codegen/javagen/ir/exports/ExportedOptionalType.h>
#include <mylang/codegen/javagen/ir/exports/PrimitiveNamedType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          OptType::OptType()

          {
          }

          OptType::~OptType()
          {
          }

          ::std::shared_ptr<const OptType> OptType::create(const Context &ctx,
              ::std::shared_ptr<const model::types::OptType> model)
          {
            struct Impl: public OptType
            {
              Impl(const Context &ctx, ::std::shared_ptr<const model::types::OptType> xmodel)
                  :
                      JavaType(
                          ctx.getTypename(std::nullopt, "opt_" + xmodel->name()->uniqueLocalName()),
                          xmodel), element(ctx.getType(xmodel->element)->name)
              {
              }

              ~Impl()
              {
              }

              ExportedJavaTypePtr exportType(const Context &c) const override final
              {
                return ::std::make_shared<exports::ExportedOptionalType>(c, self<OptType>());
              }

              ExportedJavaTypePtr exportTypeAs(const Context &c, const Typename &as,
                  const NamedTypePtr &ty) const override final
              {
                return ::std::make_shared<exports::PrimitiveNamedType>(c, as, ty);
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                CodeStream out;
                bool generated = generate(ctx, out);
                if (generated) {
                  ctx.writeClass(name.fqn, out.getText());
                }
                return generated;
              }

              bool generate(const Context&, CodeStream &out) const override final
              {
                if (name.packageName.has_value()) {
                  out << "package " << name.packageName.value() << ";" << nl();
                }
                out << "public class " << name.simpleName << ' ';
                out.brace([&]
                {
                  out << "private static " << element << " DEFAULT;" << nl();
                  out << "public static final " << name.simpleName << " NIL = new " << name.simpleName<< "(DEFAULT);" << nl();
                  out << "public final " << element << " value;" << nl();
                  out << "public " << name.simpleName << "(" << element << " value)";
                  out.brace([&]
                  {
                    out << "this.value = value;";
                  });
                }) << nl();
                return true;
              }

              ::std::string genLiteral(const Context &ctx,
                  const model::ir::LiteralOptional::LiteralOptionalPtr &literal) const
                  override final
              {
                if (literal->value) {
                  CodeStream out;
                  ctx.blockGen->generate(ctx, literal->value, out);
                  return "new " + name + '(' + out.getText() + ')';
                } else {
                  return genNIL();
                }
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
              {
                if (args.empty()) {
                  return genNIL();
                } else {
                  return genNewInstance(args.at(0).name);
                }
              }

              ::std::string genNewInstance(const ::std::string &expr) const override final
              {
                return "new " + name + '(' + expr + ')';
              }

              ::std::string genNIL() const override final
              {
                return name + ".NIL";
              }

              ::std::string genGet(const Ptr&, const Variable &opt) const override final
              {
                return opt + ".value";
              }

              ::std::string genIsPresent(const ::std::shared_ptr<const BooleanType>&,
                  const Variable &opt) const override final
              {
                return opt + "!=" + name + ".NIL";
              }

              ::std::string genIsPresent(const ::std::shared_ptr<const BooleanType>&,
                  const ::std::string &expr) const
                  override final
              {
                return "(" + expr + ")" + "!=" + name + ".NIL";
              }

              Typename element;
            }
            ;

            return ::std::make_shared<Impl>(ctx, model);
          }
        }
      }
    }
  }
}
