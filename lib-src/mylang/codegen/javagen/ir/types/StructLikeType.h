#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_STRUCTLIKETYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_STRUCTLIKETYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class StructLikeType
          {

          public:
            virtual ~StructLikeType() = 0;

            /**
             * Generate an equality expression
             * @param lhs
             * @param rhs
             * @retutrn an equality expression lhs==rhs
             */
          public:
            virtual ::std::string genGetMember(const JavaType::Ptr &ty,
                const JavaType::Variable &str, const ::std::string &member) const = 0;
          };
        }
      }
    }
  }
}
#endif
