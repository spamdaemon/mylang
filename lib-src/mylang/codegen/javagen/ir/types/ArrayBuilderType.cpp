#include <mylang/codegen/CodeStream.h>
#include <mylang/codegen/javagen/ir/BlockGen.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/javagen/ir/JavaType.h>
#include <mylang/codegen/javagen/ir/types/ArrayType.h>
#include <mylang/codegen/javagen/ir/types/ArrayBuilderType.h>
#include <mylang/codegen/javagen/ir/types/IntegerType.h>
#include <mylang/codegen/javagen/ir/Typename.h>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <vector>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          ArrayBuilderType::ArrayBuilderType()
          {
          }

          ArrayBuilderType::~ArrayBuilderType()
          {
          }

          ::std::shared_ptr<const ArrayBuilderType> ArrayBuilderType::create(const Context &ctx,
              ::std::shared_ptr<const model::types::ArrayType> model)
          {
            struct Impl: public ArrayBuilderType
            {
              Impl(const Context &ctx, mylang::codegen::model::MType xelement)
                  :
                      JavaType(
                          ctx.getTypename(::std::nullopt,
                              "arraybuilder_" + xelement->name()->uniqueLocalName()), nullptr),
                      element(ctx.getType(xelement))
              {
              }

              ~Impl()
              {
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                CodeStream out;
                bool generated = generate(ctx, out);
                if (generated) {
                  ctx.writeClass(name.fqn, out.getText());
                }
                return generated;
              }

              bool generate(const Context&, CodeStream &out) const override final
              {
                ::std::map<::std::string, ::std::string> replacements;
                replacements["builder.package"] = name.packageName.value();
                replacements["builder.class"] = name.simpleName;
                replacements["element.type"] = element->name.fqn;
                out.insertTemplateFile("templates/codegen/javagen/ir/ArrayBuilderType.java",
                    replacements);
                return true;
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
              {
                if (args.size() == 1) {
                  auto intPtr = args.at(0).type->self<IntegerType>();
                  return name + ".create(" + intPtr->getInt32(args.at(0)) + ")";
                } else {
                  return name + ".create(0)";
                }
              }

              ::std::string genBuild(const Ptr &ty, const Variable &b) const override final
              {
                auto arrTy = ty->self<ArrayType>();
                return arrTy->fromNative("(" + b + ".build())");
              }

              void genAppend(const Variable &b, const Variable &v, CodeStream &out) const
              override final
              {
                out << b;
                if (v.type->isSameType(element)) {
                  out << ".appendElement(" << v << ");";
                } else {
                  auto arrTy = v.type->self<ArrayType>();
                  // FIXME: if we have subranges, then we're making two copies, which is not ideal!
                  out << ".appendElements(" << arrTy->toNative(v) << ");";
                }
              }

              const Ptr element;
            };
            return ::std::make_shared<Impl>(ctx, model->element);
          }

        }
      }
    }
  }
}
