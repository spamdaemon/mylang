#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_COMPARABLETYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_COMPARABLETYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class ComparableType
          {
          public:
            virtual ~ComparableType() = 0;

            /**
             * Generate an equality expression
             * @param lhs
             * @param rhs
             * @retutrn an equality expression lhs==rhs
             */
          public:
            virtual ::std::string genEQ(const JavaType::Ptr &ty, const JavaType::Variable &lhs,
                const JavaType::Variable &rhs) const = 0;

            /**
             * Generate an inequality expression
             * @param lhs
             * @param rhs
             * @retutrn an inequality expression lhs==rhs
             */
          public:
            virtual ::std::string genNEQ(const JavaType::Ptr &ty, const JavaType::Variable &lhs,
                const JavaType::Variable &rhs) const = 0;

          };
        }
      }
    }
  }
}
#endif
