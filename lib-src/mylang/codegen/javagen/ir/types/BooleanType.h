#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_BOOLEANTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_BOOLEANTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_BOOLEANLIKETYPE_H
#include <mylang/codegen/javagen/ir/types/BitLikeType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PRIMITIVETYPE_H
#include <mylang/codegen/javagen/ir/types/PrimitiveType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TONATIVESTRINGTYPE_H
#include <mylang/codegen/javagen/ir/types/ToNativeStringType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_NATIVETYPE_H
#include <mylang/codegen/javagen/ir/types/NativeType.h>
#endif

#include <mylang/codegen/model/types/BooleanType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class BooleanType: public virtual JavaType, public virtual BitLikeType,
              virtual public PrimitiveType, public virtual ToNativeStringType,
              public virtual NativeType
          {
            /**
             * Create a new simple java type
             */
          public:
            BooleanType();

          public:
            virtual ~BooleanType();

          public:
            virtual ::std::string genLiteral(bool value) const = 0;

            /**
             * Create a bit type
             */
          public:
            static ::std::shared_ptr<const BooleanType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::BooleanType> model);

          };
        }
      }
    }
  }
}
#endif
