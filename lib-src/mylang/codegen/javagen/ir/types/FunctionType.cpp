#include <mylang/codegen/javagen/ir/types/FunctionType.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/CodeStream.h>
#include <mylang/codegen/javagen/ir/JavaType.h>
#include <mylang/codegen/javagen/ir/Typename.h>
#include <mylang/codegen/model/types/FunctionType.h>
#include <mylang/codegen/javagen/ir/ExportedJavaType.h>
#include <mylang/names/Name.h>
#include <stddef.h>
#include <memory>
#include <optional>
#include <stdexcept>
#include <string>
#include <vector>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          FunctionType::FunctionType()

          {
          }

          FunctionType::~FunctionType()
          {
          }

          ::std::shared_ptr<const FunctionType> FunctionType::create(const Context &ctx,
              ::std::shared_ptr<const model::types::FunctionType> model)
          {
            struct Impl: public FunctionType
            {
              Impl(const Context &ctx, ::std::shared_ptr<const model::types::FunctionType> type)
                  :
                      JavaType(
                          ctx.getTypename(std::nullopt, "fn_" + type->name()->uniqueLocalName()),
                          type)
              {
                returnType = ctx.getType(type->returnType);
                for (auto p : type->parameters) {
                  paramTypes.push_back(ctx.getType(p));
                }
              }

              ~Impl()
              {
              }

              ExportedJavaTypePtr exportType(const Context&) const override final
              {
                // FIXME: implement this properly
                return ExportedJavaType::createIdentity(self());
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                CodeStream out;
                bool generated = generate(ctx, out);
                if (generated) {
                  ctx.writeClass(name.fqn, out.getText());
                }
                return generated;
              }

              bool generate(const Context &ctx, CodeStream &out) const override final
              {
                if (name.packageName.has_value()) {
                  out << "package " << name.packageName.value() << ";" << nl();
                }

                out << "@java.lang.FunctionalInterface" << nl();
                out << "public interface " << name.simpleName << ' ';
                out.brace([&]
                {
                  genSignature(ctx, out);
                  out << ';';
                }) << nl();
                return true;
              }

              ::std::vector<Variable> genSignature(const Context&, CodeStream &out) const
              override final
              {
                ::std::vector<Variable> args;
                out << "public " << returnType->name << " apply";
                out.parens(paramTypes, ", ", [&](size_t i, Context::JavaTypePtr e) {
                  ::std::string arg = "arg"+::std::to_string(i);
                  args.push_back(Variable(arg,e));
                  out << e->name.fqn << ' ' << arg;
                });
                return args;
              }

              ::std::string genCall(const Variable &fn, const ::std::vector<Variable> &args) const
              override final
              {
                ::std::string res;
                res = fn + ".apply(";
                const char *sep = "";
                for (auto arg : args) {
                  res = res + sep + arg;
                  sep = ", ";
                }
                res = res + ')';
                return res;
              }

              ::std::string genNewInstance(const ::std::vector<Variable>&) const override final
              {
                return unexpected("unexpected call to genNewInstance");
              }

              ::std::string genEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + " == " + rhs;
              }
              ::std::string genNEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + " != " + rhs;
              }

              ::std::string genLT(const Ptr&, const Variable&, const Variable&) const
              override final
              {
                // TODO: how to implement
                throw ::std::runtime_error("Not supported");
              }

              ::std::string genLTE(const Ptr&, const Variable&, const Variable&) const
              override final
              {
                // TODO: how to implement
                throw ::std::runtime_error("Not supported");
              }

            private:
              JavaType::Ptr returnType;
              ::std::vector<JavaType::Ptr> paramTypes;
            };

            return ::std::make_shared<Impl>(ctx, model);
          }
        }
      }
    }
  }
}
