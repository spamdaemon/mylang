#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_ARRAYBUILDERTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_ARRAYBUILDERTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_SEQUENCETYPE_H
#include <mylang/codegen/javagen/ir/types/SequenceType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_NATIVETYPE_H
#include <mylang/codegen/javagen/ir/types/NativeType.h>
#endif

#include <mylang/codegen/model/types/ArrayType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class ArrayBuilderType: public virtual JavaType
          {
            /**
             * Create a new simple java type
             */
          public:
            ArrayBuilderType();

          public:
            virtual ~ArrayBuilderType();

            /**
             * Create a bit type
             */
          public:
            static ::std::shared_ptr<const ArrayBuilderType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::ArrayType> model);

            /**
             * Build the result product.
             * @return an expression that builds an array
             */
            virtual ::std::string genBuild(const JavaType::Ptr &ty,
                const JavaType::Variable &builder) const = 0;

            /**
             * Append a value.
             * @param builder a builder
             * @parma value the value to be appended
             */
            virtual void genAppend(const JavaType::Variable &builder,
                const JavaType::Variable &value, CodeStream &out) const = 0;

          };
        }
      }
    }
  }
}
#endif
