#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_OUTPUTTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_OUTPUTTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PORTTYPE_H
#include <mylang/codegen/javagen/ir/types/PortType.h>
#endif

#include <mylang/codegen/model/types/OutputType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class OutputType: public virtual JavaType, public virtual PortType
          {
          public:
            OutputType();

          public:
            ~OutputType();

            /**
             * Generate an equality expression
             * @param lhs
             */
          public:
            virtual ::std::string genIsWritable(const Ptr &ty, const Variable &opt) const = 0;

            /**
             * Generate a clear port statement
             * @param port the port to clear
             */
          public:
            virtual ::std::string genWritePort(const Variable &port,
                const Variable &data) const = 0;

            /**
             * Generate an equality expression
             * @param lhs
             */
          public:
            virtual ::std::string genIsOutputClosed(const JavaType::Ptr &ty,
                const JavaType::Variable &opt) const = 0;

            /**
             * Generate a clear port statement
             * @param port the port to clear
             */
          public:
            virtual ::std::string genCloseOutput(const JavaType::Variable &port) const = 0;
          };
        }
      }
    }
  }
}
#endif
