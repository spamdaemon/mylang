#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TONATIVESTRINGTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TONATIVESTRINGTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

#include <string>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {
          /**
           * A java type that can convert to a string
           */
          class ToNativeStringType: public virtual JavaType
          {
            /**
             *
             */
          public:
            ToNativeStringType();

          public:
            virtual ~ToNativeStringType();

            /**
             * Generate an expression that transforms an instance of this type
             * into the corresponding native string type.
             * @param expr an expression of this type
             * @return an expression for a native string
             */
          public:
            virtual ::std::string toNativeString(const ::std::string &expr) const =0;
          };
        }
      }
    }
  }
}
#endif
