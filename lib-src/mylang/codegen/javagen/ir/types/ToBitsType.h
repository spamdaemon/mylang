#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TOBITSTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TOBITSTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class ToBitsType
          {
          public:
            virtual ~ToBitsType();

          public:
            virtual ::std::string genFromBits(const JavaType::Variable &var) const = 0;
            virtual ::std::string genToBits(const JavaType::Ptr &resTy,
                const JavaType::Variable &var) const = 0;
          };
        }
      }
    }
  }
}
#endif
