#include <mylang/codegen/javagen/ir/types/RealType.h>
#include <mylang/codegen/javagen/ir/types/IntegerType.h>
#include <mylang/codegen/javagen/ir/types/ArrayType.h>
#include <mylang/codegen/model/ir/LiteralReal.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/BigReal.h>
#include <mylang/codegen/CodeStream.h>
#include <mylang/codegen/javagen/ir/JavaType.h>
#include <mylang/codegen/javagen/ir/Typename.h>
#include <mylang/codegen/javagen/ir/VariableName.h>
#include <mylang/codegen/model/ir/Node.h>
#include <mylang/codegen/model/model.h>
#include <mylang/codegen/model/types/RealType.h>
#include <mylang/codegen/javagen/ir/exports/PrimitiveExportedType.h>
#include <mylang/codegen/javagen/ir/exports/PrimitiveNamedType.h>
#include <mylang/names/Name.h>
#include <map>
#include <memory>
#include <optional>
#include <stdexcept>
#include <string>
#include <vector>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          RealType::RealType()
          {
          }

          RealType::~RealType()
          {
          }

          ::std::shared_ptr<const RealType> RealType::create(const Context &ctx,
              ::std::shared_ptr<const model::types::RealType> model)
          {
            struct Impl: public RealType
            {
              Impl(const Context &ctx, model::MType type)
                  : JavaType(ctx.getBuiltinTypename("java.math.BigDecimal"), type),
                      support(ctx.getTypename(::std::nullopt, type->name()->uniqueLocalName()))
              {
              }

              ~Impl()
              {
              }

              ExportedJavaTypePtr exportType(const Context&) const override final
              {
                return ::std::make_shared<exports::PrimitiveExportedType>(self());
              }

              ExportedJavaTypePtr exportTypeAs(const Context &c, const Typename &as,
                  const NamedTypePtr &ty) const override final
              {
                return ::std::make_shared<exports::PrimitiveNamedType>(c, as, ty);
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                writeSupportClass(ctx);
                return false;
              }

              void writeSupportClass(const Context &ctx) const
              {
                CodeStream out;
                ::std::map<::std::string, ::std::string> replacements;
                replacements["support.package"] = support.packageName.value();
                replacements["support.class"] = support.simpleName;
                out.insertTemplateFile("templates/codegen/javagen/ir/RealSupport.java",
                    replacements);

                ctx.writeClass(support.fqn, out.getText());
              }

              bool generate(const Context&, CodeStream&) const override final
              {
                return false;
              }
              ::std::string toNativeString(const ::std::string &expr) const override final
              {
                return "(" + expr + ").toString()";
              }

              ::std::string genFromBits(const JavaType::Variable &var) const override final
              {
                auto arrTy = var.type->self<ArrayType>();
                return support + ".bits2real(" + arrTy->toNative(var) + ")";
              }

              ::std::string genToBits(const JavaType::Ptr &resTy,
                  const JavaType::Variable &var) const
                  override final
              {
                auto arrTy = resTy->self<ArrayType>();
                return arrTy->fromNative(support + ".real2bits(" + var + ")");
              }
              ::std::string genFromBytes(const JavaType::Variable &var) const override final
              {
                auto arrTy = var.type->self<ArrayType>();
                return support + ".bytes2real(" + arrTy->toNative(var) + ")";
              }
              ::std::string genToBytes(const JavaType::Ptr &resTy,
                  const JavaType::Variable &var) const override final
              {
                auto arrTy = resTy->self<ArrayType>();
                return arrTy->fromNative(support + ".real2bytes(" + var + ")");
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
              {
                // default
                if (args.empty()) {
                  return "java.math.BigDecimal.ZERO";
                }
                return args.at(0).name;
              }

              ::std::string genLiteral(const model::MLiteral &literal) const override final
              {
                auto lit = literal->self<model::ir::LiteralReal>();
                if (lit->value == 0) {
                  return "java.math.BigDecimal.ZERO";
                }
                if (lit->value == 1) {
                  return "java.math.BigDecimal.ONE";
                }
                if (lit->value == 10) {
                  return "java.math.BigDecimal.TEN";
                }
                // FIXME: be a bit smarter about this and initialize from a double if possible
                return "new java.math.BigDecimal(" + lit->value.toHexString() + "/* "
                    + lit->value.toDecimalString(9) + " */)";
                //return "new java.math.BigDecimal(\"" + lit->value.toDecimalString(9) + "\")";
              }

              ::std::string genToLiteral(const Ptr&, const Variable &var) const override final
              {
                return var + ".toPlainString()";
              }

              ::std::string genEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".equals(" + rhs + ')';
              }
              ::std::string genNEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return "!" + lhs + ".equals(" + rhs + ')';
              }

              ::std::string genLT(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".compareTo(" + rhs + ")<0";
              }

              ::std::string genLTE(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".compareTo(" + rhs + ")<=0";
              }

              ::std::string genNegate(const Ptr&, const Variable &op) const override final
              {
                return op + ".negate()";
              }

              ::std::string genAdd(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".add(" + rhs + ')';
              }

              ::std::string genSub(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".subtract(" + rhs + ')';
              }

              ::std::string genMul(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".multiply(" + rhs + ')';
              }

              ::std::string genDiv(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".divide(" + rhs + ')';
              }

              ::std::string genMod(const Ptr&, const Variable&, const Variable&) const
              override final
              {
                throw ::std::runtime_error("Mod is not supported for reals");
              }

              ::std::string genRem(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".remainder(" + rhs + ')';
              }

              ::std::string genCheckedCast(const Variable &op) const override final
              {
                if (op.type->self<IntegerType>()) {
                  return "new java.math.BigDecimal(" + op + ")";
                }
                return unexpectedCheckedCast(op);
              }

            private:
              const Typename support;
            };

            return ::std::make_shared<Impl>(ctx, model);
          }

        }
      }
    }
  }
}
