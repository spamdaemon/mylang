#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_NATIVETYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_NATIVETYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

#include <string>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A java type
           */
          class NativeType: public virtual JavaType
          {
            /**
             * Create a new simple java type
             * @param the name of the corresponding native type
             */
          public:
            NativeType(const ::std::string &nativeType);

          public:
            virtual ~NativeType();

            /**
             * Generate an expression that transforms an instanceof this type
             * into the corresponding native type.
             * @param expr an expression of this type
             * @return an expression as a native type
             */
          public:
            virtual ::std::string toNative(const Variable &expr) const;

            /**
             * Generate an expression that transforms an instanceof this type
             * into the corresponding native type.
             * @param expr an expression of this type
             * @return an expression as a native type
             */
          public:
            virtual ::std::string toNative(const ::std::string &expr) const =0;

            /**
             * Create an instanceof of this type from the corresponding native type.
             * @param expr an expression of the corresponding native type
             * @return an expression of this type
             */
          public:
            virtual ::std::string fromNative(const ::std::string &expr) const =0;

            /** The corresponding natrive type */
          public:
            const ::std::string nativeType;
          };
        }
      }
    }
  }
}
#endif
