#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_INTEGERTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_INTEGERTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_NUMERICTYPE_H
#include <mylang/codegen/javagen/ir/types/NumericType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PRIMITIVETYPE_H
#include <mylang/codegen/javagen/ir/types/PrimitiveType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TONATIVESTRINGTYPE_H
#include <mylang/codegen/javagen/ir/types/ToNativeStringType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TOBITSTYPE_H
#include <mylang/codegen/javagen/ir/types/ToBitsType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_TOBYTESTYPE_H
#include <mylang/codegen/javagen/ir/types/ToBytesType.h>
#endif

#include <mylang/codegen/model/types/IntegerType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class IntegerType: public virtual JavaType, public virtual NumericType,
              public virtual PrimitiveType, public virtual ToNativeStringType,
              public virtual ToBitsType, public virtual ToBytesType
          {
            /**
             * Create a new simple java type
             */
          public:
            IntegerType();

          public:
            ~IntegerType();

            /**
             * Create a bit type
             */
          public:
            static ::std::shared_ptr<const IntegerType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::IntegerType> model);

            /**
             * Get the integer as a 8bit int.
             */
          public:
            virtual ::std::string getInt8(const JavaType::Variable &var) const = 0;

            /**
             * Get the integer as a 16bit int.
             */
          public:
            virtual ::std::string getInt16(const JavaType::Variable &var) const = 0;

            /**
             * Get the integer as a 32bit int.
             */
          public:
            virtual ::std::string getInt32(const JavaType::Variable &var) const = 0;

            /**
             * Get the integer as a 64bit int.
             */
          public:
            virtual ::std::string getInt64(const JavaType::Variable &var) const = 0;

            /**
             * Create from int32
             * @param expr an expression yielding an int32
             */
          public:
            virtual std::string createFromInt32(const ::std::string &expr) const = 0;

            /**
             * Create from int64
             * @param expr an expression yielding an int64
             */
          public:
            virtual std::string createFromInt64(const ::std::string &expr) const = 0;
          };
        }
      }
    }
  }
}
#endif
