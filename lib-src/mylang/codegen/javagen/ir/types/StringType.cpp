#include <mylang/codegen/javagen/ir/types/StringType.h>
#include <mylang/codegen/javagen/ir/RuntimeGen.h>
#include <mylang/codegen/javagen/ir/types/CharType.h>
#include <mylang/codegen/javagen/ir/types/IntegerType.h>
#include <mylang/codegen/javagen/ir/types/ArrayType.h>
#include <mylang/codegen/model/ir/LiteralString.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/javagen/ir/exports/PrimitiveExportedType.h>
#include <mylang/codegen/javagen/ir/exports/PrimitiveNamedType.h>
#include <mylang/text/AsciiTable.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          StringType::StringType()

          {
          }

          StringType::~StringType()
          {
          }

          ::std::shared_ptr<const StringType> StringType::create(const Context &ctx,
              ::std::shared_ptr<const model::types::StringType> model)
          {
            struct Impl: public StringType
            {
              Impl(const Context &ctx, model::MType type)
                  : JavaType(ctx.getBuiltinTypename("java.lang.String"), type),
                      NativeType("java.lang.String"),
                      support(ctx.getTypename(::std::nullopt, type->name()->uniqueLocalName()))
              {
              }

              ~Impl()
              {
              }

              ExportedJavaTypePtr exportType(const Context&) const override final
              {
                return ::std::make_shared<exports::PrimitiveExportedType>(self());
              }

              ExportedJavaTypePtr exportTypeAs(const Context &c, const Typename &as,
                  const NamedTypePtr &ty) const override final
              {
                return ::std::make_shared<exports::PrimitiveNamedType>(c, as, ty);
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                writeSupportClass(ctx);
                return false;
              }

              void writeSupportClass(const Context &ctx) const
              {
                CodeStream out;
                ::std::map<::std::string, ::std::string> replacements;
                replacements["support.package"] = support.packageName.value();
                replacements["support.class"] = support.simpleName;
                out.insertTemplateFile("templates/codegen/javagen/ir/StringSupport.java",
                    replacements);

                ctx.writeClass(support.fqn, out.getText());
              }

              bool generate(const Context&, CodeStream&) const override final
              {
                return false;
              }

              ::std::string toNative(const ::std::string &expr) const override final
              {
                return expr;
              }
              ::std::string fromNative(const ::std::string &expr) const override final
              {
                return expr;
              }
              ::std::string toNativeString(const ::std::string &expr) const override final
              {
                return expr;
              }
              ::std::string genFromBits(const JavaType::Variable &var) const override final
              {
                // TODO
                return "";
              }
              ::std::string genToBits(const JavaType::Ptr &resTy,
                  const JavaType::Variable &var) const
                  override final
              {
                // TODO
                return "";
              }

              ::std::string genFromBytes(const JavaType::Variable &var) const override final
              {
                return support + ".bytes2string(" + var + ")";
              }
              ::std::string genToBytes(const JavaType::Ptr&, const JavaType::Variable &var) const
              override final
              {
                return support + ".string2bytes(" + var + ")";
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
              {
                if (args.empty()) {
                  return "\"\"";
                }
                auto charTy = args.at(0).type->self<CharType>();
                if (charTy) {
                  return "java.lang.String.valueOf(" + charTy->toNative(args.at(0)) + ")";
                }
                auto charsTy = args.at(0).type->self<ArrayType>();
                if (charsTy) {
                  return "java.lang.String.valueOf(" + charsTy->toNative(args.at(0)) + ")";
                }
                return "new java.lang.String( " + args.at(0) + ')';
              }

              ::std::string genLiteral(const model::MLiteral &literal) const override final
              {
                auto lit = literal->self<model::ir::LiteralString>();

                // need to escape the characters
                return '"'
                    + mylang::text::AsciiTable::escapeText(mylang::text::AsciiTable::JAVA,
                        lit->value) + '"';
              }

              ::std::string genToLiteral(const Ptr&, const Variable &var) const override final
              {
                return var.name;
              }

              ::std::string genToCharArray(const Ptr &ty, const Variable &opt) const override final
              {
                auto arrTy = ty->self<ArrayType>();
                return arrTy->toNative(opt + ".toCharArray()");
              }

              ::std::string genLength(const ::std::shared_ptr<const IntegerType> &ty,
                  const Variable &arr) const override final
              {
                return ty->createFromInt32(arr + ".length()");
              }

              ::std::string genIndex(const Ptr&, const Variable &arr, const Variable &index) const
              override final
              {
                auto ity = index.type->self<IntegerType>();
                return arr + ".charAt(" + ity->getInt32(index) + ')';
              }

              ::std::string genSubrange(const Ptr&, const Variable &arr, const Variable &start,
                  const Variable &end) const override final
              {
                auto ity = start.type->self<IntegerType>();
                auto jty = end.type->self<IntegerType>();
                return arr + ".substring(" + ity->getInt32(start) + ", " + jty->getInt32(end) + ")";
              }

              ::std::string genConcatenate(const Ptr&, const Variable &lhs,
                  const Variable &rhs) const override final
              {
                if (lhs.type->self<types::CharType>() && rhs.type->self<types::CharType>()) {
                  // special case
                  return "java.lang.String.valueOf(" + lhs + ")+" + rhs;
                }
                return lhs + " + " + rhs;
              }

              ::std::string genEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".equals(" + rhs + ')';
              }
              ::std::string genNEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return "!" + lhs + ".equals(" + rhs + ')';
              }

              ::std::string genLT(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".compareTo(" + rhs + ")<0";
              }

              ::std::string genLTE(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".compareTo(" + rhs + ")<=0";
              }

            private:
              const Typename support;
            };

            return ::std::make_shared<Impl>(ctx, model);
          }

        }
      }
    }
  }
}
