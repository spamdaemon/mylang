#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_NATIVEARRAYTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_NATIVEARRAYTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_ARRAYTYPE_H
#include <mylang/codegen/javagen/ir/types/ArrayType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {

          /**
           * A numeric type for integers and floats
           */
          class NativeArrayType: public ArrayType
          {
            /**
             * Create a new simple java type
             */
          public:
            NativeArrayType();

          public:
            virtual ~NativeArrayType();

            /**
             * Create a bit type
             */
          public:
            static ::std::shared_ptr<const NativeArrayType> create(const Context &ctx,
                ::std::shared_ptr<const model::types::ArrayType> model);

            /**
             * Generate an allocation of an array.
             *
             * The array elements will be defaulted.
             *
             * @param lenExpr the size of the array
             * @return an allocation of an array
             */
          public:
            virtual ::std::string genAllocate(const ::std::string &lenExpr) const = 0;
          };
        }
      }
    }
  }
}
#endif
