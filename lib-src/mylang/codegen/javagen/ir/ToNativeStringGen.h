#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TONATIVESTRINGGEN_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TONATIVESTRINGGEN_H

#ifndef FILE_MYLANG_CODEGEN_MODEL_H
#include <mylang/codegen/model/model.h>
#endif

#include <string>
#include <functional>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        class Context;

        /** A generator for all top-level functions */
        class ToNativeStringGen
        {
          /**
           * A functor that takes an expression of a type and produces
           * an expression that evaluates to a native string.
           */
        public:
          typedef ::std::function<::std::string(const ::std::string&)> Functor;

          /** Constructor */
        public:
          ToNativeStringGen();

          /** Destructor */
        public:
          virtual ~ToNativeStringGen();

          /**
           * Get a function that can convert a variable of a specified type into a native string.
           * @param ty a type
           * @return a function or none if the type cannot be converted
           */
        public:
          virtual Functor getToString(const Context &ctx, const model::MType &ty) const;

        };
      }
    }
  }
}
#endif
