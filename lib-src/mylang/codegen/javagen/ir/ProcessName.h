#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_PROCESSNAME_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_PROCESSNAME_H

#include <memory>
#include <string>
#include <optional>
#include <iostream>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        class Context;

        /**
         * A java type
         */
        class ProcessName
        {
          /** The tag that indicates the origin of the typename */
        public:
          friend class Context;

          /**
           * Create a new simple java type
           */
        private:
          ProcessName(::std::optional<::std::string>, ::std::string simpleName);

        public:
          ~ProcessName();

          /**
           * Compare two typenames for equality based on their fqn
           */
        public:
          friend inline bool operator==(const ProcessName &a, const ProcessName &b)
          {
            return a.fqn == b.fqn;
          }

          /**
           * Ordering of typenames based on their fqn
           */
        public:
          friend inline bool operator<(const ProcessName &a, const ProcessName &b)
          {
            return a.fqn < b.fqn;
          }

          /**
           * Ordering of typenames based on their fqn
           */
        public:
          friend inline ::std::ostream& operator<<(::std::ostream &out, const ProcessName &b)
          {
            return out << b.fqn;
          }

          /**
           * Ordering of typenames based on their fqn
           */
        public:
          friend inline ::std::string operator+(const ::std::string &left, const ProcessName &right)
          {
            return left + right.fqn;
          }

          /**
           * Ordering of typenames based on their fqn
           */
        public:
          friend inline ::std::string operator+(const ProcessName &left, const ::std::string &right)
          {
            return left.fqn + right;
          }

          /**
           * The fully qualified name of the type. If this is a builtin type, then
           * this is the same as the simple name.
           */
        public:
          const ::std::string fqn;

          /**
           * The fully qualified package name.
           */
        public:
          const ::std::optional<::std::string> packageName;

          /**
           * The simple name.
           */
        public:
          const ::std::string simpleName;
        };
      }
    }
  }
}
#endif
