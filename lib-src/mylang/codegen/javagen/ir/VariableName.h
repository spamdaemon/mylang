#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_VARIABLENAME_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_VARIABLENAME_H

#ifndef FILE_MYLANG_CODEGEN_MODEL_H
#include <mylang/codegen/model/model.h>
#endif

#include <memory>
#include <string>
#include <iostream>
#include <optional>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        /**
         * A java type
         */
        class VariableName
        {
          /**
           * Create a new global variable.
           * @param packagename the name of the package of the class
           * @param classname the name of the class that contains the variable
           * @param simpleName the name of the static member variable in the class.
           */
        public:
          VariableName(::std::string packagename, ::std::string classname,
              ::std::string simpleName);

          /**
           * Create a new local variable.
           * @param simpleName the name of a local variable or parameter
           */
        public:
          VariableName(::std::string simpleName);

        public:
          ~VariableName();

          /**
           * Create a new variable name that is unique.
           * @return a variable name
           */
        public:
          static VariableName unique();

          /**
           * Implicit conversion to a string
           */
        public:
          inline operator const ::std::string&() const
          {
            return fqn;
          }

          friend ::std::string operator+(const ::std::string &s, const VariableName &v)
          {
            return s + v.fqn;
          }
          friend ::std::string operator+(const VariableName &v, const ::std::string &s)
          {
            return v.fqn + s;
          }
          friend ::std::string operator+(char ch, const VariableName &v)
          {
            return ch + v.fqn;
          }
          friend ::std::string operator+(const VariableName &v, char ch)
          {
            return v.fqn + ch;
          }

          /**
           * Compare two typenames for equality based on their fqn
           */
        public:
          friend inline bool operator==(const VariableName &a, const VariableName &b)
          {
            return a.fqn == b.fqn;
          }

          /**
           * Ordering of typenames based on their fqn
           */
        public:
          friend inline bool operator<(const VariableName &a, const VariableName &b)
          {
            return a.fqn < b.fqn;
          }

          /**
           * Ordering of typenames based on their fqn
           */
        public:
          friend inline ::std::ostream& operator<<(::std::ostream &out, const VariableName &b)
          {
            return out << b.fqn;
          }

          /**
           * The fully qualified name of the variable.
           */
        public:
          const ::std::string fqn;

          /**
           * The fully qualified package name. May be blank.
           */
        public:
          const ::std::optional<::std::string> packageName;

          /**
           * The class name for the variable.May be blank.
           */
        public:
          const ::std::optional<::std::string> className;

          /**
           * The member name of the variable within the class
           */
        public:
          const ::std::string simpleName;
        };
      }
    }
  }
}
#endif
