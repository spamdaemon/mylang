#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_PROCESSGEN_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_PROCESSGEN_H

#include <memory>
#include <string>
#include <vector>

#include <mylang/codegen/javagen/ir/JavaType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace types {
          class ProcessType;
        } /* namespace types */
      } /* namespace javagen */
    } /* namespace ir */
  } /* namespace codegen */
} /* namespace mylang */

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_CONTEXT_H
#include <mylang/codegen/javagen/ir/Context.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PROCESSTYPE_H
#include <mylang/codegen/javagen/ir/types/ProcessType.h>
#endif

#include <mylang/codegen/model/types/ProcessType.h>
#include <mylang/codegen/model/ir/ProcessDecl.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        /** A generator for all top-level functions */
        class ProcessGen
        {
          /** Constructor */
        public:
          ProcessGen();

          /** Destructor */
        public:
          virtual ~ProcessGen();

          /**
           * Generate the specified block.
           * @param block
           * @param out the output stream
           */
        public:
          virtual void generate(const Context &context,
              const model::ir::ProcessDecl::ProcessDeclPtr &proc) const;

          /**
           * Create the input type for the specified element.
           * @param context a context
           * @param model the model
           * @return an input type
           */
        public:
          virtual ::std::shared_ptr<const types::ProcessType> getProcessType(const Context &context,
              const ::std::shared_ptr<const model::types::ProcessType> &model) const;

          /**
           * Generate a new expression.
           */
        public:
          virtual ::std::string genNewInstance(const Context &context,
              const ::std::shared_ptr<const types::ProcessType> &ty, const ProcessName &proc,
              const ::std::vector<Context::Variable> &args) const;

          /**
           * Block read events on a port.
           * @param port the port to clear
           */
        public:
          virtual ::std::string genBlockReadEvents(const Context &context,
              const JavaType::Variable &port, const JavaType::Variable &onoff) const;

          /**
           * Block write events on a port.
           * @param port the port to clear
           */
        public:
          virtual ::std::string genBlockWriteEvents(const Context &context,
              const JavaType::Variable &port, const JavaType::Variable &onoff) const;
        };
      }
    }
  }
}
#endif
