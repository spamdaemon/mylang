#include <mylang/codegen/javagen/ir/GlobalGen.h>
#include <mylang/codegen/javagen/ir/BlockGen.h>
#include <mylang/codegen/javagen/ir/ToNativeStringGen.h>
#include <mylang/codegen/javagen/ir/types/FunctionType.h>
#include <mylang/codegen/javagen/ir/types/ArrayType.h>
#include <mylang/codegen/javagen/ir/types/VoidType.h>
#include <mylang/codegen/model/types/ArrayType.h>
#include <mylang/codegen/model/types/StringType.h>
#include <mylang/codegen/model/types/FunctionType.h>
#include <mylang/codegen/model/ir/Parameter.h>
#include <mylang/codegen/model/ir/LambdaExpression.h>
#include <mylang/names/Name.h>

#include <iostream>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        namespace {

          static bool genMain(const Context &ctx,
              ::std::shared_ptr<const model::types::FunctionType> signature, CodeStream &out)
          {
            auto ty = ctx.getType(signature->returnType);
            ::std::string resultVar;
            ::std::string printResult;

            if (ty->self<types::VoidType>() == nullptr) {
              auto toString = ctx.toNativeStringGen->getToString(ctx, signature->returnType);
              if (toString) {
                resultVar = "final " + ty->name + " res = ";
                printResult = "java.lang.System.out.println(" + toString("res") + ");";
              }
            }

            if (signature->parameters.empty()) {
              out.brace("public static void main(String[] argv)", [&]
              {
                out << resultVar << "apply();" << nl();
                out << printResult;
              });
              return true;
            } else if (signature->parameters.size() == 1) {
              auto arrTy = signature->parameters.at(0)->self<model::types::ArrayType>();
              if (arrTy && arrTy->element->self<model::types::StringType>()) {
                auto arrayTy = ctx.getType(arrTy)->self<types::ArrayType>();
                out.brace("public static void main(String[] argv)", [&]
                {
                  out << resultVar << "apply(" + arrayTy->fromNative("argv") + ");" << nl();
                  out << printResult;
                });
                return true;
              }
            }
            return false;
          }

          static void genExport(const Context &context, const ::mylang::names::FQN &fqn,
              const ::std::shared_ptr<const model::types::FunctionType> signature,
              // the internal variable that provides the implementation for the exported function
              const JavaType::Variable &internalVar)
          {
            CodeStream out;
            auto packageFqn = fqn.parent();
            auto className = fqn.localName();

            // the function type is the internal type
            auto fnTy = context.getType(signature)->self<types::FunctionType>();

            // get the return type and parameters as exported type s
            auto retTy = context.getExportedType(signature->returnType);
            ::std::vector<ExportedJavaType::Variable> eargs;

            for (size_t i = 0; i < signature->parameters.size(); ++i) {
              auto ty = context.getExportedType(signature->parameters.at(i));
              auto name = "arg" + ::std::to_string(i);
              eargs.emplace_back(name, ty);
            }

            bool haveMain = false;

            if (packageFqn) {
              out << "package " << packageFqn->fullName() << ";" << nl();
            }
            out << "public final class " << className;
            out.brace([&]
            {
              out << "public static " << retTy->name << " apply";
              out.parens(eargs, ",", [&](size_t, const ExportedJavaType::Variable &v) {
                out << v.type->name << ' ' << v.name;
              });
              out.brace([&]
              {
                ::std::vector<JavaType::Variable> args;
                for (size_t i = 0; i < eargs.size(); ++i) {
                  const ExportedJavaType::Variable &earg = eargs.at(i);
                  out << "// TODO: need to validate " << earg.name << nl();
                  auto ty = earg.type->backingType();
                  if (earg.type->name == ty->name) {
                    args.emplace_back(earg.name, ty);
                  } else {
                    auto name = "tmp" + ::std::to_string(i);
                    JavaType::Variable v(name, ty);
                    args.push_back(v);
                    // convert the argument from external to internal type
                    out << v.type->name << ' ' << v.name << " = ";
                    out << earg.type->toBackingType(earg.name) << ';' << nl();
                  }
                }

                if (retTy->backingType()->self<types::VoidType>()) {
                  out << fnTy->genCall(internalVar, args) << ';' << nl();
                } else if (retTy->backingType()->name == retTy->name) {
                  out << "return " << fnTy->genCall(internalVar, args) << ';' << nl();
                } else {
                  const JavaType::Variable ret(VariableName("result"), retTy->backingType());
                  out << ret.type->name << ' ' << ret.name << " = " << fnTy->genCall(internalVar, args) <<';' << nl();
                  out << "return " << retTy->fromBackingType(ret.name) << ';' << nl();
                }
              }) << nl();
              haveMain = genMain(context, signature, out);
            });
            context.writeClass(fqn.fullName(), out.getText(), haveMain);
          }
        }

        GlobalGen::GlobalGen()
        {
        }
        GlobalGen::~GlobalGen()
        {
        }

        void GlobalGen::generate(const Context &context,
            const model::ir::GlobalValue::GlobalValuePtr global) const
        {

          auto fnTy = context.getType(global->type);
          auto var = context.getVariable(global->variable);
          auto &varname = var.name;
          {
            ::std::string className = varname.className.value();

            // setup a code stream
            CodeStream out;
            if (varname.packageName) {
              className = varname.packageName.value() + "." + className;
              out << "package " << varname.packageName.value() << ';' << nl();
            }
            out << "public final class " << varname.className.value();
            out.brace([&]
            {
              out << "public static final " << fnTy->name << ' ' << varname.simpleName << " = ";
              context.blockGen->generate(context, global->value, out);
              out << ";" << nl();
            });
            context.writeClass(className, out.getText());
          }

          // make the name also available under the specified FQN
          if (global->exportedName) {
            auto signature = global->type->self<model::types::FunctionType>();
            if (!signature) {
              ::std::cerr << "WARNING: global non-function variables are not supported\n"
                  << ::std::endl;
              return;
            }
            genExport(context, global->exportedName.value(), signature, var);
          }
        }
      }
    }
  }
}
