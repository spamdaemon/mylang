#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/model/ir/Expression.h>
#include <mylang/codegen/model/types/Type.h>
#include <mylang/codegen/model/types/TypeVisitor.h>
#include <mylang/codegen/javagen/ir/BlockGen.h>
#include <mylang/codegen/javagen/ir/GlobalGen.h>
#include <mylang/codegen/javagen/ir/ProgramGen.h>
#include <mylang/codegen/javagen/ir/LoggerGen.h>
#include <mylang/codegen/javagen/ir/RuntimeGen.h>
#include <mylang/codegen/javagen/ir/ProcessGen.h>
#include <mylang/codegen/javagen/ir/PortGen.h>
#include <mylang/codegen/javagen/ir/PortAdapterGen.h>
#include <mylang/codegen/javagen/ir/ProcessAdapterGen.h>
#include <mylang/codegen/javagen/ir/ToNativeStringGen.h>
#include <mylang/codegen/javagen/ir/types/ArrayBuilderType.h>
#include <mylang/codegen/javagen/ir/types/ArrayType.h>
#include <mylang/codegen/javagen/ir/types/BitType.h>
#include <mylang/codegen/javagen/ir/types/BooleanType.h>
#include <mylang/codegen/javagen/ir/types/ByteType.h>
#include <mylang/codegen/javagen/ir/types/CharType.h>
#include <mylang/codegen/javagen/ir/types/FunctionType.h>
#include <mylang/codegen/javagen/ir/types/GenericType.h>
#include <mylang/codegen/javagen/ir/types/IntegerType.h>
#include <mylang/codegen/javagen/ir/types/MutableType.h>
#include <mylang/codegen/javagen/ir/types/NamedType.h>
#include <mylang/codegen/javagen/ir/types/OptType.h>
#include <mylang/codegen/javagen/ir/types/RealType.h>
#include <mylang/codegen/javagen/ir/types/StringType.h>
#include <mylang/codegen/javagen/ir/types/StructType.h>
#include <mylang/codegen/javagen/ir/types/TupleType.h>
#include <mylang/codegen/javagen/ir/types/UnionType.h>
#include <mylang/codegen/javagen/ir/types/NativeArrayType.h>
#include <mylang/codegen/javagen/ir/types/NativeIntegerType.h>
#include <mylang/codegen/javagen/ir/types/VoidType.h>

#include <limits>
#include <memory>
#include <stdexcept>
#include <string>
#include <set>
#include <utility>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        Context::Context(int xoptLevel, bool xuseNativeTypes)
            : useNativeTypes(xuseNativeTypes), optLevel(xoptLevel),
                blockGen(::std::make_shared<BlockGen>()),
                loggerGen(::std::make_shared<LoggerGen>()), portGen(::std::make_shared<PortGen>()),
                processGen(::std::make_shared<ProcessGen>()),
                globalGen(::std::make_shared<GlobalGen>()),
                programGen(::std::make_shared<ProgramGen>()),
                portAdapterGen(::std::make_shared<PortAdapterGen>()),
                processAdapterGen(::std::make_shared<ProcessAdapterGen>()),
                toNativeStringGen(::std::make_shared<ToNativeStringGen>()),
                runtimeGen(::std::make_shared<RuntimeGen>())
        {
          variables = ::std::make_shared<::std::map<NamePtr, VariableName>>();
          exportTypes = ::std::make_shared<::std::map<model::MType, ExportedJavaTypePtr>>();
          types = ::std::make_shared<::std::map<model::MType, JavaTypePtr>>();
          uniqueExportedTypes = ::std::make_shared<::std::map<Typename, ExportedJavaTypePtr>>();
          uniqueTypes = ::std::make_shared<::std::map<Typename, JavaTypePtr>>();
        }

        Context::~Context()
        {
        }

        void Context::writeClass(const ::std::string &classname, const ::std::string &contents,
            bool) const
        {
          ::std::cout << classname << ::std::endl << contents << ::std::endl;
        }

        ExportedJavaTypePtr Context::registerType(ExportedJavaTypePtr exported) const
        {
          if (auto i = uniqueExportedTypes->find(exported->name); i != uniqueExportedTypes->end()) {
            return i->second;
          }
          uniqueExportedTypes->emplace(exported->name, exported);
          return exported;
        }

        Context::ExportedJavaTypePtr Context::getExportedType(const model::MType &modelType) const
        {
          // quickly check if we already know about type
          if (auto i = exportTypes->find(modelType); i != exportTypes->end()) {
            return i->second;
          }

          auto ty = getType(modelType);
          auto exported = ty->exportType(*this);
          if (exported == nullptr) {
            throw ::std::invalid_argument(
                "Type " + ::std::string(typeid(*modelType).name()) + " cannot be exported");
          }

          if (auto i = uniqueExportedTypes->find(exported->name); i != uniqueExportedTypes->end()) {
            exported = i->second;
          }
          uniqueExportedTypes->emplace(exported->name, exported);
          exportTypes->emplace(modelType, exported);

          return exported;
        }

        Context::JavaTypePtr Context::getType(const model::MType &modelType) const
        {
          // quickly check if we already know about type
          if (auto i = types->find(modelType); i != types->end()) {
            return i->second;
          }

          // we may already know about the type, but it's using a different typename
          const model::MType t = uniqueModelTypes.add(modelType);

          if (auto i = types->find(modelType); i != types->end()) {
            return i->second;
          }

          struct V: public model::types::TypeVisitor
          {
            V(const Context &ctx)
                : context(ctx)
            {
            }
            ~V()
            {
            }

            void visit(const ::std::shared_ptr<const model::types::BitType> &type) override final
            {
              result = types::BitType::create(context, type);
            }

            void visit(const ::std::shared_ptr<const model::types::BooleanType> &type)
            override final
            {
              result = types::BooleanType::create(context, type);
            }

            void visit(const ::std::shared_ptr<const model::types::ByteType> &type) override final
            {
              result = types::ByteType::create(context, type);
            }

            void visit(const ::std::shared_ptr<const model::types::CharType> &type) override final
            {
              result = types::CharType::create(context, type);
            }

            void visit(const ::std::shared_ptr<const model::types::RealType> &type) override final
            {
              result = types::RealType::create(context, type);
            }

            void visit(const ::std::shared_ptr<const model::types::IntegerType> &type)
            override final
            {
              if (context.useNativeTypes) {
                result = types::NativeIntegerType::tryCreate(context, type);
                if (result) {
                  return;
                }
              }
              result = types::IntegerType::create(context, type);
            }

            void visit(const ::std::shared_ptr<const model::types::StringType> &type) override final
            {
              result = types::StringType::create(context, type);
            }

            void visit(const ::std::shared_ptr<const model::types::VoidType> &type) override final
            {
              result = types::VoidType::create(context, type);
            }

            void visit(const ::std::shared_ptr<const model::types::GenericType> &type)
            override final
            {
              result = types::GenericType::create(context, type);
            }

            void visit(const ::std::shared_ptr<const model::types::BuilderType> &type)
            override final
            {
              if (auto arrTy = type->product->self<model::types::ArrayType>()) {
                result = types::ArrayBuilderType::create(context, arrTy);
              } else {
                throw ::std::runtime_error(
                    "Unsupported builder product " + type->product->toString());
              }
            }

            void visit(const ::std::shared_ptr<const model::types::ArrayType> &type) override final
            {
              result = types::NativeArrayType::create(context, type);
              //            result = types::ArrayType::create(context, type);
            }

            void visit(const ::std::shared_ptr<const model::types::UnionType> &type) override final
            {
              result = types::UnionType::create(context, type);
            }

            void visit(const ::std::shared_ptr<const model::types::StructType> &type) override final
            {
              result = types::StructType::create(context, type);
            }

            void visit(const ::std::shared_ptr<const model::types::TupleType> &type) override final
            {
              result = types::TupleType::create(context, type);
            }

            void visit(const ::std::shared_ptr<const model::types::FunctionType> &type)
            override final
            {
              result = types::FunctionType::create(context, type);
            }

            void visit(const ::std::shared_ptr<const model::types::MutableType> &type)
            override final
            {
              result = types::MutableType::create(context, type);
            }

            void visit(const ::std::shared_ptr<const model::types::OptType> &type) override final
            {
              result = types::OptType::create(context, type);
            }

            void visit(const ::std::shared_ptr<const model::types::NamedType> &type) override final
            {
              result = types::NamedType::create(context, type);
            }

            void visit(const ::std::shared_ptr<const model::types::InputType> &type) override final
            {
              result = context.portGen->getInputType(context, type);
            }

            void visit(const ::std::shared_ptr<const model::types::OutputType> &type) override final
            {
              result = context.portGen->getOutputType(context, type);
            }

            void visit(const ::std::shared_ptr<const model::types::ProcessType> &type)
            override final
            {
              result = context.processGen->getProcessType(context, type);
            }

            const Context &context;
            JavaTypePtr result;
          };

          V v(*this);
          t->accept(v);
          if (v.result == nullptr) {
            throw ::std::runtime_error("Failed to map type " + t->toString());
          }
          auto rt = registerType(v.result);
          types->emplace(t, rt);
          return rt;
        }

        Context::JavaTypePtr Context::registerType(JavaTypePtr type) const
        {
          auto j = uniqueTypes->find(type->name);
          if (j == uniqueTypes->end()) {
            uniqueTypes->insert(::std::make_pair(type->name, type));
            return type;
          }
          // we need to prevent recursion when using named types
          // a reference type is always overriden with the actual type
          auto namedTy = type->self<types::NamedType>();
          if (namedTy && !namedTy->isReference()) {
            j->second = type;
          }
          return j->second;
        }

        Context::JavaTypePtr Context::getType(
            const mylang::codegen::model::ir::Expression &expr) const
        {
          return getType(expr.type);
        }

        Typename Context::getBuiltinTypename(const ::std::string &fqn)
        {
          auto i = fqn.rfind('.');
          if (i == fqn.npos) {
            return Typename(fqn);
          } else {
            auto p = fqn.substr(0, i);
            auto c = fqn.substr(i + 1);
            return Typename(p, c);
          }
        }

        JavaType::Ptr Context::getBuiltinType(const ::std::string &fqn,
            ::mylang::codegen::model::MType model) const
        {
          auto name = getBuiltinTypename(fqn);
          auto ty = findType(name);
          if (ty) {
            return ty->self<JavaType>();
          }
          return registerType(Context::getBuiltinType(name, model));
        }
        JavaType::Ptr Context::getBuiltinType(Typename fqn, ::mylang::codegen::model::MType model)
        {
          struct Impl: public JavaType
          {
            Impl(Typename n, ::mylang::codegen::model::MType model)
                : JavaType(n, model)
            {
            }
            ~Impl()
            {
            }

            ExportedJavaTypePtr exportType(const Context&) const override final
            {
              return ExportedJavaType::createIdentity(self<JavaType>());
            }

            bool isObject() const override final
            {
              return name.packageName.has_value();
            }

            bool writeClass(const Context&) const override final
            {
              return false;
            }
            bool generate(const Context&, CodeStream&) const override final
            {
              return false;
            }

            ::std::string genNewInstance(const ::std::vector<Variable>&) const override final
            {
              return unexpected("constructor");
            }
          };

          return ::std::make_shared<Impl>(::std::move(fqn), ::std::move(model));
        }

        JavaType::Ptr Context::findType(const model::MType &t) const
        {
          auto i = types->find(t);
          if (i == types->end()) {
            return nullptr;
          }
          return i->second;
        }

        JavaTypePtr Context::findType(const Typename &t) const
        {
          auto i = uniqueTypes->find(t);
          if (i == uniqueTypes->end()) {
            return nullptr;
          }
          return i->second;
        }

        Typename Context::getArrayTypename(const Typename &element)
        {
          return Typename(element + "[]");
        }

        Typename Context::genTypename(const model::MType &type) const
        {
          auto ty = findType(type);
          if (ty) {
            return ty->name;
          }
          return getTypename(::std::nullopt, type->name()->uniqueLocalName());
        }

        Typename Context::getTypename(const ::std::optional<::std::string> &packagename,
            const ::std::string &classname) const
        {
          ::std::string p = "globals";
          if (packagename.has_value()) {
            p += '.';
            p += packagename.value();
          }
          auto i = classname.rfind('.');
          if (i != classname.npos) {
            p += '.';
            p += classname.substr(0, i);
            return Typename(p, classname.substr(i + 1));
          }

          return Typename(p, classname);
        }

        VariableName Context::getVariableName(const model::MVariable &expr) const
        {
          auto name = expr->name;
          if (name->isFQN() || expr->scope == model::ir::Declaration::GLOBAL_SCOPE) {
            return VariableName("globals", name->uniqueLocalName(), "value");
          }
          return VariableName(name->uniqueLocalName());
        }

        Context::Variable Context::getVariable(const model::MVariable &expr) const
        {
          return Variable(getVariableName(expr), getType(expr->type));
        }

        ProcessName Context::getProcessName(const NamePtr &name) const
        {
          if (name->isFQN()) {
            return ProcessName(::std::string("globals"), name->uniqueLocalName());
          }
          return ProcessName(::std::nullopt, name->uniqueLocalName());
        }

        Context::Label Context::getLabelName(const NamePtr &name) const
        {
          return name->uniqueLocalName();
        }

        void Context::generateTypes() const
        {
          ::std::set<Typename> all_types;
          while (true) {
            bool foundNewType = false;
            // we might be inserting new types as we're generating them, so it's a bit recursive
            ::std::map<Typename, JavaTypePtr> java_types = *uniqueTypes;
            for (auto t : java_types) {
              if (all_types.count(t.first) == 0) {
                t.second->writeClass(*this);
                all_types.insert(t.first);
                foundNewType = true;
              }
            }

            ::std::map<Typename, ExportedJavaTypePtr> exported_types = *uniqueExportedTypes;
            for (auto t : exported_types) {
              if (all_types.count(t.first) == 0) {
                t.second->writeClass(*this);
                all_types.insert(t.first);
                foundNewType = true;
              }
            }
            if (!foundNewType) {
              break;
            }
          }

        }

        ::std::shared_ptr<const types::VoidType> Context::getVoidType() const
        {
          auto model = model::types::VoidType::create();
          auto ty = types::VoidType::create(*this, model);
          return registerType(ty)->self<types::VoidType>();
        }

        ::std::shared_ptr<const types::BooleanType> Context::getBooleanType() const
        {
          auto model = model::types::BooleanType::create();
          auto ty = types::BooleanType::create(*this, model);
          return registerType(ty)->self<types::BooleanType>();
        }

        ::std::shared_ptr<const types::OptType> Context::getOptType(model::MType element) const
        {
          auto model = model::types::OptType::get(element);
          return getType(model)->self<types::OptType>();
        }

      }
    }
  }
}
