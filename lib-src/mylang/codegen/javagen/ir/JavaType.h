#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPE_H
#include <mylang/codegen/javagen/ir/Type.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPEVARIABLE_H
#include <mylang/codegen/javagen/ir/JavaTypeVariable.h>
#endif

#ifndef FILE_MYLANG_CODEGEN_MODEL_H
#include <mylang/codegen/model/model.h>
#endif

#ifndef FILE_MYLANG_CODEGEN_JAVAGEN_IR_H
#include <mylang/codegen/javagen/ir/ir.h>
#endif

#include <string>
#include <vector>
#include <cassert>

namespace mylang::codegen::javagen::ir {
  /**
   * A java type
   */
  class JavaType: public Type
  {

    /** A pointer to a Java type */
  public:
    typedef ::std::shared_ptr<const JavaType> Ptr;

    /** A variable */
  public:
    using Variable = JavaTypeVariable;

    /** A model */
  public:
    using ModelType = ::mylang::codegen::model::MType;

    /**
     * Create a new simple java type
     * @param id the type id
     * @param model the optional model
     */
  public:
    JavaType(Typename id, ModelType optModel);

  public:
    virtual ~JavaType();

    /**
     * Get the exported type for this type.
     * @param c the context
     * @return an exported type or nullptr if this type is not exportable
     */
  public:
    virtual ExportedJavaTypePtr exportType(const Context &c) const;

    /**
     * Export this type under the specified name.
     *
     * This type must be the basetype of the backing type.
     *
     * @param c the context
     * @param exportedName the name to use when exporting
     * @param backingType a NamedType that provides the backing for the generated type.
     * @return an exported type or nullptr if this type is not exportable
     */
  public:
    virtual ExportedJavaTypePtr exportTypeAs(const Context &c, const Typename &exportedName,
        const NamedTypePtr &backingType) const;

    /**
     * Get this type.
     * @return this type instance
     */
  public:
    template<class T = JavaType>
    inline ::std::shared_ptr<const T> self() const
    {
      return ::std::dynamic_pointer_cast<const T>(shared_from_this());
    }

    /**
     * Convert a pointer to a java type
     * @param ptr a pointer
     * @return a java  type
     */
  public:
    template<class T>
    static inline Ptr from(const ::std::shared_ptr<T> &ptr)
    {
      Ptr res;
      if (ptr) {
        res = ::std::dynamic_pointer_cast<const JavaType>(ptr);
        if (!res) {
          throw ::std::runtime_error("Not a java type " + ::std::string(typeid(*ptr).name()));
        }
      }
      return res;
    }

    /**
     * Get the type for the boxed version of this type.
     *
     * Any JavaType that is already an object is a a boxed type.
     * If this type is a primitive type, then this returns the
     * corresponding boxed java type.
     *
     * The boxed type must be an object!
     *
     * @return the box type that corresponds to this type, or nullptr if there is none.
     */
  public:
    virtual Ptr getBoxedType() const;

    /**
     * Box this type into its object type.
     *
     * If this type is an object, then this is the identity function.
     *
     * @param expr an expression of this type
     * @return an expression that represents the boxed type
     */
  public:
    virtual ::std::string box(const ::std::string &expr) const;

    /**
     * Unbox an value into this type.
     *
     * If this type is an object, then this is the identity function.
     *
     * @param expr an expression of the boxed type
     * @return an expression that represents this type
     */
  public:
    virtual ::std::string unbox(const ::std::string &expr) const;

    /**
     * Determine if two types are the same. The default implementation compares
     * only the names and type's implementation type
     * @param other a type
     * @return true if this is the same type as other
     */
  public:
    virtual bool isSameType(const ::std::shared_ptr<const JavaType> &other) const;

    /**
     * Generate a new expression.
     */
  public:
    virtual ::std::string genNewInstance(const ::std::vector<Variable> &args) const = 0;

    /**
     * Generate a cast expression to this type. The type of the object and this type
     * maybe assumed to be different with regards to isSameType.
     * @param obj the object to be casted
     */
  public:
    virtual ::std::string genCheckedCast(const Variable &obj) const;

    /**
     * Generate a cast expression to this type. The type of the object and this type
     * maybe assumed to be different with regards to isSameType.
     * @param obj the object to be casted
     */
  public:
    virtual ::std::string genUncheckedCast(const Variable &obj) const;

    /**
     * Generate the zero value for this type. A zero value is meant as a means to
     * initialize a variable with a value to keep Java happy, but never actually use the value.
     * @return "null" if the type is an object, 0 otherwise
     */
  public:
    virtual ::std::string genZero() const;

    /**
     * Subclass invokes this to indicate that a method is unsupported.
     * This method always throws an exception
     */
  protected:
    ::std::string unexpected(const ::std::string &message) const;

    /**
     * A call to genCast was made that is unexpected
     * This method always throws an exception
     */
  protected:
    ::std::string unexpectedUncheckedCast(const Variable &obj) const;

    /**
     * A call to genCast was made that is unexpected
     * This method always throws an exception
     */
  protected:
    ::std::string unexpectedCheckedCast(const Variable &obj) const;

    /** The optional model for this type */
  public:
    const ModelType optModel;
  };
}
#endif
