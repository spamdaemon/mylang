#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_GLOBALGEN_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_GLOVALGEN_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_CONTEXT_H
#include <mylang/codegen/javagen/ir/Context.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_CODESTREAM_H
#include <mylang/codegen/CodeStream.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_GLOBALVALUE_H
#include <mylang/codegen/model/ir/GlobalValue.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        /** A generator for all top-level functions */
        class GlobalGen
        {

          /** Constructor */
        public:
          GlobalGen();

          /** Destructor */
        public:
          virtual ~GlobalGen();

          /**
           * Generate the specified block.
           * @param block
           * @param out the output stream
           */
        public:
          virtual void generate(const Context &context,
              const model::ir::GlobalValue::GlobalValuePtr global) const;
        };
      }
    }
  }
}
#endif
