#include <mylang/codegen/CodeStream.h>
#include <mylang/codegen/model/types/BooleanType.h>
#include <mylang/codegen/model/types/OptType.h>
#include <mylang/codegen/model/types/InputType.h>
#include <mylang/codegen/model/types/OutputType.h>
#include <mylang/codegen/javagen/ir/PortGen.h>
#include <mylang/codegen/javagen/ir/types/ArrayType.h>
#include <mylang/codegen/javagen/ir/types/BooleanType.h>
#include <mylang/codegen/javagen/ir/types/InputType.h>
#include <mylang/codegen/javagen/ir/types/OptType.h>
#include <mylang/codegen/javagen/ir/types/OutputType.h>
#include <mylang/codegen/javagen/ir/Typename.h>
#include <mylang/codegen/javagen/ir/VariableName.h>
#include <mylang/names/Name.h>
#include <functional>
#include <optional>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace {
          Typename getTimeoutClass(const Context &context)
          {
            auto name = context.getTypename(::std::nullopt, "Timeout");
            auto ty = context.findType(name);
            if (ty) {
              return name;
            }

            struct Impl: public JavaType
            {
              Impl(Typename xname)
                  : JavaType(xname, nullptr)
              {
              }
              ~Impl()
              {
              }

              ::std::string genNewInstance(const ::std::vector<Variable>&) const override final
              {
                return unexpected("newInstance");
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                CodeStream out;
                generate(ctx, out);
                ctx.writeClass(name.fqn, out.getText());
                return true;
              }

              bool generate(const Context&, CodeStream &out) const override final
              {
                ::std::map<::std::string, ::std::string> replacements;
                replacements["timeout.package"] = name.packageName.value();
                replacements["timeout.class"] = name.simpleName;
                out.insertTemplateFile("templates/codegen/javagen/ir/Timeout.java", replacements);
                return true;
              }

            };
            return context.registerType(::std::make_shared<Impl>(name))->name;
          }

          Typename getAbstractDescriptorClass(const Context &context)
          {
            auto name = context.getTypename(::std::nullopt, "Descriptor");
            auto ty = context.findType(name);
            if (ty) {
              return name;
            }

            struct Impl: public JavaType
            {
              Impl(Typename xname)
                  : JavaType(xname, nullptr)
              {
              }
              ~Impl()
              {
              }

              ::std::string genNewInstance(const ::std::vector<Variable>&) const override final
              {
                return unexpected("newInstance");
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                CodeStream out;
                generate(ctx, out);
                ctx.writeClass(name.fqn, out.getText());
                return true;
              }

              bool generate(const Context &ctx, CodeStream &out) const override final
              {
                ::std::map<::std::string, ::std::string> replacements;
                replacements["descriptor.package"] = name.packageName.value();
                replacements["descriptor.class"] = name.simpleName;
                replacements["timeout.fullname"] = getTimeoutClass(ctx).fqn;
                out.insertTemplateFile("templates/codegen/javagen/ir/Descriptor.java",
                    replacements);
                return true;
              }
            };
            return context.registerType(::std::make_shared<Impl>(name))->name;
          }

          JavaType::Ptr getInputDescriptorClass(const Context &context,
              ::std::shared_ptr<const model::types::InputType> model)
          {
            auto ty = context.findType(model);
            if (ty) {
              return ty;
            }
            auto name = context.genTypename(model);

            struct Impl: public virtual JavaType, public types::InputType
            {
              Impl(Typename xname, ::std::shared_ptr<const model::types::InputType> model)
                  : JavaType(xname, model), element(model->element)
              {
              }
              ~Impl()
              {
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &v) const override final
              {
                return "new " + name + "(" + v.at(0) + ")";
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                CodeStream out;
                generate(ctx, out);
                ctx.writeClass(name.fqn, out.getText());
                return true;
              }

              bool generate(const Context &ctx, CodeStream &out) const override final
              {
                ::std::map<::std::string, ::std::string> replacements;
                replacements["descriptor.fullname"] = getAbstractDescriptorClass(ctx).fqn;
                replacements["port.fullname"] = ctx.portGen->getPort(ctx, element)->name.fqn;
                replacements["port.descriptor.package"] = name.packageName.value();
                replacements["port.descriptor.class"] = name.simpleName;
                out.insertTemplateFile("templates/codegen/javagen/ir/InputDescriptor.java",
                    replacements);
                return true;
              }

              ::std::string genGetDescriptor(const Context &context, JavaType::Variable port) const
              override final
              {
                return port.name;
              }

              ::std::string genEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".port==" + rhs + ".port";
              }
              ::std::string genNEQ(const Ptr &ty, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".port!=" + rhs + ".port";
              }

              ::std::string genGet(const Ptr &ty, const Variable &port) const override final
              {
                return port + ".port.get()";
              }

              ::std::string genIsInputNew(const Ptr &ty, const Variable &port) const override final
              {
                return port + ".port.isNew()";
              }

              ::std::string genIsReadable(const Ptr &ty, const Variable &port) const override final
              {
                return port + ".port.isReadable()";
              }

              ::std::string genClearPort(const Variable &port) const override final
              {
                return port + ".port.clear()";
              }

              ::std::string genReadPort(const Ptr &ty, const Variable &port) const override final
              {
                return port + ".port.read(null)";
              }

              ::std::string genIsInputClosed(const JavaType::Ptr &ty,
                  const JavaType::Variable &port) const override final
              {
                return port + ".port.isReadShutdown()";
              }

              ::std::string genCloseInput(const JavaType::Variable &port) const override final
              {
                return port + ".port.shutdownRead()";
              }

            private:
              const model::MType element;
            };
            return context.registerType(::std::make_shared<Impl>(name, model));
          }

          JavaType::Ptr getOutputDescriptorClass(const Context &context,
              ::std::shared_ptr<const model::types::OutputType> model)
          {
            auto ty = context.findType(model);
            if (ty) {
              return ty;
            }
            auto name = context.genTypename(model);

            struct Impl: public virtual JavaType, public types::OutputType
            {
              Impl(Typename xname, ::std::shared_ptr<const model::types::OutputType> model)
                  : JavaType(xname, model), element(model->element)
              {
              }
              ~Impl()
              {
              }

              ::std::string genNewInstance(const ::std::vector<Variable> &v) const override final
              {
                return "new " + name + "(" + v.at(0) + ")";
              }

              bool isObject() const override final
              {
                return true;
              }

              bool writeClass(const Context &ctx) const override final
              {
                CodeStream out;
                generate(ctx, out);
                ctx.writeClass(name.fqn, out.getText());
                return true;
              }

              bool generate(const Context &ctx, CodeStream &out) const override final
              {
                ::std::map<::std::string, ::std::string> replacements;
                replacements["descriptor.fullname"] = getAbstractDescriptorClass(ctx).fqn;
                replacements["port.fullname"] = ctx.portGen->getPort(ctx, element)->name.fqn;
                replacements["port.descriptor.package"] = name.packageName.value();
                replacements["port.descriptor.class"] = name.simpleName;
                out.insertTemplateFile("templates/codegen/javagen/ir/OutputDescriptor.java",
                    replacements);
                return true;
              }

              ::std::string genGetDescriptor(const Context &context, JavaType::Variable port) const
              override final
              {
                return port.name;
              }

              ::std::string genEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".port==" + rhs + ".port";
              }
              ::std::string genNEQ(const Ptr &ty, const Variable &lhs, const Variable &rhs) const
              override final
              {
                return lhs + ".port!=" + rhs + ".port";
              }

              ::std::string genIsWritable(const Ptr &ty, const Variable &port) const override final
              {
                return port + ".port.isWritable()";
              }

              ::std::string genWritePort(const Variable &port, const Variable &data) const
              override final
              {
                return port + ".port.write(" + data + ",null)";
              }
              ::std::string genIsOutputClosed(const JavaType::Ptr &ty,
                  const JavaType::Variable &port) const override final
              {
                return port + ".port.isWriteShutdown()";
              }

              ::std::string genCloseOutput(const JavaType::Variable &port) const override final
              {
                return port + ".port.shutdownWrite()";
              }

            private:
              const model::MType element;
            };
            return context.registerType(::std::make_shared<Impl>(name, model));
          }

        }

        PortGen::Port::Port()
        {
        }
        PortGen::Port::~Port()
        {

        }

        PortGen::PortGen()
        {
        }
        PortGen::~PortGen()
        {
        }

        Typename PortGen::getPortDescriptor(const Context &context) const
        {
          return getAbstractDescriptorClass(context);
        }

        ::std::shared_ptr<const PortGen::Port> PortGen::getPort(const Context &context,
            model::MType element) const
        {
          auto name = context.getTypename(::std::nullopt,
              "port_" + element->name()->uniqueLocalName());
          auto ty = context.findType(name);
          if (ty) {
            return ty->self<const PortGen::Port>();
          }

          struct Impl: public Port
          {
            Impl(const Context &ctx, Typename id, const model::MType &xelement)
                : JavaType(id, nullptr), model(xelement)
            {
            }

            ~Impl()
            {
            }

            bool isObject() const override final
            {
              return true;
            }

            bool writeClass(const Context &ctx) const override final
            {
              CodeStream out;
              generate(ctx, out);
              ctx.writeClass(name.fqn, out.getText());
              return true;
            }

            bool generate(const Context &ctx, CodeStream &out) const override final
            {
              ::std::map<::std::string, ::std::string> replacements;

              auto element = ctx.getType(model)->name;
              auto optElement =
                  ctx.getType(model::types::OptType::get(model))->self<types::OptType>();

              auto iModel = model::types::InputType::get(model);
              auto oModel = model::types::OutputType::get(model);

              auto ity = ctx.getType(iModel)->name;
              auto oty = ctx.getType(oModel)->name;

              replacements["port.package"] = name.packageName.value();
              replacements["port.class"] = name.simpleName;
              replacements["element.fullname"] = element.fqn;
              replacements["opt.element.fullname"] = optElement->name.fqn;
              replacements["opt.element.nil"] = optElement->genNIL();
              replacements["make_optional_element_e"] = optElement->genNewInstance("e");
              replacements["input.fullname"] = ity.fqn;
              replacements["output.fullname"] = oty.fqn;
              replacements["timeout.fullname"] = getTimeoutClass(ctx).fqn;
              out.insertTemplateFile("templates/codegen/javagen/ir/Port.java", replacements);
              ctx.writeClass(name.fqn, out.getText());
              return true;
            }

            ::std::string genNewInstance(const ::std::vector<Variable>&) const override final
            {
              return "new " + name + "()";
            }

            ::std::string genGetReader(const ::std::string &portExpr) const
            {
              return portExpr + ".reader";
            }
            ::std::string genGetWriter(const ::std::string &portExpr) const
            {
              return portExpr + ".writer";
            }

          private:
            const model::MType model;
          }
          ;
          auto res = context.registerType(::std::make_shared<Impl>(context, name, element))->self<
              const PortGen::Port>();

          return res;
        }

        ::std::string PortGen::genWaitPorts(const Context &context,
            const ::std::vector<JavaType::Variable> &ports) const
        {
          auto descriptor = getPortDescriptor(context);
          ::std::string array = "new " + descriptor + "[] {";
          const char *sep = "";
          for (auto p : ports) {
            auto ty = p.type->self<types::PortType>();
            array += sep;
            array += ty->genGetDescriptor(context, p);
            sep = ", ";
          }
          array += "}";
          return genWaitPorts(context, array);
        }

        ::std::string PortGen::genWaitPorts(const Context &context,
            const ::std::string &ports) const
        {
          auto descriptor = getPortDescriptor(context);
          return descriptor + ".poll(" + ports + ",null,true,true)";
        }

        JavaType::Ptr PortGen::getInputType(const Context &context,
            const ::std::shared_ptr<const model::types::InputType> &model) const
        {
          return getInputDescriptorClass(context, model);
        }

        JavaType::Ptr PortGen::getOutputType(const Context &context,
            const ::std::shared_ptr<const model::types::OutputType> &model) const
        {
          return getOutputDescriptorClass(context, model);
        }

      }
    }
  }
}
