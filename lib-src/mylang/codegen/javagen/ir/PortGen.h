#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_PORTGEN_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_PORTGEN_H

#include <memory>
#include <string>
#include <vector>

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_CONTEXT_H
#include <mylang/codegen/javagen/ir/Context.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_PORTTYPE_H
#include <mylang/codegen/javagen/ir/types/PortType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_INPUTTYPE_H
#include <mylang/codegen/javagen/ir/types/InputType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_OUTPUTTYPE_H
#include <mylang/codegen/javagen/ir/types/OutputType.h>
#endif

#include <mylang/codegen/model/types/InputType.h>
#include <mylang/codegen/model/types/OutputType.h>
#include <mylang/codegen/model/types/InputOutputType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        /** A generator for all top-level functions */
        class PortGen
        {
          /** A a pipe */
        public:
          struct Port: public virtual JavaType
          {
            Port();
            virtual ~Port() = 0;

            virtual ::std::string genGetReader(const ::std::string &portExpr) const = 0;
            virtual ::std::string genGetWriter(const ::std::string &portExpr) const = 0;
          };

          /** Constructor */
        public:
          PortGen();

          /** Destructor */
        public:
          virtual ~PortGen();

          /**
           * Get the java type representing the port descriptor.
           */
        public:
          virtual Typename getPortDescriptor(const Context &context) const;

          /**
           * Create an IO port.
           * @param context a context
           * @param element the datatype for the port
           */
        public:
          virtual ::std::shared_ptr<const Port> getPort(const Context &context,
              model::MType element) const;

          /**
           * Get an output type for the specified model.
           * @param context a context
           * @param model for an input port
           */
        public:
          virtual JavaType::Ptr getInputType(const Context &context,
              const ::std::shared_ptr<const model::types::InputType> &model) const;

          /**
           * Get an input type for the specified model.
           * @param context a context
           * @param model for an input port
           */
        public:
          virtual JavaType::Ptr getOutputType(const Context &context,
              const ::std::shared_ptr<const model::types::OutputType> &model) const;

          /**
           * Generate a wait statement on a single port. Upon return the event that is desired has occurred.
           * @param port the port to wait on
           */
        public:
          virtual ::std::string genWaitPorts(const Context &context,
              const ::std::vector<JavaType::Variable> &port) const;

          /**
           * Generate a wait on a set of ports.
           */
        public:
          virtual ::std::string genWaitPorts(const Context &context,
              const ::std::string &ports) const;

        };
      }
    }
  }
}
#endif
