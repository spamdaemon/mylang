#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_BLOCKGEN_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_BLOCKGEN_H

#ifndef CLASS_MYLANG_CODESTREAM_H
#include <mylang/codegen/CodeStream.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENT_H
#include <mylang/codegen/model/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_OWNCONSTRUCTORCALL_H
#include <mylang/codegen/model/ir/OwnConstructorCall.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_EXPRESSION_H
#include <mylang/codegen/model/ir/Expression.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        class Context;

        class BlockGen
        {

          /**
           * Create a new block generator
           */
        public:
          BlockGen();

        public:
          virtual ~BlockGen();

          /**
           * Generate the specified block.
           * @param block
           * @param out the output stream
           */
        public:
          virtual void generate(const Context &context,
              const model::ir::Statement::StatementPtr &block, CodeStream &out) const;

          /**
           * Generate the specified block.
           * @param block
           * @param out the output stream
           */
        public:
          virtual void generate(const Context &context,
              const model::ir::Expression::ExpressionPtr &expr, CodeStream &out) const;
        };

      }
    }
  }
}
#endif
