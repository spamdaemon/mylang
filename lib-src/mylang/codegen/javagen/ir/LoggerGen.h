#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_LOGGERGEN_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_LOGGERGEN_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_CONTEXT_H
#include <mylang/codegen/javagen/ir/Context.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPES_BOOLEANTYPE_H
#include <mylang/codegen/javagen/ir/types/BooleanType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_CODESTREAM_H
#include <mylang/codegen/CodeStream.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_CONSTANT_H
#include <mylang/codegen/javagen/ir/Constant.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        /** A generator for all top-level functions */
        class LoggerGen
        {

          /** Constructor */
        public:
          LoggerGen();

          /** Destructor */
        public:
          virtual ~LoggerGen();

          /**
           * Generate a call to is loggable.
           * @param level the level
           * @return an boolean expression
           */
        public:
          virtual ::std::string genIsLoggable(
              const ::std::shared_ptr<const types::BooleanType> &type,
              const JavaType::Variable &v) const;

          /**
           * Generate a call to emit a log statement
           * @param level the log level
           * @param message the log message
           * @param args additional information
           */
        public:
          virtual void genEmitLog(const JavaType::Variable &level,
              const JavaType::Variable &message, const ::std::vector<JavaType::Variable> &extraArgs,
              CodeStream &out) const;

          /**
           * Generate a call to emit a log statement
           * @param level the log level
           * @param message the log message
           * @param args additional information
           */
        public:
          virtual void genEmitLog(const Constant<::std::string> &level,
              const Constant<::std::string> &message, CodeStream &out) const;

          /**
           * Emit an exceptio log.
           * @param level the log level
           * @param message the log message
           * @param exceptionExpr a java expression yielding an exception
           */
        public:
          virtual void genEmitExceptionLog(const Constant<::std::string> &level,
              const Constant<::std::string> &message, const ::std::string &exceptionExpr,
              CodeStream &out) const;
        };
      }
    }
  }
}
#endif
