#include <mylang/codegen/javagen/ir/ToNativeStringGen.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/javagen/ir/types/ToNativeStringType.h>
#include <mylang/codegen/model/types/types.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        ToNativeStringGen::ToNativeStringGen()
        {
        }

        ToNativeStringGen::~ToNativeStringGen()
        {
        }

        ToNativeStringGen::Functor ToNativeStringGen::getToString(const Context &ctx,
            const model::MType &ty) const
        {
          Functor f;
          auto jty = ctx.getType(ty)->self<types::ToNativeStringType>();
          if (jty) {
            f = [=](const ::std::string &expr) {
              return jty->toNativeString(expr);
            };
          }

          return f;
        }

      }
    }
  }
}
