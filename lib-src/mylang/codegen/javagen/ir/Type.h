#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPE_H

#ifndef CLASS_MYLANG_CODEGEN_CODESTREAM_H
#include <mylang/codegen/CodeStream.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_TYPENAME_H
#include <mylang/codegen/javagen/ir/Typename.h>
#endif

#include <iostream>
#include <memory>

namespace mylang::codegen::javagen::ir {

  /**
   * A java type
   */
  class Type: public ::std::enable_shared_from_this<Type>
  {
    /** A pointer to a Java type */
  public:
    typedef ::std::shared_ptr<const Type> Ptr;

    /**
     * Create a new simple java type
     * @param id the type id
     */
  public:
    Type(Typename id);

  public:
    virtual ~Type();

    /**
     * Get this type.
     * @return this type instance
     */
  public:
    template<class T = Type>
    inline ::std::shared_ptr<const T> self() const
    {
      return ::std::dynamic_pointer_cast<const T>(shared_from_this());
    }

    /**
     * Determine if this type is a Java object.
     * @return true if this type is a Java object
     */
  public:
    virtual bool isObject() const = 0;

    /**
     * Generate the classfile for the type, if it needs to be generated
     * @param ctx a context
     * @return true if anything was generated, false if the type is a JDK provided type.
     */
  public:
    virtual bool writeClass(const Context &ctx) const = 0;

    /**
     * Generate the body o
     * @param out an output
     * @return true if anything was generated, false if the type is a JDK provided type.
     */
  public:
    virtual bool generate(const Context &ctx, CodeStream &out) const = 0;

    /**
     * The MType for this java type
     */
  public:
    const Typename name;
  };
}
#endif
