#include <mylang/codegen/javagen/ir/exports/ExportedNamedStructType.h>
#include <mylang/codegen/javagen/ir/types/StructType.h>
#include <mylang/codegen/javagen/ir/types/NamedType.h>
#include <mylang/codegen/model/types/NamedType.h>
#include <mylang/codegen/model/types/StructType.h>
#include <mylang/codegen/javagen/ir/Context.h>

namespace mylang::codegen::javagen::ir::exports {
  namespace {
    static ::std::shared_ptr<const model::types::StructType> getBaseType(NamedTypePtr ty)
    {
      if (!ty->optModel) {
        throw ::std::invalid_argument("Not a valid model");
      }
      auto m = ty->optModel->self<mylang::codegen::model::types::NamedType>();
      auto bt = m->resolve()->self<model::types::StructType>();
      if (!bt) {
        throw ::std::invalid_argument("Basetype is not a struct");
      }
      return bt;
    }
  }

  ExportedNamedStructType::ExportedNamedStructType(const Context &ctx, Typename xexportedName,
      NamedTypePtr xbackingType)
      : ExportedJavaType(::std::move(xexportedName)), namedType(xbackingType)
  {
  }

  ExportedNamedStructType::~ExportedNamedStructType()
  {
  }

  JavaTypePtr ExportedNamedStructType::backingType() const
  {
    return namedType;
  }
  ::std::string ExportedNamedStructType::toBackingType(const VariableName &v) const
  {
    return v + ".getImpl()";
  }

  ::std::string ExportedNamedStructType::fromBackingType(const VariableName &v) const
  {
    return "new " + name.fqn + "(" + v + ")";
  }
  bool ExportedNamedStructType::isObject() const
  {
    return true;
  }
  bool ExportedNamedStructType::writeClass(const Context &ctx) const
  {
    CodeStream out;
    bool generated = generate(ctx, out);
    if (generated) {
      ctx.writeClass(name.fqn, out.getText());
    }
    return generated;
  }

  bool ExportedNamedStructType::generate(const Context &ctx, CodeStream &out) const
  {
    if (name.packageName.has_value()) {
      out << "package " << name.packageName.value() << ";" << nl();
    }

    out << "public final class " << name.simpleName << ' ';
    JavaTypeVariable implVar(VariableName("impl"), namedType);

    auto structModel = getBaseType(namedType);
    auto structType = ctx.getType(structModel)->self<ir::types::StructType>();

    JavaType::Variable structVar { VariableName::unique(), structType };

    ::std::vector<JavaType::Variable> implMembers;
    ::std::vector<ExportedJavaType::Variable> exportedMembers;
    for (size_t i = 0; i < structModel->members.size(); ++i) {
      const auto &m = structModel->members.at(i);
      auto mTy = JavaType::Variable(VariableName::unique(), ctx.getType(m.type));
      auto eTy = ExportedJavaType::Variable(VariableName::unique(), ctx.getExportedType(m.type));
      implMembers.emplace_back(mTy);
      exportedMembers.emplace_back(eTy);
    }

    out.brace([&]
    {
      out << "private final " << implVar.type->name << " " << implVar << ";" << nl();

      // constructor to create a named type from the exported basetype
      out << "public " << name.simpleName << "(";
      const char *sep = "";
      for (const auto &m : exportedMembers) {
        out << sep << m.type->name << " " << m;
        sep = ", ";
      }
      out << ") ";
      out.brace([&]
      {
        for (size_t i = 0; i < implMembers.size(); ++i) {
          const auto &implMember = implMembers.at(i);
          const auto &expMember = exportedMembers.at(i);
          out << "final " << implMember.type->name << " " << implMember << " = " //
              << expMember.type->toBackingType(expMember.name) << ";" << nl();
        }
        out << structVar.type->name << " " << structVar << " = " << structType->genNewInstance(implMembers) << ";";
        out << implVar << " = " << namedType->genNewInstance( { structVar }) << ';';
      }) << nl() << nl();

      // access the memebrs
      for (size_t i = 0; i < implMembers.size(); ++i) {
        const auto &mName = structModel->members.at(i).name;
        const auto &implMember = implMembers.at(i);
        const auto &expMember = exportedMembers.at(i);

        out << "public final " << expMember.type->name << " get_" << mName << "()";
        out.brace(
            [&]
            {
              // first, get the internal struct type
              out << "final " << structVar.type->name << " " << structVar << " = "
                  << namedType->genCastToBaseType(implVar) << ";" << nl();

              // get the member from the internal struct type
              out << "final " << implMember.type->name << " " << implMember << " = "
                  << structType->genGetMember(implMember.type, structVar, mName) << ";" << nl();
              // finally convert to the external type
              out << "return " << expMember.type->fromBackingType(implMember.name) << ";";
            }) << nl() << nl();
      }

      // constructor to create this type from its impl type
      out << "// @implementation " << nl();
      out << "public " << name.simpleName << "(" << implVar.type->name << " obj)";
      out.brace([&]
      {
        out << implVar << "=obj;";
      }) << nl() << nl();

      // constructor to create this type from its impl type
      out << "// @implementation " << nl();
      out << "public final " << implVar.type->name << " getImpl() { return " << implVar << "; }"
          << nl() << nl();

    }) << nl();
    return true;
  }

}
