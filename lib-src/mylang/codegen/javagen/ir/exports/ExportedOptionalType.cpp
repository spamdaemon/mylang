#include <mylang/codegen/javagen/ir/exports/ExportedOptionalType.h>
#include <mylang/codegen/javagen/ir/Typename.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/javagen/ir/JavaType.h>
#include <mylang/codegen/javagen/ir/types/OptType.h>
#include <mylang/codegen/javagen/ir/types/BooleanType.h>
#include <mylang/codegen/model/types/OptType.h>
#include <mylang/codegen/CodeStream.h>

namespace mylang::codegen::javagen::ir::exports {
  namespace {
    static BoxedExportedType getElementType(const Context &ctx, OptTypePtr ty)
    {
      if (!ty->optModel) {
        throw ::std::invalid_argument("Not a valid model");
      }
      auto m = ty->optModel->self<mylang::codegen::model::types::OptType>();
      auto unboxed = ctx.getType(m->element);
      auto bt = ctx.getExportedType(m->element);
      if (!bt) {
        throw ::std::invalid_argument("Could not export the optional");
      }
      return BoxedExportedType::getBoxedType(bt);
    }
  }

  ExportedOptionalType::ExportedOptionalType(const Context &ctx, OptTypePtr xbackingType)
      :
          ExportedJavaType(
              ctx.getBuiltinTypename(
                  "java.util.Optional<" + getElementType(ctx, xbackingType).boxedType.fqn + ">")),
          implType(xbackingType), elemType(getElementType(ctx, xbackingType)),
          boolType(ctx.getBooleanType())
  {
  }

  ExportedOptionalType::~ExportedOptionalType()
  {
  }

  JavaTypePtr ExportedOptionalType::backingType() const
  {
    return implType;
  }

  ::std::string ExportedOptionalType::toBackingType(const VariableName &v) const
  {
    CodeStream out;
    out << "((new java.util.function.Function<" << name << ", " << implType->name << ">() ";

    out.brace([&]
    {
      out << "public final " << implType->name << " apply(" << name << " opt)";
      out.brace([&]
      {
        VariableName unboxed = VariableName::unique();
        JavaTypeVariable elem = JavaTypeVariable(VariableName::unique(), elemType.type->backingType());

        out << "if (opt.isPresent()) ";
        out.brace(
            [&]
            {
              out << "final " << elemType.type->name << " " << unboxed << " = "
                  << elemType.unbox("opt.get()") << ";" << nl();
              out << "final " << elem.type->name << " " << elem << " = "
                  << elemType.type->toBackingType(unboxed) << ";" << nl();
              out << "return " << implType->genNewInstance(elem.name) << ";";
            });
        out << " else { return " << implType->genNIL() << "; }" << nl();
      });
    });
    out << ").apply(" << v << "))";
    return out.getText();
  }

  ::std::string ExportedOptionalType::fromBackingType(const VariableName &v) const
  {
    CodeStream out;
    JavaTypeVariable opt(VariableName::unique(), implType);
    out << "((new java.util.function.Function<" << opt.type->name << ", " << name << ">() ";

    out.brace([&]
    {
      out << "public final " << name << " apply(" << opt.type->name << " " << opt << ")";
      out.brace([&]
      {
        VariableName boxed = VariableName::unique();
        JavaTypeVariable elem = JavaTypeVariable(VariableName::unique(), elemType.type->backingType());

        out << "if (" << implType->genIsPresent(boolType, opt) << ") ";
        out.brace(
            [&]
            {
              out << "final " << elem.type->name << " " << elem << " = "
                  << implType->genGet(elem.type, opt) << ";" << nl();

              out << "return java.util.Optional.of("
                  << elemType.box(elemType.type->fromBackingType(elem.name)) << ");" << nl();
            });
        out << " else { return java.util.Optional.empty(); }" << nl();
      });
    });
    out << ").apply(" << v << "))";
    return out.getText();
  }

  bool ExportedOptionalType::isObject() const
  {
    return true;
  }
  bool ExportedOptionalType::writeClass(const Context&) const
  {
    // since we're passing the impl type through, we don't have to do anything
    return false;
  }

  bool ExportedOptionalType::generate(const Context&, CodeStream&) const
  {
    return false;
  }
  ::std::string ExportedOptionalType::allocateArray(const ::std::string &n) const
  {
    return "((" + name + "[])java.lang.reflect.Array.newInstance(java.util.Optional.class," + n
        + "))";
  }

}
