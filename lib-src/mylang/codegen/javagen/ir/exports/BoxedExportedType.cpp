#include <mylang/codegen/javagen/ir/exports/BoxedExportedType.h>
#include <mylang/codegen/javagen/ir/ExportedJavaType.h>
#include <mylang/codegen/javagen/ir/Context.h>

namespace mylang::codegen::javagen::ir::exports {

  BoxedExportedType::BoxedExportedType(ExportedJavaTypePtr xtype, Typename xboxedType,
      ::std::function<::std::string(const ::std::string&)> boxFN,
      ::std::function<::std::string(const ::std::string&)> unboxFN)
      : type(::std::move(xtype)), boxedType(xboxedType), box(::std::move(boxFN)),
          unbox(::std::move(unboxFN))
  {
  }

  BoxedExportedType BoxedExportedType::getBoxedType(ExportedJavaTypePtr type)
  {
    auto boxedType = ::std::make_unique<Typename>(type->name);
    ::std::function<::std::string(const ::std::string&)> boxFN = [&](
        const ::std::string &e) {return e;};
    ::std::function<::std::string(const ::std::string&)> unboxFN = [&](
        const ::std::string &e) {return e;};

    if (boxedType->fqn == "boolean") {
      boxedType = ::std::make_unique<Typename>(Context::getBuiltinTypename("java.lang.Boolean"));
      boxFN = [&](const ::std::string &e) {return "java.lang.Boolean.valueOf("+e+")";};
      unboxFN = [&](const ::std::string &e) {return "("+e+").booleanValue()";};
    } else if (boxedType->fqn == "byte") {
      boxedType = ::std::make_unique<Typename>(Context::getBuiltinTypename("java.lang.Byte"));
      boxFN = [&](const ::std::string &e) {return "java.lang.Byte.valueOf("+e+")";};
      unboxFN = [&](const ::std::string &e) {return "("+e+").byteValue()";};
    } else if (boxedType->fqn == "char") {
      boxedType = ::std::make_unique<Typename>(Context::getBuiltinTypename("java.lang.Character"));
      boxFN = [&](const ::std::string &e) {return "java.lang.Character.valueOf("+e+")";};
      unboxFN = [&](const ::std::string &e) {return "("+e+").charValue()";};
    } else if (boxedType->fqn == "short") {
      boxedType = ::std::make_unique<Typename>(Context::getBuiltinTypename("java.lang.Short"));
      boxFN = [&](const ::std::string &e) {return "java.lang.Short.valueOf("+e+")";};
      unboxFN = [&](const ::std::string &e) {return "("+e+").shortValue()";};
    } else if (boxedType->fqn == "int") {
      boxedType = ::std::make_unique<Typename>(Context::getBuiltinTypename("java.lang.Integer"));
      boxFN = [&](const ::std::string &e) {return "java.lang.Integer.valueOf("+e+")";};
      unboxFN = [&](const ::std::string &e) {return "("+e+").intValue()";};
    } else if (boxedType->fqn == "short") {
      boxedType = ::std::make_unique<Typename>(Context::getBuiltinTypename("java.lang.Long"));
      boxFN = [&](const ::std::string &e) {return "java.lang.Long.valueOf("+e+")";};
      unboxFN = [&](const ::std::string &e) {return "("+e+").longValue()";};
    }

    return BoxedExportedType(::std::move(type), *boxedType, ::std::move(boxFN),
        ::std::move(unboxFN));
  }

  BoxedExportedType::~BoxedExportedType()
  {
  }

}
