#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_EXPORTS_EXPORTEDNAMEDUNIONTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_EXPORTS_EXPORTEDNAMEDUNIONTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_EXPORTEDJAVATYPE_H
#include <mylang/codegen/javagen/ir/ExportedJavaType.h>
#endif

namespace mylang::codegen::javagen::ir::exports {

  /**
   * This class exports a primitive Javatype via another primitive type type.
   */
  class ExportedNamedUnionType: public ExportedJavaType
  {
    /**
     * Create a primitive exported java type.
     * @param ctx a context
     * @param exportedName the name of the exported java type
     * @param backingType the backing type
     * @throws ::std::invalid_argument if the base type of the backing type does not match the backing type of the value
     */
  public:
    ExportedNamedUnionType(const Context &ctx, Typename exportedName, NamedTypePtr backingType);

  public:
    ~ExportedNamedUnionType();

  public:
    JavaTypePtr backingType() const override final;
    ::std::string toBackingType(const VariableName &v) const override final;
    ::std::string fromBackingType(const VariableName &v) const override final;
    bool isObject() const override final;
    bool writeClass(const Context &ctx) const override final;
    bool generate(const Context &ctx, CodeStream &out) const override final;

  private:
    const NamedTypePtr namedType;
  };
}
#endif
