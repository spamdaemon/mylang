#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_EXPORTS_EXPORTEDOPTIONALTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_EXPORTS_EXPORTEDOPTIONALTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_EXPORTEDJAVATYPE_H
#include <mylang/codegen/javagen/ir/ExportedJavaType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_EXPORTS_BOXEDEXPORTEDTYPE_H
#include <mylang/codegen/javagen/ir/exports/BoxedExportedType.h>
#endif

namespace mylang::codegen::javagen::ir::exports {

  /**
   * This class exports a primitive Javatype via another primitive type type.
   */
  class ExportedOptionalType: public ExportedJavaType
  {
    /**
     * Create a primitive exported java type.
     * @param exportedName the name of the exported java type
     * @param backingType the backing type
     * @param map a function that maps between the exported type and the backingtype
     */
  public:
    ExportedOptionalType(const Context &ctx, OptTypePtr backingType);

  public:
    ~ExportedOptionalType();

  public:
    JavaTypePtr backingType() const override final;
    ::std::string toBackingType(const VariableName &v) const override final;
    ::std::string fromBackingType(const VariableName &v) const override final;
    ::std::string allocateArray(const ::std::string &n) const override final;
    bool isObject() const override final;
    bool writeClass(const Context &ctx) const override final;
    bool generate(const Context &ctx, CodeStream &out) const override final;

  private:
    const OptTypePtr implType;

  private:
    const BoxedExportedType elemType;

  private:
    const BooleanTypePtr boolType;
  };
}
#endif
