#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_EXPORTS_PRIMITIVEEXPORTEDTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_EXPORTS_PRIMITIVEEXPORTEDTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_EXPORTS_EXPORTEDTYPEVIEW_H
#include <mylang/codegen/javagen/ir/exports/ExportedTypeView.h>
#endif

namespace mylang::codegen::javagen::ir::exports {

  /**
   * This class wraps a JavaType and turns it into an ExportedJavaType.
   */
  class PrimitiveExportedType: public ExportedTypeView
  {
    /**
     * Create a primitive exported java type.
     */
  public:
    PrimitiveExportedType(JavaTypePtr implType);

  public:
    ~PrimitiveExportedType();
  };
}
#endif
