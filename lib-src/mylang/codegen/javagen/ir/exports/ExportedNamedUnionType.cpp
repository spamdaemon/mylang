#include <mylang/codegen/javagen/ir/exports/ExportedNamedUnionType.h>
#include <mylang/codegen/javagen/ir/types/UnionType.h>
#include <mylang/codegen/javagen/ir/types/NamedType.h>
#include <mylang/codegen/model/types/NamedType.h>
#include <mylang/codegen/model/types/UnionType.h>
#include <mylang/codegen/javagen/ir/Context.h>

namespace mylang::codegen::javagen::ir::exports {
  namespace {
    static ::std::shared_ptr<const model::types::UnionType> getBaseType(NamedTypePtr ty)
    {
      if (!ty->optModel) {
        throw ::std::invalid_argument("Not a valid model");
      }
      auto m = ty->optModel->self<mylang::codegen::model::types::NamedType>();
      auto bt = m->resolve()->self<model::types::UnionType>();
      if (!bt) {
        throw ::std::invalid_argument("Basetype is not a union");
      }
      return bt;
    }
  }

  ExportedNamedUnionType::ExportedNamedUnionType(const Context&, Typename xexportedName,
      NamedTypePtr xbackingType)
      : ExportedJavaType(::std::move(xexportedName)), namedType(xbackingType)
  {
  }

  ExportedNamedUnionType::~ExportedNamedUnionType()
  {
  }

  JavaTypePtr ExportedNamedUnionType::backingType() const
  {
    return namedType;
  }
  ::std::string ExportedNamedUnionType::toBackingType(const VariableName &v) const
  {
    return v + ".getImpl()";
  }

  ::std::string ExportedNamedUnionType::fromBackingType(const VariableName &v) const
  {
    return "new " + name.fqn + "(" + v + ")";
  }
  bool ExportedNamedUnionType::isObject() const
  {
    return true;
  }
  bool ExportedNamedUnionType::writeClass(const Context &ctx) const
  {
    CodeStream out;
    bool generated = generate(ctx, out);
    if (generated) {
      ctx.writeClass(name.fqn, out.getText());
    }
    return generated;
  }

  bool ExportedNamedUnionType::generate(const Context &ctx, CodeStream &out) const
  {
    if (name.packageName.has_value()) {
      out << "package " << name.packageName.value() << ";" << nl();
    }

    out << "public final class " << name.simpleName << ' ';
    JavaTypeVariable implVar(VariableName("impl"), namedType);

    auto unionModel = getBaseType(namedType);
    auto unionType = ctx.getType(unionModel)->self<ir::types::UnionType>();

    JavaType::Variable unionVar { VariableName::unique(), unionType };

    out.brace([&]
    {
      out << "private final " << implVar.type->name << " " << implVar << ";" << nl();

      // access the memebrs
      {
        auto mName = unionModel->discriminant.name;
        auto implD = JavaType::Variable(VariableName::unique(), ctx.getType(unionModel->discriminant.type));
        auto exportedD = ExportedJavaType::Variable(VariableName::unique(),
            ctx.getExportedType(unionModel->discriminant.type));

        out << "public final " << exportedD.type->name << " get_" << mName << "()";
        out.brace(
            [&]
            {
              // first, get the internal union type
              out << "final " << unionVar.type->name << " " << unionVar << " = "
                  << namedType->genCastToBaseType(implVar) << ";" << nl();

              // get the member from the internal union type
              out << "final " << implD.type->name << " " << implD << " = "
                  << unionType->genGetDiscriminant(implD.type, unionVar) << ";" << nl();
              // finally convert to the external type
              out << "return " << exportedD.type->fromBackingType(implD.name) << ";";
            }) << nl() << nl();
      }

      // constructor to create a named type from the exported basetype
      for (size_t i = 0; i < unionModel->members.size(); ++i) {
        const auto &m = unionModel->members.at(i);
        const auto &mName = m.name;
        auto implMember = JavaType::Variable(VariableName::unique(), ctx.getType(m.type));
        auto expMember = ExportedJavaType::Variable(VariableName::unique(),
            ctx.getExportedType(m.type));
        auto optImplMember = JavaType::Variable(VariableName::unique(), ctx.getType(m.type));

        out << "public static " << name.simpleName << " create_" << mName << "("
            << expMember.type->name << " " << expMember << ")";
        out.brace([&]
        {
          out << "final " << implMember.type->name << " " << implMember << " = " //
              << expMember.type->toBackingType(expMember.name) << ";" << nl();
          out << unionVar.type->name << " " << unionVar << " = " << unionType->genNewInstance(mName, implMember) << ";" << nl();
          out << "return new " << name.simpleName << "(" << namedType->genNewInstance( { unionVar })
              << ");";
        }) << nl() << nl();

        out << "public final " << expMember.type->name << " get_" << mName << "()";
        out.brace(
            [&]
            {
              // first, get the internal union type
              out << "final " << unionVar.type->name << " " << unionVar << " = "
                  << namedType->genCastToBaseType(implVar) << ";" << nl();

              // get the member from the internal union type
              out << "final " << implMember.type->name << " " << implMember << " = "
                  << unionType->genGetMember(implMember.type, unionVar, mName) << ";" << nl();
              // finally convert to the external type
              out << "return " << expMember.type->fromBackingType(implMember.name) << ";";
            }) << nl() << nl();
      }

      // constructor to create this type from its impl type
      out << "// @implementation " << nl();
      out << "public " << name.simpleName << "(" << implVar.type->name << " obj)";
      out.brace([&]
      {
        out << implVar << "=obj;";
      }) << nl() << nl();

      // accessor to create this type from its impl type
      out << "// @implementation " << nl();
      out << "public final " << implVar.type->name << " getImpl() { return " << implVar << "; }"
          << nl() << nl();

    }) << nl();
    return true;
  }

}
