#include <mylang/codegen/javagen/ir/exports/PrimitiveExportedType.h>
#include <mylang/codegen/javagen/ir/JavaType.h>

namespace mylang::codegen::javagen::ir::exports {

  PrimitiveExportedType::PrimitiveExportedType(JavaTypePtr ximpl)
      : ExportedTypeView(ximpl->name, ximpl, BidiMap::identity())
  {
  }

  PrimitiveExportedType::~PrimitiveExportedType()
  {
  }

}
