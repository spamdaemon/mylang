#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_EXPORTS_BOXEDEXPORTEDTYPE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_EXPORTS_BOXEDEXPORTEDTYPE_H

#ifndef FILE_MYLANG_CODEGEN_JAVAGEN_IR_H
#include <mylang/codegen/javagen/ir/ir.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_TYPENAME_H
#include <mylang/codegen/javagen/ir/Typename.h>
#endif

#include <functional>
#include <string>

namespace mylang::codegen::javagen::ir::exports {

  /**
   * This class exports a primitive Javatype via another primitive type type.
   */
  class BoxedExportedType
  {
    /**
     * Create a primitive exported java type.
     * @param type a possibly unboxed type
     */
  private:
    BoxedExportedType(ExportedJavaTypePtr type, Typename boxedType,
        ::std::function<::std::string(const ::std::string&)> boxFN,
        ::std::function<::std::string(const ::std::string&)> unboxFN);

  public:
    ~BoxedExportedType();

    /**
     * Create an unboxed type
     * ExportedJavaTypePtr
     */
  public:
    static BoxedExportedType getBoxedType(ExportedJavaTypePtr type);

    /** The exported type for based type of the namedType */
  public:
    const ExportedJavaTypePtr type;

    /** The name of the boxed type */
  public:
    const Typename boxedType;

    /** The function for boxing */
  public:
    const ::std::function<::std::string(const ::std::string&)> box;

    /** The function for unboxing */
  public:
    const ::std::function<::std::string(const ::std::string&)> unbox;
  };
}
#endif
