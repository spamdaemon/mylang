#include <mylang/codegen/javagen/ir/exports/ExportedTypeView.h>
#include <mylang/codegen/javagen/ir/JavaType.h>

namespace mylang::codegen::javagen::ir::exports {

  ExportedTypeView::ExportedTypeView(Typename xexportedName, JavaTypePtr xbackingType, BidiMap xmap)
      : ExportedJavaType(::std::move(xexportedName)), implType(::std::move(xbackingType)),
          map(::std::move(xmap))
  {
  }

  ExportedTypeView::~ExportedTypeView()
  {
  }
  JavaTypePtr ExportedTypeView::backingType() const
  {
    return implType;
  }
  ::std::string ExportedTypeView::toBackingType(const VariableName &v) const
  {
    return map.exportToBackingType(v);
  }

  ::std::string ExportedTypeView::fromBackingType(const VariableName &v) const
  {
    return map.backingTypeToExport(v);
  }

  bool ExportedTypeView::isObject() const
  {
    return implType->isObject();
  }
  bool ExportedTypeView::writeClass(const Context &ctx) const
  {
    // since we're passing the impl type through, we don't have to do anything
    return false;
  }

  bool ExportedTypeView::generate(const Context &ctx, CodeStream &out) const
  {
    return false;
  }

}
