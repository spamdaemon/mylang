#include <mylang/codegen/javagen/ir/exports/PrimitiveNamedType.h>
#include <mylang/codegen/javagen/ir/types/NamedType.h>
#include <mylang/codegen/javagen/ir/Context.h>

namespace mylang::codegen::javagen::ir::exports {
  namespace {
    static ExportedJavaTypePtr getBaseType(const Context &ctx, NamedTypePtr ty)
    {
      if (!ty->optModel) {
        throw ::std::invalid_argument("Not a valid model");
      }
      auto m = ty->optModel->self<mylang::codegen::model::types::NamedType>();
      auto bt = ctx.getExportedType(m->resolve());
      if (!bt) {
        throw ::std::invalid_argument("Could not export the basetype");
      }
      return bt;
    }
  }

  PrimitiveNamedType::PrimitiveNamedType(const Context &ctx, Typename xexportedName,
      NamedTypePtr xbackingType)
      : ExportedJavaType(::std::move(xexportedName)), namedType(xbackingType),
          baseType(getBaseType(ctx, xbackingType))
  {
  }

  PrimitiveNamedType::~PrimitiveNamedType()
  {
  }

  JavaTypePtr PrimitiveNamedType::backingType() const
  {
    return namedType;
  }
  ::std::string PrimitiveNamedType::toBackingType(const VariableName &v) const
  {
    return v + ".getImpl()";
  }

  ::std::string PrimitiveNamedType::fromBackingType(const VariableName &v) const
  {
    return "new " + name.fqn + "(" + v + ")";
  }
  bool PrimitiveNamedType::isObject() const
  {
    return true;
  }
  bool PrimitiveNamedType::writeClass(const Context &ctx) const
  {
    CodeStream out;
    bool generated = generate(ctx, out);
    if (generated) {
      ctx.writeClass(name.fqn, out.getText());
    }
    return generated;
  }

  bool PrimitiveNamedType::generate(const Context &ctx, CodeStream &out) const
  {

    if (name.packageName.has_value()) {
      out << "package " << name.packageName.value() << ";" << nl();
    }

    out << "public final class " << name.simpleName << ' ';
    JavaTypeVariable implVar(VariableName("impl"), namedType);

    out.brace([&]
    {
      out << "private final " << implVar.type->name << " " << implVar << ";" << nl();

      // constructor to create a named type from the exported basetype
      Variable arg(VariableName("value"), baseType);
      JavaTypeVariable implValue(VariableName("v"), baseType->backingType());

      out << "public " << name.simpleName << "(" << arg.type->name << " " << arg << ")";
      out.brace([&]
      {
        out << "final " << implValue.type->name << " " << implValue.name << " = " << baseType->toBackingType(arg.name) << ";" << nl();
    out << implVar << " = " << namedType->genNewInstance( {implValue}) << ';';
  }) << nl() << nl();

  // access the value
      out << "public final " << baseType->name << " getValue() ";
      out.brace(
          [&]
          {
            out << "final " << implValue.type->name << " " << implValue.name << " = "
                << namedType->genCastToBaseType(implVar) << ";" << nl();
            out << "return " << baseType->fromBackingType(implValue.name) << ";";
          }) << nl() << nl();

      // constructor to create this type from its impl type
      out << "// @implementation " << nl();
      out << "public " << name.simpleName << "(" << implVar.type->name << " obj)";
      out.brace([&]
      {
        out << implVar << "=obj;";
      }) << nl() << nl();

      // constructor to create this type from its impl type
      out << "// @implementation " << nl();
      out << "public final " << implVar.type->name << " getImpl() { return " << implVar << "; }"
          << nl() << nl();

    }) << nl();
    return true;
  }

}
