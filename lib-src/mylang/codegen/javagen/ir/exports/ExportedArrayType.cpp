#include <mylang/codegen/javagen/ir/exports/ExportedArrayType.h>
#include <mylang/codegen/javagen/ir/Typename.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/javagen/ir/JavaType.h>
#include <mylang/codegen/javagen/ir/types/NativeArrayType.h>
#include <mylang/codegen/javagen/ir/types/IntegerType.h>
//#include <mylang/codegen/javagen/ir/types/BooleanType.h>
#include <mylang/codegen/model/types/ArrayType.h>
//#include <mylang/codegen/model/types/IntegerType.h>
#include <mylang/codegen/CodeStream.h>
#include <mylang/codegen/javagen/ir/ExportedJavaType.h>
#include <mylang/codegen/javagen/ir/ir.h>
#include <mylang/codegen/javagen/ir/JavaTypeVariable.h>
#include <mylang/codegen/javagen/ir/VariableName.h>
#include <mylang/codegen/model/types/Type.h>
#include <memory>
#include <stdexcept>
#include <string>

namespace mylang::codegen::javagen::ir::exports {
  namespace {
    static ExportedJavaTypePtr getElementType(const Context &ctx, NativeArrayTypePtr ty)
    {
      if (!ty->optModel) {
        throw ::std::invalid_argument("Not a valid model");
      }
      auto m = ty->optModel->self<mylang::codegen::model::types::ArrayType>();
      auto bt = ctx.getExportedType(m->element);
      if (!bt) {
        throw ::std::invalid_argument("Could not export the optional");
      }
      return bt;
    }

    static IntegerTypePtr getLenType(const Context &ctx, NativeArrayTypePtr ty)
    {
      if (!ty->optModel) {
        throw ::std::invalid_argument("Not a valid model");
      }
      auto m = ty->optModel->self<mylang::codegen::model::types::ArrayType>();
      auto bt = ctx.getType(m->lenType);
      if (!bt) {
        throw ::std::invalid_argument("Could not create len type");
      }
      return bt->self<ir::types::IntegerType>();
    }

    static IntegerTypePtr getIndexType(const Context &ctx, NativeArrayTypePtr ty)
    {
      if (!ty->optModel) {
        throw ::std::invalid_argument("Not a valid model");
      }
      auto m = ty->optModel->self<mylang::codegen::model::types::ArrayType>();
      if (!m->indexType) {
        return nullptr;
      }

      auto bt = ctx.getType(m->indexType);
      if (!bt) {
        throw ::std::invalid_argument("Could not create index type");
      }
      return bt->self<ir::types::IntegerType>();
    }
  }

  ExportedArrayType::ExportedArrayType(const Context &ctx, NativeArrayTypePtr xbackingType)
      :
          ExportedJavaType(
              ctx.getBuiltinTypename(getElementType(ctx, xbackingType)->name.fqn + "[]")),
          implType(xbackingType), elemType(getElementType(ctx, xbackingType)),
          indexType(getIndexType(ctx, xbackingType)), lenType(getLenType(ctx, xbackingType))
  {
  }

  ExportedArrayType::~ExportedArrayType()
  {
  }

  JavaTypePtr ExportedArrayType::backingType() const
  {
    return implType;
  }

  ::std::string ExportedArrayType::toBackingType(const VariableName &v) const
  {
    CodeStream out;
    out << "((new java.util.function.Function<" << name << ", " << implType->name << ">() ";

    out.brace([&]
    {
      out << "public final " << implType->name << " apply(" << name << " arr)";
      out.brace([&]
      {
        const ExportedJavaType::Variable elem(VariableName::unique(), elemType);
        JavaTypeVariable arr = JavaTypeVariable(VariableName::unique(), backingType());

        out << arr.type->name << " " << arr << " = " << implType->genAllocate("arr.length") << ";" << nl();
        out << "for (int i=0;i<arr.length;++i) ";
        out.brace([&]
        {
          out << "final " << elem.type->name << " " << elem << " = arr[i];" << nl();
          out << arr << "[i] = " << elem.type->toBackingType(elem.name) << ";";
        });
        out << nl();
        out << "return " << arr << ";" << nl();
      });
    });
    out << ").apply(" << v << "))";
    return out.getText();
  }

  ::std::string ExportedArrayType::fromBackingType(const VariableName &v) const
  {
    CodeStream out;
    JavaTypeVariable arr(VariableName::unique(), implType);
    out << "((new java.util.function.Function<" << arr.type->name << ", " << name << ">() ";

    out.brace([&]
    {
      out << "public final " << name << " apply(" << arr.type->name << " " << arr << ")";
      out.brace([&]
      {
        VariableName boxed = VariableName::unique();
        JavaTypeVariable elem = JavaTypeVariable(VariableName::unique(), elemType->backingType());
        JavaTypeVariable len = JavaTypeVariable(VariableName::unique(), lenType);
        JavaTypeVariable i = JavaTypeVariable(VariableName::unique(), indexType);

        out << "final " << len.type->name << " " << len << " = " << implType->genLength(lenType, arr) << ';' << nl();
        out << "final int arrlen = " << lenType->getInt32(len) << ';' << nl();
        out << "final " << name << " arr = " << elemType->allocateArray("arrlen") << ';' << nl();
        out << "for (int i=0;i<arrlen;++i) ";
        out.brace(
            [&]
            {
              out << "final " << i.type->name << " " << i << " = "
                  << indexType->createFromInt32("i") << ';' << nl();
              out << "final " << elem.type->name << " " << elem << " = "
                  << implType->genIndex(elem.type, arr, i) << ';' << nl();
              out << "arr[i] = " << elemType->fromBackingType(elem.name) << ';' << nl();
            });
        out << nl();
        out << "return arr;" << nl();
      });
    });
    out << ").apply(" << v << "))";
    return out.getText();
  }

  bool ExportedArrayType::isObject() const
  {
    return true;
  }
  bool ExportedArrayType::writeClass(const Context &ctx) const
  {
    // since we're passing the impl type through, we don't have to do anything
    return false;
  }

  bool ExportedArrayType::generate(const Context &ctx, CodeStream &out) const
  {
    return false;
  }

}
