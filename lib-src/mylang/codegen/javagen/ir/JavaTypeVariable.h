#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPEVARIABLE_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPEVARIABLE_H

#include <iostream>

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_VARIABLENAME_H
#include <mylang/codegen/javagen/ir/VariableName.h>
#endif

#include <functional>
#include <memory>
#include <utility>
#include <string>

namespace mylang::codegen::javagen::ir {
  class JavaType;

  struct JavaTypeVariable
  {
    using JavaTypePtr = ::std::shared_ptr<const JavaType>;

  public:
    JavaTypeVariable(VariableName xn, JavaTypePtr t);

    friend inline ::std::ostream& operator<<(::std::ostream &out, const JavaTypeVariable &v)
    {
      return out << v.name.fqn;
    }
    friend inline ::std::string operator+(const ::std::string &s, const JavaTypeVariable &v)
    {
      return s + v.name;
    }
    friend inline ::std::string operator+(const JavaTypeVariable &v, const ::std::string &s)
    {
      return v.name + s;
    }
    friend inline ::std::string operator+(char ch, const JavaTypeVariable &v)
    {
      return ch + v.name;
    }
    friend inline ::std::string operator+(const JavaTypeVariable &v, char ch)
    {
      return v.name + ch;
    }

    /**
     * Convert an general expression into a variable and invoke some function to produce a new expression.
     * @param exprTy type of the expression
     * @param expr the expression
     * @param fn a function that produces an expression given a variable of exprTy, bound to the expression.
     * @return an expression and its corresponding type
     */
  public:
    static ::std::string withExpr(const JavaTypePtr &exprTy, const ::std::string &expr,
        const ::std::function<::std::pair<JavaTypePtr, ::std::string>(const JavaTypeVariable&)> &fn);

    /** The fully qualified name of the variable */
  public:
    const VariableName name;

    /* The type of the variable */
  public:
    const ::std::shared_ptr<const JavaType> type;
  };
}
#endif
