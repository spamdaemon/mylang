#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_PROCESSADAPTERGEN_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_PROCESSADAPTERGEN_H

#include <memory>
#include <string>
#include <vector>

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_PROCESSDECL_H
#include <mylang/codegen/model/ir/ProcessDecl.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JavaType_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_CONTEXT_H
#include <mylang/codegen/javagen/ir/Context.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        /** A generator for all top-level functions */
        class ProcessAdapterGen
        {
          /** Constructor */
        public:
          ProcessAdapterGen();

          /** Destructor */
        public:
          virtual ~ProcessAdapterGen();

          /**
           * Get an adapter for the specified process.
           * @param ctx a context
           * @param model a process definition
           * @return an adapter or null if not defined
           */
        public:
          virtual JavaType::Ptr getAdapter(const Context &ctx,
              model::ir::ProcessDecl::ProcessDeclPtr process) const;

        };
      }
    }
  }
}
#endif
