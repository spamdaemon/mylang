#include <mylang/codegen/javagen/ir/ProcessName.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        static ::std::string make_fqn(const ::std::optional<::std::string> &xpackagename,
            ::std::string xsimpleName)
        {
          if (xpackagename.has_value()) {
            return xpackagename.value() + "." + xsimpleName;
          } else {
            return xsimpleName;
          }
        }

        ProcessName::ProcessName(::std::optional<::std::string> xpackagename,
            ::std::string xsimpleName)
            : fqn(make_fqn(xpackagename, xsimpleName)), packageName(xpackagename),
                simpleName(xsimpleName)

        {
        }

        ProcessName::~ProcessName()
        {
        }
      }
    }
  }
}
