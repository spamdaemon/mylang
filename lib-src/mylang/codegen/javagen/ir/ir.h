#ifndef FILE_MYLANG_CODEGEN_JAVAGEN_IR_H
#define FILE_MYLANG_CODEGEN_JAVAGEN_IR_H

#include <memory>

namespace mylang::codegen::javagen::ir {
  namespace types {
    class ArrayType;
    class BooleanType;
    class IntegerType;
    class NamedType;
    class NativeArrayType;
    class OptType;
  }

  /** Forward declarations */
  class Type;
  class JavaType;
  class ExportedJavaType;
  class Context;

  /** Pointers */
  typedef ::std::shared_ptr<const Type> TypePtr;
  typedef ::std::shared_ptr<const JavaType> JavaTypePtr;
  typedef ::std::shared_ptr<const ExportedJavaType> ExportedJavaTypePtr;
  typedef ::std::shared_ptr<const types::BooleanType> BooleanTypePtr;
  typedef ::std::shared_ptr<const types::ArrayType> ArrayTypePtr;
  typedef ::std::shared_ptr<const types::IntegerType> IntegerTypePtr;
  typedef ::std::shared_ptr<const types::NativeArrayType> NativeArrayTypePtr;
  typedef ::std::shared_ptr<const types::OptType> OptTypePtr;
  typedef ::std::shared_ptr<const types::NamedType> NamedTypePtr;

}
#endif

