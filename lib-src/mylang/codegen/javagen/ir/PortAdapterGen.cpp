#include <mylang/codegen/CodeStream.h>
#include <mylang/codegen/model/types/TypeVisitor.h>
#include <mylang/codegen/javagen/ir/PortAdapterGen.h>
#include <mylang/codegen/javagen/ir/LoggerGen.h>
#include <mylang/codegen/javagen/ir/Typename.h>
#include <mylang/codegen/javagen/ir/types/OutputType.h>
#include <mylang/codegen/javagen/ir/types/InputType.h>
#include <mylang/codegen/javagen/ir/types/ByteType.h>
#include <mylang/codegen/javagen/ir/types/CharType.h>
#include <mylang/codegen/javagen/ir/types/OptType.h>
#include <mylang/codegen/javagen/ir/types/StringType.h>
#include <mylang/names/Name.h>
#include <optional>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        namespace {

          struct Adapter: public JavaType
          {
            Adapter(const Typename &xname,
                ::std::shared_ptr<const model::types::InputOutputType> xmodel)
                : JavaType(xname, xmodel), model(xmodel)
            {
            }
            ~Adapter()
            {
            }

            bool isObject() const override final
            {
              return true;
            }

            bool writeClass(const Context &ctx) const override final
            {
              CodeStream out;
              if (generate(ctx, out)) {
                ctx.writeClass(name.fqn, out.getText(), false);
                return true;
              }
              return false;
            }

            ::std::string genNewInstance(const ::std::vector<Variable> &args) const override final
            {
              return name + ".start(" + args.at(0) + ", " + args.at(1) + ")";
            }

            /**
             * Get a native port from which bytes are read.
             */
            virtual JavaType::Ptr getNativeBytePort(const Context &ctx) const = 0;

            /**
             * Get the native port that will contain the actual data that will be exchanged.
             */
            virtual JavaType::Ptr getNativePort(const Context &ctx) const = 0;

            /** Generate a call to close the port */
            virtual void genClosePort(const Context &ctx, const JavaType::Variable &port,
                CodeStream &out) const = 0;

            /** Generate an IO operator between two ports */
            virtual void genMoveData(const Context &ctx, const JavaType::Variable &portVar,
                const JavaType::Variable &nativePort, const JavaType::Variable &done,
                CodeStream &out) const = 0;

            virtual void genMakeNativePort(const Context &ctx, const JavaType::Variable &bytePort,
                const JavaType::Variable &logicalPort, CodeStream &out) const = 0;

            bool generate(const Context &ctx, CodeStream &out) const override final
            {
              auto port = ctx.getType(model);
              auto stream = getNativePort(ctx);
              auto bytestream = getNativeBytePort(ctx);
              auto boolTy = ctx.getBooleanType();
              JavaType::Variable portVar(VariableName("port"), port);
              JavaType::Variable bytestreamVar(VariableName("bytestream"), bytestream);
              JavaType::Variable streamVar(VariableName("stream"), stream);
              JavaType::Variable done(VariableName("done"), boolTy);

              if (name.packageName.has_value()) {
                out << "package " << name.packageName.value() << ";" << nl();
              }
              out.brace("public final class " + name.simpleName + " implements java.io.Closeable",
                  [&]
                  {
                    out << "private java.lang.Thread thread;" << nl();
                    out << "private final " << stream->name << " " << streamVar << ";" << nl();
                    out << "private final " << port->name << ' ' << portVar << ';' << nl();
                    // private constructor!
                    out.function(
                        "private " + name.simpleName + "(" + port->name + ' ' + portVar + ", "
                            + stream->name + " " + streamVar + ")", [&]
                        {
                          out << "this." << streamVar << " = " << streamVar << ";" << nl();
                          out << "this." << portVar << " = " << portVar << ';';
                        }) << nl();

                    out.function(
                        "public static " + name + " start(" + port->name + ' ' + portVar + ", "
                            + bytestream->name + " " + bytestreamVar + ")", [&]
                        {
                          out << "final " << stream->name << " " << streamVar << ";" << nl();
                          genMakeNativePort(ctx, bytestreamVar, streamVar, out);
                          out << nl();
                          out << "final " << name << " adapter = new " << name << "(" << portVar <<", " + streamVar+ ");" << nl();
                          out << "final java.lang.Runnable r = ()-> adapter.ioLoop();" << nl();
                          out << "adapter.thread = new java.lang.Thread(r);" << nl();
                          out << "adapter.thread.start();" << nl();
                          out << "return adapter;";
                        }) << nl();

                    out.function("public void close() throws java.io.IOException ", [&]
                    {
                      genClosePort(ctx, portVar, out);
                      out.newline();
                      out << "this." << streamVar << ".close();" << nl();
                      out << "this.thread.interrupt();";
                    });

                    out.function("public boolean join() ",
                        [&]
                        {
                          out.brace("try", [&]
                          {
                            out << "this.thread.join();" << nl();
                            out << "return true;";
                          });
                          out.brace("catch (java.lang.InterruptedException e)",
                              [&]
                              {
                                ctx.loggerGen->genEmitExceptionLog("error", "Adapter.join() failed",
                                    "e", out);
                                out << "return false;";
                              }) << nl();
                        });

                    out.function("private void ioLoop()",
                        [&]
                        {
                          out.brace("try",
                              [&]
                              {
                                out << done.type->name << " " << done << " = "
                                    << boolTy->genLiteral(false) << ';' << nl();
                                out.brace("while(!" + done + ")", [&]
                                {
                                  genMoveData(ctx, portVar, streamVar, done, out);
                                });
                              });
                          out.brace("catch (java.io.IOException e)",
                              [&]
                              {
                                ctx.loggerGen->genEmitExceptionLog("error",
                                    "Adapter terminated unexpectedly", "e", out);
                              }) << nl();
                          genClosePort(ctx, portVar, out);
                          out.newline();
                        });
                  });
              return true;
            }
          private:
            const ::std::shared_ptr<const model::types::InputOutputType> model;
          };

          struct InputAdapter: public Adapter
          {
            InputAdapter(const Typename &xname,
                ::std::shared_ptr<const model::types::OutputType> xmodel)
                : Adapter(xname, xmodel), model(xmodel)
            {
            }
            ~InputAdapter()
            {
            }
            JavaType::Ptr getNativeBytePort(const Context &ctx) const override final
            {
              return ctx.getBuiltinType("java.io.InputStream", optModel);
            }
            void genClosePort(const Context&, const JavaType::Variable &port, CodeStream &out) const
            override final
            {
              auto ty = port.type->self<types::OutputType>();
              out << ty->genCloseOutput(port) << ';';
            }

            const ::std::shared_ptr<const model::types::OutputType> model;

          };

          struct OutputAdapter: public Adapter
          {
            OutputAdapter(const Typename &xname,
                ::std::shared_ptr<const model::types::InputType> xmodel)
                : Adapter(xname, xmodel), model(xmodel)
            {
            }
            ~OutputAdapter()
            {
            }
            JavaType::Ptr getNativeBytePort(const Context &ctx) const override final
            {
              return ctx.getBuiltinType("java.io.OutputStream", optModel);
            }
            void genClosePort(const Context&, const JavaType::Variable &port, CodeStream &out) const
            override final
            {
              auto ty = port.type->self<types::InputType>();
              out << ty->genCloseInput(port) << ';';
            }

            const ::std::shared_ptr<const model::types::InputType> model;

          };

          struct ByteInputStream: public InputAdapter
          {
            ByteInputStream(const Typename &xname,
                ::std::shared_ptr<const model::types::InputType> xmodel)
                : InputAdapter(xname, xmodel->getPeer()->self<model::types::OutputType>())
            {
            }

            ~ByteInputStream()
            {
            }
            void genMakeNativePort(const Context&, const JavaType::Variable &bytePort,
                const JavaType::Variable &logicalPort, CodeStream &out) const override final
            {
              out << logicalPort << " = " << bytePort << ";";
            }

            JavaType::Ptr getNativePort(const Context &ctx) const override final
            {
              return ctx.getBuiltinType("java.io.InputStream", optModel);
            }

            /** Generate an IO operator between two ports */
            void genMoveData(const Context &ctx, const JavaType::Variable &portVar,
                const JavaType::Variable &nativePort, const JavaType::Variable &done,
                CodeStream &out) const override final
            {
              auto boolTy = ctx.getBooleanType();
              auto port = portVar.type->self<types::OutputType>();
              auto elem = ctx.getType(model->element)->self<types::ByteType>();
              JavaType::Variable elemVar(VariableName("value"), elem);

              out << "int b = " << nativePort << ".read();" << nl();
              out.brace("if (b<0)", [&]
              {
                out << done << " =  " << boolTy->genLiteral(true) << ";";
              });
              out.brace("else",
                  [&]
                  {
                    out << elemVar.type->name << " " << elemVar << " = "
                        << elem->fromNative("(byte)(b & 0x0ff)") << ";" << nl();
                    out.brace("if (!(" + port->genWritePort(portVar, elemVar) + "))", [&]
                    {
                      out << done << " =  " << boolTy->genLiteral(true) << ";";
                    });
                  });
            }

          };

          struct ByteOutputStream: public OutputAdapter
          {
            ByteOutputStream(const Typename &xname,
                ::std::shared_ptr<const model::types::OutputType> xmodel)
                : OutputAdapter(xname, xmodel->getPeer()->self<model::types::InputType>())
            {
            }
            ~ByteOutputStream()
            {
            }
            JavaType::Ptr getNativePort(const Context &ctx) const override final
            {
              return ctx.getBuiltinType("java.io.OutputStream", optModel);
            }
            void genMakeNativePort(const Context&, const JavaType::Variable &bytePort,
                const JavaType::Variable &logicalPort, CodeStream &out) const override final
            {
              out << logicalPort << " = " << bytePort << ";";
            }

            /** Generate an IO operator between two ports */
            void genMoveData(const Context &ctx, const JavaType::Variable &portVar,
                const JavaType::Variable &nativePort, const JavaType::Variable &done,
                CodeStream &out) const override final
            {
              auto boolTy = ctx.getBooleanType();
              auto port = portVar.type->self<types::InputType>();
              auto elem = ctx.getType(model->element)->self<types::ByteType>();
              auto opt_elem = ctx.getOptType(model->element);
              JavaType::Variable valueVar(VariableName("value"), opt_elem);

              out << opt_elem->name << " " << valueVar << " = "
                  << port->genReadPort(opt_elem, portVar) << ';' << nl();
              out.brace("if (!(" + opt_elem->genIsPresent(ctx.getBooleanType(), valueVar) + "))",
                  [&]
                  {
                    out << done << " =  " << boolTy->genLiteral(true) << ";";
                  });
              out.brace("else",
                  [&]
                  {
                    out << "byte v = " << elem->toNative(opt_elem->genGet(elem, valueVar)) << ";"
                        << nl();
                    out << nativePort << ".write(v);" << nl();
                    out << nativePort << ".flush();";
                  });
            }
          };

          struct CharInputStream: public InputAdapter
          {
            CharInputStream(const Typename &xname,
                ::std::shared_ptr<const model::types::InputType> xmodel)
                : InputAdapter(xname, xmodel->getPeer()->self<model::types::OutputType>())
            {
            }

            ~CharInputStream()
            {
            }
            void genMakeNativePort(const Context&, const JavaType::Variable &bytePort,
                const JavaType::Variable &logicalPort, CodeStream &out) const override final
            {
              out << logicalPort << " = new java.io.InputStreamReader(" << bytePort << ");";
            }

            JavaType::Ptr getNativePort(const Context &ctx) const override final
            {
              return ctx.getBuiltinType("java.io.Reader", optModel);
            }

            /** Generate an IO operator between two ports */
            void genMoveData(const Context &ctx, const JavaType::Variable &portVar,
                const JavaType::Variable &nativePort, const JavaType::Variable &done,
                CodeStream &out) const override final
            {
              auto boolTy = ctx.getBooleanType();
              auto port = portVar.type->self<types::OutputType>();
              auto elem = ctx.getType(model->element)->self<types::CharType>();
              JavaType::Variable elemVar(VariableName("value"), elem);

              out << "int b = " << nativePort << ".read();" << nl();
              out.brace("if (b<0)", [&]
              {
                out << done << " =  " << boolTy->genLiteral(true) << ";";
              });
              out.brace("else",
                  [&]
                  {
                    out << elemVar.type->name << " " << elemVar << " = "
                        << elem->fromNative("(char)(b & 0x0ffff)") << ";" << nl();
                    out.brace("if (!(" + port->genWritePort(portVar, elemVar) + "))", [&]
                    {
                      out << done << " =  " << boolTy->genLiteral(true) << ";";
                    });
                  });
            }

          };

          struct CharOutputStream: public OutputAdapter
          {
            CharOutputStream(const Typename &xname,
                ::std::shared_ptr<const model::types::OutputType> xmodel)
                : OutputAdapter(xname, xmodel->getPeer()->self<model::types::InputType>())
            {
            }
            ~CharOutputStream()
            {
            }
            JavaType::Ptr getNativePort(const Context &ctx) const override final
            {
              return ctx.getBuiltinType("java.io.Writer", optModel);
            }
            void genMakeNativePort(const Context&, const JavaType::Variable &bytePort,
                const JavaType::Variable &logicalPort, CodeStream &out) const override final
            {
              out << logicalPort << " = new java.io.OutputStreamWriter(" << bytePort << ");";
            }

            /** Generate an IO operator between two ports */
            void genMoveData(const Context &ctx, const JavaType::Variable &portVar,
                const JavaType::Variable &nativePort, const JavaType::Variable &done,
                CodeStream &out) const override final
            {
              auto boolTy = ctx.getBooleanType();
              auto port = portVar.type->self<types::InputType>();
              auto elem = ctx.getType(model->element)->self<types::CharType>();
              auto opt_elem = ctx.getOptType(model->element);
              JavaType::Variable valueVar(VariableName("value"), opt_elem);

              out << opt_elem->name << " " << valueVar << " = "
                  << port->genReadPort(opt_elem, portVar) << ';' << nl();
              out.brace("if (!(" + opt_elem->genIsPresent(ctx.getBooleanType(), valueVar) + "))",
                  [&]
                  {
                    out << done << " =  " << boolTy->genLiteral(true) << ";";
                  });
              out.brace("else",
                  [&]
                  {
                    out << "char ch = " << elem->toNative(opt_elem->genGet(elem, valueVar)) << ";"
                        << nl();
                    out << nativePort << ".write(ch);" << nl();
                    out << nativePort << ".flush();";
                  });
            }
          };

          struct StringInputStream: public InputAdapter
          {
            StringInputStream(const Typename &xname,
                ::std::shared_ptr<const model::types::InputType> xmodel)
                : InputAdapter(xname, xmodel->getPeer()->self<model::types::OutputType>())
            {
            }

            ~StringInputStream()
            {
            }
            void genMakeNativePort(const Context&, const JavaType::Variable &bytePort,
                const JavaType::Variable &logicalPort, CodeStream &out) const override final
            {
              out << logicalPort << " = new java.io.BufferedReader(new java.io.InputStreamReader("
                  << bytePort << "));";
            }

            JavaType::Ptr getNativePort(const Context &ctx) const override final
            {
              return ctx.getBuiltinType("java.io.BufferedReader", optModel);
            }

            /** Generate an IO operator between two ports */
            void genMoveData(const Context &ctx, const JavaType::Variable &portVar,
                const JavaType::Variable &nativePort, const JavaType::Variable &done,
                CodeStream &out) const override final
            {
              auto boolTy = ctx.getBooleanType();
              auto port = portVar.type->self<types::OutputType>();
              auto elem = ctx.getType(model->element)->self<types::StringType>();
              JavaType::Variable elemVar(VariableName("value"), elem);

              out << "java.lang.String str = " << nativePort << ".readLine();" << nl();
              out.brace("if (str==null)", [&]
              {
                out << done << " =  " << boolTy->genLiteral(true) << ";";
              });
              out.brace("else",
                  [&]
                  {
                    out << elemVar.type->name << " " << elemVar << " = " << elem->fromNative("str")
                        << ";" << nl();
                    out.brace("if (!(" + port->genWritePort(portVar, elemVar) + "))", [&]
                    {
                      out << done << " =  " << boolTy->genLiteral(true) << ";";
                    });
                  });
            }

          };

          struct StringOutputStream: public OutputAdapter
          {
            StringOutputStream(const Typename &xname,
                ::std::shared_ptr<const model::types::OutputType> xmodel)
                : OutputAdapter(xname, xmodel->getPeer()->self<model::types::InputType>())
            {
            }
            ~StringOutputStream()
            {
            }
            JavaType::Ptr getNativePort(const Context &ctx) const override final
            {
              return ctx.getBuiltinType("java.io.Writer", optModel);
            }
            void genMakeNativePort(const Context&, const JavaType::Variable &bytePort,
                const JavaType::Variable &logicalPort, CodeStream &out) const override final
            {
              out << logicalPort << " = new java.io.OutputStreamWriter(" << bytePort << ");";
            }

            /** Generate an IO operator between two ports */
            void genMoveData(const Context &ctx, const JavaType::Variable &portVar,
                const JavaType::Variable &nativePort, const JavaType::Variable &done,
                CodeStream &out) const override final
            {
              auto boolTy = ctx.getBooleanType();
              auto port = portVar.type->self<types::InputType>();
              auto elem = ctx.getType(model->element)->self<types::StringType>();
              auto opt_elem = ctx.getOptType(model->element);
              JavaType::Variable valueVar(VariableName("value"), opt_elem);

              out << opt_elem->name << " " << valueVar << " = "
                  << port->genReadPort(opt_elem, portVar) << ';' << nl();
              out.brace("if (!(" + opt_elem->genIsPresent(ctx.getBooleanType(), valueVar) + "))",
                  [&]
                  {
                    out << done << " =  " << boolTy->genLiteral(true) << ";";
                  });
              out.brace("else",
                  [&]
                  {
                    out << "java.lang.String str = "
                        << elem->toNative(opt_elem->genGet(elem, valueVar)) << ";" << nl();
                    out << nativePort << ".write(str);" << nl();
//                out << nativePort << ".write(\"\\n\");" << nl();
                    out << nativePort << ".flush();";
                  });
            }
          };

        }

        PortAdapterGen::PortAdapterGen()
        {
        }
        PortAdapterGen::~PortAdapterGen()
        {
        }

        ::std::string PortAdapterGen::getWaitAdapter(const Context&,
            const JavaType::Variable &v) const
        {
          return v + ".join()";
        }

        JavaType::Ptr PortAdapterGen::getAdapter(const Context &context,
            ::std::shared_ptr<const model::types::InputOutputType> type) const
        {
          auto name = context.getTypename(::std::nullopt,
              "Adapter" + type->name()->uniqueLocalName());
          auto ty = context.findType(name);
          if (ty) {
            return ty;
          }

          struct V: model::types::TypeVisitor
          {
            V(Typename xname, ::std::shared_ptr<const model::types::InputOutputType> xmodel)
                : name(xname), ity(xmodel->self<model::types::InputType>()),
                    oty(xmodel->self<model::types::OutputType>())
            {
            }
            ~V()
            {
            }

            void visit(const ::std::shared_ptr<const model::types::BitType>&) override final
            {
            }

            void visit(const ::std::shared_ptr<const model::types::BooleanType>&) override final
            {
            }

            void visit(const ::std::shared_ptr<const model::types::ByteType>&) override final
            {
              if (ity) {
                result = ::std::make_shared<ByteInputStream>(name, ity);
              } else {
                result = ::std::make_shared<ByteOutputStream>(name, oty);
              }
            }

            void visit(const ::std::shared_ptr<const model::types::CharType>&) override final
            {
              if (ity) {
                result = ::std::make_shared<CharInputStream>(name, ity);
              } else {
                result = ::std::make_shared<CharOutputStream>(name, oty);
              }
            }

            void visit(const ::std::shared_ptr<const model::types::RealType>&) override final
            {
            }

            void visit(const ::std::shared_ptr<const model::types::IntegerType>&) override final
            {
            }

            void visit(const ::std::shared_ptr<const model::types::StringType>&) override final
            {
              if (ity) {
                result = ::std::make_shared<StringInputStream>(name, ity);
              } else {
                result = ::std::make_shared<StringOutputStream>(name, oty);
              }
            }

            void visit(const ::std::shared_ptr<const model::types::BuilderType>&) override final
            {
            }

            void visit(const ::std::shared_ptr<const model::types::VoidType>&) override final
            {
            }

            void visit(const ::std::shared_ptr<const model::types::GenericType>&) override final
            {
            }

            void visit(const ::std::shared_ptr<const model::types::ArrayType>&) override final
            {
            }

            void visit(const ::std::shared_ptr<const model::types::UnionType>&) override final
            {
            }

            void visit(const ::std::shared_ptr<const model::types::StructType>&) override final
            {
            }

            void visit(const ::std::shared_ptr<const model::types::TupleType>&) override final
            {
            }

            void visit(const ::std::shared_ptr<const model::types::FunctionType>&) override final
            {
            }

            void visit(const ::std::shared_ptr<const model::types::MutableType>&) override final
            {
            }

            void visit(const ::std::shared_ptr<const model::types::OptType>&) override final
            {
            }

            void visit(const ::std::shared_ptr<const model::types::NamedType>&) override final
            {
            }

            void visit(const ::std::shared_ptr<const model::types::InputType>&) override final
            {
            }

            void visit(const ::std::shared_ptr<const model::types::OutputType>&) override final
            {
            }

            void visit(const ::std::shared_ptr<const model::types::ProcessType>&) override final
            {
            }
            const Typename name;
            ::std::shared_ptr<const model::types::InputType> ity;
            ::std::shared_ptr<const model::types::OutputType> oty;
            JavaType::Ptr result;
          };

          V v(name, type);
          type->element->accept(v);
          if (v.result) {
            return context.registerType(v.result);
          } else {
            return nullptr;
          }
        }

      }
    }
  }
}
