#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_CONTEXT_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_CONTEXT_H
#include <memory>
#include <string>

#ifndef FILE_MYLANG_CODEGEN_JAVAGEN_IR_H
#include <mylang/codegen/javagen/ir/ir.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_CODESTREAM_H
#include <mylang/codegen/CodeStream.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_EXPORTEDJAVATYPE_H
#include <mylang/codegen/javagen/ir/ExportedJavaType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_VARIABLENAME_H
#include <mylang/codegen/javagen/ir/VariableName.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_PROCESSNAME_H
#include <mylang/codegen/javagen/ir/ProcessName.h>
#endif

#ifndef FILE_MYLANG_CODEGEN_MODEL_H
#include <mylang/codegen/model/model.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_TYPESET_H
#include <mylang/codegen/model/types/TypeSet.h>
#endif

#ifndef CLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif

#include <optional>
#include <map>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        class BlockGen;
        class GlobalGen;
        class LoggerGen;
        class PortGen;
        class ProcessGen;
        class ProgramGen;
        class PortAdapterGen;
        class ProcessAdapterGen;
        class ToNativeStringGen;
        class RuntimeGen;

        namespace types {
          class BooleanType;
          class OptType;
          class VoidType;
        }
        /**
         * Provide context for rendering.
         */
        class Context
        {
          Context(const Context&) = delete;
          Context& operator=(const Context&) = delete;

          /** A name */
        public:
          typedef ::std::shared_ptr<const mylang::names::Name> NamePtr;

          /** A name */
        public:
          typedef mylang::codegen::javagen::ir::JavaType::Ptr JavaTypePtr;

          /** A name */
        public:
          typedef mylang::codegen::javagen::ir::ExportedJavaType::Ptr ExportedJavaTypePtr;

          /** A variable name */
        public:
          typedef mylang::codegen::javagen::ir::JavaType::Variable Variable;

          /** A loop label */
        public:
          typedef ::std::string Label;

          /**
           * Create a new root context.
           */
        public:
          Context(int optLevel, bool useNativeTypes);

        public:
          virtual ~Context();

          /**
           * Write a class out to the file system.
           * @param classname the fully qualified name of a class
           * @param contents the file contents
           * @param hasMain if true, the the class has a main
           */
        public:
          virtual void writeClass(const ::std::string &classname, const ::std::string &contents,
              bool hasMain = false) const;

          /**
           * Register a java type.
           * @param type a type
           * @return the registered type, which may be different!!
           */
        public:
          JavaTypePtr registerType(JavaTypePtr type) const;

          /**
           * Register a java type.
           * @param type a type
           * @return the registered type, which may be different!!
           */
        public:
          ExportedJavaTypePtr registerType(ExportedJavaTypePtr type) const;

          /**
           * Get the exported type for a model type. This is the type we
           * use in functions that are exported.
           * @param type a type
           * @return the name of the specified type
           */
        public:
          ExportedJavaTypePtr getExportedType(const model::MType &type) const;

          /**
           * Get the Java type
           * @param type a type
           * @return the name of the specified type
           */
        public:
          JavaTypePtr getType(const model::MType &type) const;

          /**
           * Get the Java type
           * @param type a type
           * @return the name of the specified type
           */
        public:
          JavaTypePtr getType(const mylang::codegen::model::ir::Expression &expr) const;

          /**
           * Get the name of a variable as Java string.
           * @param name the name of a variable, value, or parameter
           * @return the name of the variable (possibly fully qualified)
           */
        public:
          ProcessName getProcessName(const NamePtr &name) const;

          /**
           * Get the name of a variable as Java string.
           * @param name the name of a variable, value, or parameter
           * @return the name of the variable (possibly fully qualified)
           */
        public:
          VariableName getVariableName(const model::MVariable &expr) const;

          /**
           * Get the name of a variable as Java string.
           * @param name the name of a variable, value, or parameter
           * @return the name of the variable (possibly fully qualified)
           */
        public:
          Variable getVariable(const model::MVariable &expr) const;

          /**
           * Get the name of a label as Java string.
           * @param scope the name of a loop
           * @return the name of the loop (possibly fully qualified)
           */
        public:
          Label getLabelName(const NamePtr &name) const;

          /**
           * Get the typename for a builtin type
           * @param fqn the fqn of the builtin type
           */
        public:
          static Typename getBuiltinTypename(const ::std::string &fqn);

          /**
           * Create a new instance of a builtin type.
           * Prefer to use getBuiltinType to reduce memory allocations.
           * @param fqn the fqn of the object
           * @param optModel an optional model
           * @return a builtin type for the specified name
           */
        public:
          static JavaType::Ptr getBuiltinType(Typename fqn,
              ::mylang::codegen::model::MType optModel);

          /**
           * Get a builtin java type
           * @param fqn the fqn of the object
           * @param optModel an optional model
           * @return a builtin type for the specified name
           */
        public:
          JavaType::Ptr getBuiltinType(const ::std::string &fqn,
              ::mylang::codegen::model::MType optModel) const;

          /**
           * Get the typename for an unbounded array
           * @param element the element type.
           */
        public:
          static Typename getArrayTypename(const Typename &element);

          /**
           * Generate a typename for the specified type. If the type is already associated
           * with a name, then that name is returned, otherwise a proposed name is returned.
           * @param ty a type
           * @return a typename
           */
        public:
          Typename genTypename(const ::mylang::codegen::model::MType &type) const;

          /**
           * Get the typename for a builtin type
           * @param fqn the fqn of the builtin type
           */
        public:
          Typename getTypename(const ::std::optional<::std::string> &packagename,
              const ::std::string &classname) const;

          /**
           * Determine if the specified type is already known
           * @param name a typename
           */
        public:
          JavaTypePtr findType(const Typename &t) const;

          /**
           * Determine if the specified type is already known
           * @param name a typename
           */
        public:
          JavaTypePtr findType(const ::mylang::codegen::model::MType &t) const;

          /**
           * Get a boolean type.
           * @return a boolean type
           */
        public:
          ::std::shared_ptr<const types::VoidType> getVoidType() const;

          /**
           * Get a boolean type.
           * @return a boolean type
           */
        public:
          ::std::shared_ptr<const types::BooleanType> getBooleanType() const;

          /**
           * Get an optional for the specified model
           * @param model the element for the optional
           * @return an optional
           */
        public:
          ::std::shared_ptr<const types::OptType> getOptType(model::MType elementModel) const;

          /**
           * Generate the types.
           */
        public:
          void generateTypes() const;

          /** The name of globals */
        private:
          ::std::shared_ptr<::std::map<NamePtr, VariableName>> variables;

          /**Map of model types to types */
        private:
          mutable model::types::TypeSet uniqueModelTypes;

          /** Map of model types to types used in exports */
        private:
          ::std::shared_ptr<::std::map<model::MType, ExportedJavaTypePtr>> exportTypes;

          /** When multiple types map into the same Java type, then we need to ensure that we don't have
           * duplicate instances of the Java type. This map ensures that*/
        private:
          ::std::shared_ptr<::std::map<Typename, ExportedJavaTypePtr>> uniqueExportedTypes;

          /** Map of model types to types */
        private:
          ::std::shared_ptr<::std::map<model::MType, JavaTypePtr>> types;

          /** When multiple types map into the same Java type, then we need to ensure that we don't have
           * duplicate instances of the Java type. This map ensures that*/
        private:
          ::std::shared_ptr<::std::map<Typename, JavaTypePtr>> uniqueTypes;

          /** Use native types if possible */
        public:
          const int useNativeTypes;

          /** The optimization level */
        public:
          const int optLevel;

          /** The block generator */
        public:
          const ::std::shared_ptr<const BlockGen> blockGen;

          /** The logger generator */
        public:
          const ::std::shared_ptr<const LoggerGen> loggerGen;

          /** The program generator */
        public:
          const ::std::shared_ptr<const PortGen> portGen;

          /** The program generator */
        public:
          const ::std::shared_ptr<const ProcessGen> processGen;

          /** The program generator */
        public:
          const ::std::shared_ptr<const GlobalGen> globalGen;

          /** The program generator */
        public:
          const ::std::shared_ptr<const ProgramGen> programGen;

          /** The program generator */
        public:
          const ::std::shared_ptr<const PortAdapterGen> portAdapterGen;

          /** The program generator */
        public:
          const ::std::shared_ptr<const ProcessAdapterGen> processAdapterGen;

          /** The program generator */
        public:
          const ::std::shared_ptr<const ToNativeStringGen> toNativeStringGen;

          /** The program generator */
        public:
          const ::std::shared_ptr<const RuntimeGen> runtimeGen;

        };

      }
    }
  }
}
#endif
