#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_PORTADAPTERGEN_H

#include <memory>
#include <string>
#include <vector>

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_JAVATYPE_H
#include <mylang/codegen/javagen/ir/JavaType.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_CONTEXT_H
#include <mylang/codegen/javagen/ir/Context.h>
#endif

#include <mylang/codegen/model/types/InputType.h>
#include <mylang/codegen/model/types/OutputType.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        /** A generator for all top-level functions */
        class PortAdapterGen
        {

          /** Constructor */
        public:
          PortAdapterGen();

          /** Destructor */
        public:
          virtual ~PortAdapterGen();

          /**
           * Get an adapter type write data to the specified output type or read from the given input type.
           * @param type an IO type
           * @return a adapter object
           */
        public:
          virtual JavaType::Ptr getAdapter(const Context &context,
              ::std::shared_ptr<const model::types::InputOutputType> type) const;

          /**
           * Wait until the specified adapter has joined.
           * @param context ctx a contrext
           * @param adapter the adapter variable
           */
        public:
          virtual ::std::string getWaitAdapter(const Context &context,
              const JavaType::Variable &v) const;
        };
      }
    }
  }
}
#endif
