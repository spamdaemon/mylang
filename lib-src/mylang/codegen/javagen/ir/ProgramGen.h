#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_PROGRAMGEN_H
#define CLASS_MYLANG_CODEGEN_JAVAGEN_IR_PROGRAMGEN_H

#ifndef CLASS_MYLANG_CODEGEN_JAVAGEN_IR_CONTEXT_H
#include <mylang/codegen/javagen/ir/Context.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_PROGRAM_H
#include <mylang/codegen/model/ir/Program.h>
#endif

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        /** A generator for all top-level functions */
        class ProgramGen
        {

          /** Constructor */
        public:
          ProgramGen();

          /** Destructor */
        public:
          virtual ~ProgramGen();

          /**
           * Generate the specified block.
           * @param block
           * @param out the output stream
           */
        public:
          virtual void generate(const Context &context,
              const model::ir::Program::ProgramPtr &program) const;
        };
      }
    }
  }
}
#endif
