#include <mylang/codegen/javagen/ir/ProgramGen.h>
#include <mylang/codegen/javagen/ir/GlobalGen.h>
#include <mylang/codegen/javagen/ir/ProcessGen.h>
#include <mylang/codegen/javagen/ir/RuntimeGen.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        ProgramGen::ProgramGen()
        {
        }
        ProgramGen::~ProgramGen()
        {
        }

        void ProgramGen::generate(const Context &context,
            const model::ir::Program::ProgramPtr &program) const
        {
          for (auto global : program->globals) {
            context.globalGen->generate(context, global);
          }
          for (auto proc : program->processes) {
            context.processGen->generate(context, proc);
          }
          for (auto type : program->types) {
            // force generation of the explicitly named types, even if there
            // are no globals or processes
            context.getType(type);
          }

          context.generateTypes();
          context.runtimeGen->generate(context);
        }

      }
    }
  }
}
