#include <mylang/codegen/javagen/ir/LoggerGen.h>
#include <iostream>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {
        LoggerGen::LoggerGen()
        {
        }
        LoggerGen::~LoggerGen()
        {
        }

        ::std::string LoggerGen::genIsLoggable(
            const ::std::shared_ptr<const types::BooleanType> &type,
            const JavaType::Variable &v) const
        {
          return type->genLiteral(true);
        }

        void LoggerGen::genEmitLog(const JavaType::Variable &level,
            const JavaType::Variable &message, const ::std::vector<JavaType::Variable> &extraArgs,
            CodeStream &out) const
        {
          out << "java.lang.System.err.println(" << level << "+':'+" << message << ");";
        }

        void LoggerGen::genEmitLog(const Constant<::std::string> &level,
            const Constant<::std::string> &message, CodeStream &out) const
        {
          out << "java.lang.System.err.println(" << level << "+':'+" << message << ");";
        }

        void LoggerGen::genEmitExceptionLog(const Constant<::std::string> &level,
            const Constant<::std::string> &message, const ::std::string &exceptionExpr,
            CodeStream &out) const
        {
          out << "java.lang.System.err.println(" << level << "+':'+" << message << "+':'+("
              << exceptionExpr << ").getLocalizedMessage());" << nl();
          out << '(' << exceptionExpr << ").printStackTrace(java.lang.System.err);" << nl();
        }

      }
    }
  }
}
