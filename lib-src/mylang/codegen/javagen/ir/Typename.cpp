#include <mylang/codegen/javagen/ir/Typename.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        static ::std::string make_fqn(const ::std::optional<::std::string> &xpackagename,
            ::std::string xsimpleName)
        {
          if (xpackagename.has_value()) {
            return xpackagename.value() + "." + xsimpleName;
          } else {
            return xsimpleName;
          }
        }

        Typename::Typename(::std::optional<::std::string> xpackagename, ::std::string xsimpleName,
            Tag xtag)
            : tag(xtag), fqn(make_fqn(xpackagename, xsimpleName)), packageName(xpackagename),
                simpleName(xsimpleName)

        {
        }

        Typename::Typename(::std::string xsimpleName, Tag xtag)
            : Typename(::std::nullopt, xsimpleName, xtag)
        {
        }

        Typename::~Typename()
        {
        }

        Typename Typename::retag(Tag newTag) const
        {
          return Typename(packageName, simpleName, newTag);
        }

      }
    }
  }
}
