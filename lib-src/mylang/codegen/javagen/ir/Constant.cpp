#include <mylang/codegen/javagen/ir/Constant.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        template<>
        void Constant<::std::string>::print(::std::ostream &out) const
        {
          out << '"' << value << '"';
        }

        template<>
        void Constant<char>::print(::std::ostream &out) const
        {
          out << "'" << value << "'";
        }

      }
    }
  }
}
