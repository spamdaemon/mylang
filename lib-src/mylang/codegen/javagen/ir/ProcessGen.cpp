#include <mylang/codegen/javagen/ir/ProcessGen.h>
#include <mylang/codegen/javagen/ir/ProcessAdapterGen.h>
#include <mylang/codegen/javagen/ir/PortGen.h>
#include <mylang/codegen/javagen/ir/BlockGen.h>
#include <mylang/codegen/javagen/ir/LoggerGen.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/model/ir/Parameter.h>
#include <mylang/codegen/model/ir/ValueDecl.h>
#include <mylang/codegen/model/ir/VariableDecl.h>
#include <mylang/codegen/javagen/ir/types/InputType.h>
#include <mylang/codegen/javagen/ir/types/OutputType.h>
#include <mylang/codegen/javagen/ir/types/FunctionType.h>
#include <map>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        namespace {

          static ::std::string getPortAccessor(const ::std::string &name)
          {
            return "get_" + name;
          }

          void generatePublicPorts(const Context &context,
              const model::ir::ProcessDecl::ProcessDeclPtr &model, CodeStream &out)
          {

            // a process' input port becomes a public output port, and vice versa
            for (auto io : model->publicPorts) {
              auto inputTy = io->type->self<model::types::InputType>();
              auto outputTy = io->type->self<model::types::OutputType>();
              ::std::shared_ptr<const PortGen::Port> portTy;
              JavaType::Ptr writer, reader;
              JavaType::Ptr own, peer;
              if (inputTy) {
                portTy = context.portGen->getPort(context, inputTy->element);
                reader = context.getType(inputTy);
                writer = context.getType(model::types::OutputType::get(inputTy->element));
                own = reader;
                peer = writer;
              } else {
                portTy = context.portGen->getPort(context, outputTy->element);
                writer = context.getType(outputTy);
                reader = context.getType(model::types::InputType::get(outputTy->element));
                own = writer;
                peer = reader;
              }

              auto name = context.getVariableName(io->variable);
              auto portName = name + "_port";
              auto peerName = name + "_peer";
              out << "private final " << portTy->name << ' ' << portName << " = "
                  << portTy->genNewInstance( { }) << ';' << nl();
              out << "private final " << own->name << ' ' << name << " = "
                  << (own == reader ?
                      portTy->genGetReader(portName) : portTy->genGetWriter(portName)) << ";"
                  << nl();
              out << "@java.lang.Override" << nl();
              out << "public final " << peer->name << ' ' << getPortAccessor(io->publicName)
                  << "() { return "
                  << (own == reader ?
                      portTy->genGetWriter(portName) : portTy->genGetReader(portName)) << "; }"
                  << nl();
            }

          }

          void generateMembers(const Context &context,
              const model::ir::ProcessDecl::ProcessDeclPtr &model, CodeStream &out)
          {
            for (auto decl : model->declarations) {
              auto valDecl = decl->self<model::ir::ValueDecl>();
              auto varDecl = decl->self<model::ir::VariableDecl>();
              out << "private ";
              if (valDecl && nullptr == decl->type->self<model::types::FunctionType>()) {
                out << "final ";
              }
              model::MVariable var(valDecl ? valDecl->variable : varDecl->variable);
              out << context.getType(decl->type)->name << ' ' << context.getVariableName(var) << ';'
                  << nl();
            }

            out.brace([&]
            {
              for (auto decl : model->declarations) {
                auto valDecl = decl->self<model::ir::ValueDecl>();
                auto varDecl = decl->self<model::ir::VariableDecl>();

                model::MVariable var(valDecl ? valDecl->variable : varDecl->variable);
                model::MExpression expr(valDecl ? valDecl->value : varDecl->value);
                if (expr) {
                  out << context.getVariableName(var);
                  out << " = ";
                  context.blockGen->generate(context, expr, out);
                  out << ';' << nl();
                }
              }
            });
          }

          void generateConstructors(const Context &context, const ProcessName &name,
              const model::ir::ProcessDecl::ProcessDeclPtr &model, CodeStream &out)
          {
            // first, create an actual Java constructor for all those constructors  constructors that don't call other constructors
            // and an init method for all others
            for (auto ctor : model->constructors) {
              auto blk = context.blockGen;
              if (ctor->callConstructor) {
                out << "private void init";
              } else {
                out << "private " << name.simpleName;
              }
              out.parens(ctor->parameters, ", ",
                  [&](size_t,
                      model::ir::Parameter::ParameterPtr p) {
                        out << context.getType(p->variable->type)->name << ' ' << context.getVariableName(p->variable);
                      });
              blk->generate(context, ctor->body, out);
              out << nl() << nl();
            }

            // create a newInstance function all constructors and a start function
            for (auto ctor : model->constructors) {
              out << "private static " << name.simpleName << " newInstance";
              out.parens(ctor->parameters, ", ",
                  [&](size_t,
                      model::ir::Parameter::ParameterPtr p) {
                        out << context.getType(p->variable->type)->name << ' ' << context.getVariableName(p->variable);
                      });
              out.brace([&]
              {
                if (ctor->callConstructor) {
                  auto blk = context.blockGen;
                  for (auto s : ctor->callConstructor->pre) {
                    blk->generate(context, s, out);
                  }
                  out << "final " << name.simpleName << " instance = newInstance";
                  out.parens(ctor->callConstructor->arguments, ", ", [&](size_t, model::ir::Variable::VariablePtr v) {
                out << context.getVariable(v);
              });
                  out << ";" << nl();
                  out << "instance.init";
                  out.parens(ctor->parameters, ", ",
                      [&](size_t, model::ir::Parameter::ParameterPtr p) {
                        out << context.getVariableName(p->variable);
                      });
                  out << ";" << nl();
                  out << "return instance;";
                } else {
                  out << "return new " << name.simpleName;
                  out.parens(ctor->parameters, ", ",
                      [&](size_t, model::ir::Parameter::ParameterPtr p) {
                        out << context.getVariableName(p->variable);
                      });
                  out << ";";
                }
              }) << nl() << nl();

              out << "public static " << name.simpleName << " start";
              out.parens(ctor->parameters, ", ",
                  [&](size_t,
                      model::ir::Parameter::ParameterPtr p) {
                        out << context.getType(p->variable->type)->name << ' ' << context.getVariableName(p->variable);
                      });
              out.brace([&]
              {
                out << "final " << name.simpleName << " proc = newInstance";
                out.parens(ctor->parameters, ", ", [&](size_t, model::ir::Parameter::ParameterPtr p) {
                out << context.getVariableName(p->variable);
              }) << ';' << nl();

                out << "java.lang.Runnable r = () -> proc.mainloop();" << nl();
                out << "proc.processThread = new java.lang.Thread(r);" << nl();
                out << "proc.processThread.start();" << nl();
                out << "return proc;";
              }) << nl();
            }
          }

          ::std::vector<::std::string> generateProcessBlocks(const Context &context,
              const model::ir::ProcessDecl::ProcessDeclPtr &model, CodeStream &out)
          {
            ::std::vector<::std::string> blocks;
            for (auto block : model->blocks) {
              ::std::string blockName = "block_" + ::std::to_string(blocks.size());
              if (block->name.has_value()) {
                blockName += '_';
                blockName += block->name.value();
              }
              out << "private void " << blockName << "() ";
              context.blockGen->generate(context, block->body, out);
              out << nl();
              blocks.push_back(blockName);
            }
            return blocks;
          }

          void generateMainLoop(const Context &context,
              const model::ir::ProcessDecl::ProcessDeclPtr &model, CodeStream &out)
          {
            auto descriptor = context.portGen->getPortDescriptor(context);
            auto blocks = generateProcessBlocks(context, model, out);

            out.brace("private void mainloop()", [&]
            {
              out.brace("try", [&]
              {
                if (model->getMainBlock()) {
                  out << blocks.at(0) << "();" << nl();
                } else {
                  out.brace("while(true)", [&]
                  {

                    out << descriptor << "[] D = descriptors();" << nl();
                    out.brace("if (D.length==0)", [&]
                    {
                      context.loggerGen->genEmitLog("info", "no descriptors; exiting mainloop", out);
            out << nl();
            out << "break;";
          }) << nl();

                    out << "D = " << context.portGen->genWaitPorts(context, "descriptors()") << ";"
                        << nl();
                    out.brace("if (D.length!=0)", [&]
                    {
                      for (auto block : blocks) {
                        out << block << "();" << nl();
                      }
                    });
                  });
                }
              });
              out.brace("catch (java.lang.Exception e)",
              [&]
              {
                context.loggerGen->genEmitExceptionLog("info",
                "unexpected exception caught", "e", out);
              }) << nl();

              for (auto p : model->publicPorts) {
                auto pname = context.getVariableName(p->variable);
                auto pty = context.getType(p->type);
                JavaType::Variable pvar(pname,pty);
                if (pty->self<types::InputType>()) {
                  out << pty->self<types::InputType>()->genCloseInput(pvar) << ';' << nl();
                }
                else {
                  out << pty->self<types::OutputType>()->genCloseOutput(pvar) << ';' << nl();
                }
              }
            });
          }

          void generatePortManager(const Context &context,
              const model::ir::ProcessDecl::ProcessDeclPtr&, CodeStream &out)
          {
            auto descriptor = context.portGen->getPortDescriptor(context);
            out << "private final java.util.HashSet<" << descriptor
                << "> descriptorSet = new java.util.HashSet<>();" << nl();
            out.brace("private final " + descriptor + "[] descriptors()",
                [&]
                {
                  out.brace(
                      "for (java.util.Iterator<" + descriptor
                          + "> i=descriptorSet.iterator();i.hasNext();)", [&]
                      {
                        out << descriptor << " d = i.next();" << nl();
                        out.brace("if (d.isShutdown())", [&]
                        {
                          out << "i.remove();";
                        });
                      });

                  out << "return descriptorSet.toArray(new " << descriptor
                      << "[descriptorSet.size()]);";
                }) << nl();
            out.brace("private void blockEvents(" + descriptor + " d, boolean block)", [&]
            {
              out.brace("if (block || d.isShutdown())", [&]
              {
                out << "descriptorSet.remove(d);";
              });
              out.brace("else", [&]
              {
                out << "descriptorSet.add(d);";
              });
            }) << nl();
          }

          void generateProcessClass(const Context &context, const ProcessName &name,
              const model::ir::ProcessDecl::ProcessDeclPtr &proc, CodeStream &out)
          {
            auto pty = context.getType(proc->type)->self<types::ProcessType>();

            if (name.packageName.has_value()) {
              out << "package " << name.packageName.value() << ';' << nl();
            }
            out << "public class " << name.simpleName << " implements " << pty->name;
            out.brace([&]
            {
              out << "private java.lang.Thread processThread;" << nl();
              generatePublicPorts(context, proc, out);
              out.newlines(2);
              generateMembers(context, proc, out);
              out.newlines(2);
              generateConstructors(context, name, proc, out);
              out.newlines(2);
              generateMainLoop(context, proc, out);
              out.newlines(2);
              generatePortManager(context, proc, out);

              // some additional functions
              out.function("public void join() throws java.lang.InterruptedException", [&]
              {
                out.brace("if (this.processThread!=null)", "this.processThread.join();");
              });

            }) << nl();
          }
        }

        ProcessGen::ProcessGen()
        {
        }
        ProcessGen::~ProcessGen()
        {
        }

        void ProcessGen::generate(const Context &context,
            const model::ir::ProcessDecl::ProcessDeclPtr &proc) const
        {
          CodeStream out;
          auto name = context.getProcessName(proc->name);
          generateProcessClass(context, name, proc, out);
          context.writeClass(name.fqn, out.getText(), false);
          context.processAdapterGen->getAdapter(context, proc);
        }

        ::std::string ProcessGen::genBlockReadEvents(const Context &context,
            const JavaType::Variable &port, const JavaType::Variable &onoff) const
        {
          auto ty = port.type->self<types::PortType>();
          auto descriptor = ty->genGetDescriptor(context, port);
          return "blockEvents(" + descriptor + ", " + onoff + ")";
        }

        ::std::string ProcessGen::genBlockWriteEvents(const Context &context,
            const JavaType::Variable &port, const JavaType::Variable &onoff) const
        {
          auto ty = port.type->self<types::PortType>();
          auto descriptor = ty->genGetDescriptor(context, port);
          return "blockEvents(" + descriptor + ", " + onoff + ")";
        }

        ::std::string ProcessGen::genNewInstance(const Context &context,
            const ::std::shared_ptr<const types::ProcessType>&, const ProcessName &proc,
            const ::std::vector<Context::Variable> &args) const
        {
          ::std::string res = proc + ".start(";
          for (size_t i = 0; i < args.size(); ++i) {
            if (i != 0) {
              res += ", ";
            }
            res = res + args.at(i);
          }
          res += ')';
          return res;
        }

        ::std::shared_ptr<const types::ProcessType> ProcessGen::getProcessType(
            const Context &context,
            const ::std::shared_ptr<const model::types::ProcessType> &model) const
        {
          struct Impl: public types::ProcessType
          {
            Impl(const Context &ctx, ::std::shared_ptr<const model::types::ProcessType> xmodel)
                :
                    JavaType(
                        ctx.getTypename(std::nullopt, "proc_" + xmodel->name()->uniqueLocalName()),
                        xmodel)
            {
              // a process' input port becomes a public output port, and vice versa
              for (auto io : xmodel->ports) {
                publicPorts[io.first] = ctx.getType(io.second->getPeer());
              }
            }

            ~Impl()
            {
            }

            bool isObject() const override final
            {
              return true;
            }

            bool writeClass(const Context &ctx) const override final
            {
              CodeStream out;
              generate(ctx, out);
              ctx.writeClass(name.fqn, out.getText());

              return true;
            }

            bool generate(const Context&, CodeStream &out) const override final
            {
              if (name.packageName.has_value()) {
                out << "package " << name.packageName.value() << ";" << nl();
              }
              out << "public interface " << name.simpleName << ' ';
              out.brace([&]
              {
                for (auto p : publicPorts) {
                  out << p.second->name << ' ' << getPortAccessor(p.first) << "();" << nl();
                }
              }) << nl();
              return true;
            }

            ::std::string genGetMember(const JavaType::Ptr&, const JavaType::Variable &str,
                const ::std::string &member) const override final
            {
              return str + "." + getPortAccessor(member) + "()";
            }

            ::std::string genNewInstance(const ::std::vector<Variable>&) const override final
            {
              return unexpected("Cannot instantiate a process type");
            }

            ::std::string genEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
            override final
            {
              return lhs + " == " + rhs;
            }
            ::std::string genNEQ(const Ptr&, const Variable &lhs, const Variable &rhs) const
            override final
            {
              return lhs + " != " + rhs;
            }

            ::std::map<::std::string, JavaType::Ptr> publicPorts;
          }
          ;

          return context.registerType(::std::make_shared<Impl>(context, model))->self<
              types::ProcessType>();
        }
      }
    }
  }
}
