#include <mylang/codegen/javagen/ir/BlockGen.h>
#include <mylang/codegen/javagen/ir/LoggerGen.h>
#include <mylang/codegen/javagen/ir/ProcessGen.h>
#include <mylang/codegen/javagen/ir/PortGen.h>
#include <mylang/codegen/javagen/ir/Context.h>
#include <mylang/codegen/javagen/ir/types/ArrayBuilderType.h>
#include <mylang/codegen/javagen/ir/types/ArrayType.h>
#include <mylang/codegen/javagen/ir/types/NumericType.h>
#include <mylang/codegen/javagen/ir/types/SequenceType.h>
#include <mylang/codegen/javagen/ir/types/StructLikeType.h>
#include <mylang/codegen/javagen/ir/types/BooleanType.h>
#include <mylang/codegen/javagen/ir/types/BitLikeType.h>
#include <mylang/codegen/javagen/ir/types/ByteType.h>
#include <mylang/codegen/javagen/ir/types/MutableType.h>
#include <mylang/codegen/javagen/ir/types/FunctionType.h>
#include <mylang/codegen/javagen/ir/types/NamedType.h>
#include <mylang/codegen/javagen/ir/types/TupleType.h>
#include <mylang/codegen/javagen/ir/types/StringType.h>
#include <mylang/codegen/javagen/ir/types/InputType.h>
#include <mylang/codegen/javagen/ir/types/OutputType.h>
#include <mylang/codegen/javagen/ir/types/OptType.h>
#include <mylang/codegen/javagen/ir/types/UnionType.h>
#include <mylang/codegen/javagen/ir/types/StructType.h>
#include <mylang/codegen/javagen/ir/types/ProcessType.h>
#include <mylang/codegen/javagen/ir/types/VoidType.h>
#include <mylang/codegen/javagen/ir/types/Trampoline.h>
#include <mylang/codegen/model/ir/nodes.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <mylang/codegen/model/types/types.h>

namespace mylang {
  namespace codegen {
    namespace javagen {
      namespace ir {

        using namespace model;
        using namespace model::ir;
        using namespace model::types;

        namespace {
          struct Visitor: public NodeVisitor
          {
            Visitor(const Context &ctx, CodeStream &xout)
                : context(ctx), out(xout)
            {
            }

            ~Visitor()
            {
            }

            const Context &context;
            CodeStream &out;

            void unexpected(model::MNode node) const
            {
              throw ::std::runtime_error("Unexpected visit " + ::std::string(typeid(*node).name()));
            }

            void visitNewUnion(NewUnion::NewUnionPtr expr) override final
            {
              auto obj = context.getVariable(expr->value);
              auto ty = context.getType(*expr)->self<types::UnionType>();
              out << ty->genNewInstance(expr->member, obj);
            }

            void visitNewProcess(NewProcess::NewProcessPtr expr) override final
            {
              ::std::vector<Context::Variable> args;
              for (auto arg : expr->arguments) {
                args.push_back(context.getVariable(arg));
              }
              auto ty = context.getType(*expr)->self<types::ProcessType>();
              auto proc = context.getProcessName(expr->processName);
              out << context.processGen->genNewInstance(context, ty, proc, args);
            }

            void visitInputPortDecl(InputPortDecl::InputPortDeclPtr stmt)
            override final
            {
              unexpected(stmt);
            }

            void visitOutputPortDecl(OutputPortDecl::OutputPortDeclPtr stmt)
            override final
            {
              unexpected(stmt);
            }

            void visit(const ::std::vector<Statement::StatementPtr> stmts)
            {
              for (auto s : stmts) {
                s->accept(*this);
              }
            }

            void visitCommentStatement(CommentStatement::CommentStatementPtr stmt)
            override final
            {
//            out.blockComment(stmt->comment);
            }

            void visitStatementBlock(StatementBlock::StatementBlockPtr stmt)
            override final
            {
              out.brace([&]() {
                visit(stmt->statements);
              }) << nl();
            }

            void visitIfStatement(IfStatement::IfStatementPtr stmt) override final
            {
              auto cond = context.getVariableName(stmt->condition);
              out << "if ( " << cond << " ) ";
              out.brace([&]
              {
                visit(stmt->iftrue->statements);
              });
              if (!stmt->iffalse->statements.empty()) {
                out.brace(" else ", [&]
                {
                  visit(stmt->iffalse->statements);
                });
              }
              out.newline();
            }

            void visitTryCatch(TryCatch::TryCatchPtr stmt) override final
            {
              out.brace("try ", [&]
              {
                visit(stmt->tryBody->statements);
              });
              out.brace(" catch (java.lang.Exception e)", [&]
              {
                visit(stmt->catchBody->statements);
              });
              out.newline();
            }

            void visitOperatorExpression(OperatorExpression::OperatorExpressionPtr expr)
            override final
            {
              ::std::vector<JavaType::Variable> vars;

              auto resTy = context.getType(expr->type);
              for (auto v : expr->arguments) {
                vars.push_back( { context.getVariableName(v), context.getType(v->type) });
              }

              switch (expr->name) {
              case model::ir::OperatorExpression::OP_IS_LOGGABLE: {
                auto boolTy = resTy->self<types::BooleanType>();
                out << context.loggerGen->genIsLoggable(boolTy, vars.at(0));
                break;
              }
              case model::ir::OperatorExpression::OP_CALL:
                out
                    << vars.at(0).type->self<types::FunctionType>()->genCall(vars.at(0),
                        ::std::vector<JavaType::Variable>(vars.begin() + 1, vars.end()));
                break;
              case model::ir::OperatorExpression::OP_TO_STRING:
                out
                    << vars.at(0).type->self<types::PrimitiveLikeType>()->genToLiteral(resTy,
                        vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_GET:
                out << vars.at(0).type->self<types::WrapperLikeType>()->genGet(resTy, vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_IS_PRESENT:
                out
                    << vars.at(0).type->self<types::OptType>()->genIsPresent(
                        resTy->self<types::BooleanType>(), vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_NEW:
                out << resTy->genNewInstance(vars);
                break;
              case model::ir::OperatorExpression::OP_CHECKED_CAST:
                if (resTy->isSameType(vars.at(0).type)) {
                  out << vars.at(0);
                } else {
                  out << resTy->genCheckedCast(vars.at(0));
                }
                break;
              case model::ir::OperatorExpression::OP_UNCHECKED_CAST:
                out << resTy->genUncheckedCast(vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_NEG:
                out << vars.at(0).type->self<types::NumericType>()->genNegate(resTy, vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_ADD:
                out
                    << vars.at(0).type->self<types::NumericType>()->genAdd(resTy, vars.at(0),
                        vars.at(1));
                break;
              case model::ir::OperatorExpression::OP_SUB:
                out
                    << vars.at(0).type->self<types::NumericType>()->genSub(resTy, vars.at(0),
                        vars.at(1));
                break;
              case model::ir::OperatorExpression::OP_MUL:
                out
                    << vars.at(0).type->self<types::NumericType>()->genMul(resTy, vars.at(0),
                        vars.at(1));
                break;
              case model::ir::OperatorExpression::OP_DIV:
                out
                    << vars.at(0).type->self<types::NumericType>()->genDiv(resTy, vars.at(0),
                        vars.at(1));
                break;
              case model::ir::OperatorExpression::OP_MOD:
                out
                    << vars.at(0).type->self<types::NumericType>()->genMod(resTy, vars.at(0),
                        vars.at(1));
                break;
              case model::ir::OperatorExpression::OP_REM:
                out
                    << vars.at(0).type->self<types::NumericType>()->genRem(resTy, vars.at(0),
                        vars.at(1));
                break;
              case model::ir::OperatorExpression::OP_INDEX:
                out
                    << vars.at(0).type->self<types::SequenceType>()->genIndex(resTy, vars.at(0),
                        vars.at(1));
                break;
              case model::ir::OperatorExpression::OP_SUBRANGE:
                out
                    << vars.at(0).type->self<types::SequenceType>()->genSubrange(resTy, vars.at(0),
                        vars.at(1), vars.at(2));
                break;
              case model::ir::OperatorExpression::OP_SIZE:
                out
                    << vars.at(0).type->self<types::SequenceType>()->genLength(
                        resTy->self<types::IntegerType>(), vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_CONCATENATE:
                out
                    << resTy->self<types::SequenceType>()->genConcatenate(resTy, vars.at(0),
                        vars.at(1));
                break;
              case model::ir::OperatorExpression::OP_EQ:
                out
                    << vars.at(0).type->self<types::ComparableType>()->genEQ(resTy, vars.at(0),
                        vars.at(1));
                break;
              case model::ir::OperatorExpression::OP_NEQ:
                out
                    << vars.at(0).type->self<types::ComparableType>()->genNEQ(resTy, vars.at(0),
                        vars.at(1));
                break;
              case model::ir::OperatorExpression::OP_LT:
                out
                    << vars.at(0).type->self<types::OrderableType>()->genLT(resTy, vars.at(0),
                        vars.at(1));
                break;
              case model::ir::OperatorExpression::OP_LTE:
                out
                    << vars.at(0).type->self<types::OrderableType>()->genLTE(resTy, vars.at(0),
                        vars.at(1));
                break;
              case model::ir::OperatorExpression::OP_AND:
                out
                    << vars.at(0).type->self<types::BitLikeType>()->genAnd(resTy, vars.at(0),
                        vars.at(1));
                break;
              case model::ir::OperatorExpression::OP_OR:
                out
                    << vars.at(0).type->self<types::BitLikeType>()->genOr(resTy, vars.at(0),
                        vars.at(1));
                break;
              case model::ir::OperatorExpression::OP_XOR:
                out
                    << vars.at(0).type->self<types::BitLikeType>()->genXor(resTy, vars.at(0),
                        vars.at(1));
                break;
              case model::ir::OperatorExpression::OP_NOT:
                out << vars.at(0).type->self<types::BitLikeType>()->genNot(resTy, vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_IS_INPUT_NEW:
                out << vars.at(0).type->self<types::InputType>()->genIsInputNew(resTy, vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_IS_INPUT_CLOSED:
                out
                    << vars.at(0).type->self<types::InputType>()->genIsInputClosed(resTy,
                        vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_IS_OUTPUT_CLOSED:
                out
                    << vars.at(0).type->self<types::OutputType>()->genIsOutputClosed(resTy,
                        vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_IS_PORT_READABLE:
                out << vars.at(0).type->self<types::InputType>()->genIsReadable(resTy, vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_IS_PORT_WRITABLE:
                out << vars.at(0).type->self<types::OutputType>()->genIsWritable(resTy, vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_READ_PORT:
                out << vars.at(0).type->self<types::InputType>()->genReadPort(resTy, vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_BASETYPE_CAST:
                out << vars.at(0).type->self<types::NamedType>()->genCastToBaseType(vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_ROOTTYPE_CAST:
                out << vars.at(0).type->self<types::NamedType>()->genCastToRootType(vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_STRING_TO_CHARS:
                out
                    << vars.at(0).type->self<types::StringType>()->genToCharArray(resTy,
                        vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_BYTE_TO_UINT8:
                out
                    << vars.at(0).type->self<types::ByteType>()->genGetUnsignedInteger(
                        resTy->self<types::IntegerType>(), vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_BYTE_TO_SINT8:
                out
                    << vars.at(0).type->self<types::ByteType>()->genGetSignedInteger(
                        resTy->self<types::IntegerType>(), vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_FROM_BITS:
                out << resTy->self<types::ToBitsType>()->genFromBits(vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_TO_BITS:
                out << vars.at(0).type->self<types::ToBitsType>()->genToBits(resTy, vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_FROM_BYTES:
                out << resTy->self<types::ToBytesType>()->genFromBytes(vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_TO_BYTES:
                out << vars.at(0).type->self<types::ToBytesType>()->genToBytes(resTy, vars.at(0));
                break;
              case model::ir::OperatorExpression::OP_CLAMP:
                break;
              case model::ir::OperatorExpression::OP_WRAP:
                break;
              case model::ir::OperatorExpression::OP_HEAD:
                break;
              case model::ir::OperatorExpression::OP_TAIL:
                break;
              case model::ir::OperatorExpression::OP_LOG: {
                auto i = vars.begin();
                auto level = *i++;
                auto message = *i++;
                ::std::vector<JavaType::Variable> extraArgs(i, vars.end());
                context.loggerGen->genEmitLog(level, message, extraArgs, out);
                break;
              }
              case model::ir::OperatorExpression::OP_APPEND: {
                if (auto ty = vars.at(0).type->self<types::ArrayBuilderType>(); ty) {
                  ty->genAppend(vars.at(0), vars.at(1), out);
                } else {
                  unexpected(expr);
                }
                break;
              }
              case model::ir::OperatorExpression::OP_BUILD: {
                if (auto ty = vars.at(0).type->self<types::ArrayBuilderType>(); ty) {
                  out << ty->genBuild(resTy, vars.at(0));
                } else {
                  unexpected(expr);
                }
                break;
              }

              case model::ir::OperatorExpression::OP_SET: {
                auto ty = vars.at(0).type->self<types::MutableType>();
                out << ty->genSetValue(vars.at(0), vars.at(1));
                break;
              }
              case model::ir::OperatorExpression::OP_BLOCK_READ: {
                out << context.processGen->genBlockReadEvents(context, vars.at(0), vars.at(1));
                break;
              }
              case model::ir::OperatorExpression::OP_BLOCK_WRITE: {
                out << context.processGen->genBlockWriteEvents(context, vars.at(0), vars.at(1));
                break;
              }
              case model::ir::OperatorExpression::OP_CLEAR_PORT: {
                auto ty = vars.at(0).type->self<types::InputType>();
                out << ty->genClearPort(vars.at(0));
                break;
              }
              case model::ir::OperatorExpression::OP_CLOSE_PORT: {
                auto oty = vars.at(0).type->self<types::OutputType>();
                if (oty) {
                  out << oty->genCloseOutput(vars.at(0));
                } else {
                  auto ity = vars.at(0).type->self<types::InputType>();
                  out << ity->genCloseInput(vars.at(0));
                }
                break;
              }
              case model::ir::OperatorExpression::OP_WAIT_PORT: {
                out << context.portGen->genWaitPorts(context, vars);
                break;
              }
              case model::ir::OperatorExpression::OP_WRITE_PORT: {
                auto ty = vars.at(0).type->self<types::OutputType>();
                out << ty->genWritePort(vars.at(0), vars.at(1));
                break;
              }
              default:
                unexpected(expr);
              }

            }

            void visitProcessDecl(ProcessDecl::ProcessDeclPtr stmt)
            override final
            {
              unexpected(stmt);
            }

            void visitParameter(Parameter::ParameterPtr param)
            override final
            {
              auto ty = context.getType(param->variable->type);
              auto var = context.getVariableName(param->variable);
              out << "final " << ty->name << ' ' << var;
            }

            void visitBreakStatement(BreakStatement::BreakStatementPtr stmt)
            override final
            {
              out << "break " << context.getLabelName(stmt->scope) << ';' << nl();
            }

            void visitContinueStatement(ContinueStatement::ContinueStatementPtr stmt)
            override final
            {
              out << "continue " << context.getLabelName(stmt->scope) << ';' << nl();
            }

            void visitLambdaExpression(LambdaExpression::LambdaExpressionPtr expr)
            override final
            {
              out << "(";
              const char *sep = "";
              for (auto p : expr->parameters) {
                out << sep;
                p->accept(*this);
                sep = ", ";
              }
              out.brace(") -> ", [&]() {
                visit(expr->body->statements);
              });
            }

            void visitGetDiscriminant(GetDiscriminant::GetDiscriminantPtr expr)
            override final
            {
              auto resTy = context.getType(expr->type);
              auto ty = context.getType(*expr->object)->self<types::UnionType>();
              auto obj = context.getVariable(expr->object);
              out << ty->genGetDiscriminant(resTy, obj);
            }

            void visitGetMember(GetMember::GetMemberPtr expr)
            override final
            {
              auto resTy = context.getType(expr->type);
              auto objTy = context.getType(*expr->object);
              auto ty = objTy->self<types::StructLikeType>();
              auto obj = context.getVariable(expr->object);
              if (ty) {
                out << ty->genGetMember(resTy, obj, expr->name);
              } else {
                auto tty = objTy->self<types::TupleType>();
                auto modelTy = expr->object->type->self<TupleType>();
                auto i = modelTy->indexOfMember(expr->name);
                out << tty->genGetMember(resTy, obj, i);
              }
            }

            void visitVariable(Variable::VariablePtr expr)
            override final
            {
              out << context.getVariableName(expr);
            }

            void visitAbortStatement(AbortStatement::AbortStatementPtr stmt)
            override final
            {
              out << "throw new java.lang.RuntimeException(";
              if (stmt->value) {
                out << context.getVariableName(stmt->value);
              }
              out << ");";
            }

            void visitReturnStatement(ReturnStatement::ReturnStatementPtr stmt)
            override final
            {
              out << "return";
              if (stmt->value) {
                out << ' ';
                out << context.getVariableName(stmt->value);
              }
              out << ';';
              out.newline();
            }

            void visitThrowStatement(ThrowStatement::ThrowStatementPtr stmt)
            override final
            {
              if (stmt->value) {
                out << "/* FIXME: thrown value is ignored */" << nl();
              }
              out << "throw new java.lang.RuntimeException();";
              out.newline();
            }

            void visitOwnConstructorCall(OwnConstructorCall::OwnConstructorCallPtr stmt)
            override final
            {
              unexpected(stmt);
            }

            void visitLoopStatement(LoopStatement::LoopStatementPtr stmt)
            override final
            {
              out << context.getLabelName(stmt->name) << ':' << nl();
              out.brace("while(true) ", [&]
              {
                visit(stmt->body->statements);
              }) << nl();
            }

            void visitForeachStatement(ForeachStatement::ForeachStatementPtr stmt)
            override final
            {
              auto ty = context.getType(stmt->variable->type);
              auto vn = context.getVariableName(stmt->variable);
              auto arr = context.getVariableName(stmt->data);

              out << context.getLabelName(stmt->name) << ':' << nl();
              out.brace("for (" + ty->name.fqn + ' ' + vn + ':' + arr + ")", [&]
              {
                visit(stmt->body->statements);
              }) << nl();

            }

            void visitProcessBlock(ProcessBlock::ProcessBlockPtr stmt)
            override final
            {
              unexpected(stmt);
            }

            void visitProcessConstructor(ProcessConstructor::ProcessConstructorPtr stmt)
            override final
            {
              unexpected(stmt);
            }

            void visitLiteralBit(LiteralBit::LiteralBitPtr expr)
            override final
            {
              out << context.getType(*expr)->self<types::PrimitiveLikeType>()->genLiteral(expr);
            }

            void visitLiteralChar(LiteralChar::LiteralCharPtr expr)
            override final
            {
              out << context.getType(*expr)->self<types::PrimitiveLikeType>()->genLiteral(expr);
            }

            void visitLiteralString(LiteralString::LiteralStringPtr expr)
            override final
            {
              out << context.getType(*expr)->self<types::PrimitiveLikeType>()->genLiteral(expr);
            }

            void visitLiteralReal(LiteralReal::LiteralRealPtr expr)
            override final
            {
              out << context.getType(*expr)->self<types::PrimitiveLikeType>()->genLiteral(expr);
            }

            void visitLiteralInteger(LiteralInteger::LiteralIntegerPtr expr)
            override final
            {
              out << context.getType(*expr)->self<types::PrimitiveLikeType>()->genLiteral(expr);
            }
            void visitLiteralBoolean(LiteralBoolean::LiteralBooleanPtr expr)
            override final
            {
              auto ty = context.getType(*expr);
              auto boolTy = ty->self<types::PrimitiveLikeType>();
              out << boolTy->genLiteral(expr);
            }

            void visitLiteralByte(LiteralByte::LiteralBytePtr expr)
            override final
            {
              out << context.getType(*expr)->self<types::PrimitiveLikeType>()->genLiteral(expr);
            }

            void visitLiteralArray(LiteralArray::LiteralArrayPtr expr)
            override final
            {
              out << context.getType(*expr)->self<types::ArrayType>()->genLiteral(context, expr);
            }

            void visitLiteralNamedType(LiteralNamedType::LiteralNamedTypePtr expr)
            override final
            {
              out << context.getType(*expr)->self<types::NamedType>()->genLiteral(context, expr);
            }
            void visitLiteralOptional(LiteralOptional::LiteralOptionalPtr expr)
            override final
            {
              out << context.getType(*expr)->self<types::OptType>()->genLiteral(context, expr);
            }
            void visitLiteralStruct(LiteralStruct::LiteralStructPtr expr)
            override final
            {
              out << context.getType(*expr)->self<types::StructType>()->genLiteral(context, expr);
            }
            void visitLiteralTuple(LiteralTuple::LiteralTuplePtr expr)
            override final
            {
              out << context.getType(*expr)->self<types::TupleType>()->genLiteral(context, expr);
            }
            void visitLiteralUnion(LiteralUnion::LiteralUnionPtr expr)
            override final
            {
              out << context.getType(*expr)->self<types::UnionType>()->genLiteral(context, expr);
            }

            void visitVariableUpdate(VariableUpdate::VariableUpdatePtr stmt)
            override final
            {
              auto ty = context.getType(stmt->target->type);
              if (context.getVoidType()->isSameType(ty)) {
                // don't output anything, because we're assigning a void value to a void variable
                return;
              }

              auto target = context.getVariableName(stmt->target);
              auto value = context.getVariableName(stmt->value);
              out << target << " = " << value << ';' << nl();
            }

            void visitVariableDecl(VariableDecl::VariableDeclPtr stmt)
            override final
            {
              auto ty = context.getType(stmt->type);

              // there are no void variables, so we just ignore the variable name
              if (context.getVoidType()->isSameType(ty)) {
                return;
              }

              out << ty->name.fqn << ' ' << context.getVariableName(stmt->variable);
              out << " = ";
              if (stmt->value) {
                stmt->value->accept(*this);
              } else {
                out << ty->genZero();
              }
              out << ';' << nl();
            }

            void visitValueDecl(ValueDecl::ValueDeclPtr stmt)
            override final
            {
              auto ty = context.getType(stmt->type);

              if (context.getVoidType()->isSameType(ty)) {
                if (stmt->value && !stmt->value->self<Variable>()) {
                  stmt->value->accept(*this);
                  out << ';' << nl();
                } else {
                  // don't output anything
                }
                return;
              }

              VariableName v = context.getVariableName(stmt->variable);
              if (stmt->value && stmt->value->self<LambdaExpression>()) {
                auto fnTy = stmt->value->type->self<model::types::FunctionType>();
                auto trampoline = types::Trampoline::create(context, fnTy);
                out << "final " << trampoline->name << ' ' << v << " = "
                    << trampoline->genNewInstance( { }) << ';' << nl();
                JavaType::Variable trampolineVar(v, trampoline);
                out << "final " << ty->name << ' ' << v << "impl = ";
                stmt->value->accept(*this);
                out << ';' << nl();
                out << trampoline->genSet(trampolineVar, v + "impl") << ';' << nl();
              } else {
                out << "final " << ty->name << ' ' << v;
                if (stmt->value) {
                  out << " = ";
                  stmt->value->accept(*this);
                }
                out << ';' << nl();
              }
            }

            void visitProgram(Program::ProgramPtr node)
            {
              unexpected(node);
            }

            void visitGlobalValue(GlobalValue::GlobalValuePtr node)
            {
              unexpected(node);
            }

            void visitNoValue(NoValue::NoValuePtr node)
            {
              out << context.getType(node->type)->genZero();
            }
          }
          ;
        }

        BlockGen::BlockGen()
        {
        }
        BlockGen::~BlockGen()
        {
        }

        void BlockGen::generate(const Context &context,
            const model::ir::Expression::ExpressionPtr &expr, CodeStream &out) const
        {
          Visitor v(context, out);
          expr->accept(v);
        }

        void BlockGen::generate(const Context &context, const Statement::StatementPtr &block,
            CodeStream &out) const
        {
          Visitor v(context, out);
          block->accept(v);

        }
      }
    }
  }
}
