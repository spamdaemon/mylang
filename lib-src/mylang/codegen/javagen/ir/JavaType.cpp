#include <mylang/codegen/javagen/ir/JavaType.h>
#include <stdexcept>
#include <typeinfo>
#include <typeindex>
#include <utility>

namespace mylang::codegen::javagen::ir {
  using namespace std::rel_ops;

  JavaType::JavaType(Typename xid, ModelType m)
      : Type(xid), optModel(::std::move(m))
  {
  }

  JavaType::~JavaType()
  {
  }

  JavaType::Ptr JavaType::getBoxedType() const
  {
    if (!isObject()) {
      return nullptr;
    }
    return self();
  }

  ::std::string JavaType::box(const ::std::string &expr) const
  {
    return expr;
  }

  ::std::string JavaType::unbox(const ::std::string &expr) const
  {
    return expr;
  }

  ::std::string JavaType::genZero() const
  {
    if (isObject()) {
      return "null";
    } else {
      return "0";
    }
  }

  bool JavaType::isSameType(const ::std::shared_ptr<const JavaType> &other) const
  {
    if (this == other.get()) {
      return true;
    }
    if (name != other->name) {
      return false;
    }
    const ::std::type_info &THIS = typeid(*this);
    const ::std::type_info &THAT = typeid(*other);
    return ::std::type_index(THIS) == ::std::type_index(THAT);
  }

  ::std::string JavaType::unexpected(const ::std::string &message) const
  {
    throw ::std::runtime_error("Unexpected call " + message);
  }

  ::std::string JavaType::unexpectedUncheckedCast(const Variable &op) const
  {
    throw ::std::runtime_error("Unexpected unchecked_cast from " + op.type->name + " to " + name);
  }

  ::std::string JavaType::unexpectedCheckedCast(const Variable &op) const
  {
    throw ::std::runtime_error("Unexpected checked_cast from " + op.type->name + " to " + name);
  }

  ::std::string JavaType::genCheckedCast(const Variable &op) const
  {
    return unexpectedCheckedCast(op);
  }
  ::std::string JavaType::genUncheckedCast(const Variable &op) const
  {
    return unexpectedUncheckedCast(op);
  }
  ExportedJavaTypePtr JavaType::exportType(const Context&) const
  {
    return nullptr;
  }
  ExportedJavaTypePtr JavaType::exportTypeAs(const Context&, const Typename&,
      const NamedTypePtr&) const
  {
    return nullptr;
  }

}
