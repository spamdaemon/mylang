#include <mylang/codegen/javagen/runtools/bash/ScriptBuilder.h>
#include <iostream>

namespace mylang::codegen::javagen::runtools::bash {

  ScriptBuilder::ScriptBuilder()
  {

  }

  ScriptBuilder& ScriptBuilder::setMainClass(const ::mylang::names::FQN &classname)
  {
    mainClass = classname.fullName(".");
    return *this;
  }

  bool ScriptBuilder::write(::std::ostream &runFile) const
  {
    if (mainClass.has_value() == false) {
      return false;
    }
    const ::std::string &classname = *mainClass;

    runFile << "#!/bin/bash" << ::std::endl;
    runFile << ::std::endl;
    runFile << "SCRIPT_DIR=\"$(dirname \"$0\")\"" << ::std::endl;
    runFile << ::std::endl;
    runFile << "exec java -cp \"${SCRIPT_DIR}\"/target/classes " << classname << " \"$@\""
        << ::std::endl;
    return true;
  }
}
