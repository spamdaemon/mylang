#ifndef MYLANG_CODEGEN_JAVEGEN_RUNTOOLS_BASH_SCRIPTBUILDER_H
#define MYLANG_CODEGEN_JAVEGEN_RUNTOOLS_BASH_SCRIPTBUILDER_H

#ifndef MYLANG_NAMES_FQN_H
#include <mylang/names/FQN.h>
#endif

#include <iosfwd>
#include <set>

namespace mylang::codegen::javagen::runtools::bash {

  /**
   * A builder for a POM file.
   *
   */
  struct ScriptBuilder
  {
    /**
     * Default constructor.
     * The artifact will be "none" and the version "1.0-SNAPSHOT"
     */
  public:
    ScriptBuilder();

    /**
     * Add a class for a main program.
     * @param fqn an FQN
     * @return this builder
     */
  public:
    ScriptBuilder& setMainClass(const ::mylang::names::FQN &classname);

    /**
     * Write this POM to the specified stream.
     * @param out a stream
     * @return true if a build script was written false otherwise
     */
  public:
    bool write(::std::ostream &out) const;

    /** The main classes */
  private:
    ::std::optional<::std::string> mainClass;
  };
}

#endif
