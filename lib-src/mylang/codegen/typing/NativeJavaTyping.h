#ifndef CLASS_MYLANG_CODEGEN_TYPING_NATIVEJAVATYPING_H
#define CLASS_MYLANG_CODEGEN_TYPING_NATIVEJAVATYPING_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

namespace mylang {
  namespace codegen {
    namespace typing {

      /**
       * Convert ethir types into native Java types.
       */
      class NativeJavaTyping
      {
      public:
        NativeJavaTyping();

      public:
        virtual ~NativeJavaTyping();

        /**
         * Convert an ethir type into a new ethir type that represents
         * the best or smallest native type. If a conversion cannot be performed
         * the argument is returned.
         * @return a type (never nullptr)
         */
      public:
        virtual mylang::ethir::EType convert(const mylang::ethir::EType &type) const;

        /** An operator */
      public:
        inline mylang::ethir::EType operator()(const mylang::ethir::EType &type) const
        {
          return convert(type);
        }

        /** The java types for integers */
      protected:
        const mylang::ethir::EType signed_integer;
        const mylang::ethir::EType unsigned_integer;

        const mylang::ethir::EType sint8;
        const mylang::ethir::EType sint16;
        const mylang::ethir::EType sint32;
        const mylang::ethir::EType sint64;

        const mylang::ethir::EType uint8;
        const mylang::ethir::EType uint16;
        const mylang::ethir::EType uint32;
        const mylang::ethir::EType uint64;

      };
    }
  }
}
#endif
