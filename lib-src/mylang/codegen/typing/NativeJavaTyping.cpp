#include <mylang/codegen/typing/NativeJavaTyping.h>
#include <mylang/ethir/types/types.h>

#include <memory>
#include <stdexcept>
#include <vector>
#include <limits>

namespace mylang {
  namespace codegen {
    namespace typing {

      NativeJavaTyping::NativeJavaTyping()
          : signed_integer(mylang::ethir::types::IntegerType::create()),
              unsigned_integer(mylang::ethir::types::IntegerType::create(Interval::NON_NEGATIVE())),
              sint8(
                  mylang::ethir::types::IntegerType::create(
                      ::std::numeric_limits<::std::int8_t>::min(),
                      ::std::numeric_limits<::std::int8_t>::max())),
              sint16(
                  mylang::ethir::types::IntegerType::create(
                      ::std::numeric_limits<::std::int16_t>::min(),
                      ::std::numeric_limits<::std::int16_t>::max())),
              sint32(
                  mylang::ethir::types::IntegerType::create(
                      ::std::numeric_limits<::std::int32_t>::min(),
                      ::std::numeric_limits<::std::int32_t>::max())),
              sint64(
                  mylang::ethir::types::IntegerType::create(
                      ::std::numeric_limits<::std::int64_t>::min(),
                      ::std::numeric_limits<::std::int64_t>::max())),
              uint8(
                  mylang::ethir::types::IntegerType::create(
                      BigUInt(::std::numeric_limits<::std::uint8_t>::min()),
                      BigUInt(::std::numeric_limits<::std::uint8_t>::max()))),
              uint16(
                  mylang::ethir::types::IntegerType::create(
                      BigUInt(::std::numeric_limits<::std::uint16_t>::min()),
                      BigUInt(::std::numeric_limits<::std::uint16_t>::max()))),
              uint32(
                  mylang::ethir::types::IntegerType::create(
                      BigUInt(::std::numeric_limits<::std::uint32_t>::min()),
                      BigUInt(::std::numeric_limits<::std::uint32_t>::max()))),
              uint64(
                  mylang::ethir::types::IntegerType::create(
                      BigUInt(::std::numeric_limits<::std::uint64_t>::min()),
                      BigUInt(::std::numeric_limits<::std::uint64_t>::max())))
      {

      }

      NativeJavaTyping::~NativeJavaTyping()
      {
      }

      mylang::ethir::EType NativeJavaTyping::convert(const mylang::ethir::EType &type) const
      {
        auto ty = type->self<mylang::ethir::types::IntegerType>();
        if (ty) {
          if (ty->range.min().isInfinite()) {
            return signed_integer;
          }
          if (ty->range.max().isInfinite()) {
            if (ty->range.min() >= 0) {
              return unsigned_integer;
            } else {
              return signed_integer;
            }
          }

          if (ty->range.min()->isSigned8() && ty->range.max()->isSigned8()) {
            return sint8;
          }
          if (ty->range.min()->isSigned16() && ty->range.max()->isSigned16()) {
            return sint16;
          }
          if (ty->range.min()->isSigned32() && ty->range.max()->isSigned32()) {
            return sint32;
          }
          if (ty->range.min()->isSigned64() && ty->range.max()->isSigned64()) {
            return sint64;
          }

          if (ty->range.min()->isUnsigned8() && ty->range.max()->isUnsigned8()) {
            return uint8;
          }
          if (ty->range.min()->isUnsigned16() && ty->range.max()->isUnsigned16()) {
            return uint16;
          }
          if (ty->range.min()->isUnsigned32() && ty->range.max()->isUnsigned32()) {
            return uint32;
          }
          if (ty->range.min()->isUnsigned64() && ty->range.max()->isUnsigned64()) {
            return uint64;
          }
          if (ty->range.min()->isUnsigned64() && ty->range.max()->isUnsigned64()) {
            return uint64;
          }

          if (ty->range.min() >= 0) {
            return unsigned_integer;
          }
          return signed_integer;
        }
        return ty;
      }
    }
  }
}
