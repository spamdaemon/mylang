#include <mylang/codegen/CodeStream.h>
#include <string>
#include <fstream>
#include <cassert>

#define TEMPLATE_ROOT_HELPER(DIR) "" #DIR
#define TEMPLATE_ROOT(CONFIG_DIR) TEMPLATE_ROOT_HELPER(CONFIG_DIR)

namespace mylang {
  namespace codegen {
    static const char *INDENT_TEXT = "   ";

    CodeStream::CodeStream(size_t indent)
        : _indent(indent)
    {
    }

    CodeStream::CodeStream(const CodeStream &src)
        : _indent(src._indent)
    {
    }

    CodeStream::~CodeStream()
    {
    }

    ::std::ostream& CodeStream::stream()
    {
      return _stream;
    }

    ::std::string CodeStream::getText() const
    {
      return _stream.str();
    }

    CodeStream& CodeStream::newline(IndentOp op)
    {
      if (op < 0) {
        assert(_indent > 0);
        --_indent;
      } else if (op > 0) {
        ++_indent;
      }
      _stream << ::std::endl;
      for (size_t i = 0; i < _indent; ++i) {
        _stream << INDENT_TEXT;
      }
      return *this;
    }

    CodeStream& CodeStream::newlines(size_t n, IndentOp op)
    {
      for (size_t i = 0; i < n; ++i) {
        newline(op);
      }
      return *this;
    }

    CodeStream& CodeStream::function(const ::std::string &signature, const ::std::string &body)
    {
      return function(signature, [&]
      {
        _stream << body;
      });
    }

    CodeStream& CodeStream::function(const ::std::string &signature, ::std::function<void()> fn)
    {
      _stream << signature;
      newline();
      _stream << '{';
      newline(INDENT);
      fn();
      newline(OUTDENT);
      _stream << '}';
      newlines(2);
      return *this;
    }

    CodeStream& CodeStream::brace(const ::std::string &before, const ::std::string &content)
    {
      return brace(before, [&]() {
        _stream << content;
      });
    }

    CodeStream& CodeStream::brace(const ::std::string &before, ::std::function<void()> fn)
    {
      _stream << before;
      return brace(fn);
    }

    CodeStream& CodeStream::brace(::std::function<void()> fn)
    {
      _stream << '{';
      newline(INDENT);
      fn();
      newline(OUTDENT);
      _stream << '}';
      return *this;
    }

    CodeStream& CodeStream::parens(const ::std::string &before, ::std::function<void()> fn)
    {
      _stream << before;
      return parens(fn);
    }

    CodeStream& CodeStream::parens(::std::function<void()> fn)
    {
      _stream << '(';
      fn();
      _stream << ')';
      return *this;
    }

    CodeStream& CodeStream::lineComment(const ::std::string &comment, bool emitNewLine)
    {
      _stream << "// " << comment;
      if (emitNewLine) {
        newline();
      }
      return *this;
    }

    CodeStream& CodeStream::blockComment(const ::std::string &comment, bool emitNewLine)
    {
      _stream << "/*";
      newline(INDENT);
      for (char ch : comment) {
        if (ch == '\n') {
          newline();
        } else {
          _stream << ch;
        }
      }
      newline(OUTDENT);
      _stream << "*/";
      if (emitNewLine) {
        newline();
      }
      return *this;
    }

    CodeStream& CodeStream::insertTemplateFile(const ::std::string &file,
        const ::std::map<::std::string, ::std::string> &replacements)
    {
      ::std::string templateFile(TEMPLATE_ROOT(TEMPLATE_DIR));
      templateFile += '/';
      templateFile += file;
      ::std::ifstream in(templateFile);
      if (!in.is_open()) {
        throw ::std::runtime_error("Failed to open file " + templateFile);
      }
      try {
#if 0
        ::std::cerr << "File " << templateFile << ::std::endl;
        for (auto i :replacements) {
          ::std::cerr << "  '"<< i.first << "' = " << i.second << ::std::endl;
        }
#endif
        return insertTemplate(in, replacements);
      } catch (const ::std::exception &e) {
        throw ::std::runtime_error(file + ":" + e.what());
      }
    }

    CodeStream& CodeStream::insertTemplateString(const ::std::string &str,
        const ::std::map<::std::string, ::std::string> &replacements)
    {
      auto text = str;

      ::std::string::size_type i = 0;
      while (i < text.length()) {
        i = text.find("{{", i);
        if (i == text.npos) {
          break;
        }
        auto k = i + 2;
        auto j = text.find("}}", k);
        if (j == text.npos) {
          break; // since we did not find a {{ }} sequence we skip out
        }
        auto key = text.substr(k, j - k);
        // TODO: normally we ought to trim the key here
        auto repl = replacements.find(key);
        if (repl == replacements.end()) {
          throw ::std::runtime_error("No replacement for key '" + key + "'");
        }
        text.replace(i, (j + 2 - i), repl->second);
        i += repl->second.length();
      }
      _stream << text;
      return *this;
    }

    CodeStream& CodeStream::insertTemplate(::std::istream &stream,
        const ::std::map<::std::string, ::std::string> &replacements)
    {
      ::std::string line;
      line.reserve(1024);
      while (::std::getline(stream, line)) {
        insertTemplateString(line, replacements);
        newline();
      }
      return *this;
    }
  }
}

