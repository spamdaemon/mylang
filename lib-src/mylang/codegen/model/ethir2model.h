#ifndef FILE_MYLANG_CODEGEN_MODEL_ETHIR2MODEL_H
#define FILE_MYLANG_CODEGEN_MODEL_ETHIR2MODEL_H

#include <mylang/codegen/model/model.h>
#include <mylang/ethir/ethir.h>

namespace mylang {
  namespace codegen {
    namespace model {

      /**
       * Transform an ehtir program into an equivalent model program
       */
      class EthirToModel
      {

      public:
        EthirToModel();

      public:
        virtual ~EthirToModel();

        /**
         * Transform the program.
         * @param node an ethir program
         * @return a model program
         */
      public:
        virtual MProgram transform(mylang::ethir::EProgram node) const;

        /** True if the conversion process should strip comments */
      public:
        bool stripComments;
      };
    }
  }
}
#endif
