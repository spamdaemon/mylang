#include <mylang/codegen/model/ethir2model.h>
#include <mylang/codegen/model/model.h>
#include <mylang/codegen/model/ir/nodes.h>
#include <mylang/codegen/model/types/types.h>
#include <mylang/codegen/model/types/TypeSet.h>
#include <mylang/ethir/ethir.h>
#include <mylang/ethir/transforms/RewriteBuiltins.h>
#include <mylang/ethir/transforms/ToLoops.h>
#include <mylang/ethir/transforms/PruneGlobals.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/ssa/SSAProgram.h>
#include <mylang/ethir/ssa/PruneUnreachableCode.h>
#include <mylang/ethir/types/TypeVisitor.h>
#include <mylang/names/Name.h>
#include <cassert>
#include <memory>
#include <stdexcept>
#include <vector>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace {

        struct Visitor: public mylang::ethir::ir::NodeVisitor,
            public mylang::ethir::types::TypeVisitor
        {
          MNode result;
          types::TypeSet uniqueTypes;

          Visitor(const EthirToModel &config)
              : configuration(config)
          {
          }

          ~Visitor()
          {
          }

          MType toType(const mylang::ethir::EExpression &expr)
          {
            return toType(expr->type);
          }

          ir::Declaration::Scope toScope(::mylang::ethir::ir::Declaration::Scope scope)
          {
            switch (scope) {
            case mylang::ethir::ir::Declaration::GLOBAL_SCOPE:
              return ir::Declaration::GLOBAL_SCOPE;
            case mylang::ethir::ir::Declaration::PROCESS_SCOPE:
              return ir::Declaration::PROCESS_SCOPE;
            case mylang::ethir::ir::Declaration::FUNCTION_SCOPE:
              return ir::Declaration::FUNCTION_SCOPE;
            }
            throw ::std::runtime_error("Unknown scope");
          }

          MType toType(const mylang::ethir::EType &type)
          {
            auto i = convertedTypes.find(type);
            if (i != convertedTypes.end()) {
              return i->second;
            }
            resultType = nullptr;
            type->accept(*this);
            if (!resultType) {
              throw ::std::runtime_error("Failed to map type " + type->toString());
            }
            resultType = uniqueTypes.add(resultType);
            convertedTypes[type] = resultType;
            return resultType;
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::CharType>&) override
          {
            resultType = types::CharType::create();
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::FunctionType> &type)
          override
          {
            auto retTy = toType(type->returnType);
            types::FunctionType::Parameters params;
            for (auto p : type->parameters) {
              params.push_back(toType(p));
            }
            resultType = types::FunctionType::get(retTy, params);
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::BuilderType> &type) override
          {
            auto ty = toType(type->product)->self<types::BuilderType::ProductType>();
            resultType = types::BuilderType::get(ty);
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::GenericType>&) override
          {
            resultType = types::GenericType::create();
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::RealType>&) override
          {
            resultType = types::RealType::create();
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::OutputType> &type) override
          {
            auto ty = toType(type->element);
            resultType = types::OutputType::get(ty);
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::BooleanType>&) override
          {
            resultType = types::BooleanType::create();
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::IntegerType> &ty) override
          {
            resultType = types::IntegerType::create(ty->range);
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::InputType> &type) override
          {
            auto ty = toType(type->element);
            resultType = types::InputType::get(ty);
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::BitType>&) override
          {
            resultType = types::BitType::create();
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::ByteType>&) override
          {
            resultType = types::ByteType::create();
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::OptType> &type) override
          {
            auto ty = toType(type->element);
            resultType = types::OptType::get(ty);
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::NamedType> &type) override
          {
            struct Holder
            {
              MType type;
            };

            struct Resolver
            {
              Resolver()
                  : holder(::std::make_shared<Holder>())
              {
              }
              MType operator()() const
              {
                return holder->type;
              }
              ::std::shared_ptr<Holder> holder;
            };

            Resolver resolver;
            ::std::function<MType()> fn(resolver);

            auto namedType = types::NamedType::get(type->name().source(), resolver);
            // prevent recursion when converting the derived type
            convertedTypes[type] = namedType;
            resolver.holder->type = toType(type->resolve());
            resultType = namedType;
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::ArrayType> &type) override
          {
            auto ty = toType(type->element);
            resultType = types::ArrayType::get(ty, type->minSize, type->maxSize);
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::StructType> &type) override
          {
            ::std::vector<types::StructType::Member> members;
            for (auto m : type->members) {
              auto ty = toType(m.type);
              members.emplace_back(m.name, ty);
            }
            resultType = types::StructType::get(members);
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::VoidType>&) override
          {
            resultType = types::VoidType::create();
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::UnionType> &type) override
          {
            auto discriminant = types::UnionType::Discriminant(type->discriminant.name,
                toType(type->discriminant.type));
            ::std::vector<types::UnionType::Member> members;
            for (auto m : type->members) {
              auto ty = toType(m.type);
              ir::Literal::LiteralPtr lit = toExpression(m.discriminant);
              members.emplace_back(lit, m.name, ty);
            }
            resultType = types::UnionType::get(discriminant, members);
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::StringType>&) override
          {
            resultType = types::StringType::create();
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::TupleType> &type) override
          {
            ::std::vector<MType> types;
            for (auto ty : type->types) {
              types.push_back(toType(ty));
            }
            resultType = types::TupleType::get(types);
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::MutableType> &type)
          override
          {
            auto ty = toType(type->element);
            resultType = types::MutableType::get(ty);
          }

          void visit(const std::shared_ptr<const mylang::ethir::types::ProcessType> &type)
          override
          {
            types::ProcessType::Ports ports;
            for (auto p : type->ports) {
              ports[p.first] = toType(p.second)->self<types::InputOutputType>();
            }
            resultType = types::ProcessType::get(ports);
          }

          types::Type::Ptr resultType;

          ::std::map<mylang::ethir::types::Type::Ptr, types::Type::Ptr> convertedTypes;

          MStatement convertStatement(const mylang::ethir::ENode &node)
          {
            result = nullptr;
            if (node) {
              node->accept(*this);
            }
            auto res = result;
            result = nullptr;
            if (res) {
              return res->self<ir::Statement>();
            } else {
              return nullptr;
            }
          }

          MExpression convertExpression(const mylang::ethir::ENode &node)
          {
            result = nullptr;
            if (!node) {
              return nullptr;
            }
            node->accept(*this);
            auto res = result;
            result = nullptr;
            return res->self<ir::Expression>();
          }

          ir::Parameter::ParameterPtr convertParameter(const mylang::ethir::ENode &node)
          {
            result = nullptr;
            if (node) {
              node->accept(*this);
            }
            auto res = result;
            result = nullptr;
            return res->self<ir::Parameter>();
          }

          MLiteral toExpression(const mylang::ethir::ELiteral &literal)
          {
            auto res = convertExpression(literal);
            if (res) {
              return res->self<ir::Literal>();
            } else {
              return nullptr;
            }
          }

          MVariable toExpression(const mylang::ethir::EVariable &var)
          {
            if (var) {
              return ir::Variable::create(toScope(var->scope), toType(var->type),
                  var->name.source());
            }
            return nullptr;
          }

          ::std::vector<MVariable> toExpressions(
              const ::std::vector<mylang::ethir::EVariable> &exprs)
          {
            ::std::vector<MVariable> res;
            for (auto expr : exprs) {
              res.push_back(toExpression(expr));
            }
            return res;
          }

          template<class MTYPE, class ETYPE>
          ::std::vector<::std::shared_ptr<const MTYPE>> toExpressions(
              const ::std::vector<::std::shared_ptr<const ETYPE>> &exprs)
          {
            ::std::vector<::std::shared_ptr<const MTYPE>> res;
            for (auto expr : exprs) {
              res.push_back(toExpression(expr)->template self<MTYPE>());
            }
            return res;
          }

          ir::StatementBlock::StatementBlockPtr toStatement(const mylang::ethir::EStatement &stmt)
          {
            push();
            mylang::ethir::EStatement node = stmt;
            while (node) {
              auto n = convertStatement(node);
              if (n) {
                append(n);
              }
              node = node->next;
            }

            return popBlock();
          }

          /**
           * Push a new context onto the stack.
           */
          void push()
          {
            stack.push_back(::std::vector<MStatement>());
          }

          /**
           * Append a statement to the context on top of the stack.
           * @param stmt a statement
           * @return nullptr
           */
          MStatement append(ir::Statement::StatementPtr stmt)
          {
            if (stmt) {
              stack.back().push_back(stmt);
            }
            return nullptr;
          }

          /**
           * Pop the current context off the stack and return its statements
           * in a statement list.
           */
          ir::StatementBlock::StatementBlockPtr popBlock()
          {
            auto res = ir::StatementBlock::create(stack.back());
            stack.pop_back();
            return res;
          }

          /** The stack statement lists and blocks */
          ::std::vector<::std::vector<MStatement>> stack;

          void visitPhi(mylang::ethir::ir::Phi::PhiPtr) override
          {
            throw ::std::runtime_error("Internal error: unexpected phi function");
          }

          void visitToArray(mylang::ethir::ir::ToArray::ToArrayPtr) override
          {
            throw ::std::runtime_error("Internal error: unexpected ToArray function");
          }

          void visitLetExpression(mylang::ethir::ir::LetExpression::LetExpressionPtr expr)
          override
          {
            auto val = convertExpression(expr->value);
            auto var = toExpression(expr->variable);
            auto decl = ir::ValueDecl::create(var, val);
            append(decl);
            result = convertExpression(expr->result);
          }

          void visitNewUnion(mylang::ethir::ir::NewUnion::NewUnionPtr expr) override
          {
            auto ty = toType(expr);
            auto value = toExpression(expr->value);
            result = ir::NewUnion::create(ty, expr->member, value);
          }

          void visitNewProcess(mylang::ethir::ir::NewProcess::NewProcessPtr expr) override
          {
            auto ty = toType(expr);
            auto args = toExpressions(expr->arguments);
            result = ir::NewProcess::create(ty, expr->processName.source(),
                expr->constructorName.source(), args);
          }

          void visitInputPortDecl(mylang::ethir::ir::InputPortDecl::InputPortDeclPtr stmt)
          override
          {
            auto var = toExpression(stmt->variable);
            result = ir::InputPortDecl::create(var, stmt->publicName);
          }

          void visitStatementBlock(mylang::ethir::ir::StatementBlock::StatementBlockPtr stmt)
          override
          {
            result = toStatement(stmt->statements);
          }

          void visitVariableUpdate(mylang::ethir::ir::VariableUpdate::VariableUpdatePtr stmt)
          override
          {
            auto target = toExpression(stmt->target);
            auto value = toExpression(stmt->value);
            result = ir::VariableUpdate::create(target, value);
          }

          void visitProcessVariableUpdate(
              mylang::ethir::ir::ProcessVariableUpdate::ProcessVariableUpdatePtr stmt)
              override
          {
            auto target = toExpression(stmt->target);
            auto value = toExpression(stmt->value);
            result = ir::VariableUpdate::create(target, value);
          }

          void visitIfStatement(mylang::ethir::ir::IfStatement::IfStatementPtr stmt) override
          {
            auto cond = toExpression(stmt->condition);
            auto iftrue = toStatement(stmt->iftrue);
            auto iffalse = toStatement(stmt->iffalse);
            result = ir::IfStatement::create(cond, iftrue, iffalse);
          }

          void visitTryCatch(mylang::ethir::ir::TryCatch::TryCatchPtr stmt) override
          {
            auto tryBlock = toStatement(stmt->tryBody);
            auto catchBlock = toStatement(stmt->catchBody);
            result = ir::TryCatch::create(tryBlock, catchBlock);
          }

          void visitOpaqueExpression(mylang::ethir::ir::OpaqueExpression::OpaqueExpressionPtr expr)
          override
          {
            result = convertExpression(expr->restore());
          }

          void visitOperatorExpression(
              mylang::ethir::ir::OperatorExpression::OperatorExpressionPtr expr) override
          {
            auto ty = toType(expr);
            auto args = toExpressions(expr->arguments);
            ir::OperatorExpression::Operator op;
            switch (expr->name) {
            case mylang::ethir::ir::OperatorExpression::OP_NEW:
              op = ir::OperatorExpression::OP_NEW;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_IS_LOGGABLE:
              op = ir::OperatorExpression::OP_IS_LOGGABLE;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_CALL:
              op = ir::OperatorExpression::OP_CALL;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_UNCHECKED_CAST:
              op = ir::OperatorExpression::OP_UNCHECKED_CAST;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_CHECKED_CAST:
              op = ir::OperatorExpression::OP_CHECKED_CAST;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_BASETYPE_CAST:
              op = ir::OperatorExpression::OP_BASETYPE_CAST;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_ROOTTYPE_CAST:
              op = ir::OperatorExpression::OP_ROOTTYPE_CAST;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_BYTE_TO_UINT8:
              op = ir::OperatorExpression::OP_BYTE_TO_UINT8;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_BYTE_TO_SINT8:
              op = ir::OperatorExpression::OP_BYTE_TO_SINT8;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_NEG:
              op = ir::OperatorExpression::OP_NEG;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_ADD:
              op = ir::OperatorExpression::OP_ADD;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_SUB:
              op = ir::OperatorExpression::OP_SUB;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_MUL:
              op = ir::OperatorExpression::OP_MUL;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_DIV:
              op = ir::OperatorExpression::OP_DIV;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_MOD:
              op = ir::OperatorExpression::OP_MOD;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_REM:
              op = ir::OperatorExpression::OP_REM;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_CLAMP:
              op = ir::OperatorExpression::OP_CLAMP;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_WRAP:
              op = ir::OperatorExpression::OP_WRAP;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_INDEX:
              op = ir::OperatorExpression::OP_INDEX;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_SUBRANGE:
              op = ir::OperatorExpression::OP_SUBRANGE;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_SIZE:
              op = ir::OperatorExpression::OP_SIZE;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_CONCATENATE:
              op = ir::OperatorExpression::OP_CONCATENATE;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_HEAD:
              op = ir::OperatorExpression::OP_HEAD;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_TAIL:
              op = ir::OperatorExpression::OP_TAIL;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_EQ:
              op = ir::OperatorExpression::OP_EQ;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_NEQ:
              op = ir::OperatorExpression::OP_NEQ;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_LT:
              op = ir::OperatorExpression::OP_LT;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_LTE:
              op = ir::OperatorExpression::OP_LTE;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_AND:
              op = ir::OperatorExpression::OP_AND;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_OR:
              op = ir::OperatorExpression::OP_OR;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_XOR:
              op = ir::OperatorExpression::OP_XOR;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_NOT:
              op = ir::OperatorExpression::OP_NOT;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_GET:
              op = ir::OperatorExpression::OP_GET;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_IS_PRESENT:
              op = ir::OperatorExpression::OP_IS_PRESENT;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_IS_INPUT_NEW:
              op = ir::OperatorExpression::OP_IS_INPUT_NEW;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_IS_INPUT_CLOSED:
              op = ir::OperatorExpression::OP_IS_INPUT_CLOSED;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_IS_OUTPUT_CLOSED:
              op = ir::OperatorExpression::OP_IS_OUTPUT_CLOSED;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_IS_PORT_READABLE:
              op = ir::OperatorExpression::OP_IS_PORT_READABLE;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_IS_PORT_WRITABLE:
              op = ir::OperatorExpression::OP_IS_PORT_WRITABLE;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_FROM_BITS:
              op = ir::OperatorExpression::OP_FROM_BITS;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_TO_BITS:
              op = ir::OperatorExpression::OP_TO_BITS;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_FROM_BYTES:
              op = ir::OperatorExpression::OP_FROM_BYTES;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_TO_BYTES:
              op = ir::OperatorExpression::OP_TO_BYTES;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_STRING_TO_CHARS:
              op = ir::OperatorExpression::OP_STRING_TO_CHARS;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_TO_STRING:
              op = ir::OperatorExpression::OP_TO_STRING;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_READ_PORT:
              op = ir::OperatorExpression::OP_READ_PORT;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_LOG:
              op = ir::OperatorExpression::OP_LOG;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_SET:
              op = ir::OperatorExpression::OP_SET;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_BLOCK_READ:
              op = ir::OperatorExpression::OP_BLOCK_READ;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_BLOCK_WRITE:
              op = ir::OperatorExpression::OP_BLOCK_WRITE;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_CLEAR_PORT:
              op = ir::OperatorExpression::OP_CLEAR_PORT;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_CLOSE_PORT:
              op = ir::OperatorExpression::OP_CLOSE_PORT;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_WAIT_PORT:
              op = ir::OperatorExpression::OP_WAIT_PORT;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_WRITE_PORT:
              op = ir::OperatorExpression::OP_WRITE_PORT;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_APPEND:
              op = ir::OperatorExpression::OP_APPEND;
              break;
            case mylang::ethir::ir::OperatorExpression::OP_BUILD:
              op = ir::OperatorExpression::OP_BUILD;
              break;
            default:
              throw ::std::runtime_error("Unsupported operator " + ::std::to_string(expr->name));
            }
            result = ir::OperatorExpression::create(ty, op, args);
          }

          void visitVariableDecl(mylang::ethir::ir::VariableDecl::VariableDeclPtr stmt) override
          {
            auto var = toExpression(stmt->variable);
            auto init = toExpression(stmt->value);
            result = ir::VariableDecl::create(var, init);
          }

          void visitCommentStatement(mylang::ethir::ir::CommentStatement::CommentStatementPtr stmt)
          override
          {
            if (configuration.stripComments) {
              result = nullptr;
            } else {
              result = ir::CommentStatement::create(stmt->comment);
            }
          }

          void visitNoStatement(mylang::ethir::ir::NoStatement::NoStatementPtr)
          override
          {
            result = nullptr;
          }

          void visitProcessDecl(mylang::ethir::ir::ProcessDecl::ProcessDeclPtr stmt) override
          {
            ir::ProcessDecl::Ports ports;
            ir::ProcessDecl::Blocks blocks;
            ir::ProcessDecl::Constructors constructors;
            ir::ProcessDecl::Declarations variables;

            for (auto p : stmt->publicPorts) {
              auto s = convertStatement(p);
              ports.push_back(s->self<ir::PortDecl>());
            }
            for (auto p : stmt->variables) {
              auto s = convertStatement(p);
              variables.push_back(s->self<ir::Declaration>());
            }
            for (auto p : stmt->constructors) {
              auto s = convertStatement(p);
              constructors.push_back(s->self<ir::ProcessConstructor>());
            }
            for (auto p : stmt->blocks) {
              auto s = convertStatement(p);
              blocks.push_back(s->self<ir::ProcessBlock>());
            }
            if (constructors.empty()) {
              // add a default constructor!
              constructors.push_back(ir::ProcessConstructor::createDefaultConstructor());
            }

            auto ty = toType(stmt->type)->self<types::ProcessType>();
            auto scope = toScope(stmt->scope);
            result = ir::ProcessDecl::create(scope, stmt->name.source(), ty, ports, variables,
                constructors, blocks);
          }

          void visitParameter(mylang::ethir::ir::Parameter::ParameterPtr param) override
          {
            auto ty = toType(param->variable->type);
            result = ir::Parameter::create(param->variable->name.source(), ty);
          }

          void visitAbortStatement(mylang::ethir::ir::AbortStatement::AbortStatementPtr stmt)
          override
          {
            auto message = toExpression(stmt->value);
            result = ir::AbortStatement::create(message);
          }

          void visitReturnStatement(mylang::ethir::ir::ReturnStatement::ReturnStatementPtr stmt)
          override
          {
            auto value = toExpression(stmt->value);
            result = ir::ReturnStatement::create(value);
          }

          void visitYieldStatement(mylang::ethir::ir::YieldStatement::YieldStatementPtr)
          override
          {
            throw ::std::runtime_error("Internal error: unexpected YieldStatement");
          }

          void visitSkipStatement(mylang::ethir::ir::SkipStatement::SkipStatementPtr)
          override
          {
            throw ::std::runtime_error("Internal error: unexpected SkipStatement");
          }

          void visitIterateArrayStep(mylang::ethir::ir::IterateArrayStep::IterateArrayStepPtr)
          override
          {
            throw ::std::runtime_error("Internal error: unexpected IterateArray step");
          }

          void visitSequenceStep(mylang::ethir::ir::SequenceStep::SequenceStepPtr)
          override
          {
            throw ::std::runtime_error("Internal error: unexpected Sequence step");
          }

          void visitSingleValueStep(mylang::ethir::ir::SingleValueStep::SingleValueStepPtr)
          override
          {
            throw ::std::runtime_error("Internal error: unexpected SingleValue step");
          }

          void visitLoop(mylang::ethir::ir::Loop::LoopPtr)
          override
          {
            throw ::std::runtime_error("Internal error: unexpected Loop");
          }

          void visitThrowStatement(mylang::ethir::ir::ThrowStatement::ThrowStatementPtr stmt)
          override
          {
            auto value = toExpression(stmt->value);
            result = ir::ThrowStatement::create(value);
          }

          void visitBreakStatement(mylang::ethir::ir::BreakStatement::BreakStatementPtr stmt)
          override
          {
            result = ir::BreakStatement::create(stmt->scope.source());
          }

          void visitContinueStatement(
              mylang::ethir::ir::ContinueStatement::ContinueStatementPtr stmt)
              override
          {
            result = ir::ContinueStatement::create(stmt->scope.source());
          }

          void visitLambdaExpression(mylang::ethir::ir::LambdaExpression::LambdaExpressionPtr expr)
          override
          {
            ir::LambdaExpression::Parameters params;
            for (auto p : expr->parameters) {
              params.push_back(convertParameter(p)->self<ir::Parameter>());
            }
            auto fnTy = toType(expr)->self<types::FunctionType>();
            auto body = toStatement(expr->body);
            result = ir::LambdaExpression::create(fnTy, params, body);
          }

          void visitGetDiscriminant(mylang::ethir::ir::GetDiscriminant::GetDiscriminantPtr expr)
          override
          {
            auto ty = toType(expr);
            auto obj = toExpression(expr->object);
            result = ir::GetDiscriminant::create(ty, obj);
          }

          void visitGetMember(mylang::ethir::ir::GetMember::GetMemberPtr expr) override
          {
            auto ty = toType(expr);
            auto obj = toExpression(expr->object);
            result = ir::GetMember::create(ty, obj, expr->name);
          }

          void visitVariable(mylang::ethir::ir::Variable::VariablePtr expr) override
          {
            result = toExpression(expr);
          }

          void visitOwnConstructorCall(
              mylang::ethir::ir::OwnConstructorCall::OwnConstructorCallPtr stmt) override
          {
            auto block = toStatement(stmt->pre);
            auto args = toExpressions(stmt->arguments);
            result = ir::OwnConstructorCall::create(block->statements, args);
          }

          void visitNoValue(mylang::ethir::ir::NoValue::NoValuePtr expr) override
          {
            auto ty = toType(expr);
            result = ir::NoValue::create(ty);
          }

          void visitLiteralString(mylang::ethir::ir::LiteralString::LiteralStringPtr expr)
          override
          {
            auto ty = toType(expr)->self<types::StringType>();
            result = ir::LiteralString::create(ty, expr->value);
          }
          void visitLiteralBit(mylang::ethir::ir::LiteralBit::LiteralBitPtr expr) override
          {
            auto ty = toType(expr)->self<types::BitType>();
            result = ir::LiteralBit::create(ty, expr->value);
          }
          void visitLiteralChar(mylang::ethir::ir::LiteralChar::LiteralCharPtr expr) override
          {
            auto ty = toType(expr)->self<types::CharType>();
            result = ir::LiteralChar::create(ty, expr->value);
          }

          void visitLiteralReal(mylang::ethir::ir::LiteralReal::LiteralRealPtr expr) override
          {
            auto ty = toType(expr)->self<types::RealType>();
            result = ir::LiteralReal::create(ty, expr->value);
          }

          void visitLiteralInteger(mylang::ethir::ir::LiteralInteger::LiteralIntegerPtr expr)
          override
          {
            auto ty = toType(expr)->self<types::IntegerType>();
            result = ir::LiteralInteger::create(ty, expr->value);
          }

          void visitLiteralBoolean(mylang::ethir::ir::LiteralBoolean::LiteralBooleanPtr expr)
          override
          {
            auto ty = toType(expr)->self<types::BooleanType>();
            result = ir::LiteralBoolean::create(ty, expr->value);
          }
          void visitLiteralByte(mylang::ethir::ir::LiteralByte::LiteralBytePtr expr) override
          {
            auto ty = toType(expr)->self<types::ByteType>();
            result = ir::LiteralByte::create(ty, expr->value);
          }

          virtual void visitLiteralArray(mylang::ethir::ir::LiteralArray::LiteralArrayPtr expr)
          override
          {
            auto ty = toType(expr)->self<types::ArrayType>();
            ir::LiteralArray::Elements exprs;
            for (auto e : expr->elements) {
              exprs.push_back(toExpression(e));
            }
            result = ir::LiteralArray::create(ty, exprs);
          }

          virtual void visitLiteralNamedType(
              mylang::ethir::ir::LiteralNamedType::LiteralNamedTypePtr expr) override
          {
            auto ty = toType(expr)->self<types::NamedType>();
            result = ir::LiteralNamedType::create(ty, toExpression(expr->value));
          }

          virtual void visitLiteralOptional(
              mylang::ethir::ir::LiteralOptional::LiteralOptionalPtr expr) override
          {
            auto ty = toType(expr)->self<types::OptType>();
            result = ir::LiteralOptional::create(ty, toExpression(expr->value));
          }

          virtual void visitLiteralStruct(mylang::ethir::ir::LiteralStruct::LiteralStructPtr expr)
          override
          {
            auto ty = toType(expr)->self<types::StructType>();
            ir::LiteralStruct::Members exprs;
            for (auto e : expr->members) {
              exprs.push_back(toExpression(e));
            }
            result = ir::LiteralStruct::create(ty, exprs);
          }

          virtual void visitLiteralTuple(mylang::ethir::ir::LiteralTuple::LiteralTuplePtr expr)
          override
          {
            auto ty = toType(expr)->self<types::TupleType>();
            ir::LiteralTuple::Elements exprs;
            for (auto e : expr->elements) {
              exprs.push_back(toExpression(e));
            }
            result = ir::LiteralTuple::create(ty, exprs);
          }

          virtual void visitLiteralUnion(mylang::ethir::ir::LiteralUnion::LiteralUnionPtr expr)
          override
          {
            auto ty = toType(expr)->self<types::UnionType>();
            result = ir::LiteralUnion::create(ty, expr->member, toExpression(expr->value));
          }

          void visitProgram(mylang::ethir::ir::Program::ProgramPtr node) override
          {
            ir::Program::Globals globals;
            ir::Program::Processes processes;
            ir::Program::NamedTypes types;
            for (auto g : node->globals) {
              globals.push_back(convertStatement(g)->self<ir::GlobalValue>());
            }
            for (auto p : node->processes) {
              processes.push_back(convertStatement(p)->self<ir::ProcessDecl>());
            }
            for (auto t : node->types) {
              types.push_back(toType(t)->self<types::NamedType>());
            }
            result = ir::Program::create(globals, processes, types);
          }

          void visitLoopStatement(mylang::ethir::ir::LoopStatement::LoopStatementPtr stmt)
          override
          {
            auto body = toStatement(stmt->body);
            result = ir::LoopStatement::create(stmt->name.source(), body);
          }

          void visitGlobalValue(mylang::ethir::ir::GlobalValue::GlobalValuePtr stmt) override
          {
            auto init = convertExpression(stmt->value);
            ::std::optional<::mylang::names::FQN> fqn;
            if (stmt->kind == mylang::ethir::ir::GlobalValue::EXPORTED) {
              fqn = ::mylang::names::FQN::parseFQN(stmt->name.source()->fullName());
            }
            result = ir::GlobalValue::create(stmt->name.source(), fqn, init);
          }

          void visitProcessBlock(mylang::ethir::ir::ProcessBlock::ProcessBlockPtr stmt) override
          {
            auto body = toStatement(stmt->body);
            result = ir::ProcessBlock::create(stmt->name, body);
          }

          void visitProcessConstructor(
              mylang::ethir::ir::ProcessConstructor::ProcessConstructorPtr stmt) override
          {
            ir::ProcessConstructor::Parameters params;
            for (auto p : stmt->parameters) {
              params.push_back(convertParameter(p)->self<ir::Parameter>());
            }
            auto body = toStatement(stmt->body);

            ir::OwnConstructorCall::OwnConstructorCallPtr self;
            if (stmt->callConstructor) {
              auto blk = toStatement(stmt->callConstructor);
              if (blk->statements.size() == 1) {
                self = blk->statements.at(0)->self<ir::OwnConstructorCall>();
              }
              if (!self) {
                throw ::std::runtime_error("Failed to convert call to own constructor");
              }
            }

            result = ir::ProcessConstructor::create(params, self, body);
          }

          void visitOutputPortDecl(mylang::ethir::ir::OutputPortDecl::OutputPortDeclPtr stmt)
          override
          {
            auto var = toExpression(stmt->variable);
            result = ir::OutputPortDecl::create(var, stmt->publicName);
          }

          void visitProcessVariable(mylang::ethir::ir::ProcessVariable::ProcessVariablePtr stmt)
          override
          {
            auto var = toExpression(stmt->variable);
            auto init = toExpression(stmt->value);
            result = ir::VariableDecl::create(var, init);
          }
          void visitProcessValue(mylang::ethir::ir::ProcessValue::ProcessValuePtr stmt) override
          {
            auto var = toExpression(stmt->variable);
            auto init = convertExpression(stmt->value);
            result = ir::ValueDecl::create(var, init);
          }

          void visitValueDecl(mylang::ethir::ir::ValueDecl::ValueDeclPtr stmt) override
          {
            auto var = toExpression(stmt->variable);
            auto init = convertExpression(stmt->value);
            result = ir::ValueDecl::create(var, init);
          }

          void visitForeachStatement(mylang::ethir::ir::ForeachStatement::ForeachStatementPtr stmt)
          override
          {
            auto data = toExpression(stmt->data);
            auto var = toExpression(stmt->variable);
            auto body = toStatement(stmt->body);

            result = ir::ForeachStatement::create(stmt->name.source(), var, data, body);
          }

          const EthirToModel &configuration;
        };

      }

      EthirToModel::EthirToModel()
          : stripComments(false)
      {
      }
      EthirToModel::~EthirToModel()
      {
      }

      MProgram EthirToModel::transform(mylang::ethir::EProgram prog) const
      {
        prog = mylang::ethir::transforms::PruneGlobals::transform(prog);

        // apply the replacement transform to get rid of unsupported expressions and functions
        mylang::ethir::transforms::RewriteBuiltins repl;
        prog = repl.apply(prog)->self<mylang::ethir::ir::Program>();

        // replace create loops for complex expressions
        mylang::ethir::transforms::ToLoops mkLoops;
        prog = mkLoops.apply(prog)->self<mylang::ethir::ir::Program>();

        // prune unreachable code that would trigger javac errors
        // needs to be done in SSA form
        auto ssaForm = ethir::ssa::SSAProgram::createSSA(prog);
        auto pruneUnreachable = ::std::make_unique<mylang::ethir::ssa::PruneUnreachableCode>();
        while (pruneUnreachable->transformProgram(*ssaForm)) {
          // keep transforming
        }
        auto node = ssaForm->getNonSSAForm();

        Visitor v(*this);
        node->accept(v);
        auto program = v.result->self<ir::Program>();
        return program;
      }
    }
  }
}
