#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_VOIDTYPE_H
#define CLASS_MYLANG_CODEGEN_MODEL_TYPES_VOIDTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_PRIMITIVETYPE_H
#include <mylang/codegen/model/types/PrimitiveType.h>
#endif
#include <memory>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        /** A type representing an error */
        class VoidType: public PrimitiveType
        {

        private:
          VoidType();

        public:
          ~VoidType();

          /** Two primitives are the same if their type ids are the same  */
        public:
          void accept(TypeVisitor &v) const;

          bool isSameType(const Type &other) const final;

          ::std::string toString() const final;

        public:
          static ::std::shared_ptr<const VoidType> create();
        };
      }
    }
  }
}
#endif
