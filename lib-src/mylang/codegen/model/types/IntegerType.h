#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_INTEGERTYPE_H
#define CLASS_MYLANG_CODEGEN_MODEL_TYPES_INTEGERTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_NUMERICTYPE_H
#include <mylang/codegen/model/types/NumericType.h>
#endif

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_INTERVAL_H
#include <mylang/Interval.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        /** The baseclass for all types */
        class IntegerType: public NumericType
        {
        public:
          using Range = Interval;

        protected:
          IntegerType(Range range);

        public:
          ~IntegerType();

          /**
           * Get the bit type
           * @return a primitive type for a INTEGER
           */
        public:
          static ::std::shared_ptr<const IntegerType> create(::std::optional<Integer> min,
              ::std::optional<Integer> max);

          /**
           * Get the bit type
           * @return a primitive type for a INTEGER
           */
        public:
          static ::std::shared_ptr<const IntegerType> create(const Range &r);

        public:
          void accept(TypeVisitor &v) const override final;

          bool isSameType(const Type &other) const final override;
          bool canSafeCastFrom(const Type &t) const final override;

          bool canCastFrom(const Type &t) const final override;

          ::std::string toString() const final override;

          /** The range of values for this type */
        public:
          const Interval range;
        };
      }
    }
  }
}
#endif
