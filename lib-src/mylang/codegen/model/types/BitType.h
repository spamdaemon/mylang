#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_BITTYPE_H
#define CLASS_MYLANG_CODEGEN_MODEL_TYPES_BITTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_PRIMITIVETYPE_H
#include <mylang/codegen/model/types/PrimitiveType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        /** The baseclass for all types */
        class BitType: public PrimitiveType
        {

        private:
          BitType();

        public:
          ~BitType();

          /**
           * Get the bit type
           * @return a primitive type for a INTEGER
           */
        public:
          static ::std::shared_ptr<const BitType> create();

        public:
          void accept(TypeVisitor &v) const;

          bool isSameType(const Type &other) const final;

          ::std::string toString() const final;
        };
      }
    }
  }
}
#endif
