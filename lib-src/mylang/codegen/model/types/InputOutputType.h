#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_INPUTOUTPUTTYPE_H
#define CLASS_MYLANG_CODEGEN_MODEL_TYPES_INPUTOUTPUTTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_TYPE_H
#include <mylang/codegen/model/types/Type.h>
#endif
#include <memory>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        /** A type representing an error */
        class InputOutputType: public Type
        {
        protected:
          InputOutputType(::std::shared_ptr<const Type> xelement);

        public:
          ~InputOutputType();

          /** Get the peer port */
        public:
          virtual ::std::shared_ptr<const InputOutputType> getPeer() const = 0;

          /** The element type */
        public:
          const ::std::shared_ptr<const Type> element;

        };
      }
    }
  }
}
#endif
