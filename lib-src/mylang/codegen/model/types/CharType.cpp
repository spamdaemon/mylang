#include <mylang/codegen/model/types/CharType.h>
#include <mylang/codegen/model/types/TypeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        CharType::CharType()
        {
        }

        CharType::~CharType()
        {
        }

        void CharType::accept(TypeVisitor &v) const
        {
          v.visit(::std::dynamic_pointer_cast<const CharType>(self()));
        }

        bool CharType::isSameType(const Type &other) const
        {
          return dynamic_cast<const CharType*>(&other) != nullptr;
        }

        ::std::string CharType::toString() const
        {
          return "char";
        }

        ::std::shared_ptr<const CharType> CharType::create()
        {
          struct Impl: public CharType
          {
            ~Impl()
            {
            }
          };

          return ::std::make_shared<Impl>();
        }

      }
    }
  }
}
