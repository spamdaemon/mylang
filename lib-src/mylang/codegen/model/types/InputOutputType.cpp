#include <mylang/codegen/model/types/InputOutputType.h>
#include <stdexcept>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        InputOutputType::InputOutputType(::std::shared_ptr<const Type> xelement)
            : element(xelement)
        {
          if (!xelement) {
            throw ::std::invalid_argument("Null element");
          }
        }

        InputOutputType::~InputOutputType()
        {
        }

      }
    }
  }
}
