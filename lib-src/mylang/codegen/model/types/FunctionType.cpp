#include <mylang/codegen/model/types/FunctionType.h>
#include <mylang/codegen/model/types/TypeVisitor.h>
#include <mylang/codegen/model/types/TypeSet.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        FunctionType::FunctionType(const Type::Ptr &ret, const Parameters &xparameters)
            : returnType(ret), parameters(xparameters)
        {
        }

        FunctionType::~FunctionType()
        {
        }

        bool FunctionType::canSafeCastFrom(const Type &t) const
        {
          auto that = dynamic_cast<const FunctionType*>(&t);
          bool ok = false;
          if (that && that->parameters.size() == parameters.size()
              && returnType->canSafeCastFrom(*that->returnType)) {

            ok = true;
            // for the parameters, we need to invert the test and ensure that we can
            // cast the parameters to the of t!
            for (size_t i = 0; ok && i < parameters.size(); ++i) {
              ok = that->parameters.at(i)->canSafeCastFrom(*parameters.at(i));
            }
          }
          return ok || Type::canSafeCastFrom(t);
        }

        bool FunctionType::canCastFrom(const Type &t) const
        {
          // this kind of cast will most likely require a lambda function
          // that adapts the parameters
          auto that = dynamic_cast<const FunctionType*>(&t);
          bool ok = false;
          if (that && that->parameters.size() == parameters.size()
              && returnType->canCastFrom(*that->returnType)) {

            ok = true;
            // for the parameters, we need to invert the test and ensure that we can
            // cast the parameters to the of t!
            for (size_t i = 0; ok && i < parameters.size(); ++i) {
              ok = that->parameters.at(i)->canCastFrom(*parameters.at(i));
            }
          }
          return ok || Type::canCastFrom(t);
        }

        Type::Ptr FunctionType::normalize(TypeSet &ts) const
        {
          auto ret = returnType->normalize(ts);
          Parameters parms;
          for (auto v : parameters) {
            parms.push_back(v->normalize(ts));
          }
          return ts.add(get(ret, parms));
        }

        void FunctionType::accept(TypeVisitor &v) const
        {
          v.visit(::std::dynamic_pointer_cast<const FunctionType>(self()));
        }

        bool FunctionType::isSameType(const Type &other) const
        {
          auto t = dynamic_cast<const FunctionType*>(&other);
          if (!t) {
            return false;
          }

          if (t->parameters.size() != parameters.size()
              || !t->returnType->isSameType(*returnType)) {
            return false;
          }
          for (size_t i = 0; i < parameters.size(); ++i) {
            if (!t->parameters[i]->isSameType(*parameters[i])) {
              return false;
            }
          }
          return true;
        }

        ::std::string FunctionType::toString() const
        {
          ::std::string s;
          s += '(';
          for (auto &p : parameters) {
            if (s.size() > 1) {
              s += ',';
            }
            s += p->toString();
          }
          s += ')';
          s += ':';
          s += returnType->toString();
          return s;
        }

        bool FunctionType::matchParameters(const Parameters &argv) const
        {
          if (argv.size() != parameters.size()) {
            return false;
          }
          for (size_t i = 0; i < argv.size(); ++i) {
            if (!argv[i]) {
              throw ::std::runtime_error("Wildcard matching is not yet implemented");
            }
            if (argv[i] && !parameters[i]->isSameType(*argv[i])) {
              return false;
            }
          }
          return true;
        }

        ::std::shared_ptr<const FunctionType> FunctionType::get(
            const ::std::shared_ptr<const Type> &ret, const Parameters &xparameters)
        {
          struct Impl: public FunctionType
          {
            Impl(const ::std::shared_ptr<const Type> &ret, const Parameters &args)
                : FunctionType(ret, args)
            {
            }

            ~Impl()
            {
            }
          };
          return ::std::make_shared<Impl>(ret, xparameters);
        }

      }
    }
  }
}
