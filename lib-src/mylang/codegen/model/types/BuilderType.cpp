#include <mylang/codegen/model/types/BuilderType.h>
#include <mylang/codegen/model/types/TypeVisitor.h>
#include <mylang/codegen/model/types/TypeSet.h>
#include <cstddef>
#include <string>
#include <utility>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        BuilderType::BuilderType(ProductTypePtr ty)
            : product(ty)
        {
        }

        BuilderType::~BuilderType()
        {
        }

        Type::Ptr BuilderType::normalize(TypeSet &ts) const
        {
          return BuilderType::get(product->normalize(ts)->self<ProductType>());
        }

        void BuilderType::accept(TypeVisitor &v) const
        {
          v.visit(::std::dynamic_pointer_cast<const BuilderType>(self()));
        }

        ::std::string BuilderType::toString() const
        {
          ::std::string res;
          res += "builder{";
          res += product->toString();
          res += '}';
          return res;
        }

        bool BuilderType::isSameType(const Type &other) const
        {
          auto that = dynamic_cast<const BuilderType*>(&other);
          if (!that) {
            return false;
          }
          return product->isSameType(*that->product);
        }

        bool BuilderType::canSafeCastFrom(const Type &from) const
        {
          return isSameType(from);
        }

        bool BuilderType::canCastFrom(const Type &from) const
        {
          return isSameType(from);
        }

        ::std::shared_ptr<const BuilderType> BuilderType::get(ProductTypePtr xp)
        {
          struct Impl: public BuilderType
          {
            Impl(ProductTypePtr xp)
                : BuilderType(xp)
            {
            }

            ~Impl()
            {
            }
          };
          return ::std::make_shared<const Impl>(xp);
        }
      }
    }
  }
}
