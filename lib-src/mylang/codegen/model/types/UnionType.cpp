#include <mylang/codegen/model/types/UnionType.h>
#include <mylang/codegen/model/types/TypeVisitor.h>
#include <mylang/codegen/model/types/TypeSet.h>
#include <mylang/codegen/model/ir/Literal.h>
#include <stdexcept>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        UnionType::Discriminant::Discriminant(const ::std::string &xname,
            const ::std::shared_ptr<const Type> &xtype)
            : name(xname), type(xtype)
        {
        }

        UnionType::Member::Member(const UnionType::LiteralPtr &xdiscriminant,
            const ::std::string &xname, const Type::Ptr &xtype)
            : discriminant(xdiscriminant), name(xname), type(xtype)
        {

        }

        UnionType::UnionType(const Discriminant &xdiscriminant,
            const ::std::vector<Member> &xmembers)
            : discriminant(xdiscriminant), members(xmembers)
        {
          for (auto sf : members) {
            if (!discriminant.type->canSafeCastFrom(*sf.discriminant->type)) {
              throw ::std::invalid_argument("Discriminant type mismatch");
            }
            if (sf.name.empty()) {
              throw ::std::invalid_argument("Missing union member name");
            }
            if (!sf.type) {
              throw ::std::invalid_argument("Missing union member type");
            }
            if (_indexedMembers.count(sf.name) != 0 || sf.name == discriminant.name) {
              throw ::std::invalid_argument("Duplicate union member " + sf.name);
            }

            _indexedMembers.insert(::std::make_pair(sf.name, sf));
          }
          for (size_t i = 0, sz = members.size(); i < sz; ++i) {
            for (size_t j = i + 1; j < sz; ++j) {
              if ((*members[i].discriminant) == (*members[j].discriminant)) {
                throw ::std::invalid_argument("Duplicate discriminant value");
              }
            }
          }
        }

        UnionType::~UnionType()
        {
        }

        Type::Ptr UnionType::normalize(TypeSet &ts) const
        {
          Discriminant d(discriminant.name, discriminant.type->normalize(ts));
          ::std::vector<Member> tmp;
          for (auto m : members) {
            auto optTy = ::std::dynamic_pointer_cast<const OptType>(m.type->normalize(ts));
            tmp.push_back(Member(m.discriminant, m.name, optTy));
          }
          return ts.add(get(d, tmp));
        }

        void UnionType::accept(TypeVisitor &v) const
        {
          v.visit(::std::dynamic_pointer_cast<const UnionType>(self()));
        }

        bool UnionType::isSameType(const Type &other) const
        {
          auto t = dynamic_cast<const UnionType*>(&other);
          if (!t || t->members.size() != members.size()) {
            return false;
          }
          if (discriminant.name != t->discriminant.name) {
            return false;
          }
          if (!discriminant.type->isSameType(*t->discriminant.type)) {
            return false;
          }
          for (size_t i = 0; i < members.size(); ++i) {
            if (!((*members[i].discriminant) == (*t->members[i].discriminant))) {
              return false;
            }
            if (members[i].name != t->members[i].name) {
              return false;
            }
            if (!members[i].type->isSameType(*t->members[i].type)) {
              return false;
            }
          }

          return true;
        }

        ::std::string UnionType::toString() const
        {
          ::std::string str;
          str += "union(";
          str += discriminant.name;
          str += ":";
          str += discriminant.type->toString();
          str += ") {";
          if (!members.empty()) {
            str += '\n';
          }
          for (auto sf : members) {
            str += sf.name;
            str += ':';
            str += sf.type->toString();
            str += ";\n";
          }
          str += "}";
          return str;
        }

        ::std::shared_ptr<const UnionType> UnionType::get(const Discriminant &xd,
            const ::std::vector<Member> &members)
        {
          struct Impl: public UnionType
          {
            Impl(const Discriminant &xd, const ::std::vector<Member> &t)
                : UnionType(xd, t)
            {
            }

            ~Impl()
            {
            }
          };
          return ::std::make_shared<Impl>(xd, members);
        }

        Type::Ptr UnionType::getMemberType(const ::std::string &field) const
        {
          auto i = _indexedMembers.find(field);
          if (i == _indexedMembers.end()) {
            return nullptr;
          }
          return i->second.type;
        }

        Type::Ptr UnionType::getMemberType(const LiteralPtr &field) const
        {
          for (auto m : members) {
            if (*field == *m.discriminant) {
              return m.type;
            }
          }
          return nullptr;
        }

        size_t UnionType::indexOfMember(const ::std::string &member) const
        {
          for (size_t i = 0; i < members.size(); ++i) {
            if (members[i].name == member) {
              return i;
            }
          }
          throw ::std::runtime_error("No such member : " + member);
        }
      }
    }
  }
}
