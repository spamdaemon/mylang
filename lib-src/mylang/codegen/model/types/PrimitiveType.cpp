#include <mylang/codegen/model/types/PrimitiveType.h>
#include <mylang/codegen/model/types/TypeVisitor.h>
#include <mylang/codegen/model/types/BitType.h>
#include <mylang/codegen/model/types/BooleanType.h>
#include <mylang/codegen/model/types/ByteType.h>
#include <mylang/codegen/model/types/CharType.h>
#include <mylang/codegen/model/types/RealType.h>
#include <mylang/codegen/model/types/IntegerType.h>
#include <mylang/codegen/model/types/StringType.h>
#include <mylang/codegen/model/types/TypeSet.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        PrimitiveType::PrimitiveType()
        {
        }

        PrimitiveType::~PrimitiveType()
        {
        }

        Type::Ptr PrimitiveType::normalize(TypeSet &ts) const
        {
          return ts.add(self());
        }

        ::std::shared_ptr<const PrimitiveType> PrimitiveType::getBit()
        {
          return BitType::create();
        }

        ::std::shared_ptr<const PrimitiveType> PrimitiveType::getBoolean()
        {
          return BooleanType::create();
        }

        ::std::shared_ptr<const PrimitiveType> PrimitiveType::getByte()
        {
          return ByteType::create();
        }

        ::std::shared_ptr<const PrimitiveType> PrimitiveType::getChar()
        {
          return CharType::create();
        }

        ::std::shared_ptr<const PrimitiveType> PrimitiveType::getReal()
        {
          return RealType::create();
        }

        ::std::shared_ptr<const PrimitiveType> PrimitiveType::getInteger(
            const IntegerType::Range &r)
        {
          return IntegerType::create(r);
        }

        ::std::shared_ptr<const PrimitiveType> PrimitiveType::getString()
        {
          return StringType::create();
        }

        ::std::shared_ptr<const PrimitiveType> PrimitiveType::getVoid()
        {
          return VoidType::create();
        }

      }
    }
  }
}
