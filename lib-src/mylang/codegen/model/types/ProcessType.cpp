#include <mylang/codegen/model/types/ProcessType.h>
#include <mylang/codegen/model/types/TypeVisitor.h>
#include <mylang/codegen/model/types/TypeSet.h>
#include <cassert>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        ProcessType::ProcessType(Ports xports)
            : ports(::std::move(xports))
        {
          for (auto i : ports) {
            if (i.second == nullptr) {
              throw ::std::invalid_argument("null value for input " + i.first);
            }
          }

        }

        ProcessType::~ProcessType()
        {
        }

        ::std::string ProcessType::toString() const
        {
          return "process";
        }

        Type::Ptr ProcessType::normalize(TypeSet &ts) const
        {
          Ports p;
          for (auto i : ports) {
            p[i.first] = i.second->normalize(ts)->self<InputType>();
          }
          return ts.add(get(p));
        }

        bool ProcessType::isSameType(const Type &other) const
        {
          auto that = dynamic_cast<const ProcessType*>(&other);
          if (!that || that->ports.size() != ports.size()) {
            return false;
          }

          for (auto i : that->ports) {
            auto tmp = ports.find(i.first);
            if (tmp == ports.end() || !tmp->second->isSameType(*i.second)) {
              return false;
            }
          }
          return true;
        }

        void ProcessType::accept(TypeVisitor &v) const
        {
          v.visit(::std::dynamic_pointer_cast<const ProcessType>(self()));
        }

        ::std::shared_ptr<const ProcessType> ProcessType::get(Ports xports)
        {
          struct Impl: public ProcessType
          {
            Impl(Ports xports)
                : ProcessType(xports)
            {
            }

            ~Impl()
            {
            }

          };
          return ::std::make_shared<const Impl>(::std::move(xports));
        }
      }
    }
  }
}
