#include <mylang/codegen/model/types/BitType.h>
#include <mylang/codegen/model/types/TypeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        BitType::BitType()
        {
        }

        BitType::~BitType()
        {
        }

        void BitType::accept(TypeVisitor &v) const
        {
          v.visit(::std::dynamic_pointer_cast<const BitType>(self()));
        }

        bool BitType::isSameType(const Type &other) const
        {
          return dynamic_cast<const BitType*>(&other) != nullptr;
        }

        ::std::string BitType::toString() const
        {
          return "bit";
        }

        ::std::shared_ptr<const BitType> BitType::create()
        {
          struct Impl: public BitType
          {
            ~Impl()
            {
            }
          };

          return ::std::make_shared<Impl>();
        }

      }
    }
  }
}
