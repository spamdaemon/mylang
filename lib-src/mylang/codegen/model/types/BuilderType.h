#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_BUILDERTYPE_H
#define CLASS_MYLANG_CODEGEN_MODEL_TYPES_BUILDERTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_ARRAYTYPE_H
#include <mylang/codegen/model/types/ArrayType.h>
#endif

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#include <memory>
#include <optional>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        /** A type representing an array */
        class BuilderType: public Type
        {
          /** The output type */
        public:
          typedef ArrayType ProductType;

          /** The output type */
        public:
          typedef ::std::shared_ptr<const ProductType> ProductTypePtr;

        private:
          BuilderType(ProductTypePtr ptr);

        public:
          ~BuilderType();

          /**
           * Create a new array with the specified element type. The element
           * type may an any element, otherwise be concrete.
           * @param xelement the element
           */
        public:
          static ::std::shared_ptr<const BuilderType> get(ProductTypePtr ptr);

          bool canSafeCastFrom(const Type &t) const override;
          bool canCastFrom(const Type &t) const override;
          Ptr normalize(TypeSet &ts) const;
          void accept(TypeVisitor &v) const override;
          ::std::string toString() const override;
          bool isSameType(const Type &other) const override;

          /** The target for the build */
        public:
          const ProductTypePtr product;

        };
      }
    }
  }
}
#endif
