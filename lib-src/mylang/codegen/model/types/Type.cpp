#include <mylang/codegen/model/types/Type.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {
        Type::Type()
        {
        }

        Type::Type(const ::mylang::names::Name::Ptr &xid)
            : _id(xid)
        {
        }

        Type::~Type()
        {
        }

        bool Type::canSafeCastFrom(const Type &from) const
        {
          return isSameType(from);
        }

        bool Type::canCastFrom(const Type &from) const
        {
          return canSafeCastFrom(from);
        }

        ::mylang::names::Name::Ptr Type::name() const
        {
          if (!_id) {
            _id = mylang::names::Name::create();
          }
          return _id;
        }

        Type::Ptr Type::unionWith(const Ptr &t) const
        {
          if (canSafeCastFrom(*t)) {
            return self();
          }
          if (t->canSafeCastFrom(*this)) {
            return t;
          }
          return nullptr;
        }

      }
    }
  }
}
