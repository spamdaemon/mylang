#ifndef FILE_MYLANG_CODEGEN_MODEL_TYPES_TYPES_H
#define FILE_MYLANG_CODEGEN_MODEL_TYPES_TYPES_H
#include <mylang/codegen/model/types/ArrayType.h>
#include <mylang/codegen/model/types/BuilderType.h>
#include <mylang/codegen/model/types/BitType.h>
#include <mylang/codegen/model/types/BooleanType.h>
#include <mylang/codegen/model/types/ByteType.h>
#include <mylang/codegen/model/types/CharType.h>
#include <mylang/codegen/model/types/IntegerType.h>
#include <mylang/codegen/model/types/FunctionType.h>
#include <mylang/codegen/model/types/MutableType.h>
#include <mylang/codegen/model/types/OptType.h>
#include <mylang/codegen/model/types/PrimitiveType.h>
#include <mylang/codegen/model/types/RealType.h>
#include <mylang/codegen/model/types/NamedType.h>
#include <mylang/codegen/model/types/StringType.h>
#include <mylang/codegen/model/types/StructType.h>
#include <mylang/codegen/model/types/TupleType.h>
#include <mylang/codegen/model/types/Type.h>
#include <mylang/codegen/model/types/UnionType.h>
#include <mylang/codegen/model/types/VoidType.h>
#include <mylang/codegen/model/types/GenericType.h>
#include <mylang/codegen/model/types/InputType.h>
#include <mylang/codegen/model/types/OutputType.h>
#include <mylang/codegen/model/types/ProcessType.h>

#endif
