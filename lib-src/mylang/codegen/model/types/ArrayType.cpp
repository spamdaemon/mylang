#include <mylang/codegen/model/types/ArrayType.h>
#include <mylang/codegen/model/types/TypeVisitor.h>
#include <mylang/codegen/model/types/TypeSet.h>
#include <mylang/codegen/model/types/IntegerType.h>
#include <cstddef>
#include <string>
#include <vector>
#include <cassert>
#include <utility>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {
        namespace {
          static ::std::shared_ptr<const IntegerType> getIndexType(Integer mx)
          {
            if (mx <= Integer::ZERO()) {
              return nullptr;
            }
            return IntegerType::create(Integer::ZERO(), mx.subtract(Integer::PLUS_ONE()));
          }
        }

        ArrayType::ArrayType(const Ptr &xelement, Integer xminSize, Integer xmaxSize)
            : element(xelement), minSize(xminSize), maxSize(xmaxSize), bounds(xminSize, xmaxSize),
                indexType(getIndexType(xmaxSize)), lenType(IntegerType::create(bounds))
        {
          if (!xelement) {
            throw ::std::invalid_argument("Null element");
          }
          if (maxSize.sign() < 0) {
            // TODO: this needs an explanation
            throw ::std::invalid_argument("Invalid min array size");
          }
          if (xmaxSize < xminSize) {
            throw ::std::invalid_argument("Invalid array bounds");
          }
        }

        ArrayType::~ArrayType()
        {
        }

        size_t ArrayType::dimensionality() const
        {
          size_t dim = 0;
          auto c = self<ArrayType>();
          while (c) {
            ++dim;
            c = c->element->self<ArrayType>();
          }
          return dim;
        }

        Type::Ptr ArrayType::leafElement() const
        {
          Ptr res;
          auto c = self<ArrayType>();
          while (c) {
            res = c->element;
            c = res->self<ArrayType>();
          }
          assert(res);
          return res;
        }

        Type::Ptr ArrayType::normalize(TypeSet &ts) const
        {
          auto e = element->normalize(ts);
          return ArrayType::get(e, minSize, maxSize);
        }

        void ArrayType::accept(TypeVisitor &v) const
        {
          v.visit(::std::dynamic_pointer_cast<const ArrayType>(self()));
        }

        Type::Ptr ArrayType::flatten() const
        {
          auto arr = ::std::dynamic_pointer_cast<const ArrayType>(element);
          if (arr) {
            return arr->flatten();
          } else {
            return element;
          }
        }

        ::std::string ArrayType::toString() const
        {
          ::std::string res;
          res += element->toString();
          res += "[]";
          return res;
        }

        bool ArrayType::isSameType(const Type &other) const
        {
          auto that = dynamic_cast<const ArrayType*>(&other);
          if (!that) {
            return false;
          }
          return bounds == that->bounds && element->isSameType(*that->element);
        }

        bool ArrayType::canSafeCastFrom(const Type &from) const
        {
          auto other = dynamic_cast<const ArrayType*>(&from);
          if (!other) {
            return Type::canSafeCastFrom(from);
          }

          if (!bounds.contains(other->bounds)) {
            return false;
          }
          return element->canSafeCastFrom(*other->element);
        }

        bool ArrayType::canCastFrom(const Type &from) const
        {
          auto other = dynamic_cast<const ArrayType*>(&from);
          if (!other) {
            return Type::canCastFrom(from);
          }
          if (!bounds.intersectWith(other->bounds).has_value()) {
            return false;
          }
          return element->canCastFrom(*other->element);
        }

        ::std::shared_ptr<const ArrayType> ArrayType::copy(
            const ::std::shared_ptr<const Type> &newElement) const
        {
          return get(newElement, minSize, maxSize);
        }

        ::std::shared_ptr<const ArrayType> ArrayType::get(::std::shared_ptr<const Type> xelement,
            Integer xminSize, Integer xmaxSize)
        {
          struct Impl: public ArrayType
          {
            Impl(::std::shared_ptr<const Type> t, Integer xminSize, Integer xmaxSize)
                : ArrayType(t, xminSize, xmaxSize)
            {
            }

            ~Impl()
            {
            }
          };
          return ::std::make_shared<const Impl>(xelement, xminSize, xmaxSize);
        }
      }
    }
  }
}
