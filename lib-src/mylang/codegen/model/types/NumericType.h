#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_NUMERICTYPE_H
#define CLASS_MYLANG_CODEGEN_MODEL_TYPES_NUMERICTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_PRIMITIVETYPE_H
#include <mylang/codegen/model/types/PrimitiveType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        class NumericType: public PrimitiveType
        {

        protected:
          NumericType();

        public:
          ~NumericType();
        };
      }
    }
  }
}
#endif
