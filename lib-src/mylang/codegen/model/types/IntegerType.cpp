#include <mylang/codegen/model/types/IntegerType.h>
#include <mylang/codegen/model/types/TypeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        IntegerType::IntegerType(Range xrange)
            : range(xrange)
        {
        }

        IntegerType::~IntegerType()
        {
        }

        void IntegerType::accept(TypeVisitor &v) const
        {
          v.visit(::std::dynamic_pointer_cast<const IntegerType>(self()));
        }

        bool IntegerType::isSameType(const Type &other) const
        {
          if (this == &other) {
            return true;
          }
          auto that = dynamic_cast<const IntegerType*>(&other);
          if (!that) {
            return false;
          }
          return that->range == range;
        }

        bool IntegerType::canSafeCastFrom(const Type &from) const
        {
          auto other = dynamic_cast<const IntegerType*>(&from);
          if (!other) {
            return Type::canSafeCastFrom(from);
          }

          return range.contains(other->range);
        }

        bool IntegerType::canCastFrom(const Type &from) const
        {
          auto other = dynamic_cast<const IntegerType*>(&from);
          if (!other) {
            return Type::canCastFrom(from);
          }
          return range.intersectWith(other->range).has_value();
        }

        ::std::string IntegerType::toString() const
        {
          ::std::string res;
          res += "integer";
          if (range.min().isFinite() || range.max().isFinite()) {
            res += '{';
            const char *sep = "";
            if (range.min().isFinite()) {
              res += sep;
              res += "min:";
              res += range.min()->toString();
              sep = ";";
            }
            if (range.max().isFinite()) {
              res += sep;
              res += "max:";
              res += range.max()->toString();
            }
            res += '}';
          }
          return res;
        }

        ::std::shared_ptr<const IntegerType> IntegerType::create(const Range &r)
        {
          struct Impl: public IntegerType
          {
            Impl(Range r)
                : IntegerType(r)
            {
            }

            ~Impl()
            {
            }
          };

          return ::std::make_shared<Impl>(r);
        }

        ::std::shared_ptr<const IntegerType> IntegerType::create(::std::optional<Integer> min,
            ::std::optional<Integer> max)
        {
          struct Impl: public IntegerType
          {
            Impl(::std::optional<Integer> min, ::std::optional<Integer> max)
                : IntegerType(Range(min, max))
            {
            }

            ~Impl()
            {
            }
          };

          return ::std::make_shared<Impl>(min, max);
        }
      }
    }
  }
}
