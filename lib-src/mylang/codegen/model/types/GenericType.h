#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_GENERICTYPE_H
#define CLASS_MYLANG_CODEGEN_MODEL_TYPES_GENERICTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_TYPE_H
#include <mylang/codegen/model/types/Type.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        /** A type representing an error */
        class GenericType: public Type
        {

        private:
          GenericType();

        public:
          ~GenericType();

          /** Two primitives are the same if their type ids are the same  */
        public:
          void accept(TypeVisitor &v) const;

          bool isSameType(const Type &other) const final;

          ::std::string toString() const final;

          Ptr normalize(TypeSet &ts) const;

        public:
          static ::std::shared_ptr<const GenericType> create();
        };
      }
    }
  }
}
#endif
