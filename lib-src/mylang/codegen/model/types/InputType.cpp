#include <mylang/codegen/model/types/InputType.h>
#include <mylang/codegen/model/types/OutputType.h>
#include <mylang/codegen/model/types/TypeVisitor.h>
#include <mylang/codegen/model/types/TypeSet.h>
#include <cassert>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        InputType::InputType(::std::shared_ptr<const Type> xelement)
            : InputOutputType(xelement)
        {
        }

        InputType::~InputType()
        {
        }

        ::std::string InputType::toString() const
        {
          return "=>" + element->toString();
        }

        Type::Ptr InputType::normalize(TypeSet &ts) const
        {
          return ts.add(get(element->normalize(ts)));
        }

        bool InputType::isSameType(const Type &other) const
        {
          auto that = dynamic_cast<const InputType*>(&other);
          if (!that) {
            return false;
          }
          return element->isSameType(*that->element);
        }

        void InputType::accept(TypeVisitor &v) const
        {
          v.visit(::std::dynamic_pointer_cast<const InputType>(self()));
        }

        ::std::shared_ptr<const InputType> InputType::get(::std::shared_ptr<const Type> xelement)
        {
          struct Impl: public InputType
          {
            Impl(::std::shared_ptr<const Type> t)
                : InputType(t)
            {
            }

            ~Impl()
            {
            }
            ::std::shared_ptr<const InputOutputType> getPeer() const
            {
              return OutputType::get(element);
            }

          };
          return ::std::make_shared<const Impl>(xelement);
        }
      }
    }
  }
}
