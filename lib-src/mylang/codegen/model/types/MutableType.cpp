#include <mylang/codegen/model/types/MutableType.h>
#include <mylang/codegen/model/types/TypeVisitor.h>
#include <mylang/codegen/model/types/TypeSet.h>
#include <cassert>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        MutableType::MutableType(::std::shared_ptr<const Type> xelement)
            : element(xelement)
        {
          if (!xelement) {
            throw ::std::invalid_argument("Null element");
          }
        }

        MutableType::~MutableType()
        {
        }

        Type::Ptr MutableType::flatten() const
        {
          auto opt = ::std::dynamic_pointer_cast<const MutableType>(element);
          if (opt) {
            return opt->flatten();
          } else {
            return element;
          }
        }

        bool MutableType::canSafeCastFrom(const Type &from) const
        {
          auto other = dynamic_cast<const MutableType*>(&from);
          if (!other) {
            return Type::canSafeCastFrom(from);
          }
          return element->canSafeCastFrom(*other->element);
        }

        bool MutableType::canCastFrom(const Type &from) const
        {
          auto other = dynamic_cast<const MutableType*>(&from);
          if (!other) {
            return Type::canCastFrom(from);
          }
          return element->canCastFrom(*other->element);
        }

        ::std::string MutableType::toString() const
        {
          return element->toString() + '!';
        }

        Type::Ptr MutableType::normalize(TypeSet &ts) const
        {
          return ts.add(get(element->normalize(ts)));
        }

        bool MutableType::isSameType(const Type &other) const
        {
          auto that = dynamic_cast<const MutableType*>(&other);
          if (!that) {
            return false;
          }
          return element->isSameType(*that->element);
        }

        ::std::shared_ptr<const MutableType> MutableType::copy(
            const ::std::shared_ptr<const Type> &newElement) const
        {
          return get(newElement);
        }

        void MutableType::accept(TypeVisitor &v) const
        {
          v.visit(::std::dynamic_pointer_cast<const MutableType>(self()));
        }

        ::std::shared_ptr<const MutableType> MutableType::get(
            ::std::shared_ptr<const Type> xelement)
        {
          struct Impl: public MutableType
          {
            Impl(::std::shared_ptr<const Type> t)
                : MutableType(t)
            {
            }

            ~Impl()
            {
            }

          };
          return ::std::make_shared<const Impl>(xelement);
        }
      }
    }
  }
}
