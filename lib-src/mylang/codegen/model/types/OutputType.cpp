#include <mylang/codegen/model/types/OutputType.h>
#include <mylang/codegen/model/types/InputType.h>
#include <mylang/codegen/model/types/TypeVisitor.h>
#include <mylang/codegen/model/types/TypeSet.h>
#include <cassert>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        OutputType::OutputType(::std::shared_ptr<const Type> xelement)
            : InputOutputType(xelement)
        {
        }

        OutputType::~OutputType()
        {
        }

        ::std::string OutputType::toString() const
        {
          return "<=" + element->toString();
        }

        Type::Ptr OutputType::normalize(TypeSet &ts) const
        {
          return ts.add(get(element->normalize(ts)));
        }

        bool OutputType::isSameType(const Type &other) const
        {
          auto that = dynamic_cast<const OutputType*>(&other);
          if (!that) {
            return false;
          }
          return element->isSameType(*that->element);
        }

        void OutputType::accept(TypeVisitor &v) const
        {
          v.visit(::std::dynamic_pointer_cast<const OutputType>(self()));
        }

        ::std::shared_ptr<const OutputType> OutputType::get(::std::shared_ptr<const Type> xelement)
        {
          struct Impl: public OutputType
          {
            Impl(::std::shared_ptr<const Type> t)
                : OutputType(t)
            {
            }

            ~Impl()
            {
            }
            ::std::shared_ptr<const InputOutputType> getPeer() const
            {
              return InputType::get(element);
            }

          };
          return ::std::make_shared<const Impl>(xelement);
        }
      }
    }
  }
}
