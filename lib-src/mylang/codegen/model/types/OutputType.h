#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_OUTPUTTYPE_H
#define CLASS_MYLANG_CODEGEN_MODEL_TYPES_OUTPUTTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_INPUTOUTPUTTYPE_H
#include <mylang/codegen/model/types/InputOutputType.h>
#endif

#include <memory>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        /** A type representing an error */
        class OutputType: public InputOutputType
        {

        private:
          OutputType(::std::shared_ptr<const Type> xelement);

        public:
          ~OutputType();

        public:
          static ::std::shared_ptr<const OutputType> get(::std::shared_ptr<const Type> xelement);

        public:
          Ptr normalize(TypeSet &ts) const override;

          void accept(TypeVisitor &v) const override;

          ::std::string toString() const final;

          bool isSameType(const Type &other) const override;
        };
      }
    }
  }
}
#endif
