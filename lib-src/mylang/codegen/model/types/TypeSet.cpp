#include <mylang/codegen/model/types/TypeSet.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        TypeSet::TypeSet()
        {
        }

        TypeSet::~TypeSet()
        {
        }

        Type::Ptr TypeSet::add(const Type::Ptr &type)
        {

          if (!type) {
            return type;
          }

          for (auto i = _types.begin(); i != _types.end(); ++i) {
            if ((*i)->isSameType(*type)) {
              return *i;
            }
          }
          _types.push_back(type);
          return type;
        }
      }
    }
  }
}
