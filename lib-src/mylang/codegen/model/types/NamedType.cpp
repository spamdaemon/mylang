#include <mylang/codegen/model/types/NamedType.h>
#include <mylang/codegen/model/types/TypeVisitor.h>
#include <mylang/codegen/model/types/TypeSet.h>
#include <cassert>
#include <set>
#include <functional>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        struct RecursionControl
        {
          ::std::set<mylang::names::Name::Ptr> names;

          template<class RET>
          RET recurse(const NamedType &ty, ::std::function<RET()> lambda, RET defaultValue)
          {
            if (names.insert(ty.name()).second) {
              try {
                auto res = lambda();
                names.erase(ty.name());
                return res;
              } catch (...) {
                names.erase(ty.name());
                throw;
              }
            } else {
              return defaultValue;
            }
          }

        };

        NamedType::NamedType(const mylang::names::Name::Ptr &xname)
            : Type(xname)
        {
        }

        NamedType::~NamedType()
        {
        }

        void NamedType::accept(TypeVisitor &v) const
        {
          v.visit(::std::dynamic_pointer_cast<const NamedType>(self()));
        }

        ::std::string NamedType::toString() const
        {
          return name()->fullName();
        }

        bool NamedType::isSameType(const Type &other) const
        {
          auto t = dynamic_cast<const NamedType*>(&other);
          if (t && name() == t->name()) {
            // TODO: we probably should have a check to make sure we have no inconsistencies
            return true;
          }
          return false;
        }

        ::std::shared_ptr<const NamedType> NamedType::getRecursiveReference() const
        {
          struct Impl: public NamedType
          {
            Impl(const ::std::shared_ptr<const NamedType> &ptr)
                : NamedType(ptr->name()), weakRef(ptr)
            {
            }

            ~Impl()
            {
            }

            ::std::shared_ptr<const NamedType> getRecursiveReference()
            {
              return ::std::dynamic_pointer_cast<const NamedType>(self());
            }

            Ptr normalize(TypeSet &ts) const
            {
              if (isResolved()) {
                ts.add(resolve());
              }
              return self();
            }

            bool isResolved() const
            {
              return weakRef.lock()->isResolved();
            }

            /** Resolve this name to an actual type. Throws an exception if the name cannot be resolved. */
          public:
            ::std::shared_ptr<const Type> resolve() const
            {
              return weakRef.lock()->resolve();
            }

            ::std::weak_ptr<const NamedType> weakRef;
          };
          return ::std::make_shared<Impl>(::std::dynamic_pointer_cast<const NamedType>(self()));
        }

        ::std::shared_ptr<const NamedType> NamedType::get(const mylang::names::Name::Ptr &xname,
            Resolver xresolver)
        {
          struct Impl: public NamedType
          {
            Impl(const mylang::names::Name::Ptr &xname, Resolver xresolver)
                : NamedType(xname), resolver(xresolver)
            {
            }

            ~Impl()
            {
            }

            bool isResolved() const
            {
              try {
                resolve();
                return true;
              } catch (...) {
                return false;
              }
            }

            Ptr normalize(TypeSet &ts) const
            {
              ts.add(self());
              if (isResolved()) {
                ts.add(impl->normalize(ts));
              }
              return self();
            }

            /** Resolve this name to an actual type. Throws an exception if the name cannot be resolved. */
          public:
            ::std::shared_ptr<const Type> resolve() const
            {
              if (impl) {
                return impl;
              }

              auto t = resolver();
              if (t) {
                resolver = Resolver();
                impl = t;
                return t;
              }
              throw ::std::runtime_error("Type not resolved : " + name()->fullName());
            }

            mutable Resolver resolver;
            mutable Ptr impl;
          };
          return ::std::make_shared<Impl>(xname, xresolver);
        }
      }
    }
  }
}
