#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_NAMEDTYPE_H
#define CLASS_MYLANG_CODEGEN_MODEL_TYPES_NAMEDTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_TYPE_H
#include <mylang/codegen/model/types/Type.h>
#endif

#ifndef CLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif
#include <memory>
#include <functional>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        /** A type representing an error */
        class NamedType: public Type
        {

          /** A resolver function; returns null if the name cannot be resolved */
        public:
          typedef ::std::function<Type::Ptr()> Resolver;

        private:
          NamedType(const mylang::names::Name::Ptr &name);

        public:
          ~NamedType();

        public:
          static ::std::shared_ptr<const NamedType> get(const mylang::names::Name::Ptr &name,
              Resolver resolver);

        public:
          void accept(TypeVisitor &v) const;

          bool isSameType(const Type &other) const final;

          ::std::string toString() const;

          bool isConstructible() const;

          /**
           * Create a type that can weakly reference
           */
        public:
          virtual ::std::shared_ptr<const NamedType> getRecursiveReference() const;

          /**
           * Determine if this name is resolved. If true, then resolve() will not throw an exception *
           */
        public:
          virtual bool isResolved() const = 0;

          /** Resolve this name to an actual type. Throws an exception if the name cannot be resolved. */
        public:
          virtual ::std::shared_ptr<const Type> resolve() const = 0;
        };
      }
    }
  }
}
#endif
