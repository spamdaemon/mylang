#include <mylang/codegen/model/types/GenericType.h>
#include <mylang/codegen/model/types/TypeVisitor.h>
#include <mylang/codegen/model/types/TypeSet.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace types {

        GenericType::GenericType()
        {
        }

        GenericType::~GenericType()
        {
        }

        Type::Ptr GenericType::normalize(TypeSet &ts) const
        {
          return ts.add(self());
        }

        bool GenericType::isSameType(const Type &other) const
        {
          return dynamic_cast<const GenericType*>(&other) != nullptr;
        }

        ::std::string GenericType::toString() const
        {
          return "generic";
        }

        void GenericType::accept(TypeVisitor &v) const
        {
          v.visit(::std::dynamic_pointer_cast<const GenericType>(self()));
        }

        ::std::shared_ptr<const GenericType> GenericType::create()
        {
          struct Impl: public GenericType
          {
            Impl()
            {
            }

            ~Impl()
            {
            }
          };
          return ::std::make_shared<Impl>();
        }
      }
    }
  }
}
