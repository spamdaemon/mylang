#ifndef FILE_MYLANG_CODEGEN_MODEL_H
#define FILE_MYLANG_CODEGEN_MODEL_H

#include <memory>

namespace mylang {
  namespace codegen {
    namespace model {

      namespace types {
        class Type;
      }

      namespace ir {
        class Node;
        class Statement;
        class Expression;
        class Literal;
        class Variable;
        class Program;
      }

      typedef ::std::shared_ptr<const mylang::codegen::model::types::Type> MType;
      typedef ::std::shared_ptr<const mylang::codegen::model::ir::Node> MNode;
      typedef ::std::shared_ptr<const mylang::codegen::model::ir::Statement> MStatement;
      typedef ::std::shared_ptr<const mylang::codegen::model::ir::Expression> MExpression;
      typedef ::std::shared_ptr<const mylang::codegen::model::ir::Literal> MLiteral;
      typedef ::std::shared_ptr<const mylang::codegen::model::ir::Variable> MVariable;
      typedef ::std::shared_ptr<const mylang::codegen::model::ir::Program> MProgram;

    }
  }
}
#endif
