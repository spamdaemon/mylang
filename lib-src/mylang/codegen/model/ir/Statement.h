#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENT_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENT_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_NODE_H
#include <mylang/codegen/model/ir/Node.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        class Statement: public Node
        {
          Statement(const Statement &e) = delete;

          Statement& operator=(const Statement &e) = delete;

          /** An statement pointer */
        public:
          typedef ::std::shared_ptr<const Statement> StatementPtr;

          /** A statement */
        public:
          Statement();

          /** Destructor */
        public:
          virtual ~Statement() = 0;

        };
      }
    }
  }
}
#endif
