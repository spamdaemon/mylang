#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALTUPLE_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALTUPLE_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERAL_H
#include <mylang/codegen/model/ir/Literal.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_TUPLETYPE_H
#include <mylang/codegen/model/types/TupleType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * This class represent the constructor for a literal.
         */
        class LiteralTuple: public Literal
        {
          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const LiteralTuple> LiteralTuplePtr;

          /** The elements */
        public:
          typedef ::std::vector<Literal::LiteralPtr> Elements;

          /**
           * Create an expression that returns a literal value.
           * @param type the type
           * @aparam value the Tuple value for the literal
           */
        public:
          LiteralTuple(::std::shared_ptr<const types::TupleType> type, const Elements &elements);

          /** Destructor */
        public:
          ~LiteralTuple();

          /**
           * Create an bit literal
           * @param value true or false
           * @return a literal
           */
        public:
          static LiteralTuplePtr create(::std::shared_ptr<const types::TupleType> type,
              const Elements &elements);

        public:
          void accept(NodeVisitor &visitor) const override final;
          bool isSameLiteral(const Literal &that) const override;

          /** The Int64 value */
        public:
          const Elements elements;
        };
      }
    }
  }
}
#endif
