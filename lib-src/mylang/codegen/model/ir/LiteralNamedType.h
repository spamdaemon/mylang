#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALNAMEDTYPE_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALNAMEDTYPE_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERAL_H
#include <mylang/codegen/model/ir/Literal.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_NamedType_H
#include <mylang/codegen/model/types/NamedType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * This class represent the constructor for a literal.
         */
        class LiteralNamedType: public Literal
        {
          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const LiteralNamedType> LiteralNamedTypePtr;

          /**
           * Create an expression that returns a literal value.
           * @param type the type
           * @aparam value the NamedType value for the literal
           */
        public:
          LiteralNamedType(::std::shared_ptr<const types::NamedType> type, LiteralPtr value);

          /** Destructor */
        public:
          ~LiteralNamedType();

          /**
           * Create an bit literal
           * @param value true or false
           * @return a literal
           */
        public:
          static LiteralNamedTypePtr create(::std::shared_ptr<const types::NamedType> type,
              LiteralPtr value);

        public:
          void accept(NodeVisitor &visitor) const override final;
          bool isSameLiteral(const Literal &that) const override;

          /** The Int64 value */
        public:
          const LiteralPtr value;
        };
      }
    }
  }
}
#endif
