#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALUNION_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALUNION_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERAL_H
#include <mylang/codegen/model/ir/Literal.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_UNIONTYPE_H
#include <mylang/codegen/model/types/UnionType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * This class represent the constructor for a literal.
         */
        class LiteralUnion: public Literal
        {
          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const LiteralUnion> LiteralUnionPtr;

          /**
           * Create an expression that returns a literal value.
           * @param type the type
           * @aparam value the Union value for the literal
           */
        public:
          LiteralUnion(::std::shared_ptr<const types::UnionType> type, const ::std::string &member,
              LiteralPtr value);

          /** Destructor */
        public:
          ~LiteralUnion();

          /**
           * Create an bit literal
           * @param value true or false
           * @return a literal
           */
        public:
          static LiteralUnionPtr create(::std::shared_ptr<const types::UnionType> type,
              const ::std::string &member, LiteralPtr value);

        public:
          void accept(NodeVisitor &visitor) const override final;
          bool isSameLiteral(const Literal &that) const override;

          /** The member to be set (equivalent to the discriminant) */
        public:
          const ::std::string member;

          /** The value */
        public:
          const LiteralPtr value;

        };
      }
    }
  }
}
#endif
