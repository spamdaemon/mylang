#include <mylang/codegen/model/ir/ForeachStatement.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <mylang/codegen/model/types/ArrayType.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        ForeachStatement::ForeachStatement(NamePtr loopName, Variable::VariablePtr loopvar,
            Variable::VariablePtr loopdata, StatementBlock::StatementBlockPtr xbody)
            : name(loopName), variable(loopvar), data(loopdata), body(xbody)
        {
        }

        ForeachStatement::~ForeachStatement()
        {
        }

        ForeachStatement::ForeachStatementPtr ForeachStatement::create(NamePtr loopName,
            Variable::VariablePtr loopvar, Variable::VariablePtr loopdata,
            StatementBlock::StatementBlockPtr xbody)
        {
          return ::std::make_shared<ForeachStatement>(loopName, loopvar, loopdata, xbody);
        }

        void ForeachStatement::accept(NodeVisitor &visitor) const
        {
          visitor.visitForeachStatement(self<ForeachStatement>());
        }

      }
    }
  }
}
