#include <mylang/codegen/model/ir/OwnConstructorCall.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        OwnConstructorCall::OwnConstructorCall(::std::vector<StatementPtr> xpre,
            ::std::vector<Variable::VariablePtr> args)
            : pre(xpre), arguments(::std::move(args))
        {
        }

        OwnConstructorCall::~OwnConstructorCall()
        {
        }

        OwnConstructorCall::OwnConstructorCallPtr OwnConstructorCall::create(
            ::std::vector<StatementPtr> xpre, ::std::vector<Variable::VariablePtr> args)
        {
          return ::std::make_shared<OwnConstructorCall>(xpre, args);
        }

        void OwnConstructorCall::accept(NodeVisitor &visitor) const
        {
          visitor.visitOwnConstructorCall(self<OwnConstructorCall>());
        }

      }
    }
  }
}
