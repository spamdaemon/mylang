#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALBOOLEAN_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALBOOLEAN_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALPRIMITIVE_H
#include <mylang/codegen/model/ir/LiteralPrimitive.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_BOOLEANTYPE_H
#include <mylang/codegen/model/types/BooleanType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * This class represent the constructor for a literal.
         */
        class LiteralBoolean: public LiteralPrimitive
        {
          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const LiteralBoolean> LiteralBooleanPtr;

          /** The value type */
        public:
          typedef bool ValueType;

          /**
           * Create an expression that returns a literal value.
           * @param type the type
           * @aparam value the string value for the literal
           */
        public:
          LiteralBoolean(::std::shared_ptr<const types::BooleanType> type, ValueType value);

          /** Destructor */
        public:
          ~LiteralBoolean();

          /**
           * Create an bit literal
           * @param value true or false
           * @return a literal
           */
        public:
          static LiteralBooleanPtr create(::std::shared_ptr<const types::BooleanType> type,
              ValueType value);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /** The bit value */
        public:
          const ValueType value;
        };
      }
    }
  }
}
#endif
