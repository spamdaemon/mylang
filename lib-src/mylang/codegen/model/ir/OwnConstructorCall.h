#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_OWNCONSTRUCTORCALL_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_OWNCONSTRUCTORCALL_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENT_H
#include <mylang/codegen/model/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#include <mylang/codegen/model/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_TYPE_H
#include <mylang/codegen/model/types/Type.h>
#endif
#include <vector>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * This class represents a call to another constructor of the same being constructed.
         * It is not a normal statement and must effectively be the first statement in a constructor,
         * e.g. process constructor.
         */
        class OwnConstructorCall: public Statement
        {
          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const OwnConstructorCall> OwnConstructorCallPtr;

          /** The name */
        public:
          typedef ::std::shared_ptr<const mylang::names::Name> FQN;

          /**
           * Create an expression with the specified type.
           * @param type the return type
           * @param fn the name of the function
           * @param args the arguments
           */
        public:
          OwnConstructorCall(::std::vector<StatementPtr> pre,
              ::std::vector<Variable::VariablePtr> args);

          /**destructor */
        public:
          ~OwnConstructorCall();

          /**
           * Create a function call.
           * @param type the return type
           * @param name the name of the function
           * @param args the arguments
           * @return afunction call ptr/
           */
        public:
          static OwnConstructorCallPtr create(::std::vector<StatementPtr> pre,
              ::std::vector<Variable::VariablePtr> args);

          void accept(NodeVisitor &visitor) const override final;

        public:
          const ::std::vector<StatementPtr> pre;

          /** The function arguments */
        public:
          const ::std::vector<Variable::VariablePtr> arguments;
        };
      }
    }
  }
}
#endif
