#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_THROWSTATEMENT_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_THROWSTATEMENT_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENT_H
#include <mylang/codegen/model/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#include <mylang/codegen/model/ir/Variable.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * Assign a value to a variable that is in scope.
         */
        class ThrowStatement: public Statement
        {
          ThrowStatement(const ThrowStatement &e) = delete;

          ThrowStatement& operator=(const ThrowStatement &e) = delete;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const ThrowStatement> ThrowStatementPtr;

          /**
           * Crea
           * @param name a name
           * @param value the value to be bound
           */
        public:
          ThrowStatement(Variable::VariablePtr value);

          /** Destructor */
        public:
          ~ThrowStatement();

          /**
           * Updateare a mutable variable with the specified initializer
           * @param value the value to be bound
           */
        public:
          static ThrowStatementPtr create(Variable::VariablePtr value);

          /**
           * Return.
           */
        public:
          static ThrowStatementPtr create();

        public:
          void accept(NodeVisitor &visitor) const override final;

          /**
           * The bound value (may be null!)
           */
        public:
          const Variable::VariablePtr value;
        };
      }
    }
  }
}
#endif
