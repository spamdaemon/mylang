#include <mylang/codegen/model/ir/CommentStatement.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        CommentStatement::CommentStatement(::std::string xcomment)
            : comment(xcomment)
        {
        }

        CommentStatement::~CommentStatement()
        {
        }

        CommentStatement::CommentStatementPtr CommentStatement::create(::std::string xcomment)
        {
          return ::std::make_shared<CommentStatement>(xcomment);
        }

        void CommentStatement::accept(NodeVisitor &visitor) const
        {
          visitor.visitCommentStatement(self<CommentStatement>());
        }

      }
    }
  }
}
