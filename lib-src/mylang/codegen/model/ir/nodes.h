#ifndef FILE_MYLANG_CODEGEN_MODEL_IR_NODES_H
#define FILE_MYLANG_CODEGEN_MODEL_IR_NODES_H

#include <mylang/codegen/model/ir/GlobalValue.h>
#include <mylang/codegen/model/ir/Program.h>

// expressions
#include <mylang/codegen/model/ir/OperatorExpression.h>
#include <mylang/codegen/model/ir/GetDiscriminant.h>
#include <mylang/codegen/model/ir/GetMember.h>
#include <mylang/codegen/model/ir/LambdaExpression.h>
#include <mylang/codegen/model/ir/LiteralBit.h>
#include <mylang/codegen/model/ir/LiteralBoolean.h>
#include <mylang/codegen/model/ir/LiteralByte.h>
#include <mylang/codegen/model/ir/LiteralChar.h>
#include <mylang/codegen/model/ir/LiteralInteger.h>
#include <mylang/codegen/model/ir/LiteralReal.h>
#include <mylang/codegen/model/ir/LiteralString.h>
#include <mylang/codegen/model/ir/LiteralArray.h>
#include <mylang/codegen/model/ir/LiteralNamedType.h>
#include <mylang/codegen/model/ir/LiteralOptional.h>
#include <mylang/codegen/model/ir/LiteralStruct.h>
#include <mylang/codegen/model/ir/LiteralTuple.h>
#include <mylang/codegen/model/ir/LiteralUnion.h>
#include <mylang/codegen/model/ir/NewProcess.h>
#include <mylang/codegen/model/ir/NewUnion.h>
#include <mylang/codegen/model/ir/NoValue.h>
#include <mylang/codegen/model/ir/Variable.h>

// statements
#include <mylang/codegen/model/ir/AbortStatement.h>
#include <mylang/codegen/model/ir/BreakStatement.h>
#include <mylang/codegen/model/ir/ContinueStatement.h>
#include <mylang/codegen/model/ir/CommentStatement.h>
#include <mylang/codegen/model/ir/ForeachStatement.h>
#include <mylang/codegen/model/ir/IfStatement.h>
#include <mylang/codegen/model/ir/InputPortDecl.h>
#include <mylang/codegen/model/ir/LoopStatement.h>
#include <mylang/codegen/model/ir/OutputPortDecl.h>
#include <mylang/codegen/model/ir/OwnConstructorCall.h>
#include <mylang/codegen/model/ir/ProcessBlock.h>
#include <mylang/codegen/model/ir/ProcessConstructor.h>
#include <mylang/codegen/model/ir/ProcessDecl.h>
#include <mylang/codegen/model/ir/ReturnStatement.h>
#include <mylang/codegen/model/ir/StatementBlock.h>
#include <mylang/codegen/model/ir/ThrowStatement.h>
#include <mylang/codegen/model/ir/TryCatch.h>
#include <mylang/codegen/model/ir/ValueDecl.h>
#include <mylang/codegen/model/ir/VariableDecl.h>
#include <mylang/codegen/model/ir/VariableUpdate.h>

// other
#include <mylang/codegen/model/ir/Parameter.h>

#endif
