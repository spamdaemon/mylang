#include <mylang/codegen/model/ir/Program.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        Program::Program(Globals xglobals, Processes xprocesses, NamedTypes xtypes)
            : globals(xglobals), processes(xprocesses), types(xtypes)
        {
        }

        Program::~Program()
        {
        }

        Program::ProgramPtr Program::create(Globals xglobals, Processes xprocesses,
            NamedTypes xtypes)
        {
          return ::std::make_shared<Program>(xglobals, xprocesses, xtypes);
        }

        void Program::accept(NodeVisitor &visitor) const
        {
          visitor.visitProgram(self<Program>());
        }

      }
    }
  }
}
