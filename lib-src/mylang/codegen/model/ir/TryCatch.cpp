#include <mylang/codegen/model/ir/TryCatch.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        TryCatch::TryCatch(StatementBlock::StatementBlockPtr xTryBody,
            StatementBlock::StatementBlockPtr xCatchBody)
            : tryBody(xTryBody), catchBody(xCatchBody)
        {
        }

        TryCatch::~TryCatch()
        {
        }

        TryCatch::TryCatchPtr TryCatch::create(StatementBlock::StatementBlockPtr xTryBody,
            StatementBlock::StatementBlockPtr xCatchBody)
        {
          return ::std::make_shared<TryCatch>(xTryBody, xCatchBody);
        }

        void TryCatch::accept(NodeVisitor &visitor) const
        {
          visitor.visitTryCatch(self<TryCatch>());
        }

      }
    }
  }
}
