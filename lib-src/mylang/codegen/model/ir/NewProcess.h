#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_NEWPROCESS_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_NEWPROCESS_H

#ifndef MYLANGCLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#include <mylang/codegen/model/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERAL_H
#include <mylang/codegen/model/ir/Literal.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_TYPE_H
#include <mylang/codegen/model/types/Type.h>
#endif
#include <vector>

namespace mylang {
  namespace codegen {
    namespace model {

      namespace ir {
        class NewProcess: public Expression
        {

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const NewProcess> NewProcessPtr;

          /**
           * Create an expression gets the specified member variable.
           * @param type the return type
           * @param name the name of the process
           * @param ctor the name of the process constructor
           * @param arguments to the constructor
           */
        public:
          NewProcess(TypePtr t, const mylang::names::Name::Ptr &pname,
              const mylang::names::Name::Ptr &ctorName, ::std::vector<Variable::VariablePtr> args);

          /**destructor */
        public:
          ~NewProcess();

          /**
           * Create a function call.
           * @param type the return type
           * @param member the member to be initialized
           * @param args the arguments
           * @return afunction call ptr/
           */
        public:
          static NewProcessPtr create(TypePtr t, const mylang::names::Name::Ptr &pname,
              const mylang::names::Name::Ptr &ctorName, ::std::vector<Variable::VariablePtr> value);

          void accept(NodeVisitor &visitor) const override final;

          /** The name of the process */
        public:
          const mylang::names::Name::Ptr processName;

          /** The constructor name */
        public:
          const mylang::names::Name::Ptr constructorName;

          /** The object on which to invoke the method */
        public:
          const ::std::vector<Variable::VariablePtr> arguments;
        };

      }
    }
  }
}
#endif
