#include <mylang/codegen/model/ir/InputPortDecl.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        InputPortDecl::InputPortDecl(Variable::VariablePtr xval, ::std::string xpublicName)
            : PortDecl(xval, xpublicName)
        {
        }

        InputPortDecl::~InputPortDecl()
        {
        }

        InputPortDecl::InputPortDeclPtr InputPortDecl::create(Variable::VariablePtr xval,
            ::std::string xpublicName)
        {
          return ::std::make_shared<InputPortDecl>(xval, xpublicName);
        }

        void InputPortDecl::accept(NodeVisitor &visitor) const
        {
          visitor.visitInputPortDecl(self<InputPortDecl>());
        }

      }
    }
  }
}
