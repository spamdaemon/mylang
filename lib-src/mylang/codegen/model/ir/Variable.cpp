#include <mylang/codegen/model/ir/Variable.h>
#include <mylang/codegen/model/ir/Node.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <algorithm>
#include <memory>
#include <string>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        Variable::Variable(Scope xscope, TypePtr t, NamePtr xname)
            : Expression(::std::move(t)), scope(xscope), name(::std::move(xname))
        {
        }

        Variable::~Variable()
        {
        }

        Variable::VariablePtr Variable::create(Scope xscope, TypePtr t, NamePtr xname)
        {
          return ::std::make_shared<Variable>(xscope, t, xname);
        }
        void Variable::accept(NodeVisitor &visitor) const
        {
          visitor.visitVariable(self<Variable>());
        }

      }
    }
  }
}
