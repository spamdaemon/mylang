#include <mylang/codegen/model/ir/LiteralByte.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        LiteralByte::LiteralByte(::std::shared_ptr<const types::ByteType> xtype, ValueType xvalue)
            : LiteralPrimitive(xtype, ::std::to_string(static_cast<uint16_t>(xvalue))),
                value(xvalue)
        {
        }

        LiteralByte::~LiteralByte()
        {
        }

        LiteralByte::LiteralBytePtr LiteralByte::create(
            ::std::shared_ptr<const types::ByteType> xtype, ValueType xvalue)
        {
          return ::std::make_shared<LiteralByte>(xtype, xvalue);
        }

        void LiteralByte::accept(NodeVisitor &visitor) const
        {
          visitor.visitLiteralByte(self<LiteralByte>());
        }

      }
    }
  }
}
