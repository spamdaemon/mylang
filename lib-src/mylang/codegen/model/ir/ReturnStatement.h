#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_RETURNSTATEMENT_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_RETURNSTATEMENT_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENT_H
#include <mylang/codegen/model/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#include <mylang/codegen/model/ir/Variable.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * Assign a value to a variable that is in scope.
         */
        class ReturnStatement: public Statement
        {
          ReturnStatement(const ReturnStatement &e) = delete;

          ReturnStatement& operator=(const ReturnStatement &e) = delete;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const ReturnStatement> ReturnStatementPtr;

          /**
           * Crea
           * @param name a name
           * @param value the value to be bound
           */
        public:
          ReturnStatement(Variable::VariablePtr value);

          /** Destructor */
        public:
          ~ReturnStatement();

          /**
           * Updateare a mutable variable with the specified initializer
           * @param value the value to be bound
           */
        public:
          static ReturnStatementPtr create(Variable::VariablePtr value);

          /**
           * Return.
           */
        public:
          static ReturnStatementPtr create();

        public:
          void accept(NodeVisitor &visitor) const override final;

          /**
           * The bound value (may be null!)
           */
        public:
          const Variable::VariablePtr value;
        };
      }
    }
  }
}
#endif
