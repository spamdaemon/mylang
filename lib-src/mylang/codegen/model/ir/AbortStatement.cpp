#include <mylang/codegen/model/ir/AbortStatement.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        AbortStatement::AbortStatement(Variable::VariablePtr xexpr)
            : value(xexpr)
        {
        }

        AbortStatement::~AbortStatement()
        {
        }

        AbortStatement::AbortStatementPtr AbortStatement::create()
        {
          return ::std::make_shared<AbortStatement>(nullptr);
        }

        AbortStatement::AbortStatementPtr AbortStatement::create(Variable::VariablePtr xvalue)
        {
          return ::std::make_shared<AbortStatement>(xvalue);
        }

        void AbortStatement::accept(NodeVisitor &visitor) const
        {
          visitor.visitAbortStatement(self<AbortStatement>());
        }

      }

    }
  }
}
