#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_BreakSTATEMENT_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_BreakSTATEMENT_H

#ifndef MYLANGCLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENT_H
#include <mylang/codegen/model/ir/Statement.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * Assign a value to a variable that is in scope.
         */
        class BreakStatement: public Statement
        {
          BreakStatement(const BreakStatement &e) = delete;

          BreakStatement& operator=(const BreakStatement &e) = delete;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const BreakStatement> BreakStatementPtr;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const mylang::names::Name> NamePtr;

          /**
           * Create an Breakion statement.
           * @param scope the name of hte scope to exit (e.g. a loop name)
           */
        public:
          BreakStatement(NamePtr scope);

          /** Destructor */
        public:
          ~BreakStatement();

          /**
           * Create a new break statement
           * @param scope the name of hte scope to exit (e.g. a loop name)
           */
        public:
          static BreakStatementPtr create(NamePtr scope);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /** The name of the scope to exit */
        public:
          const NamePtr scope;
        };
      }
    }
  }
}
#endif
