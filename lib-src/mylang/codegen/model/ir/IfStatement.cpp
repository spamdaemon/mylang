#include <mylang/codegen/model/ir/IfStatement.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        IfStatement::IfStatement(Variable::VariablePtr xcond,
            StatementBlock::StatementBlockPtr xiftrue, StatementBlock::StatementBlockPtr xiffalse)
            : condition(xcond), iftrue(xiftrue ? xiftrue : StatementBlock::create()),
                iffalse(xiffalse ? xiffalse : StatementBlock::create())

        {
        }

        IfStatement::~IfStatement()
        {
        }

        IfStatement::IfStatementPtr IfStatement::create(Variable::VariablePtr xcond,
            StatementBlock::StatementBlockPtr xiftrue, StatementBlock::StatementBlockPtr xiffalse)
        {
          return ::std::make_shared<IfStatement>(xcond, xiftrue, xiffalse);
        }

        void IfStatement::accept(NodeVisitor &visitor) const
        {
          visitor.visitIfStatement(self<IfStatement>());
        }

      }

    }
  }
}
