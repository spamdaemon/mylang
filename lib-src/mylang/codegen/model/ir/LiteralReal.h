#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALREAL_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALREAL_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALPRIMITIVE_H
#include <mylang/codegen/model/ir/LiteralPrimitive.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_REALTYPE_H
#include <mylang/codegen/model/types/RealType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * This class represent the constructor for a literal.
         */
        class LiteralReal: public LiteralPrimitive
        {
          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const LiteralReal> LiteralRealPtr;

          /** The value type */
        public:
          typedef BigReal ValueType;

          /**
           * Create an expression that returns a literal value.
           * @param type the type
           * @aparam value the string value for the literal
           */
        public:
          LiteralReal(::std::shared_ptr<const types::RealType> type, ValueType value);

          /** Destructor */
        public:
          ~LiteralReal();

          /**
           * Create an bit literal
           * @param value true or false
           * @return a literal
           */
        public:
          static LiteralRealPtr create(const ::std::shared_ptr<const types::RealType> type,
              ValueType value);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /** The real value */
        public:
          const ValueType value;
        };
      }
    }
  }
}
#endif
