#include <mylang/codegen/model/ir/Literal.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        Literal::Literal(TypePtr xtype)
            : Expression(::std::move(xtype))
        {
        }

        Literal::~Literal()
        {
        }

      }
    }
  }
}
