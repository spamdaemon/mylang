#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_DECLARATION_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_DECLARATION_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENT_H
#include <mylang/codegen/model/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_NODE_H
#include <mylang/codegen/model/ir/Node.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_TYPE_H
#include <mylang/codegen/model/types/Type.h>
#endif

#ifndef CLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {
        class Declaration: public Statement
        {
        public:
          enum Scope
          {
            FUNCTION_SCOPE, GLOBAL_SCOPE, PROCESS_SCOPE
          };

          Declaration(const Declaration &e) = delete;

          Declaration& operator=(const Declaration &e) = delete;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const mylang::codegen::model::types::Type> TypePtr;

          /** A name pointer */
        public:
          typedef ::std::shared_ptr<const mylang::names::Name> NamePtr;

          /** An expression pointer */
        public:
          typedef ::std::shared_ptr<const Declaration> DeclarationPtr;

          /**
           * Create a declaration of that associates a type with a name.
           * @param name the name of the declaration
           * @param type the type
           */
        public:
          Declaration(Scope scope, NamePtr name, TypePtr t);

          /** Destructor */
        public:
          virtual ~Declaration() = 0;

          /**
           * The scope at which the variable is defined.
           */
        public:
          const Scope scope;

          /**
           * Get the type of this expression.
           * @return a type
           */
        public:
          const NamePtr name;

          /**
           * Get the type of this expression.
           * @return a type
           */
        public:
          const TypePtr type;
        };
      }
    }
  }
}
#endif
