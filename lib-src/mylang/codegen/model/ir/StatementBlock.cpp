#include <mylang/codegen/model/ir/StatementBlock.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        StatementBlock::StatementBlock(Statements xstatements)
            : statements(xstatements)
        {
        }

        StatementBlock::~StatementBlock()
        {
        }

        StatementBlock::StatementBlockPtr StatementBlock::create()
        {
          Statements stmts;
          return create(stmts);
        }

        StatementBlock::StatementBlockPtr StatementBlock::create(Statements xstatements)
        {
          if (xstatements.size() == 1) {
            auto block = xstatements.at(0)->self<StatementBlock>();
            if (block) {
              return block;
            }
          }
          return ::std::make_shared<StatementBlock>(xstatements);
        }

        StatementBlock::StatementBlockPtr StatementBlock::create(StatementPtr stmt)
        {
          Statements stmts;
          if (stmt) {
            stmts.push_back(stmt);
          }
          return create(stmts);
        }

        void StatementBlock::accept(NodeVisitor &visitor) const
        {
          visitor.visitStatementBlock(self<StatementBlock>());
        }

      }
    }
  }
}
