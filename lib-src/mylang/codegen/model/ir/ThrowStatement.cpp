#include <mylang/codegen/model/ir/ThrowStatement.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        ThrowStatement::ThrowStatement(Variable::VariablePtr xexpr)
            : value(xexpr)
        {
        }

        ThrowStatement::~ThrowStatement()
        {
        }

        ThrowStatement::ThrowStatementPtr ThrowStatement::create()
        {
          return ::std::make_shared<ThrowStatement>(nullptr);
        }

        ThrowStatement::ThrowStatementPtr ThrowStatement::create(Variable::VariablePtr xvalue)
        {
          return ::std::make_shared<ThrowStatement>(xvalue);
        }

        void ThrowStatement::accept(NodeVisitor &visitor) const
        {
          visitor.visitThrowStatement(self<ThrowStatement>());
        }

      }
    }
  }
}
