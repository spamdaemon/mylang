#include <mylang/codegen/model/model.h>
#include <mylang/codegen/model/ir/Node.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <mylang/codegen/model/ir/ProcessDecl.h>
#include <mylang/codegen/model/types/InputOutputType.h>
#include <mylang/codegen/model/types/ProcessType.h>
#include <memory>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {
        namespace {
          static MType make_process_type(const ProcessDecl::Ports ports)
          {
            mylang::codegen::model::types::ProcessType::Ports P;

            for (auto p : ports) {
              auto ty = p->type->self<mylang::codegen::model::types::InputOutputType>();
              P.emplace(p->name->localName(), ty);
            }

            return mylang::codegen::model::types::ProcessType::get(P);
          }
        }

        ProcessDecl::ProcessDecl(Scope xscope, NamePtr xname,
            ::std::shared_ptr<const types::ProcessType> xtype, Ports xpublicPorts,
            Declarations xdecls, Constructors xctors, Blocks xblocks)
            : Declaration(xscope, xname, xtype), publicPorts(xpublicPorts), declarations(xdecls),
                constructors(xctors), blocks(xblocks)
        {
          if (!xtype->isSameType(*make_process_type(xpublicPorts))) {
            throw ::std::runtime_error("Public ports and process type mismatch");
          }
        }

        ProcessDecl::~ProcessDecl()
        {
        }

        ProcessDecl::ProcessDeclPtr ProcessDecl::create(Scope xscope, NamePtr xname,
            ::std::shared_ptr<const types::ProcessType> xtype, Ports xpublicPorts,
            Declarations xdecls, Constructors xctors, Blocks xblocks)
        {
          return ::std::make_shared<ProcessDecl>(xscope, xname, xtype, xpublicPorts, xdecls, xctors,
              xblocks);
        }

        ProcessBlock::ProcessBlockPtr ProcessDecl::getMainBlock() const
        {
          if (blocks.size() == 1 && blocks.at(0)->name.has_value()
              && blocks.at(0)->name.value() == "main") {
            return blocks.at(0);
          }
          return nullptr;
        }

        PortDecl::PortDeclPtr ProcessDecl::getDeclaredPort(const ::std::string &portName) const
        {
          for (auto p : publicPorts) {
            if (p->publicName == portName) {
              return p;
            }
          }
          return nullptr;
        }

        void ProcessDecl::accept(NodeVisitor &visitor) const
        {
          visitor.visitProcessDecl(self<ProcessDecl>());
        }

      }

    }
  }
}
