#include <mylang/codegen/model/ir/LambdaExpression.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        LambdaExpression::LambdaExpression(::std::shared_ptr<const types::FunctionType> xtype,
            Parameters xparameters, StatementBlock::StatementBlockPtr xbody)
            : Expression(xtype), parameters(xparameters), body(xbody)
        {
          assert(parameters.size() == xtype->parameters.size());
          for (size_t i = 0; i < parameters.size(); ++i) {
            assert(parameters.at(i)->variable->type->isSameType(*xtype->parameters.at(i)));
          }
        }

        LambdaExpression::~LambdaExpression()
        {
        }

        LambdaExpression::LambdaExpressionPtr LambdaExpression::create(
            ::std::shared_ptr<const types::FunctionType> xtype, Parameters xparameters,
            StatementBlock::StatementBlockPtr xbody)
        {
          return ::std::make_shared<LambdaExpression>(xtype, xparameters, xbody);
        }

        void LambdaExpression::accept(NodeVisitor &visitor) const
        {
          visitor.visitLambdaExpression(self<LambdaExpression>());
        }

      }

    }
  }
}
