#include <mylang/codegen/model/ir/LiteralBit.h>
#include <mylang/codegen/model/ir/Node.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <memory>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        LiteralBit::LiteralBit(::std::shared_ptr<const types::BitType> xtype, ValueType xvalue)
            : LiteralPrimitive(xtype, xvalue ? "1" : "0"), value(xvalue)
        {
        }

        LiteralBit::~LiteralBit()
        {
        }

        LiteralBit::LiteralBitPtr LiteralBit::create(::std::shared_ptr<const types::BitType> xtype,
            ValueType xvalue)
        {
          return ::std::make_shared<LiteralBit>(xtype, xvalue);
        }

        void LiteralBit::accept(NodeVisitor &visitor) const
        {
          visitor.visitLiteralBit(self<LiteralBit>());
        }

      }
    }
  }
}
