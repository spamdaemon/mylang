#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALSTRING_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALSTRING_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALPRIMITIVE_H
#include <mylang/codegen/model/ir/LiteralPrimitive.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_STRINGTYPE_H
#include <mylang/codegen/model/types/StringType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * This class represent the constructor for a literal.
         */
        class LiteralString: public LiteralPrimitive
        {
          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const LiteralString> LiteralStringPtr;

          /** The value type */
        public:
          typedef ::std::string ValueType;

          /**
           * Create an expression that returns a literal value.
           * @param type the type
           * @aparam value the string value for the literal
           */
        public:
          LiteralString(::std::shared_ptr<const types::StringType> type, ValueType value);

          /** Destructor */
        public:
          ~LiteralString();

          /**
           * Create an bit literal
           * @param value true or false
           * @return a literal
           */
        public:
          static LiteralStringPtr create(::std::shared_ptr<const types::StringType> type,
              ValueType value);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /** The Int64 value */
        public:
          const ValueType value;
        };
      }
    }
  }
}
#endif
