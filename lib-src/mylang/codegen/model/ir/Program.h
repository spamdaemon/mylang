#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_PROGRAM_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_PROGRAM_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_GLOBALVALUE_H
#include <mylang/codegen/model/ir/GlobalValue.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_PROCESSDECL_H
#include <mylang/codegen/model/ir/ProcessDecl.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_NAMEDTYPE_H
#include <mylang/codegen/model/types/NamedType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * A variable declaration. Variables are mutable!
         */
        class Program: public Node
        {
          Program(const Program &e) = delete;
          Program& operator=(const Program &e) = delete;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const Program> ProgramPtr;

          /** Values */
        public:
          typedef ::std::vector<GlobalValue::GlobalValuePtr> Globals;

          /** The processes */
        public:
          typedef ::std::vector<ProcessDecl::ProcessDeclPtr> Processes;

          /** The named types */
        public:
          typedef ::std::vector<::std::shared_ptr<const mylang::codegen::model::types::NamedType>> NamedTypes;

          /**
           * Create a program
           * @param functions the global functions
           * @param values the global values
           * @param processes the defined processes
           */
        public:
          Program(Globals globals, Processes processes, NamedTypes types);

          /** Destructor */
        public:
          ~Program();

          /**
           * Create a program
           * @param functions the global functions
           * @param values the global values
           * @param processes the defined processes
           */
        public:
          static ProgramPtr create(Globals globals, Processes processes, NamedTypes types);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /** Global values, functions, and procedures  */
        public:
          const Globals globals;

          /** The process constructors */
        public:
          const Processes processes;

          /** The named types */
        public:
          const NamedTypes types;
        };
      }

    }
  }
}

#endif
