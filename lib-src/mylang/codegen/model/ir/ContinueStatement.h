#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_CONTINUESTATEMENT_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_CONTINUESTATEMENT_H

#ifndef MYLANGCLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENT_H
#include <mylang/codegen/model/ir/Statement.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * Assign a value to a variable that is in scope.
         */
        class ContinueStatement: public Statement
        {
          ContinueStatement(const ContinueStatement &e) = delete;

          ContinueStatement& operator=(const ContinueStatement &e) = delete;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const ContinueStatement> ContinueStatementPtr;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const mylang::names::Name> NamePtr;

          /**
           * Create an Breakion statement.
           * @param scope the name of hte scope to exit (e.g. a loop name)
           */
        public:
          ContinueStatement(NamePtr scope);

          /** Destructor */
        public:
          ~ContinueStatement();

          /**
           * Create a new break statement
           * @param scope the name of hte scope to exit (e.g. a loop name)
           */
        public:
          static ContinueStatementPtr create(NamePtr scope);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /** The name of the scope to exit */
        public:
          const NamePtr scope;
        };
      }
    }
  }
}
#endif
