#include <mylang/codegen/model/ir/LiteralChar.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        LiteralChar::LiteralChar(::std::shared_ptr<const types::CharType> xtype, ValueType xvalue)
            : LiteralPrimitive(xtype, xvalue), value(xvalue)
        {
        }

        LiteralChar::~LiteralChar()
        {
        }

        LiteralChar::LiteralCharPtr LiteralChar::create(
            ::std::shared_ptr<const types::CharType> xtype, ValueType xvalue)
        {
          return ::std::make_shared<LiteralChar>(xtype, xvalue);
        }

        void LiteralChar::accept(NodeVisitor &visitor) const
        {
          visitor.visitLiteralChar(self<LiteralChar>());
        }

      }
    }
  }
}
