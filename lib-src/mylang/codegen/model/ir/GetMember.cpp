#include <mylang/codegen/model/ir/GetMember.h>
#include <mylang/codegen/model/ir/Node.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <algorithm>
#include <memory>
#include <string>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        GetMember::GetMember(TypePtr t, Variable::VariablePtr obj, ::std::string xname)
            : Expression(::std::move(t)), object(::std::move(obj)), name(::std::move(xname))
        {
        }

        GetMember::~GetMember()
        {
        }

        GetMember::GetMemberPtr GetMember::create(TypePtr t, Variable::VariablePtr obj,
            ::std::string xname)
        {
          return ::std::make_shared<GetMember>(t, obj, xname);
        }

        void GetMember::accept(NodeVisitor &visitor) const
        {
          visitor.visitGetMember(self<GetMember>());
        }

      }

    }
  }
}
