#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENTBLOCK_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENTBLOCK_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENT_H
#include <mylang/codegen/model/ir/Statement.h>
#endif

#include <vector>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * A variable declaration that cannot be modified.
         */
        class StatementBlock: public Statement
        {
          StatementBlock(const StatementBlock &e) = delete;

          StatementBlock& operator=(const StatementBlock &e) = delete;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const StatementBlock> StatementBlockPtr;

          /** A list of statements */
        public:
          typedef ::std::vector<Statement::StatementPtr> Statements;

          /**
           * Create a statement block
           * @param statements the statements in the block
           */
        public:
          StatementBlock(Statements statements);

          /** Destructor */
        public:
          ~StatementBlock();

          /**
           * Create an empty statement block.
           * @return a statement block
           */
        public:
          static StatementBlockPtr create();

          /**
           * Create a statement block containing the specified statements. If
           * there is only a single statement, and it is a StatementBlock, then that statement block
           * is returned and new one is not created!
           * @param statements the statements in the block
           * @return a statement block
           */
        public:
          static StatementBlockPtr create(Statements statements);

          /**
           * Declare a mutable variable with the specified initializer
           * @param statement the single statement in the block
           * @return a statement block
           */
        public:
          static StatementBlockPtr create(StatementPtr statement);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /**
           * The name and type of the local variable that was introduced.
           */
        public:
          const Statements statements;
        };

      }
    }
  }
}
#endif
