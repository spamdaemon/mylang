#include <mylang/codegen/model/ir/LiteralUnion.h>
#include <mylang/codegen/model/ir/Node.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <memory>
#include <string>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        LiteralUnion::LiteralUnion(::std::shared_ptr<const types::UnionType> xtype,
            const ::std::string &xmember, LiteralPtr xvalue)
            : Literal(xtype), member(xmember), value(xvalue)
        {
        }

        LiteralUnion::~LiteralUnion()
        {
        }

        LiteralUnion::LiteralUnionPtr LiteralUnion::create(
            ::std::shared_ptr<const types::UnionType> xtype, const ::std::string &xmember,
            LiteralPtr xvalue)
        {
          return ::std::make_shared<LiteralUnion>(xtype, xmember, xvalue);
        }

        void LiteralUnion::accept(NodeVisitor &visitor) const
        {
          visitor.visitLiteralUnion(self<LiteralUnion>());
        }
        bool LiteralUnion::isSameLiteral(const Literal &other) const
        {
          const LiteralUnion *that = dynamic_cast<const LiteralUnion*>(&other);
          return that && type->isSameType(*that->type) && member == that->member
              && value->isSameLiteral(*that->value);
        }

      }
    }
  }
}
