#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_OPERATOREXPRESSION_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_OPERATOREXPRESSION_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#include <mylang/codegen/model/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#include <mylang/codegen/model/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_TYPE_H
#include <mylang/codegen/model/types/Type.h>
#endif
#include <vector>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        class OperatorExpression: public Expression
        {
          /** The operator */
        public:
          enum Operator
          {
            OP_NEW, // the operator to allocate a new object
            OP_CALL, // call a function

            // logging support
            OP_IS_LOGGABLE,

            // Type conversion
            OP_CHECKED_CAST, // cast an expression
            OP_UNCHECKED_CAST, // cast an expression

            OP_BASETYPE_CAST, // convert object to its basetype
            OP_ROOTTYPE_CAST, // convert object to its root type (can be implemented in ters of basetype cast, but that may be too inefficient)

            OP_BYTE_TO_UINT8, // treat a byte an unsigned int8
            OP_BYTE_TO_SINT8, // treat a byte as a signed int8

            // Numeric/Arithmetic operations
            OP_NEG, // the negation operator on numbers
            OP_ADD, // the plus operator on numbers
            OP_SUB, // the minus operator on numbers
            OP_MUL, // the multiplication operator on numbers
            OP_DIV, // the division operator on numbers
            OP_MOD, // the modulus operator on numbers
            OP_REM, // the remainder operator on numbers
            OP_CLAMP, // clamp the result to a range
            OP_WRAP, // wrap the result into a range

            // operations on array-like objects
            OP_INDEX, // indexing into an array or string
            OP_SUBRANGE, // subrange into an array or string
            OP_SIZE, // length of an array
            OP_CONCATENATE,

            // operations on list-like objects
            OP_HEAD,
            OP_TAIL,

            // comparisons on primitives
            OP_EQ, // comparing two values for equality
            OP_NEQ, // comparing two values for inequality
            OP_LT, // comparing two values for ordering LessThan
            OP_LTE, // comparing two values for ordering LessThanOrEqual

            // operations on primitive boolean-like objects (e.g. byte, boolean, bit, bit arrays)
            OP_AND, // the bitwise/logical AND operator &
            OP_OR,  // the bitwise/logical OR operator |
            OP_XOR,  // the bitwise/logical XOR operator ^
            OP_NOT,  // the bitwise NOT operator ~

            // operations on optionals
            OP_GET, // get value of an optional
            OP_IS_PRESENT, // true if an optional is present

            // operations on port
            OP_IS_INPUT_NEW, // true if input is new
            OP_IS_INPUT_CLOSED, // true if input port is closed
            OP_IS_OUTPUT_CLOSED, // ture if output port is closed
            OP_IS_PORT_READABLE, // true if a port can be read without block
            OP_IS_PORT_WRITABLE, // true if a port can be written without blocking
            OP_READ_PORT, // read from a port
            OP_BLOCK_READ,
            OP_BLOCK_WRITE,
            OP_CLEAR_PORT,
            OP_CLOSE_PORT,
            OP_WAIT_PORT,
            OP_WRITE_PORT,
            OP_LOG,
            OP_SET,

            // building arrays
            OP_APPEND,
            OP_BUILD,

            // convert primitive from/to an array of bits/bytes
            OP_FROM_BITS,
            OP_TO_BITS,
            OP_FROM_BYTES,
            OP_TO_BYTES,

            // string operations
            OP_STRING_TO_CHARS,

            // convert a primitive into a literal
            OP_TO_STRING,

            // the number of operators
            NUM_OPERATORS
          };

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const OperatorExpression> OperatorExpressionPtr;

          /** A type pointer */
        public:
          typedef Variable::VariablePtr VariablePtr;

          /** An expression list */
        public:
          typedef ::std::vector<VariablePtr> Expressions;

          /**
           * Create an expression with the specified type.
           * @param type the type
           * @param op the builtin function
           * @param args the arguments
           */
        public:
          OperatorExpression(TypePtr t, Operator op, Expressions args);

          /** destructor */
        public:
          ~OperatorExpression();

          /**
           * Create an expression.
           * @param type the type
           * @param op the builtin function
           */
        public:
          static OperatorExpressionPtr create(TypePtr t, Operator op);

          /**
           * Create a unary operator expression.
           * @param type the type
           * @param op the builtin function
           * @param arg an argument
           */
        public:
          static OperatorExpressionPtr create(TypePtr t, Operator op, Expressions args);

          /**
           * Create a unary operator expression.
           * @param type the type
           * @param op the builtin function
           * @param arg an argument
           */
        public:
          static OperatorExpressionPtr create(TypePtr t, Operator op, VariablePtr arg);

          /**
           * Create an expression with the specified type.
           * @param type the type
           * @param op the builtin function
           * @param left the left argument
           * @param right the right argument
           * @return a builtin call
           */
        public:
          static OperatorExpressionPtr create(TypePtr t, Operator op, VariablePtr left,
              VariablePtr right);

          /**
           * Create an expression with the specified type.
           * @param type the type
           * @param op the builtin function
           * @param arg1 the first argument
           * @param arg2 the second argument
           * @param arg3 the third argument
           * @return a builtin call
           */
        public:
          static OperatorExpressionPtr create(TypePtr t, Operator op, VariablePtr arg1,
              VariablePtr arg2, VariablePtr arg3);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /**
           * Perform a type-check on this expression.
           * @throws ::std::exception if the type-check fails
           */
        public:
          void typecheck() const;

          /** The builtin name */
        public:
          const Operator name;

          /** The function arguments */
        public:
          const Expressions arguments;
        };
      }
    }
  }
}
#endif
