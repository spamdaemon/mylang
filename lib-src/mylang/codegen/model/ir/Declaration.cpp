#include <mylang/codegen/model/ir/Declaration.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        Declaration::Declaration(Scope xscope, NamePtr xname, TypePtr xtype)
            : scope(xscope), name(xname), type(xtype)
        {
        }

        Declaration::~Declaration()
        {
        }

      }
    }
  }
}
