#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALINTEGER_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALINTEGER_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALPRIMITIVE_H
#include <mylang/codegen/model/ir/LiteralPrimitive.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_INTEGERTYPE_H
#include <mylang/codegen/model/types/IntegerType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * This class represent the constructor for a literal.
         */
        class LiteralInteger: public LiteralPrimitive
        {
          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const LiteralInteger> LiteralIntegerPtr;

          /** The value type */
        public:
          typedef BigInt ValueType;

          /**
           * Create an expression that returns a literal value.
           * @param type the type
           * @aparam value the string value for the literal
           */
        public:
          LiteralInteger(::std::shared_ptr<const types::IntegerType> type, ValueType value);

          /** Destructor */
        public:
          ~LiteralInteger();

          /**
           * Create an bit literal
           * @param value true or false
           * @return a literal
           */
        public:
          static LiteralIntegerPtr create(::std::shared_ptr<const types::IntegerType> type,
              ValueType value);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /** The Int64 value */
        public:
          const ValueType value;
        };
      }
    }
  }
}
#endif
