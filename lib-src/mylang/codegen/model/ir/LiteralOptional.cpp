#include <mylang/codegen/model/ir/LiteralOptional.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        LiteralOptional::LiteralOptional(::std::shared_ptr<const types::OptType> xtype,
            LiteralPtr xvalue)
            : Literal(xtype), value(xvalue)
        {
          assert(
              (xvalue == nullptr || xtype->element->isSameType(*value->type))
                  && "OptType mismatch");
        }

        LiteralOptional::~LiteralOptional()
        {
        }

        LiteralOptional::LiteralOptionalPtr LiteralOptional::create(
            ::std::shared_ptr<const types::OptType> xtype, LiteralPtr xvalue)
        {
          return ::std::make_shared<LiteralOptional>(xtype, xvalue);
        }

        LiteralOptional::LiteralOptionalPtr LiteralOptional::create(
            ::std::shared_ptr<const types::OptType> xtype)
        {
          return create(xtype, nullptr);
        }

        void LiteralOptional::accept(NodeVisitor &visitor) const
        {
          visitor.visitLiteralOptional(self<LiteralOptional>());
        }

        bool LiteralOptional::isSameLiteral(const Literal &other) const
        {
          const LiteralOptional *that = dynamic_cast<const LiteralOptional*>(&other);
          if (that && type->isSameType(*that->type)) {
            if (value == that->value) {
              return true;
            }
            if (value && that->value && value->isSameLiteral(*that->value)) {
              return true;
            }
          }
          return false;
        }
      }
    }
  }
}
