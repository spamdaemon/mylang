#include <mylang/codegen/model/ir/NewUnion.h>
#include <mylang/codegen/model/ir/Node.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        NewUnion::NewUnion(TypePtr t, ::std::string xmember, Variable::VariablePtr xvalue)
            : Expression(::std::move(t)), member(xmember), value(xvalue)
        {
          assert(value && "Missing value");
        }

        NewUnion::~NewUnion()
        {
        }

        NewUnion::NewUnionPtr NewUnion::create(TypePtr t, ::std::string xmember,
            Variable::VariablePtr xvalue)
        {
          return ::std::make_shared<NewUnion>(t, xmember, xvalue);
        }

        void NewUnion::accept(NodeVisitor &visitor) const
        {
          visitor.visitNewUnion(self<NewUnion>());
        }

      }

    }
  }
}
