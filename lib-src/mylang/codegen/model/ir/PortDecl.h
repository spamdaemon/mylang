#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_PORTDECL_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_PORTDECL_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_DECLARATION_H
#include <mylang/codegen/model/ir/Declaration.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#include <mylang/codegen/model/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_TYPE_H
#include <mylang/codegen/model/types/Type.h>
#endif

#ifndef CLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {
        class PortDecl: public Declaration
        {
          PortDecl(const PortDecl &e) = delete;

          PortDecl& operator=(const PortDecl &e) = delete;

          /** An expression pointer */
        public:
          typedef ::std::shared_ptr<const PortDecl> PortDeclPtr;

          /**
           * Create a declaration of that associates a type with a name.
           * @param name the name of the declaration
           * @param type the type
           */
        public:
          PortDecl(Variable::VariablePtr val, ::std::string xpublicName);

          /** Destructor */
        public:
          virtual ~PortDecl() = 0;

          /** The variable that can be used to reference the port */
        public:
          const Variable::VariablePtr variable;

          /** The name of the port as part of a process type */
        public:
          const ::std::string publicName;
        };
      }
    }
  }
}
#endif
