#include <mylang/codegen/model/ir/Node.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <mylang/codegen/model/ir/OperatorExpression.h>
#include <mylang/codegen/model/ir/Variable.h>
#include <algorithm>
#include <cassert>
#include <memory>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        namespace {
          enum TypeClass
          {
            // simple types
            BIT_TYPE,
            BOOLEAN_TYPE,
            BYTE_TYPE,
            CHAR_tyhpe,
            FUNCTION_TYPE,
            INTEGER_TYPE,
            REAL_TYPE,
            // composite types
            ARRAY_TYPE,
            NAMED_TYPE,
            OPT_TYPE,
            STRUCT_TYPE,
            TUPLE_TYPE,
            UNION_TYPE,
            // special types
            PROCESS_TYPE,
            INPUT_TYPE,
            OUTPUT_TYPE,

            NUM_TYPES
          // last entry to count number of entries
          };

          struct TypeCheckTable
          {
            TypeCheckTable()
            {
              for (int i = 0; i < OperatorExpression::NUM_OPERATORS; ++i) {
                for (int j = 0; j < NUM_TYPES; ++j) {
                  allowed[i][j] = false;
                }
              }
            }

            bool allowed[OperatorExpression::NUM_OPERATORS][NUM_TYPES];
          };

          static TypeCheckTable createUnaryOps()
          {
            TypeCheckTable ops;
            return ops;
          }

          static TypeCheckTable createBinaryOps()
          {
            TypeCheckTable ops;
            return ops;
          }

        }

        OperatorExpression::OperatorExpression(TypePtr t, Operator xname, Expressions args)
            : Expression(::std::move(t)), name(xname), arguments(::std::move(args))
        {
          for (auto arg : arguments) {
            assert(arg && "Argument is nullptr");
          }
        }

        OperatorExpression::~OperatorExpression()
        {
        }

        OperatorExpression::OperatorExpressionPtr OperatorExpression::create(TypePtr t, Operator op,
            Expressions args)
        {
          return ::std::make_shared<OperatorExpression>(t, op, args);
        }

        OperatorExpression::OperatorExpressionPtr OperatorExpression::create(TypePtr t, Operator op,
            VariablePtr arg)
        {
          Expressions args( { arg });
          return create(t, op, args);
        }

        OperatorExpression::OperatorExpressionPtr OperatorExpression::create(TypePtr t, Operator op)
        {
          Expressions args;
          return create(t, op, args);
        }

        OperatorExpression::OperatorExpressionPtr OperatorExpression::create(TypePtr t, Operator op,
            VariablePtr left, VariablePtr right)
        {
          return create(t, op, { left, right });
        }

        OperatorExpression::OperatorExpressionPtr OperatorExpression::create(TypePtr t, Operator op,
            VariablePtr arg1, VariablePtr arg2, VariablePtr arg3)
        {
          return create(t, op, { arg1, arg2, arg3 });
        }

        void OperatorExpression::accept(NodeVisitor &visitor) const
        {
          visitor.visitOperatorExpression(self<OperatorExpression>());
        }

        void OperatorExpression::typecheck() const
        {

        }
      }
    }
  }
}
