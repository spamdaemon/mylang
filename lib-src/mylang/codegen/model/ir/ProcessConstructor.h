#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_PROCESSCONSTRUCTOR_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_PROCESSCONSTRUCTOR_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_PARAMETER_H
#include <mylang/codegen/model/ir/Parameter.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENTBLOCK_H
#include <mylang/codegen/model/ir/StatementBlock.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_OWNCONSTRUCTORCALL_H
#include <mylang/codegen/model/ir/OwnConstructorCall.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * A variable declaration. Variables are mutable!
         */
        class ProcessConstructor: public Statement
        {
          ProcessConstructor(const ProcessConstructor &e) = delete;

          ProcessConstructor& operator=(const ProcessConstructor &e) = delete;

          /** The parameters */
        public:
          typedef ::std::vector<Parameter::ParameterPtr> Parameters;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const ProcessConstructor> ProcessConstructorPtr;

          /**
           * Create a bind statement.
           * @param name a name
           * @param value the value to be bound
           */
        public:
          ProcessConstructor(Parameters parameters,
              OwnConstructorCall::OwnConstructorCallPtr fwdCtor,
              StatementBlock::StatementBlockPtr body);

          /** Destructor */
        public:
          ~ProcessConstructor();

          /**
           * Declare a mutable variable with the specified initializer
           * @param name a name
           * @param value the value to be bound
           */
        public:
          static ProcessConstructorPtr create(Parameters parameters,
              OwnConstructorCall::OwnConstructorCallPtr fwdCtor,
              StatementBlock::StatementBlockPtr body);

          /**
           * Declare a mutable variable with the specified initializer
           * @param name a name
           * @param value the value to be bound
           */
        public:
          static ProcessConstructorPtr createDefaultConstructor();

        public:
          void accept(NodeVisitor &visitor) const override final;

          /** The parameters */
        public:
          const Parameters parameters;

          /** A call to another constructor (optional) before processing the body */
        public:
          const OwnConstructorCall::OwnConstructorCallPtr callConstructor;

          /**
           * The body
           */
        public:
          const StatementBlock::StatementBlockPtr body;
        };
      }

    }
  }
}
#endif
