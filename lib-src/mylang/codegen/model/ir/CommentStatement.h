#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_COMMENTSTATEMENT_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_COMMENTSTATEMENT_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENT_H
#include <mylang/codegen/model/ir/Statement.h>
#endif

#include <string>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * A variable declaration that cannot be modified.
         */
        class CommentStatement: public Statement
        {
          CommentStatement(const CommentStatement &e) = delete;

          CommentStatement& operator=(const CommentStatement &e) = delete;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const CommentStatement> CommentStatementPtr;

          /**
           * Create a statement block
           * @param statements the statements in the block
           */
        public:
          CommentStatement(::std::string comment);

          /** Destructor */
        public:
          ~CommentStatement();

          /**
           * Declare a mutable variable with the specified initializer
           * @param statement the single statement in the block
           * @return a statement block
           */
        public:
          static CommentStatementPtr create(::std::string comment);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /**
           * The name and type of the local variable that was introduced.
           */
        public:
          const ::std::string comment;
        };

      }
    }
  }
}
#endif
