#include <mylang/codegen/model/ir/LiteralInteger.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        LiteralInteger::LiteralInteger(::std::shared_ptr<const types::IntegerType> xtype,
            ValueType xvalue)
            : LiteralPrimitive(xtype, xvalue.toString()), value(xvalue)
        {
        }

        LiteralInteger::~LiteralInteger()
        {
        }

        LiteralInteger::LiteralIntegerPtr LiteralInteger::create(
            ::std::shared_ptr<const types::IntegerType> xtype, ValueType xvalue)
        {
          return ::std::make_shared<LiteralInteger>(xtype, xvalue);
        }

        void LiteralInteger::accept(NodeVisitor &visitor) const
        {
          visitor.visitLiteralInteger(self<LiteralInteger>());
        }

      }
    }
  }
}
