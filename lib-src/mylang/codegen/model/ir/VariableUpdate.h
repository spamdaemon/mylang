#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLEUPDATE_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLEUPDATE_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENT_H
#include <mylang/codegen/model/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#include <mylang/codegen/model/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#include <mylang/codegen/model/ir/Variable.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * Assign a value to a variable that is in scope.
         */
        class VariableUpdate: public Statement
        {
          VariableUpdate(const VariableUpdate &e) = delete;

          VariableUpdate& operator=(const VariableUpdate &e) = delete;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const VariableUpdate> VariableUpdatePtr;

          /**
           * Crea
           * @param name a name
           * @param value the value to be bound
           */
        public:
          VariableUpdate(Variable::VariablePtr target, Variable::VariablePtr value);

          /** Destructor */
        public:
          ~VariableUpdate();

          /**
           * Updateare a mutable variable with the specified initializer
           * @param name a name
           * @param value the value to be bound
           */
        public:
          static VariableUpdatePtr create(Variable::VariablePtr target,
              Variable::VariablePtr value);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /**
           * The name and type of the local variable that was introduced.
           */
        public:
          const Variable::VariablePtr target;

          /**
           * The bound value (may be null!)
           */
        public:
          const Variable::VariablePtr value;
        };
      }
    }
  }
}
#endif
