#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_ABORTSTATEMENT_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_ABORTSTATEMENT_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENT_H
#include <mylang/codegen/model/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#include <mylang/codegen/model/ir/Variable.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * Assign a value to a variable that is in scope.
         */
        class AbortStatement: public Statement
        {
          AbortStatement(const AbortStatement &e) = delete;

          AbortStatement& operator=(const AbortStatement &e) = delete;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const AbortStatement> AbortStatementPtr;

          /**
           * Crea
           * @param name a name
           * @param value the value to be bound
           */
        public:
          AbortStatement(Variable::VariablePtr value);

          /** Destructor */
        public:
          ~AbortStatement();

          /**
           * Updateare a mutable variable with the specified initializer
           * @param message a message to printed
           */
        public:
          static AbortStatementPtr create(Variable::VariablePtr value);

          /**
           * Updateare a mutable variable with the specified initializer
           */
        public:
          static AbortStatementPtr create();

        public:
          void accept(NodeVisitor &visitor) const override final;

          /**
           * The bound value (may be null!)
           */
        public:
          const Variable::VariablePtr value;
        };
      }
    }
  }
}
#endif
