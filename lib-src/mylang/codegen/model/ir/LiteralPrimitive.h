#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALPRIMITIVE_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALPRIMITIVE_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERAL_H
#include <mylang/codegen/model/ir/Literal.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_TYPE_H
#include <mylang/codegen/model/types/Type.h>
#endif

#ifndef CLASS_MYLANG_BIGINT_H
#include <mylang/BigInt.h>
#endif

#ifndef CLASS_MYLANG_BIGUINT_H
#include <mylang/BigUInt.h>
#endif

#ifndef CLASS_MYLANG_BIGREAL_H
#include <mylang/BigReal.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * This class represent the constructor for a literal.
         */
        class LiteralPrimitive: public Literal
        {
          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const LiteralPrimitive> LiteralPrimitivePtr;

          /**
           * Create an expression that returns a literal value.
           * @param type the type
           * @aparam value the string value for the literal
           */
        public:
          LiteralPrimitive(TypePtr t, ::std::string text);

          /** Destructor */
        public:
          virtual ~LiteralPrimitive() = 0;

        public:
          bool isSameLiteral(const Literal &that) const override;

          /** The text representation for the value */
        public:
          const ::std::string text;
        };
      }
    }
  }
}
#endif
