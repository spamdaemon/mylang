#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERAL_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_LITERAL_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_EXPRESSION_H
#include <mylang/codegen/model/ir/Expression.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_TYPE_H
#include <mylang/codegen/model/types/Type.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * This class represent the constructor for a literal.
         */
        class Literal: public Expression
        {
          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const Literal> LiteralPtr;

          /**
           * Create an expression that returns a literal value.
           * @param type the type
           */
        public:
          Literal(TypePtr t);

          /** Destructor */
        public:
          virtual ~Literal() = 0;

          /**
           * Determine if two literals are the same
           * @param a a literal
           * @param b a literal
           * @return true if the literals are the same
           */
        public:
          inline friend bool operator==(const Literal &a, const Literal &b)
          {
            return a.isSameLiteral(b);
          }

          /**
           * Determine if two literals are the same. This method uses the text value
           * to determine value equality
           * @param that another literal
           */
        public:
          virtual bool isSameLiteral(const Literal &that) const = 0;
        };
      }
    }
  }
}
#endif
