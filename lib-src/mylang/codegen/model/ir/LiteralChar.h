#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALCHAR_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALCHAR_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALPRIMITIVE_H
#include <mylang/codegen/model/ir/LiteralPrimitive.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_CHARTYPE_H
#include <mylang/codegen/model/types/CharType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * This class represent the constructor for a literal.
         */
        class LiteralChar: public LiteralPrimitive
        {
          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const LiteralChar> LiteralCharPtr;

          /** The value type as utf8 encoded string */
        public:
          typedef ::std::string ValueType;

          /**
           * Create an expression that returns a literal value.
           * @param type the type
           * @aparam value the string value for the literal
           */
        public:
          LiteralChar(::std::shared_ptr<const types::CharType> type, ValueType value);

          /** Destructor */
        public:
          ~LiteralChar();

          /**
           * Create an bit literal
           * @param value true or false
           * @return a literal
           */
        public:
          static LiteralCharPtr create(::std::shared_ptr<const types::CharType> type,
              ValueType value);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /** The bit value */
        public:
          const ValueType value;
        };
      }
    }
  }
}
#endif
