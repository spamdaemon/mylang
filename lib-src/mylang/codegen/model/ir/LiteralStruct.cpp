#include <mylang/codegen/model/ir/LiteralStruct.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        LiteralStruct::LiteralStruct(::std::shared_ptr<const types::StructType> xtype,
            const Members &xmembers)
            : Literal(xtype), members(xmembers)
        {
          assert(members.size() == xtype->members.size() && "Invalid literal struct");
        }

        LiteralStruct::~LiteralStruct()
        {
        }

        LiteralStruct::LiteralStructPtr LiteralStruct::create(
            ::std::shared_ptr<const types::StructType> xtype, const Members &xmembers)
        {
          return ::std::make_shared<LiteralStruct>(xtype, xmembers);
        }

        void LiteralStruct::accept(NodeVisitor &visitor) const
        {
          visitor.visitLiteralStruct(self<LiteralStruct>());
        }
        bool LiteralStruct::isSameLiteral(const Literal &other) const
        {
          const LiteralStruct *that = dynamic_cast<const LiteralStruct*>(&other);
          if (that == this) {
            return true;
          }
          if (that && type->isSameType(*that->type)) {
            for (size_t i = 0; i < members.size(); ++i) {
              if (!members.at(i)->isSameLiteral(*that->members.at(i))) {
                return false;
              }
            }
            return true;
          }
          return false;
        }

      }
    }
  }
}
