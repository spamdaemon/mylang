#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_NOVALUE_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_NOVALUE_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_EXPRESSION_H
#include <mylang/codegen/model/ir/Expression.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * An expression that represent the undefined constant.
         */
        class NoValue: public Expression
        {
          NoValue(const NoValue&) = delete;

          NoValue& operator=(const NoValue&) = delete;

          /** A value pointer */
        public:
          typedef ::std::shared_ptr<const NoValue> NoValuePtr;

          /**
           * Create a NoValue expression of the given type.
           */
        public:
          NoValue(TypePtr t);

          /** Destructor */
        public:
          ~NoValue();

          /**
           * Create an opaque expression.
           * @param e an expression
           * @return an opaque expression
           */
        public:
          static NoValuePtr create(TypePtr t);

        public:
          void accept(NodeVisitor &visitor) const override final;
        };
      }

    }
  }

}
#endif
