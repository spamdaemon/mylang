#include <mylang/codegen/model/ir/GetDiscriminant.h>
#include <mylang/codegen/model/ir/Node.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <algorithm>
#include <memory>
#include <string>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        GetDiscriminant::GetDiscriminant(TypePtr t, Variable::VariablePtr obj)
            : Expression(::std::move(t)), object(::std::move(obj))
        {
        }

        GetDiscriminant::~GetDiscriminant()
        {
        }

        GetDiscriminant::GetDiscriminantPtr GetDiscriminant::create(TypePtr t,
            Variable::VariablePtr obj)
        {
          return ::std::make_shared<GetDiscriminant>(t, obj);
        }

        void GetDiscriminant::accept(NodeVisitor &visitor) const
        {
          visitor.visitGetDiscriminant(self<GetDiscriminant>());
        }

      }
    }
  }
}
