#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_OUTPUTPORTDECL_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_OUTPUTPORTDECL_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_PORTDECL_H
#include <mylang/codegen/model/ir/PortDecl.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {
        class OutputPortDecl: public PortDecl
        {
          OutputPortDecl(const OutputPortDecl &e) = delete;

          OutputPortDecl& operator=(const OutputPortDecl &e) = delete;

          /** An expression pointer */
        public:
          typedef ::std::shared_ptr<const OutputPortDecl> OutputPortDeclPtr;

          /**
           * Create a declaration of that associates a type with a name.
           * @param name the name of the declaration
           * @param type the type
           */
        public:
          OutputPortDecl(Variable::VariablePtr t, ::std::string publicName);

          /** Destructor */
        public:
          ~OutputPortDecl();

          /**
           * Create a output port declaration.
           * @param name the name of the parameter
           * @param type the parameter type
           */
        public:
          static OutputPortDeclPtr create(Variable::VariablePtr val, ::std::string publicName);

        public:
          void accept(NodeVisitor &visitor) const override final;

        };
      }
    }
  }
}
#endif
