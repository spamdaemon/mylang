#include <mylang/codegen/model/ir/ValueDecl.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        ValueDecl::ValueDecl(Variable::VariablePtr xvar, Expression::ExpressionPtr xexpr)
            : Declaration(xvar->scope, xvar->name, xvar->type), variable(xvar), value(xexpr)
        {
          if (xexpr) {
            assert(xvar->type->isSameType(*xexpr->type) && "Internal error: type differ");
          }
        }

        ValueDecl::~ValueDecl()
        {
        }

        ValueDecl::ValueDeclPtr ValueDecl::create(Variable::VariablePtr xvar,
            Expression::ExpressionPtr xvalue)
        {
          return ::std::make_shared<ValueDecl>(xvar, xvalue);
        }

        void ValueDecl::accept(NodeVisitor &visitor) const
        {
          visitor.visitValueDecl(self<ValueDecl>());
        }

      }

    }
  }
}
