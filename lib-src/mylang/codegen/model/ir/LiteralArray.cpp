#include <mylang/codegen/model/ir/LiteralArray.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        LiteralArray::LiteralArray(::std::shared_ptr<const types::ArrayType> xtype,
            const Elements &xelements)
            : Literal(xtype), elements(xelements)
        {
        }

        LiteralArray::~LiteralArray()
        {
        }

        LiteralArray::LiteralArrayPtr LiteralArray::create(
            ::std::shared_ptr<const types::ArrayType> xtype, const Elements &xelements)
        {
          return ::std::make_shared<LiteralArray>(xtype, xelements);
        }

        void LiteralArray::accept(NodeVisitor &visitor) const
        {
          visitor.visitLiteralArray(self<LiteralArray>());
        }

        bool LiteralArray::isSameLiteral(const Literal &other) const
        {
          const LiteralArray *that = dynamic_cast<const LiteralArray*>(&other);
          if (that == this) {
            return true;
          }
          if (that && type->isSameType(*that->type) && that->elements.size() == elements.size()) {
            for (size_t i = 0; i < elements.size(); ++i) {
              if (!elements.at(i)->isSameLiteral(*that->elements.at(i))) {
                return false;
              }
            }
            return true;
          }
          return false;
        }

      }
    }
  }
}
