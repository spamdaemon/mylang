#include <mylang/codegen/model/ir/LiteralPrimitive.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        LiteralPrimitive::LiteralPrimitive(TypePtr xtype, ::std::string xtext)
            : Literal(::std::move(xtype)), text(xtext)
        {
        }

        LiteralPrimitive::~LiteralPrimitive()
        {
        }

        bool LiteralPrimitive::isSameLiteral(const Literal &other) const
        {
          const LiteralPrimitive *that = dynamic_cast<const LiteralPrimitive*>(&other);
          return that && text == that->text && type->isSameType(*that->type);
        }

      }
    }
  }
}
