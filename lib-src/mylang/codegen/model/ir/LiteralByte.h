#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALBYTE_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALBYTE_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALPRIMITIVE_H
#include <mylang/codegen/model/ir/LiteralPrimitive.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_BYTETYPE_H
#include <mylang/codegen/model/types/ByteType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * This class represent the constructor for a literal.
         */
        class LiteralByte: public LiteralPrimitive
        {
          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const LiteralByte> LiteralBytePtr;

          /** The value type */
        public:
          typedef unsigned char ValueType;

          /**
           * Create an expression that returns a literal value.
           * @param type the type
           * @aparam value the string value for the literal
           */
        public:
          LiteralByte(::std::shared_ptr<const types::ByteType> type, ValueType value);

          /** Destructor */
        public:
          ~LiteralByte();

          /**
           * Create an bit literal
           * @param value true or false
           * @return a literal
           */
        public:
          static LiteralBytePtr create(::std::shared_ptr<const types::ByteType> type,
              ValueType value);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /** The bit value */
        public:
          const ValueType value;
        };
      }
    }
  }
}
#endif
