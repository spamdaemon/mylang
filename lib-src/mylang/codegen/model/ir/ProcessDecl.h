#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_PROCESSDECL_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_PROCESSDECL_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_DECLARATION_H
#include <mylang/codegen/model/ir/Declaration.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_PORTDECL_H
#include <mylang/codegen/model/ir/PortDecl.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_PROCESSBLOCK_H
#include <mylang/codegen/model/ir/ProcessBlock.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_PROCESSCONSTRUCTOR_H
#include <mylang/codegen/model/ir/ProcessConstructor.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_PROCESSTYPE_H
#include <mylang/codegen/model/types/ProcessType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * A variable declaration. Variables are mutable!
         */
        class ProcessDecl: public Declaration
        {
          ProcessDecl(const ProcessDecl &e) = delete;

          ProcessDecl& operator=(const ProcessDecl &e) = delete;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const ProcessDecl> ProcessDeclPtr;

          /** The public known ports */
        public:
          typedef ::std::vector<PortDecl::PortDeclPtr> Ports;

          /** A type pointer */
        public:
          typedef ::std::vector<Declaration::DeclarationPtr> Declarations;

          /** A type pointer */
        public:
          typedef ::std::vector<ProcessConstructor::ProcessConstructorPtr> Constructors;

          /** A type pointer */
        public:
          typedef ::std::vector<ProcessBlock::ProcessBlockPtr> Blocks;

          /**
           * Create a process statement.
           * @param name a name
           * @param publicPorts the public ports
           * @parma decls process private declarations
           * @param blocks the processing block
           */
        public:
          ProcessDecl(Scope scope, NamePtr name, ::std::shared_ptr<const types::ProcessType> type,
              Ports publicPorts, Declarations decls, Constructors ctors, Blocks blocks);

          /** Destructor */
        public:
          ~ProcessDecl();

          /**
           * Declare a mutable variable with the specified initializer
           * @param name a name
           * @param publicPorts the public ports
           * @parma decls process private declarations
           * @param blocks the processing block
           */
        public:
          static ProcessDeclPtr create(Scope scope, NamePtr name,
              ::std::shared_ptr<const types::ProcessType> type, Ports publicPorts,
              Declarations decls, Constructors ctors, Blocks blocks);

          /**
           * Get the main block.
           * @return the single block called main, nullptr otherwise
           */
        public:
          ProcessBlock::ProcessBlockPtr getMainBlock() const;

          /**
           * Get the declared port.
           * @param portName the name of the port.
           * @return the port or null if not found
           */
        public:
          PortDecl::PortDeclPtr getDeclaredPort(const ::std::string &portName) const;

        public:
          void accept(NodeVisitor &visitor) const override final;

          /** The public input and output ports  */
        public:
          const Ports publicPorts;

          /** Declarations in the scope of the process */
        public:
          const Declarations declarations;

          /** The process constructors */
        public:
          const Constructors constructors;

          /**
           * The processing blocks
           */
        public:
          const Blocks blocks;
        };
      }

    }
  }
}
#endif
