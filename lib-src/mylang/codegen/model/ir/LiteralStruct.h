#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALSTRUCT_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALSTRUCT_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERAL_H
#include <mylang/codegen/model/ir/Literal.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_STRUCTTYPE_H
#include <mylang/codegen/model/types/StructType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * This class represent the constructor for a literal.
         */
        class LiteralStruct: public Literal
        {
          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const LiteralStruct> LiteralStructPtr;

          /** The elements */
        public:
          typedef ::std::vector<Literal::LiteralPtr> Members;

          /**
           * Create an expression that returns a literal value.
           * @param type the type
           * @aparam value the Struct value for the literal
           */
        public:
          LiteralStruct(::std::shared_ptr<const types::StructType> type, const Members &members);

          /** Destructor */
        public:
          ~LiteralStruct();

          /**
           * Create an bit literal
           * @param value true or false
           * @return a literal
           */
        public:
          static LiteralStructPtr create(::std::shared_ptr<const types::StructType> type,
              const Members &members);

        public:
          void accept(NodeVisitor &visitor) const override final;
          bool isSameLiteral(const Literal &that) const override;

          /** The struct memebers */
        public:
          const Members members;
        };
      }
    }
  }
}
#endif
