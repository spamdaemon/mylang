#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LOOPSTATEMENT_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_LOOPSTATEMENT_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENTBLOCK_H
#include <mylang/codegen/model/ir/StatementBlock.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#include <mylang/codegen/model/ir/Variable.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * A variable declaration. Variables are mutable!
         */
        class LoopStatement: public Statement
        {
          LoopStatement(const LoopStatement &e) = delete;

          LoopStatement& operator=(const LoopStatement &e) = delete;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const LoopStatement> LoopStatementPtr;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const mylang::names::Name> NamePtr;

          /**
           * Create an if-statment
           * @param a label for the loop
           * @param body body of the statement
           */
        public:
          LoopStatement(NamePtr name, StatementBlock::StatementBlockPtr body);

          /** Destructor */
        public:
          ~LoopStatement();

          /**
           * Declare a mutable variable with the specified initializer
           * @param cond the condition
           * @param body body of the statement
           */
        public:
          static LoopStatementPtr create(NamePtr name, StatementBlock::StatementBlockPtr body);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /**
           * The bound value (may be null!)
           */
        public:
          const NamePtr name;

          /**
           * The statement to be executed while condition is true
           */
        public:
          const StatementBlock::StatementBlockPtr body;
        };
      }

    }
  }
}
#endif
