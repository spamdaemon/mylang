#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_EXPRESSION_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_EXPRESSION_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_NODE_H
#include <mylang/codegen/model/ir/Node.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_TYPE_H
#include <mylang/codegen/model/types/Type.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {
        class Expression: public Node
        {
          Expression(const Expression &e) = delete;

          Expression& operator=(const Expression &e) = delete;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const mylang::codegen::model::types::Type> TypePtr;

          /** An expression pointer */
        public:
          typedef ::std::shared_ptr<const Expression> ExpressionPtr;

          /**
           * Create an expression with the specified type.
           * @param type the type
           */
        public:
          Expression(TypePtr t);

          /** Destructor */
        public:
          virtual ~Expression() = 0;

          /**
           * Get the type of this expression.
           * @return a type
           */
        public:
          const TypePtr type;
        };
      }
    }
  }
}
#endif
