#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H

#ifndef MYLANGCLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_DECLARATION_H
#include <mylang/codegen/model/ir/Declaration.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_EXPRESSION_H
#include <mylang/codegen/model/ir/Expression.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_TYPE_H
#include <mylang/codegen/model/types/Type.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {
        class Variable: public Expression
        {
          /** The scope of the variable's declaration */
        public:
          typedef Declaration::Scope Scope;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const Variable> VariablePtr;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const mylang::names::Name> NamePtr;

          /**
           * Create an expression gets the specified member variable.
           * @param type the return type
           * @param obj object on which to invoke the method name
           * @param name the name of the member to retrieve
           */
        public:
          Variable(Scope scope, TypePtr t, NamePtr name);

          /**destructor */
        public:
          ~Variable();

          /**
           * Create a function call.
           * @param type the return type
           * @param name the name of the function
           * @param args the arguments
           * @return afunction call ptr/
           */
        public:
          static VariablePtr create(Scope scope, TypePtr t, NamePtr name);

          void accept(NodeVisitor &visitor) const override final;

          /** The scope */
        public:
          const Scope scope;

          /** The function name */
        public:
          const NamePtr name;
        };
      }

    }
  }
}
#endif
