#include <mylang/codegen/model/ir/LiteralString.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        LiteralString::LiteralString(::std::shared_ptr<const types::StringType> xtype,
            ValueType xvalue)
            : LiteralPrimitive(xtype, xvalue), value(xvalue)
        {
        }

        LiteralString::~LiteralString()
        {
        }

        LiteralString::LiteralStringPtr LiteralString::create(
            ::std::shared_ptr<const types::StringType> xtype, ValueType xvalue)
        {
          return ::std::make_shared<LiteralString>(xtype, xvalue);
        }

        void LiteralString::accept(NodeVisitor &visitor) const
        {
          visitor.visitLiteralString(self<LiteralString>());
        }

      }
    }
  }
}
