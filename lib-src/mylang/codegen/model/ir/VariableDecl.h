#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLEDECL_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLEDECL_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_DECLARATION_H
#include <mylang/codegen/model/ir/Declaration.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_EXPRESSION_H
#include <mylang/codegen/model/ir/Expression.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#include <mylang/codegen/model/ir/Variable.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * A variable declaration. Variables are mutable!
         */
        class VariableDecl: public Declaration
        {
          VariableDecl(const VariableDecl&) = delete;

          VariableDecl& operator=(const VariableDecl&) = delete;

          /** A variable pointer */
        public:
          typedef ::std::shared_ptr<const VariableDecl> VariableDeclPtr;

          /**
           * Create a bind statement.
           * @param name a name
           * @param value the value to be bound
           */
        public:
          VariableDecl(Variable::VariablePtr var, Variable::VariablePtr value);

          /** Destructor */
        public:
          ~VariableDecl();

          /**
           * Declare a mutable variable with the specified initializer
           * @param name a name
           * @param value the value to be bound (maybe nullptr)
           */
        public:
          static VariableDeclPtr create(Variable::VariablePtr var, Variable::VariablePtr value);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /**
           * The name and type of the local variable that was introduced.
           */
        public:
          const Variable::VariablePtr variable;

          /**
           * The bound value (may be null!)
           */
        public:
          const Variable::VariablePtr value;
        };
      }

    }
  }
}
#endif
