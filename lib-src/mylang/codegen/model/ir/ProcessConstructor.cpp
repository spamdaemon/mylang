#include <mylang/codegen/model/ir/ProcessConstructor.h>
#include <mylang/codegen/model/ir/OwnConstructorCall.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        ProcessConstructor::ProcessConstructor(Parameters xparameters,
            OwnConstructorCall::OwnConstructorCallPtr fwdCtor,
            StatementBlock::StatementBlockPtr xbody)
            : parameters(xparameters), callConstructor(fwdCtor), body(xbody)
        {
        }

        ProcessConstructor::~ProcessConstructor()
        {
        }

        ProcessConstructor::ProcessConstructorPtr ProcessConstructor::create(Parameters xparameters,
            OwnConstructorCall::OwnConstructorCallPtr fwdCtor,
            StatementBlock::StatementBlockPtr xbody)
        {
          return ::std::make_shared<ProcessConstructor>(xparameters, fwdCtor, xbody);
        }

        ProcessConstructor::ProcessConstructorPtr ProcessConstructor::createDefaultConstructor()
        {
          return create( { }, nullptr, StatementBlock::create());
        }

        void ProcessConstructor::accept(NodeVisitor &visitor) const
        {
          visitor.visitProcessConstructor(self<ProcessConstructor>());
        }

      }

    }
  }
}
