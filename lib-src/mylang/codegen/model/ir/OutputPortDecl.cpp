#include <mylang/codegen/model/ir/OutputPortDecl.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        OutputPortDecl::OutputPortDecl(Variable::VariablePtr xval, ::std::string xpublicName)
            : PortDecl(xval, xpublicName)
        {
        }

        OutputPortDecl::~OutputPortDecl()
        {
        }

        OutputPortDecl::OutputPortDeclPtr OutputPortDecl::create(Variable::VariablePtr xval,
            ::std::string xpublicName)
        {
          return ::std::make_shared<OutputPortDecl>(xval, xpublicName);
        }

        void OutputPortDecl::accept(NodeVisitor &visitor) const
        {
          visitor.visitOutputPortDecl(self<OutputPortDecl>());
        }

      }
    }
  }
}
