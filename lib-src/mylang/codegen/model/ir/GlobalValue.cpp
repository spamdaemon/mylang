#include <mylang/codegen/model/ir/GlobalValue.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        GlobalValue::GlobalValue(NamePtr xname, ::std::optional<const ::mylang::names::FQN> eName,
            Expression::ExpressionPtr xexpr)
            : Declaration(GLOBAL_SCOPE, xname, xexpr->type), exportedName(::std::move(eName)),
                variable(Variable::create(GLOBAL_SCOPE, xexpr->type, xname)), value(xexpr)
        {
        }

        GlobalValue::~GlobalValue()
        {
        }

        GlobalValue::GlobalValuePtr GlobalValue::create(NamePtr xname,
            ::std::optional<const ::mylang::names::FQN> eName, Expression::ExpressionPtr xvalue)
        {
          return ::std::make_shared<GlobalValue>(xname, ::std::move(eName), xvalue);
        }

        void GlobalValue::accept(NodeVisitor &visitor) const
        {
          visitor.visitGlobalValue(self<GlobalValue>());
        }

      }

    }
  }
}
