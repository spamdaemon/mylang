#include <mylang/codegen/model/ir/BreakStatement.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        BreakStatement::BreakStatement(NamePtr xscope)
            : scope(xscope)
        {
        }

        BreakStatement::~BreakStatement()
        {
        }

        BreakStatement::BreakStatementPtr BreakStatement::create(NamePtr xscope)
        {
          return ::std::make_shared<BreakStatement>(xscope);
        }

        void BreakStatement::accept(NodeVisitor &visitor) const
        {
          visitor.visitBreakStatement(self<BreakStatement>());
        }

      }

    }
  }
}
