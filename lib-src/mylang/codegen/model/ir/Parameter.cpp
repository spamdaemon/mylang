#include <mylang/codegen/model/ir/Parameter.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        Parameter::Parameter(NamePtr xname, TypePtr xtype)
            : variable(Variable::create(ir::Declaration::FUNCTION_SCOPE, xtype, xname))
        {
        }

        Parameter::~Parameter()
        {
        }

        Parameter::ParameterPtr Parameter::create(NamePtr xname, TypePtr xtype)
        {
          return ::std::make_shared<Parameter>(xname, xtype);
        }

        void Parameter::accept(NodeVisitor &visitor) const
        {
          visitor.visitParameter(self<Parameter>());
        }

      }
    }
  }
}
