#include <mylang/codegen/model/ir/VariableDecl.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        VariableDecl::VariableDecl(Variable::VariablePtr xvar, Variable::VariablePtr xexpr)
            : Declaration(xvar->scope, xvar->name, xvar->type), variable(xvar), value(xexpr)
        {
          if (xexpr) {
            assert(xvar->type->isSameType(*xexpr->type) && "Internal error: type differ");
          }
        }

        VariableDecl::~VariableDecl()
        {
        }

        VariableDecl::VariableDeclPtr VariableDecl::create(Variable::VariablePtr xvar,
            Variable::VariablePtr xvalue)
        {
          return ::std::make_shared<VariableDecl>(xvar, xvalue);
        }

        void VariableDecl::accept(NodeVisitor &visitor) const
        {
          visitor.visitVariableDecl(self<VariableDecl>());
        }

      }

    }
  }
}
