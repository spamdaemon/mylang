#include <mylang/codegen/model/ir/NoValue.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        NoValue::NoValue(TypePtr t)
            : Expression(t)
        {
        }

        NoValue::~NoValue()
        {
        }

        NoValue::NoValuePtr NoValue::create(TypePtr t)
        {
          return ::std::make_shared<NoValue>(t);
        }

        void NoValue::accept(NodeVisitor &visitor) const
        {
          visitor.visitNoValue(self<NoValue>());
        }

      }

    }
  }
}
