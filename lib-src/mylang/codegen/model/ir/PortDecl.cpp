#include <mylang/codegen/model/ir/PortDecl.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        PortDecl::PortDecl(Variable::VariablePtr xval, ::std::string xpublicName)
            : Declaration(PROCESS_SCOPE, xval->name, xval->type), variable(xval),
                publicName(xpublicName)
        {
        }

        PortDecl::~PortDecl()
        {
        }

      }
    }
  }
}
