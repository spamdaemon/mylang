#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LAMBDAEXPRESSION_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_LAMBDAEXPRESSION_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_EXPRESSION_H
#include <mylang/codegen/model/ir/Expression.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#include <mylang/codegen/model/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_PARAMETER_H
#include <mylang/codegen/model/ir/Parameter.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENTBLOCK_H
#include <mylang/codegen/model/ir/StatementBlock.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_FUNCTIONTYPE_H
#include <mylang/codegen/model/types/FunctionType.h>
#endif

#include <memory>
#include <vector>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * A variable declaration. Variables are mutable!
         */
        class LambdaExpression: public Expression
        {
          LambdaExpression(const LambdaExpression &e) = delete;

          LambdaExpression& operator=(const LambdaExpression &e) = delete;

          /** The parameters */
        public:
          typedef ::std::vector<Parameter::ParameterPtr> Parameters;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const LambdaExpression> LambdaExpressionPtr;

          /**
           * Create a bind statement.
           * @param name a name
           * @param value the value to be bound
           */
        public:
          LambdaExpression(::std::shared_ptr<const types::FunctionType> type, Parameters parameters,
              StatementBlock::StatementBlockPtr body);

          /** Destructor */
        public:
          ~LambdaExpression();

          /**
           * Declare a mutable variable with the specified initializer
           * @param name a name
           * @param value the value to be bound
           */
        public:
          static LambdaExpressionPtr create(::std::shared_ptr<const types::FunctionType> type,
              Parameters parameters, StatementBlock::StatementBlockPtr body);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /** The parameters */
        public:
          const Parameters parameters;

          /**
           * The bound value (may be null!)
           */
        public:
          const StatementBlock::StatementBlockPtr body;
        };
      }

    }
  }
}
#endif
