#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VALUEDECL_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_VALUEDECL_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_DECLARATION_H
#include <mylang/codegen/model/ir/Declaration.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_EXPRESSION_H
#include <mylang/codegen/model/ir/Expression.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#include <mylang/codegen/model/ir/Variable.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * A variable declaration that cannot be modified.
         */
        class ValueDecl: public Declaration
        {
          ValueDecl(const ValueDecl&) = delete;

          ValueDecl& operator=(const ValueDecl&) = delete;

          /** A value pointer */
        public:
          typedef ::std::shared_ptr<const ValueDecl> ValueDeclPtr;

          /**
           * Create a bind statement.
           * @param name a name
           * @param value the value to be bound
           */
        public:
          ValueDecl(Variable::VariablePtr var, Expression::ExpressionPtr value);

          /** Destructor */
        public:
          ~ValueDecl();

          /**
           * Declare a mutable variable with the specified initializer
           * @param name a name
           * @param value the value to be bound (maybe nullptr)
           */
        public:
          static ValueDeclPtr create(Variable::VariablePtr var, Expression::ExpressionPtr value);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /**
           * The name and type of the local variable that was introduced.
           */
        public:
          const Variable::VariablePtr variable;

          /**
           * The bound value (never null)
           */
        public:
          const Expression::ExpressionPtr value;
        };
      }

    }
  }
}
#endif
