#include <mylang/codegen/model/ir/LoopStatement.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        LoopStatement::LoopStatement(NamePtr xname, StatementBlock::StatementBlockPtr xbody)
            : name(xname), body(xbody)
        {
        }

        LoopStatement::~LoopStatement()
        {
        }

        LoopStatement::LoopStatementPtr LoopStatement::create(NamePtr xname,
            StatementBlock::StatementBlockPtr xbody)
        {
          return ::std::make_shared<LoopStatement>(xname, xbody);
        }

        void LoopStatement::accept(NodeVisitor &visitor) const
        {
          visitor.visitLoopStatement(self<LoopStatement>());
        }

      }
    }
  }
}
