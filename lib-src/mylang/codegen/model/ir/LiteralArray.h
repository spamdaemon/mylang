#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALARRAY_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALARRAY_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERAL_H
#include <mylang/codegen/model/ir/Literal.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_ARRAYTYPE_H
#include <mylang/codegen/model/types/ArrayType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * This class represent the constructor for a literal.
         */
        class LiteralArray: public Literal
        {
          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const LiteralArray> LiteralArrayPtr;

          /** The elements */
        public:
          typedef ::std::vector<Literal::LiteralPtr> Elements;

          /**
           * Create an expression that returns a literal value.
           * @param type the type
           * @aparam elements the array elements
           */
        public:
          LiteralArray(::std::shared_ptr<const types::ArrayType> type, const Elements &elements);

          /** Destructor */
        public:
          ~LiteralArray();

          /**
           * Create an bit literal
           * @aparam elements the array elements
           * @return a literal
           */
        public:
          static LiteralArrayPtr create(::std::shared_ptr<const types::ArrayType> type,
              const Elements &elements);

        public:
          void accept(NodeVisitor &visitor) const override final;
          bool isSameLiteral(const Literal &that) const override;

          /** The elements */
        public:
          const Elements elements;
        };
      }
    }
  }
}
#endif
