#include <mylang/codegen/model/ir/LiteralBoolean.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        LiteralBoolean::LiteralBoolean(::std::shared_ptr<const types::BooleanType> xtype,
            ValueType xvalue)
            : LiteralPrimitive(xtype, xvalue ? "true" : "false"), value(xvalue)
        {
        }

        LiteralBoolean::~LiteralBoolean()
        {
        }

        LiteralBoolean::LiteralBooleanPtr LiteralBoolean::create(
            ::std::shared_ptr<const types::BooleanType> xtype, ValueType xvalue)
        {
          return ::std::make_shared<LiteralBoolean>(xtype, xvalue);
        }

        void LiteralBoolean::accept(NodeVisitor &visitor) const
        {
          visitor.visitLiteralBoolean(self<LiteralBoolean>());
        }

      }
    }
  }
}
