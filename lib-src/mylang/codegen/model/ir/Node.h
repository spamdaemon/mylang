#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_NODE_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_NODE_H
#include <memory>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        class NodeVisitor;

        class Node: public ::std::enable_shared_from_this<Node>
        {
          Node(const Node &e) = delete;

          Node& operator=(const Node &e) = delete;

          /** A pointer to a node */
        public:
          typedef ::std::shared_ptr<const Node> NodePtr;

          /** Constructor */
        public:
          Node();

          /** Destructor */
        public:
          virtual ~Node() = 0;

          /**
           * Accept the specified node visitor
           * @param v a visitor
           * @return a node pointer, or nullptr if the node has not changed
           */
        public:
          virtual void accept(NodeVisitor &visitor) const = 0;

          /**
           * Get this node casted to a derived node.
           * @return a pointer to this node or nullptr if not of the requested type
           */
        public:
          template<class T = Node>
          inline ::std::shared_ptr<const T> self() const
          {
            return ::std::dynamic_pointer_cast<const T>(shared_from_this());
          }

        };
      }
    }
  }
}
#endif
