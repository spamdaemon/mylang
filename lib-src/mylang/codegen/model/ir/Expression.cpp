#include <mylang/codegen/model/ir/Expression.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        Expression::Expression(TypePtr xtype)
            : type(::std::move(xtype))
        {
          if (!type) {
            throw ::std::runtime_error("Expression has no type");
          }
        }

        Expression::~Expression()
        {
        }

      }
    }
  }
}
