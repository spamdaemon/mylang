#include <mylang/codegen/model/ir/VariableUpdate.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        VariableUpdate::VariableUpdate(Variable::VariablePtr xtarget, Variable::VariablePtr xexpr)
            : target(xtarget), value(xexpr)
        {
        }

        VariableUpdate::~VariableUpdate()
        {
        }

        VariableUpdate::VariableUpdatePtr VariableUpdate::create(Variable::VariablePtr xtarget,
            Variable::VariablePtr xvalue)
        {
          return ::std::make_shared<VariableUpdate>(xtarget, xvalue);
        }

        void VariableUpdate::accept(NodeVisitor &visitor) const
        {
          visitor.visitVariableUpdate(self<VariableUpdate>());
        }

      }

    }
  }
}
