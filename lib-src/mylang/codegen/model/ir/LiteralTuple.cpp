#include <mylang/codegen/model/ir/LiteralTuple.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        LiteralTuple::LiteralTuple(::std::shared_ptr<const types::TupleType> xtype,
            const Elements &xelements)
            : Literal(xtype), elements(xelements)
        {
          assert(elements.size() == xtype->tuple.size() && "Invalid literal tuple");
        }

        LiteralTuple::~LiteralTuple()
        {
        }

        LiteralTuple::LiteralTuplePtr LiteralTuple::create(
            ::std::shared_ptr<const types::TupleType> xtype, const Elements &xelements)
        {
          return ::std::make_shared<LiteralTuple>(xtype, xelements);
        }

        void LiteralTuple::accept(NodeVisitor &visitor) const
        {
          visitor.visitLiteralTuple(self<LiteralTuple>());
        }
        bool LiteralTuple::isSameLiteral(const Literal &other) const
        {
          const LiteralTuple *that = dynamic_cast<const LiteralTuple*>(&other);
          if (that && type->isSameType(*that->type)) {
            for (size_t i = 0; i < elements.size(); ++i) {
              if (!elements.at(i)->isSameLiteral(*that->elements.at(i))) {
                return false;
              }
            }
            return true;
          }
          return false;
        }

      }
    }
  }
}
