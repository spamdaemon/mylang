#include <mylang/codegen/model/ir/ContinueStatement.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        ContinueStatement::ContinueStatement(NamePtr xscope)
            : scope(xscope)
        {
        }

        ContinueStatement::~ContinueStatement()
        {
        }

        ContinueStatement::ContinueStatementPtr ContinueStatement::create(NamePtr xscope)
        {
          return ::std::make_shared<ContinueStatement>(xscope);
        }

        void ContinueStatement::accept(NodeVisitor &visitor) const
        {
          visitor.visitContinueStatement(self<ContinueStatement>());
        }

      }

    }
  }
}
