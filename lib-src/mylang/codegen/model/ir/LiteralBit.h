#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALBIT_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALBIT_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_LITERALPRIMITIVE_H
#include <mylang/codegen/model/ir/LiteralPrimitive.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_BITTYPE_H
#include <mylang/codegen/model/types/BitType.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * This class represent the constructor for a literal.
         */
        class LiteralBit: public LiteralPrimitive
        {
          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const LiteralBit> LiteralBitPtr;

          /** The value type */
        public:
          typedef bool ValueType;

          /**
           * Create an expression that returns a literal value.
           * @param type the type
           * @aparam value the string value for the literal
           */
        public:
          LiteralBit(::std::shared_ptr<const types::BitType> type, ValueType value);

          /** Destructor */
        public:
          ~LiteralBit();

          /**
           * Create an bit literal
           * @param value true or false
           * @return a literal
           */
        public:
          static LiteralBitPtr create(::std::shared_ptr<const types::BitType> type,
              ValueType value);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /** The bit value */
        public:
          const ValueType value;
        };
      }
    }
  }
}
#endif
