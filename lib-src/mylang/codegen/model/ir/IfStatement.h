#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_IFSTATEMENT_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_IFSTATEMENT_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENTBLOCK_H
#include <mylang/codegen/model/ir/StatementBlock.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#include <mylang/codegen/model/ir/Variable.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * A variable declaration. Variables are mutable!
         */
        class IfStatement: public Statement
        {
          IfStatement(const IfStatement &e) = delete;

          IfStatement& operator=(const IfStatement &e) = delete;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const IfStatement> IfStatementPtr;

          /**
           * Create an if-statment
           * @param cond the condition
           * @param iftrue if-true branch
           * @param iffalse if-false branch
           */
        public:
          IfStatement(Variable::VariablePtr cond, StatementBlock::StatementBlockPtr iftrue,
              StatementBlock::StatementBlockPtr iffalse);

          /** Destructor */
        public:
          ~IfStatement();

          /**
           * Declare a mutable variable with the specified initializer
           * @param cond the condition
           * @param iftrue if-true branch
           * @param iffalse if-false branch
           */
        public:
          static IfStatementPtr create(Variable::VariablePtr cond,
              StatementBlock::StatementBlockPtr iftrue, StatementBlock::StatementBlockPtr iffalse);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /**
           * The bound value (may be null!)
           */
        public:
          const Variable::VariablePtr condition;

          /**
           * The statement to be executed if condition is true
           */
        public:
          const StatementBlock::StatementBlockPtr iftrue;

          /**
           * The statement to be executed if condition is false
           */
        public:
          const StatementBlock::StatementBlockPtr iffalse;
        };
      }

    }
  }
}
#endif
