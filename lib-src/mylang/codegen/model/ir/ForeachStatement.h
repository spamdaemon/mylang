#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_FOREACHSTATEMENT_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_FOREACHSTATEMENT_H

#ifndef FILE_MYLANG_CODEGEN_MODEL_H
#include <mylang/codegen/model/model.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENT_H
#include <mylang/codegen/model/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENTBLOCK_H
#include <mylang/codegen/model/ir/StatementBlock.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#include <mylang/codegen/model/ir/Variable.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * Read from a port into a variable.
         */
        class ForeachStatement: public Statement
        {
          ForeachStatement(const ForeachStatement &e) = delete;

          ForeachStatement& operator=(const ForeachStatement &e) = delete;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const ForeachStatement> ForeachStatementPtr;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const mylang::names::Name> NamePtr;

          /**
           * Create a foreach statement
           * @param loopvar the loop variable the variable to be defined
           * @param loopdata the data over which to loop
           * @param body the function body
           */
        public:
          ForeachStatement(NamePtr loopname, Variable::VariablePtr loopvar,
              Variable::VariablePtr loopdata, StatementBlock::StatementBlockPtr body);

          /** Destructor */
        public:
          ~ForeachStatement();

          /**
           * Create a read statement to read into the specified variable.
           * @param loopvar the loop variable the variable to be defined
           * @param loopdata the data over which to loop
           * @param body the function body
           * @return a foreach statement
           */
        public:
          static ForeachStatementPtr create(NamePtr loopname, Variable::VariablePtr loopvar,
              Variable::VariablePtr loopdata, StatementBlock::StatementBlockPtr body);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /**
           * The name of the loop statement, which is used in conjunection with a break statement
           */
        public:
          const NamePtr name;

          /**
           * The name and type of the local variable that was introduced.
           */
        public:
          const Variable::VariablePtr variable;

          /** The loop data */
        public:
          const Variable::VariablePtr data;

          /** The loop data */
        public:
          const StatementBlock::StatementBlockPtr body;
        };

      }
    }
  }
}
#endif
