#include <mylang/codegen/model/ir/LiteralReal.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        LiteralReal::LiteralReal(::std::shared_ptr<const types::RealType> xtype, ValueType xvalue)
            : LiteralPrimitive(xtype, xvalue.toString()), value(xvalue)
        {
        }

        LiteralReal::~LiteralReal()
        {
        }

        LiteralReal::LiteralRealPtr LiteralReal::create(
            ::std::shared_ptr<const types::RealType> xtype, ValueType xvalue)
        {
          return ::std::make_shared<LiteralReal>(xtype, xvalue);
        }

        void LiteralReal::accept(NodeVisitor &visitor) const
        {
          visitor.visitLiteralReal(self<LiteralReal>());
        }

      }
    }
  }
}
