#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_PARAMETER_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_PARAMETER_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_NODE_H
#include <mylang/codegen/model/ir/Node.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#include <mylang/codegen/model/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_TYPES_TYPE_H
#include <mylang/codegen/model/types/Type.h>
#endif

#ifndef CLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {
        class Parameter: public Node
        {
          Parameter(const Parameter &e) = delete;

          Parameter& operator=(const Parameter &e) = delete;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const mylang::codegen::model::types::Type> TypePtr;

          /** A name pointer */
        public:
          typedef ::std::shared_ptr<const mylang::names::Name> NamePtr;

          /** An expression pointer */
        public:
          typedef ::std::shared_ptr<const Parameter> ParameterPtr;

          /**
           * Create a declaration of that associates a type with a name.
           * @param name the name of the declaration
           * @param type the type
           */
        public:
          Parameter(NamePtr name, TypePtr t);

          /** Destructor */
        public:
          virtual ~Parameter();

          /**
           * Create a parameter.
           * @param name the name of the parameter
           * @param type the parameter type
           */
        public:
          static ParameterPtr create(NamePtr name, TypePtr type);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /** The variable that references this parameter */
        public:
          const Variable::VariablePtr variable;
        };
      }
    }
  }
}
#endif
