#include <mylang/codegen/model/ir/ReturnStatement.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        ReturnStatement::ReturnStatement(Variable::VariablePtr xexpr)
            : value(xexpr)
        {
        }

        ReturnStatement::~ReturnStatement()
        {
        }

        ReturnStatement::ReturnStatementPtr ReturnStatement::create()
        {
          return ::std::make_shared<ReturnStatement>(nullptr);
        }

        ReturnStatement::ReturnStatementPtr ReturnStatement::create(Variable::VariablePtr xvalue)
        {
          return ::std::make_shared<ReturnStatement>(xvalue);
        }

        void ReturnStatement::accept(NodeVisitor &visitor) const
        {
          visitor.visitReturnStatement(self<ReturnStatement>());
        }

      }
    }
  }
}
