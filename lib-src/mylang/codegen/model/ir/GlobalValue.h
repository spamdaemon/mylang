#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_GLOBALVALUE_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_GLOBALVALUE_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_DECLARATION_H
#include <mylang/codegen/model/ir/Declaration.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_EXPRESSION_H
#include <mylang/codegen/model/ir/Expression.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#include <mylang/codegen/model/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_NAMES_FQN_H
#include <mylang/names/FQN.h>
#endif

#include <optional>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * A variable declaration that cannot be modified.
         */
        class GlobalValue: public Declaration
        {
          GlobalValue(const GlobalValue&) = delete;

          GlobalValue& operator=(const GlobalValue&) = delete;

          /** A value pointer */
        public:
          typedef ::std::shared_ptr<const GlobalValue> GlobalValuePtr;

          /**
           * Create a bind statement.
           * @param name a name
           * @param value the value to be bound
           */
        public:
          GlobalValue(NamePtr name, ::std::optional<const ::mylang::names::FQN> exportedName,
              Expression::ExpressionPtr value);

          /** Destructor */
        public:
          ~GlobalValue();

          /**
           * Declare a mutable variable with the specified initializer
           * @param name a name
           * @param value the value to be bound
           */
        public:
          static GlobalValuePtr create(NamePtr name,
              ::std::optional<const ::mylang::names::FQN> exportedName,
              Expression::ExpressionPtr value);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /** The name under which this global value is known to the outside world. */
        public:
          const ::std::optional<const ::mylang::names::FQN> exportedName;

          /**
           * The name and type of the local variable that was introduced.
           */
        public:
          const Variable::VariablePtr variable;

          /**
           * The bound value (never null)
           */
        public:
          const Expression::ExpressionPtr value;
        };
      }

    }
  }
}
#endif
