#include <mylang/codegen/model/ir/LiteralNamedType.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        LiteralNamedType::LiteralNamedType(::std::shared_ptr<const types::NamedType> xtype,
            LiteralPtr xvalue)
            : Literal(xtype), value(xvalue)
        {
          if (!value) {
            throw ::std::invalid_argument("Missing value");
          }
          if (!xtype->resolve()->isSameType(*value->type)) {
            throw ::std::invalid_argument("Named type mismatch");
          }
        }

        LiteralNamedType::~LiteralNamedType()
        {
        }

        LiteralNamedType::LiteralNamedTypePtr LiteralNamedType::create(
            ::std::shared_ptr<const types::NamedType> xtype, LiteralPtr xvalue)
        {
          return ::std::make_shared<LiteralNamedType>(xtype, xvalue);
        }

        void LiteralNamedType::accept(NodeVisitor &visitor) const
        {
          visitor.visitLiteralNamedType(self<LiteralNamedType>());
        }
        bool LiteralNamedType::isSameLiteral(const Literal &other) const
        {
          const LiteralNamedType *that = dynamic_cast<const LiteralNamedType*>(&other);
          return that && type->isSameType(*that->type) && value->isSameLiteral(*that->value);
        }

      }
    }
  }
}
