#include <mylang/codegen/model/ir/ProcessBlock.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <mylang/codegen/model/types/ArrayType.h>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        ProcessBlock::ProcessBlock(::std::optional<::std::string> xname,
            StatementBlock::StatementBlockPtr xbody)
            : name(xname), body(xbody)
        {
        }

        ProcessBlock::~ProcessBlock()
        {
        }

        ProcessBlock::ProcessBlockPtr ProcessBlock::create(::std::optional<::std::string> xname,
            StatementBlock::StatementBlockPtr xbody)
        {
          return ::std::make_shared<ProcessBlock>(xname, xbody);
        }

        void ProcessBlock::accept(NodeVisitor &visitor) const
        {
          visitor.visitProcessBlock(self<ProcessBlock>());
        }

      }
    }
  }
}
