#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_TRYCATCH_H
#define CLASS_MYLANG_CODEGEN_MODEL_IR_TRYCATCH_H

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_STATEMENTBLOCK_H
#include <mylang/codegen/model/ir/StatementBlock.h>
#endif

#ifndef CLASS_MYLANG_CODEGEN_MODEL_IR_VARIABLE_H
#include <mylang/codegen/model/ir/Variable.h>
#endif

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        /**
         * A variable declaration. Variables are mutable!
         */
        class TryCatch: public Statement
        {
          TryCatch(const TryCatch &e) = delete;

          TryCatch& operator=(const TryCatch &e) = delete;

          /** A type pointer */
        public:
          typedef ::std::shared_ptr<const TryCatch> TryCatchPtr;

          /**
           * Create a try catch statment
           * @param tryBody the statements in the try body
           * @param catchBody the statements in the catch body
           */
        public:
          TryCatch(StatementBlock::StatementBlockPtr tryBody,
              StatementBlock::StatementBlockPtr catchBody);

          /** Destructor */
        public:
          ~TryCatch();

          /**
           * Declare a mutable variable with the specified initializer
           * @param cond the condition
           * @param body body of the statement
           */
        public:
          static TryCatchPtr create(StatementBlock::StatementBlockPtr tryBody,
              StatementBlock::StatementBlockPtr catchBody);

        public:
          void accept(NodeVisitor &visitor) const override final;

          /**
           * The statement to be executed if an exception is thrown
           */
        public:
          const StatementBlock::StatementBlockPtr tryBody;

          /**
           * The statement to be executed if an exception is thrown
           */
        public:
          const StatementBlock::StatementBlockPtr catchBody;
        };
      }
    }
  }
}
#endif
