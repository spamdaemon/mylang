#include <mylang/codegen/model/ir/NewProcess.h>
#include <mylang/codegen/model/ir/Node.h>
#include <mylang/codegen/model/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace codegen {
    namespace model {
      namespace ir {

        NewProcess::NewProcess(TypePtr t, const mylang::names::Name::Ptr &pname,
            const mylang::names::Name::Ptr &ctorName, ::std::vector<Variable::VariablePtr> value)
            : Expression(::std::move(t)), processName(pname), constructorName(ctorName),
                arguments(value)
        {
        }

        NewProcess::~NewProcess()
        {
        }

        NewProcess::NewProcessPtr NewProcess::create(TypePtr t,
            const mylang::names::Name::Ptr &pname, const mylang::names::Name::Ptr &ctorName,
            ::std::vector<Variable::VariablePtr> value)
        {
          return ::std::make_shared<NewProcess>(t, pname, ctorName, value);
        }

        void NewProcess::accept(NodeVisitor &visitor) const
        {
          visitor.visitNewProcess(self<NewProcess>());
        }

      }

    }
  }
}
