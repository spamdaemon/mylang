#ifndef CLASS_MYLANG_CODEGEN_CODESTREAM_H
#define CLASS_MYLANG_CODEGEN_CODESTREAM_H
#include <iostream>
#include <sstream>
#include <functional>
#include <map>

namespace mylang {
  namespace codegen {

    /**
     * A simple class that provides allows to generated formatted output.
     */
    class CodeStream
    {
    public:
      enum IndentOp
      {
        INDENT = 1, NONE = 0, OUTDENT = -1
      };

    public:
      enum class ParenOp
      {
        YES = 1, DEFAULT = 0, NO = -1
      };

      /**
       * Create a new stream with the initial indent
       * @param indent the initial indent
       */
    public:
      CodeStream(size_t indent = 0);

      /**
       * Create a new stream with the initial indent
       * @param src the source stream
       */
    public:
      CodeStream(const CodeStream &src);

      /** Destructor */
    public:
      ~CodeStream();

      /**
       * Get the stream
       * @return the io stream for this stream
       */
    public:
      ::std::ostream& stream();

      /**
       * Get the text written to this stream.
       * @return all text written to this stream
       */
    public:
      ::std::string getText() const;

      /**
       * Convenience function.
       * @param value
       * @return this stream
       */
    public:
      template<class T>
      friend CodeStream& operator<<(CodeStream &out, const T &value)
      {
        out.stream() << value;
        return out;
      }

      /**
       * Emit a newline and optionally indent
       * @param indentOp
       */
    public:
      CodeStream& newline(IndentOp op = NONE);

      /**
       * Emit multiple newlines
       * @param n the number of new lines to emit
       * @param indentOp
       */
    public:
      CodeStream& newlines(size_t n = 2, IndentOp op = NONE);

      /**
       * Emit a function definition.
       * @param signature the signature of the function
       * @param body the body of the function
       */
    public:
      CodeStream& function(const ::std::string &signature, const ::std::string &body);

      /**
       * Invoke the specified function within a brace. The brace
       * causes newlines to be emitted after { and before };
       * @param signature the signature of the function
       * @param body a generator for the body
       */
    public:
      CodeStream& function(const ::std::string &signature, ::std::function<void()> fn);

      /**
       * Emit a simple brace with a body.
       * @param before a string to emit just before brace
       * @param body the body of the brace
       */
    public:
      CodeStream& brace(const ::std::string &before, const ::std::string &body);

      /**
       * Invoke the specified function within a brace. The brace
       * causes newlines to be emitted after { and before };
       * @param before a string to emit just before brace
       * @param fn a function
       */
    public:
      CodeStream& brace(const ::std::string &before, ::std::function<void()> fn);

      /**
       * Invoke the specified function within a brace. The brace
       * causes newlines to be emitted after { and before };
       * @param fn a function
       */
    public:
      CodeStream& brace(::std::function<void()> fn);

      /**
       * Invoke the specified function within a parenthesis but without any newlines.
       * @param before a string to emit just before parens
       * @param fn a function
       */
    public:
      CodeStream& parens(const ::std::string &before, ::std::function<void()> fn);

      /**
       * Invoke the specified function within a parenthesis but without any newlines.
       * @param fn a function
       */
    public:
      CodeStream& parens(::std::function<void()> fn);

      /**
       * Invoke the specified function within a parenthesis but without any newlines.
       * @param fn a function that takes the argument number and the argument value
       */
    public:
      template<class T, class U, class FN>
      CodeStream& parens(T begin, U end, const char *separator, FN fn)
      {
        CodeStream &self = *this;
        return parens([&]
        {
          const char *sep = "";
          size_t cnt = 0;
          for (auto i = begin; i != end; ++i) {
            self << sep;
            sep = separator;
            fn(cnt++, *i);
          }
        });
      }

      /**
       * Invoke the specified function within a parenthesis but without any newlines.
       * @param fn a function
       */
    public:
      template<class T, class FN>
      CodeStream& parens(const ::std::vector<T> &v, const char *separator, FN fn)
      {
        return parens(v.begin(), v.end(), separator, fn);
      }

      /**
       * Emit a line comment followed by a newline
       */
    public:
      CodeStream& lineComment(const ::std::string &comment, bool emitNewLine = true);

      /**
       * Emit a block comment followed by a newline
       */
    public:
      CodeStream& blockComment(const ::std::string &comment, bool emitNewLine = true);

      /**
       * Emit the contents of input stream into this code stream. Any substring of the form "{{<key>}}" is replaced
       * with the defined replacement.
       * @param stream an input stream
       * @param replacements the replacements to be applied
       * @return this stream
       */
    public:
      CodeStream& insertTemplate(::std::istream &stream,
          const ::std::map<::std::string, ::std::string> &replacements);

      /**
       * Emit the contents of a file into this code stream. Any substring of the form "{{<key>}}" is replaced
       * with the defined replacement.
       * @param file a file
       * @param replacements the replacements to be applied
       * @return this stream
       */
    public:
      CodeStream& insertTemplateFile(const ::std::string &file,
          const ::std::map<::std::string, ::std::string> &replacements);

      /**
       * Emit the contents of a text into this code stream. Any substring of the form "{{<key>}}" is replaced
       * with the defined replacement.
       * @param file a file
       * @param replacements the replacements to be applied
       * @return this stream
       */
    public:
      CodeStream& insertTemplateString(const ::std::string &text,
          const ::std::map<::std::string, ::std::string> &replacements);

      /** The current indent */
    private:
      size_t _indent;

      /** The stream */
    private:
      ::std::ostringstream _stream;

    };

    struct NewLine
    {
      inline NewLine(size_t n)
          : count(n)
      {
      }
      friend CodeStream& operator<<(CodeStream &out, const NewLine &nl)
      {
        out.newlines(nl.count);
        return out;
      }
    private:
      size_t count;
    };

    inline NewLine nl(size_t n = 1)
    {
      return NewLine(n);
    }
  }
}
#endif
