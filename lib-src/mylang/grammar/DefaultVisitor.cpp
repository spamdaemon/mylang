#include <mylang/grammar/DefaultVisitor.h>

namespace mylang::grammar {
  DefaultVisitor::DefaultVisitor()
  {
  }

  DefaultVisitor::~DefaultVisitor()
  {
  }

  GroupNodeCPtr DefaultVisitor::default_visit(const GroupNodeCPtr &node)
  {
    visitChildren(node);
    return node;
  }

  GroupNodeCPtr DefaultVisitor::visit_syntax_error(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_root(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_import(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_assert_stmt(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_break_stmt(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_continue_stmt(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_call_constructor_stmt(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_expression_stmt(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_foreach_stmt(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_if_stmt(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_log_stmt(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_return_stmt(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_set_stmt(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_try_stmt(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_throw_stmt(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_update_stmt(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_val_stmt(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_var_stmt(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_wait_stmt(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_while_stmt(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_stmt_block(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_literal_bit(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_literal_bit_sequence(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_literal_boolean(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_literal_byte(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_literal_byte_sequence(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_literal_char(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_literal_decimal_integer(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_literal_hex_integer(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_literal_octal_integer(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_literal_real(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_literal_array(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_literal_tuple(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_literal_tuple_member(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_literal_struct(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_literal_struct_member(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_literal_string(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_literal_nil(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_interpolate_text_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_quote_text_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_stringify_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_variable_reference_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_named_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_new_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_arithmetic_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_negate_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_loop_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_with_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_bitop_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_conditional_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_comparison_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_sub_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_member_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_member_chain_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_apply_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_optify_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_deref_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_index_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_subrange_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_call_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_instantiate_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_lambda_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_concatenate_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_merge_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_zip_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_transform_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_safe_cast_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_unsafe_cast_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_clamp_cast_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_wrap_cast_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_orelse_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_try_expression(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_typeof(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_grouped(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_bit(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_boolean(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_byte(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_char(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_real(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_function(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_integer(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_string(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_array(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_tuple(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_struct(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_union(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_void(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_generic(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_process(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_process_input_member(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_process_output_member(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_member(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_struct_member(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_union_member(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_discriminant(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_function_body(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_lambda_body(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_mutable(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_type_opt(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_typename(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_constrained_type(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_named_constraint(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_macro_body(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_def_parameter(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_def_value(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_def_variable(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_def_transform(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_def_type(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_def_typename(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_def_namespace(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_def_function(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_def_extension_function(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_def_macro(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_def_process(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_def_alias(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_process_constructor(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_process_block(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_process_input(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_process_output(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_pattern_grouped(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_pattern_any(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_pattern_primitive(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_pattern_bit(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_pattern_boolean(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_pattern_byte(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_pattern_char(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_pattern_integer(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_pattern_real(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_pattern_string(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_pattern_struct(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_pattern_union(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_pattern_tuple(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_pattern_mutable(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_pattern_opt(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_pattern_array(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_pattern_function(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_named_type_pattern(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_constrained_type_pattern(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_def_type_pattern(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_def_value_pattern(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_def_generic_parameter(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_def_generic_function(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }

  GroupNodeCPtr DefaultVisitor::visit_def_generic_extension_function(const GroupNodeCPtr &node)
  {
    return default_visit(node);
  }
}
