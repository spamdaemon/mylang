#ifndef CLASS_MYLANG_GRAMMAR_DEFAULTVISITOR_H
#define CLASS_MYLANG_GRAMMAR_DEFAULTVISITOR_H

#ifndef CLASS_MYLANG_GRAMMAR_PAST_H
#include <mylang/grammar/PAst.h>
#endif

namespace mylang::grammar {
  class DefaultVisitor: public PAst::Visitor
  {
  protected:
    DefaultVisitor();

  public:
    ~DefaultVisitor();

    /**
     *  This function is invoked for each default implementation of a visit
     * function.
     *
     * The default implementation is to visit all children and then return the node.
     * @param node the node
     * @return node
     */
  public:
    virtual GroupNodeCPtr default_visit(const GroupNodeCPtr &node);

  public:
    // an error has occurred!
    GroupNodeCPtr visit_syntax_error(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_root(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_import(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_assert_stmt(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_break_stmt(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_continue_stmt(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_call_constructor_stmt(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_expression_stmt(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_foreach_stmt(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_if_stmt(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_log_stmt(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_return_stmt(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_set_stmt(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_try_stmt(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_throw_stmt(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_update_stmt(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_val_stmt(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_var_stmt(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_wait_stmt(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_while_stmt(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_stmt_block(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_literal_bit(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_literal_bit_sequence(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_literal_boolean(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_literal_byte(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_literal_byte_sequence(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_literal_char(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_literal_decimal_integer(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_literal_hex_integer(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_literal_octal_integer(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_literal_real(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_literal_array(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_literal_tuple(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_literal_tuple_member(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_literal_struct(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_literal_struct_member(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_literal_string(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_literal_nil(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_interpolate_text_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_quote_text_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_stringify_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_variable_reference_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_named_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_new_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_arithmetic_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_negate_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_loop_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_with_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_bitop_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_conditional_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_comparison_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_sub_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_member_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_member_chain_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_apply_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_optify_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_deref_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_index_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_subrange_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_call_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_instantiate_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_lambda_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_concatenate_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_merge_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_zip_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_transform_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_safe_cast_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_unsafe_cast_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_clamp_cast_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_wrap_cast_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_orelse_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_try_expression(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_typeof(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_grouped(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_bit(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_boolean(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_byte(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_char(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_real(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_function(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_integer(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_string(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_array(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_tuple(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_struct(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_union(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_void(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_generic(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_process(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_process_input_member(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_process_output_member(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_member(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_struct_member(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_union_member(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_discriminant(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_function_body(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_lambda_body(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_mutable(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_type_opt(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_typename(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_constrained_type(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_named_constraint(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_macro_body(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_def_parameter(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_def_value(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_def_variable(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_def_transform(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_def_type(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_def_typename(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_def_namespace(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_def_function(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_def_extension_function(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_def_macro(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_def_process(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_def_alias(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_process_constructor(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_process_block(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_process_input(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_process_output(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_pattern_grouped(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_pattern_any(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_pattern_primitive(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_pattern_bit(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_pattern_boolean(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_pattern_byte(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_pattern_char(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_pattern_integer(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_pattern_real(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_pattern_string(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_pattern_struct(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_pattern_union(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_pattern_tuple(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_pattern_mutable(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_pattern_opt(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_pattern_array(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_pattern_function(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_named_type_pattern(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_constrained_type_pattern(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_def_type_pattern(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_def_value_pattern(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_def_generic_parameter(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_def_generic_function(const GroupNodeCPtr &node) override;

    GroupNodeCPtr visit_def_generic_extension_function(const GroupNodeCPtr &node) override;
  };
}
#endif
