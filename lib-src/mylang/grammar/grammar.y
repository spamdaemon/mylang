// reserved words
%token LITERAL_DECIMAL_INTEGER LITERAL_OCTAL_INTEGER LITERAL_HEX_INTEGER
%token LITERAL_BIT LITERAL_BYTE LITERAL_BOOLEAN LITERAL_REAL LITERAL_CHAR LITERAL_STRING LITERAL_NIL
%token LITERAL_BIT_SEQUENCE LITERAL_BYTE_SEQUENCE
%token IDENTIFIER 
%token TYPE_BIT TYPE_BOOLEAN TYPE_BYTE TYPE_CHAR TYPE_REAL TYPE_INTEGER TYPE_STRING TYPE_TUPLE TYPE_STRUCT TYPE_UNION TYPE_VOID
%token GENERIC 
%token DO 
%token PLACEHOLDER
%token TYPEDEF TYPENAME NAMESPACEDEF FUNCTIONDEF EXTENSIONDEF MACRODEF DEFPROC
%token TRANSFORMDEF
%token PROCESS
%token EXTERN
%token EXPORT
%token TYPEOF
%token IMPORT ALIAS
%token ASSERT RETURN VAL VAR MUTABLE WHILE FOREACH WAIT 
%token LOG


/* TOKENS */
%token OP_APPLY OP_HASH OP_FOLLOWS

%token OP_WITH  
%token OP_FOR

%token OP_SET OP_ASSIGN OP_CONCATENATE OP_ZIP
%token OP_SAFE_TYPECAST OP_UNSAFE_TYPECAST 
%token OP_CLAMP_TYPECAST OP_WRAP_TYPECAST OP_TRANSFORM
%token CATCH BREAK CONTINUE
%token OP_TRY OP_IF OP_ELSE OP_NEW 

%token BEGIN_QUOTE QUOTED_TEXT END_QUOTE

 /* comparison */
%token OP_LE OP_LT OP_EQ OP_NEQ OP_GT OP_GE
 /* logical options on booleans */
%token OP_LOGICAL_AND OP_LOGICAL_OR OP_LOGICAL_NOT
 /* bit operattions */
%token OP_BITWISE_AND OP_BITWISE_OR OP_BITWISE_XOR OP_BITWISE_NOT
%token OP_SHIFT_RIGHT OP_ARITHMETIC_SHIFT_RIGHT OP_SHIFT_LEFT

/* arithmetic ops */
%token OP_PLUS OP_MINUS OP_MULT OP_DIV OP_MOD OP_REM

 /* member select operator */
%token OP_MEMBER OP_TYPE_MEMBER

 /* A ternary token */
%token OP_QUESTIONMARK OP_ORELSE

 /* A list separator (i.e. comma) */
%token OP_LIST 

/* define the precedence and associativity for each operator */
%precedence OP_ASSIGN OP_SET
%precedence OP_TRY OP_THROW
%precedence DO 
%precedence '{' '}'
%right ':'
%left OP_CONCATENATE OP_MERGE OP_HASH
%left OP_QUESTIONMARK
%left OP_BITWISE_AND OP_BITWISE_OR OP_BITWISE_XOR
%left OP_LOGICAL_OR
%left OP_LOGICAL_AND 
%precedence OP_BITWISE_NOT
%precedence OP_LOGICAL_NOT
%left OP_LE OP_LT OP_EQ OP_NEQ OP_GT OP_GE
%left OP_PLUS OP_MINUS
%left OP_MULT OP_DIV OP_MOD OP_REM
%precedence OP_UNARY_MINUS /* tag */
%precedence OP_DEREF /* tag */
%precedence OP_APPLY
%right OP_ORELSE
%precedence OP_WITH OP_SAFE_TYPECAST OP_UNSAFE_TYPECAST OP_CLAMP_TYPECAST OP_TRANSFORM OP_WRAP_TYPECAST OP_IF OP_ELSE
%precedence '[' ']'
%precedence '(' ')'
%left OP_MEMBER OP_TYPE_MEMBER

%%

AST: Root { ast = $1; }

Root: 
 Opt_List_Import Opt_List_Definition  { $$ = mkGrp(PAst::GRP_ROOT,{{PAst::REL_IMPORTS,$1},{PAst::REL_DECLS,$2}}); }
| Opt_List_Import TransformDefinition  { $$ = mkGrp(PAst::GRP_ROOT,{{PAst::REL_IMPORTS,$1},{PAst::REL_DECLS,$2}}); }


Import : 
  IMPORT List_Identifier ';' { $$ = mkGrp(PAst::GRP_IMPORT,{$1,{PAst::REL_QN,$2},$3}); }
/*|    IMPORT LITERAL_STRING ';' { $$ = mkGrp(PAst::GRP_IMPORT,{$1,{PAst::TOK_FILE,$2},$3}); } */
 
Definition : NamespaceDefinition  
|TopLevelDefinition
|ProcessDefinition;

TransformDefinition :
OP_TRANSFORM List_Identifier '(' List_TransformParameter ')' ':' OPT_FQNType FunctionBody
{ $$=mkGrp(PAst::GRP_DEF_TRANSFORM,{$1,{PAst::REL_QN,$2},$3,{PAst::REL_PARAMETERS,$4},$5,$6,{PAst::REL_TYPE,$7},{PAst::REL_BODY,$8}}); } 

OPT_FQNType: 
  FQNType 
| FQNType OP_QUESTIONMARK { $$ = mkGrp(PAst::GRP_TYPE_OPT,{{PAst::REL_TYPE,$1},$2}); }

TransformParameter: IDENTIFIER ':' FQNType { $$=mkGrp(PAst::GRP_DEF_PARAMETER,{{PAst::TOK_IDENTIFIER,$1},$2,{PAst::REL_TYPE,$3}}); }

ProcessDefinition : 
  PROCESS IDENTIFIER '{' Opt_List_Process_Stmt '}' { $$=mkGrp(PAst::GRP_DEF_PROCESS,{ $1,{PAst::TOK_IDENTIFIER,$2},$3,{PAst::REL_STATEMENTS,$4},$5}); }

TopLevelDefinition:
 TypeDefinition
| GenericDefinition
| FunctionDefinition
| ExtensionDefinition
| ValueDefinition
| MacroDefinition
| AliasDefinition

Process_Stmt:
 TopLevelDefinition
| VariableDefinition
| ProcessDefinition
| OP_NEW '(' Opt_List_Parameter ')' '{' Opt_Call_Process_Constructor Opt_List_Stmt '}'
   { $$=mkGrp(PAst::GRP_PROCESS_CONSTRUCTOR,{$1,$2,{PAst::REL_PARAMETERS,$3},$4,$5,{PAst::REL_INIT_STATEMENT,$6},{PAst::REL_STATEMENTS,$7},$8}); } 
| IDENTIFIER '(' Expr ')' StmtBlock 
   { $$=mkGrp(PAst::GRP_PROCESS_BLOCK,{{PAst::TOK_IDENTIFIER,$1},$2,{PAst::EXPR_CONDITION,$3},$4,{PAst::REL_BODY,$5}}); } 
| IDENTIFIER StmtBlock 
   { $$=mkGrp(PAst::GRP_PROCESS_BLOCK,{{PAst::TOK_IDENTIFIER,$1},{PAst::REL_BODY,$2}}); } 
| OP_FOLLOWS IDENTIFIER VariableType ';'
   { $$=mkGrp(PAst::GRP_PROCESS_INPUT,{ $1,{PAst::TOK_IDENTIFIER,$2}, {PAst::REL_TYPE,$3},$4 }); }
| OP_LE IDENTIFIER VariableType ';'
   { $$=mkGrp(PAst::GRP_PROCESS_OUTPUT,{ $1,{PAst::TOK_IDENTIFIER,$2}, {PAst::REL_TYPE,$3},$4  }); }

Call_Process_Constructor:
 PROCESS '(' Opt_List_Expr ')' ';' { $$=mkGrp(PAst::GRP_CALL_CONSTRUCTOR_STMT,{$1,$2,{PAst::EXPR_EXPRESSION,$3},$4,$5}); }  


StmtBlock: 
'{' Opt_List_Stmt '}' { $$=mkGrp(PAst::GRP_STATEMENT_BLOCK,{$1,{PAst::REL_STATEMENTS,$2},$3}); }
|'{' error '}' { $$=mkGrp(PAst::GRP_SYNTAX_ERROR,{$1,$2,$3});  }

Stmt:
 TopLevelDefinition
| VariableDefinition
| RETURN Opt_Expr ';' { $$=mkGrp(PAst::GRP_RETURN_STMT,{{PAst::TOK_KEYWORD,$1},{PAst::EXPR_EXPRESSION,$2}, $3}); }
| TryCatchStmt
| ThrowStmt
| LOG OP_MEMBER RESERVED_IDENTIFIER List_Expr ';'  { $$=mkGrp(PAst::GRP_LOG_STMT,{{PAst::TOK_KEYWORD,$1},$2,{PAst::TOK_LOG_LEVEL,$3}, {PAst::EXPR_ARGUMENTS,$4}, $5}); }
| Expr OP_ASSIGN Expr ';' { $$=mkGrp(PAst::GRP_UPDATE_STMT,{{PAst::EXPR_LHS,$1},{$2},{PAst::EXPR_RHS,$3},$4}); }
| Expr OP_SET Expr ';' { $$=mkGrp(PAst::GRP_SET_STMT,{{PAst::EXPR_LHS,$1},{$2},{PAst::EXPR_RHS,$3},$4}); }
| AssertStmt
| LoopStmt
| IfStmt
| BREAK ';' { $$=mkGrp(PAst::GRP_BREAK_STMT,{{PAst::TOK_KEYWORD,$1},$2}); } 
| CONTINUE ';' { $$=mkGrp(PAst::GRP_CONTINUE_STMT,{{PAst::TOK_KEYWORD,$1},$2}); } 
| StmtBlock
/* expressions that can be used as a statement */
| FunctionCallExpr ';'  { $$=mkGrp(PAst::GRP_EXPRESSION_STMT,{{PAst::EXPR_EXPRESSION,$1}, $2}); }
/* process/port related functions */
| WAIT Opt_List_Expr ';' { $$=mkGrp(PAst::GRP_WAIT_STMT,{{PAst::TOK_KEYWORD,$1},{PAst::EXPR_EXPRESSION,$2}, $3}); }
/* error handling */
| error ';' { $$=mkGrp(PAst::GRP_SYNTAX_ERROR,{$1,$2}); } 

AssertStmt:
  ASSERT Expr ';' { $$=mkGrp(PAst::GRP_ASSERT_STMT,{$1,{PAst::EXPR_CONDITION,$2},$3}); } 
| ASSERT Expr ':' Expr ';' { $$=mkGrp(PAst::GRP_ASSERT_STMT,{$1,{PAst::EXPR_CONDITION,$2},$3,{PAst::EXPR_MESSAGE,$4},$5}); } 

LoopStmt:
 FOREACH IDENTIFIER ':' Expr StmtBlock { $$=mkGrp(PAst::GRP_FOREACH_STMT,{$1,{PAst::TOK_IDENTIFIER,$2},$3,{PAst::EXPR_EXPRESSION,$4},{PAst::REL_STATEMENTS,$5}}); } 
| WHILE Expr StmtBlock { $$=mkGrp(PAst::GRP_WHILE_STMT,{$1,{PAst::EXPR_CONDITION,$2},{PAst::STMT_IFTRUE,$3}}); } 

TryCatchStmt:
 OP_TRY StmtBlock CATCH StmtBlock { $$=mkGrp(PAst::GRP_TRY_STMT,{$1,{PAst::STMT_TRY,$2}, $3,{PAst::STMT_CATCH,$4}}); }

ThrowStmt:
 OP_THROW ';' { $$=mkGrp(PAst::GRP_THROW_STMT,{{PAst::TOK_KEYWORD,$1},$2}); }
 
IfStmt: 
 OP_IF Expr StmtBlock  {  $$=mkGrp(PAst::GRP_IF_STMT,{$1,{PAst::EXPR_CONDITION,$2}, {PAst::STMT_IFTRUE,$3}}); }
| OP_IF Expr StmtBlock OP_ELSE StmtBlock {  $$=mkGrp(PAst::GRP_IF_STMT,{$1,{PAst::EXPR_CONDITION,$2}, {PAst::STMT_IFTRUE,$3},$4,{PAst::STMT_IFFALSE,$5}}); }
| OP_IF Expr StmtBlock OP_ELSE IfStmt {  $$=mkGrp(PAst::GRP_IF_STMT,{$1,{PAst::EXPR_CONDITION,$2}, {PAst::STMT_IFTRUE,$3},$4,{PAst::STMT_IFFALSE,$5}}); }

AliasDefinition:
OP_WITH List_Identifier ';' { $$= mkGrp(PAst::GRP_DEF_ALIAS,{$1,{PAst::REL_QN,$2},$3}); }

ValueDefinition:
 VAL IDENTIFIER  Opt_VariableType Opt_Initializer ';'
  { $$=mkGrp(PAst::GRP_VAL_STMT,{$1,{PAst::TOK_IDENTIFIER,$2},{PAst::REL_TYPE,$3},{PAst::EXPR_EXPRESSION,$4}, $5}); }
| VAL IDENTIFIER VariableType  OP_LT OP_GT Expr ';'
  { $$=mkGrp(PAst::GRP_VAL_STMT,{$1,{PAst::TOK_IDENTIFIER,$2},{PAst::REL_TYPE,$3},$4,$5,
  	{PAst::EXPR_EXPRESSION,
  		mkGrp(PAst::GRP_TRANSFORM_EXPRESSION,{{PAst::REL_TYPE,$3},{PAst::EXPR_ARGUMENTS,$6}})}, $7}); }

VariableDefinition:
 VAR IDENTIFIER  Opt_VariableType Opt_Initializer ';'
  { $$=mkGrp(PAst::GRP_VAR_STMT,{$1,{PAst::TOK_IDENTIFIER,$2},{PAst::REL_TYPE,$3},{PAst::EXPR_EXPRESSION,$4}, $5}); }
| MUTABLE IDENTIFIER  Opt_VariableType Opt_Initializer ';'
  { $$=mkGrp(PAst::GRP_VAR_STMT,{$1,{PAst::TOK_IDENTIFIER,$2},{PAst::REL_TYPE,$3},{PAst::EXPR_EXPRESSION,$4}, $5}); }

TypeDefinition : 
TYPEDEF IDENTIFIER ';' { $$= mkGrp(PAst::GRP_DEF_TYPE,{$1,{PAst::TOK_IDENTIFIER,$2},$3}); }
|TYPEDEF IDENTIFIER OP_ASSIGN Type ';' { $$= mkGrp(PAst::GRP_DEF_TYPE,{$1,{PAst::TOK_IDENTIFIER,$2},$3,{PAst::REL_TYPE,$4},$5}); }
|TypeName;



TypeName: TYPENAME IDENTIFIER OP_ASSIGN Type ';' { $$= mkGrp(PAst::GRP_DEF_TYPENAME,{$1,{PAst::TOK_IDENTIFIER,$2},$3,{PAst::REL_TYPE,$4},$5}); }

NamespaceDefinition : 
NAMESPACEDEF List_Identifier '{' Opt_List_Definition '}' { $$= mkGrp(PAst::GRP_DEF_NAMESPACE,{$1,{PAst::REL_QN,$2},$3,{PAst::REL_DECLS,$4},$5}); }

GenericDefinition: GenericFunctionDefinition | GenericExtensionDefinition;

FunctionDefinition : 
FUNCTIONDEF Opt_Export IDENTIFIER  '(' Opt_List_Parameter ')' Opt_ReturnType FunctionBody
{ $$=mkGrp(PAst::GRP_DEF_FUNCTION,{$1,{PAst::REL_EXPORTED,$2},{PAst::TOK_IDENTIFIER,$3},$4,{PAst::REL_PARAMETERS,$5},$6,{PAst::REL_TYPE,$7},{PAst::REL_BODY,$8}}); } 
| FUNCTIONDEF Opt_Export IDENTIFIER  '(' Opt_List_Parameter ')' ReturnType ';'
{ $$=mkGrp(PAst::GRP_DEF_FUNCTION,{$1,{PAst::REL_EXPORTED,$2},{PAst::TOK_IDENTIFIER,$3},$4,{PAst::REL_PARAMETERS,$5},$5,{PAst::REL_TYPE,$7},$8}); } 

Export: EXPORT;


ExtensionDefinition:
EXTENSIONDEF IDENTIFIER  '(' List_Parameter ')' Opt_ReturnType FunctionBody
{ $$=mkGrp(PAst::GRP_DEF_EXTENSION_FUNCTION,{$1,{PAst::TOK_IDENTIFIER,$2},$3,{PAst::REL_PARAMETERS,$4},$5,{PAst::REL_TYPE,$6},{PAst::REL_BODY,$7}}); } 
| EXTENSIONDEF IDENTIFIER  '(' List_Parameter ')' ReturnType ';'
{ $$=mkGrp(PAst::GRP_DEF_EXTENSION_FUNCTION,{$1,{PAst::TOK_IDENTIFIER,$2},$3,{PAst::REL_PARAMETERS,$4},$5,{PAst::REL_TYPE,$6},$7}); } 

GenericFunctionDefinition : 
GENERIC Opt_List_Generic FUNCTIONDEF IDENTIFIER  '(' Opt_List_GenericParameter ')' Opt_ReturnTypePattern FunctionBody
{ $$=mkGrp(PAst::GRP_DEF_GENERIC_FUNCTION,{$1,{PAst::REL_DEF_PATTERNS,$2}, $3, {PAst::TOK_IDENTIFIER,$4},$5,{PAst::REL_PARAMETERS,$6},$7,{PAst::REL_TYPE,$8},{PAst::REL_BODY,$9}}); } 
| GENERIC Opt_List_Generic FUNCTIONDEF IDENTIFIER  '(' Opt_List_GenericParameter ')' Opt_ReturnTypePattern ';'
{ $$=mkGrp(PAst::GRP_DEF_GENERIC_FUNCTION,{$1,{PAst::REL_DEF_PATTERNS,$2}, $3, {PAst::TOK_IDENTIFIER,$4},$5,{PAst::REL_PARAMETERS,$6},$7,{PAst::REL_TYPE,$8},$9}); } 

GenericExtensionDefinition : 
GENERIC List_Generic EXTENSIONDEF IDENTIFIER  '(' List_GenericParameter ')' Opt_ReturnTypePattern FunctionBody
{ $$=mkGrp(PAst::GRP_DEF_GENERIC_EXTENSION_FUNCTION,{$1,{PAst::REL_DEF_PATTERNS,$2}, $3, {PAst::TOK_IDENTIFIER,$4},$5,{PAst::REL_PARAMETERS,$6},$7,{PAst::REL_TYPE,$8},{PAst::REL_BODY,$9}}); } 
| GENERIC List_Generic EXTENSIONDEF IDENTIFIER  '(' List_GenericParameter ')' Opt_ReturnTypePattern ';'
{ $$=mkGrp(PAst::GRP_DEF_GENERIC_EXTENSION_FUNCTION,{$1,{PAst::REL_DEF_PATTERNS,$2}, $3, {PAst::TOK_IDENTIFIER,$4},$5,{PAst::REL_PARAMETERS,$6},$7,{PAst::REL_TYPE,$8},$9}); } 

 
MacroDefinition : 
MACRODEF IDENTIFIER  '(' Opt_List_MacroParameter ')' MacroBody
{ $$=mkGrp(PAst::GRP_DEF_MACRO,{$1,{PAst::TOK_IDENTIFIER,$2},$3,{PAst::REL_VARIABLES,$4},$5,{PAst::REL_BODY,$6}}); } 

MacroParameter: IDENTIFIER Opt_ParameterType { $$=mkGrp(PAst::GRP_DEF_VALUE,{{PAst::TOK_IDENTIFIER,$1},{PAst::REL_TYPE,$2}}); }


FunctionBody: 
'{' Opt_List_Stmt '}' { $$=mkGrp(PAst::GRP_FUNCTION_BODY,{$1,{PAst::REL_STATEMENTS,$2},$3}); }
| OP_ASSIGN Expr ';' { $$=mkGrp(PAst::GRP_FUNCTION_BODY,{$1,{PAst::EXPR_EXPRESSION,$2},$3}); }

MacroBody: 
'{' Opt_List_TypeName Expr '}' { $$=mkGrp(PAst::GRP_MACRO_BODY,{$1,{PAst::REL_DECLS,$2},{PAst::EXPR_EXPRESSION,$3},$4}); }
| OP_ASSIGN Expr ';' { $$=mkGrp(PAst::GRP_MACRO_BODY,{$1,{PAst::EXPR_EXPRESSION,$2},$3}); }

Generic:
IDENTIFIER { $$ = mkGrp(PAst::GRP_DEF_TYPE_PATTERN,{{PAst::TOK_IDENTIFIER,$1}}); }
|IDENTIFIER VariableType { $$ = mkGrp(PAst::GRP_DEF_VALUE_PATTERN,{{PAst::TOK_IDENTIFIER,$1},{PAst::REL_TYPE,$2}}); }
|IDENTIFIER OP_ASSIGN TypePattern { $$ = mkGrp(PAst::GRP_DEF_TYPE_PATTERN,{{PAst::TOK_IDENTIFIER,$1},$2, {PAst::REL_PATTERN,$3}}); }

TypePattern:
  PrimitiveType { $$= mkGrp(PAst::GRP_PATTERN_PRIMITIVE,{{PAst::REL_TYPE,$1}}); }
| AnyPattern
| BitPattern
| BooleanPattern
| BytePattern
| CharPattern
| IntegerPattern
| RealPattern
| StringPattern
| StructTypePattern 
| TupleTypePattern
| UnionTypePattern
| ArrayTypePattern
| OptTypePattern
| MutableTypePattern
| TypePattern '{' Opt_List_TypePatternConstraint '}' { $$= mkGrp(PAst::GRP_CONSTRAINED_TYPE_PATTERN,{ {PAst::REL_PATTERN,$1},$2,{PAst::REL_CONSTRAINTS,$3},$4}); }
| PatternOrTypeName
| FunctionTypePattern;

AnyPattern : OP_MULT { $$= mkGrp(PAst::GRP_PATTERN_ANY,{$1}); }

BitPattern: TYPE_BIT OP_MULT { $$= mkGrp(PAst::GRP_PATTERN_BIT,{$1,$2}); }
BooleanPattern: TYPE_BOOLEAN OP_MULT { $$= mkGrp(PAst::GRP_PATTERN_BOOLEAN,{$1,$2}); }
BytePattern: TYPE_BYTE OP_MULT { $$= mkGrp(PAst::GRP_PATTERN_BYTE,{$1,$2}); }
CharPattern: TYPE_CHAR OP_MULT { $$= mkGrp(PAst::GRP_PATTERN_CHAR,{$1,$2}); }
IntegerPattern: TYPE_INTEGER OP_MULT { $$= mkGrp(PAst::GRP_PATTERN_INTEGER,{$1,$2}); }
RealPattern: TYPE_REAL OP_MULT { $$= mkGrp(PAst::GRP_PATTERN_REAL,{$1,$2}); }
StringPattern: TYPE_STRING OP_MULT { $$= mkGrp(PAst::GRP_PATTERN_STRING,{$1,$2}); }

ArrayDimensionPattern:
  '[' OP_MULT ']'  { $$= mkGrp(PAst::GRP_ARRAY_ANY_DIMENSION_PATTERN,{$1,$2,$3}); }
| '['  ']' { $$= mkGrp(PAst::GRP_ARRAY_DIMENSION_PATTERN,{$1,$2}); }
| '[' Expr ':'  ']' { $$= mkGrp(PAst::GRP_ARRAY_DIMENSION_PATTERN,{$1,{PAst::REL_MIN_SIZE,$2},$3,{$4} }); }
| '[' Expr ']' { $$= mkGrp(PAst::GRP_ARRAY_DIMENSION_PATTERN,{$1,{PAst::REL_SIZE,$2},$3}); }
| '[' Expr ':' Expr ']' { $$= mkGrp(PAst::GRP_ARRAY_DIMENSION_PATTERN,{$1,{PAst::REL_MIN_SIZE,$2},$3,{PAst::REL_MAX_SIZE,$4},{$5} }); }
| '['  ':' Expr ']' { $$= mkGrp(PAst::GRP_ARRAY_DIMENSION_PATTERN,{$1,$2,{PAst::REL_MAX_SIZE,$3},{$4} }); }

ArrayTypePattern: TypePattern ArrayDimensionPattern  { $$= mkGrp(PAst::GRP_PATTERN_ARRAY,{{PAst::REL_PATTERN,$1},{PAst::REL_DIMENSION,$2}}); }

OptTypePattern: TypePattern OP_QUESTIONMARK { $$= mkGrp(PAst::GRP_PATTERN_OPT,{{PAst::REL_PATTERN,$1},$2}); }
MutableTypePattern: TypePattern OP_LOGICAL_NOT { $$= mkGrp(PAst::GRP_PATTERN_MUTABLE,{{PAst::REL_PATTERN,$1},$2}); }

FunctionTypePattern: '(' Opt_List_TypePattern ')' ':' TypePattern { $$ = mkGrp(PAst::GRP_PATTERN_FUNCTION,{ $1,{PAst::REL_PATTERNS,$2},$3,$4,{PAst::REL_PATTERN,$5} }); }
| '(' Opt_List_TypePattern ')' { $$ = mkGrp(PAst::GRP_GROUPED_PATTERN,{ $1,{PAst::REL_PATTERNS,$2},$3 }); }

StructTypePattern:
TYPE_STRUCT OP_MULT  { $$= mkGrp(PAst::GRP_PATTERN_STRUCT,{$1,$2}); }
| StructType    { $$= mkGrp(PAst::GRP_PATTERN_PRIMITIVE,{{PAst::REL_TYPE,$1}}); }

TupleTypePattern:
TYPE_TUPLE OP_MULT   { $$= mkGrp(PAst::GRP_PATTERN_TUPLE,{$1,$2}); }
| TupleType    { $$= mkGrp(PAst::GRP_PATTERN_PRIMITIVE,{{PAst::REL_TYPE,$1}}); }

UnionTypePattern:
TYPE_UNION OP_MULT { $$= mkGrp(PAst::GRP_PATTERN_UNION,{$1,$2}); }
| UnionType    { $$= mkGrp(PAst::GRP_PATTERN_PRIMITIVE,{{PAst::REL_TYPE,$1}}); }

PatternOrTypeName: List_Identifier { $$= mkGrp(PAst::GRP_NAMED_TYPE_PATTERN,{{PAst::REL_QN,$1}}); } 

Type: Type '{' Opt_List_TypeConstraint '}' { $$= mkGrp(PAst::GRP_CONSTRAINED_TYPE,{ {PAst::REL_TYPE,$1},$2,{PAst::REL_CONSTRAINTS,$3},$4}); }
| UnconstrainedType;

UnconstrainedType:
 PrimitiveType
| ArrayType
| TypeOf
| OptType
| MutableType
| StructType
| TupleType
| UnionType
| FQNType
| VoidType
| GENERIC   { $$ = mkGrp(PAst::GRP_TYPE_GENERIC,{}); }
| FunctionType
| ProcessType
| TypeMember;

FQNType: List_Identifier { $$= mkGrp(PAst::GRP_TYPENAME,{{PAst::REL_QN,$1}}); } 

TypeOf: TYPEOF '(' Expr ')' { $$= mkGrp(PAst::GRP_TYPEOF,{ $1,$2,{PAst::EXPR_EXPRESSION,$3},$4}); }

ArrayDimension:
  '['  ']' { $$= mkGrp(PAst::GRP_ARRAY_DIMENSION,{$1,$2}); }
| '[' Literal_Integer ']' { $$= mkGrp(PAst::GRP_ARRAY_DIMENSION,{$1,{PAst::REL_SIZE,$2},$3}); }
| '[' Literal_Integer ':' Literal_Integer ']' { $$= mkGrp(PAst::GRP_ARRAY_DIMENSION,{$1,{PAst::REL_MIN_SIZE,$2},$3,{PAst::REL_MAX_SIZE,$4},{$5} }); }
| '['  ':' Literal_Integer ']' { $$= mkGrp(PAst::GRP_ARRAY_DIMENSION,{$1,$2,{PAst::REL_MAX_SIZE,$3},{$4} }); }
| '[' Literal_Integer ':'  ']' { $$= mkGrp(PAst::GRP_ARRAY_DIMENSION,{$1,{PAst::REL_MIN_SIZE,$2},$3,{$4} }); }

ArrayType: 
  Type ArrayDimension { $$= mkGrp(PAst::GRP_TYPE_ARRAY,{{PAst::REL_TYPE,$1},{PAst::REL_DIMENSION,$2}}); }

FunctionType: '(' Opt_List_Type ')' ':' Type { $$ = mkGrp(PAst::GRP_TYPE_FUNCTION,{ $1,{PAst::REL_PARAMETER_TYPES,$2},$3,$4,{PAst::REL_TYPE,$5} }); }
| '(' Opt_List_Type ')' { $$ = mkGrp(PAst::GRP_GROUPED_TYPE,{ $1,{PAst::REL_TYPE,$2},$3 }); }

MutableType: Type OP_LOGICAL_NOT { $$= mkGrp(PAst::GRP_TYPE_MUTABLE,{{PAst::REL_TYPE,$1},$2}); }
OptType: Type OP_QUESTIONMARK { $$= mkGrp(PAst::GRP_TYPE_OPT,{{PAst::REL_TYPE,$1},$2}); }
StructType: TYPE_STRUCT '{' Opt_List_StructMember '}' { $$= mkGrp(PAst::GRP_TYPE_STRUCT,{$1,$2,{PAst::REL_MEMBERS,$3},$4}); }
TupleType: TYPE_TUPLE '(' Opt_List_Type ')' { $$= mkGrp(PAst::GRP_TYPE_TUPLE,{$1,$2,{PAst::REL_MEMBERS,$3},$4}); }
UnionType: TYPE_UNION '(' Discriminant ')' '{' List_UnionMember '}' { $$= mkGrp(PAst::GRP_TYPE_UNION,{$1,$2,{PAst::REL_DISCRIMINANT,$3},$4,$5,{PAst::REL_MEMBERS,$6},$7}); }
VoidType: TYPE_VOID   { $$ = mkGrp(PAst::GRP_TYPE_VOID,{{PAst::TOK_KEYWORD,$1}}); }
ProcessType: PROCESS '{' Opt_List_ProcessMember '}' { $$= mkGrp(PAst::GRP_TYPE_PROCESS,{$1,$2,{PAst::REL_MEMBERS,$3},$4}); }

TypeMember:
  Type OP_TYPE_MEMBER RESERVED_IDENTIFIER { $$=mkGrp(PAst::GRP_TYPE_MEMBER,{{PAst::REL_TYPE,$1},{$2},{PAst::TOK_IDENTIFIER,$3}}); }


ProcessMember:
 OP_FOLLOWS IDENTIFIER VariableType
   { $$=mkGrp(PAst::GRP_TYPE_PROCESS_INPUT_MEMBER,{ $1,{PAst::TOK_IDENTIFIER,$2}, {PAst::REL_TYPE,$3} }); }
| OP_LE IDENTIFIER VariableType
   { $$=mkGrp(PAst::GRP_TYPE_PROCESS_OUTPUT_MEMBER,{ $1,{PAst::TOK_IDENTIFIER,$2}, {PAst::REL_TYPE,$3}  }); }

UnionMember : Expr OP_FOLLOWS IDENTIFIER ':' Type { $$=mkGrp(PAst::GRP_UNION_MEMBER,{{PAst::EXPR_EXPRESSION,$1},$2,{PAst::TOK_IDENTIFIER,$3},$4,{PAst::REL_TYPE,$5}}); }

StructMember: IDENTIFIER ':' Type { $$= mkGrp(PAst::GRP_STRUCT_MEMBER,{{PAst::TOK_IDENTIFIER,$1},$2,{PAst::REL_TYPE,$3}}); }

Discriminant: IDENTIFIER ':' PrimitiveType { $$= mkGrp(PAst::GRP_DISCRIMINANT,{{PAst::TOK_IDENTIFIER,$1},$2,{PAst::REL_TYPE,$3}}); }

TypeConstraint: NamedConstraint;

TypePatternConstraint: NamedConstraint;

NamedConstraint : 
 IDENTIFIER OP_ASSIGN Expr { $$=mkGrp(PAst::GRP_NAMED_CONSTRAINT,{ {PAst::TOK_IDENTIFIER,$1},$2,{PAst::EXPR_EXPRESSION,$3}}); }
| IDENTIFIER ':' Expr { $$=mkGrp(PAst::GRP_NAMED_CONSTRAINT,{ {PAst::TOK_IDENTIFIER,$1},$2,{PAst::EXPR_EXPRESSION,$3}}); }


PrimitiveType: 
 TYPE_BIT       { $$ = mkGrp(PAst::GRP_TYPE_BIT,{{PAst::TOK_TYPE,$1}}); }
| TYPE_BOOLEAN  { $$ = mkGrp(PAst::GRP_TYPE_BOOLEAN,{{PAst::TOK_TYPE,$1}}); } 
| TYPE_BYTE     { $$ = mkGrp(PAst::GRP_TYPE_BYTE,{{PAst::TOK_TYPE,$1}}); }
| TYPE_CHAR     { $$ = mkGrp(PAst::GRP_TYPE_CHAR,{{PAst::TOK_TYPE,$1}}); }
| TYPE_INTEGER  { $$ = mkGrp(PAst::GRP_TYPE_INTEGER,{{PAst::TOK_TYPE,$1}}); }
| TYPE_REAL    { $$ = mkGrp(PAst::GRP_TYPE_REAL,{{PAst::TOK_TYPE,$1}}); }
| TYPE_STRING   { $$ = mkGrp(PAst::GRP_TYPE_STRING,{{PAst::TOK_TYPE,$1}}); }

// fixme: want an address type (i.e. a pointer type)
// so we can peek and poke at an address

VariableType: ':' Type 
{ $$=join(mkIgn($1),$2); }

ParameterType: ':' Type 
{ $$=join(mkIgn($1),$2); }

ReturnType: ':' Type 
{ $$=join(mkIgn($1),$2); }

GenericParameter: IDENTIFIER ':' TypePattern { $$=mkGrp(PAst::GRP_DEF_GENERIC_PARAMETER,{{PAst::TOK_IDENTIFIER,$1},$2,{PAst::REL_PATTERN,$3}}); }

ReturnTypePattern: ':' TypePattern 
{ $$=join(mkIgn($1),$2); }

Initializer: OP_ASSIGN Expr 
{ $$=join(mkIgn($1),$2); }

FunctionCallExpr: 
Expr '(' Opt_List_Expr ')' 
 { $$=mkGrp(PAst::GRP_CALL_EXPRESSION,{{PAst::EXPR_FUNCTION,$1},{$2},{PAst::EXPR_ARGUMENTS,$3},{$4}}); }

/* not a real expression, because that's going to interfere with assignment statements */
NamedExpr: IDENTIFIER OP_ASSIGN Expr { $$=mkGrp(PAst::GRP_NAMED_EXPRESSION,{{PAst::TOK_IDENTIFIER,$1},{$2},{PAst::EXPR_EXPRESSION,$3}}); }

Expr: Literal
| IDENTIFIER { $$=mkGrp(PAst::GRP_VARIABLE_REFERENCE_EXPRESSION,{{PAst::TOK_IDENTIFIER,$1}}); }
| IDENTIFIER OP_TYPE_MEMBER List_TypeMember { $$=mkGrp(PAst::GRP_TYPE_MEMBER_CHAIN_EXPRESSION,{{PAst::TOK_IDENTIFIER,$1},{$2},{PAst::TOK_IDENTIFIERS,$3}}); }
| PrimitiveType OP_TYPE_MEMBER List_TypeMember { $$=mkGrp(PAst::GRP_TYPE_MEMBER_CHAIN_EXPRESSION,{{PAst::REL_TYPE,$1},{$2},{PAst::TOK_IDENTIFIERS,$3}}); }
| TypeOf OP_TYPE_MEMBER List_TypeMember { $$=mkGrp(PAst::GRP_TYPE_MEMBER_CHAIN_EXPRESSION,{{PAst::REL_TYPE,$1},{$2},{PAst::TOK_IDENTIFIERS,$3}}); }
| OP_TRY Expr OP_ELSE Expr { $$=mkGrp(PAst::GRP_TRY_EXPRESSION,{{$1},{PAst::EXPR_LHS,$2},{$3},{PAst::EXPR_RHS,$4}}); }
| OP_NEW Type  '(' Opt_List_Expr ')' { $$=mkGrp(PAst::GRP_NEW_EXPRESSION,{$1,{PAst::REL_TYPE,$2},{$3},{PAst::EXPR_ARGUMENTS,$4},{$5}}); }
| OP_NEW Type  '(' List_NamedExpr ')' { $$=mkGrp(PAst::GRP_NEW_EXPRESSION,{$1,{PAst::REL_TYPE,$2},{$3},{PAst::EXPR_ARGUMENTS,$4},{$5}}); }
| Expr OP_ORELSE Expr { $$=mkGrp(PAst::GRP_ORELSE_EXPRESSION,{{PAst::EXPR_LHS,$1},$2,{PAst::EXPR_RHS,$3}}); }
| OP_SAFE_TYPECAST  OP_LT Type OP_GT '(' Expr ')' { $$=mkGrp(PAst::GRP_SAFE_TYPECAST_EXPRESSION,{$1,$2,{PAst::REL_TYPE,$3},$4,$5,{PAst::EXPR_EXPRESSION,$6},$7}); } 
| OP_UNSAFE_TYPECAST  OP_LT Type OP_GT '(' Expr ')' { $$=mkGrp(PAst::GRP_UNSAFE_TYPECAST_EXPRESSION,{$1,$2,{PAst::REL_TYPE,$3},$4,$5,{PAst::EXPR_EXPRESSION,$6},$7}); } 
| OP_CLAMP_TYPECAST  OP_LT Type OP_GT '(' Expr ')' { $$=mkGrp(PAst::GRP_CLAMP_TYPECAST_EXPRESSION,{$1,$2,{PAst::REL_TYPE,$3},$4,$5,{PAst::EXPR_EXPRESSION,$6},$7}); } 
| OP_WRAP_TYPECAST  OP_LT Type OP_GT '(' Expr ')' { $$=mkGrp(PAst::GRP_WRAP_TYPECAST_EXPRESSION,{$1,$2,{PAst::REL_TYPE,$3},$4,$5,{PAst::EXPR_EXPRESSION,$6},$7}); } 
| OP_TRANSFORM  OP_LT OPT_FQNType OP_GT '(' List_Expr ')' { $$=mkGrp(PAst::GRP_TRANSFORM_EXPRESSION,{$1,$2,{PAst::REL_TYPE,$3},$4,$5,{PAst::EXPR_ARGUMENTS,$6},$7}); } 
| '(' Opt_List_Parameter ')' Opt_ReturnType  LambdaBody { $$=mkGrp(PAst::GRP_LAMBDA_EXPRESSION,{$1,{PAst::REL_PARAMETERS,$2},$3,{PAst::REL_TYPE,$4},{PAst::REL_BODY,$5}}); } 
| OP_WITH List_WithVariable Opt_Separator_WithVariable DoExpr { $$=mkGrp(PAst::GRP_WITH_EXPRESSION,{{$1},{PAst::REL_DECLS,$2},{$3},{PAst::EXPR_EXPRESSION,$4}}); } 
| OP_FOR ForVariable OP_LIST Expr DoExpr  { $$=mkGrp(PAst::GRP_LOOP_EXPRESSION,{{$1},{PAst::REL_DECL,$2},{$3},{PAst::EXPR_CONDITION,$4},{PAst::EXPR_EXPRESSION,$5}}); }
| '(' Expr ')' { $$=mkGrp(PAst::GRP_SUB_EXPRESSION,{{$1},{PAst::EXPR_EXPRESSION,$2},{$3}}); }
| Expr OP_CONCATENATE Expr { $$=mkGrp(PAst::GRP_CONCATENATE_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); } 
| Expr OP_HASH Expr %prec OP_CONCATENATE { $$=mkGrp(PAst::GRP_ZIP_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); } 
| Expr OP_MERGE Expr %prec OP_CONCATENATE { $$=mkGrp(PAst::GRP_MERGE_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); } 
/* deref a pointer or a member */
| OP_BITWISE_AND Expr %prec OP_DEREF { $$=mkGrp(PAst::GRP_OPTIFY_EXPRESSION,{$1,{PAst::EXPR_EXPRESSION,$2}}); }
| OP_MULT Expr %prec OP_DEREF { $$=mkGrp(PAst::GRP_DEREF_EXPRESSION,{{$1},{PAst::EXPR_EXPRESSION,$2}}); }
/* indexing into an array */
| Expr '[' Expr ']' { $$=mkGrp(PAst::GRP_INDEX_EXPRESSION,{{PAst::EXPR_LHS,$1},{$2},{PAst::EXPR_RHS,$3},{$4}}); }
/* subrange of an array or string */
| Expr '[' Expr ':' Expr ']' { $$=mkGrp(PAst::GRP_SUBRANGE_EXPRESSION,{{PAst::EXPR_EXPRESSION,$1},{$2},{PAst::EXPR_BEGIN,$3},{$4},{PAst::EXPR_END,$5},$6}); }
| Expr '[' ':' Expr ']' { $$=mkGrp(PAst::GRP_SUBRANGE_EXPRESSION,{{PAst::EXPR_EXPRESSION,$1},{$2},{$3},{PAst::EXPR_END,$4},$5}); }
| Expr '[' Expr ':' ']' { $$=mkGrp(PAst::GRP_SUBRANGE_EXPRESSION,{{PAst::EXPR_EXPRESSION,$1},{$2},{PAst::EXPR_BEGIN,$3},{$4},$5}); }
/* a function call */
| FunctionCallExpr
| GENERIC Expr '{' Opt_List_Type '}' { $$=mkGrp(PAst::GRP_INSTANTIATE_EXPRESSION,{{$1},{PAst::EXPR_EXPRESSION,$2},{$3},{PAst::REL_PARAMETER_TYPES,$4},{$5}}); }

/* arithmetic ops */
| Expr OP_PLUS Expr { $$=mkGrp(PAst::GRP_ARITHMETIC_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); } 
| Expr OP_MINUS Expr { $$=mkGrp(PAst::GRP_ARITHMETIC_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); } 
| Expr OP_MULT Expr { $$=mkGrp(PAst::GRP_ARITHMETIC_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); } 
| Expr OP_DIV Expr { $$=mkGrp(PAst::GRP_ARITHMETIC_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); } 
| Expr OP_MOD Expr { $$=mkGrp(PAst::GRP_ARITHMETIC_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); }
| Expr OP_REM Expr { $$=mkGrp(PAst::GRP_ARITHMETIC_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); }
| OP_MINUS Expr %prec OP_UNARY_MINUS { $$=mkGrp(PAst::GRP_NEGATE_EXPRESSION,{{PAst::TOK_OP,$1},{PAst::EXPR_EXPRESSION,$2}}); } 
/* comparison ops */
| Expr OP_EQ Expr { $$=mkGrp(PAst::GRP_COMPARISON_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); } 
| Expr OP_NEQ Expr { $$=mkGrp(PAst::GRP_COMPARISON_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); } 
| Expr OP_LE Expr { $$=mkGrp(PAst::GRP_COMPARISON_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); } 
| Expr OP_LT Expr { $$=mkGrp(PAst::GRP_COMPARISON_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); } 
| Expr OP_GE Expr { $$=mkGrp(PAst::GRP_COMPARISON_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); } 
| Expr OP_GT Expr { $$=mkGrp(PAst::GRP_COMPARISON_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); }
/* logical ops */
| Expr OP_LOGICAL_AND Expr { $$=mkGrp(PAst::GRP_LOGICAL_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); } 
| Expr OP_LOGICAL_OR Expr { $$=mkGrp(PAst::GRP_LOGICAL_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); } 
| OP_LOGICAL_NOT Expr { $$=mkGrp(PAst::GRP_NEGATE_EXPRESSION,{{PAst::TOK_OP,$1},{PAst::EXPR_EXPRESSION,$2}}); } 
/* bit ops */
| Expr OP_BITWISE_AND Expr { $$=mkGrp(PAst::GRP_BITOP_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); } 
| Expr OP_BITWISE_OR Expr { $$=mkGrp(PAst::GRP_BITOP_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); } 
| Expr OP_BITWISE_XOR Expr { $$=mkGrp(PAst::GRP_BITOP_EXPRESSION,{{PAst::EXPR_LHS,$1},{PAst::TOK_OP,$2},{PAst::EXPR_RHS,$3}}); }
| OP_BITWISE_NOT Expr { $$=mkGrp(PAst::GRP_NEGATE_EXPRESSION,{{PAst::TOK_OP,$1},{PAst::EXPR_EXPRESSION,$2}}); }
/* ternary ops */
| Expr OP_QUESTIONMARK Expr ':' Expr  { $$=mkGrp(PAst::GRP_CONDITIONAL_EXPRESSION,{{PAst::EXPR_CONDITION,$1},{$2},{PAst::EXPR_IFTRUE,$3},{$4},{PAst::EXPR_IFFALSE,$5}}); }
/* errors */
| '(' error ')' {  $$=mkGrp(PAst::GRP_SYNTAX_ERROR,{$1,$2,$3}); }
| Expr '(' error ')' { $$=mkGrp(PAst::GRP_SYNTAX_ERROR,{$1,$2,$3}); }
| Expr '[' error ']' { $$=mkGrp(PAst::GRP_SYNTAX_ERROR,{$1,$2,$3,$4}); }
/* member operator (default uses an identifier) */
| Expr OP_APPLY RESERVED_IDENTIFIER { $$=mkGrp(PAst::GRP_APPLY_EXPRESSION,{{PAst::EXPR_EXPRESSION,$1},{$2},{PAst::TOK_IDENTIFIER,$3}}); }
| Expr OP_MEMBER RESERVED_IDENTIFIER { $$=mkGrp(PAst::GRP_MEMBER_EXPRESSION,{{PAst::EXPR_EXPRESSION,$1},{$2},{PAst::TOK_IDENTIFIER,$3}}); }
/* an interpolated string */
| BEGIN_QUOTE Opt_List_TextOrExpression END_QUOTE { $$=mkGrp(PAst::GRP_INTERPOLATE_TEXT_EXPRESSION,{$1,{PAst::EXPR_ARGUMENTS,$2}, $3}); }

/** reserved words that can be used as an identifier in certain circumstances; primarily to make it easier to introduce new builtin constructs */
RESERVED_IDENTIFIER: IDENTIFIER
| OP_NEW
| WAIT
| TYPE_TUPLE
;

Parameter: IDENTIFIER ParameterType { $$=mkGrp(PAst::GRP_DEF_PARAMETER,{{PAst::TOK_IDENTIFIER,$1},{PAst::REL_TYPE,$2}}); }

TextOrExpression:
 QUOTED_TEXT   { $$=mkGrp(PAst::GRP_QUOTE_TEXT_EXPRESSION,{{PAst::TOK_LITERAL,$1}}); }
| '{' Expr '}'  { $$=mkGrp(PAst::GRP_STRINGIFY_EXPRESSION,{$1,{PAst::EXPR_EXPRESSION,$2},$3}); }  

LambdaBody : 
StmtBlock { $$=mkGrp(PAst::GRP_LAMBDA_BODY,{{PAst::REL_STATEMENTS,$1}}); }
| OP_ASSIGN Expr  { $$=mkGrp(PAst::GRP_LAMBDA_BODY,{$1,{PAst::EXPR_EXPRESSION,$2}}); }

DoExpr: '{' Expr '}'{ $$=mkGrp(PAst::GRP_SUB_EXPRESSION,{{$1},{PAst::EXPR_EXPRESSION,$2},{$3}}); }
| DO Expr  { $$=mkGrp(PAst::GRP_SUB_EXPRESSION,{{$1},{PAst::EXPR_EXPRESSION,$2}}); }

WithVariable:
 IDENTIFIER  Opt_VariableType Initializer
  { $$=mkGrp(PAst::GRP_DEF_VALUE,{{PAst::TOK_IDENTIFIER,$1},{PAst::REL_TYPE,$2},{PAst::EXPR_EXPRESSION,$3}}); }
| TYPENAME IDENTIFIER OP_ASSIGN Type
   { $$= mkGrp(PAst::GRP_DEF_TYPENAME,{$1,{PAst::TOK_IDENTIFIER,$2},$3,{PAst::REL_TYPE,$4}}); }
| error { $$=mkGrp(PAst::GRP_SYNTAX_ERROR,{$1}); }

ForVariable: IDENTIFIER  Opt_VariableType Initializer
  { $$=mkGrp(PAst::GRP_DEF_VARIABLE,{{PAst::TOK_IDENTIFIER,$1},{PAst::REL_TYPE,$2},{PAst::EXPR_EXPRESSION,$3}}); }
| error { $$=mkGrp(PAst::GRP_SYNTAX_ERROR,{$1}); }


Literal: 
  Literal_Integer
| Literal_Bit
| Literal_Boolean
| Literal_Byte
| Literal_Real
| Literal_Char
| Literal_String
| Literal_Nil
| '[' Opt_List_ArrayLiteralExpr ']' { $$=mkGrp(PAst::GRP_LITERAL_ARRAY,{$1,{PAst::EXPR_EXPRESSION,$2},$3}); }
| OP_HASH '{' Opt_List_Literal_Struct_Member '}' { $$=mkGrp(PAst::GRP_LITERAL_STRUCT,{$1,$2,{PAst::REL_MEMBERS,$3},$4}); }
| OP_HASH '(' Opt_List_Literal_Tuple_Member ')' { $$=mkGrp(PAst::GRP_LITERAL_TUPLE,{$1,$2,{PAst::REL_MEMBERS,$3},$4}); }

ArrayLiteralExpr : Expr | Literal_Bit_Sequence | Literal_Byte_Sequence;

Literal_Struct_Member: IDENTIFIER Opt_VariableType Initializer { $$=mkGrp(PAst::GRP_LITERAL_STRUCT_MEMBER,{{PAst::TOK_IDENTIFIER,$1},{PAst::REL_TYPE,$2},{PAst::EXPR_EXPRESSION,$3}}); }

Literal_Tuple_Member: 
  /* FIXME: I don't like having to use :Type=value, but we need to distinguish Type from Identifier */
  VariableType OP_ASSIGN Expr { $$=mkGrp(PAst::GRP_LITERAL_TUPLE_MEMBER,{{PAst::REL_TYPE,$1},$2,{PAst::EXPR_EXPRESSION,$3}}); }
| Expr { $$=mkGrp(PAst::GRP_LITERAL_TUPLE_MEMBER,{{PAst::EXPR_EXPRESSION,$1}}); }

Literal_Integer: 
  LITERAL_DECIMAL_INTEGER   { $$=mkGrp(PAst::GRP_LITERAL_DECIMAL_INTEGER,{{PAst::TOK_LITERAL,$1}}); }
| LITERAL_HEX_INTEGER   { $$=mkGrp(PAst::GRP_LITERAL_HEX_INTEGER,{{PAst::TOK_LITERAL,$1}}); }
| LITERAL_OCTAL_INTEGER   { $$=mkGrp(PAst::GRP_LITERAL_OCTAL_INTEGER,{{PAst::TOK_LITERAL,$1}}); }

Literal_Bit_Sequence:
  LITERAL_BIT_SEQUENCE { $$=mkGrp(PAst::GRP_LITERAL_BIT_SEQUENCE,{{PAst::TOK_LITERAL,$1}}); }

Literal_Bit:
  LITERAL_BIT       { $$=mkGrp(PAst::GRP_LITERAL_BIT,{{PAst::TOK_LITERAL,$1}}); }

Literal_Boolean:
  LITERAL_BOOLEAN  { $$=mkGrp(PAst::GRP_LITERAL_BOOLEAN,{{PAst::TOK_LITERAL,$1}}); }

Literal_Byte_Sequence:
  LITERAL_BYTE_SEQUENCE { $$=mkGrp(PAst::GRP_LITERAL_BYTE_SEQUENCE,{{PAst::TOK_LITERAL,$1}}); }

Literal_Byte:
  LITERAL_BYTE  { $$=mkGrp(PAst::GRP_LITERAL_BYTE,{{PAst::TOK_LITERAL,$1}}); }

Literal_Char:
  LITERAL_CHAR { $$=mkGrp(PAst::GRP_LITERAL_CHAR,{{PAst::TOK_LITERAL,$1}}); }

Literal_String:
  LITERAL_STRING { $$=mkGrp(PAst::GRP_LITERAL_STRING,{{PAst::TOK_LITERAL,$1}}); }

Literal_Real:
  LITERAL_REAL { $$=mkGrp(PAst::GRP_LITERAL_REAL,{{PAst::TOK_LITERAL,$1}}); }

Literal_Nil:
  LITERAL_NIL { $$=mkGrp(PAst::GRP_LITERAL_NIL,{{PAst::TOK_LITERAL,$1}}); }


////////////////////////////////////////////////////////////////////////////
// optionals
////////////////////////////////////////////////////////////////////////////
Opt_Separator_WithVariable: { $$=mkIgn(); } | Separator_WithVariable;
Opt_Separator_Expr: { $$=mkIgn(); } | Separator_Expr;
Opt_VariableType:  { $$=mkIgn(); } | VariableType;
Opt_ReturnType: { $$=mkIgn(); } | ReturnType;
Opt_ReturnTypePattern: { $$=mkIgn(); } | ReturnTypePattern;
Opt_ParameterType: { $$=mkIgn(); } | ParameterType;
Opt_Initializer: { $$=mkIgn(); } | Initializer;
Opt_Expr: { $$=mkIgn(); } | Expr;
Opt_Call_Process_Constructor: { $$=mkIgn(); } | Call_Process_Constructor;

Opt_List_TextOrExpression:  { $$=mkIgn(); } | List_TextOrExpression;

Opt_List_MacroParameter:  { $$=mkIgn(); } | List_MacroParameter;
Opt_List_Parameter:  { $$=mkIgn(); } | List_Parameter;
Opt_List_StructMember:  { $$=mkIgn(); } | List_StructMember;
Opt_List_Expr:  { $$=mkIgn(); } | List_Expr;
Opt_List_ArrayLiteralExpr:  { $$=mkIgn(); } | List_ArrayLiteralExpr;
Opt_List_Type:  { $$=mkIgn(); } | List_Type;
Opt_List_TypePattern:  { $$=mkIgn(); } | List_TypePattern;
Opt_List_Definition:  { $$=mkIgn(); } | List_Definition;
Opt_List_TypeName:  { $$=mkIgn(); } | List_TypeName;
Opt_List_Literal_Struct_Member: { $$=mkIgn(); } | List_Literal_Struct_Member;
Opt_List_Literal_Tuple_Member: { $$=mkIgn(); } | List_Literal_Tuple_Member;
Opt_List_TypeConstraint: { $$=mkIgn(); } | List_TypeConstraint;
Opt_List_TypePatternConstraint: { $$=mkIgn(); } | List_TypePatternConstraint;
Opt_List_Import: { $$=mkIgn(); } | List_Import;
Opt_List_Stmt: { $$=mkIgn(); } | List_Stmt;
Opt_List_Process_Stmt: { $$=mkIgn(); } | List_Process_Stmt;
Opt_List_ProcessMember: { $$=mkIgn(); } | List_ProcessMember;
Opt_List_Generic: { $$=mkIgn(); } | List_Generic;
Opt_List_GenericParameter: { $$=mkIgn(); } | List_GenericParameter;
Opt_Export: { $$=mkIgn(); } | Export;

////////////////////////////////////////////////////////////////////////////
// list productions
////////////////////////////////////////////////////////////////////////////
Separator_Expr: OP_LIST
Separator_WithVariable: ';'
Separator_Parameter: OP_LIST
List_Expr: Expr | List_Expr Separator_Expr Expr  { $$=join({$1,mkIgn($2),$3}); }
List_Type: Type | List_Type Separator_Expr Type  { $$=join({$1,mkIgn($2),$3}); }
List_TypePattern: TypePattern | List_TypePattern Separator_Expr TypePattern  { $$=join({$1,mkIgn($2),$3}); }
List_WithVariable: WithVariable | List_WithVariable Separator_WithVariable WithVariable {  $$=join({$1,mkIgn($2),$3}); } 
List_Parameter: Parameter | List_Parameter Separator_Parameter Parameter {  $$=join({$1,mkIgn($2),$3}); }
List_TransformParameter: TransformParameter | List_TransformParameter Separator_Parameter TransformParameter {  $$=join({$1,mkIgn($2),$3}); }
List_GenericParameter: GenericParameter | List_GenericParameter Separator_Parameter GenericParameter {  $$=join({$1,mkIgn($2),$3}); }
List_MacroParameter: MacroParameter | List_MacroParameter Separator_Parameter MacroParameter {  $$=join({$1,mkIgn($2),$3}); }
List_ProcessMember: ProcessMember | List_ProcessMember Separator_WithVariable ProcessMember { $$=join({$1,mkIgn($2),$3}); }
List_Identifier: IDENTIFIER | List_Identifier OP_MEMBER IDENTIFIER { $$=join({$1,mkIgn($2),$3}); }
List_TypeMember: RESERVED_IDENTIFIER | List_TypeMember OP_TYPE_MEMBER RESERVED_IDENTIFIER { $$=join({$1,mkIgn($2),$3}); }
List_Definition: Definition | List_Definition Definition { $$=join({$1,$2}); }
List_TypeName: TypeName | List_TypeName TypeName { $$=join({$1,$2}); }
List_Literal_Struct_Member: Literal_Struct_Member | List_Literal_Struct_Member Separator_Expr Literal_Struct_Member { $$=join({$1,mkIgn($2),$3}); }
List_Literal_Tuple_Member: Literal_Tuple_Member | List_Literal_Tuple_Member Separator_Expr Literal_Tuple_Member { $$=join({$1,mkIgn($2),$3}); }
List_TypeConstraint: TypeConstraint | List_TypeConstraint Separator_WithVariable TypeConstraint { $$=join({$1,mkIgn($2),$3}); }
List_TypePatternConstraint: TypePatternConstraint | List_TypePatternConstraint Separator_WithVariable TypePatternConstraint { $$=join({$1,mkIgn($2),$3}); }
List_Import: Import | List_Import Import { $$=join({$1,$2}); }
List_Stmt: Stmt | List_Stmt Stmt { $$=join({$1,$2}); }
List_Process_Stmt: Process_Stmt | List_Process_Stmt Process_Stmt { $$=join({$1,$2}); }
List_NamedExpr: NamedExpr | List_NamedExpr Separator_Expr NamedExpr  { $$=join({$1,mkIgn($2),$3}); }
List_Generic: Generic | List_Generic Separator_Parameter Generic { $$=join({$1,mkIgn($2),$3}); }
List_TextOrExpression: TextOrExpression | List_TextOrExpression TextOrExpression { $$=join({$1,$2}); }

///////////////////////////////////////////////////////////
// do we really want to allow trailing chars
// it makes it easier to add new fields, entries
///////////////////////////////////////////////////////////
List_ArrayLiteralExpr2: ArrayLiteralExpr | List_ArrayLiteralExpr2 Separator_Expr ArrayLiteralExpr  { $$=join({$1,mkIgn($2),$3}); }
List_ArrayLiteralExpr: List_ArrayLiteralExpr2 Opt_Separator_Expr  { $$=join({$1,mkIgn($2)}); }
List_StructMember2: StructMember | List_StructMember2 Separator_WithVariable StructMember { $$=join({$1,mkIgn($2),$3}); }
List_UnionMember2: UnionMember | List_UnionMember2 Separator_WithVariable UnionMember { $$=join({$1,mkIgn($2),$3}); }
List_StructMember: List_StructMember2 Opt_Separator_WithVariable { $$=join({$1,mkIgn($2)}); }
List_UnionMember: List_UnionMember2 Opt_Separator_WithVariable { $$=join({$1,mkIgn($2)}); }
