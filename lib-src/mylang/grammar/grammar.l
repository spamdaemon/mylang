
ESCAPE_CHAR [0abtnvfr"'?\\]
CHAR_CHAR   []"0-9a-zA-Z !#$%&()*+,./:;<=>?@[^_`{|}~-]
STRING_CHAR []'0-9a-zA-Z !#$%&()*+,-./:;<=>?@[^_`{|}~-]

LETTER     [a-zA-Z]
DIGIT      [0-9]
LETTERORDIGIT ({LETTER}|{DIGIT})

HEX        [0-9A-Fa-f]
OCTAL      [0-7]
BYTE       {HEX}{HEX}
INTEGER    (0|([1-9]+{DIGIT}*))
REAL       {INTEGER}[.]{DIGIT}+
WHITESPACE [ \t]
NEWLINE    [\n\v\f\r]|(\r\n)|(\n\r)

IDENTIFIER {LETTER}(({LETTER}|{DIGIT}|[_])*({LETTER}|{DIGIT}))?

UNICODE_CHAR (u{HEX}{4})|(U{HEX}{8})

LINE_COMMENT \/\/[^\r\n\f\v]*
/* support multiple characters for char in order to support unicode literals */
CHAR   \'(({CHAR_CHAR})|(\\({ESCAPE_CHAR}|{UNICODE_CHAR})))\'
STRING \"(({STRING_CHAR})|(\\({ESCAPE_CHAR}|{UNICODE_CHAR})))*\"

%{
int push_state=1;
%}

%s initial_state
%x mlcomment mlquote
%option stack

%%
                         { if (YYSTATE == INITIAL) {yy_push_state(initial_state,yyscanner); } }

<*>{NEWLINE}             { yyextra[1].newline(yytext[0]!='\f' && yytext[0]!='\v'); }

 /* supporting nested comments */
"/*"                     { yy_push_state(mlcomment,yyscanner); }
<mlcomment>"/*"          { yy_push_state(mlcomment,yyscanner); }
<mlcomment>"*/"          { yy_pop_state(yyscanner); }
<mlcomment>.             { }

 /* multiple text with embedded expressions ($ and % are reserved at this time) */
"''"                       { yy_push_state(mlquote,yyscanner); return tokens::BEGIN_QUOTE; }
<mlquote>''                { yy_pop_state(yyscanner); return tokens::END_QUOTE; }
<mlquote>\\[{$%'\\]        { yytext[0] = yytext[1]; yytext[1]='\0'; yyleng=1; return tokens::QUOTED_TEXT; }
<mlquote>[^%${'\\]+        { return tokens::QUOTED_TEXT; }
<mlquote>[{]               { yy_push_state(initial_state,yyscanner); return yytext[0]; }
<mlquote>[']               { return tokens::QUOTED_TEXT; }
<mlquote>[\\]              { return tokens::QUOTED_TEXT; }
<mlquote>.                 { return -1; }

 /* braces must come in pairs and always bring the scanner into expression parsing mode; this is needed for 
  interpolated text */
[{]                      { yy_push_state(initial_state,yyscanner); return yytext[0]; }
[}]                      { yy_pop_state(yyscanner); return yytext[0]; }

{LINE_COMMENT}           { }
{WHITESPACE}             { }
_						 { return tokens::PLACEHOLDER; }
{INTEGER}                { return tokens::LITERAL_DECIMAL_INTEGER; }
0{OCTAL}+                { return tokens::LITERAL_OCTAL_INTEGER; }
0x{HEX}+                 { return tokens::LITERAL_HEX_INTEGER; }
{REAL}                   { return tokens::LITERAL_REAL; }
{CHAR}                   { return tokens::LITERAL_CHAR; }
{STRING}                 { return tokens::LITERAL_STRING; }
true                     { return tokens::LITERAL_BOOLEAN; }
false                    { return tokens::LITERAL_BOOLEAN; }
0bx{BYTE}                { return tokens::LITERAL_BYTE; }
0bx{BYTE}{BYTE}+         { return tokens::LITERAL_BYTE_SEQUENCE; }
0b[01]                   { return tokens::LITERAL_BIT; }
0b[01][01]+              { return tokens::LITERAL_BIT_SEQUENCE; }
nil                      { return tokens::LITERAL_NIL; }
do                       { return tokens::DO; }
mutable                  { return tokens::MUTABLE; }
bit                      { return tokens::TYPE_BIT; }
boolean                  { return tokens::TYPE_BOOLEAN; }
byte                     { return tokens::TYPE_BYTE; }
char                     { return tokens::TYPE_CHAR; }
real                     { return tokens::TYPE_REAL; }
integer                  { return tokens::TYPE_INTEGER; }
string                   { return tokens::TYPE_STRING; }
void                     { return tokens::TYPE_VOID; }
tuple                    { return tokens::TYPE_TUPLE; }
struct                   { return tokens::TYPE_STRUCT; }
union                    { return tokens::TYPE_UNION; }
generic                  { return tokens::GENERIC; }
typeof                   { return tokens::TYPEOF; }
unit                     { return tokens::NAMESPACEDEF; }
with                     { return tokens::OP_WITH; }
safe_cast                { return tokens::OP_SAFE_TYPECAST; }
unsafe_cast              { return tokens::OP_UNSAFE_TYPECAST; }
wrap_cast                { return tokens::OP_WRAP_TYPECAST; }
clamp_cast               { return tokens::OP_CLAMP_TYPECAST; }
transform                { return tokens::OP_TRANSFORM; }
new                      { return tokens::OP_NEW; }
try                      { return tokens::OP_TRY; }
throw                    { return tokens::OP_THROW; }
catch                    { return tokens::CATCH; }
continue				 { return tokens::CONTINUE; }
break					 { return tokens::BREAK; }
if                       { return tokens::OP_IF; }
else                     { return tokens::OP_ELSE; }
for                      { return tokens::OP_FOR; }
import                   { return tokens::IMPORT; }
alias                    { return tokens::ALIAS; }
assert                   { return tokens::ASSERT; }
extern                   { return tokens::EXTERN; }
export                   { return tokens::EXPORT; }
foreach                  { return tokens::FOREACH; }
log                      { return tokens::LOG; }
return                   { return tokens::RETURN; }
val                      { return tokens::VAL; }
var                      { return tokens::VAR; }
wait                     { return tokens::WAIT; }
while                    { return tokens::WHILE; }
typename                 { return tokens::TYPENAME; }
deftype                  { return tokens::TYPEDEF; }
defun                    { return tokens::FUNCTIONDEF; }
defext                   { return tokens::EXTENSIONDEF; }
defmacro                 { return tokens::MACRODEF; }
defproc                  { return tokens::DEFPROC; }
process                  { return tokens::PROCESS; }
{IDENTIFIER}             { return tokens::IDENTIFIER; }
[()[\]]                  { return yytext[0]; }
::                       { return tokens::OP_TYPE_MEMBER; }
:=                       { return tokens::OP_SET; }
\.                       { return tokens::OP_MEMBER; }
[:;]                     { return yytext[0]; }
##                       { return tokens::OP_MERGE; }
#                        { return tokens::OP_HASH; }
\<\<                     { return tokens::OP_SHIFT_LEFT; }
\>\>                     { return tokens::OP_SHIFT_RIGHT; }
=                        { return tokens::OP_ASSIGN; }
\+                       { return tokens::OP_PLUS; }
-                        { return tokens::OP_MINUS; }
\*                       { return tokens::OP_MULT; }
\/                       { return tokens::OP_DIV; }
\%\%                     { return tokens::OP_REM; }
\%                       { return tokens::OP_MOD; }
\?:                      { return tokens::OP_ORELSE; }
\?                       { return tokens::OP_QUESTIONMARK; }
,                        { return tokens::OP_LIST; }
=\>                      { return tokens::OP_FOLLOWS; }
==                       { return tokens::OP_EQ; }
!=                       { return tokens::OP_NEQ; }
\<=                      { return tokens::OP_LE; }
\>=                      { return tokens::OP_GE; }
\<                       { return tokens::OP_LT; }
\>                       { return tokens::OP_GT; }
\+\+	                 { return tokens::OP_CONCATENATE; }
&&                       { return tokens::OP_LOGICAL_AND; }
\|\|                     { return tokens::OP_LOGICAL_OR; }
!                        { return tokens::OP_LOGICAL_NOT; }
~                        { return tokens::OP_BITWISE_NOT; }
&                        { return tokens::OP_BITWISE_AND; }
\|                       { return tokens::OP_BITWISE_OR; }
\^                       { return tokens::OP_BITWISE_XOR; }
->                       { return tokens::OP_APPLY; }
<<EOF>>                  { yyterminate(); return 0; }
.                        { return -1; }

