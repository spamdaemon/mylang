#ifndef CLASS_MYLANG_GRAMMAR_H
#define CLASS_MYLANG_GRAMMAR_H
#include <mylang/defs.h>

namespace mylang {
  namespace grammar {
    /**
     * This class defineds the nodes for the Parsed Ast, or PAst for short.
     */
    struct PAst
    {
      enum RelationType : int
      {
        // tokens
        TOK_IDENTIFIER,
        TOK_IDENTIFIERS,
        TOK_OP,
        TOK_LITERAL,
        TOK_TYPE,
        TOK_FILE,
        TOK_LOG_LEVEL,

        TOK_KEYWORD, // used make sure that certain nodes are not removed from the parse tree so we have location information

        // expressions
        EXPR_LHS, // LHS in a binary expression
        EXPR_RHS, // RHS in a binary expression
        EXPR_EXPRESSION, // an expression
        EXPR_CONDITION, // a conditional expression (used in ternary)
        EXPR_IFTRUE, // true branch in a conditional statement or expression
        EXPR_IFFALSE, // false branch in a conditional statement or expression
        EXPR_FUNCTION, // an expression whose type must be a function
        EXPR_ARGUMENTS, // maybe change this to EXPR_EXPRESSIONS??
        EXPR_BEGIN,
        EXPR_END,
        EXPR_MESSAGE,

        REL_EXPORTED,
        REL_GENERICS,
        REL_STATEMENTS,
        REL_PARAMETERS,
        REL_PARAMETER_TYPES,
        REL_VARIABLES,
        REL_VAR_KIND,
        REL_SIZE,
        REL_MIN_SIZE,
        REL_MAX_SIZE,
        REL_MEMBERS,
        REL_DISCRIMINANT,
        REL_TYPE,
        REL_DECLS,
        REL_DECL,
        REL_BODY,
        REL_CONSTRAINTS,
        REL_TYPE_AS_EXPR,
        REL_INIT_STATEMENT,
        REL_PATTERN,
        REL_PATTERNS,
        REL_DEF_PATTERNS,
        REL_DIMENSION,

        //  or statement
        STMT_IFTRUE, // true branch in a conditional statement or expression
        STMT_IFFALSE, // false branch in a conditional statement or expression
        STMT_TRY, // true branch in a conditional statement or expression
        STMT_CATCH, // false branch in a conditional statement or expression
        REL_FQN,   // a fully qualified name
        REL_QN,   // a qualified name
        REL_IMPORTS,
        REL_FILE,
        REL_LAST
      };
      enum GroupType : int
      {
        GRP_ROOT, GRP_SYNTAX_ERROR, GRP_IGNORABLE, GRP_IMPORT,

        // references
        GRP_REFERENCE,

        // definitions
        GRP_DEF_TRANSFORM,
        GRP_DEF_NAMESPACE,
        GRP_DEF_TYPE,
        GRP_DEF_TYPENAME,
        GRP_DEF_FUNCTION,
        GRP_DEF_EXTENSION_FUNCTION,
        GRP_DEF_GENERIC_FUNCTION,
        GRP_DEF_GENERIC_EXTENSION_FUNCTION,
        GRP_DEF_VALUE,
        GRP_DEF_VARIABLE,
        GRP_DEF_PARAMETER,
        GRP_DEF_MACRO,
        GRP_DEF_PROCESS,
        GRP_DEF_ALIAS,

        GRP_PROCESS_CONSTRUCTOR,
        GRP_PROCESS_BLOCK,
        GRP_PROCESS_INPUT,
        GRP_PROCESS_OUTPUT,

        // patterns
        GRP_GROUPED_PATTERN,
        GRP_PATTERN_ANY,
        GRP_PATTERN_PRIMITIVE,
        GRP_PATTERN_ARRAY,
        GRP_PATTERN_FUNCTION,
        GRP_PATTERN_MUTABLE,
        GRP_PATTERN_OPT,
        GRP_PATTERN_BIT,
        GRP_PATTERN_BOOLEAN,
        GRP_PATTERN_BYTE,
        GRP_PATTERN_CHAR,
        GRP_PATTERN_INTEGER,
        GRP_PATTERN_REAL,
        GRP_PATTERN_STRING,
        GRP_PATTERN_STRUCT,
        GRP_PATTERN_TUPLE,
        GRP_PATTERN_UNION,
        GRP_NAMED_TYPE_PATTERN,
        GRP_CONSTRAINED_TYPE_PATTERN,
        GRP_DEF_TYPE_PATTERN,
        GRP_DEF_VALUE_PATTERN,
        GRP_DEF_GENERIC_PARAMETER,
        GRP_ARRAY_DIMENSION_PATTERN,
        GRP_ARRAY_ANY_DIMENSION_PATTERN,

        // primitive types
        GRP_GROUPED_TYPE,
        GRP_TYPE_BIT,
        GRP_TYPE_BOOLEAN,
        GRP_TYPE_BYTE,
        GRP_TYPE_CHAR,
        GRP_TYPE_REAL,
        GRP_TYPE_FUNCTION,
        GRP_TYPE_INTEGER,
        GRP_TYPE_STRING,
        GRP_TYPE_VOID,
        GRP_TYPE_GENERIC,
        // composite types
        GRP_TYPE_ARRAY,
        GRP_TYPE_MUTABLE,
        GRP_TYPE_OPT,
        GRP_TYPE_TUPLE,
        GRP_TYPE_STRUCT,
        GRP_TYPE_UNION,
        GRP_TYPE_PROCESS,
        GRP_TYPENAME,
        GRP_TYPEOF,
        GRP_TYPE_MEMBER,
        GRP_CONSTRAINED_TYPE,

        GRP_NAMED_CONSTRAINT,
        GRP_ARRAY_DIMENSION,

        // literals (expressions)
        GRP_LITERAL_BIT,
        GRP_LITERAL_BIT_SEQUENCE,
        GRP_LITERAL_BOOLEAN,
        GRP_LITERAL_BYTE,
        GRP_LITERAL_BYTE_SEQUENCE,
        GRP_LITERAL_CHAR,
        GRP_LITERAL_REAL,
        GRP_LITERAL_DECIMAL_INTEGER,
        GRP_LITERAL_HEX_INTEGER,
        GRP_LITERAL_OCTAL_INTEGER,
        GRP_LITERAL_STRING,
        // arrays are semi-literals; size is compiled time fixed, but contents are expressions
        GRP_LITERAL_ARRAY,
        GRP_LITERAL_TUPLE,
        GRP_LITERAL_STRUCT,
        GRP_LITERAL_NIL,
        // expressions
        GRP_INTERPOLATE_TEXT_EXPRESSION,
        GRP_QUOTE_TEXT_EXPRESSION,
        GRP_STRINGIFY_EXPRESSION,

        GRP_NAMED_EXPRESSION,
        GRP_VARIABLE_REFERENCE_EXPRESSION,
        GRP_LOOP_EXPRESSION,
        GRP_NEW_EXPRESSION,
        GRP_TRY_EXPRESSION,
        GRP_ORELSE_EXPRESSION,
        GRP_OPTIFY_EXPRESSION,
        GRP_ARITHMETIC_EXPRESSION,
        GRP_NEGATE_EXPRESSION,
        GRP_WITH_EXPRESSION,
        GRP_CONDITIONAL_EXPRESSION,
        GRP_LOGICAL_EXPRESSION,
        GRP_BITOP_EXPRESSION,
        GRP_COMPARISON_EXPRESSION,
        GRP_SUB_EXPRESSION,
        GRP_MEMBER_EXPRESSION,
        GRP_TYPE_MEMBER_CHAIN_EXPRESSION,
        GRP_APPLY_EXPRESSION,
        GRP_DEREF_EXPRESSION,
        GRP_INDEX_EXPRESSION,
        GRP_CALL_EXPRESSION,
        GRP_INSTANTIATE_EXPRESSION,
        GRP_LAMBDA_EXPRESSION,
        GRP_SAFE_TYPECAST_EXPRESSION,
        GRP_UNSAFE_TYPECAST_EXPRESSION,
        GRP_CLAMP_TYPECAST_EXPRESSION,
        GRP_WRAP_TYPECAST_EXPRESSION,
        GRP_SUBRANGE_EXPRESSION,
        GRP_CONCATENATE_EXPRESSION,
        GRP_MERGE_EXPRESSION,
        GRP_ZIP_EXPRESSION,
        GRP_TRANSFORM_EXPRESSION,

        // helpers
        GRP_STRUCT_MEMBER,
        GRP_TYPE_PROCESS_INPUT_MEMBER,
        GRP_TYPE_PROCESS_OUTPUT_MEMBER,
        GRP_UNION_MEMBER,
        GRP_DISCRIMINANT,
        GRP_FUNCTION_BODY,
        GRP_LAMBDA_BODY,
        GRP_MACRO_BODY,
        GRP_LITERAL_STRUCT_MEMBER,
        GRP_LITERAL_TUPLE_MEMBER,

        // statements
        GRP_ASSERT_STMT,
        GRP_BREAK_STMT,
        GRP_CONTINUE_STMT,
        GRP_CALL_CONSTRUCTOR_STMT,
        GRP_EXPRESSION_STMT,
        GRP_FOREACH_STMT,
        GRP_IF_STMT,
        GRP_LOG_STMT,
        GRP_RETURN_STMT,
        GRP_SET_STMT,
        GRP_STATEMENT_BLOCK,
        GRP_TRY_STMT,
        GRP_THROW_STMT,
        GRP_UPDATE_STMT,
        GRP_VAL_STMT,
        GRP_VAR_STMT,
        GRP_WAIT_STMT,
        GRP_WHILE_STMT,

        // the error
        GRP_LAST
      };

    public:
      class Visitor
      {
      public:
        virtual ~Visitor() = 0;

      public:
        GroupNodeCPtr visit(const GroupNodeCPtr &node);
        void visitChildren(const GroupNodeCPtr &node);

      public:
        // an error has occurred!
        virtual GroupNodeCPtr visit_syntax_error(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_root(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_import(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_assert_stmt(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_break_stmt(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_continue_stmt(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_call_constructor_stmt(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_expression_stmt(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_foreach_stmt(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_if_stmt(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_log_stmt(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_return_stmt(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_set_stmt(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_try_stmt(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_throw_stmt(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_update_stmt(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_val_stmt(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_var_stmt(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_wait_stmt(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_while_stmt(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_stmt_block(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_literal_bit(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_literal_bit_sequence(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_literal_boolean(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_literal_byte(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_literal_byte_sequence(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_literal_char(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_literal_decimal_integer(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_literal_hex_integer(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_literal_octal_integer(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_literal_real(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_literal_array(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_literal_tuple(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_literal_tuple_member(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_literal_struct(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_literal_struct_member(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_literal_string(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_literal_nil(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_interpolate_text_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_quote_text_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_stringify_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_variable_reference_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_named_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_new_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_arithmetic_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_negate_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_loop_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_with_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_bitop_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_conditional_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_comparison_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_sub_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_member_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_member_chain_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_apply_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_optify_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_deref_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_index_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_subrange_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_call_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_instantiate_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_lambda_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_concatenate_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_merge_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_zip_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_transform_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_safe_cast_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_unsafe_cast_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_clamp_cast_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_wrap_cast_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_orelse_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_try_expression(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_typeof(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_grouped(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_bit(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_boolean(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_byte(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_char(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_real(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_function(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_integer(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_string(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_array(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_tuple(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_struct(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_union(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_void(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_generic(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_process(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_process_input_member(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_process_output_member(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_member(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_struct_member(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_union_member(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_discriminant(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_function_body(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_lambda_body(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_mutable(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_type_opt(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_typename(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_constrained_type(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_named_constraint(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_macro_body(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_def_parameter(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_def_value(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_def_variable(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_def_transform(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_def_type(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_def_typename(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_def_namespace(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_def_function(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_def_extension_function(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_def_macro(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_def_process(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_def_alias(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_process_constructor(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_process_block(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_process_input(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_process_output(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_pattern_grouped(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_pattern_any(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_pattern_primitive(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_pattern_bit(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_pattern_boolean(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_pattern_byte(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_pattern_char(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_pattern_integer(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_pattern_real(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_pattern_string(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_pattern_struct(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_pattern_union(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_pattern_tuple(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_pattern_mutable(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_pattern_opt(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_pattern_array(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_pattern_function(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_named_type_pattern(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_constrained_type_pattern(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_def_type_pattern(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_def_value_pattern(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_def_generic_parameter(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_def_generic_function(const GroupNodeCPtr &node) = 0;

        virtual GroupNodeCPtr visit_def_generic_extension_function(const GroupNodeCPtr &node) = 0;
      };
    };
  }
}
#endif
