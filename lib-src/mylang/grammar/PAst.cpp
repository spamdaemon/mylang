#include <mylang/grammar/PAst.h>

namespace mylang {
  namespace grammar {
    PAst::Visitor::~Visitor()
    {
    }

    void PAst::Visitor::visitChildren(const GroupNodeCPtr &node)
    {
      auto children = node->children();
      for (auto c : children) {
        auto g = c->self<GroupNode>();
        visit(g);
      }
    }

    GroupNodeCPtr PAst::Visitor::visit(const GroupNodeCPtr &node)
    {
      auto key = node->type.value<PAst::GroupType>();
      if (!key) {
        ::std::cerr << "Missing key " << __FILE__ << ":" << __LINE__ << ::std::endl;
        throw ::std::runtime_error("Missing key");
      }
      if (key) {
        // ::std::cerr << "KEY : " << key->key << ::std::endl;
        switch (key->key) {
        case PAst::GRP_SYNTAX_ERROR:
          return visit_syntax_error(node);
        case PAst::GRP_ROOT:
          return visit_root(node);
        case PAst::GRP_IMPORT:
          return visit_import(node);
        case PAst::GRP_INTERPOLATE_TEXT_EXPRESSION:
          return visit_interpolate_text_expression(node);
        case PAst::GRP_QUOTE_TEXT_EXPRESSION:
          return visit_quote_text_expression(node);
        case PAst::GRP_STRINGIFY_EXPRESSION:
          return visit_stringify_expression(node);
        case PAst::GRP_ARITHMETIC_EXPRESSION:
          return visit_arithmetic_expression(node);
        case PAst::GRP_NEW_EXPRESSION:
          return visit_new_expression(node);
        case PAst::GRP_NEGATE_EXPRESSION:
          return visit_negate_expression(node);
        case PAst::GRP_WITH_EXPRESSION:
          return visit_with_expression(node);
        case PAst::GRP_LOOP_EXPRESSION:
          return visit_loop_expression(node);
        case PAst::GRP_CONCATENATE_EXPRESSION:
          return visit_concatenate_expression(node);
        case PAst::GRP_MERGE_EXPRESSION:
          return visit_merge_expression(node);
        case PAst::GRP_TRANSFORM_EXPRESSION:
          return visit_transform_expression(node);
        case PAst::GRP_ZIP_EXPRESSION:
          return visit_zip_expression(node);
        case PAst::GRP_CONDITIONAL_EXPRESSION:
          return visit_conditional_expression(node);
        case PAst::GRP_LOGICAL_EXPRESSION: // no difference
        case PAst::GRP_BITOP_EXPRESSION:
          return visit_bitop_expression(node);
        case PAst::GRP_COMPARISON_EXPRESSION:
          return visit_comparison_expression(node);
        case PAst::GRP_SUB_EXPRESSION:
          return visit_sub_expression(node);
        case PAst::GRP_DEF_PARAMETER:
          return visit_def_parameter(node);
        case PAst::GRP_DEF_VALUE:
          return visit_def_value(node);
        case PAst::GRP_DEF_VARIABLE:
          return visit_def_variable(node);
        case PAst::GRP_MEMBER_EXPRESSION:
          return visit_member_expression(node);
        case PAst::GRP_TYPE_MEMBER_CHAIN_EXPRESSION:
          return visit_type_member_chain_expression(node);
        case PAst::GRP_APPLY_EXPRESSION:
          return visit_apply_expression(node);
        case PAst::GRP_OPTIFY_EXPRESSION:
          return visit_optify_expression(node);
        case PAst::GRP_DEREF_EXPRESSION:
          return visit_deref_expression(node);
        case PAst::GRP_INDEX_EXPRESSION:
          return visit_index_expression(node);
        case PAst::GRP_SUBRANGE_EXPRESSION:
          return visit_subrange_expression(node);
        case PAst::GRP_INSTANTIATE_EXPRESSION:
          return visit_instantiate_expression(node);
        case PAst::GRP_CALL_EXPRESSION:
          return visit_call_expression(node);
        case PAst::GRP_LAMBDA_EXPRESSION:
          return visit_lambda_expression(node);
        case PAst::GRP_CLAMP_TYPECAST_EXPRESSION:
          return visit_clamp_cast_expression(node);
        case PAst::GRP_WRAP_TYPECAST_EXPRESSION:
          return visit_wrap_cast_expression(node);
        case PAst::GRP_SAFE_TYPECAST_EXPRESSION:
          return visit_safe_cast_expression(node);
        case PAst::GRP_UNSAFE_TYPECAST_EXPRESSION:
          return visit_unsafe_cast_expression(node);
        case PAst::GRP_TRY_EXPRESSION:
          return visit_try_expression(node);
        case PAst::GRP_ORELSE_EXPRESSION:
          return visit_orelse_expression(node);
        case PAst::GRP_LITERAL_DECIMAL_INTEGER:
          return visit_literal_decimal_integer(node);
        case PAst::GRP_LITERAL_HEX_INTEGER:
          return visit_literal_hex_integer(node);
        case PAst::GRP_LITERAL_OCTAL_INTEGER:
          return visit_literal_octal_integer(node);
        case PAst::GRP_LITERAL_REAL:
          return visit_literal_real(node);
        case PAst::GRP_LITERAL_NIL:
          return visit_literal_nil(node);
        case PAst::GRP_LITERAL_BIT_SEQUENCE:
          return visit_literal_bit_sequence(node);
        case PAst::GRP_LITERAL_BIT:
          return visit_literal_bit(node);
        case PAst::GRP_LITERAL_BOOLEAN:
          return visit_literal_boolean(node);
        case PAst::GRP_LITERAL_BYTE:
          return visit_literal_byte(node);
        case PAst::GRP_LITERAL_BYTE_SEQUENCE:
          return visit_literal_byte_sequence(node);
        case PAst::GRP_LITERAL_CHAR:
          return visit_literal_char(node);
        case PAst::GRP_LITERAL_STRING:
          return visit_literal_string(node);
        case PAst::GRP_LITERAL_ARRAY:
          return visit_literal_array(node);
        case PAst::GRP_LITERAL_TUPLE:
          return visit_literal_tuple(node);
        case PAst::GRP_LITERAL_TUPLE_MEMBER:
          return visit_literal_tuple_member(node);
        case PAst::GRP_LITERAL_STRUCT:
          return visit_literal_struct(node);
        case PAst::GRP_LITERAL_STRUCT_MEMBER:
          return visit_literal_struct_member(node);
        case PAst::GRP_VARIABLE_REFERENCE_EXPRESSION:
          return visit_variable_reference_expression(node);
        case PAst::GRP_NAMED_EXPRESSION:
          return visit_named_expression(node);
        case PAst::GRP_TYPENAME:
          return visit_typename(node);
        case PAst::GRP_DEF_TYPE:
          return visit_def_type(node);
        case PAst::GRP_DEF_TRANSFORM:
          return visit_def_transform(node);
        case PAst::GRP_DEF_PROCESS:
          return visit_def_process(node);
        case PAst::GRP_DEF_ALIAS:
          return visit_def_alias(node);
        case PAst::GRP_DEF_TYPENAME:
          return visit_def_typename(node);
        case PAst::GRP_DEF_GENERIC_FUNCTION:
          return visit_def_generic_function(node);
        case PAst::GRP_DEF_GENERIC_EXTENSION_FUNCTION:
          return visit_def_generic_extension_function(node);
        case PAst::GRP_DEF_FUNCTION:
          return visit_def_function(node);
        case PAst::GRP_DEF_EXTENSION_FUNCTION:
          return visit_def_extension_function(node);
        case PAst::GRP_DEF_MACRO:
          return visit_def_macro(node);
        case PAst::GRP_MACRO_BODY:
          return visit_macro_body(node);
        case PAst::GRP_DEF_NAMESPACE:
          return visit_def_namespace(node);
        case PAst::GRP_PROCESS_BLOCK:
          return visit_process_block(node);
        case PAst::GRP_PROCESS_INPUT:
          return visit_process_input(node);
        case PAst::GRP_PROCESS_OUTPUT:
          return visit_process_output(node);
        case PAst::GRP_PROCESS_CONSTRUCTOR:
          return visit_process_constructor(node);
        case PAst::GRP_GROUPED_TYPE:
          return visit_type_grouped(node);
        case PAst::GRP_TYPE_ARRAY:
          return visit_type_array(node);
        case PAst::GRP_TYPE_MEMBER:
          return visit_type_member(node);
        case PAst::GRP_TYPEOF:
          return visit_typeof(node);
        case PAst::GRP_TYPE_MUTABLE:
          return visit_type_mutable(node);
        case PAst::GRP_TYPE_OPT:
          return visit_type_opt(node);
        case PAst::GRP_TYPE_BIT:
          return visit_type_bit(node);
        case PAst::GRP_TYPE_BOOLEAN:
          return visit_type_boolean(node);
        case PAst::GRP_TYPE_BYTE:
          return visit_type_byte(node);
        case PAst::GRP_TYPE_CHAR:
          return visit_type_char(node);
        case PAst::GRP_TYPE_REAL:
          return visit_type_real(node);
        case PAst::GRP_TYPE_INTEGER:
          return visit_type_integer(node);
        case PAst::GRP_TYPE_STRING:
          return visit_type_string(node);
        case PAst::GRP_TYPE_VOID:
          return visit_type_void(node);
        case PAst::GRP_TYPE_GENERIC:
          return visit_type_generic(node);
        case PAst::GRP_TYPE_TUPLE:
          return visit_type_tuple(node);
        case PAst::GRP_TYPE_PROCESS:
          return visit_type_process(node);
        case PAst::GRP_TYPE_PROCESS_INPUT_MEMBER:
          return visit_type_process_input_member(node);
        case PAst::GRP_TYPE_PROCESS_OUTPUT_MEMBER:
          return visit_type_process_output_member(node);
        case PAst::GRP_TYPE_STRUCT:
          return visit_type_struct(node);
        case PAst::GRP_TYPE_UNION:
          return visit_type_union(node);
        case PAst::GRP_TYPE_FUNCTION:
          return visit_type_function(node);
        case PAst::GRP_STRUCT_MEMBER:
          return visit_struct_member(node);
        case PAst::GRP_UNION_MEMBER:
          return visit_union_member(node);
        case PAst::GRP_DISCRIMINANT:
          return visit_discriminant(node);
        case PAst::GRP_FUNCTION_BODY:
          return visit_function_body(node);
        case PAst::GRP_LAMBDA_BODY:
          return visit_lambda_body(node);
        case PAst::GRP_NAMED_CONSTRAINT:
          return visit_named_constraint(node);
        case PAst::GRP_CONSTRAINED_TYPE:
          return visit_constrained_type(node);
        case PAst::GRP_ASSERT_STMT:
          return visit_assert_stmt(node);
        case PAst::GRP_BREAK_STMT:
          return visit_break_stmt(node);
        case PAst::GRP_CONTINUE_STMT:
          return visit_continue_stmt(node);
        case PAst::GRP_CALL_CONSTRUCTOR_STMT:
          return visit_call_constructor_stmt(node);
        case PAst::GRP_EXPRESSION_STMT:
          return visit_expression_stmt(node);
        case PAst::GRP_FOREACH_STMT:
          return visit_foreach_stmt(node);
        case PAst::GRP_IF_STMT:
          return visit_if_stmt(node);
        case PAst::GRP_LOG_STMT:
          return visit_log_stmt(node);
        case PAst::GRP_RETURN_STMT:
          return visit_return_stmt(node);
        case PAst::GRP_SET_STMT:
          return visit_set_stmt(node);
        case PAst::GRP_STATEMENT_BLOCK:
          return visit_stmt_block(node);
        case PAst::GRP_TRY_STMT:
          return visit_try_stmt(node);
        case PAst::GRP_THROW_STMT:
          return visit_throw_stmt(node);
        case PAst::GRP_UPDATE_STMT:
          return visit_update_stmt(node);
        case PAst::GRP_VAL_STMT:
          return visit_val_stmt(node);
        case PAst::GRP_VAR_STMT:
          return visit_var_stmt(node);
        case PAst::GRP_WAIT_STMT:
          return visit_wait_stmt(node);
        case PAst::GRP_WHILE_STMT:
          return visit_while_stmt(node);
        case PAst::GRP_GROUPED_PATTERN:
          return visit_pattern_grouped(node);
        case PAst::GRP_PATTERN_ANY:
          return visit_pattern_any(node);
        case PAst::GRP_PATTERN_PRIMITIVE:
          return visit_pattern_primitive(node);
        case PAst::GRP_PATTERN_ARRAY:
          return visit_pattern_array(node);
        case PAst::GRP_PATTERN_BIT:
          return visit_pattern_bit(node);
        case PAst::GRP_PATTERN_BOOLEAN:
          return visit_pattern_boolean(node);
        case PAst::GRP_PATTERN_BYTE:
          return visit_pattern_byte(node);
        case PAst::GRP_PATTERN_CHAR:
          return visit_pattern_char(node);
        case PAst::GRP_PATTERN_INTEGER:
          return visit_pattern_integer(node);
        case PAst::GRP_PATTERN_REAL:
          return visit_pattern_real(node);
        case PAst::GRP_PATTERN_STRING:
          return visit_pattern_string(node);
        case PAst::GRP_PATTERN_STRUCT:
          return visit_pattern_struct(node);
        case PAst::GRP_PATTERN_TUPLE:
          return visit_pattern_tuple(node);
        case PAst::GRP_PATTERN_UNION:
          return visit_pattern_union(node);
        case PAst::GRP_PATTERN_MUTABLE:
          return visit_pattern_mutable(node);
        case PAst::GRP_PATTERN_OPT:
          return visit_pattern_opt(node);
        case PAst::GRP_PATTERN_FUNCTION:
          return visit_pattern_function(node);
        case PAst::GRP_NAMED_TYPE_PATTERN:
          return visit_named_type_pattern(node);
        case PAst::GRP_CONSTRAINED_TYPE_PATTERN:
          return visit_constrained_type_pattern(node);
        case PAst::GRP_DEF_TYPE_PATTERN:
          return visit_def_type_pattern(node);
        case PAst::GRP_DEF_VALUE_PATTERN:
          return visit_def_value_pattern(node);
        case PAst::GRP_DEF_GENERIC_PARAMETER:
          return visit_def_generic_parameter(node);
        default:
          break;
        }
      }
      throw ::std::runtime_error(
          "Unexpected group " + ::std::to_string((int) key->key) + ": " + __FILE__);
    }
  }
}
