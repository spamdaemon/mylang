#ifndef CLASS_MYLANG_PATTERN_FUNCTIONPATTERN_H
#define CLASS_MYLANG_PATTERN_FUNCTIONPATTERN_H

#ifndef CLASS_MYLANG_PATTERN_PATTERN_H
#include <mylang/patterns/Pattern.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDFUNCTIONTYPE_H
#include <mylang/constraints/ConstrainedFunctionType.h>
#endif
#include <vector>

namespace mylang {
  namespace patterns {

    /** A constraint representing an error */
    class FunctionPattern: public Pattern
    {

    private:
      FunctionPattern(const Pattern::Ptr &ret, const ::std::vector<Pattern::Ptr> &xparameters);

    public:
      ~FunctionPattern();

      /**
       * Create a function constraint.
       * @param res the return constraint
       * @param parameters the positional parameters
       */
    public:
      static ::std::shared_ptr<const FunctionPattern> create(const Pattern::Ptr &ret,
          const ::std::vector<Pattern::Ptr> &xparameters);

    public:
      MatchContext::Ptr match(const ConstrainedTypePtr &type,
          const MatchContext::Ptr &context) const override;

    protected:
      ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> createTypeAndUpdateContext(
          const MatchContext::Ptr &context) const override;

    public:
      bool isSamePattern(const Pattern &other) const override;

      EquivalencyMapPtr isEquivalentPattern(const Pattern &other, EquivalencyMapPtr eq) const
          override;

      ::std::string toString() const override;

      Specificity getSpecificity() const override;

      bool isMoreSpecificThan(const Pattern &other) const override;

      bool isMoreGeneralThan(const Pattern &other) const override;

      /** The return constraint */
    public:
      const Pattern::Ptr returnType;

      /** The return constraint */
    public:
      const ::std::vector<Pattern::Ptr> parameters;
    };
  }
}
#endif
