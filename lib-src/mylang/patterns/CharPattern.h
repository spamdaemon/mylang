#ifndef CLASS_MYLANG_PATTERNS_CHARPATTERN_H
#define CLASS_MYLANG_PATTERNS_CHARPATTERN_H

#ifndef CLASS_MYLANG_PATTERNS_PATTERN_H
#include <mylang/patterns/Pattern.h>
#endif

namespace mylang {
  namespace patterns {

    /**
     * The base class for all patterns.
     */
    class CharPattern: public Pattern
    {
      CharPattern(const CharPattern&) = delete;

      CharPattern& operator=(const CharPattern&) = delete;

      /**
       * Default constructor
       */
    private:
      CharPattern();

      /**
       * Destructor
       */
    public:
      ~CharPattern();

      /**
       * Create a new generic array
       * @param element the element type
       */
    public:
      static ::std::shared_ptr<const CharPattern> create();

    public:
      MatchContext::Ptr match(const ConstrainedTypePtr &type,
          const MatchContext::Ptr &context) const override;

    public:
      bool isSamePattern(const Pattern &other) const override;

      EquivalencyMapPtr isEquivalentPattern(const Pattern &other, EquivalencyMapPtr eq) const
          override;

      ::std::string toString() const override;

      Specificity getSpecificity() const override;

      bool isMoreSpecificThan(const Pattern &other) const override;

      bool isMoreGeneralThan(const Pattern &other) const override;

    protected:
      ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> createTypeAndUpdateContext(
          const MatchContext::Ptr &context) const override;

    };
  }
}
#endif
