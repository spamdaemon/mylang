#include <mylang/constraints/ConstrainedType.h>
#include <mylang/defs.h>
#include <mylang/patterns/ArrayPattern.h>
#include <mylang/patterns/MatchContext.h>
#include <algorithm>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <sstream>
#include <utility>

namespace mylang {
  namespace patterns {
    using namespace mylang::constraints;

    ArrayPattern::ArrayPattern(Ptr elmt, ::std::shared_ptr<const Range> r)
        : element(elmt), bounds(r)
    {

    }

    ArrayPattern::~ArrayPattern()
    {
    }

    bool ArrayPattern::isMoreGeneralThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const ArrayPattern*>(&other);
      if (!that) {
        return false;
      }
      // check by specificity of the element
      if (element->isMoreGeneralThan(*that->element)) {
        return true;
      }
      if (!bounds) {
        // we know we're not more specific
        return that->bounds != nullptr;
      }
      if (!that->bounds) {
        return false;
      }
      if (bounds->max().isInfinite() && that->bounds->max().isFinite()) {
        return true;
      }

      return false;
    }

    bool ArrayPattern::isMoreSpecificThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const ArrayPattern*>(&other);
      if (!that) {
        return other.getSpecificity() == 0;
      }

      // check by specificity of the element
      if (element->isMoreSpecificThan(*that->element)) {
        return true;
      }
      if (that->bounds == nullptr) {
        // if the other pattern doesn't have bounds, but we this one does,
        // then it's more specific
        return bounds != nullptr;
      }
      if (!bounds) {
        // we know we're not more specific
        return false;
      }
      // both have bounds
      // if one has an upper bound and the other one doesn't then it's more specific
      if (bounds->max() < that->bounds->max()) {
        return true;
      }

      if (bounds->min() > that->bounds->min()) {
        return true;
      }

      return false;
    }

    Pattern::Specificity ArrayPattern::getSpecificity() const
    {
      Specificity s = 1;
      if (bounds) {
        s = 1 + s;
        if (bounds->max().isFinite()) {
          s = 1 + s;
        }
      }
      s = s + element->getSpecificity();
      return s;
    }

    bool ArrayPattern::isSamePattern(const Pattern &other) const
    {
      if (id == other.id) {
        return true;
      }
      auto that = dynamic_cast<const ArrayPattern*>(&other);
      if (!that) {
        return false;
      }
      if (!that->element->isSamePattern(*element)) {
        return false;
      }
      if ((bounds == nullptr || that->bounds == nullptr) && that->bounds != bounds) {
        return false;
      }
      if (bounds && *bounds != *that->bounds) {
        return false;
      }
      return true;
    }

    Pattern::EquivalencyMapPtr ArrayPattern::isEquivalentPattern(const Pattern &other,
        EquivalencyMapPtr eq) const
    {
      if (id == other.id) {
        return eq;
      }
      auto that = dynamic_cast<const ArrayPattern*>(&other);
      if (!that) {
        return nullptr;
      }
      if ((bounds == nullptr || that->bounds == nullptr) && that->bounds != bounds) {
        return nullptr;
      }
      if (bounds && *bounds != *that->bounds) {
        return nullptr;
      }
      return element->isEquivalentPattern(*that->element, ::std::move(eq));
    }

    ::std::string ArrayPattern::toString() const
    {
      ::std::ostringstream out;
      out << element->toString();
      if (bounds) {
        out << '[';
        if (bounds->min() != 0) {
          out << bounds->min();
        }
        if (!bounds->isFixed()) {
          out << ':';
          if (bounds->max().isFinite()) {
            out << bounds->max();
          }
        }

        out << ']';
      } else {
        out << "[]*";
      }
      return out.str();
    }

    ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> ArrayPattern::createTypeAndUpdateContext(
        const MatchContext::Ptr &context) const
    {
      auto res = element->createType(context);
      if (res.second) {
        if (bounds) {
          res.second = ConstrainedArrayType::getBoundedArray(res.second, *bounds);
        } else {
          res.second = ConstrainedArrayType::getUnboundedArray(res.second);
        }
        auto ctx = res.first->update(self(), res.second);
        if (ctx) {
          res.first = ctx;
        } else {
          res.second = nullptr;
        }
      }
      return res;
    }

    MatchContext::Ptr ArrayPattern::match(const ConstrainedTypePtr &type,
        const MatchContext::Ptr &context) const
    {
      MatchContext::Ptr ctx;
      auto ty = type->self<ConstrainedArrayType>();
      if (ty) {
        if (!bounds) {
          ctx = context->update(self(), ty);
        } else if (bounds->contains(ty->bounds)) {
          ty = ConstrainedArrayType::getBoundedArray(ty->element, *bounds);
          ctx = context->update(self(), ty);
        }
        if (ctx) {
          ctx = element->match(ty->element, ctx);
        }
      }

      return ctx;
    }

    ::std::shared_ptr<const ArrayPattern> ArrayPattern::create(Ptr element,
        ::std::shared_ptr<const Range> bounds)
    {
      struct Impl: public ArrayPattern
      {
        Impl(Ptr e, ::std::shared_ptr<const Range> xbounds)
            : ArrayPattern(e, xbounds)
        {
        }

        ~Impl()
        {
        }

      };
      return ::std::make_shared<Impl>(element, bounds);
    }

    ::std::shared_ptr<const ArrayPattern> ArrayPattern::create(Ptr element)
    {
      return create(element, nullptr);
    }

    ::std::shared_ptr<const ArrayPattern> ArrayPattern::createUnbounded(Ptr element, BigUInt min)
    {
      return create(element, ::std::make_shared<const Range>(Range::EndPoint(min), ::std::nullopt));
    }

    ::std::shared_ptr<const ArrayPattern> ArrayPattern::createBounded(Ptr element, BigUInt min,
        BigUInt max)
    {
      return create(element,
          ::std::make_shared<const Range>(Range::EndPoint(min), Range::EndPoint(max)));
    }
  }
}
