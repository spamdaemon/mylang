#include <mylang/patterns/BitPattern.h>
#include <mylang/constraints/ConstrainedBitType.h>

namespace mylang {
  namespace patterns {
    using namespace mylang::constraints;

    BitPattern::BitPattern()
    {

    }

    BitPattern::~BitPattern()
    {
    }

    bool BitPattern::isMoreGeneralThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const BitPattern*>(&other);
      if (!that) {
        return false;
      }
      return false;
    }

    bool BitPattern::isMoreSpecificThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const BitPattern*>(&other);
      if (!that) {
        return other.getSpecificity() == 0;
      }
      return false;
    }

    Pattern::Specificity BitPattern::getSpecificity() const
    {
      return 1;
    }

    bool BitPattern::isSamePattern(const Pattern &other) const
    {
      if (id == other.id) {
        return true;
      }
      auto that = dynamic_cast<const BitPattern*>(&other);
      if (that) {
        return true;
      }
      return false;
    }

    Pattern::EquivalencyMapPtr BitPattern::isEquivalentPattern(const Pattern &other,
        EquivalencyMapPtr eq) const
    {
      if (id == other.id) {
        return eq;
      }
      auto that = dynamic_cast<const BitPattern*>(&other);
      if (that) {
        return eq;
      }
      return nullptr;
    }

    ::std::string BitPattern::toString() const
    {
      return "bit*";
    }

    ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> BitPattern::createTypeAndUpdateContext(
        const MatchContext::Ptr &context) const
    {
      ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> res(context, nullptr);
      return res;
    }

    MatchContext::Ptr BitPattern::match(const ConstrainedTypePtr &type,
        const MatchContext::Ptr &context) const
    {
      auto ty = type->self<ConstrainedBitType>();
      if (ty) {
        return context->update(self(), type);
      }
      return nullptr;
    }

    ::std::shared_ptr<const BitPattern> BitPattern::create()
    {
      struct Impl: public BitPattern
      {
        Impl()
        {
        }

        ~Impl()
        {
        }

      };
      return ::std::make_shared<Impl>();
    }
  }
}
