#include <mylang/constraints/ConstrainedType.h>
#include <mylang/defs.h>
#include <mylang/patterns/ConstrainedTypePattern.h>
#include <mylang/patterns/MatchContext.h>
#include <memory>

namespace mylang {
  namespace patterns {

    ConstrainedTypePattern::ConstrainedTypePattern(ConstrainedTypePtr ty)
        : type(ty)
    {
    }

    ConstrainedTypePattern::~ConstrainedTypePattern()
    {
    }

    bool ConstrainedTypePattern::isMoreGeneralThan(const Pattern&) const
    {
      return false;
    }

    bool ConstrainedTypePattern::isMoreSpecificThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const ConstrainedTypePattern*>(&other);
      if (that) {
        return false;
      }

      if (other.match(type, MatchContext::create()) != nullptr) {
        return true;
      }
      return false;
    }

    Pattern::Specificity ConstrainedTypePattern::getSpecificity() const
    {
      return 1;
    }

    bool ConstrainedTypePattern::isSamePattern(const Pattern &other) const
    {
      if (id == other.id) {
        return true;
      }
      auto that = dynamic_cast<const ConstrainedTypePattern*>(&other);
      if (that && that->type->isSameConstraint(*type)) {
        return true;
      }
      return false;
    }

    Pattern::EquivalencyMapPtr ConstrainedTypePattern::isEquivalentPattern(const Pattern &other,
        EquivalencyMapPtr eq) const
    {
      if (id == other.id) {
        return eq;
      }
      auto that = dynamic_cast<const ConstrainedTypePattern*>(&other);
      if (that && that->type->isSameConstraint(*type)) {
        return eq;
      }
      return nullptr;
    }

    ::std::string ConstrainedTypePattern::toString() const
    {
      return type->toString();
    }

    ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> ConstrainedTypePattern::createTypeAndUpdateContext(
        const MatchContext::Ptr &context) const
    {
      auto ctx = context->update(self(), type);
      if (ctx) {
        return ::std::make_pair(ctx, type);
      } else {
        return ::std::pair<MatchContext::Ptr, ConstrainedTypePtr>(context, nullptr);
      }
    }

    MatchContext::Ptr ConstrainedTypePattern::match(const ConstrainedTypePtr &ty,
        const MatchContext::Ptr &context) const
    {
      MatchContext::Ptr ctx;
      if (type->canImplicitlyCastFrom(*ty)) {
        return context->update(self(), ty);
      }
      return nullptr;
    }

    ::std::shared_ptr<const ConstrainedTypePattern> ConstrainedTypePattern::create(
        ConstrainedTypePtr t)
    {
      struct Impl: public ConstrainedTypePattern
      {
        Impl(ConstrainedTypePtr e)
            : ConstrainedTypePattern(e)
        {
        }

        ~Impl()
        {
        }

      };
      return ::std::make_shared<Impl>(t);
    }
  }
}
