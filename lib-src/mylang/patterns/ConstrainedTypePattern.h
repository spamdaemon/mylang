#ifndef CLASS_MYLANG_PATTERNS_CONSTRAINEDTYPEPATTERN_H
#define CLASS_MYLANG_PATTERNS_CONSTRAINEDTYPEPATTERN_H

#ifndef CLASS_MYLANG_PATTERNS_PATTERN_H
#include <mylang/patterns/Pattern.h>
#endif

namespace mylang {
  namespace patterns {

    /**
     * The base class for all patterns.
     */
    class ConstrainedTypePattern: public Pattern
    {
      ConstrainedTypePattern(const ConstrainedTypePattern&) = delete;

      ConstrainedTypePattern& operator=(const ConstrainedTypePattern&) = delete;

      /**
       * Default constructor
       */
    private:
      ConstrainedTypePattern(ConstrainedTypePtr ty);

      /**
       * Destructor
       */
    public:
      ~ConstrainedTypePattern();

      /**
       * Create a new generic array
       * @param t the element type
       */
    public:
      static ::std::shared_ptr<const ConstrainedTypePattern> create(ConstrainedTypePtr t);

    public:
      MatchContext::Ptr match(const ConstrainedTypePtr &type,
          const MatchContext::Ptr &context) const override;

    protected:
      ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> createTypeAndUpdateContext(
          const MatchContext::Ptr &context) const override;

    public:
      bool isSamePattern(const Pattern &other) const override;

      EquivalencyMapPtr isEquivalentPattern(const Pattern &other, EquivalencyMapPtr eq) const
          override;

      ::std::string toString() const override;

      Specificity getSpecificity() const override;

      bool isMoreSpecificThan(const Pattern &other) const override;

      bool isMoreGeneralThan(const Pattern &other) const override;

      /**
       * The element type
       */
    public:
      const ConstrainedTypePtr type;
    };
  }
}
#endif
