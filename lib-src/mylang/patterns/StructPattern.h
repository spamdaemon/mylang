#ifndef CLASS_MYLANG_PATTERNS_STRUCTPATTERN_H
#define CLASS_MYLANG_PATTERNS_STRUCTPATTERN_H

#ifndef CLASS_MYLANG_PATTERNS_PATTERN_H
#include <mylang/patterns/Pattern.h>
#endif

namespace mylang {
  namespace patterns {

    /**
     * The base class for all patterns.
     */
    class StructPattern: public Pattern
    {
      StructPattern(const StructPattern&) = delete;

      StructPattern& operator=(const StructPattern&) = delete;

      /**
       * Default constructor
       */
    private:
      StructPattern();

      /**
       * Destructor
       */
    public:
      ~StructPattern();

      /**
       * Create a new generic array
       * @param element the element type
       */
    public:
      static ::std::shared_ptr<const StructPattern> create();

    public:
      MatchContext::Ptr match(const ConstrainedTypePtr &type,
          const MatchContext::Ptr &context) const override;

    public:
      bool isSamePattern(const Pattern &other) const override;

      EquivalencyMapPtr isEquivalentPattern(const Pattern &other, EquivalencyMapPtr eq) const
          override;

      ::std::string toString() const override;

      Specificity getSpecificity() const override;

      bool isMoreSpecificThan(const Pattern &other) const override;

      bool isMoreGeneralThan(const Pattern &other) const override;

    protected:
      ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> createTypeAndUpdateContext(
          const MatchContext::Ptr &context) const override;

    };
  }
}
#endif
