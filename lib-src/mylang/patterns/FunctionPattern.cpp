#include <mylang/patterns/FunctionPattern.h>
#include <sstream>

namespace mylang {
  namespace patterns {
    using namespace mylang::constraints;

    FunctionPattern::FunctionPattern(const Pattern::Ptr &ret,
        const ::std::vector<Pattern::Ptr> &xparameters)
        : returnType(ret), parameters(xparameters)
    {
    }

    FunctionPattern::~FunctionPattern()
    {
    }

    bool FunctionPattern::isMoreGeneralThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const FunctionPattern*>(&other);
      if (!that) {
        return false;
      }
      if (parameters.size() != that->parameters.size()) {
        // if we have more parameters, then we're more general (and more specific)
        return false;
      }
      for (size_t i = 0; i < parameters.size(); ++i) {
        if (parameters.at(i)->isMoreGeneralThan(*that->parameters.at(i))) {
          return true;
        }
      }

      return false;
    }

    bool FunctionPattern::isMoreSpecificThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const FunctionPattern*>(&other);
      if (!that) {
        return other.getSpecificity() == 0;
      }
      if (parameters.size() != that->parameters.size()) {
        // if we have more parameters, then we're more specific
        return false;
      }
      for (size_t i = 0; i < parameters.size(); ++i) {
        if (parameters.at(i)->isMoreSpecificThan(*that->parameters.at(i))) {
          return true;
        }
      }
      // lastly, consider the return type
      // TODO: should we really consider the return type here?
      if (returnType->isMoreSpecificThan(*that->returnType)) {
        return true;
      }
      return false;
    }

    Pattern::Specificity FunctionPattern::getSpecificity() const
    {
      Pattern::Specificity s = 1 + returnType->getSpecificity();
      for (auto p : parameters) {
        s = s + p->getSpecificity();
      }
      // the number of parameters increases the specificity as well
      s = parameters.size() + s;
      return s;
    }

    bool FunctionPattern::isSamePattern(const Pattern &other) const
    {
      if (id == other.id) {
        return true;
      }
      auto that = dynamic_cast<const FunctionPattern*>(&other);
      if (that && parameters.size() == that->parameters.size()) {
        if (!returnType->isSamePattern(*that->returnType)) {
          return false;
        }
        for (size_t i = 0; i < parameters.size(); ++i) {
          if (!parameters.at(i)->isSamePattern(*that->parameters.at(i))) {
            return false;
          }
        }
        return true;
      }
      return false;
    }

    Pattern::EquivalencyMapPtr FunctionPattern::isEquivalentPattern(const Pattern &other,
        EquivalencyMapPtr eq) const
    {
      if (id == other.id) {
        return eq;
      }
      auto that = dynamic_cast<const FunctionPattern*>(&other);
      if (that && parameters.size() == that->parameters.size()) {
        eq = returnType->isEquivalentPattern(*that->returnType, ::std::move(eq));
        if (!eq) {
          return nullptr;
        }

        for (size_t i = 0; i < parameters.size(); ++i) {
          eq = parameters.at(i)->isEquivalentPattern(*that->parameters.at(i), ::std::move(eq));
          if (!eq) {
            return nullptr;
          }
        }
        return eq;
      }
      return nullptr;
    }

    ::std::string FunctionPattern::toString() const
    {
      ::std::ostringstream out;
      const char *sep = "";
      out << '(';
      for (auto p : parameters) {
        out << sep << p->toString();
        sep = ",";
      }
      out << ')' << ':' << returnType->toString();
      return out.str();
    }

    ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> FunctionPattern::createTypeAndUpdateContext(
        const MatchContext::Ptr &context) const
    {
      ::std::vector<ConstrainedTypePtr> params;
      ConstrainedTypePtr retTy;

      auto res = returnType->createType(context);
      if (res.second) {
        retTy = res.second;
      }
      for (auto p : parameters) {
        res = p->createType(res.first);
        if (res.second) {
          params.push_back(res.second);
        }
      }

      res.second = nullptr;
      if (retTy && params.size() == parameters.size()) {
        res.second = ConstrainedFunctionType::get(retTy, params);
        auto ctx = res.first->update(self(), res.second);
        if (ctx) {
          res.first = ctx;
        } else {
          res.second = nullptr;
        }
      }
      return res;
    }

    MatchContext::Ptr FunctionPattern::match(const ConstrainedTypePtr &type,
        const MatchContext::Ptr &context) const
    {
      auto fnTy = type->self<ConstrainedFunctionType>();
      if (!fnTy) {
        return nullptr;
      }
      if (fnTy->parameters.size() != parameters.size()) {
        return nullptr;
      }

      auto ctx = context->update(self(), type);
      if (ctx) {
        ctx = returnType->match(fnTy->returnType, ctx);
        for (size_t i = 0; ctx && i < parameters.size(); ++i) {
          ctx = parameters.at(i)->match(fnTy->parameters.at(i), ctx);
        }
      }
      return ctx;
    }

    ::std::shared_ptr<const FunctionPattern> FunctionPattern::create(const Pattern::Ptr &ret,
        const ::std::vector<Pattern::Ptr> &xparameters)
    {
      struct Impl: public FunctionPattern
      {
        Impl(const Pattern::Ptr &ret, const ::std::vector<Pattern::Ptr> &args)
            : FunctionPattern(ret, args)
        {
        }

        ~Impl()
        {
        }
      };
      return ::std::make_shared<Impl>(ret, xparameters);
    }

  }
}
