#ifndef CLASS_MYLANG_PATTERNS_UNIONPATTERN_H
#define CLASS_MYLANG_PATTERNS_UNIONPATTERN_H

#ifndef CLASS_MYLANG_PATTERNS_PATTERN_H
#include <mylang/patterns/Pattern.h>
#endif

namespace mylang {
  namespace patterns {

    /**
     * The base class for all patterns.
     */
    class UnionPattern: public Pattern
    {
      UnionPattern(const UnionPattern&) = delete;

      UnionPattern& operator=(const UnionPattern&) = delete;

      /**
       * Default constructor
       */
    private:
      UnionPattern();

      /**
       * Destructor
       */
    public:
      ~UnionPattern();

      /**
       * Create a new generic array
       * @param element the element type
       */
    public:
      static ::std::shared_ptr<const UnionPattern> create();

    public:
      MatchContext::Ptr match(const ConstrainedTypePtr &type,
          const MatchContext::Ptr &context) const override;

    public:
      bool isSamePattern(const Pattern &other) const override;

      EquivalencyMapPtr isEquivalentPattern(const Pattern &other, EquivalencyMapPtr eq) const
          override;

      ::std::string toString() const override;

      Specificity getSpecificity() const override;

      bool isMoreSpecificThan(const Pattern &other) const override;

      bool isMoreGeneralThan(const Pattern &other) const override;

    protected:
      ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> createTypeAndUpdateContext(
          const MatchContext::Ptr &context) const override;

    };
  }
}
#endif
