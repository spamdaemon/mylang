#include <mylang/patterns/StringPattern.h>
#include <mylang/constraints/ConstrainedStringType.h>

namespace mylang {
  namespace patterns {
    using namespace mylang::constraints;

    StringPattern::StringPattern()
    {

    }

    StringPattern::~StringPattern()
    {
    }

    bool StringPattern::isMoreGeneralThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const StringPattern*>(&other);
      if (!that) {
        return false;
      }
      return false;
    }

    bool StringPattern::isMoreSpecificThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const StringPattern*>(&other);
      if (!that) {
        return other.getSpecificity() == 0;
      }
      return false;
    }

    Pattern::Specificity StringPattern::getSpecificity() const
    {
      return 1;
    }

    bool StringPattern::isSamePattern(const Pattern &other) const
    {
      if (id == other.id) {
        return true;
      }
      auto that = dynamic_cast<const StringPattern*>(&other);
      if (that) {
        return true;
      }
      return false;
    }

    Pattern::EquivalencyMapPtr StringPattern::isEquivalentPattern(const Pattern &other,
        EquivalencyMapPtr eq) const
    {
      if (id == other.id) {
        return eq;
      }
      auto that = dynamic_cast<const StringPattern*>(&other);
      if (that) {
        return eq;
      }
      return nullptr;
    }

    ::std::string StringPattern::toString() const
    {
      return "string*";
    }

    ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> StringPattern::createTypeAndUpdateContext(
        const MatchContext::Ptr &context) const
    {
      ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> res(context, nullptr);
      return res;
    }

    MatchContext::Ptr StringPattern::match(const ConstrainedTypePtr &type,
        const MatchContext::Ptr &context) const
    {
      auto ty = type->self<ConstrainedStringType>();
      if (ty) {
        return context->update(self(), type);
      }
      return nullptr;
    }

    ::std::shared_ptr<const StringPattern> StringPattern::create()
    {
      struct Impl: public StringPattern
      {
        Impl()
        {
        }

        ~Impl()
        {
        }

      };
      return ::std::make_shared<Impl>();
    }
  }
}
