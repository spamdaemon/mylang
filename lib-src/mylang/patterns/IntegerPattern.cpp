#include <mylang/patterns/IntegerPattern.h>
#include <mylang/constraints/ConstrainedIntegerType.h>

namespace mylang {
  namespace patterns {
    using namespace mylang::constraints;

    IntegerPattern::IntegerPattern()
    {

    }

    IntegerPattern::~IntegerPattern()
    {
    }

    bool IntegerPattern::isMoreGeneralThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const IntegerPattern*>(&other);
      if (!that) {
        return false;
      }
      return false;
    }

    bool IntegerPattern::isMoreSpecificThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const IntegerPattern*>(&other);
      if (!that) {
        return other.getSpecificity() == 0;
      }
      return false;
    }

    Pattern::Specificity IntegerPattern::getSpecificity() const
    {
      return 1;
    }

    bool IntegerPattern::isSamePattern(const Pattern &other) const
    {
      if (id == other.id) {
        return true;
      }
      auto that = dynamic_cast<const IntegerPattern*>(&other);
      if (that) {
        return true;
      }
      return false;
    }

    Pattern::EquivalencyMapPtr IntegerPattern::isEquivalentPattern(const Pattern &other,
        EquivalencyMapPtr eq) const
    {
      if (id == other.id) {
        return eq;
      }
      auto that = dynamic_cast<const IntegerPattern*>(&other);
      if (that) {
        return eq;
      }
      return nullptr;
    }

    ::std::string IntegerPattern::toString() const
    {
      return "integer*";
    }

    ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> IntegerPattern::createTypeAndUpdateContext(
        const MatchContext::Ptr &context) const
    {
      ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> res(context, nullptr);
      return res;
    }

    MatchContext::Ptr IntegerPattern::match(const ConstrainedTypePtr &type,
        const MatchContext::Ptr &context) const
    {
      auto ty = type->self<ConstrainedIntegerType>();
      if (ty) {
        return context->update(self(), type);
      }
      return nullptr;
    }

    ::std::shared_ptr<const IntegerPattern> IntegerPattern::create()
    {
      struct Impl: public IntegerPattern
      {
        Impl()
        {
        }

        ~Impl()
        {
        }

      };
      return ::std::make_shared<Impl>();
    }
  }
}
