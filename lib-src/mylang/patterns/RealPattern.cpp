#include <mylang/patterns/RealPattern.h>
#include <mylang/constraints/ConstrainedRealType.h>

namespace mylang {
  namespace patterns {
    using namespace mylang::constraints;

    RealPattern::RealPattern()
    {

    }

    RealPattern::~RealPattern()
    {
    }

    bool RealPattern::isMoreGeneralThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const RealPattern*>(&other);
      if (!that) {
        return false;
      }
      return false;
    }

    bool RealPattern::isMoreSpecificThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const RealPattern*>(&other);
      if (!that) {
        return other.getSpecificity() == 0;
      }
      return false;
    }

    Pattern::Specificity RealPattern::getSpecificity() const
    {
      return 1;
    }

    bool RealPattern::isSamePattern(const Pattern &other) const
    {
      if (id == other.id) {
        return true;
      }
      auto that = dynamic_cast<const RealPattern*>(&other);
      if (that) {
        return true;
      }
      return false;
    }

    Pattern::EquivalencyMapPtr RealPattern::isEquivalentPattern(const Pattern &other,
        EquivalencyMapPtr eq) const
    {
      if (id == other.id) {
        return eq;
      }
      auto that = dynamic_cast<const RealPattern*>(&other);
      if (that) {
        return eq;
      }
      return nullptr;
    }

    ::std::string RealPattern::toString() const
    {
      return "real*";
    }

    ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> RealPattern::createTypeAndUpdateContext(
        const MatchContext::Ptr &context) const
    {
      ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> res(context, nullptr);
      return res;
    }

    MatchContext::Ptr RealPattern::match(const ConstrainedTypePtr &type,
        const MatchContext::Ptr &context) const
    {
      auto ty = type->self<ConstrainedRealType>();
      if (ty) {
        return context->update(self(), type);
      }
      return nullptr;
    }

    ::std::shared_ptr<const RealPattern> RealPattern::create()
    {
      struct Impl: public RealPattern
      {
        Impl()
        {
        }

        ~Impl()
        {
        }

      };
      return ::std::make_shared<Impl>();
    }
  }
}
