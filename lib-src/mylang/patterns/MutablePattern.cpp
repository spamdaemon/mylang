#include <mylang/patterns/MutablePattern.h>

namespace mylang {
  namespace patterns {
    using namespace mylang::constraints;

    MutablePattern::MutablePattern(Ptr elmt)
        : element(elmt)
    {

    }

    MutablePattern::~MutablePattern()
    {
    }

    bool MutablePattern::isMoreGeneralThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const MutablePattern*>(&other);
      if (!that) {
        return false;
      }
      return element->isMoreGeneralThan(*that->element);
    }

    bool MutablePattern::isMoreSpecificThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const MutablePattern*>(&other);
      if (!that) {
        return other.getSpecificity() == 0;
      }
      return element->isMoreSpecificThan(*that->element);
    }

    Pattern::Specificity MutablePattern::getSpecificity() const
    {
      return 1 + element->getSpecificity();
    }

    bool MutablePattern::isSamePattern(const Pattern &other) const
    {
      if (id == other.id) {
        return true;
      }
      auto that = dynamic_cast<const MutablePattern*>(&other);
      if (that && that->element->isSamePattern(*that->element)) {
        return true;
      }
      return false;
    }

    Pattern::EquivalencyMapPtr MutablePattern::isEquivalentPattern(const Pattern &other,
        EquivalencyMapPtr eq) const
    {
      if (id == other.id) {
        return eq;
      }
      auto that = dynamic_cast<const MutablePattern*>(&other);
      if (that) {
        return element->isEquivalentPattern(*that->element, ::std::move(eq));
      }
      return nullptr;
    }

    ::std::string MutablePattern::toString() const
    {
      auto res = element->toString();
      return res + "!";
    }

    ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> MutablePattern::createTypeAndUpdateContext(
        const MatchContext::Ptr &context) const
    {
      auto res = element->createType(context);
      if (res.second) {
        res.second = ConstrainedMutableType::get(res.second);
        auto ctx = res.first->update(self(), res.second);
        if (ctx) {
          res.first = ctx;
        } else {
          res.second = nullptr;
        }
      }
      return res;
    }

    MatchContext::Ptr MutablePattern::match(const ConstrainedTypePtr &type,
        const MatchContext::Ptr &context) const
    {
      MatchContext::Ptr ctx;
      auto ty = type->self<ConstrainedMutableType>();
      if (ty) {
        ctx = context->update(self(), type);
        if (ctx) {
          ctx = element->match(ty->element, ctx);
        }
      }

      return ctx;
    }

    ::std::shared_ptr<const MutablePattern> MutablePattern::create(Ptr element)
    {
      struct Impl: public MutablePattern
      {
        Impl(Ptr e)
            : MutablePattern(e)
        {
        }

        ~Impl()
        {
        }

      };
      return ::std::make_shared<Impl>(element);
    }
  }
}
