#ifndef CLASS_MYLANG_PATTERNS_OPTPATTERN_H
#define CLASS_MYLANG_PATTERNS_OPTPATTERN_H

#ifndef CLASS_MYLANG_PATTERNS_PATTERN_H
#include <mylang/patterns/Pattern.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDOPTTYPE_H
#include <mylang/constraints/ConstrainedOptType.h>
#endif
#include <functional>

namespace mylang {
  namespace patterns {

    /**
     * The base class for all patterns.
     */
    class OptPattern: public Pattern
    {
      OptPattern(const OptPattern&) = delete;

      OptPattern& operator=(const OptPattern&) = delete;

      /**
       * Default constructor
       */
    private:
      OptPattern(Ptr element);

      /**
       * Destructor
       */
    public:
      ~OptPattern();

      /**
       * Create a new generic array
       * @param element the element type
       */
    public:
      static ::std::shared_ptr<const OptPattern> create(Ptr element);

    public:
      MatchContext::Ptr match(const ConstrainedTypePtr &type,
          const MatchContext::Ptr &context) const override;

    public:
      bool isSamePattern(const Pattern &other) const override;

      EquivalencyMapPtr isEquivalentPattern(const Pattern &other, EquivalencyMapPtr eq) const
          override;

      ::std::string toString() const override;

      Specificity getSpecificity() const override;

      bool isMoreSpecificThan(const Pattern &other) const override;

      bool isMoreGeneralThan(const Pattern &other) const override;

    protected:
      ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> createTypeAndUpdateContext(
          const MatchContext::Ptr &context) const override;

      /**
       * The element type
       */
    public:
      const Ptr element;
    };
  }
}
#endif
