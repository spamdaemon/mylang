#ifndef CLASS_MYLANG_PATTERNS_GENERICARRAY_H
#define CLASS_MYLANG_PATTERNS_GENERICARRAY_H

#ifndef CLASS_MYLANG_PATTERNS_PATTERN_H
#include <mylang/patterns/Pattern.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDARRAYTYPE_H
#include <mylang/constraints/ConstrainedArrayType.h>
#endif

namespace mylang {
  namespace patterns {

    /**
     * The base class for all patterns.
     */
    class ArrayPattern: public Pattern
    {
      ArrayPattern(const ArrayPattern&) = delete;

      ArrayPattern& operator=(const ArrayPattern&) = delete;

      /** The range */
    public:
      typedef mylang::constraints::ConstrainedArrayType::Range Range;

      /**
       * Default constructor
       */
    private:
      ArrayPattern(Ptr element, ::std::shared_ptr<const Range> bounds);

      /**
       * Destructor
       */
    public:
      ~ArrayPattern();

      /**
       * Create a new generic array
       * @param element the element type
       */
    public:
      static ::std::shared_ptr<const ArrayPattern> create(Ptr element,
          ::std::shared_ptr<const Range> bounds);

      /**
       * Create a new generic array
       * @param element the element type
       */
    public:
      static ::std::shared_ptr<const ArrayPattern> create(Ptr element);

      /**
       * Create a new generic unbounded array
       * @param element the element type
       */
    public:
      static ::std::shared_ptr<const ArrayPattern> createUnbounded(Ptr element, BigUInt min = 0);

      /**
       * Create a new generic unbounded array
       * @param element the element type
       */
    public:
      static ::std::shared_ptr<const ArrayPattern> createBounded(Ptr element, BigUInt min,
          BigUInt max);

    public:
      MatchContext::Ptr match(const ConstrainedTypePtr &type,
          const MatchContext::Ptr &context) const override;

    public:
      bool isSamePattern(const Pattern &other) const override;

      EquivalencyMapPtr isEquivalentPattern(const Pattern &other, EquivalencyMapPtr eq) const
          override;

      ::std::string toString() const override;

      Specificity getSpecificity() const override;

      bool isMoreSpecificThan(const Pattern &other) const override;

      bool isMoreGeneralThan(const Pattern &other) const override;

    protected:
      ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> createTypeAndUpdateContext(
          const MatchContext::Ptr &context) const override;

      /**
       * The element type
       */
    public:
      const Ptr element;

      /** The bounds to match (null to match ANY bounds) */
    public:
      const ::std::shared_ptr<const Range> bounds;
    };
  }
}
#endif
