#include <mylang/patterns/MatchContext.h>
#include <mylang/patterns/Pattern.h>
#include <mylang/constraints/ConstrainedType.h>
#include <mylang/expressions/Constant.h>
#include <cassert>

namespace mylang {
  namespace patterns {

    MatchContext::MatchContext()
    {
    }

    MatchContext::~MatchContext()
    {
    }

    ConstrainedTypePtr MatchContext::findType(const PatternPtr&) const
    {
      return nullptr;
    }

    ConstantPtr MatchContext::findValue(const PatternPtr&) const
    {
      return nullptr;
    }

    MatchContext::Ptr MatchContext::create()
    {
      struct Impl: public MatchContext
      {
        ~Impl()
        {
        }

        void debug(::std::ostream &out) const
        {
          out << "ROOT Match Context" << ::std::endl;
        }
      };
      return ::std::make_shared<Impl>();
    }

    MatchContext::Ptr MatchContext::create(Ptr parent, const PatternPtr &pattern,
        const ConstantPtr &value, const ConstrainedTypePtr &type)
    {
      struct Impl: public MatchContext
      {
        Impl(const Ptr &parent, const PatternPtr &pattern, const ConstantPtr &value,
            const ConstrainedTypePtr &type)
            : _parent(parent), _pattern(pattern), _value(value), _type(type)
        {
        }

        ~Impl()
        {
        }

        ConstantPtr findValue(const PatternPtr &pattern) const
        {
          if (_pattern == pattern) {
            return _value; // will be null if associated with a type
          } else {
            return _parent->findValue(pattern);
          }
        }

        ConstrainedTypePtr findType(const PatternPtr &pattern) const
        {
          if (pattern == _pattern) {
            return _type;  // will be null if associated with a value
          } else {
            return _parent->findType(pattern);
          }
        }

        void debug(::std::ostream &out) const
        {
          if (_parent) {
            _parent->debug(out);
          }
          if (_value) {
            out << "Pattern-Value : " << _pattern->toString() << " = " << _value->value()
                << ::std::endl;
          } else {
            out << "Pattern-Type : " << _pattern->toString() << " = " << _type->toString()
                << ::std::endl;
          }
        }

        const Ptr _parent;
        const PatternPtr _pattern;
        const ConstantPtr _value;
        const ConstrainedTypePtr _type;
      };

      if (type) {
        assert(nullptr == value);
        auto curTy = parent->findType(pattern);
        if (curTy) {
          if (curTy->canImplicitlyCastFrom(*type)) {
            // if the current type is more generic than what we want to match
            // we use the current type
            return parent;
          } else if (type->canImplicitlyCastFrom(*curTy)) {
            // if the current is narrower than what we need, we just override it
            // with the more general type
            // TODO: not 100% sure this is really the right thing to do
            return nullptr;
          } else {
            return nullptr;
          }
        }
      } else {
        assert(value != nullptr);
        auto curVal = parent->findValue(pattern);
        if (curVal) {
          return curVal->isSameConstant(*value) ? parent : nullptr;
        }
      }

      return ::std::make_shared<Impl>(parent, pattern, value, type);
    }

    MatchContext::Ptr MatchContext::update(const PatternPtr &pattern,
        const ConstrainedTypePtr &type) const
    {
      return create(shared_from_this(), pattern, nullptr, type);
    }

    MatchContext::Ptr MatchContext::update(const PatternPtr &pattern,
        const ConstantPtr &value) const
    {
      return create(shared_from_this(), pattern, value, nullptr);
    }

  }
}
