#include <mylang/patterns/OptPattern.h>

namespace mylang {
  namespace patterns {
    using namespace mylang::constraints;

    OptPattern::OptPattern(Ptr elmt)
        : element(elmt)
    {

    }

    OptPattern::~OptPattern()
    {
    }

    bool OptPattern::isMoreGeneralThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const OptPattern*>(&other);
      if (!that) {
        return false;
      }
      return element->isMoreGeneralThan(*that->element);
    }

    bool OptPattern::isMoreSpecificThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const OptPattern*>(&other);
      if (!that) {
        return other.getSpecificity() == 0;
      }
      return element->isMoreSpecificThan(*that->element);
    }

    Pattern::Specificity OptPattern::getSpecificity() const
    {
      return 1 + element->getSpecificity();
    }

    bool OptPattern::isSamePattern(const Pattern &other) const
    {
      if (id == other.id) {
        return true;
      }
      auto that = dynamic_cast<const OptPattern*>(&other);
      if (that && that->element->isSamePattern(*that->element)) {
        return true;
      }
      return false;
    }

    Pattern::EquivalencyMapPtr OptPattern::isEquivalentPattern(const Pattern &other,
        EquivalencyMapPtr eq) const
    {
      if (id == other.id) {
        return eq;
      }
      auto that = dynamic_cast<const OptPattern*>(&other);
      if (that) {
        return element->isEquivalentPattern(*that->element, ::std::move(eq));
      }
      return nullptr;
    }

    ::std::string OptPattern::toString() const
    {
      auto res = element->toString();
      return res + "?";
    }

    ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> OptPattern::createTypeAndUpdateContext(
        const MatchContext::Ptr &context) const
    {
      auto res = element->createType(context);
      if (res.second) {
        res.second = ConstrainedOptType::get(res.second);
        auto ctx = res.first->update(self(), res.second);
        if (ctx) {
          res.first = ctx;
        } else {
          res.second = nullptr;
        }
      }
      return res;
    }

    MatchContext::Ptr OptPattern::match(const ConstrainedTypePtr &type,
        const MatchContext::Ptr &context) const
    {
      MatchContext::Ptr ctx;
      auto ty = type->self<ConstrainedOptType>();
      if (ty) {
        ctx = context->update(self(), type);
        if (ctx) {
          ctx = element->match(ty->element, ctx);
        }
      }

      return ctx;
    }

    ::std::shared_ptr<const OptPattern> OptPattern::create(Ptr element)
    {
      struct Impl: public OptPattern
      {
        Impl(Ptr e)
            : OptPattern(e)
        {
        }

        ~Impl()
        {
        }

      };
      return ::std::make_shared<Impl>(element);
    }
  }
}
