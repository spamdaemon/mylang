#ifndef CLASS_MYLANG_PATTERNS_MUTABLEPATTERN_H
#define CLASS_MYLANG_PATTERNS_MUTABLEPATTERN_H

#ifndef CLASS_MYLANG_PATTERNS_PATTERN_H
#include <mylang/patterns/Pattern.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDOPTTYPE_H
#include <mylang/constraints/ConstrainedMutableType.h>
#endif
#include <functional>

namespace mylang {
  namespace patterns {

    /**
     * The base class for all patterns.
     */
    class MutablePattern: public Pattern
    {
      MutablePattern(const MutablePattern&) = delete;

      MutablePattern& operator=(const MutablePattern&) = delete;

      /**
       * Default constructor
       */
    private:
      MutablePattern(Ptr element);

      /**
       * Destructor
       */
    public:
      ~MutablePattern();

      /**
       * Create a new generic array
       * @param element the element type
       */
    public:
      static ::std::shared_ptr<const MutablePattern> create(Ptr element);

    public:
      MatchContext::Ptr match(const ConstrainedTypePtr &type,
          const MatchContext::Ptr &context) const override;

    public:
      bool isSamePattern(const Pattern &other) const override;

      EquivalencyMapPtr isEquivalentPattern(const Pattern &other, EquivalencyMapPtr eq) const
          override;

      ::std::string toString() const override;

      Specificity getSpecificity() const override;

      bool isMoreSpecificThan(const Pattern &other) const override;

      bool isMoreGeneralThan(const Pattern &other) const override;

    protected:
      ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> createTypeAndUpdateContext(
          const MatchContext::Ptr &context) const override;

      /**
       * The element type
       */
    public:
      const Ptr element;
    };
  }
}
#endif
