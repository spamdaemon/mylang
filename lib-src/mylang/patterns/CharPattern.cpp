#include <mylang/patterns/CharPattern.h>
#include <mylang/constraints/ConstrainedCharType.h>

namespace mylang {
  namespace patterns {
    using namespace mylang::constraints;

    CharPattern::CharPattern()
    {

    }

    CharPattern::~CharPattern()
    {
    }

    bool CharPattern::isMoreGeneralThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const CharPattern*>(&other);
      if (!that) {
        return false;
      }
      return false;
    }

    bool CharPattern::isMoreSpecificThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const CharPattern*>(&other);
      if (!that) {
        return other.getSpecificity() == 0;
      }
      return false;
    }

    Pattern::Specificity CharPattern::getSpecificity() const
    {
      return 1;
    }

    bool CharPattern::isSamePattern(const Pattern &other) const
    {
      if (id == other.id) {
        return true;
      }
      auto that = dynamic_cast<const CharPattern*>(&other);
      if (that) {
        return true;
      }
      return false;
    }

    Pattern::EquivalencyMapPtr CharPattern::isEquivalentPattern(const Pattern &other,
        EquivalencyMapPtr eq) const
    {
      if (id == other.id) {
        return eq;
      }
      auto that = dynamic_cast<const CharPattern*>(&other);
      if (that) {
        return eq;
      }
      return nullptr;
    }

    ::std::string CharPattern::toString() const
    {
      return "char*";
    }

    ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> CharPattern::createTypeAndUpdateContext(
        const MatchContext::Ptr &context) const
    {
      ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> res(context, nullptr);
      return res;
    }

    MatchContext::Ptr CharPattern::match(const ConstrainedTypePtr &type,
        const MatchContext::Ptr &context) const
    {
      auto ty = type->self<ConstrainedCharType>();
      if (ty) {
        return context->update(self(), type);
      }
      return nullptr;
    }

    ::std::shared_ptr<const CharPattern> CharPattern::create()
    {
      struct Impl: public CharPattern
      {
        Impl()
        {
        }

        ~Impl()
        {
        }

      };
      return ::std::make_shared<Impl>();
    }
  }
}
