#include <mylang/patterns/StructPattern.h>
#include <mylang/constraints/ConstrainedStructType.h>

namespace mylang {
  namespace patterns {
    using namespace mylang::constraints;

    StructPattern::StructPattern()
    {

    }

    StructPattern::~StructPattern()
    {
    }

    bool StructPattern::isMoreGeneralThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const StructPattern*>(&other);
      if (!that) {
        return other.getSpecificity() == 0;
      }
      return false;
    }

    bool StructPattern::isMoreSpecificThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const StructPattern*>(&other);
      if (!that) {
        return other.getSpecificity() == 0;
      }
      return false;
    }

    Pattern::Specificity StructPattern::getSpecificity() const
    {
      return 1;
    }

    bool StructPattern::isSamePattern(const Pattern &other) const
    {
      if (id == other.id) {
        return true;
      }
      auto that = dynamic_cast<const StructPattern*>(&other);
      if (that) {
        return true;
      }
      return false;
    }

    Pattern::EquivalencyMapPtr StructPattern::isEquivalentPattern(const Pattern &other,
        EquivalencyMapPtr eq) const
    {
      if (id == other.id) {
        return eq;
      }
      auto that = dynamic_cast<const StructPattern*>(&other);
      if (that) {
        return eq;
      }
      return nullptr;
    }

    ::std::string StructPattern::toString() const
    {
      return "struct*";
    }

    ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> StructPattern::createTypeAndUpdateContext(
        const MatchContext::Ptr &context) const
    {
      ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> res(context, nullptr);
      ::std::cerr << "No struct type" << ::std::endl;
      return res;
    }

    MatchContext::Ptr StructPattern::match(const ConstrainedTypePtr &type,
        const MatchContext::Ptr &context) const
    {
      auto ty = type->self<ConstrainedStructType>();
      if (ty) {
        return context->update(self(), type);
      }
      return nullptr;
    }

    ::std::shared_ptr<const StructPattern> StructPattern::create()
    {
      struct Impl: public StructPattern
      {
        Impl()
        {
        }

        ~Impl()
        {
        }

      };
      return ::std::make_shared<Impl>();
    }
  }
}
