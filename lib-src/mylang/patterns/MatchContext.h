#ifndef CLASS_MYLANG_PATTERNS_MATCHCONTEXT_H
#define CLASS_MYLANG_PATTERNS_MATCHCONTEXT_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif
#include <iostream>

namespace mylang {
  namespace patterns {

    /**
     * The match context is used when matching a constrained type against
     * a generic. When there is a match the generic instance is assigned the type.
     */
    class MatchContext: public ::std::enable_shared_from_this<MatchContext>
    {
      MatchContext(const MatchContext&) = delete;

      MatchContext& operator=(const MatchContext&) = delete;

      /** A match context pointer */
    public:
      typedef ::std::shared_ptr<const MatchContext> Ptr;

      /**
       * The default constructor for a match context
       */
    private:
      MatchContext();

      /** Destructor */
    public:
      virtual ~MatchContext();

      /**
       * Get a new match context
       * @return a new match context
       */
    public:
      static Ptr create();

      /**
       * Create a new context by associating a pattern with a type.
       * @param pattern a pattern
       * @param type a type
       * @return a match context, or null if the pattern was associated with a different type
       */
    public:
      virtual Ptr update(const PatternPtr &pattern, const ConstrainedTypePtr &type) const;

      /**
       * Create a new context by associating a pattern with a compile=time constant.
       * @param pattern a pattern
       * @param value a constant value
       * @return a match context, or null if the pattern was associated with a different type
       */
    public:
      virtual Ptr update(const PatternPtr &pattern, const ConstantPtr &value) const;

      /**
       * Get the type that is associated with the specified pattern.
       * @param pattern a pattern
       * @return a type currently associated with the pattern, or nullptr if not found
       */
    public:
      virtual ConstrainedTypePtr findType(const PatternPtr &pattern) const;

      /**
       * Get the value that is associated with the specified pattern.
       * @param pattern a pattern
       * @return a value currently associated with the pattern, or nullptr if not found
       */
    public:
      virtual ConstantPtr findValue(const PatternPtr &pattern) const;

      /**
       * Print a debug representation of this context
       * @param out a stream
       */
    public:
      virtual void debug(::std::ostream &out) const = 0;

      /**
       * Update this context with a new pattern. Value and type are mutually exclusive.
       * @param pattern a pattern
       * @param value the value to be associated (can be nullptr)
       * @param type the type to be associated (can be nullptr)
       * @return a new context, or null if the pattern was associated with a different type
       */
    private:
      static Ptr create(Ptr parent, const PatternPtr &pattern, const ConstantPtr &value,
          const ConstrainedTypePtr &type);

    };

  }
}
#endif
