#ifndef CLASS_MYLANG_PATTERNS_PATTERN_H
#define CLASS_MYLANG_PATTERNS_PATTERN_H
#include <stddef.h>
#include <memory>
#include <string>

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_PATTERNS_MATCHCONTEXT_H
#include <mylang/patterns/MatchContext.h>
#endif
#include <functional>
#include <utility>
#include <map>

namespace mylang {
  namespace patterns {

    /**
     * The base class for all patterns.
     */
    class Pattern: public ::std::enable_shared_from_this<Pattern>
    {
      Pattern(const Pattern&) = delete;

      Pattern& operator=(const Pattern&) = delete;

      /** A pattern name */
    public:
      typedef size_t PatternId;

      /**
       * An equivalency map.
       */
    public:
      typedef ::std::map<PatternId, PatternId> EquivalencyMap;
      typedef ::std::unique_ptr<EquivalencyMap> EquivalencyMapPtr;
      typedef size_t Specificity;

      /** A pointer to a generic */
    public:
      typedef ::std::shared_ptr<const Pattern> Ptr;

      /**
       * Construct a pattern.
       */
    protected:
      Pattern();

      /**
       * Destructor
       */
    public:
      virtual ~Pattern();

      /**
       * Create a generic that can match any type.
       * @return a generic pattern
       */
    public:
      static Ptr create();

      /**
       * Determine if this pattern is the same as the specified pattern
       * @param other another pattern
       * @return true if the two types are the same
       */
    public:
      virtual bool isSamePattern(const Pattern &other) const = 0;

      /**
       * Determine if this pattern is equivalent to the specified pattern.
       * @param other a pattern
       * @param eq a mapping of pattern names that are equivalent
       * @return a new equivalency map, or nullptr if not equivalent
       */
    public:
      virtual EquivalencyMapPtr isEquivalentPattern(const Pattern &other,
          EquivalencyMapPtr eq) const = 0;

      /**
       * Get the specificity for this pattern. The higher the returned
       * number, the more specific the pattern is. A return value of 0 indicates
       * no specificity.
       * @return the specificity
       */
    public:
      virtual Specificity getSpecificity() const = 0;

      /**
       * Determine if this pattern is more specific than the specified pattern.
       * @param other  a pattern
       * @return true if this pattern is strictly more specific than the given pattern.
       */
    public:
      virtual bool isMoreSpecificThan(const Pattern &other) const = 0;

      /**
       * Determine if this pattern is more general than the specified pattern.
       * @param other  a pattern
       * @return true if this pattern is strictly more general than the given pattern.
       */
    public:
      virtual bool isMoreGeneralThan(const Pattern &other) const = 0;

    public:
      static EquivalencyMapPtr createEquivalencyMap();

      /**
       * Get a string representation of this pattern.
       * @return a string representation of this pattern
       */
    public:
      virtual ::std::string toString() const = 0;

      /**
       * Get this pattern as a specified type.
       * @return this pattern or nullptr if it's not if the requested type.
       */
    public:
      template<class T = Pattern>
      inline ::std::shared_ptr<const T> self() const
      {
        return ::std::dynamic_pointer_cast<const T>(shared_from_this());
      }

      /**
       * Match this pattern against the specified type constraint
       * @param cs a type constraint
       * @param context a context
       * @return a new match context or if a match could not per performed
       */
    public:
      virtual MatchContext::Ptr match(const ConstrainedTypePtr &type,
          const MatchContext::Ptr &context) const;

      /**
       * Given a context, turn this pattern into a corresponding type. The context
       * may need to be updated as part of this operation, and a new context may be returned.
       * @param context a match context
       * @return a type constraint or nullptr on error and a context, which is never nullptr
       */
    public:
      ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> createType(
          const MatchContext::Ptr &context) const;

      /**
       * Given a context, turn this pattern into a corresponding type. The context
       * may need to be updated as part of this operation, and a new context may be returned.
       * @param context a match context
       * @return a type constraint or nullptr on error and a context, which is never nullptr
       */
    protected:
      virtual ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> createTypeAndUpdateContext(
          const MatchContext::Ptr &context) const = 0;

      /** A unique id */
    public:
      const PatternId id;
    };
  }
}
#endif
