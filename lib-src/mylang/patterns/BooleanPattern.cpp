#include <mylang/patterns/BooleanPattern.h>
#include <mylang/constraints/ConstrainedBooleanType.h>

namespace mylang {
  namespace patterns {
    using namespace mylang::constraints;

    BooleanPattern::BooleanPattern()
    {

    }

    BooleanPattern::~BooleanPattern()
    {
    }

    bool BooleanPattern::isMoreGeneralThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const BooleanPattern*>(&other);
      if (!that) {
        return false;
      }
      return false;
    }

    bool BooleanPattern::isMoreSpecificThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const BooleanPattern*>(&other);
      if (!that) {
        return other.getSpecificity() == 0;
      }
      return false;
    }

    Pattern::Specificity BooleanPattern::getSpecificity() const
    {
      return 1;
    }

    bool BooleanPattern::isSamePattern(const Pattern &other) const
    {
      if (id == other.id) {
        return true;
      }
      auto that = dynamic_cast<const BooleanPattern*>(&other);
      if (that) {
        return true;
      }
      return false;
    }

    Pattern::EquivalencyMapPtr BooleanPattern::isEquivalentPattern(const Pattern &other,
        EquivalencyMapPtr eq) const
    {
      if (id == other.id) {
        return eq;
      }
      auto that = dynamic_cast<const BooleanPattern*>(&other);
      if (that) {
        return eq;
      }
      return nullptr;
    }

    ::std::string BooleanPattern::toString() const
    {
      return "boolean*";
    }

    ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> BooleanPattern::createTypeAndUpdateContext(
        const MatchContext::Ptr &context) const
    {
      ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> res(context, nullptr);
      return res;
    }

    MatchContext::Ptr BooleanPattern::match(const ConstrainedTypePtr &type,
        const MatchContext::Ptr &context) const
    {
      auto ty = type->self<ConstrainedBooleanType>();
      if (ty) {
        return context->update(self(), type);
      }
      return nullptr;
    }

    ::std::shared_ptr<const BooleanPattern> BooleanPattern::create()
    {
      struct Impl: public BooleanPattern
      {
        Impl()
        {
        }

        ~Impl()
        {
        }

      };
      return ::std::make_shared<Impl>();
    }
  }
}
