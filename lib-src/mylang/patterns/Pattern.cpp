#include <mylang/constraints/ConstrainedType.h>
#include <mylang/patterns/Pattern.h>
#include <stddef.h>
#include <initializer_list>
#include <memory>
#include <string>

namespace mylang {
  namespace patterns {
    using namespace mylang::constraints;
    static size_t patternNames = 0;

    Pattern::Pattern()
        : id(patternNames++)
    {
    }

    Pattern::~Pattern()
    {
    }

    MatchContext::Ptr Pattern::match(const ConstrainedTypePtr &type,
        const MatchContext::Ptr &context) const
    {
      return context->update(self(), type);
    }

    ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> Pattern::createType(
        const MatchContext::Ptr &context) const
    {
      auto ty = context->findType(self());
      if (ty) {
        return ::std::make_pair(context, ty);
      } else {
        return createTypeAndUpdateContext(context);
      }
    }

    Pattern::EquivalencyMapPtr Pattern::createEquivalencyMap()
    {
      return ::std::make_unique<EquivalencyMap>();
    }

    Pattern::Ptr Pattern::create()
    {
      struct Impl: public Pattern
      {
        Impl()
            : name("$" + ::std::to_string(id))
        {
        }

        ~Impl()
        {
        }

        Specificity getSpecificity() const override
        {
          return 0;
        }

        bool isMoreSpecificThan(const Pattern&) const
        {
          return false;
        }

        bool isMoreGeneralThan(const Pattern &other) const
        {
          return other.getSpecificity() > 0;
        }

        bool isSamePattern(const Pattern &other) const override
        {
          return other.id == id;
        }

        EquivalencyMapPtr isEquivalentPattern(const Pattern &other, EquivalencyMapPtr eq) const
        override
        {
          auto that = dynamic_cast<const Impl*>(&other);
          if (that) {
            auto i = eq->find(id);
            auto j = eq->find(that->id);
            if (i == eq->end() && j == eq->end()) {
              eq->insert(::std::make_pair(id, that->id));
              eq->insert(::std::make_pair(that->id, this->id));
              return eq;
            } else if (i == eq->end() || j == eq->end()) {
              return nullptr;
            } else if ((i->second == j->first) && (j->second == i->first)) {
              return eq;
            }
          }
          return nullptr;
        }

        ::std::string toString() const
        {
          return name;
        }

        ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> createTypeAndUpdateContext(
            const MatchContext::Ptr &context) const
        {
          return ::std::pair<MatchContext::Ptr, ConstrainedTypePtr>(context, nullptr);
        }

      private:
        const ::std::string name;
      };
      return ::std::make_shared<Impl>();
    }

  }
}
