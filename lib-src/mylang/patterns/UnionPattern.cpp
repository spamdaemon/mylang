#include <mylang/patterns/UnionPattern.h>
#include <mylang/constraints/ConstrainedUnionType.h>

namespace mylang {
  namespace patterns {
    using namespace mylang::constraints;

    UnionPattern::UnionPattern()
    {

    }

    UnionPattern::~UnionPattern()
    {
    }

    bool UnionPattern::isMoreGeneralThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const UnionPattern*>(&other);
      if (!that) {
        return false;
      }
      return false;
    }

    bool UnionPattern::isMoreSpecificThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const UnionPattern*>(&other);
      if (!that) {
        return other.getSpecificity() == 0;
      }
      return false;
    }

    Pattern::Specificity UnionPattern::getSpecificity() const
    {
      return 1;
    }

    bool UnionPattern::isSamePattern(const Pattern &other) const
    {
      if (id == other.id) {
        return true;
      }
      auto that = dynamic_cast<const UnionPattern*>(&other);
      if (that) {
        return true;
      }
      return false;
    }

    Pattern::EquivalencyMapPtr UnionPattern::isEquivalentPattern(const Pattern &other,
        EquivalencyMapPtr eq) const
    {
      if (id == other.id) {
        return eq;
      }
      auto that = dynamic_cast<const UnionPattern*>(&other);
      if (that) {
        return eq;
      }
      return nullptr;
    }

    ::std::string UnionPattern::toString() const
    {
      return "union*";
    }

    ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> UnionPattern::createTypeAndUpdateContext(
        const MatchContext::Ptr &context) const
    {
      ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> res(context, nullptr);
      return res;
    }

    MatchContext::Ptr UnionPattern::match(const ConstrainedTypePtr &type,
        const MatchContext::Ptr &context) const
    {
      auto ty = type->self<ConstrainedUnionType>();
      if (ty) {
        return context->update(self(), type);
      }
      return nullptr;
    }

    ::std::shared_ptr<const UnionPattern> UnionPattern::create()
    {
      struct Impl: public UnionPattern
      {
        Impl()
        {
        }

        ~Impl()
        {
        }

      };
      return ::std::make_shared<Impl>();
    }
  }
}
