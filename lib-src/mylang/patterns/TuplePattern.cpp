#include <mylang/patterns/TuplePattern.h>
#include <mylang/constraints/ConstrainedTupleType.h>

namespace mylang {
  namespace patterns {
    using namespace mylang::constraints;

    TuplePattern::TuplePattern()
    {

    }

    TuplePattern::~TuplePattern()
    {
    }

    bool TuplePattern::isMoreGeneralThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const TuplePattern*>(&other);
      if (!that) {
        return false;
      }
      return false;
    }

    bool TuplePattern::isMoreSpecificThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const TuplePattern*>(&other);
      if (!that) {
        return other.getSpecificity() == 0;
      }
      return false;
    }

    Pattern::Specificity TuplePattern::getSpecificity() const
    {
      return 1;
    }

    bool TuplePattern::isSamePattern(const Pattern &other) const
    {
      if (id == other.id) {
        return true;
      }
      auto that = dynamic_cast<const TuplePattern*>(&other);
      if (that) {
        return true;
      }
      return false;
    }

    Pattern::EquivalencyMapPtr TuplePattern::isEquivalentPattern(const Pattern &other,
        EquivalencyMapPtr eq) const
    {
      if (id == other.id) {
        return eq;
      }
      auto that = dynamic_cast<const TuplePattern*>(&other);
      if (that) {
        return eq;
      }
      return nullptr;
    }

    ::std::string TuplePattern::toString() const
    {
      return "tuple*";
    }

    ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> TuplePattern::createTypeAndUpdateContext(
        const MatchContext::Ptr &context) const
    {
      ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> res(context, nullptr);
      return res;
    }

    MatchContext::Ptr TuplePattern::match(const ConstrainedTypePtr &type,
        const MatchContext::Ptr &context) const
    {
      auto ty = type->self<ConstrainedTupleType>();
      if (ty) {
        return context->update(self(), type);
      }
      return nullptr;
    }

    ::std::shared_ptr<const TuplePattern> TuplePattern::create()
    {
      struct Impl: public TuplePattern
      {
        Impl()
        {
        }

        ~Impl()
        {
        }

      };
      return ::std::make_shared<Impl>();
    }
  }
}
