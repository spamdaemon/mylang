#include <mylang/patterns/BytePattern.h>
#include <mylang/constraints/ConstrainedByteType.h>

namespace mylang {
  namespace patterns {
    using namespace mylang::constraints;

    BytePattern::BytePattern()
    {

    }

    BytePattern::~BytePattern()
    {
    }

    bool BytePattern::isMoreGeneralThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const BytePattern*>(&other);
      if (!that) {
        return false;
      }
      return false;
    }

    bool BytePattern::isMoreSpecificThan(const Pattern &other) const
    {
      if (id == other.id) {
        return false;
      }
      auto that = dynamic_cast<const BytePattern*>(&other);
      if (!that) {
        return other.getSpecificity() == 0;
      }
      return false;
    }

    Pattern::Specificity BytePattern::getSpecificity() const
    {
      return 1;
    }

    bool BytePattern::isSamePattern(const Pattern &other) const
    {
      if (id == other.id) {
        return true;
      }
      auto that = dynamic_cast<const BytePattern*>(&other);
      if (that) {
        return true;
      }
      return false;
    }

    Pattern::EquivalencyMapPtr BytePattern::isEquivalentPattern(const Pattern &other,
        EquivalencyMapPtr eq) const
    {
      if (id == other.id) {
        return eq;
      }
      auto that = dynamic_cast<const BytePattern*>(&other);
      if (that) {
        return eq;
      }
      return nullptr;
    }

    ::std::string BytePattern::toString() const
    {
      return "byte*";
    }

    ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> BytePattern::createTypeAndUpdateContext(
        const MatchContext::Ptr &context) const
    {
      ::std::pair<MatchContext::Ptr, ConstrainedTypePtr> res(context, nullptr);
      return res;
    }

    MatchContext::Ptr BytePattern::match(const ConstrainedTypePtr &type,
        const MatchContext::Ptr &context) const
    {
      auto ty = type->self<ConstrainedByteType>();
      if (ty) {
        return context->update(self(), type);
      }
      return nullptr;
    }

    ::std::shared_ptr<const BytePattern> BytePattern::create()
    {
      struct Impl: public BytePattern
      {
        Impl()
        {
        }

        ~Impl()
        {
        }

      };
      return ::std::make_shared<Impl>();
    }
  }
}
