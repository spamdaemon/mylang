#include <mylang/BigReal.h>
#include <mylang/BigInt.h>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <cmath>

namespace mylang {

  BigReal::BigReal()
      : value(0)
  {
  }

  BigReal::BigReal(double xvalue)
      : value(xvalue)
  {
  }

  BigReal::BigReal(const BigInt &xvalue)
      : value(xvalue.signedValue())
  {
  }

  BigReal::~BigReal()
  {
  }

  double BigReal::doubleValue() const
  {
    return value;
  }

  BigReal BigReal::parse(const ::std::string &str)
  {
    BigReal res;
    size_t n = 0;
    res.value = ::std::stold(str, &n);
    if (n != str.size()) {
      throw ::std::invalid_argument("Not a valid real value: " + str);
    }
    return res;
  }

  BigReal BigReal::negate() const
  {
    return -value;
  }

  BigReal BigReal::add(const BigReal &op2) const
  {
    return value + op2.value;
  }

  BigReal BigReal::subtract(const BigReal &op2) const
  {
    return value - op2.value;
  }

  BigReal BigReal::multiply(const BigReal &op2) const
  {
    return value * op2.value;
  }

  BigReal BigReal::divide(const BigReal &op2) const
  {
    return value / op2.value;
  }

  BigReal BigReal::remainder(const BigReal &op2) const
  {
    return ::std::fmod(value, op2.value);
  }

  BigReal BigReal::abs() const
  {
    return ::std::abs(value);
  }

  int BigReal::compare(const BigReal &op2) const
  {
    return value < op2.value ? -1 : (value == op2.value ? 0 : 1);
  }

  ::std::ostream& BigReal::write(::std::ostream &out) const
  {
    return out << std::hexfloat << value;
  }

  ::std::string BigReal::toString() const
  {
    ::std::ostringstream out;
    out << std::hexfloat << value;
    return out.str();
  }

  ::std::string BigReal::toHexString() const
  {
    ::std::ostringstream out;
    out << std::hexfloat << value;
    return out.str();
  }

  ::std::string BigReal::toDecimalString(int prec) const
  {
    ::std::ostringstream out;
    out << std::fixed << ::std::setprecision(prec) << value;
    return out.str();
  }

}
