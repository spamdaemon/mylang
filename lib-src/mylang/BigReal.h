#ifndef CLASS_MYLANG_BIGREAL_H
#define CLASS_MYLANG_BIGREAL_H
#include <iosfwd>
#include <string>
#include <gmp.h>

namespace mylang {

  class BigInt;

  /** A multiprecision integer class */
  class BigReal
  {
    /** Default constructor with the value 0 */
  public:
    BigReal();

    /** Constructor with a builtin integer */
  public:
    BigReal(double value);

    /** Constructor with a builtin integer */
  public:
    BigReal(const BigInt &integer);

    /**
     * Destructor.
     */
  public:
    ~BigReal();

    /**
     * Parse a string as a real
     * @param str a string
     * @return a bigint
     */
  public:
    static BigReal parse(const ::std::string &str);

    /**
     * Get a double value.
     * @return the double value
     * @throws Exception if the value does not fit
     */
  public:
    double doubleValue() const;

    /**
     * Add two big reaks.
     * @param y a real
     * @return a new bigint
     */
  public:
    BigReal add(const BigReal &op2) const;

    /**
     * Add two big ints.
     * @param y a real
     * @return a new bigint
     */
  public:
    BigReal subtract(const BigReal &op2) const;

    /**
     * Negate this integer.
     * @return a new bigint
     */
  public:
    BigReal negate() const;

    /**
     * Add two big ints.
     * @param y a real
     * @return a new bigint
     */
  public:
    BigReal multiply(const BigReal &op2) const;

    /**
     * Add two big ints.
     * @param y a real
     * @return a new bigint
     */
  public:
    BigReal divide(const BigReal &op2) const;

    /**
     * Add two big ints.
     * @param y a real
     * @return a new bigint
     */
  public:
    BigReal remainder(const BigReal &op2) const;

    /**
     * Get the absolute value.
     * @return the absolute value
     */
  public:
    BigReal abs() const;

    /**
     * Compare
     * @param y a real
     * @return -1 if this integer is less, 0 if equal, 1 if greater than y
     */
  public:
    int compare(const BigReal &op2) const;

    /**
     * Write this real to a stream
     * @param out a stream
     * @return the outoput stream
     */
  public:
    ::std::ostream& write(::std::ostream &out) const;

    /**
     * Get the string representation.
     * @return a string
     */
  public:
    ::std::string toString() const;

    /**
     * Get the decimal string representation.
     * @param precision
     * @return a decimal string
     */
  public:
    ::std::string toDecimalString(int prec) const;

    /**
     * Get the hex string representation.
     * @return a hex string
     */
  public:
    ::std::string toHexString() const;

    /** The operators */
  public:
    inline friend BigReal operator+(const BigReal &x, const BigReal &y)
    {
      return x.add(y);
    }

    inline friend BigReal operator-(const BigReal &x, const BigReal &y)
    {
      return x.subtract(y);
    }

    inline friend BigReal operator*(const BigReal &x, const BigReal &y)
    {
      return x.multiply(y);
    }

    inline friend BigReal operator/(const BigReal &x, const BigReal &y)
    {
      return x.divide(y);
    }

    inline friend BigReal operator-(const BigReal &x)
    {
      return x.negate();
    }

    inline friend bool operator<(const BigReal &x, const BigReal &y)
    {
      return x.compare(y) < 0;
    }

    inline friend bool operator<=(const BigReal &x, const BigReal &y)
    {
      return x.compare(y) <= 0;
    }

    inline friend bool operator>(const BigReal &x, const BigReal &y)
    {
      return x.compare(y) > 0;
    }

    inline friend bool operator>=(const BigReal &x, const BigReal &y)
    {
      return x.compare(y) >= 0;
    }

    inline friend bool operator==(const BigReal &x, const BigReal &y)
    {
      return x.compare(y) == 0;
    }

    inline friend bool operator!=(const BigReal &x, const BigReal &y)
    {
      return x.compare(y) != 0;
    }

    inline friend ::std::ostream& operator<<(::std::ostream &s, const BigReal &x)
    {
      return x.write(s);
    }

    /** The real value */
  private:
    double value;
  };

}
#endif
