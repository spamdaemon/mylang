#include <mylang/SourceLocation.h>
#include <sstream>
#include <cassert>

namespace mylang {
  SourceLocation::SourceLocation()
  {
    _text = "no-source";
  }

  SourceLocation::SourceLocation(const ::std::string &file, size_t line)
  {
    ::std::ostringstream out;
    out << file << ":" << line;
    _text = out.str();
  }

  SourceLocation::SourceLocation(::std::string file)
      : _text(::std::move(file))
  {
  }

  SourceLocation::SourceLocation(::idioma::ast::Node::Span span)
      : _span(::std::move(span))
  {
    // eventually, this should always be true, but since we're
    // removing ignorable nodes from the original tree we lose information
    // in some circumstances. For example, the empty array literal node [] will have
    // no span, because the '[' and ']' are ignorable tokens.

    //assert(_span.first &&  "WARNING: no source location provided");

    if (_span.first) {
      ::std::ostringstream out;
      _span.first->print(out);
      _text = out.str();
    } else {
      ::std::cerr << "WARNING: no source location provided" << ::std::endl;
      _text = "unknown";
    }
  }

  void SourceLocation::print(::std::ostream &out) const
  {
    out << _text;
  }
}
