#include <mylang/filesystem/EmptyFS.h>

namespace mylang::filesystem {
  EmptyFS::~EmptyFS()
  {
  }

  ::std::shared_ptr<EmptyFS> EmptyFS::create()
  {
    return ::std::make_shared<EmptyFS>();
  }

  ::std::optional<EmptyFS::Details> EmptyFS::getDetails(const FQN&) const
  {
    return ::std::nullopt;
  }

  ::std::unique_ptr<::std::istream> EmptyFS::open(const FQN&, Details*) const
  {
    return nullptr;
  }
  bool EmptyFS::enumerate(const ::std::optional<FQN>&, const Callback&) const
  {
    return true;
  }
}
