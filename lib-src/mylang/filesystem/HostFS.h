#ifndef CLASS_MYLANG_FILESYSTEM_HOSTFS_H
#define CLASS_MYLANG_FILESYSTEM_HOSTFS_H

#ifndef CLASS_MYLANG_FILESYSTEM_ITERABLEFILESYSTEM_H
#include <mylang/filesystem/IterableFileSystem.h>
#endif

#include <filesystem>

namespace mylang::filesystem {
  /**
   * A filesystem on the current host operating system.
   */
  class HostFS: public IterableFileSystem
  {
    /**
     * Create a host FS with the specified base directory.
     * The the directory does not exist then a filesystem_error.
     * @param dir a directory
     */
  public:
    HostFS(::std::filesystem::path dir);

  public:
    ~HostFS();

    /**
     * Create a new host file system.
     * If the directory does not exist then nullptr is returned.
     * @param the base directory
     * @return a file system
     */
  public:
    static ::std::shared_ptr<HostFS> create(::std::filesystem::path dir);

  public:
    ::std::optional<Details> getDetails(const FQN &fqn) const override;

  public:
    ::std::unique_ptr<::std::istream> open(const FQN &fqn, Details *out) const;

  public:
    bool enumerate(const ::std::optional<FQN> &prefix, const Callback &cb) const override;

    /** The base directory */
  public:
    const ::std::filesystem::path base;
  };
}
#endif
