#include <mylang/filesystem/FileSystem.h>
#include <mylang/filesystem/EmptyFS.h>
#include <mylang/filesystem/HostFS.h>
#include <mylang/filesystem/UnionFS.h>

namespace mylang::filesystem {
  namespace {
    static ::std::function<FileSystem::Ptr(::std::filesystem::path)> fsConstructor;
  }

  FileSystem::AmbiguousFQN::AmbiguousFQN(const FQN &xfqn)
      : ::std::runtime_error("Ambiguous FQN " + xfqn.fullName()), fqn(xfqn)
  {
  }
  FileSystem::AmbiguousFQN::~AmbiguousFQN()
  {
  }

  FileSystem::FileSystem()
  {
  }
  FileSystem::~FileSystem()
  {
  }

  void FileSystem::setDefaultFilesystem(::std::function<Ptr(::std::filesystem::path)> ctor)
  {
    fsConstructor = ::std::move(ctor);
  }

  FileSystem::Ptr FileSystem::empty()
  {
    return EmptyFS::create();
  }

  FileSystem::Ptr FileSystem::create(::std::filesystem::path absPath)
  {
    Ptr res;
    if (fsConstructor) {
      res = fsConstructor(::std::move(absPath));
    } else {
      res = ::std::make_shared<HostFS>(::std::move(absPath));
    }
    if (!res) {
      res = empty();
    }
    return res;
  }

  FileSystem::Ptr FileSystem::unionOf(::std::set<Ptr> fs)
  {
    auto xfs = UnionFS::create(::std::move(fs));
    if (xfs->fs.empty()) {
      return empty();
    }
    if (xfs->fs.size() == 1) {
      return *xfs->fs.begin();
    }
    return xfs;
  }

}
