#ifndef MYLANG_FILESYSTEM_FILESYSTEM_H
#define MYLANG_FILESYSTEM_FILESYSTEM_H

#ifndef MYLANG_NAMES_FQN_H
#include <mylang/names/FQN.h>
#endif

#include <filesystem>
#include <functional>
#include <iosfwd>
#include <memory>
#include <optional>
#include <set>
#include <stdexcept>

namespace mylang::filesystem {

  /**
   * A simple file system abstraction for FQNs.
   *
   * The simplest of filesystem allows us to open a stream
   * corresponding to an FQN.
   *
   * If an FQN can be resolved, then it will be associated with
   * a filesystem specific identifier. It is an error for an FQN
   * to be associated with two or more different such identifiers.
   *
   * In addition to opening files, the details about an FQN can be
   * queried.
   */
  class FileSystem: public ::std::enable_shared_from_this<FileSystem>
  {
    /** A pointer to a file system */
  public:
    typedef ::std::shared_ptr<FileSystem> Ptr;

    /** The file is always an FQN */
  public:
    typedef ::mylang::names::FQN FQN;

    /** A filesystem specific identifier for an FQN. */
  public:
    typedef ::std::string Identifier;

    /** The modification time of a file */
  public:
    typedef ::std::filesystem::file_time_type ModificationTime;

    /** Details about a file */
  public:
    struct Details
    {
      /** The file identifier in the file system */
      Identifier identifier;

      /** The time the file was last modified */
      ModificationTime lastModifiedTime;
    };

    /** The exception that is thrown when a FQN is determined to be ambiguous */
  public:
    class AmbiguousFQN: public ::std::runtime_error
    {
    public:
      AmbiguousFQN(const FQN &fqn);

    public:
      ~AmbiguousFQN();

      /** The fqn */
    public:
      const FQN fqn;
    };
  protected:
    FileSystem();

  public:
    virtual ~FileSystem() = 0;

    /**
     * Set the default filesystem constructor
     * @param ctor a constructor function
     */
  public:
    static void setDefaultFilesystem(::std::function<Ptr(::std::filesystem::path)> ctor);

    /**
     * Create a simple file-based filesystem.
     *
     * An FQN f is resolved to a file relative to the
     * base directory as follows:
     * <code>
     * f.fullName("/") + ".ka"
     * </code>
     *
     * The path is canonicalized and if it does not exist, empty() is returned.
     *
     * @param base the base directory under which FQN are resolved to filenames.
     * @return a filesystem
     */
  public:
    static Ptr create(::std::filesystem::path base);

    /**
     * Create an empty file system.
     *
     * Every invocation of open returns nullptr.
     * @return an a filesystem
     */
  public:
    static Ptr empty();

    /**
     * Union of file systems.
     *
     * Aggregate multiple different filesystem into a single
     * unified filesystem. If an FQN is associated with more
     * than two or more different identifiers, then the that
     * FQN cannot be opened.
     *
     * The following is would be a valid union, because the
     * the any FQN will be associated with the same file:
     * <code>
     * unionOf({create("foo"), create("foo")});
     * </code>
     *
     * @param fs a list of file systems
     * @return an a filesystem
     */
  public:
    static Ptr unionOf(::std::set<Ptr> fs);

    /**
     * Get the unique identifier for the specified FQN.
     * @param fqn an fqn
     * @return the FQN details or nothing if FQN is not known
     * @throws AmbiguousFQN if the fqn is ambiguous
     */
  public:
    virtual ::std::optional<Details> getDetails(const FQN &fqn) const = 0;

    /**
     * Open a stream to a file in the filesystem.
     * If the FQN is associated with more than two different identifiers, then
     * the AmbiguousFQN error is thrown. If the FQN could not be resolved, then
     * nullptr is returned.
     *
     * @param fqn a fqn
     * @param outIdent if not nullptr, fill in with the identifier for the FQN
     * @return a stream or nullptr if not found
     * @throws AmbiguousFQN if the fqn is ambiguous
     */
  public:
    virtual ::std::unique_ptr<::std::istream> open(const FQN &fqn,
        Details *outDetails = nullptr) const = 0;
  };
}

#endif
