#include <mylang/filesystem/IterableFileSystem.h>
#include <mylang/filesystem/EmptyFS.h>
#include <mylang/filesystem/HostFS.h>
#include <mylang/filesystem/IterableUnionFS.h>

namespace mylang::filesystem {

  namespace {
    static ::std::function<IterableFileSystem::Ptr(::std::filesystem::path)> fsConstructor;
  }

  IterableFileSystem::IterableFileSystem()
  {
  }
  IterableFileSystem::~IterableFileSystem()
  {
  }

  void IterableFileSystem::setDefaultFilesystem(::std::function<Ptr(::std::filesystem::path)> ctor)
  {
    fsConstructor = ::std::move(ctor);
  }

  IterableFileSystem::Ptr IterableFileSystem::empty()
  {
    return ::std::make_shared<EmptyFS>();
  }

  IterableFileSystem::Ptr IterableFileSystem::create(::std::filesystem::path base)
  {
    ::std::error_code ec;
    auto absPath = ::std::filesystem::canonical(base, ec);
    if (ec) {
      return empty();
    }
    Ptr res;
    if (fsConstructor) {
      res = fsConstructor(::std::move(absPath));
    } else {
      res = ::std::make_shared<HostFS>(::std::move(absPath));
    }
    if (!res) {
      res = empty();
    }
    return res;

    return res;
  }

  IterableFileSystem::Ptr IterableFileSystem::unionOf(const ::std::set<Ptr> &fs)
  {
    auto xfs = IterableUnionFS::create(fs);
    if (xfs->fs.empty()) {
      return empty();
    }
    if (xfs->fs.size() == 1) {
      return *xfs->fs.begin();
    }
    return xfs;
  }

  bool IterableFileSystem::enumerate(const Callback &cb) const
  {
    return enumerate(::std::nullopt, cb);
  }

  bool IterableFileSystem::enumerate(const ::std::set<FQN> &prefixes, const Callback &cb) const
  {
    if (prefixes.empty()) {
      return enumerate(::std::nullopt, cb);
    }
    for (const auto &p : FQN::normalizePrefixes(prefixes)) {
      ::std::optional<FQN> prefix(p);
      if (!enumerate(prefix, cb)) {
        return false;
      }
    }
    return true;
  }

}
