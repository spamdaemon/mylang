#include <mylang/filesystem/UnionFS.h>
#include <mylang/filesystem/EmptyFS.h>

namespace mylang::filesystem {
  UnionFS::UnionFS(::std::set<Ptr> list, bool xlenient)
      : fs(::std::move(list)), lenient(xlenient)
  {
  }

  UnionFS::~UnionFS()
  {
  }

  ::std::shared_ptr<UnionFS> UnionFS::makeLenient()
  {
    if (lenient) {
      return ::std::dynamic_pointer_cast<UnionFS>(shared_from_this());
    } else {
      return ::std::make_shared<UnionFS>(fs, true);
    }
  }

  ::std::shared_ptr<UnionFS> UnionFS::create(::std::set<Ptr> xlist)
  {
    ::std::set<Ptr> flat;
    for (const auto &p : xlist) {
      if (!p || ::std::dynamic_pointer_cast<EmptyFS>(p)) {
        continue;
      }
      if (auto ufs = ::std::dynamic_pointer_cast<UnionFS>(p); ufs) {
        flat.insert(ufs->fs.begin(), ufs->fs.end());
      } else {
        flat.insert(p);
      }
    }
    return ::std::make_shared<UnionFS>(::std::move(flat));
  }

  // locate a filesystem for the specified fqn
  FileSystem* UnionFS::find(const FQN &fqn, ::std::optional<Details> &out) const
  {
    FileSystem *res = nullptr;
    out = ::std::nullopt;
    for (const auto &f : fs) {
      auto tmp = f->getDetails(fqn);
      if (tmp && out) {
        if (tmp->identifier != out->identifier) {
          throw AmbiguousFQN(fqn);
        }
      }
      if (res == nullptr && tmp) {
        out = ::std::move(tmp);
        res = f.get();
        // we need to keep looking, in case we have another mapping
        // for the same FQN
        if (lenient) {
          break;
        }
      }
    }

    return res;
  }

  ::std::optional<FileSystem::Details> UnionFS::getDetails(const FQN &fqn) const
  {
    ::std::optional<Details> res;
    find(fqn, res);
    return res;
  }

  ::std::unique_ptr<::std::istream> UnionFS::open(const FQN &fqn, Details *out) const

  {
    ::std::optional<Details> res;
    auto f = find(fqn, res);
    if (f) {
      return f->open(fqn, out);
    }
    return nullptr;
  }
}
