#ifndef CLASS_MYLANG_FILESYSTEM_EMPTYFS_H
#define CLASS_MYLANG_FILESYSTEM_EMPTYFS_H

#ifndef CLASS_MYLANG_FILESYSTEM_ITERABLEFILESYSTEM_H
#include <mylang/filesystem/IterableFileSystem.h>
#endif

namespace mylang::filesystem {
  class EmptyFS: public IterableFileSystem
  {
  public:
    ~EmptyFS();

  public:
    static ::std::shared_ptr<EmptyFS> create();

    ::std::optional<Details> getDetails(const FQN&) const override;
    ::std::unique_ptr<::std::istream> open(const FQN&, Details*) const override;
    bool enumerate(const ::std::optional<FQN>&, const Callback &cb) const override;
  };
}
#endif
