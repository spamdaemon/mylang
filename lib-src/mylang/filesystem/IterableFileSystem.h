#ifndef CLASS_MYLANG_FILESYSTEM_ITERABLEFILESYSTEM_H
#define CLASS_MYLANG_FILESYSTEM_ITERABLEFILESYSTEM_H

#ifndef CLASS_MYLANG_FILESYSTEM_FILESYSTEM_H
#include <mylang/filesystem/FileSystem.h>
#endif

#include <functional>
#include <optional>
#include <set>

namespace mylang::filesystem {

  /**
   * IterableFileSystem can be used enumerate FQNs in the filesystem.
   *
   */
  class IterableFileSystem: public FileSystem
  {
    /** A pointer to a file system */
  public:
    typedef ::std::shared_ptr<IterableFileSystem> Ptr;

    /**
     * The enumeration callback
     * When the callback returns false, no further fqns will be enumerated and false
     * is returned from the enumeration call.
     *
     * TODO: how to deal with ambiguous FQNs?
     *
     * @param fqn the FQN of the object
     * @param details details about the FQN
     */
  public:
    typedef ::std::function<bool(const FQN &fqn, const Details&)> Callback;

  protected:
    IterableFileSystem();

  public:
    ~IterableFileSystem() = 0;

    /**
     * Set the default filesystem constructor
     * @param ctor a constructor function
     */
  public:
    static void setDefaultFilesystem(::std::function<Ptr(::std::filesystem::path)> ctor);

    /**
     * Create a simple filesystem.
     *
     * This filesystem is bound to be slow as no
     * efforts are made to be efficient.
     *
     * @param path the root path
     * @return a filesystem
     */
  public:
    static Ptr create(::std::filesystem::path base);

    /**
     * Create an empty file system.
     */
  public:
    static Ptr empty();

    /**
     * Union of file systems.
     *
     * Aggregate multiple different filesystem into a single
     * unified filesystem. If an FQN is associated with more
     * than two or more different identifiers, then the that
     * FQN cannot be opened.
     *
     * The following is would be a valid union, because the
     * the any FQN will be associated with the same file:
     * <code>
     * unionOf({create("foo"), create("foo")});
     * </code>
     *
     * @param fs a list of file systems
     * @return an a filesystem
     */
  public:
    static Ptr unionOf(const ::std::set<Ptr> &fs);

    /**
     * Enumerate all known FQNs in the filesystem.
     * Enumeration stops when the first FQN returns false or all FQNs
     * have been enumerated.
     * @param cb a callback that is invoked with each known FQN
     * @param prefix an optional prefix that all FQNs must match
     * @return true if all callbacks returned true, false otherwise
     * @throws AmbiguousFQN if any fqn is ambiguous
     */
  public:
    virtual bool enumerate(const ::std::optional<FQN> &prefix, const Callback &cb) const = 0;

    /**
     * Enumerate all known FQNs in the filesystem subject to a set of constraint FQNs.
     * Enumeration stops when the first FQN returns false or all FQNs
     * have been enumerated.
     * @param cb a callback that is invoked with each known FQN
     * @param prefixes a set of prefixes to restrict the FQN that are reported
     * @return true if all callbacks returned true, false otherwise
     * @throws AmbiguousFQN if any fqn is ambiguous
     */
  public:
    virtual bool enumerate(const ::std::set<FQN> &prefixes, const Callback &cb) const;

    /**
     * Enumerate all known FQNs in the filesystem.
     * Enumeration stops when the first FQN returns false or all FQNs
     * have been enumerated.
     * @param cb a callback that is invoked with each known FQN
     * @param prefix an optional prefix that all FQNs must match
     * @return true if all callbacks returned true, false otherwise
     * @throws AmbiguousFQN if any fqn is ambiguous
     */
  public:
    bool enumerate(const Callback &cb) const;
  };

}

#endif
