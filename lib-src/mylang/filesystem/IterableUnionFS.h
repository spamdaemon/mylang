#ifndef CLASS_MYLANG_FILESYSTEM_ITERABLEUNIONFS_H
#define CLASS_MYLANG_FILESYSTEM_ITERABLEUNIONFS_H

#ifndef CLASS_MYLANG_FILESYSTEM_ITERABLEFILESYSTEM_H
#include <mylang/filesystem/IterableFileSystem.h>
#endif

namespace mylang::filesystem {
  /**
   * A union filesystem.
   */
  class IterableUnionFS: public IterableFileSystem
  {
  public:
    IterableUnionFS(::std::set<Ptr> list, bool lenient = false);

  public:
    ~IterableUnionFS();

  public:
    static ::std::shared_ptr<IterableUnionFS> create(::std::set<IterableFileSystem::Ptr> list);

    ::std::optional<Details> getDetails(const FQN &fqn) const override;
    ::std::unique_ptr<::std::istream> open(const FQN &fqn, Details *out) const override;
    bool enumerate(const ::std::optional<FQN>&, const Callback &cb) const override;

    /**
     *  Make this a lenient filesystem.
     */
  public:
    ::std::shared_ptr<IterableUnionFS> makeLenient();

    /** Find the filesystem that uniquely maps the specified FQN. */
  private:
    FileSystem* find(const FQN &fqn, ::std::optional<Details> &out) const;

    /** The composed files-systems */
  public:
    const ::std::set<Ptr> fs;

    /** True if ambiguous FQNs are allowed */
  public:
    const bool lenient;
  };
}
#endif
