#include <mylang/filesystem/HostFS.h>
#include <fstream>

namespace mylang::filesystem {
  namespace {

    static ::std::optional<HostFS::Details> detailsOf(const FileSystem::Identifier &f)
    {
      ::std::error_code ec;
      auto cf = ::std::filesystem::canonical(f, ec);
      if (ec) {
        // could not canonicalize
        return ::std::nullopt;
      }
      if (!::std::filesystem::is_regular_file(cf)) {
        // not a regular file
        return ::std::nullopt;
      }
      auto tm = ::std::filesystem::last_write_time(cf, ec);
      if (ec) {
        // could not get the write time
        return ::std::nullopt;
      }
      return HostFS::Details { cf.string(), ::std::move(tm) };
    }

    static ::std::optional<HostFS::Details> detailsOf(const ::std::filesystem::path &base,
        const HostFS::FQN &fqn)
    {
      auto f = base / (fqn.fullName("/") + ".ka");
      return detailsOf(f);
    }

    static ::std::optional<FileSystem::FQN> fromIdentifier(const ::std::filesystem::path &base,
        const FileSystem::Identifier &id)
    {
      auto pos = base.string().size() + 1;
      if (id.size() < pos) {
        return ::std::nullopt;
      }
      ::std::filesystem::path p(id.substr(pos, ::std::string::npos));
      if (p.extension() == ".ka") {
        auto tmp = p.replace_extension("").string();
        return FileSystem::FQN::parseFQN(p.replace_extension("").string(), '/');
      }
      return ::std::nullopt;
    }

    // turn an fqn into either a directory (first) or a file identifier (second)
    static ::std::pair<::std::optional<FileSystem::Identifier>,
        ::std::optional<FileSystem::Identifier>> toIdentifier(const ::std::filesystem::path &base,
        const FileSystem::FQN &fqn)
    {
      ::std::filesystem::path dirPath = base / fqn.fullName("/");
      ::std::filesystem::path filePath = dirPath;
      filePath.replace_extension(".ka");

      ::std::optional<FileSystem::Identifier> dir, file;
      if (::std::filesystem::is_directory(dirPath)) {
        dir = dirPath;
      }
      if (::std::filesystem::is_regular_file(filePath)) {
        file = filePath;
      }
      return {dir,file};
    }

    static bool enumerateFS(const ::std::filesystem::path &base,
        const ::std::filesystem::path &reldir, const IterableFileSystem::Callback &cb)
    {
      for (auto const &e : ::std::filesystem::directory_iterator(base / reldir)) {
        const auto &p = e.path().filename();
        if (e.is_regular_file()) {
          const FileSystem::Identifier id = e.path().string();
          const auto fqn = fromIdentifier(base, id);
          if (fqn) {
            // get the canonical path and update time for the file
            auto details = detailsOf(base, fqn.value());
            if (details) {
              if (false == cb(fqn.value(), *details)) {
                return false;
              }
            }
          }
        } else if (e.is_directory()) {
          // we can speed things up a little bit by checking if the path has invalid fqn characters
          if (FileSystem::FQN::isSegment(p)) {
            if (enumerateFS(base, reldir / p, cb) == false) {
              return false;
            }
          }
        }
      }
      return true;
    }
  }
  HostFS::HostFS(::std::filesystem::path xbase)
      : base(::std::filesystem::canonical(xbase))
  {
  }

  HostFS::~HostFS()
  {
  }

  ::std::shared_ptr<HostFS> HostFS::create(::std::filesystem::path xbase)
  {
    ::std::error_code ec;
    auto absPath = ::std::filesystem::canonical(xbase, ec);
    if (ec) {
      return nullptr;
    }

    return ::std::make_shared<HostFS>(::std::move(absPath));
  }

  ::std::optional<HostFS::Details> HostFS::getDetails(const FQN &fqn) const
  {
    return detailsOf(base, fqn);
  }

  ::std::unique_ptr<::std::istream> HostFS::open(const FQN &fqn, Details *out) const
  {
    auto p = getDetails(fqn);
    if (!p) {
      return nullptr;
    }
    auto res = ::std::make_unique<::std::ifstream>(p->identifier);
    if (res->is_open()) {
      if (out) {
        *out = p.value();
      }
    } else {
      res.release();
    }
    return res;
  }

  bool HostFS::enumerate(const ::std::optional<FQN> &prefix, const Callback &cb) const
  {
    bool ret = true;
    if (prefix) {
      auto paths = toIdentifier(base, *prefix);
      // if the prefix points to a file, then report the file
      if (paths.second) {
        auto details = detailsOf(*paths.second);
        if (details) {
          ret = ret && cb(*prefix, *details);
        }
      }
      if (paths.first) {
        ret = ret && enumerateFS(base, *paths.first, cb);
      }
      return ret;
    } else {
      return ret && enumerateFS(base, "", cb);
    }
  }

}
