#ifndef CLASS_MYLANG_FILESYSTEM_UNIONFS_H
#define CLASS_MYLANG_FILESYSTEM_UNIONFS_H

#ifndef CLASS_MYLANG_FILESYSTEM_FILESYSTEM_H
#include <mylang/filesystem/FileSystem.h>
#endif

namespace mylang::filesystem {
  /**
   * A union filesystem.
   */
  class UnionFS: public FileSystem
  {
  public:
    UnionFS(::std::set<Ptr> list, bool lenient = false);

  public:
    ~UnionFS();

  public:
    static ::std::shared_ptr<UnionFS> create(::std::set<Ptr> list);

    template<class T, class U>
    static ::std::shared_ptr<UnionFS> create(T begin, U end)
    {
      ::std::set<Ptr> tmp;
      tmp.insert(begin, end);
      return create(tmp);
    }

    ::std::optional<Details> getDetails(const FQN &fqn) const override;
    ::std::unique_ptr<::std::istream> open(const FQN &fqn, Details *out) const override;

    /**
     *  Make this a lenient filesystem.
     */
  public:
    ::std::shared_ptr<UnionFS> makeLenient();

    /** Find the filesystem that uniquely maps the specified FQN. */
  private:
    FileSystem* find(const FQN &fqn, ::std::optional<Details> &out) const;

    /** The composed files-systems */
  public:
    const ::std::set<Ptr> fs;

    /** True if ambiguous FQNs are allowed */
  public:
    const bool lenient;
  };
}
#endif
