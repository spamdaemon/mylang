#ifndef FILE_MYLANG_DEFS_H
#define FILE_MYLANG_DEFS_H
#include <memory>

#ifndef _IDIOMA_AST_H
#include <idioma/ast.h>
#endif

#ifndef CLASS_MYLANG_BIGINT_H
#include <mylang/BigInt.h>
#endif

#ifndef CLASS_MYLANG_BIGUINT_H
#include <mylang/BigUInt.h>
#endif

#ifndef CLASS_MYLANG_BIGREAL_H
#include <mylang/BigReal.h>
#endif

namespace mylang {

  namespace constraints {
    class ConstrainedType;
  }
  namespace expressions {
    class Expression;

    class Constant;
  }
  namespace names {
    class Name;
  }
  namespace types {
    class Type;
  }
  namespace patterns {
    class Pattern;
  }
  namespace variables {
    class Variable;
  }

  typedef ::std::shared_ptr<const mylang::expressions::Expression> ExpressionPtr;
  typedef ::std::shared_ptr<const mylang::expressions::Constant> ConstantPtr;
  typedef ::std::shared_ptr<const mylang::names::Name> NamePtr;
  typedef ::std::shared_ptr<const mylang::types::Type> TypePtr;
  typedef ::std::shared_ptr<const mylang::constraints::ConstrainedType> ConstrainedTypePtr;
  typedef ::std::shared_ptr<const mylang::variables::Variable> VariablePtr;
  typedef ::std::shared_ptr<const mylang::patterns::Pattern> PatternPtr;

  using GroupNode = idioma::ast::GroupNode;
  using TokenNode = idioma::ast::TokenNode;
  using GroupNodeCPtr = ::std::shared_ptr<const GroupNode>;
  using GroupNodePtr = ::std::shared_ptr<GroupNode>;
  using TokenNodeCPtr = ::std::shared_ptr<const TokenNode>;
  using TokenNodePtr = ::std::shared_ptr<TokenNode>;
  using NodeCPtr = ::std::shared_ptr<const idioma::ast::Node>;
  using NodePtr = ::std::shared_ptr<idioma::ast::Node>;
  using GroupNodes = ::std::vector<GroupNodeCPtr>;
  using TokenNodes = ::std::vector<TokenNodeCPtr>;

}
#endif

