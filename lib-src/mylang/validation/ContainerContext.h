#ifndef CLASS_MYLANG_VALIDATION_CONTAINERCONTEXT_H
#define CLASS_MYLANG_VALIDATION_CONTAINERCONTEXT_H

#ifndef CLASS_MYLANG_VALIDATION_DEFINITION_H
#include <mylang/validation/Definition.h>
#endif

namespace mylang {
  namespace validation {

    /**
     * A terrible name.
     */
    class ContainerContext: public Definition
    {
    public:
      typedef ::std::shared_ptr<const ContainerContext> Container;

      /**
       * Constructor with a name.
       * @param n a name for this context
       */
    public:
      ContainerContext(const NamePtr &n);

      /** Default constructor */
      ContainerContext();

      /** destructor */
      ~ContainerContext();

    };

  }
}
#endif
