#include <mylang/validation/TransformResolver.h>
#include <mylang/validation/TransformChainBuilder.h>
#include <mylang/constraints/ConstrainedOptType.h>
#include <mylang/transforms/TransformBase.h>
#include <mylang/names/Name.h>
#include <mylang/annotations.h>
#include <cassert>
#include <optional>
#include <string>
#include <vector>

namespace mylang {
  namespace validation {

    namespace {
      using namespace mylang::transforms;

      struct Impl: public TransformResolver
      {
        Impl(mylang::transforms::TransformBase::Ptr db)
            : database(::std::move(db)), quality(TransformBase::QUALITY_BEST)
        {
        }
        ~Impl()
        {
        }

        void addFQN(const ::std::string &name)
        {
          auto fqn = mylang::names::FQN::parseFQN(name, '.');
          if (!fqn) {
            throw ::std::runtime_error("Not a valid FQN " + name);
          }
          required.insert(fqn.value());
        }

        void updateResolution(const Type &c)
        {
          addFQN(c.asNonOptional().getName());
        }

        void updateResolution(const Call &c)
        {
          updateResolution(c.output);
          for (const auto &i : c.input) {
            updateResolution(i);
          }
        }

        void updateResolution(const Transform::Ptr &c)
        {
          updateResolution(c->call);
          if (!c->isIdentity()) {
            addFQN(c->name);
            for (const auto &i : c->nested) {
              updateResolution(i);
            }
          }
        }

        void updateResolution(const Chain::Ptr &chain)
        {
          const auto &tx = chain->getTransform();
          updateResolution(tx);
          if (tx->isIdentity()) {
            return;
          }

          for (auto c : tx->nested) {
            auto n = chain->getNestedChain(c);
            assert(n && "nested chain should exist");
            chains.emplace(c, n);
            updateResolution(n);
          }

          for (auto i : tx->call.input) {
            auto arg = chain->getArgChain(*i.name);
            updateResolution(arg);
          }
        }

        static ::std::optional<Type> toType(const ConstrainedTypePtr &cty)
        {
          auto ty = cty;
          auto optTy = ty->self<mylang::constraints::ConstrainedOptType>();
          if (optTy) {
            ty = optTy->element;
          }
          auto t = ty->self<mylang::constraints::ConstrainedNamedType>();
          if (t) {
            return Type(t->name()->fullName(), optTy != nullptr);
          } else {
            return ::std::nullopt;
          }
        }
        static ::std::optional<Call> toCall(const FunctionTypePtr &fn)
        {
          auto outTy = toType(fn->returnType);
          if (!outTy) {
            return ::std::nullopt;
          }
          ::std::set<Type> inTy;
          for (size_t i = 0; i < fn->parameters.size(); ++i) {
            auto paramTy = toType(fn->parameters.at(i));
            if (paramTy) {
              inTy.insert(paramTy.value());
            } else {
              return ::std::nullopt;
            }
          }

          return Call(outTy.value(), inTy);
        }

        bool resolve(const FunctionTypePtr &fnTy) override
        {
          auto maybeCall = toCall(fnTy);
          if (!maybeCall) {
            return false;
          }
          const auto &call = maybeCall.value();

          Chain::Ptr chain;
          auto c = chains.find(call);
          if (c == chains.end()) {
            chain = database->findChain(call, quality);
            if (chain) {
              updateResolution(chain);
            } else {
              return false; // could not create chain
            }
            chains.emplace(call, chain);
          }
          return true;
        }

        GroupNodeCPtr createFunction(const FunctionTypePtr &fnTy, NameResolver nameResolver,
            TypeResolver typeResolver) override
        {
          auto maybeCall = toCall(fnTy);
          if (!maybeCall) {
            return nullptr;
          }
          const auto &call = maybeCall.value();
          auto c = chains.find(call);
          if (c == chains.end()) {
            return nullptr;
          }
          return TransformChainBuilder::createFunction(c->second, fnTy, nameResolver, typeResolver);
        }

        ::std::set<mylang::names::FQN> getRequiredImports() override
        {
          return required;
        }

        const TransformBase::Ptr database;
        const TransformBase::Quality quality;
        ::std::map<Call, Chain::Ptr> chains;
        ::std::set<mylang::names::FQN> required;

      };
    }

    TransformResolver::TransformResolver()
    {
    }

    TransformResolver::~TransformResolver()
    {
    }

    TransformResolver::Ptr TransformResolver::create(mylang::transforms::TransformBase::Ptr db)
    {
      return ::std::make_shared<Impl>(::std::move(db));
    }
  }
}
