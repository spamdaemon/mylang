#ifndef CLASS_MYLANG_VALIDATION_TRANSFORMCHAINBUILDER_H
#define CLASS_MYLANG_VALIDATION_TRANSFORMCHAINBUILDER_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDNAMEDTYPE_H
#include <mylang/constraints/ConstrainedNamedType.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDFUNCTIONTYPE_H
#include <mylang/constraints/ConstrainedFunctionType.h>
#endif

#ifndef CLASS_MYLANG_TRANSFORMS_CHAIN_H
#include <mylang/transforms/Chain.h>
#endif

#include <memory>
#include <vector>
#include <string>
#include <optional>

namespace mylang {
  namespace validation {

    /**
     * Convert a transform chain to VAST.
     * @param chain the chain to convert
     */
    struct TransformChainBuilder
    {
      /** The type pointer */
    public:
      typedef ::std::shared_ptr<const mylang::constraints::ConstrainedNamedType> NamedTypePtr;

      /** A type resolver*/
    public:
      typedef ::std::function<NamedTypePtr(NamePtr)> TypeResolver;

      /** A type resolver*/
    public:
      typedef ::std::function<NamePtr(const ::std::string&)> NameResolver;

      /** The type pointer */
    public:
      typedef ::std::shared_ptr<const mylang::constraints::ConstrainedFunctionType> FunctionTypePtr;

      /**
       * Create function with the specified signature.
       * @param fn a function signature
       * @param typeResolver a function that can map type names to type objects
       * @return a function expression that has the specified type
       *
       */
    public:
      static GroupNodeCPtr createFunction(const mylang::transforms::Chain::Ptr &chain,
          const FunctionTypePtr &fn, NameResolver nameResolver, TypeResolver typeResolver);
    };

  }
}
#endif
