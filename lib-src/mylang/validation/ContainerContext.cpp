#include <mylang/validation/ContainerContext.h>

namespace mylang {
  namespace validation {

    ContainerContext::ContainerContext(const NamePtr &n)
        : Definition(n)
    {
    }

    ContainerContext::ContainerContext()
    {
    }

    ContainerContext::~ContainerContext()
    {
    }

  }
}
