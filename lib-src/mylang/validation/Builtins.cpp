#include <idioma/ast/Node.h>
#include <mylang/annotations.h>
#include <mylang/constraints/ConstrainedArrayType.h>
#include <mylang/constraints/ConstrainedBitType.h>
#include <mylang/constraints/ConstrainedBooleanType.h>
#include <mylang/constraints/ConstrainedByteType.h>
#include <mylang/constraints/ConstrainedCharType.h>
#include <mylang/constraints/ConstrainedInputType.h>
#include <mylang/constraints/ConstrainedIntegerType.h>
#include <mylang/constraints/ConstrainedMutableType.h>
#include <mylang/constraints/ConstrainedOptType.h>
#include <mylang/constraints/ConstrainedOutputType.h>
#include <mylang/constraints/ConstrainedRealType.h>
#include <mylang/constraints/ConstrainedStringType.h>
#include <mylang/constraints/ConstrainedStructType.h>
#include <mylang/constraints/ConstrainedTupleType.h>
#include <mylang/constraints/ConstrainedType.h>
#include <mylang/constraints/ConstrainedUnionType.h>
#include <mylang/SourceLocation.h>
#include <mylang/types/FunctionType.h>
#include <mylang/validation/Builtins.h>
#include <mylang/validation/DefinitionGroup.h>
#include <mylang/validation/FunctionLikeType.h>
#include <mylang/vast/ValidationError.h>
#include <mylang/vast/VAst.h>
#include <optional>
#include <utility>
#include <vector>

namespace mylang {
  namespace validation {

    using mylang::vast::ValidationError;
    using mylang::vast::VAst;

    namespace {
      using namespace mylang::constraints;

      template<class T = mylang::constraints::ConstrainedType>
      static ::std::shared_ptr<const T> isConstraint(const ConstrainedTypePtr &xty)
      {
        ConstrainedTypePtr ty = xty;
        if (!ty) {
          throw ::std::runtime_error("missing constraint annotation on expression");
        }
        auto res = ty->self<T>();
        while (!res) {
          auto bt = ty->basetype();
          if (!bt) {
            break;
          }
          ty = bt;
          res = ty->self<T>();
        }
        return res;
      }

      static ConstrainedTypePtr constraintOf(const GroupNodeCPtr &g)
      {
        auto ty = getConstraintAnnotation(g);
        if (!ty) {
          throw ::std::runtime_error("missing constraint annotation on expresssion");
        }
        return ty;
      }

      /**
       * A definition that wraps a expression which is return when create_reference is called.
       */
      ::std::shared_ptr<const Definition> create_reference_expression(const GroupNodeCPtr &expr)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &expr)
              : the_expression(expr)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_reference(const ScopePtr&, const TokenNodeCPtr&) const
          override final
          {
            return the_expression;
          }

          const GroupNodeCPtr the_expression;
        };
        return ::std::make_shared<Def>(expr);
      }

      ::std::shared_ptr<const Definition> create_definition_expression(
          const ::std::shared_ptr<const Definition> &def)
      {
        auto internal = DefinitionGroup::create(def);
        auto type = FunctionLikeType::get([=](const ::std::vector<TypePtr> &argv) {
          return def->get_function_type(argv);
        });
        setTypeAnnotation(internal, type);
        return create_reference_expression(internal);
      }

      ::std::shared_ptr<const Definition> create_type_definition(ConstrainedTypePtr type)
      {
        struct Def: public Definition
        {
          Def(ConstrainedTypePtr ty)
              : type(ty)
          {
          }

          ~Def()
          {
          }

          ConstrainedTypePtr get_type() const override
          {
            return type;
          }

        private:
          const ConstrainedTypePtr type;
        };
        return ::std::make_shared<Def>(type);
      }

      GroupNodeCPtr create_clone_mutable_expression(const GroupNodeCPtr &expr)
      {
        VAst ast;
        auto ty = ast.createType(constraintOf(expr), expr->span());
        return ast.createNewMutableExpression(ty, { ast.createGetMutableValueExpression(expr) });
      }

      ::std::shared_ptr<const Definition> create_wait_port_definition(const GroupNodeCPtr &expr)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &xexpr)
              : expr(xexpr)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (!params.empty()) {
              throw ValidationError(src,
                  "Expected no parameters, but found " + ::std::to_string(params.size()));
            }

            return ast.createWaitPortExpression(expr);
          }

          const GroupNodeCPtr expr;
        };
        return ::std::make_shared<Def>(expr);
      }

      ::std::shared_ptr<const Definition> create_block_events_definition(const GroupNodeCPtr &port)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &xport)
              : port(xport)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected one parameters, but found " + ::std::to_string(params.size()));
            }
            if (constraintOf(port)->self<ConstrainedInputType>()) {
              return ast.createBlockReadEventsExpression(port, params.at(0));
            } else {
              return ast.createBlockWriteEventsExpression(port, params.at(0));
            }
          }

          const GroupNodeCPtr port;
        };
        return ::std::make_shared<Def>(port);
      }

      ::std::shared_ptr<const Definition> create_close_port_definition(const GroupNodeCPtr &port)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &xport)
              : port(xport)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 0) {
              throw ValidationError(src,
                  "Expected one parameter, but found " + ::std::to_string(params.size()));
            }
            return ast.createClosePortExpression(port);
          }

          const GroupNodeCPtr port;
        };
        return ::std::make_shared<Def>(port);
      }

      ::std::shared_ptr<const Definition> create_write_port_definition(const GroupNodeCPtr &expr)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &xexpr)
              : expr(xexpr)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected one parameters, but found " + ::std::to_string(params.size()));
            }

            return ast.createWritePortExpression(expr, params.at(0));
          }

          const GroupNodeCPtr expr;
        };
        return ::std::make_shared<Def>(expr);
      }

      ::std::shared_ptr<const Definition> create_read_port_definition(const GroupNodeCPtr &expr)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &xexpr)
              : expr(xexpr)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 0) {
              throw ValidationError(src,
                  "Expected no parameters, but found " + ::std::to_string(params.size()));
            }

            return ast.createReadPortExpression(expr);
          }

          const GroupNodeCPtr expr;
        };
        return ::std::make_shared<Def>(expr);
      }

      ::std::shared_ptr<const Definition> create_clear_port_definition(const GroupNodeCPtr &expr)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &xexpr)
              : expr(xexpr)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 0) {
              throw ValidationError(src,
                  "Expected no parameters, but found " + ::std::to_string(params.size()));
            }

            return ast.createClearPortExpression(expr);
          }

          const GroupNodeCPtr expr;
        };
        return ::std::make_shared<Def>(expr);
      }

      ::std::shared_ptr<const Definition> create_bits_to_bit_definition()
      {
        struct Def: public Definition
        {
          Def()
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected no parameters, but found " + ::std::to_string(params.size()));
            }

            return ast.createGetBitFromBitsExpression(params.at(0));
          }
        };
        return ::std::make_shared<Def>();
      }

      ::std::shared_ptr<const Definition> create_bits_to_boolean_definition()
      {
        struct Def: public Definition
        {
          Def()
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected no parameters, but found " + ::std::to_string(params.size()));
            }

            return ast.createGetBooleanFromBitsExpression(params.at(0));
          }
        };
        return ::std::make_shared<Def>();
      }

      ::std::shared_ptr<const Definition> create_bits_to_byte_definition()
      {
        struct Def: public Definition
        {
          Def()
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected no parameters, but found " + ::std::to_string(params.size()));
            }

            return ast.createGetByteFromBitsExpression(params.at(0));
          }
        };
        return ::std::make_shared<Def>();
      }

      ::std::shared_ptr<const Definition> create_bits_to_char_definition()
      {
        struct Def: public Definition
        {
          Def()
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected no parameters, but found " + ::std::to_string(params.size()));
            }

            return ast.createGetCharFromBitsExpression(params.at(0));
          }
        };
        return ::std::make_shared<Def>();
      }

      ::std::shared_ptr<const Definition> create_bits_to_integer_definition(
          ::std::shared_ptr<const ConstrainedIntegerType> ty)
      {
        struct Def: public Definition
        {
          Def(::std::shared_ptr<const ConstrainedIntegerType> ty)
              : type(ty)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected no parameters, but found " + ::std::to_string(params.size()));
            }
            return ast.createGetIntegerFromBitsExpression(
                ast.createType(type, params.at(0)->span()), params.at(0));
          }

          ::std::shared_ptr<const ConstrainedIntegerType> type;
        };
        return ::std::make_shared<Def>(ty);
      }

      ::std::shared_ptr<const Definition> create_bits_to_real_definition(
          ::std::shared_ptr<const ConstrainedRealType> ty)
      {
        struct Def: public Definition
        {
          Def(::std::shared_ptr<const ConstrainedRealType> ty)
              : type(ty)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected no parameters, but found " + ::std::to_string(params.size()));
            }
            return ast.createGetRealFromBitsExpression(ast.createType(type, params.at(0)->span()),
                params.at(0));
          }

          ::std::shared_ptr<const ConstrainedRealType> type;
        };
        return ::std::make_shared<Def>(ty);
      }

      ::std::shared_ptr<const Definition> create_bytes_to_bit_definition()
      {
        struct Def: public Definition
        {
          Def()
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected no parameters, but found " + ::std::to_string(params.size()));
            }

            return ast.createDecodeBitExpression(params.at(0));
          }
        };
        return ::std::make_shared<Def>();
      }

      ::std::shared_ptr<const Definition> create_bytes_to_boolean_definition()
      {
        struct Def: public Definition
        {
          Def()
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected no parameters, but found " + ::std::to_string(params.size()));
            }

            return ast.createDecodeBooleanExpression(params.at(0));
          }
        };
        return ::std::make_shared<Def>();
      }

      ::std::shared_ptr<const Definition> create_bytes_to_byte_definition()
      {
        struct Def: public Definition
        {
          Def()
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected no parameters, but found " + ::std::to_string(params.size()));
            }

            return ast.createDecodeByteExpression(params.at(0));
          }
        };
        return ::std::make_shared<Def>();
      }

      ::std::shared_ptr<const Definition> create_bytes_to_char_definition()
      {
        struct Def: public Definition
        {
          Def()
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected no parameters, but found " + ::std::to_string(params.size()));
            }

            return ast.createDecodeCharExpression(params.at(0));
          }
        };
        return ::std::make_shared<Def>();
      }

      ::std::shared_ptr<const Definition> create_bytes_to_integer_definition()
      {
        struct Def: public Definition
        {
          Def()
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected no parameters, but found " + ::std::to_string(params.size()));
            }

            return ast.createDecodeIntegerExpression(params.at(0));
          }
        };
        return ::std::make_shared<Def>();
      }

      ::std::shared_ptr<const Definition> create_bytes_to_real_definition()
      {
        struct Def: public Definition
        {
          Def()
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected no parameters, but found " + ::std::to_string(params.size()));
            }

            return ast.createDecodeRealExpression(params.at(0));
          }
        };
        return ::std::make_shared<Def>();
      }

      ::std::shared_ptr<const Definition> create_bytes_to_string_definition()
      {
        struct Def: public Definition
        {
          Def()
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected no parameters, but found " + ::std::to_string(params.size()));
            }

            return ast.createDecodeStringExpression(params.at(0));
          }
        };
        return ::std::make_shared<Def>();
      }

      ::std::shared_ptr<const Definition> getTypeBuiltin(const ConstrainedTypePtr &type,
          const ::std::string &name, const ::idioma::ast::Node::Span &span)
      {
        VAst ast;

        if (name == "root") {
          return create_type_definition(type->roottype());
        }
        if (name == "base") {
          auto ty = type->basetype();
          if (!ty) {
            throw ValidationError(span, "type " + type->toString() + " does not have a basetype");
          }
          return create_type_definition(type->basetype());
        }
        if (type->self<ConstrainedArrayType>()) {
          if (name == "element") {
            return create_type_definition(type->self<ConstrainedArrayType>()->element);
          }
          if (name == "counter") {
            return create_type_definition(type->self<ConstrainedArrayType>()->counter);
          }
          if (name == "index") {
            auto ty = type->self<ConstrainedArrayType>()->index;
            if (!ty) {
              throw ValidationError(span, "array " + type->toString() + " cannot be indexed");
            }
            return create_type_definition(type->self<ConstrainedArrayType>()->index);
          }
        }
        if (type->self<ConstrainedOptType>()) {
          if (name == "element") {
            return create_type_definition(type->self<ConstrainedOptType>()->element);
          }
        }
        if (type->self<ConstrainedMutableType>()) {
          if (name == "element") {
            return create_type_definition(type->self<ConstrainedMutableType>()->element);
          }
        }
        if (type->self<ConstrainedInputType>()) {
          if (name == "element") {
            return create_type_definition(type->self<ConstrainedInputType>()->element);
          }
        }
        if (type->self<ConstrainedOutputType>()) {
          if (name == "element") {
            return create_type_definition(type->self<ConstrainedOutputType>()->element);
          }
        }
        if (type->self<ConstrainedStructType>()) {
          if (name == "tuple") {
            return create_type_definition(type->self<ConstrainedStructType>()->getPeerTuple());
          }
        }
        if (type->self<ConstrainedUnionType>()) {
          if (name == "discriminant") {
            return create_type_definition(
                type->self<ConstrainedUnionType>()->discriminant.constraint);
          }
        }

        if (type->self<ConstrainedTupleType>()) {
          auto ty = type->self<ConstrainedTupleType>();
          if (ty->types.empty()) {
            throw ValidationError(span, "type " + type->toString() + " has no members");
          }
          if (name == "head") {
            return create_type_definition(ty->getHead());
          }
          if (name == "tail") {
            return create_type_definition(ty->getTail());
          }
        }

        if (type->self<ConstrainedBitType>()) {
          if (name == "from_bytes") {
            return create_definition_expression(create_bytes_to_bit_definition());
          }
          if (name == "from_bits") {
            return create_definition_expression(create_bits_to_bit_definition());
          }
        }
        if (type->self<ConstrainedBooleanType>()) {
          if (name == "from_bytes") {
            return create_definition_expression(create_bytes_to_boolean_definition());
          }
          if (name == "from_bits") {
            return create_definition_expression(create_bits_to_boolean_definition());
          }
        }
        if (type->self<ConstrainedByteType>()) {
          if (name == "from_bits") {
            return create_definition_expression(create_bits_to_byte_definition());
          }
          if (name == "from_bytes") {
            return create_definition_expression(create_bytes_to_byte_definition());
          }
        }
        if (type->self<ConstrainedCharType>()) {
          if (name == "from_bytes") {
            return create_definition_expression(create_bytes_to_char_definition());
          }
          if (name == "from_bits") {
            return create_definition_expression(create_bits_to_char_definition());
          }
        }
        if (type->self<ConstrainedIntegerType>()) {
          if (name == "from_bytes") {
            return create_definition_expression(create_bytes_to_integer_definition());
          }
          if (name == "from_bits") {
            return create_definition_expression(
                create_bits_to_integer_definition(type->self<ConstrainedIntegerType>()));
          }
        }
        if (type->self<ConstrainedRealType>()) {
          if (name == "from_bytes") {
            return create_definition_expression(create_bytes_to_real_definition());
          }
          if (name == "from_bits") {
            return create_definition_expression(
                create_bits_to_real_definition(type->self<ConstrainedRealType>()));
          }
        }
        if (type->self<ConstrainedStringType>()) {
          if (name == "from_bytes") {
            return create_definition_expression(create_bytes_to_string_definition());
          }
        }

        if (type->self<ConstrainedIntegerType>()) {
          auto ty = type->self<ConstrainedIntegerType>();

          if (name == "min") {
            if (ty->range.min().isFinite()) {
              auto arg = ast.createLiteralInteger(ty->range.min(), span);
              auto ity = ast.createType(ConstrainedOptType::get(constraintOf(arg)), span);
              return create_reference_expression(ast.createNewOptionalExpression(ity, { arg }));
            } else {
              auto ity = ast.createType(
                  ConstrainedOptType::get(ConstrainedIntegerType::getInteger()), span);
              return create_reference_expression(ast.createNewOptionalExpression(ity, { }));
            }
          }
          if (name == "max") {
            if (ty->range.max().isFinite()) {
              auto arg = ast.createLiteralInteger(ty->range.max(), span);
              auto ity = ast.createType(ConstrainedOptType::get(constraintOf(arg)), span);
              return create_reference_expression(ast.createNewOptionalExpression(ity, { arg }));
            } else {
              auto ity = ast.createType(
                  ConstrainedOptType::get(ConstrainedIntegerType::getInteger()), span);
              return create_reference_expression(ast.createNewOptionalExpression(ity, { }));
            }
          }
        }
        if (type->self<ConstrainedArrayType>()) {
          auto ty = type->self<ConstrainedArrayType>();

          if (name == "min") {
            auto arg = ast.createLiteralInteger(*ty->bounds.min(), span);
            return create_reference_expression(arg);
          }
          if (name == "max") {
            if (ty->bounds.max().isFinite()) {
              auto arg = ast.createLiteralInteger(*ty->bounds.max(), span);
              auto ity = ast.createType(ConstrainedOptType::get(constraintOf(arg)), span);
              return create_reference_expression(ast.createNewOptionalExpression(ity, { arg }));
            } else {
              auto ity = ast.createType(
                  ConstrainedOptType::get(ConstrainedIntegerType::getInteger()), span);
              return create_reference_expression(ast.createNewOptionalExpression(ity, { }));
            }
          }
        }
        return nullptr;
      }

      ::std::optional<::std::shared_ptr<const Definition>> getInstanceBuiltin(
          const GroupNodeCPtr &expr, const ConstrainedTypePtr &ty, const ::std::string &name,
          const ::idioma::ast::Node::Span &span)
      {
        VAst ast;

        if (name == "asRootType") {
          if (expr == nullptr) {
            return nullptr;
          }
          return create_reference_expression(ast.createGetAsRootTypeExpression(expr));
        }
        if (name == "asBaseType") {
          if (expr == nullptr) {
            return nullptr;
          }
          return create_reference_expression(ast.createGetAsBaseTypeExpression(expr));
        }

        if (isConstraint<ConstrainedBitType>(ty)) {
          if (name == "bits") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createGetBitsFromBitExpression(expr));
          }
          if (name == "bytes") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createEncodeBitExpression(expr));
          }
        }

        if (isConstraint<ConstrainedBooleanType>(ty)) {
          if (name == "bits") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createGetBitsFromBooleanExpression(expr));
          }
          if (name == "bytes") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createEncodeBooleanExpression(expr));
          }
        }
        if (isConstraint<ConstrainedByteType>(ty)) {
          if (name == "bytes") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createEncodeByteExpression(expr));
          }
          if (name == "bits") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createGetBitsFromByteExpression(expr));
          }
          if (name == "unsignedInteger") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createGetByteAsUnsignedIntegerExpression(expr));
          }
          if (name == "signedInteger") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createGetByteAsSignedIntegerExpression(expr));
          }
        }
        if (isConstraint<ConstrainedCharType>(ty)) {
          if (name == "bits") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createGetBitsFromCharExpression(expr));
          }
          if (name == "bytes") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createEncodeCharExpression(expr));
          }
        }
        if (isConstraint<ConstrainedIntegerType>(ty)) {
          if (name == "bits") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createGetBitsFromIntegerExpression(expr));
          }
          if (name == "bytes") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createEncodeIntegerExpression(expr));
          }
        }
        if (isConstraint<ConstrainedRealType>(ty)) {
          if (name == "bits") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createGetBitsFromRealExpression(expr));
          }
          if (name == "bytes") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createEncodeRealExpression(expr));
          }
        }

        if (isConstraint<ConstrainedStringType>(ty)) {
          if (name == "bytes") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createEncodeStringExpression(expr));
          }
          if (name == "chars") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createStringToCharsExpression(expr));
          }
          if (name == "length") {
            return create_reference_expression(ast.createGetStringLengthExpression(expr));
          }
        }
        if (isConstraint<ConstrainedStructType>(ty)) {
          auto structTy = ty->self<ConstrainedStructType>();
          if (name == "tuple") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createConvertToTupleExpression(expr));
          }
        }

        if (isConstraint<ConstrainedTupleType>(ty)) {
          auto tupleTy = ty->self<ConstrainedTupleType>();

          if (name == "head") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createGetHeadExpression(expr));
          }
          if (name == "tail") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createGetTailExpression(expr));
          }
        }

        if (isConstraint<ConstrainedArrayType>(ty)) {
          if (name == "length") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createGetArrayLengthExpression(expr));
          }
        }

        if (isConstraint<ConstrainedOptType>(ty)) {
          if (name == "present") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createIsOptionalValuePresentExpression(expr));
          }
          if (name == "value" && constraintOf(expr)->isConcrete()) {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createGetOptionalValueExpression(expr));
          }
        }

        if (isConstraint<ConstrainedMutableType>(ty)) {
          if (name == "value") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createGetMutableValueExpression(expr));
          }
          if (name == "clone") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(create_clone_mutable_expression(expr));
          }
        }

        if (isConstraint<ConstrainedInputType>(ty)) {
          if (name == "closed") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createIsInputClosedExpression(expr));
          }
          if (name == "new") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createHasNewInputExpression(expr));
          }
          if (name == "readable") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createIsPortReadableExpression(expr));
          }
          if (name == "wait") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_wait_port_definition(expr));
          }
          if (name == "read") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_read_port_definition(expr));
          }
          if (name == "close") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_close_port_definition(expr));
          }
          if (name == "clear") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_clear_port_definition(expr));
          }
          if (name == "block") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_block_events_definition(expr));
          }
        }

        if (isConstraint<ConstrainedOutputType>(ty)) {
          if (name == "closed") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createIsOutputClosedExpression(expr));
          }
          if (name == "writable") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createIsPortWritableExpression(expr));
          }
          if (name == "wait") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_wait_port_definition(expr));
          }
          if (name == "write") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_write_port_definition(expr));
          }
          if (name == "close") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_close_port_definition(expr));
          }
          if (name == "block") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_block_events_definition(expr));
          }
        }

        return ::std::nullopt;
      }

    }

    Builtins::Builtins()
    {
    }

    Builtins::~Builtins()
    {
    }

    ::std::shared_ptr<Builtins> Builtins::get()
    {
      struct Impl: public Builtins
      {
        ~Impl()
        {
        }

        ::std::shared_ptr<const Definition> findTypeBuiltin(const ConstrainedTypePtr &type,
            const ::std::string &name, const ::idioma::ast::Node::Span &span) const override
        {
          return getTypeBuiltin(type, name, span);
        }

        ::std::shared_ptr<const Definition> findInstanceBuiltin(const GroupNodeCPtr &expr,
            const ::std::string &name, const ::idioma::ast::Node::Span &span) const override
        {
          auto res = getInstanceBuiltin(expr, getConstraintAnnotation(expr), name, span);
          return res.has_value() ? *res : nullptr;
        }

        bool hasInstanceBuiltin(const ConstrainedTypePtr &type, const ::std::string &name) const
        override
        {
          return getInstanceBuiltin(nullptr, type, name, ::idioma::ast::Node::Span()).has_value();
        }
      };
      return ::std::make_shared<Impl>();
    }
  }
}
