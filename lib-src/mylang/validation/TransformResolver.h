#ifndef CLASS_MYLANG_VALIDATION_TRANSFORMRESOLVER_H
#define CLASS_MYLANG_VALIDATION_TRANSFORMRESOLVER_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDNAMEDTYPE_H
#include <mylang/constraints/ConstrainedNamedType.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDFUNCTIONTYPE_H
#include <mylang/constraints/ConstrainedFunctionType.h>
#endif

#ifndef CLASS_MYLANG_TRANSFORMS_TRANSFORMBASE_H
#include <mylang/transforms/TransformBase.h>
#endif

#ifndef CLASS_MYLANG_NAMES_FQN_H
#include <mylang/names/FQN.h>
#endif

#ifndef CLASS_MYLANG_FILESYSTEM_FILESYSTEM_H
#include <mylang/filesystem/FileSystem.h>
#endif

#include <memory>
#include <vector>
#include <string>
#include <optional>

namespace mylang {
  namespace validation {

    struct TransformResolver
    {
      /** The type pointer */
    public:
      typedef ::std::shared_ptr<const mylang::constraints::ConstrainedNamedType> NamedTypePtr;

      /** A type resolver*/
    public:
      typedef ::std::function<NamedTypePtr(NamePtr)> TypeResolver;

      /** A type resolver*/
    public:
      typedef ::std::function<NamePtr(const ::std::string&)> NameResolver;

      /** The type pointer */
    public:
      typedef ::std::shared_ptr<const mylang::constraints::ConstrainedFunctionType> FunctionTypePtr;

    public:
      typedef ::std::shared_ptr<TransformResolver> Ptr;

    protected:
      TransformResolver();

    public:
      virtual ~TransformResolver();

      /**
       * Create a basic resolver.
       * @param db a transform database
       * @param fs a filesystem
       * @return a resolver
       */
    public:
      static Ptr create(mylang::transforms::TransformBase::Ptr db);

      /**
       * Find a transform that has the specified function signature.
       * @param fn a function signature
       * @return true if transform was created, false otherwise
       \       */
    public:
      virtual bool resolve(const FunctionTypePtr &fn) = 0;

      /**
       * Create function with the specified signature.
       * @param fn a function signature
       * @param typeResolver a function that can map type names to type objects
       * @return a function expression that has the specified type
       *
       */
    public:
      virtual GroupNodeCPtr createFunction(const FunctionTypePtr &fn, NameResolver nameResolver,
          TypeResolver typeResolver) = 0;

      /**
       * Get the required imports. This method must be called prior to creating calls,
       */
    public:
      virtual ::std::set<::mylang::names::FQN> getRequiredImports() = 0;
    };

  }
}
#endif
