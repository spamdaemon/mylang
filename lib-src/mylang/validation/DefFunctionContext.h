#ifndef CLASS_MYLANG_VALIDATION_DEFFUNCTIONCONTEXT_H
#define CLASS_MYLANG_VALIDATION_DEFFUNCTIONCONTEXT_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_VALIDATION_COUNTER_H
#include <mylang/validation/Counter.h>
#endif

#ifndef CLASS_MYLANG_VALIDATION_CONTAINERCONTEXT_H
#include <mylang/validation/ContainerContext.h>
#endif

#ifndef CLASS_MYLANG_VALIDATION_GENERICINSTANTIATIONREQUEST_H
#include <mylang/validation/GenericInstantiationRequest.h>
#endif

#ifndef FILE_MYLANG_CONSTRAINTS_H
#include <mylang/constraints.h>
#endif

#ifndef CLASS_MYLANG_VALIDATION_DEFGENERICFUNCTIONCONTEXT_H
#include <mylang/validation/DefGenericFunctionContext.h>
#endif
#include <mylang/SourceLocation.h>
#include <memory>
#include <vector>

namespace mylang {
  namespace validation {

    struct DefFunctionContext: public ContainerContext
    {

      struct Entry
      {
        Entry(const TokenNodeCPtr &xbasename, ConstrainedTypePtr xreturnType,
            ::std::vector<ConstrainedTypePtr> xparameters, bool xhaveBody);

        ::std::shared_ptr<const mylang::constraints::ConstrainedFunctionType> signature() const;

        void setReturnType(ConstrainedTypePtr xreturnType);

        bool areParametersCompatible(const ::std::vector<ConstrainedTypePtr> &params) const;

        bool hasExtension(const ConstrainedTypePtr &ty) const;

        /**
         * Determine the kind of match of the arguments with this entry.
         * @return -1 if no match possible, otherwise number of exact matches
         */
        int determineMatch(const ::std::vector<ConstrainedTypePtr> &args) const;

        const TokenNodeCPtr defName;
        const NamePtr id;
        const ::std::vector<ConstrainedTypePtr> parameters;
        const ConstrainedTypePtr canonical;
        bool haveBody;
      private:
        ConstrainedTypePtr returnType;
        ::std::shared_ptr<const mylang::constraints::ConstrainedFunctionType> functionSignature;
      };

      DefFunctionContext(const NamePtr &the_name, VariablePtr vinfo, bool xisExtension,
          bool xisExported, bool xisTransform);

      ~DefFunctionContext();

      ::std::shared_ptr<DefFunctionContext> specialize(
          const ::std::vector<ConstrainedTypePtr> &spec) const;

      ::std::shared_ptr<DefFunctionContext> generalize() const;

      GroupNodeCPtr create_reference(const ScopePtr&, const TokenNodeCPtr &src) const override;

      GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src, const GroupNodes &args,
          RegisterVariableFN, VisitFN) const override;

      NamePtr makeNewName();

      NamePtr addEntry(TokenNodeCPtr sourceName, const ::std::vector<ConstrainedTypePtr> &params,
          const ConstrainedTypePtr &retTy, bool haveBody);

    private:
      ::std::shared_ptr<const mylang::constraints::ConstrainedFunctionType> getSignature(
          const NamePtr &key) const;

    public:

      bool hasExtension(const ConstrainedTypePtr &ty) const;

      Entry& getEntry(const NamePtr &key);

      const Entry& getEntry() const;

      const Entry& getEntry(const NamePtr &key) const;

      const DefGenericFunctionContext& getGenerics() const;

      DefGenericFunctionContext& getGenerics();

    private:
      NamePtr findEntry(const ::std::vector<ConstrainedTypePtr> &args) const;

    public:
      const bool isExtension;
      const bool isExported;
      const bool isTransform;
      const VariablePtr vinfo;

    private:
      mutable ::std::vector<Entry> entries;
      ::std::shared_ptr<Counter> nameCounter;
      ::std::shared_ptr<DefGenericFunctionContext> generics;
    };

  }
}
#endif
