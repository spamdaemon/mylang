#include <idioma/ast/Node.h>
#include <mylang/annotations.h>
#include <mylang/constraints/ConstrainedArrayType.h>
#include <mylang/constraints/ConstrainedBitType.h>
#include <mylang/constraints/ConstrainedBooleanType.h>
#include <mylang/constraints/ConstrainedByteType.h>
#include <mylang/constraints/ConstrainedCharType.h>
#include <mylang/constraints/ConstrainedIntegerType.h>
#include <mylang/constraints/ConstrainedOptType.h>
#include <mylang/constraints/ConstrainedRealType.h>
#include <mylang/constraints/ConstrainedStringType.h>
#include <mylang/constraints/ConstrainedStructType.h>
#include <mylang/constraints/ConstrainedTupleType.h>
#include <mylang/constraints/ConstrainedType.h>
#include <mylang/constraints/ConstrainedUnionType.h>
#include <mylang/SourceLocation.h>
#include <mylang/types/FunctionType.h>
#include <mylang/validation/Library.h>
#include <mylang/validation/DefinitionGroup.h>
#include <mylang/validation/FunctionLikeType.h>
#include <mylang/vast/ValidationError.h>
#include <mylang/vast/VAst.h>
#include <optional>
#include <utility>
#include <vector>

namespace mylang {
  namespace validation {

    using mylang::vast::ValidationError;
    using mylang::vast::VAst;

    namespace {
      using namespace mylang::constraints;

      template<class T = mylang::constraints::ConstrainedType>
      static ::std::shared_ptr<const T> isConstraint(const ConstrainedTypePtr &xty)
      {
        ConstrainedTypePtr ty = xty;
        if (!ty) {
          throw ::std::runtime_error("missing constraint annotation on expression");
        }
        auto res = ty->self<T>();
        while (!res) {
          auto bt = ty->basetype();
          if (!bt) {
            break;
          }
          ty = bt;
          res = ty->self<T>();
        }
        return res;
      }

      /**
       * A definition that wraps a expression which is return when create_reference is called.
       */
      ::std::shared_ptr<const Definition> create_reference_expression(const GroupNodeCPtr &expr)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &expr)
              : the_expression(expr)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_reference(const ScopePtr&, const TokenNodeCPtr&) const
          override final
          {
            return the_expression;
          }

          const GroupNodeCPtr the_expression;
        };
        return ::std::make_shared<Def>(expr);
      }

      ::std::shared_ptr<const Definition> create_definition_expression(
          const ::std::shared_ptr<const Definition> &def)
      {
        auto internal = DefinitionGroup::create(def);
        auto type = FunctionLikeType::get([=](const ::std::vector<TypePtr> &argv) {
          return def->get_function_type(argv);
        });
        setTypeAnnotation(internal, type);
        return create_reference_expression(internal);
      }

      ::std::shared_ptr<const Definition> create_array_filter_definition(const GroupNodeCPtr &expr)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &xexpr)
              : expr(xexpr)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected a single parameter, but found " + ::std::to_string(params.size()));
            }
            return ast.createFilterArrayExpression(params.at(0), expr);
          }

          const GroupNodeCPtr expr;
        };
        return ::std::make_shared<Def>(expr);
      }

      ::std::shared_ptr<const Definition> create_array_find_definition(const GroupNodeCPtr &expr)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &xexpr)
              : expr(xexpr)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() == 1) {
              return ast.createFindArrayExpression(params.at(0), expr);
            }
            if (params.size() == 2) {
              return ast.createFindArrayExpression(params.at(0), expr, params.at(1));
            }
            throw ValidationError(src,
                "Expected a one or two parameters, but found " + ::std::to_string(params.size()));
          }

          const GroupNodeCPtr expr;
        };
        return ::std::make_shared<Def>(expr);
      }

      ::std::shared_ptr<const Definition> create_array_map_definition(const GroupNodeCPtr &expr)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &xexpr)
              : expr(xexpr)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected a single parameter, but found " + ::std::to_string(params.size()));
            }
            return ast.createMapArrayExpression(params.at(0), expr);
          }

          const GroupNodeCPtr expr;
        };
        return ::std::make_shared<Def>(expr);
      }

      ::std::shared_ptr<const Definition> create_array_fold_definition(const GroupNodeCPtr &expr)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &xexpr)
              : expr(xexpr)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 2) {
              throw ValidationError(src,
                  "Expected two arguments, but found " + ::std::to_string(params.size()));
            }
            return ast.createFoldArrayExpression(expr, params.at(1), params.at(0));
          }

          const GroupNodeCPtr expr;
        };
        return ::std::make_shared<Def>(expr);
      }

      ::std::shared_ptr<const Definition> create_array_partition_definition(
          const GroupNodeCPtr &expr)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &xexpr)
              : expr(xexpr)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected one argument, but found " + ::std::to_string(params.size()));
            }
            return ast.createPartitionArrayExpression(expr, params.at(0));
          }

          const GroupNodeCPtr expr;
        };
        return ::std::make_shared<Def>(expr);
      }

      ::std::shared_ptr<const Definition> create_array_pad_definition(const GroupNodeCPtr &expr)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &xexpr)
              : expr(xexpr)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 2) {
              throw ValidationError(src,
                  "Expected two arguments, but found " + ::std::to_string(params.size()));
            }
            return ast.createPadArrayExpression(expr, params.at(0), params.at(1));
          }

          const GroupNodeCPtr expr;
        };
        return ::std::make_shared<Def>(expr);
      }

      ::std::shared_ptr<const Definition> create_array_trim_definition(const GroupNodeCPtr &expr)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &xexpr)
              : expr(xexpr)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected one argument, but found " + ::std::to_string(params.size()));
            }
            return ast.createTrimArrayExpression(expr, params.at(0));
          }

          const GroupNodeCPtr expr;
        };
        return ::std::make_shared<Def>(expr);
      }

      ::std::shared_ptr<const Definition> create_array_drop_definition(const GroupNodeCPtr &expr)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &xexpr)
              : expr(xexpr)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected one argument, but found " + ::std::to_string(params.size()));
            }
            return ast.createDropArrayExpression(expr, params.at(0));
          }

          const GroupNodeCPtr expr;
        };
        return ::std::make_shared<Def>(expr);
      }

      ::std::shared_ptr<const Definition> create_array_take_definition(const GroupNodeCPtr &expr)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &xexpr)
              : expr(xexpr)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected one argument, but found " + ::std::to_string(params.size()));
            }
            return ast.createTakeArrayExpression(expr, params.at(0));
          }

          const GroupNodeCPtr expr;
        };
        return ::std::make_shared<Def>(expr);
      }

      ::std::shared_ptr<const Definition> create_array_flatten_definition(const GroupNodeCPtr &expr)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &xexpr)
              : expr(xexpr)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 0) {
              throw ValidationError(src,
                  "Expected no arguments, but found " + ::std::to_string(params.size()));
            }
            return ast.createFlattenArrayExpression(expr);
          }

          const GroupNodeCPtr expr;
        };
        return ::std::make_shared<Def>(expr);
      }

      ::std::shared_ptr<const Definition> create_array_reverse_definition(const GroupNodeCPtr &expr)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &xexpr)
              : expr(xexpr)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 0) {
              throw ValidationError(src,
                  "Expected no arguments, but found " + ::std::to_string(params.size()));
            }
            return ast.createReverseArrayExpression(expr);
          }

          const GroupNodeCPtr expr;
        };
        return ::std::make_shared<Def>(expr);
      }

      ::std::shared_ptr<const Definition> create_optional_map_definition(const GroupNodeCPtr &expr)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &xexpr)
              : expr(xexpr)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 1) {
              throw ValidationError(src,
                  "Expected a single parameter, but found " + ::std::to_string(params.size()));
            }
            return ast.createMapOptionalExpression(params.at(0), expr);
          }

          const GroupNodeCPtr expr;
        };
        return ::std::make_shared<Def>(expr);
      }

      ::std::shared_ptr<const Definition> create_optional_flatten_definition(
          const GroupNodeCPtr &expr)
      {
        struct Def: public Definition
        {
          Def(const GroupNodeCPtr &xexpr)
              : expr(xexpr)
          {
          }

          ~Def()
          {
          }

          GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src,
              const GroupNodes &params, RegisterVariableFN, VisitFN) const
              override
          {
            VAst ast;
            if (params.size() != 0) {
              throw ValidationError(src,
                  "Expected no parameters, but found " + ::std::to_string(params.size()));
            }
            return ast.createFlattenOptionalExpression(expr);
          }

          const GroupNodeCPtr expr;
        };
        return ::std::make_shared<Def>(expr);
      }

      ::std::optional<::std::shared_ptr<const Definition>> getInstanceBuiltin(
          const GroupNodeCPtr &expr, const ConstrainedTypePtr &ty, const ::std::string &name,
          const ::idioma::ast::Node::Span &span)
      {
        VAst ast;

        if (isConstraint<ConstrainedArrayType>(ty)) {
          if (name == "head") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createGetHeadExpression(expr));
          }
          if (name == "tail") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_reference_expression(ast.createGetTailExpression(expr));
          }
          if (name == "map") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_array_map_definition(expr));
          }
          if (name == "fold") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_array_fold_definition(expr));
          }
          if (name == "filter") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_array_filter_definition(expr));
          }
          if (name == "find") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_array_find_definition(expr));
          }
          if (name == "flatten") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_array_flatten_definition(expr));
          }
          if (name == "reverse") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_array_reverse_definition(expr));
          }
          if (name == "partition") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_array_partition_definition(expr));
          }
          if (name == "pad") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_array_pad_definition(expr));
          }
          if (name == "trim") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_array_trim_definition(expr));
          }
          if (name == "drop") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_array_drop_definition(expr));
          }
          if (name == "take") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_array_take_definition(expr));
          }
        }

        if (isConstraint<ConstrainedOptType>(ty)) {
          if (name == "map") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_optional_map_definition(expr));
          }
          if (name == "flatten") {
            if (expr == nullptr) {
              return nullptr;
            }
            return create_definition_expression(create_optional_flatten_definition(expr));
          }
        }

        return ::std::nullopt;
      }

    }

    Library::Library()
    {
    }

    Library::~Library()
    {
    }

    ::std::shared_ptr<Library> Library::get()
    {
      struct Impl: public Library
      {
        ~Impl()
        {
        }

        ::std::shared_ptr<const Definition> findInstanceBuiltin(const GroupNodeCPtr &expr,
            const ::std::string &name, const ::idioma::ast::Node::Span &span) const override
        {
          auto res = getInstanceBuiltin(expr, getConstraintAnnotation(expr), name, span);
          return res.has_value() ? *res : nullptr;
        }

        bool hasInstanceBuiltin(const ConstrainedTypePtr &type, const ::std::string &name) const
        override
        {
          return getInstanceBuiltin(nullptr, type, name, ::idioma::ast::Node::Span()).has_value();
        }
      };
      return ::std::make_shared<Impl>();
    }
  }
}
