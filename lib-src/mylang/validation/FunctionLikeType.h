#ifndef CLASS_MYLANG_VALIDATION_FUNCTIONLIKETYPE_H
#define CLASS_MYLANG_VALIDATION_FUNCTIONLIKETYPE_H

#ifndef CLASS_MYLANG_TYPES_TYPE_H
#include <mylang/types/Type.h>
#endif

#ifndef CLASS_MYLANG_TYPES_FUNCTIONTYPE_H
#include <mylang/types/FunctionType.h>
#endif
#include <mylang/defs.h>
#include <functional>
#include <memory>
#include <vector>

namespace mylang {
  namespace validation {

    /** A function like type is a type, to whose instances the call operator can be applied */
    class FunctionLikeType: public mylang::types::Type
    {
      /** A concretize functor */
    public:
      typedef ::std::function<
          ::std::shared_ptr<const mylang::types::FunctionType>(const ::std::vector<TypePtr> &argv)> ConcretizeFN;

    protected:
      FunctionLikeType();

    public:
      ~FunctionLikeType() = 0;

      /**
       * Create a function like type with a lambda function
       * @param f a concretize function
       * @return a FunctionLikeType type
       */
    public:
      static ::std::shared_ptr<const FunctionLikeType> get(ConcretizeFN f);

      /**
       * Get a signature for the specified arguments.
       * @return a function type that corresponds to the specified arguments or null if this generic function cannot process the arguments
       */
    public:
      virtual ::std::shared_ptr<const mylang::types::FunctionType> concretize(
          const ::std::vector<TypePtr> &argv) const = 0;

      /** This type is internal and will not accept a visitor */
    public:
      void accept(mylang::types::TypeVisitor &v) const final;

      bool isSameType(const Type &other) const final;

      ::std::string toString() const final;

      Ptr normalize(mylang::types::TypeSet &ts) const final;

      bool isConcrete() const final;

    };
  }
}
#endif
