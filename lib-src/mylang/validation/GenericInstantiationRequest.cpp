#include <mylang/validation/GenericInstantiationRequest.h>
#include <mylang/annotations.h>

namespace mylang {
  namespace validation {

    NamePtr GenericInstantiationRequest::instanceKey() const
    {
      return getNameAnnotation(instanceName);
    }

  }
}
