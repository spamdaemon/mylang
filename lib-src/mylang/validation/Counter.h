#ifndef CLASS_MYLANG_VALIDATION_COUNTER_H
#define CLASS_MYLANG_VALIDATION_COUNTER_H

namespace mylang {
  namespace validation {

    class Counter
    {
    public:
      Counter()
          : count(0)
      {
      }

    public:
      unsigned long long increment()
      {
        return count++;
      }

    private:
      unsigned long long count;
    };

  }
}
#endif
