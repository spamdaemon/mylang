#include <mylang/validation/DefGenericFunctionContext.h>
#include <mylang/annotations.h>
#include <mylang/vast/VAst.h>
#include <mylang/vast/ValidationError.h>

namespace mylang {
  namespace validation {
    namespace {
      enum class MatchKind
      {
        NONE, EXACT, COMPATIBLE
      };

      static MatchKind getMatchKind(const ConstrainedTypePtr &param, const ConstrainedTypePtr &ty)
      {
        if (param->isSameConstraint(*ty)) {
          return MatchKind::EXACT;
        }
        if (param->canImplicitlyCastFrom(*ty)) {
          return MatchKind::COMPATIBLE;
        }
        return MatchKind::NONE;
      }
    }

    using namespace mylang::constraints;
    using namespace mylang::patterns;
    using namespace mylang::vast;

    DefGenericFunctionContext::Entry::Entry(TokenNodeCPtr xdefName,
        ::std::vector<::std::pair<TokenNodeCPtr, PatternPtr>> genericParams, PatternPtr xreturnType,
        ::std::vector<PatternPtr> xparameters, bool xhaveBody)
        : defName(xdefName), id(getNameAnnotation(xdefName)), genericParameters(genericParams),
            returnType(xreturnType), parameters(xparameters),
            canonical(
                FunctionPattern::create(
                    ConstrainedTypePattern::create(ConstrainedVoidType::create()), xparameters)),
            haveBody(xhaveBody),
            functionSignature(
                xreturnType ? FunctionPattern::create(returnType, parameters) : nullptr)
    {
    }

    ::std::shared_ptr<const FunctionPattern> DefGenericFunctionContext::Entry::signature() const
    {
      return functionSignature;
    }

    mylang::patterns::MatchContext::Ptr DefGenericFunctionContext::Entry::createMatchContext() const
    {
      auto ctx = MatchContext::create();
      for (size_t i = 0; ctx && i < specializations.size(); ++i) {
        ctx = genericParameters.at(i).second->match(specializations.at(i), ctx);
      }
      return ctx;
    }

    ::std::unique_ptr<DefGenericFunctionContext::Entry> DefGenericFunctionContext::Entry::specialize(
        ::std::vector<ConstrainedTypePtr> xspecialization) const
    {
      auto res = ::std::make_unique<Entry>(*this);
      res->specializations.insert(res->specializations.end(), xspecialization.begin(),
          xspecialization.end());

      if (res->specializations.size() > res->genericParameters.size()) {
        return nullptr;
      }
      auto ctx = res->createMatchContext();

      if (!ctx) {
        return nullptr;
      }
      return res;
    }

    bool DefGenericFunctionContext::Entry::hasExtension(const ConstrainedTypePtr &ty) const
    {
      if (parameters.size() > 0) {
        if (parameters.at(0)->match(ty, createMatchContext())) {
          return true;
        }
      }

      return false;
    }

    /**
     * Determine the kind of match of the arguments with this entry.
     * @return a match context or nullptr if a match was not possible
     */
    int DefGenericFunctionContext::Entry::determineMatch(
        const ::std::vector<ConstrainedTypePtr> &args, MatchContext::Ptr &ctx) const
    {
      if (parameters.size() != args.size()) {
        return -1;
      }
      int matchCount = 0;
      for (size_t i = 0; ctx && i < args.size(); ++i) {
        auto ctype = parameters.at(i)->self<ConstrainedTypePattern>();
        if (ctype) {
          auto kind = getMatchKind(ctype->type, args.at(i));
          if (kind == MatchKind::EXACT) {
            ++matchCount;
          } else if (kind != MatchKind::COMPATIBLE) {
            return -1;
          }
        } else {
          ctx = parameters.at(i)->match(args.at(i), ctx);
        }
      }
      return ctx ? matchCount : -1;
    }

    DefGenericFunctionContext::DefGenericFunctionContext(const NamePtr &the_name,
        VariablePtr xvinfo, bool xisExtension, ::std::shared_ptr<Counter> c)
        : ContainerContext(the_name), isExtension(xisExtension), vinfo(xvinfo), nameCounter(c)
    {
    }

    DefGenericFunctionContext::~DefGenericFunctionContext()
    {
    }

    ::std::shared_ptr<DefGenericFunctionContext> DefGenericFunctionContext::specialize(
        const ::std::vector<ConstrainedTypePtr> &types) const
    {
      auto res = ::std::make_shared<DefGenericFunctionContext>(name, vinfo, isExtension,
          nameCounter);

      ::std::vector<Entry> newGenerics;

      for (size_t i = 0; i < generics.size(); ++i) {
        auto ptr = generics.at(i).specialize(types);
        if (ptr) {
          newGenerics.push_back(*ptr);
        }
      }

      for (size_t i = 0; i < newGenerics.size(); ++i) {
        bool keep_i = true;
        for (size_t j = 0; keep_i && j < newGenerics.size(); ++j) {
          if (j != i
              && newGenerics.at(i).canonical->isEquivalentPattern(*newGenerics.at(j).canonical,
                  Pattern::createEquivalencyMap())
              && isMoreSpecificThan(newGenerics.at(j).genericParameters,
                  newGenerics.at(i).genericParameters)) {
            keep_i = false;
          }
        }
        if (keep_i) {
          res->generics.push_back(newGenerics.at(i));
        }
      }
      return res;

    }

    ::std::shared_ptr<DefGenericFunctionContext> DefGenericFunctionContext::generalize() const
    {
      auto res = ::std::make_shared<DefGenericFunctionContext>(name, vinfo, isExtension,
          nameCounter);
      for (size_t i = 0; i < generics.size(); ++i) {
        bool keep_i = true;
        for (size_t j = 0; keep_i && j < generics.size(); ++j) {
          if (j != i
              && generics.at(j).canonical->isEquivalentPattern(*generics.at(j).canonical,
                  Pattern::createEquivalencyMap())
              && isMoreGeneralThan(generics.at(j).genericParameters,
                  generics.at(i).genericParameters)) {
            keep_i = false;
          }
        }
        if (keep_i) {
          res->generics.push_back(generics.at(i));
        }
      }
      return res;
    }

    GroupNodeCPtr DefGenericFunctionContext::create_reference(const ScopePtr&,
        const TokenNodeCPtr &src) const
    {
      return nullptr;
    }

    GroupNodeCPtr DefGenericFunctionContext::create_call(const ScopePtr&, const SourceLocation &src,
        const GroupNodes &args, RegisterVariableFN, VisitFN) const
    {
      ::std::vector<ConstrainedTypePtr> argTypes;
      for (auto arg : args) {
        argTypes.push_back(getConstraintAnnotation(arg));
      }
      // first find an exact match for the function
      auto fnExpr = instantiateGeneric(src, argTypes);
      if (fnExpr) {
        VAst ast;
        return ast.createFunctionCallExpression(fnExpr, args);
      }
      throw ValidationError(src, "arguments do not match function signature");
    }

    NamePtr DefGenericFunctionContext::makeNewName()
    {
      NamePtr nm;
      ::std::string id = ::std::to_string(nameCounter->increment());

      if (name->isFQN()) {
        nm = mylang::names::Name::createFQN(name->localName() + id, name->parent());
      } else {
        nm = mylang::names::Name::create(name->localName() + id, name->parent());
      }
      return nm;
    }

    ::std::shared_ptr<const ConstrainedFunctionType> DefGenericFunctionContext::getSignature(
        const NamePtr &key) const
    {
      for (Entry &e : generics) {
        auto instance = e.requests.find(key);
        if (instance != e.requests.end()) {
          return instance->second->signature;
        }
      }
      return nullptr;
    }

    bool DefGenericFunctionContext::isMoreGeneralThan(
        const ::std::vector<::std::pair<TokenNodeCPtr, PatternPtr>> &p1,
        const ::std::vector<::std::pair<TokenNodeCPtr, PatternPtr>> &p2)
    {
      if (p1.size() != p2.size()) {
        return false;
      }
      for (size_t i = 0; i < p1.size(); ++i) {
        if (p1.at(i).second->isMoreGeneralThan(*p2.at(i).second)) {
          return true;
        }
      }
      return false;
    }

    bool DefGenericFunctionContext::isMoreSpecificThan(
        const ::std::vector<::std::pair<TokenNodeCPtr, PatternPtr>> &p1,
        const ::std::vector<::std::pair<TokenNodeCPtr, PatternPtr>> &p2)
    {
      if (p1.size() > p2.size()) {
        return true;
      }
      for (size_t i = 0; i < p1.size(); ++i) {
        if (p1.at(i).second->isMoreSpecificThan(*p2.at(i).second)) {
          return true;
        }
      }
      return false;
    }

    bool DefGenericFunctionContext::isEquivalent(
        const ::std::vector<::std::pair<TokenNodeCPtr, PatternPtr>> &p1,
        const ::std::vector<::std::pair<TokenNodeCPtr, PatternPtr>> &p2)
    {
      if (p1.size() != p2.size()) {
        return false;
      }
      auto eq = Pattern::createEquivalencyMap();
      for (size_t i = 0; eq && i < p1.size(); ++i) {
        eq = p1.at(i).second->isEquivalentPattern(*p2.at(i).second, ::std::move(eq));
      }
      return eq != nullptr;
    }

    NamePtr DefGenericFunctionContext::addEntry(const TokenNodeCPtr &sourceName,
        const ::std::vector<::std::pair<TokenNodeCPtr, PatternPtr>> &genericParams,
        const PatternPtr &retTy, const ::std::vector<PatternPtr> &params, bool haveBody)
    {
      auto canonical = FunctionPattern::create(
          ConstrainedTypePattern::create(ConstrainedVoidType::getVoid()), params);
      PatternPtr fnPattern;
      if (retTy) {
        fnPattern = FunctionPattern::create(retTy, params);
      }

      for (Entry &e : generics) {

        if (e.canonical->isEquivalentPattern(*canonical, Pattern::createEquivalencyMap())
            && isEquivalent(genericParams, e.genericParameters)) {
          if (e.haveBody && haveBody) {
            return nullptr;
          }
          if (!e.haveBody && fnPattern) {
            if (!e.signature()->isEquivalentPattern(*fnPattern, Pattern::createEquivalencyMap())) {
              return nullptr;
            }
          }

          if (!e.haveBody) {
            e.haveBody = haveBody;
            return e.id;
          }
        }
      }

      NamePtr nm = makeNewName();
      auto defName = sourceName->copy()->self<TokenNode>();
      setNameAnnotation(defName, nm);

      generics.push_back(Entry(defName, genericParams, retTy, params, haveBody));
      return nm;
    }

    DefGenericFunctionContext::Entry& DefGenericFunctionContext::getEntry(const NamePtr &key)
    {
      for (Entry &e : generics) {
        if (e.id == key) {
          return e;
        }
      }
      throw ::std::invalid_argument("no such entry");
    }

    const DefGenericFunctionContext::Entry& DefGenericFunctionContext::getEntry(
        const NamePtr &key) const
    {
      for (const Entry &e : generics) {
        if (e.id == key) {
          return e;
        }
      }
      throw ::std::invalid_argument("no such entry");
    }

    bool DefGenericFunctionContext::hasExtension(const ConstrainedTypePtr &ty) const
    {
      for (auto e : generics) {
        if (e.hasExtension(ty)) {
          return true;
        }
      }

      return false;
    }

    GroupNodeCPtr DefGenericFunctionContext::instantiateGeneric(const SourceLocation &sloc,
        const ::std::vector<ConstrainedTypePtr> &args) const
    {
      // first find an exact match for the function
      const Entry *match = nullptr;
      MatchContext::Ptr context;
      int bestMatch = -1;

      for (const Entry &e : generics) {
        auto ctx = e.createMatchContext();
        auto matchCount = e.determineMatch(args, ctx);
        if (matchCount < 0) {
          continue;
        } else if (matchCount < bestMatch) {
          continue;
        } else if (matchCount > bestMatch) {
          bestMatch = matchCount;
          context = ctx;
          match = &e;
//        } else if (match && isMoreSpecificThan(e.genericParameters, match->genericParameters)) {
//          context = ctx;
//          match = &e;
        } else {
          // match is ambiguous, but we might still find a better match
          match = nullptr;
        }
      }
      if (bestMatch < 0) {
        throw ValidationError(sloc, "failed to match arguments for function: " + name->localName());
      } else if (!match) {
        throw ValidationError(sloc, "ambiguous function instantiation: " + name->localName());
      }

      // determine the type signature of the invocation with respect to the pattern
      // this allows us to make we don't generate multiple functions for the same
      // invocation
      ::std::vector<ConstrainedTypePtr> argTypes;
      for (size_t i = 0; i < args.size(); ++i) {
        auto p = match->parameters.at(i);
        auto t = p->createType(context);
        if (!t.second) {
          throw ValidationError(sloc, "failed to create types");
        }
        argTypes.push_back(t.second);
      }

      ::std::shared_ptr<const GenericInstantiationRequest> request;
      for (auto e : match->requests) {
        if (e.second->argumentTypes.size() == argTypes.size()) {
          bool found = true;
          for (size_t i = 0; i < argTypes.size(); ++i) {
            if (!argTypes.at(i)->isSameConstraint(*e.second->argumentTypes.at(i))) {
              found = false;
              break;
            }
          }
          if (found) {
            request = e.second;
            break;
          }
        }
      }
      if (!request) {
        /* build up the instantiation request */
        auto req = ::std::make_shared<GenericInstantiationRequest>();
        req->vinfo = vinfo;
        req->providerKey = match->id;
        req->sloc = sloc;
        req->argumentTypes = argTypes;
        req->names = match->genericParameters;
        req->initialMatchContext = context;

        if (match->signature()) {
          auto fnTy = match->signature()->createType(context);
          if (fnTy.second) {
            req->signature = fnTy.second->self<ConstrainedFunctionType>();
          } else {
            throw ValidationError(sloc,
                "failed to determine a valid function signature: " + name->localName());
          }
        }

        const ::std::string gid = "generic" + ::std::to_string(nameCounter->increment());
        NamePtr instanceName;
        if (match->id->isFQN()) {
          instanceName = mylang::names::Name::createFQN(gid, match->id);
        } else {
          instanceName = mylang::names::Name::create(gid, match->id);
        }
        auto tok = match->defName->copy()->self<TokenNode>();
        setNameAnnotation(tok, instanceName);
        req->instanceName = tok;

        request = req;
        match->requests.emplace(instanceName, request);
      }
      GroupNodeCPtr res;
      if (request->signature) {
        VAst ast;
        auto ty = ast.createType(request->signature, sloc.span());
        auto node = ast.createInstantiateGenericFunction(ty, match->defName, request->instanceName);
        node->annotate(GenericInstantiationRequest::Annotation(),
            match->requests[request->instanceKey()]);
        res = node;
      } else if (match->provider) {
        res = match->provider(*request);
      } else {
        throw ValidationError(sloc, "no generic function definition found");
      }

      return res;
    }

  }
}

