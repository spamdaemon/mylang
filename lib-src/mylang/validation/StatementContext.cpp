#include <mylang/validation/StatementContext.h>

namespace mylang {
  namespace validation {

    StatementContext::VariableDispositions::VariableDispositions()
    {
    }

    StatementContext::VariableDispositions::~VariableDispositions()
    {
    }

    bool StatementContext::VariableDispositions::existsName(const NamePtr &n) const
    {
      return valueDispositions.count(n) > 0 || variableDispositions.count(n) > 0;
    }

    ::std::optional<StatementContext::VariableDisposition> StatementContext::VariableDispositions::getValueDisposition(
        NamePtr n) const
    {
      auto i = valueDispositions.find(n);
      if (i != valueDispositions.end()) {
        return i->second;
      }
      return ::std::nullopt;
    }

    ::std::optional<StatementContext::VariableDisposition> StatementContext::VariableDispositions::getVariableDisposition(
        NamePtr n) const
    {
      auto i = variableDispositions.find(n);
      if (i != variableDispositions.end()) {
        return i->second;
      }
      return ::std::nullopt;
    }

    void StatementContext::VariableDispositions::setValueDisposition(const NamePtr &n,
        VariableDisposition disp)
    {
      valueDispositions[n] = disp;
    }

    void StatementContext::VariableDispositions::setVariableDisposition(const NamePtr &n,
        VariableDisposition disp)
    {
      variableDispositions[n] = disp;
    }

    const ::std::map<NamePtr, StatementContext::VariableDisposition>&
    StatementContext::VariableDispositions::getVariableDispositions() const
    {
      return variableDispositions;
    }

    const ::std::map<NamePtr, StatementContext::VariableDisposition>&
    StatementContext::VariableDispositions::getValueDispositions() const
    {
      return valueDispositions;
    }

    StatementContext::StatementContext(::std::shared_ptr<const StatementContext> xparent)
        : returnDisposition(ReturnDisposition::NO_RETURN),
            throwDisposition(ThrowDisposition::NO_THROW),
            continueDisposition(ContinueDisposition::NO_CONTINUE),
            breakDisposition(BreakDisposition::NO_BREAK), parent(xparent)
    {
    }

    StatementContext::~StatementContext()
    {
    }

    void StatementContext::setReturnDisposition(ReturnDisposition disp)
    {
      if (returnDisposition != disp) {
        returnDisposition = disp;
      }
    }

    StatementContext::ReturnDisposition StatementContext::getReturnDisposition() const
    {
      return returnDisposition;
    }

    void StatementContext::setThrowDisposition(ThrowDisposition disp)
    {
      if (throwDisposition != disp) {
        throwDisposition = disp;
      }
    }

    StatementContext::ThrowDisposition StatementContext::getThrowDisposition() const
    {
      return throwDisposition;
    }

    void StatementContext::setContinueDisposition(ContinueDisposition disp)
    {
      if (continueDisposition != disp) {
        continueDisposition = disp;
      }
    }

    StatementContext::ContinueDisposition StatementContext::getContinueDisposition() const
    {
      return continueDisposition;
    }

    void StatementContext::setBreakDisposition(BreakDisposition disp)
    {
      if (breakDisposition != disp) {
        breakDisposition = disp;
      }
    }

    StatementContext::BreakDisposition StatementContext::getBreakDisposition() const
    {
      return breakDisposition;
    }

    bool StatementContext::existsName(const NamePtr &n) const
    {
      const StatementContext *ctx = this;
      while (ctx) {
        if (ctx->dispositions.existsName(n)) {
          return true;
        }
        ctx = ctx->parent.get();
      }
      return false;
    }

    StatementContext::VariableDisposition StatementContext::getValueDisposition(NamePtr n) const
    {
      const StatementContext *ctx = this;
      while (ctx) {
        auto res = ctx->dispositions.getValueDisposition(n);
        if (res.has_value()) {
          return *res;
        }
        ctx = ctx->parent.get();
      }
      return VariableDisposition::UNINITIALIZED;
    }

    StatementContext::VariableDisposition StatementContext::getVariableDisposition(NamePtr n) const
    {
      const StatementContext *ctx = this;
      while (ctx) {
        auto res = ctx->dispositions.getVariableDisposition(n);
        if (res.has_value()) {
          return *res;
        }
        ctx = ctx->parent.get();
      }
      return VariableDisposition::UNINITIALIZED;
    }

    ::std::map<NamePtr, StatementContext::VariableDisposition> StatementContext::inheritedVariableDispositions() const
    {
      ::std::map<NamePtr, VariableDisposition> res;
      for (auto e : dispositions.getVariableDispositions()) {
        if (parent && parent->existsName(e.first)) {
          res.insert(e);
        }
      }
      return res;
    }

    ::std::map<NamePtr, StatementContext::VariableDisposition> StatementContext::inheritedValueDispositions() const
    {
      ::std::map<NamePtr, VariableDisposition> res;
      for (auto e : dispositions.getValueDispositions()) {
        if (parent && parent->existsName(e.first)) {
          res.insert(e);
        }
      }
      return res;
    }

    void StatementContext::setValueDisposition(const NamePtr &n, VariableDisposition disp)
    {
      dispositions.setValueDisposition(n, disp);
    }

    void StatementContext::setVariableDisposition(const NamePtr &n, VariableDisposition disp)
    {
      dispositions.setVariableDisposition(n, disp);
    }

    void StatementContext::setDispositions(const VariableDispositions &disp)
    {
      dispositions = disp;
    }

    const StatementContext::VariableDispositions& StatementContext::getDispositions() const
    {
      return dispositions;
    }

  }
}
