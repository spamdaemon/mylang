#ifndef CLASS_MYLANG_VALIDATION_DefinitionGroup_H
#define CLASS_MYLANG_VALIDATION_DefinitionGroup_H

#ifndef _IDIOMA_AST_GROUPNODE_H
#include <idioma/ast/GroupNode.h>
#endif

#ifndef CLASS_MYLANG_VALIDATION_SCOPING_DEFINITION_H
#include <mylang/validation/Definition.h>
#endif

namespace mylang {
  namespace validation {

    class DefinitionGroup: public ::idioma::ast::GroupNode
    {

    public:
      DefinitionGroup(const ::std::shared_ptr<const Definition> &def);

    public:
      ~DefinitionGroup();

      /**
       * Create a new group with the specified definition.
       * @param def a defintion
       * @retirn a group
       */
    public:
      static GroupNodePtr create(const ::std::shared_ptr<const Definition> &def);

    public:
      const ::std::shared_ptr<const Definition> definition;
    };
  }
}
#endif
