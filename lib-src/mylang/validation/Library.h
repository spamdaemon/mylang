#ifndef CLASS_MYLANG_VALIDATION_LIBRARY_H
#define CLASS_MYLANG_VALIDATION_LIBRARY_H
#include <mylang/defs.h>
#include <memory>
#include <string>

namespace mylang {
  namespace validation {
    class Definition;
  }
}

#ifndef CLASS_MYLANG_VALIDATION_DEFINITION_H
#include <mylang/validation/Definition.h>
#endif

namespace mylang {
  namespace validation {

    /**
     * This class represents the standard library. Any functions in the standard
     * library can be implemented using either operators, builtins, or other
     * library functions.
     */
    class Library
    {
    public:

      /** Constructor */
    private:
      Library();

    public:
      virtual ~Library() = 0;

      /**
       * Get the builtins.
       * @return an instanceof the builtins
       */
    public:
      static ::std::shared_ptr<Library> get();

      /**
       * Find a builtin for an instanceof of the given type or the type itself.
       * @param expr an expression
       * @param name a a name
       * @return a definition or nullptr if not found.
       */
    public:
      virtual ::std::shared_ptr<const Definition> findInstanceBuiltin(const GroupNodeCPtr &expr,
          const ::std::string &name, const ::idioma::ast::Node::Span &span) const = 0;

      /**
       * Find a builtin for an instanceof of the given type or the type itself.
       * @param expr an expression
       * @param name a a name
       * @return a definition or nullptr if not found.
       */
    public:
      virtual bool hasInstanceBuiltin(const ConstrainedTypePtr &ty,
          const ::std::string &name) const = 0;
    };
  }
}
#endif
