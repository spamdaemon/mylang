#ifndef CLASS_MYLANG_VALIDATION_STATEMENTCONTEXT_H
#define CLASS_MYLANG_VALIDATION_STATEMENTCONTEXT_H

#ifndef CLASS_MYLANG_VALIDATION_DEFINITION_H
#include <mylang/validation/Definition.h>
#endif

#include <optional>

namespace mylang {
  namespace validation {

    struct StatementContext: public Definition
    {
      enum class ReturnDisposition
      {
        /** A statement will not return */
        NO_RETURN = 1,
        /** A statement may return return, but is not guaranteed to return */
        MAY_RETURN = 2,
        /** A statement is guaranteed to return (this is always true for a return statement) */
        WILL_RETURN = 3
      };

      enum class ThrowDisposition
      {
        /** A statement will not throw */
        NO_THROW = 1,
        /** A statement may throw an exception */
        MAY_THROW = 2,
        /** A statement is guaranteed to throw an exception (true for a throw statement) */
        WILL_THROW = 3
      };

      enum class BreakDisposition
      {
        /** A statement will not break out of a loop */
        NO_BREAK = 1,
        /** A statement may break out of a loop */
        MAY_BREAK = 2,
        /** A statement is guaranteed to break out of a loop (true for a break statement) */
        WILL_BREAK = 3
      };

      enum class ContinueDisposition
      {
        /** A statement will not continue a loop */
        NO_CONTINUE = 1,
        /** A statement may continue a loop */
        MAY_CONTINUE = 2,
        /** A statement is guaranteed to continue a loop (true for a continue statement) */
        WILL_CONTINUE = 3
      };

      enum class VariableDisposition
      {
        /** A variable or value is not yet initialized */
        UNINITIALIZED = 1,
        /** A variable may be initializer, or , conversely, may not be initialized */
        MAYBE_INITIALIZED = 2,
        /** The variable is guaranteed to have a value */
        INITIALIZED = 3
      };

      struct VariableDispositions
      {
        VariableDispositions();

        ~VariableDispositions();

        /**
         * Get the disposition of a variable.
         * @param name name of a variable
         * @return the variable disposition, or UNINITIALIZED if not found
         */
        bool existsName(const NamePtr &n) const;

        /**
         * Get the disposition of a variable.
         * @param name name of a variable
         * @return the variable disposition, or UNINITIALIZED if not found
         */
        ::std::optional<VariableDisposition> getValueDisposition(NamePtr n) const;

        /**
         * Get the disposition of a variable.
         * @param name name of a variable
         * @return the variable disposition, or UNINITIALIZED if not found
         */
        ::std::optional<VariableDisposition> getVariableDisposition(NamePtr n) const;

        void setValueDisposition(const NamePtr &n, VariableDisposition disp);

        void setVariableDisposition(const NamePtr &n, VariableDisposition disp);

        const ::std::map<NamePtr, VariableDisposition>& getVariableDispositions() const;

        const ::std::map<NamePtr, VariableDisposition>& getValueDispositions() const;

        /** A map of variable and they initialization disposition */
      private:
        ::std::map<NamePtr, VariableDisposition> variableDispositions;

        /** A map of values and they initialization disposition */
      private:
        ::std::map<NamePtr, VariableDisposition> valueDispositions;
      };

    public:
      StatementContext(::std::shared_ptr<const StatementContext> xparent);

    public:
      virtual ~StatementContext();

      /**
       * Set the return disposition.
       * @param disp
       */
    public:
      void setReturnDisposition(ReturnDisposition disp);

      /**
       * Set the return disposition.
       * @param disp
       */
    public:
      ReturnDisposition getReturnDisposition() const;

      /**
       * Set the break disposition.
       * @param disp
       */
    public:
      void setBreakDisposition(BreakDisposition disp);

      /**
       * Set the break disposition.
       * @param disp
       */
    public:
      BreakDisposition getBreakDisposition() const;

      /**
       * Set the continue disposition.
       * @param disp
       */
    public:
      void setContinueDisposition(ContinueDisposition disp);

      /**
       * Set the continue disposition.
       * @param disp
       */
    public:
      ContinueDisposition getContinueDisposition() const;

      /**
       * Set the throw disposition.
       * @param disp
       */
    public:
      void setThrowDisposition(ThrowDisposition disp);

      /**
       * Set the throw disposition.
       * @param disp
       */
    public:
      ThrowDisposition getThrowDisposition() const;

      /**
       * Get the disposition of a variable.
       * @param name name of a variable
       * @return the variable disposition, or UNINITIALIZED if not found
       */
      bool existsName(const NamePtr &n) const;

      /**
       * Get the disposition of a variable.
       * @param name name of a variable
       * @return the variable disposition, or UNINITIALIZED if not found
       */
      VariableDisposition getValueDisposition(NamePtr n) const;

      /**
       * Get the disposition of a variable.
       * @param name name of a variable
       * @return the variable disposition, or UNINITIALIZED if not found
       */
      VariableDisposition getVariableDisposition(NamePtr n) const;

      /** A map of variable and they initialization disposition */
      ::std::map<NamePtr, VariableDisposition> inheritedVariableDispositions() const;

      /** A map of values and they initialization disposition */
      ::std::map<NamePtr, VariableDisposition> inheritedValueDispositions() const;

      void setValueDisposition(const NamePtr &n, VariableDisposition disp);

      void setVariableDisposition(const NamePtr &n, VariableDisposition disp);

      /**
       * Get the variable dispositions
       * @return the variable dispositions
       */
    public:
      void setDispositions(const VariableDispositions &disp);

      const VariableDispositions& getDispositions() const;

      /** The return disposition of this context */
    private:
      ReturnDisposition returnDisposition;

      /** The return disposition of this context */
    private:
      ThrowDisposition throwDisposition;

      /** The continue disposition */
    private:
      ContinueDisposition continueDisposition;

      /** The continue disposition */
    private:
      BreakDisposition breakDisposition;

      /** The variable dispositions */
    private:
      VariableDispositions dispositions;

      /** The parent context */
    private:
      const ::std::shared_ptr<const StatementContext> parent;
    };

  }
}
#endif
