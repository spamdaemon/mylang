#include <mylang/validation/TransformChainBuilder.h>
#include <mylang/constraints/ConstrainedOptType.h>
#include <mylang/transforms/TransformBase.h>
#include <mylang/names/Name.h>
#include <mylang/vast/VAst.h>
#include <mylang/annotations.h>
#include <cassert>
#include <optional>
#include <string>
#include <vector>

namespace mylang {
  namespace validation {
    namespace {
      using namespace mylang::transforms;

      static ::std::optional<Type> toType(const ConstrainedTypePtr &cty)
      {
        auto ty = cty;
        auto optTy = ty->self<mylang::constraints::ConstrainedOptType>();
        if (optTy) {
          ty = optTy->element;
        }
        auto t = ty->self<mylang::constraints::ConstrainedNamedType>();
        if (t) {
          return Type(t->name()->fullName(), optTy != nullptr);
        } else {
          return ::std::nullopt;
        }
      }

      static ConstrainedTypePtr toConstrainedType(const Type &t,
          TransformChainBuilder::NameResolver nameResolver,
          TransformChainBuilder::TypeResolver typeResolver)
      {
        auto name = nameResolver(t.asNonOptional().getName());
        if (name == nullptr) {
          return nullptr;
        }
        auto ty = typeResolver(name);
        if (ty && t.optional) {
          return mylang::constraints::ConstrainedOptType::get(ty);
        }
        return ty;
      }

      static TransformChainBuilder::FunctionTypePtr fromCall(const Call &c,
          TransformChainBuilder::NameResolver nameResolver,
          TransformChainBuilder::TypeResolver typeResolver)
      {
        ConstrainedTypePtr retTy = toConstrainedType(c.output, nameResolver, typeResolver);
        if (retTy == nullptr) {
          return nullptr;
        }
        // how do I create a reference to a named type we've never seen????
        ::std::vector<ConstrainedTypePtr> params;
        for (const auto &t : c.input) {
          auto ty = toConstrainedType(t, nameResolver, typeResolver);
          if (ty == nullptr) {
            return nullptr;
          }
          params.push_back(ty);
        }

        return mylang::constraints::ConstrainedFunctionType::get(retTy, params);
      }

      GroupNodeCPtr createGuardedGroup(const GroupNodeCPtr &fnExpr, const GroupNodeCPtr &nil,
          ::std::vector<GroupNodeCPtr> &arguments, size_t i)
      {
        ::mylang::vast::VAst ast;
        if (i == arguments.size()) {
          auto callExpr = ast.createFunctionCallExpression(fnExpr, arguments);
          if (nil
              && getConstraintAnnotation(callExpr)->self<mylang::constraints::ConstrainedOptType>()
                  == nullptr) {

            auto ty = getConstraintAnnotation(nil);
            callExpr = ast.createNewOptionalExpression(ast.createType(ty, { }), { callExpr });
          }
          return callExpr;
        }
        GroupNodeCPtr arg = arguments.at(i);
        auto ty = getConstraintAnnotation(arg);
        if (auto optTy = ty->self<mylang::constraints::ConstrainedOptType>(); optTy) {
          return ast.createWithExpression(arg, [&](GroupNodeCPtr optval) {
            auto present = ast.createIsOptionalValuePresentExpression(optval);
            arguments.at(i)=ast.createGetOptionalValueExpression(optval);
            return ast.createConditionalExpression(present,
                createGuardedGroup(fnExpr, nil,arguments, ++i),
                nil);
          });
        } else {
          return createGuardedGroup(fnExpr, nil, arguments, ++i);
        }
      }

      GroupNodeCPtr createCallExpr(const Chain::Ptr &chain,
          const ::std::map<Type, GroupNodeCPtr> &args,
          TransformChainBuilder::NameResolver nameResolver,
          TransformChainBuilder::TypeResolver typeResolver)
      {
        ::mylang::vast::VAst ast;
        const auto &tx = chain->getTransform();
        GroupNodeCPtr callExpr;

        auto fnTy = fromCall(tx->call, nameResolver, typeResolver);
        if (fnTy == nullptr) {
          return nullptr;
        }

        GroupNodeCPtr nil;
        if (auto optTy = fnTy->returnType->self<mylang::constraints::ConstrainedOptType>(); optTy) {
          nil = ast.createNewOptionalExpression(ast.createType(optTy, { }), { });
        } else if (chain->isOptional()) {
          nil = ast.createNewOptionalExpression(
              ast.createType(mylang::constraints::ConstrainedOptType::get(fnTy->returnType), { }),
              { });
        }

        if (tx->isIdentity()) {
          callExpr = args.at(tx->call.output);
          if (nil) {
            auto ty = ast.getRelation(callExpr, mylang::vast::VAst::Relation::TYPE);
            callExpr = ast.createNewOptionalExpression(ty, { callExpr });
          }
        } else {
          auto fn = nameResolver(tx->name);
          if (fn == nullptr) {
            return nullptr;
          }
          auto fnExpr = ast.createVariableExpression(nullptr, fn, fnTy);

          GroupNodes arguments;
          for (auto p : tx->call.input) {
            auto argChain = chain->getArgChain(*p.name);
            auto arg = createCallExpr(argChain, args, nameResolver, typeResolver);
            if (arg == nullptr) {
              return nullptr;
            }
            arguments.push_back(arg);
          }

          callExpr = createGuardedGroup(fnExpr, nil, arguments, 0);
        }

        return callExpr;
      }
    }

    GroupNodeCPtr TransformChainBuilder::createFunction(const mylang::transforms::Chain::Ptr &chain,
        const FunctionTypePtr &fnTy, NameResolver nameResolver, TypeResolver typeResolver)
    {
      ::mylang::vast::VAst ast;
      // create a lambda expression whose type is the given function pointer
      try {
        return ast.createLambdaExpression(*fnTy, { },
            [&](
                GroupNodes args) {
                  GroupNodes res;
                  ::std::map<Type,GroupNodeCPtr> arguments;
                  for (const auto& arg : args) {
                    auto argTy =getConstraintAnnotation(arg);
                    auto ty = toType(argTy);
                    if (!ty.has_value()) {
                      throw ::std::invalid_argument("Not a valid argument type for a transform chain " + argTy->toString());
                    }
                    arguments.emplace(ty.value(),arg);
                  }

                  GroupNodeCPtr expr = createCallExpr(chain, arguments,nameResolver, typeResolver);
                  if (expr==nullptr) {
                    throw ::std::runtime_error("Failed to create a call");
                  }

                  res = {ast.createReturnStatement(expr)};
                  return res;
                });
      } catch (const ::std::exception &ex) {
        ::std::cerr << ex.what() << ::std::endl;
        return nullptr;
      }
    }

  }
}
