#include <mylang/validation/DefinitionGroup.h>

namespace mylang {
  namespace validation {
    enum class UNUSED
    {
      KEY = 0
    };

    DefinitionGroup::DefinitionGroup(const ::std::shared_ptr<const Definition> &def)
        : ::idioma::ast::GroupNode(UNUSED::KEY), definition(def)
    {
    }

    DefinitionGroup::~DefinitionGroup()
    {
    }

    GroupNodePtr DefinitionGroup::create(const ::std::shared_ptr<const Definition> &def)
    {
      return ::std::make_shared<DefinitionGroup>(def);
    }

  }
}
