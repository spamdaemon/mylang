#ifndef CLASS_MYLANG_VALIDATION_GENERICINSTANTIATIONREQUEST_H
#define CLASS_MYLANG_VALIDATION_GENERICINSTANTIATIONREQUEST_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_SOURCELOCATION_H
#include <mylang/SourceLocation.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDFUNCTIONTYPE_H
#include <mylang/constraints/ConstrainedFunctionType.h>
#endif

#ifndef CLASS_MYLANG_PATTERNS_MATCHCONTEXT_H
#include <mylang/patterns/MatchContext.h>
#endif

namespace mylang {
  namespace validation {

    struct GenericInstantiationRequest
    {
      struct Annotation
      {
        bool operator==(const Annotation&) const
        {
          return true;
        }
      };

      NamePtr providerKey;
      TokenNodeCPtr instanceName;
      ::std::vector<::std::pair<TokenNodeCPtr, PatternPtr>> names;
      ::mylang::patterns::MatchContext::Ptr initialMatchContext;
      ::std::vector<ConstrainedTypePtr> argumentTypes;
      SourceLocation sloc;
      ::std::shared_ptr<const mylang::constraints::ConstrainedFunctionType> signature;
      VariablePtr vinfo;
      NamePtr instanceKey() const;
    };

  }
}
#endif
