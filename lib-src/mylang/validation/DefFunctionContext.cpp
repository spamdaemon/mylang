#include <mylang/validation/DefFunctionContext.h>
#include <mylang/annotations.h>
#include <mylang/vast/VAst.h>
#include <mylang/vast/ValidationError.h>
#include <cassert>

namespace mylang {
  namespace validation {
    namespace {
      enum class MatchKind
      {
        NONE, EXACT, COMPATIBLE
      };

      static MatchKind getMatchKind(const ConstrainedTypePtr &param, const ConstrainedTypePtr &ty)
      {
        if (param->isSameConstraint(*ty)) {
          return MatchKind::EXACT;
        }
        if (param->canImplicitlyCastFrom(*ty)) {
          return MatchKind::COMPATIBLE;
        }
        return MatchKind::NONE;
      }
    }

    using namespace mylang::constraints;
    using namespace mylang::patterns;
    using namespace mylang::vast;

    DefFunctionContext::Entry::Entry(const TokenNodeCPtr &xbasename, ConstrainedTypePtr xreturnType,
        ::std::vector<ConstrainedTypePtr> xparameters, bool xhaveBody)
        : defName(xbasename), id(getNameAnnotation(xbasename)), parameters(xparameters),
            canonical(ConstrainedFunctionType::get(ConstrainedVoidType::create(), xparameters)),
            haveBody(xhaveBody)
    {
      if (xreturnType) {
        setReturnType(xreturnType);
      }
    }

    const DefGenericFunctionContext& DefFunctionContext::getGenerics() const
    {
      return *generics;
    }

    DefGenericFunctionContext& DefFunctionContext::getGenerics()
    {
      return *generics;
    }

    ::std::shared_ptr<const ConstrainedFunctionType> DefFunctionContext::Entry::signature() const
    {
      return functionSignature;
    }

    bool DefFunctionContext::Entry::hasExtension(const ConstrainedTypePtr &ty) const
    {
      if (parameters.size() > 0) {
        MatchKind kind = getMatchKind(parameters.at(0), ty);
        if (kind != MatchKind::NONE) {
          return true;
        }
      }
      return false;
    }

    void DefFunctionContext::Entry::setReturnType(ConstrainedTypePtr xreturnType)
    {
      if (returnType && !returnType->isSameConstraint(*xreturnType)) {
        throw ::std::runtime_error("return type already set");
      }
      returnType = xreturnType;
      functionSignature = ConstrainedFunctionType::get(returnType, parameters);
    }

    bool DefFunctionContext::Entry::areParametersCompatible(
        const ::std::vector<ConstrainedTypePtr> &params) const
    {
      if (parameters.size() != params.size()) {
        return true;
      }
      for (size_t i = 0; i < params.size(); ++i) {
        if (!params.at(i)->intersectWith(parameters.at(i))) {
          return true;
        }
      }
      return false;
    }

    /**
     * Determine the kind of match of the arguments with this entry.
     * @return -1 if no match possible, otherwise number of exact matches
     */
    int DefFunctionContext::Entry::determineMatch(
        const ::std::vector<ConstrainedTypePtr> &args) const
    {
      if (parameters.size() != args.size()) {
        return -1;
      }
      // do we have at least 1 exact match
      int matchCount = 0;
      for (size_t i = 0; i < args.size(); ++i) {
        auto kind = getMatchKind(parameters.at(i), args.at(i));
        if (kind == MatchKind::EXACT) {
          // there are exact matches
          ++matchCount;
        } else if (kind != MatchKind::COMPATIBLE) {
          // the signature doesn't match the arguments at all
          return -1;
        } else {

        }
      }
      return matchCount;
    }

    DefFunctionContext::DefFunctionContext(const NamePtr &the_name, VariablePtr xvinfo,
        bool xisExtension, bool xisExported, bool xisTransform)
        : ContainerContext(the_name), isExtension(xisExtension), isExported(xisExported),
            isTransform(xisTransform), vinfo(xvinfo), nameCounter(::std::make_shared<Counter>()),
            generics(
                ::std::make_shared<DefGenericFunctionContext>(name, vinfo, xisExtension,
                    nameCounter))
    {
    }

    DefFunctionContext::~DefFunctionContext()
    {
    }

    ::std::shared_ptr<DefFunctionContext> DefFunctionContext::specialize(
        const ::std::vector<ConstrainedTypePtr> &spec) const
    {
      assert(!isExported && !isTransform && "Cannot specialize an exported function");
      auto res = ::std::make_shared<DefFunctionContext>(name, vinfo, isExtension, false, false);
      res->generics = generics->specialize(spec);
      return res;
    }

    ::std::shared_ptr<DefFunctionContext> DefFunctionContext::generalize() const
    {
      assert(!isExported && !isTransform && "Cannot generalize an exported function");
      auto res = ::std::make_shared<DefFunctionContext>(name, vinfo, isExtension, false, false);
      res->generics = generics->generalize();
      return res;
    }

    GroupNodeCPtr DefFunctionContext::create_reference(const ScopePtr&,
        const TokenNodeCPtr &src) const
    {

      if (entries.size() == 1 && generics->isEmpty()) {
        auto e = entries.at(0);
        auto signature = e.signature();
        if (signature) {
          VAst ast;
          return ast.createVariableExpression(src, e.id, signature);
        } else {
          throw ValidationError(src, "return type not set");
        }
      }
      // TODO: how to do a reference to a generic?
      return nullptr;
    }

    GroupNodeCPtr DefFunctionContext::create_call(const ScopePtr &scope, const SourceLocation &src,
        const GroupNodes &args, RegisterVariableFN rfn, VisitFN vfn) const
    {
      ::std::vector<ConstrainedTypePtr> argTypes;
      for (auto arg : args) {
        argTypes.push_back(getConstraintAnnotation(arg));
      }
      VAst ast;
      // first find an exact match for the function
      NamePtr fnName = findEntry(argTypes);
      if (fnName) {
        auto fn = getSignature(fnName);
        if (!fn) {
          // this error can happen if this function is called, but it's return type has not been
          // determined due to an error
          throw ValidationError(src, "signature of function is not known");
        }
        auto var = ast.createVariableExpression(nullptr, fnName, fn);
        return ast.createFunctionCallExpression(var, args);
      }
      return generics->create_call(scope, src, args, rfn, vfn);
    }

    NamePtr DefFunctionContext::makeNewName()
    {
      NamePtr nm;
      ::std::string id = ::std::to_string(nameCounter->increment());
      if (isExported || isTransform) {
        assert(name->isFQN() && "exported should be an FQN");
        nm = mylang::names::Name::createFQN(name->localName(), name->parent());
      } else if (name->isFQN()) {
        nm = mylang::names::Name::createFQN(name->localName() + id, name->parent());
      } else {
        nm = mylang::names::Name::create(name->localName() + id, name->parent());
      }
      return nm;
    }

    NamePtr DefFunctionContext::addEntry(TokenNodeCPtr sourceName,
        const ::std::vector<ConstrainedTypePtr> &params, const ConstrainedTypePtr &retTy,
        bool haveBody)
    {
      auto canonical = ConstrainedFunctionType::get(ConstrainedVoidType::getVoid(), params);
      ConstrainedTypePtr fnType;
      if (retTy) {
        fnType = ConstrainedFunctionType::get(retTy, params);
      }

      bool checkForAmbiguousSignature = true;
      for (Entry &e : entries) {
        if (canonical->isSameConstraint(*e.canonical)) {
          if (e.haveBody && haveBody) {
            return nullptr;
          }
          if (!e.haveBody && fnType) {
            if (!e.signature()->isSameConstraint(*fnType)) {
              return nullptr;
            }
          }
          if (!e.haveBody) {
            e.haveBody = haveBody;
            return e.id;
          }
          checkForAmbiguousSignature = false;
        }
      }

      // make sure the parameters are unambiguous
      if (checkForAmbiguousSignature) {
        for (Entry &e : entries) {
          if (!e.areParametersCompatible(params)) {
            return nullptr;
          }
        }
      }

      if (entries.size() == 1 && (isExported || isTransform)) {
        return nullptr;
      }

      // if we we're exported, then we can safely reuse the current name
      NamePtr nm = (isExported || isTransform) ? name : makeNewName();
      auto defName = sourceName->copy()->self<TokenNode>();
      setNameAnnotation(defName, nm);
      entries.push_back(Entry(defName, retTy, params, haveBody));
      return nm;
    }

    ::std::shared_ptr<const ConstrainedFunctionType> DefFunctionContext::getSignature(
        const NamePtr &key) const
    {
      for (Entry &e : entries) {
        if (e.id == key) {
          return e.signature();
        }
      }
      return nullptr;
    }

    DefFunctionContext::Entry& DefFunctionContext::getEntry(const NamePtr &key)
    {
      for (Entry &e : entries) {
        if (e.id == key) {
          return e;
        }
      }
      throw ::std::invalid_argument("no such entry");
    }

    const DefFunctionContext::Entry& DefFunctionContext::getEntry() const
    {
      if (entries.size() == 1) {
        return entries.at(0);
      }
      throw ::std::invalid_argument("no such entry");
    }

    const DefFunctionContext::Entry& DefFunctionContext::getEntry(const NamePtr &key) const
    {
      for (const Entry &e : entries) {
        if (e.id == key) {
          return e;
        }
      }
      throw ::std::invalid_argument("no such entry");
    }

    bool DefFunctionContext::hasExtension(const ConstrainedTypePtr &ty) const
    {
      for (auto e : entries) {
        if (e.hasExtension(ty)) {
          return true;
        }
      }
      return generics->hasExtension(ty);
    }

    NamePtr DefFunctionContext::findEntry(const ::std::vector<ConstrainedTypePtr> &args) const
    {
      // first find an exact match for the function
      const Entry *match = nullptr;
      int bestMatch = -1;
      for (const Entry &e : entries) {
        auto matchCount = e.determineMatch(args);
        if (matchCount < 0) {
          continue;
        } else if (matchCount < bestMatch) {
          continue;
        } else if (matchCount > bestMatch) {
          bestMatch = matchCount;
          match = &e;
        } else {
          // match is ambiguous, but we might still find a better match
          match = nullptr;
        }
      }
      if (!match) {
        return nullptr;
      }
      return match->id;
    }

  }
}

