#include <mylang/names/Name.h>
#include <mylang/validation/Definition.h>
#include <mylang/vast/ValidationError.h>
#include <memory>
#include <string>

namespace mylang {
  namespace validation {
    Definition::Definition()
        : Definition(mylang::names::Name::create())
    {
    }

    Definition::Definition(const NamePtr &the_name)
        : name(the_name)
    {
    }

    /** The destructor */
    Definition::~Definition()
    {
    }

    ::std::shared_ptr<const Definition> Definition::find(const ::std::string&) const
    {
      return nullptr;
    }

    GroupNodeCPtr Definition::create_call(const ScopePtr&, const SourceLocation &src,
        const GroupNodes&, RegisterVariableFN, VisitFN) const
    {
      throw mylang::vast::ValidationError(src, "not a function");
    }

    GroupNodeCPtr Definition::create_reference(const ScopePtr&, const TokenNodeCPtr &src) const
    {
      throw mylang::vast::ValidationError(src, "cannot create expression");
    }

    ::std::shared_ptr<const mylang::types::FunctionType> Definition::get_function_type(
        const ::std::vector<TypePtr> &argv) const
    {
      return nullptr;
    }

    ConstrainedTypePtr Definition::get_type() const
    {
      return nullptr;
    }

  }
}
