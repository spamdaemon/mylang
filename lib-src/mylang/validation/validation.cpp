#include <idioma/ast/GroupNode.h>
#include <idioma/ast/Node.h>
#include <idioma/ast/TokenNode.h>
#include <idioma/astutils/utils.h>
#include <idioma/parser/ParseTree.h>
#include <idioma/Parser.h>
#include <idioma/scope/Scope.h>
#include <idioma/scope/Tree.h>
#include <idioma/util/Value.h>
#include <mylang/annotations.h>
#include <mylang/constraints.h>
#include <mylang/patterns.h>
#include <mylang/expressions/Constant.h>
#include <mylang/grammar/PAst.h>
#include <mylang/names/Name.h>
#include <mylang/names/FQN.h>
#include <mylang/types/ArrayType.h>
#include <mylang/types/BitType.h>
#include <mylang/types/BooleanType.h>
#include <mylang/types/IntegerType.h>
#include <mylang/types/OptType.h>
#include <mylang/types/ProcessType.h>
#include <mylang/types/RealType.h>
#include <mylang/types/StringType.h>
#include <mylang/types/StructType.h>
#include <mylang/types/TupleType.h>
#include <mylang/types/Type.h>
#include <mylang/types/UnionType.h>
#include <mylang/validation/validation.h>
#include <mylang/validation/Builtins.h>
#include <mylang/validation/Library.h>
#include <mylang/validation/ContainerContext.h>
#include <mylang/validation/DefFunctionContext.h>
#include <mylang/validation/Definition.h>
#include <mylang/validation/DefinitionGroup.h>
#include <mylang/validation/FunctionLikeType.h>
#include <mylang/validation/GenericInstantiationRequest.h>
#include <mylang/validation/StatementContext.h>
#include <mylang/validation/TransformResolver.h>
#include <mylang/types/ExportableTypeChecker.h>
#include <mylang/types/SerializableTypeChecker.h>
#include <mylang/vast/InternalError.h>
#include <mylang/vast/ValidationError.h>
#include <mylang/vast/VAstTransform.h>
#include <mylang/vast/VAst.h>
#include <mylang/variables/Variable.h>
#include <parser.h>
#include <algorithm>
#include <cassert>
#include <cstddef>
#include <functional>
#include <initializer_list>
#include <iostream>
#include <iterator>
#include <list>
#include <map>
#include <memory>
#include <optional>
#include <set>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

namespace mylang {
  namespace validation {
    namespace {
      static size_t MAX_GENERIC_RECURSION = 256;

      using namespace mylang::patterns;
      using namespace mylang::types;
      using namespace mylang::constraints;
      using mylang::variables::Variable;
      using mylang::vast::VAst;
      using mylang::vast::ValidationError;
      using mylang::vast::InternalError;
      typedef ::idioma::scope::Scope::Ptr ScopePtr;

      inline bool isSerializable(const ConstrainedType::Ptr &cty)
      {
        return ::mylang::types::SerializableTypeChecker::isSerializable(cty->type());
      }

      inline bool isExportable(const ConstrainedType::Ptr &cty)
      {
        auto ty = cty->type();

        // something other than a function must be exportable
        return mylang::types::ExportableTypeChecker::isExportable(ty);
      }

      static void append(::std::list<GroupNodeCPtr> &THIS, const GroupNodes &that)
      {
        THIS.insert(THIS.end(), that.begin(), that.end());
      }

      static void append(::std::list<GroupNodeCPtr> &THIS, const ::std::list<GroupNodeCPtr> &that)
      {
        THIS.insert(THIS.end(), that.begin(), that.end());
      }

      enum class Shadowing
      {
        // FIXME: DISALLOW_AND_CONTINUE should be the defaul to prevent issues due to values not having been added
        ALLOW,
        WARN,
        DISALLOW,
        DISALLOW_AND_CONTINUE // disallow shadowing, but add the definition anyways
      };

      enum class LocalRelation
      {
        SOURCE
      };

      enum class LocalNode
      {
        ERROR,
        NAMED_CONSTRAINT,
        TYPENAME,
        STRUCT_MEMBER,
        UNION_MEMBER,
        DISCRIMINANT,
        PROCESS_MEMBER,
        GENERIC_PARAMETER,
        DEF_TYPE_PATTERN,
        DEF_VALUE_PATTERN
      };

      template<class T = ConstrainedType>
      static ::std::shared_ptr<const T> constraintOf(const GroupNodeCPtr &g)
      {
        auto ty = getConstraintAnnotation(g);
        return ::std::dynamic_pointer_cast<const T>(ty);
      }

      template<class T = Pattern>
      static ::std::shared_ptr<const T> patternOf(const GroupNodeCPtr &g)
      {
        auto ty = getPatternAnnotation(g);
        return ::std::dynamic_pointer_cast<const T>(ty);
      }

      static bool hasMember(const TypePtr &ty, const ::std::string &member)
      {
        if (!ty) {
          return false;
        }

        auto unionTy = ty->self<UnionType>();
        if (unionTy) {
          return unionTy->getMemberType(member) || unionTy->discriminant.name == member;
        }
        auto structTy = ty->self<StructType>();
        if (structTy) {
          return structTy->getMemberType(member) != nullptr;
        }

        auto processTy = ty->self<ProcessType>();
        if (processTy) {
          return (processTy->inputs.count(member) != 0 || processTy->outputs.count(member) != 0);
        }
        return false;
      }

      static TypePtr rootType(const GroupNodeCPtr &g)
      {
        auto ty = constraintOf(g);
        while (ty) {
          auto b = ty->basetype();
          if (!b) {
            return ty->type();
          }
          ty = b;
        }
        return nullptr;
      }

      template<class T = mylang::types::Type>
      static ::std::shared_ptr<const T> typeOf(const GroupNodeCPtr &g)
      {
        auto ty = constraintOf(g);
        if (!ty) {
          return nullptr;
        }
        while (true) {
          if (ty->type()->self<T>()) {
            break;
          }
          auto b = ty->basetype();
          if (!b) {
            return nullptr;
          }
          ty = b;
        }

        return ::std::dynamic_pointer_cast<const T>(ty->type());
      }

      template<class T = mylang::types::Type>
      static ::std::shared_ptr<const T> exactTypeOf(const GroupNodeCPtr &g)
      {
        auto ty = constraintOf(g);
        if (!ty) {
          return nullptr;
        }
        return ::std::dynamic_pointer_cast<const T>(ty->type());
      }

      ::std::ostream& operator<<(::std::ostream &out, const TokenNodeCPtr &node)
      {
        if (node) {
          out << node->text;
        }
        return out;
      }

      /** An FQN */
      struct SimpleFQN
      {
        SimpleFQN()
        {
        }

        SimpleFQN(const SimpleFQN &parent, const ::std::string &local)
        {
          if (parent._name.empty()) {
            _name = local;
          } else {
            _name = parent._name + " " + local;
          }
        }

      public:
        const ::std::string& name() const
        {
          return _name;
        }

      public:
        friend bool operator==(const SimpleFQN &a, const SimpleFQN &b)
        {
          return a._name == b._name;
        }

      public:
        friend bool operator<(const SimpleFQN &a, const SimpleFQN &b)
        {
          return a._name < b._name;
        }

      private:
        ::std::string _name;
      };

      typedef ::std::shared_ptr<Definition> DefinitionPtr;

      struct GenericParameter
      {
        TokenNodeCPtr id;
        GroupNodeCPtr pattern;
      };

      struct LoopStatementContext: public StatementContext
      {
        LoopStatementContext(::std::shared_ptr<const StatementContext> xparent)
            : StatementContext(xparent)
        {
        }
        ~LoopStatementContext()
        {
        }
      };

      struct ProcessConstructorContext: public Definition
      {
        ProcessConstructorContext()
            : call_to_own_constructor(false)
        {
        }

        ~ProcessConstructorContext()
        {
        }

        // set to true if constructor makes a call to self
        bool call_to_own_constructor;
      };

      struct Converter;
      struct GenericInstantiationProvider
      {
        NamePtr key;
        TokenNodeCPtr defName;
        bool extension;
        ::std::vector<GenericParameter> parameters;
        GroupNodeCPtr returnType;
        GroupNodeCPtr body;
        ::std::unique_ptr<Converter> scope;
      };

      struct DefUnitContext: public Definition
      {
        DefUnitContext(const NamePtr &xname)
            : Definition(xname)
        {
        }
        ~DefUnitContext()
        {
        }

        static ::std::shared_ptr<DefUnitContext> makeContext(const NamePtr &xname,
            ScopePtr parentScope)
        {
          auto c = ::std::make_shared<DefUnitContext>(xname);
          c->scope = parentScope->create(DefinitionPtr { c });
          return c;
        }

        GroupNodeCPtr create_reference(const ScopePtr&, const TokenNodeCPtr&) const override
        {
          return nullptr;
        }

        ::std::shared_ptr<const Definition> find(const ::std::string &n) const
        {
          auto i = entries.find(n);
          if (i == entries.end()) {
            return nullptr;
          }
          return i->second;
        }

        ::std::shared_ptr<const Definition> find(const NamePtr &n) const
        {
          for (auto e : entries) {
            if (e.second->name == n) {
              return e.second;
            }
          }
          return nullptr;
        }

        void put(const ::std::string &n, const DefinitionPtr &definition)
        {
          entries[n] = definition;
        }

        ScopePtr getScope()
        {
          auto res = scope.lock();
          if (res) {
            return res;
          } else {
            throw InternalError("Scope is lost");
          }
        }

      private:
        ::std::map<::std::string, ::std::shared_ptr<const Definition>> entries;
        ::std::weak_ptr<::idioma::scope::Scope> scope;
      };

      GroupNodeCPtr createApplyExpr(GroupNodeCPtr optionalDecl, GroupNodeCPtr optionalDeclRef,
          GroupNodeCPtr value, bool allowedNestedOpts = false)
      {
        VAst ast;

        GroupNodeCPtr iftrue;
        GroupNodeCPtr iffalse;

        if (getConstraintAnnotation(value)->self<ConstrainedVoidType>()) {
          // ok, we've got a void pointer
          iftrue = value;
          iffalse = ast.createVoidExpression();
        } else {
          iftrue = ast.createOptifyExpression(value);
          iffalse = ast.createLiteralNilExpression(value->span());

          // it makes sense to normalize and not allow double optified values
          // e;g; an optional union member deref would result in a double optified value
          // if the user doesn't like the behavior, they can just manually do this.
          if (!allowedNestedOpts) {
            iftrue = typeOf<OptType>(value) ? value : iftrue;
          }

        }
        GroupNodes decls( { optionalDecl });
        return ast.createWithExpression(decls,
            ast.createConditionalExpression(
                ast.createIsOptionalValuePresentExpression(optionalDeclRef), iftrue, iffalse));
      }

      struct VariableDefinition: public Definition
      {
        VariableDefinition(const GroupNodeCPtr &vdecl, const VariablePtr &xinfo,
            const ContainerContext::Container &fn)
            : Definition(getNameAnnotation(vdecl)), decl(vdecl), info(xinfo), containerContext(fn)
        {
        }

        ~VariableDefinition()
        {
        }

        bool hasInitialValue() const
        {
          return VAst::getExpr(decl, VAst::Expr::INITIALIZER) != nullptr;
        }

        GroupNodeCPtr create_reference(const ScopePtr&, const TokenNodeCPtr &src) const override
        {
          VAst ast;
          return ast.createVariableExpression(src, name, getConstraintAnnotation(decl));
        }

        const GroupNodeCPtr decl;
        const VariablePtr info;
        const ContainerContext::Container containerContext;
      };

      struct DefTypeContext: public Definition
      {

        DefTypeContext(const NamePtr &the_name)
            : Definition(the_name)
        {
        }

        ~DefTypeContext()
        {
        }

        ::std::shared_ptr<const ConstrainedNamedType> getType() const
        {
          auto THIS = self<DefTypeContext>();
          auto ty = ConstrainedNamedType::get(name,
              [=]() {return getConstraintAnnotation(THIS->getImplType());});
          return ty;
        }

        void setImplType(const GroupNodeCPtr &t)
        {
          if (implType) {
            throw ::std::runtime_error("type already defined");
          }
          implType = t;
        }

        GroupNodeCPtr getImplType() const
        {
          return implType;
        }

        /** A typename node */
      private:
        GroupNodeCPtr implType;
      };

      struct DefPatternContext: public Definition
      {

        DefPatternContext(const NamePtr &the_name)
            : Definition(the_name)
        {
        }

        ~DefPatternContext()
        {
        }

        void setPattern(const GroupNodeCPtr &p)
        {
          if (pattern) {
            throw ::std::runtime_error("pattern already defined");
          }
          pattern = p;
        }

        GroupNodeCPtr getPattern() const
        {
          return pattern;
        }

        /** A typename node */
      private:
        GroupNodeCPtr pattern;
      };

      struct DefAliasContext: public Definition
      {
        DefAliasContext(::std::shared_ptr<const Definition> the_alias)
            : alias(the_alias)
        {
        }

        ~DefAliasContext()
        {
        }

        const ::std::shared_ptr<const Definition> alias;
      };

      struct DefTypeAliasContext: public Definition
      {
        DefTypeAliasContext(const GroupNodeCPtr &the_alias)
            : alias(the_alias)
        {
        }

        ~DefTypeAliasContext()
        {
        }

        const GroupNodeCPtr alias;
      };

      struct DefProcessContext: public ContainerContext
      {
        DefProcessContext(const NamePtr &the_name, const ::idioma::ast::Node::Span &xspan)
            : ContainerContext(the_name), span(xspan)
        {
        }

        ~DefProcessContext()
        {
        }

      public:
        void createType()
        {
          VAst ast;
          _type = ast.createType(ConstrainedProcessType::get(inputs, outputs), span);
        }

        GroupNodeCPtr type() const
        {
          return _type;
        }

        bool hasConstructor(const ::std::vector<ConstrainedTypePtr> &argTypes) const
        {
          if (argTypes.empty() && constructors.empty()) {
            // default constructor always exists!!!
            return true;
          }
          // the result is always a void
          for (auto c : constructors) {
            auto sig = createSignature(VAst::getRelations(c, VAst::Relation::PARAMETERS));
            if (sig->parameters.size() == argTypes.size()) {
              bool matched = true;
              for (size_t i = 0; i < argTypes.size(); ++i) {
                if (!sig->parameters.at(i)->canImplicitlyCastFrom(*argTypes.at(i))) {
                  matched = false;
                  break;
                }
              }
              if (matched) {
                return true;
              }
            }
          }

          return false;
        }

        bool addConstructor(GroupNodeCPtr ctor)
        {
          auto parameters = VAst::getRelations(ctor, VAst::Relation::PARAMETERS);
          auto signature = createSignature(parameters);
          for (auto c : constructors) {
            auto params = VAst::getRelations(c, VAst::Relation::PARAMETERS);
            if (createSignature(params)->type()->isSameType(*signature->type())) {
              return false;
            }
          }
          constructors.push_back(ctor);
          return true;
        }

        void createInitializedDispositions(
            StatementContext::VariableDispositions &dispositions) const
        {
          for (auto v : unitializedStateVariables) {
            dispositions.setVariableDisposition(v->name,
                StatementContext::VariableDisposition::INITIALIZED);
          }
          for (auto v : unitializedStateValues) {
            dispositions.setValueDisposition(v->name,
                StatementContext::VariableDisposition::INITIALIZED);
          }
        }

      private:
        ::std::shared_ptr<const ConstrainedFunctionType> createSignature(
            const GroupNodes &parameters) const
        {
          ::std::vector<ConstrainedTypePtr> parameterTypes;
          for (auto p : parameters) {
            parameterTypes.push_back(getConstraintAnnotation(p));
          }
          // the result is always a void
          return ConstrainedFunctionType::get(ConstrainedVoidType::create(), parameterTypes);
        }

      public:
        GroupNodes constructors;

      private:
        GroupNodeCPtr _type;

      public:
        ConstrainedProcessType::Inputs inputs;
        ConstrainedProcessType::Outputs outputs;

        // the names of process values and variables
        ::std::vector<::std::shared_ptr<VariableDefinition>> unitializedStateValues;
        ::std::vector<::std::shared_ptr<VariableDefinition>> unitializedStateVariables;
        const ::idioma::ast::Node::Span span;
      };

      struct MacroDefinition: public Definition
      {
        MacroDefinition(TokenNodes the_params, const GroupNodeCPtr &the_body)
            : params(::std::move(the_params)), body(the_body)
        {
        }

        ~MacroDefinition()
        {
        }

        GroupNodeCPtr create_reference(const ScopePtr&, const TokenNodeCPtr&) const override
        {
          return nullptr;
        }

        GroupNodeCPtr create_call(const ScopePtr &scope, const SourceLocation &src,
            const GroupNodes &args, RegisterVariableFN registerVar, VisitFN visitFN) const
            override
        {
          if (args.size() != params.size()) {
            throw ValidationError(src,
                "cannot expand macro: number of arguments do match the macro definition");
          }
          VAst ast;

          GroupNodes variables;
          auto s = scope->create();
          auto vinfo = Variable::createLocal(false);
          for (size_t i = 0; i < params.size(); ++i) {
            auto varname = params.at(i)->text;
            auto gname = mylang::names::Name::create(varname);
            auto decl = ast.createDefineVariable(params.at(i), gname, vinfo, nullptr, args.at(i));
            registerVar(decl);

            DefinitionPtr v = ::std::make_shared<VariableDefinition>(decl, vinfo,
                nullptr /* TODO: is this right*/);
            // FIXME: we need to safely put this into the scope
            scope->put(varname, v);
            variables.push_back(decl);
          }
          auto expr = visitFN(scope, body);

          return ast.createWithExpression(variables, expr);
        }

        TokenNodes params;
        const GroupNodeCPtr body;
      };

      struct OptionalBuiltinDefinition: public Definition
      {
        OptionalBuiltinDefinition(const GroupNodeCPtr &xoptDecl, const GroupNodeCPtr &xoptDeclRef,
            const ::std::shared_ptr<const Definition> &xInner)
            : optDecl(xoptDecl), optDeclRef(xoptDeclRef), functionLike(xInner)
        {

        }

        ~OptionalBuiltinDefinition()
        {
        }

        GroupNodeCPtr create_call(const ScopePtr &scope, const SourceLocation &src,
            const GroupNodes &args, RegisterVariableFN registerFN, VisitFN visitFN) const override
        {
          auto expr = functionLike->create_call(scope, src, args, registerFN, visitFN);
          return createApplyExpr(optDecl, optDeclRef, expr);
        }

        const GroupNodeCPtr optDecl;
        const GroupNodeCPtr optDeclRef;
        const ::std::shared_ptr<const Definition> functionLike;
      };

      static GroupNodePtr createError(const NodeCPtr &source, const ConstrainedTypePtr &ty)
      {
        auto res = GroupNode::create(LocalNode::ERROR, { { LocalRelation::SOURCE, source } });
        setTypeAnnotation(res, ty->type());
        setConstraintAnnotation(res, ty);
        return res;
      }

      GroupNodePtr createTypeProcessMember(TokenNodeCPtr name, GroupNodeCPtr type)
      {
        auto ty = constraintOf(type);
        auto res = GroupNode::create(LocalNode::PROCESS_MEMBER, { { VAst::Token::IDENTIFIER, name },
            { VAst::Relation::TYPE, type } });
        setConstraintAnnotation(res, ty);
        return res;
      }

      GroupNodePtr createTypeStructMember(TokenNodeCPtr name, GroupNodeCPtr type)
      {
        if (!getConstraintAnnotation(type)->isConcrete()) {
          throw ValidationError(type, "struct member must have a concrete type");
        }
        auto ty = constraintOf(type);
        auto res = GroupNode::create(LocalNode::STRUCT_MEMBER, { { VAst::Token::IDENTIFIER, name },
            { VAst::Relation::TYPE, type } });
        setConstraintAnnotation(res, ty);
        return res;
      }

      GroupNodePtr createTypeUnionDiscriminant(TokenNodeCPtr name, GroupNodeCPtr type)
      {
        auto ty = constraintOf(type);
        if (!ty->isConcrete()) {
          throw ValidationError(type, "union discriminant must have a concrete type");
        }
        if (!isSerializable(ty)) {
          throw ValidationError(type, "union discriminant must have a concrete serializable type");
        }

        auto res = GroupNode::create(LocalNode::DISCRIMINANT, { { VAst::Token::IDENTIFIER, name }, {
            VAst::Relation::TYPE, type } });
        setConstraintAnnotation(res, ty);
        return res;
      }

      GroupNodePtr createTypeUnionMember(GroupNodeCPtr discriminantValue, TokenNodeCPtr name,
          GroupNodeCPtr type)
      {
        if (!getConstraintAnnotation(type)->isConcrete()) {
          throw ValidationError(type, "union member must have a concrete type");
        }
        if (discriminantValue && !getConstAnnotation(discriminantValue)) {
          throw ValidationError(discriminantValue,
              "discriminant value must be a compile-time constant");
        }

        auto ty = constraintOf(type);
        auto res = GroupNode::create(LocalNode::UNION_MEMBER,
            { { VAst::Expr::EXPRESSION, discriminantValue }, { VAst::Token::IDENTIFIER, name }, {
                VAst::Relation::TYPE, type } });
        setConstraintAnnotation(res, ty);
        return res;
      }

      GroupNodePtr createNamedConstraint(TokenNodeCPtr name, GroupNodeCPtr expr)
      {
        if (!getConstAnnotation(expr)) {
          throw ValidationError(expr, "constraint must be compile-time constant");
        }
        auto ty = constraintOf(expr);
        auto res = GroupNode::create(LocalNode::NAMED_CONSTRAINT, {
            { VAst::Token::IDENTIFIER, name }, { VAst::Expr::EXPRESSION, expr } });
        setConstraintAnnotation(res, ty);
        return res;
      }

      struct Converter: public mylang::grammar::PAst::Visitor, public FeedbackBase
      {

        /** Tokens can be just be copied; no need to visit them */
        static TokenNodeCPtr getToken(const GroupNodeCPtr &g,
            const mylang::grammar::PAst::RelationType &token)
        {
          return ::idioma::astutils::getToken(g, token);
        }

        static ::std::vector<TokenNodeCPtr> getTokens(const GroupNodeCPtr &g,
            const mylang::grammar::PAst::RelationType &token)
        {
          return ::idioma::astutils::getTokens(g, token);
        }

        /** The current scope */
        ScopePtr scope;

        /** A typed ast */
        mutable VAst ast;

        /** The builtins to use */
        ::std::shared_ptr<const Builtins> builtins;

        /** The builtins to use */
        ::std::shared_ptr<const Library> library;

        /** The current unit context */
        ::std::shared_ptr<DefUnitContext> current_unit;

        /** The current unit context */
        ::std::shared_ptr<std::map<NamePtr, DefinitionPtr>> definitionsByName;

        /** The current unit context */
        ::std::shared_ptr<std::map<mylang::names::FQN, DefinitionPtr>> definitionsByFQN;

        /** The generics */
        ::std::shared_ptr<::std::map<NamePtr, ::std::shared_ptr<GenericInstantiationProvider>>> generics;

        /** The variables in declaration order */
        GroupNodes declaredVariables;

        /** The search context */
        ::std::shared_ptr<mylang::filesystem::FileSystem> externalContext;

        /** The files that have been imported */
        ::std::shared_ptr<std::set<mylang::names::FQN>> importedFiles;

        /** The calls to transforms (VAst nodes) */
        TransformResolver::Ptr transformResolver;

        /**
         * Create a new scope.
         */
        Converter(const FeedbackHandler &fb, ::std::shared_ptr<mylang::filesystem::FileSystem> sc,
            const ScopePtr &s, ::std::shared_ptr<const Builtins> the_builtins,
            ::std::shared_ptr<const Library> the_library,
            ::std::shared_ptr<std::map<NamePtr, DefinitionPtr>> all_defs,
            ::std::shared_ptr<std::map<mylang::names::FQN, DefinitionPtr>> all_defs_by_FQN,
            ::std::shared_ptr<std::map<NamePtr, ::std::shared_ptr<GenericInstantiationProvider>>> all_generics,
            ::std::shared_ptr<std::set<mylang::names::FQN>> imported,
            TransformResolver::Ptr transformer_resolver)
            : FeedbackBase(fb), scope(s->create()), builtins(the_builtins), library(the_library),
                definitionsByName(all_defs), definitionsByFQN(all_defs_by_FQN),
                generics(all_generics), externalContext(sc), importedFiles(imported),
                transformResolver(transformer_resolver)
        {
        }

        Converter(const Converter &p, const ScopePtr &s)
            : FeedbackBase(p), scope(s), builtins(p.builtins), library(p.library),
                definitionsByName(p.definitionsByName), definitionsByFQN(p.definitionsByFQN),
                generics(p.generics), externalContext(p.externalContext),
                importedFiles(p.importedFiles), transformResolver(p.transformResolver)
        {
          auto defPtr = s->getContext<DefinitionPtr>().first;
          if (defPtr) {
            auto value = defPtr->value();
            current_unit = ::std::dynamic_pointer_cast<DefUnitContext>(value);
          }
        }

        Converter(const Converter &p, const ::std::shared_ptr<Definition> &the_context)
            : Converter(p, p.scope->create(the_context))
        {
          if (the_context->self<DefUnitContext>()) {
            assert(current_unit == the_context && "Expected a def-unit context");
          }
        }

        Converter(const Converter &p)
            : Converter(p, p.scope->create())
        {
        }

        ~Converter()
        {
        }

        GroupNodeCPtr instantiateFunction(const GenericInstantiationRequest &req) const
        {
          auto it = generics->find(req.providerKey);
          if (it == generics->end()) {
            throw ValidationError(req.sloc, "cannot instantiate template without a definition");
          }
          const GenericInstantiationProvider &provider = *(it->second);

          auto statementCtx = ::std::make_shared<StatementContext>(nullptr);
          return provider.scope->withNewScope(statementCtx, [&](Converter &conv) {
            GroupNodeCPtr res;
            MatchContext::Ptr ctx = req.initialMatchContext;

            auto vinfo = Variable::createParameter();
            GroupNodes parameters;
            for (size_t i = 0; i < req.argumentTypes.size(); ++i) {
              auto p = provider.parameters.at(i);
              auto pattern = patternOf(p.pattern);
              ctx = pattern->match(req.argumentTypes.at(i), ctx);
              if (!ctx) {
                // should have already match correctly!
              notifyInternalError(req.sloc, stream
                  << "could not determine type for generic parameter "
                  << p.id);
              return res;
            }
          }

          for (auto p : provider.parameters) {
            auto pattern = patternOf(p.pattern);
            auto matched_type = pattern->createType(ctx);
            ctx = matched_type.first; // we may have update the context as part of the type creation
            auto ty = matched_type.second;
            if (!ty) {
              notifyError(req.sloc, stream
                  << "could not instantiate type for parameter "
                  << p.id);
              return res;
            }
            auto parameter = conv.define_variable_or_value(p.id,
                ast.createType(
                    ty,
                    p.pattern->span()),
                nullptr,
                vinfo);
            parameters.push_back(parameter);
          }

          GroupNodeCPtr retTy;
          if (provider.returnType) {
            auto pattern = patternOf(provider.returnType);
            auto matched_type = pattern->createType(ctx);
            ctx = matched_type.first; // we may have update the context as part of the type creation
            auto ty = matched_type.second;
            if (!ty) {
              notifyError(provider.returnType, stream
                  << "could not instantiate return type ");
              return res;
            }
            retTy = ast.createType(ty, provider.returnType->span());
          }
          if (provider.extension) {
            auto paramTy = constraintOf(parameters.at(0));
            if (hasMember(paramTy->roottype()->type(),
                    provider.defName->text)) {
              notifyError(provider.defName,
                  stream << "extension "
                  << provider.defName
                  << " conflicts with a member of type "
                  << paramTy);
            } else if (conv.builtins->hasInstanceBuiltin(paramTy,
                    provider.defName->text)) {
              notifyError(provider.defName,
                  stream << "extension "
                  << provider.defName
                  << " conflicts with a builtin definition of type "
                  << paramTy);
            } else if (conv.library->hasInstanceBuiltin(paramTy,
                    provider.defName->text)) {
              notifyError(provider.defName,
                  stream << "extension "
                  << provider.defName
                  << " conflicts with a library definition of type "
                  << paramTy);
            }
          }

          // need to create a new scope and add the typenames in
          for (auto kv : req.names) {
            auto ty = ctx->findType(kv.second);
            if (ty) {
              auto node = ast.createType(ty, kv.first->span());
              auto def = ::std::make_shared<DefTypeAliasContext>(
                  node);
              conv.putDefinition(kv.first, def, Shadowing::ALLOW);
            } else {
              conv.notifyError(provider.defName,
                  stream
                  << "no type for generic parameter "
                  << kv.first);
            }
          }

          auto localConv = Converter(conv);
          auto body = localConv.visit(provider.body);
          if (statementCtx->getReturnDisposition() ==
              StatementContext::ReturnDisposition::MAY_RETURN) {
            if (statementCtx->getThrowDisposition() !=
                StatementContext::ThrowDisposition::WILL_THROW) {

              // FIXME: what about break and continue??

              notifyError(provider.body,
                  stream << "missing return");
            }
          }

          auto instance = ast.createDefineFunction(req.instanceName,
              getNameAnnotation(
                  req.instanceName),
              req.vinfo,
              retTy, parameters,
              body);
          res = ast.createInstantiateGenericFunction(provider.defName,
              req.instanceName,
              instance);
          return res;
        });
        }

        /**
         * Unescape the characters in the specified string. The string is expected to be
         * surrounded by matching characters, which are not part of the string contents.
         */
        ::std::string unescape_chars(NodeCPtr src, const ::std::string &chars)
        {
          size_t n = chars.size();
          if (n < 2) {
            notifyError(src, stream << "expected at least 2 characters in text literal: " << chars);
            return "";
          }
          std::string res;
          for (size_t i = 1; i < n - 1; ++i) {
            char ch = chars[i];
            if (ch == '\\') {
              ch = chars[++i];
              switch (ch) {
              case '0':
                ch = '\0';
                break;
              case 'a':
                ch = '\a';
                break;
              case 'b':
                ch = '\b';
                break;
              case 't':
                ch = '\t';
                break;
              case 'n':
                ch = '\n';
                break;
              case 'v':
                ch = '\v';
                break;
              case 'f':
                ch = '\f';
                break;
              case 'r':
                ch = '\r';
                break;
              case '"':
                ch = '"';
                break;
              case '\'':
                ch = '\'';
                break;
              case '?':
                ch = '\?';
                break;
              case '\\':
                ch = '\\';
                break;
                // unicode
              case 'u':
              case 'U': {
                size_t len = ch == 'u' ? 4 : 8;
                ::std::string ucode;
                for (size_t j = 0; j < len && i < n; ++j) {
                  ucode += chars[++i];
                }
                try {
                  size_t end;
                  unsigned long code = ::std::stoul(ucode, &end, 16);
                  if (end != ucode.length()) {
                    throw ::std::runtime_error("invalid escape sequence");
                  }
                  if (code < 0x80) {
                    ch = (char) code;
                  } else {
                    notifyWarning(src,
                        stream
                            << "unicode escape sequences above 007f are unsupported; replace with ~");
                    ch = '~';
                  }
                } catch (...) {
                  throw ValidationError(src, "invalid unicode escape sequence");
                }
                break;
              }
              default:
                throw ValidationError(src, "unexpected escape sequence \\" + ch);
              }
            }
            res += ch;
          }
          return res;
        }

        StatementContext::VariableDisposition getValueDisposition(
            const ::std::shared_ptr<const StatementContext> &ctx, NamePtr n) const
        {
          StatementContext::VariableDisposition res = ctx->getValueDisposition(n);
          if (res == StatementContext::VariableDisposition::INITIALIZED) {
            return res;
          }

          // if it's a variable or value, make sure it has been initialized
          auto vardef = searchDefinition<VariableDefinition>(n);
          if (vardef && vardef->info->scope == Variable::SCOPE_PROCESS) {
            auto pCtor = searchContext<ProcessConstructorContext>();
            if (pCtor == nullptr || pCtor->call_to_own_constructor) {
              return StatementContext::VariableDisposition::INITIALIZED;
            }
          }
          return res;
        }

        StatementContext::VariableDisposition getVariableDisposition(
            const ::std::shared_ptr<const StatementContext> &ctx, NamePtr n) const
        {
          StatementContext::VariableDisposition res = ctx->getVariableDisposition(n);
          if (res == StatementContext::VariableDisposition::INITIALIZED) {
            return res;
          }

          // if it's a variable or value, make sure it has been initialized
          auto vardef = searchDefinition<VariableDefinition>(n);
          if (vardef && vardef->info->scope == Variable::SCOPE_PROCESS) {
            auto pCtor = searchContext<ProcessConstructorContext>();
            if (pCtor == nullptr || pCtor->call_to_own_constructor) {
              return StatementContext::VariableDisposition::INITIALIZED;
            }
          }
          return res;
        }

        /**
         * Determine the current context is a global context.
         */
        bool isGlobalContext() const
        {
          auto res = scope->findContext<DefinitionPtr>();
          if (!res.second) {
            return true;
          }
          return ::std::dynamic_pointer_cast<DefUnitContext>(res.first->value()) != nullptr;
        }

        template<class T = Definition>
        ::std::shared_ptr<T> currentContext() const
        {
          auto res = scope->findContext<DefinitionPtr>();
          if (!res.second) {
            return nullptr; // no such context
          }
          return ::std::dynamic_pointer_cast<T>(res.first->value());
        }

        template<class T = Definition>
        ::std::shared_ptr<T> parentContext() const
        {
          auto curCtxScope = scope->findContextScope<DefinitionPtr>();
          if (!curCtxScope || !curCtxScope->parent()) {
            return nullptr;
          }
          auto res = curCtxScope->parent()->findContext<DefinitionPtr>();
          if (!res.second) {
            return nullptr; // no such context
          }
          return ::std::dynamic_pointer_cast<T>(res.first->value());
        }

        template<class T = Definition>
        ::std::shared_ptr<T> searchContext(bool stopAtContainer = false) const
        {
          auto S = scope;
          while (S) {
            S = S->findContextScope<DefinitionPtr>();
            if (!S) {
              return nullptr;
            }
            auto def = S->getContext<DefinitionPtr>().first->value();
            auto ctx = ::std::dynamic_pointer_cast<T>(def);
            if (ctx) {
              return ctx;
            }
            if (stopAtContainer && ::std::dynamic_pointer_cast<ContainerContext>(def)) {
              // we need to stop at the container boundary
              return nullptr;
            }
            S = S->parent();
          }
          return nullptr;
        }

        ContainerContext::Container containerContext() const
        {
          return searchContext<ContainerContext>();
        }

        NamePtr createContextName(const ::std::string &text)
        {
          auto ctx = currentContext();
          if (!ctx || ::std::dynamic_pointer_cast<const DefUnitContext>(ctx)) {
            return mylang::names::Name::createFQN(text, ctx ? ctx->name : nullptr);
          }
          return mylang::names::Name::create(text, ctx ? ctx->name : nullptr);
        }

        /**
         * Unalias a definition.
         * @param def a definition
         * @return an unalias definition
         */
        ::std::shared_ptr<const Definition> unalias(::std::shared_ptr<const Definition> def) const
        {
          auto alias = ::std::dynamic_pointer_cast<const DefAliasContext>(def);
          if (alias) {
            def = alias->alias;
          }
          return def;
        }

        /**
         * Search for the definition of a name in the scope.
         * @param name the name to search for
         * @return def context that is associated with the name
         */
        template<class T = Definition>
        ::std::shared_ptr<T> searchDefinition(const NamePtr &name) const
        {
          auto i = definitionsByName->find(name);
          if (i == definitionsByName->end()) {
            return nullptr;
          }
          return ::std::dynamic_pointer_cast<T>(i->second);
        }

        /**
         * Search for the definition of a name in the scope.
         * @param name the name to search for
         * @return def context that is associated with the name
         */
        DefinitionPtr findLocalDefinition(const TokenNodeCPtr &token) const
        {
          auto entry = scope->find(token->text);
          if (!entry.second) {
            return nullptr;
          }
          auto result = entry.first.cast<DefinitionPtr>();
          if (!result) {
            notifyInternalError(token, stream << "not a context pointer");
          }
          return result->value();
        }

        /**
         * Search for the definition of a name in the scope.
         * @param name the name to search for
         * @return def context that is associated with the name
         */
        template<class T = Definition>
        ::std::shared_ptr<const T> searchDefinition(const std::string &name) const
        {
          auto entry = scope->lookup(name);
          if (!entry.second) {
            return nullptr;
          }
          auto result = entry.first.cast<DefinitionPtr>();
          if (!result) {
            notifyInternalError(SOURCELOCATION, stream << "not a context pointer");
          }
          auto def = unalias(result->value());
          return ::std::dynamic_pointer_cast<const T>(def);
        }

        ::std::shared_ptr<const Definition> findDefinition(const TokenNodeCPtr &token) const
        {
          auto def = searchDefinition(token->text);
          if (!def) {
            throw ValidationError(token, "no such name: " + token->text);
          }
          return def;
        }

        ::std::shared_ptr<const Definition> findDefinition(const TokenNodes &names) const
        {
          ::std::shared_ptr<const Definition> def = findDefinition(names.at(0));

          for (size_t i = 1; i < names.size(); ++i) {
            def = def->find(names.at(i)->text);
            if (!def) {
              throw ValidationError(names.at(i), "name not found " + names.at(i)->text);
            }
            def = unalias(def);
          }
          return def;
        }

        ::std::shared_ptr<const Definition> findDefinition(const NodeCPtr &node,
            const NamePtr &fqn) const
        {
          auto segments = fqn->segments();
          auto def = searchDefinition(segments.at(0)->localName());
          if (!def) {
            throw ValidationError(node, "name not found " + fqn->fullName());
          }
          for (size_t i = 1; i < segments.size(); ++i) {
            def = def->find(segments.at(i)->localName());
            if (!def) {
              throw ValidationError(node, "name not found " + fqn->fullName());
            }
            def = unalias(def);
          }
          return def;
        }

        bool unsafePutDefinition(const NodeCPtr &src, const ::std::string &name,
            const DefinitionPtr &definition)
        {
          bool ok = scope->put(name, definition);
          if (!ok) {
            notifyInternalError(src, stream << "name already defined in scope " << name);
          }
          if (current_unit) {
            current_unit->put(name, definition);
          }
          definitionsByName->emplace(definition->name, definition);
          auto fqn = definition->name->fqn();
          if (fqn) {
            definitionsByFQN->emplace(*fqn, definition);
          }
          return ok;
        }

        bool putDefinition(const TokenNodeCPtr &token, const DefinitionPtr &definition,
            Shadowing shadowing)
        {
          // we never allow definitions for the same name in the local scope!
          // shadowing refers to names in a different scope
          ::std::shared_ptr<const Definition> def = findLocalDefinition(token);
          if (def) {
            notifyError(token,
                stream << "name already bound in the current scope: " << token->text);
            return false;
          }

          // if shadowing is not allowed, look for the original definition
          // if the original definition is a unit, then ignore the shadowing
          if (shadowing != Shadowing::ALLOW) {
            def = searchDefinition(token->text);
          }

          if (def) {
            // TODO: be even more generic; e.g. TYPE and VARIABLE can shadow each other as well
            if (nullptr == ::std::dynamic_pointer_cast<const DefUnitContext>(def)) {
              if (shadowing == Shadowing::WARN) {
                notifyWarning(token, stream << "name shadows another definition: " << token->text);
              } else {
                notifyError(token,
                    stream << "name already bound in an enclosing scope: " << token->text);
              }
            }
          }

          return unsafePutDefinition(token, token->text, definition);
        }

        ::std::pair<GroupNodeCPtr, GroupNodeCPtr> createTemporary(const GroupNodeCPtr &value)
        {
          auto vinfo = Variable::createLocal(false);
          NamePtr varname = mylang::names::Name::create("tmp");
          auto tok = TokenNode::create(varname->localName(), value->span());
          auto def = ast.createDefineVariable(tok, varname, vinfo, nullptr, value);
          declaredVariables.push_back(def);
          auto ref = ast.createVariableExpression(tok, getNameAnnotation(def),
              getConstraintAnnotation(def));
          return {def, ref};
        }

        void notifyValidationError(const ValidationError &err) const
        {
          notifyError(err.source, stream << err.message);
        }

        GroupNodeCPtr withNewScope(const ::std::shared_ptr<Definition> &the_context,
            ::std::function<GroupNodeCPtr(Converter&)> fn) const
        {
          GroupNodeCPtr res;
          Converter local(*this, the_context);
          res = fn(local);
          return res;
        }

        GroupNodeCPtr withNewScope(::std::vector<::std::shared_ptr<Definition>> contexts,
            ::std::function<GroupNodeCPtr(Converter&)> fn)
        {
          if (contexts.empty()) {
            return fn(*this);
          }
          Converter local(*this, contexts.front());
          contexts.erase(contexts.begin(), contexts.begin() + 1);
          return local.withNewScope(contexts, fn);
        }

        GroupNodeCPtr withNewScope(ScopePtr s, ::std::function<GroupNodeCPtr(Converter&)> fn) const
        {
          GroupNodeCPtr res;
          Converter local(*this, s);
          res = fn(local);
          return res;
        }

        GroupNodeCPtr withNewScope(::std::function<GroupNodeCPtr(Converter&)> fn) const
        {
          GroupNodeCPtr res;
          Converter local(*this);
          res = fn(local);
          return res;
        }

        GroupNodeCPtr withNewScope(const GroupNodeCPtr &g) const
        {
          if (g) {
            return withNewScope([&](Converter &local) {
              return local.visit(g);
            });
          }
          return nullptr;
        }

        GroupNodeCPtr withNewScope(const GroupNodeCPtr &g,
            const mylang::grammar::PAst::RelationType &rel) const
        {
          auto c = idioma::astutils::getGroup(g, rel);
          return withNewScope(c);
        }

        GroupNodeCPtr visitGroup(const GroupNodeCPtr &g,
            const mylang::grammar::PAst::RelationType &rel)
        {
          auto c = idioma::astutils::getGroup(g, rel);
          if (c) {
            return visit(c);
          }
          return nullptr;
        }

        GroupNodeCPtr visitRequiredGroup(const GroupNodeCPtr &g,
            const mylang::grammar::PAst::RelationType &rel, const ::std::string &message,
            const GroupNodeCPtr &Definition = nullptr)
        {
          auto c = visitGroup(g, rel);
          if (c) {
            return c;
          }
          notifyError(g, stream << message);
          return Definition ? Definition : c;
        }

        GroupNodeCPtr visitOptionalGroup(const GroupNodeCPtr &g,
            const mylang::grammar::PAst::RelationType &rel, const GroupNodeCPtr &def = nullptr)
        {
          auto c = visitGroup(g, rel);
          if (c) {
            return c;
          }
          return def ? def : c;
        }

        GroupNodes visitGroups(const GroupNodes &nodes, bool newScope = false)
        {
          GroupNodes res;
          for (auto c : nodes) {
            GroupNodeCPtr resGroup;
            try {
              resGroup = newScope ? withNewScope(c) : visit(c);
            } catch (const ValidationError &e) {
              notifyValidationError(e);
            }
            if (resGroup) {
              res.push_back(resGroup);
            }
          }
          return res;
        }

        GroupNodes visitGroups(const GroupNodeCPtr &g,
            const mylang::grammar::PAst::RelationType &rel, bool newScope = false)
        {
          return visitGroups(::idioma::astutils::getGroups(g, rel), newScope);
        }

        GroupNodeCPtr visitExpression(const GroupNodeCPtr &c)
        {
          if (!c) {
            return nullptr;
          }

          auto res = visit(c);
          if (!res || !getConstraintAnnotation(res)) {
            throw ValidationError(c, "missing type information on expression");
          }
          return res;
        }

        GroupNodeCPtr visitExpression(const GroupNodeCPtr &g,
            mylang::grammar::PAst::RelationType rel)
        {
          return visitExpression(::idioma::astutils::getGroup(g, rel));
        }

        GroupNodes visitExpressions(const GroupNodeCPtr &g, mylang::grammar::PAst::RelationType rel)
        {
          auto gg = visitGroups(g, rel);
          for (auto c : gg) {
            if (!getConstraintAnnotation(c)) {
              throw ValidationError(c, "missing type information on expression");
            }
          }
          return gg;
        }

        GroupNodes visitStatements(const GroupNodeCPtr &g, mylang::grammar::PAst::RelationType rel)
        {
          GroupNodes statements;
          auto groups = ::idioma::astutils::getGroups(g, rel);
          auto ctx = searchContext<StatementContext>();
          if (!ctx) {
            notifyInternalError(g, stream << "missing statement context");
            return statements;
          }

          for (auto gg : groups) {
            bool isTerminalStatement = ctx->getReturnDisposition()
                == StatementContext::ReturnDisposition::WILL_RETURN
                || ctx->getThrowDisposition() == StatementContext::ThrowDisposition::WILL_THROW
                || ctx->getBreakDisposition() == StatementContext::BreakDisposition::WILL_BREAK
                || ctx->getContinueDisposition()
                    == StatementContext::ContinueDisposition::WILL_CONTINUE;

            if (isTerminalStatement) {
              notifyError(gg, stream << "unreachable statement");
              break;
            }
            auto stmt = visit(gg);
            if (stmt) {
              statements.push_back(stmt);
            }
          }
          return statements;
        }

        GroupNodeCPtr visitPattern(GroupNodeCPtr c)
        {
          if (!c) {
            return nullptr;
          }
          auto res = visit(c);
          if (!res || !getPatternAnnotation(res)) {
            throw ValidationError(c, "missing pattern information");
          }
          return res;
        }

        GroupNodeCPtr visitPattern(const GroupNodeCPtr &g, mylang::grammar::PAst::RelationType rel)
        {
          return visitPattern(idioma::astutils::getGroup(g, rel));
        }

        GroupNodes visitPatterns(const GroupNodeCPtr &g, mylang::grammar::PAst::RelationType rel)
        {
          auto gg = visitGroups(g, rel);
          for (auto c : gg) {
            if (!getPatternAnnotation(c)) {
              throw ValidationError(c, "missing pattern information on expression");
            }
          }
          return gg;
        }

        GroupNodeCPtr visitType(const GroupNodeCPtr &c)
        {
          if (!c) {
            return nullptr;
          }
          auto res = visit(c);
          if (!res || !getTypeAnnotation(res)) {
            throw ValidationError(c, "missing type information");
          }
          return res;
        }

        GroupNodeCPtr visitType(const GroupNodeCPtr &g, mylang::grammar::PAst::RelationType rel)
        {
          return visitType(idioma::astutils::getGroup(g, rel));
        }

        GroupNodes visitTypes(const GroupNodeCPtr &g, mylang::grammar::PAst::RelationType rel)
        {
          auto gg = visitGroups(g, rel);
          for (auto c : gg) {
            if (!getTypeAnnotation(c)) {
              throw ValidationError(c, "missing type information");
            }
          }
          return gg;
        }

        //////////////////////////////////////////////////////////////////////////////////////
        ////// OVERRIDES
        //////////////////////////////////////////////////////////////////////////////////////
        GroupNodeCPtr visit_syntax_error(const GroupNodeCPtr &node)
        override
        {
          notifyError(node, stream << "syntax error");
          return nullptr;
        }

        NodeCPtr parseStream(::std::istream &in, const ::std::string &file)
        {
          auto parser = mylang::grammar::parser::createParser();
          auto parsedAst = parser->parseStream(in, file);
          if (parsedAst) {
            auto parsed_ast = parsedAst->toTree();
            return parsed_ast->removeIgnorables();
          }
          return nullptr;
        }

        ::std::vector<::std::string> getDeclName(GroupNodeCPtr decl)
        {
          auto tok = idioma::astutils::getToken(decl, mylang::grammar::PAst::TOK_IDENTIFIER);
          if (tok) {
            return {tok->text};
          }
          auto toks = idioma::astutils::getTokens(decl, mylang::grammar::PAst::REL_QN);
          ::std::vector<::std::string> res;
          for (const auto &t : toks) {
            res.push_back(t->text);
          }
          return res;
        }

        ::std::list<GroupNodeCPtr> importObject(const GroupNodeCPtr &node,
            const mylang::names::FQN &fqn)
        {
          ::std::list<GroupNodeCPtr> res;

          if (importedFiles->insert(fqn).second == false) {
            // already imported, so ignore it
            return res;
          }

          mylang::filesystem::FileSystem::Details fileDetails;
          ::std::unique_ptr<::std::istream> in;
          try {
            in = externalContext->open(fqn, &fileDetails);
            if (in == nullptr) {
              notifyError(node, stream << "FQN " << fqn << " could not be resolved for import");
              return res;
            }
          } catch (const mylang::filesystem::FileSystem::AmbiguousFQN &e) {
            notifyError(node, stream << "Could not import ambiguous FQN " << fqn);
            return res;
          } catch (const ::std::exception &e) {
            notifyError(node,
                stream << "Failed to import FQN " << fqn << " due to unexpected I/O exception: "
                    << e.what());
            return res;
          }

          auto parsed_ast = parseStream(*in, fileDetails.identifier);
          in.release();

          if (parsed_ast) {

            // we're expecting only a single decls and it needs to match the name of the import
            auto topLevelDecls = idioma::astutils::getGroups(parsed_ast->self<GroupNode>(),
                mylang::grammar::PAst::REL_DECLS);
            if (topLevelDecls.size() != 1) {
              notifyError(node,
                  stream << "imported file must have exactly one top-level declaration " << fqn);
            } else {
              auto segments = fqn.localComponents();
              auto decls = topLevelDecls;
              // check internal segments
              for (size_t i = 0; i < segments.size();) {
                if (decls.size() != 1) {
                  notifyError(node, stream << "too many declarations for import " << fqn);
                  break;
                }
                std::vector<::std::string> declName = getDeclName(decls.at(0));
                for (size_t j = 0; j < declName.size(); ++j) {
                  if (i >= segments.size()) {
                    notifyError(node, stream << "invalid declaration imported");
                    return {};
                  }
                  if (segments.at(i) != declName.at(j)) {
                    notifyError(node,
                        stream << "unexpected name; expected " << segments.at(i) << ", but found "
                            << declName.at(j));
                    return {};
                  }
                  ++i;
                }

                decls = idioma::astutils::getGroups(decls.at(0), mylang::grammar::PAst::REL_DECLS);
              }
            }

            // recursively resolve all imports now
            append(res,
                loadImports(
                    idioma::astutils::getGroups(parsed_ast->self<GroupNode>(),
                        mylang::grammar::PAst::REL_IMPORTS)));

            append(res, ::std::move(topLevelDecls));
          } else {
            notifyError(node, stream << "Could not import " << fqn);
          }
          return res;
        }

        ::std::list<GroupNodeCPtr> loadImport(const GroupNodeCPtr &import)
        {
          ::std::list<GroupNodeCPtr> res;
          ::std::vector<::std::string> pathComponents;

          try {
            for (auto pc : getTokens(import, mylang::grammar::PAst::REL_QN)) {
              pathComponents.push_back(pc->text);
            }
            assert(pathComponents.size() != 0);
          } catch (const ::std::exception &ex) {
            notifyError(import, stream << "Invalid name");
            return {};
          }
          auto fqn = mylang::names::FQN::create(pathComponents);
          return importObject(import, fqn);
        }

        ::std::list<GroupNodeCPtr> loadImports(const GroupNodes &imports)
        {
          ::std::list<GroupNodeCPtr> res;
          for (auto import : imports) {
            append(res, loadImport(import));
          }
          return res;
        }

        GroupNodeCPtr visit_root(const GroupNodeCPtr &node)
        override
        {
          auto decls = loadImports(
              idioma::astutils::getGroups(node, mylang::grammar::PAst::REL_IMPORTS));
          append(decls, idioma::astutils::getGroups(node, mylang::grammar::PAst::REL_DECLS));

          // process any children that exist
          GroupNodes content = visitGroups(GroupNodes(decls.begin(), decls.end()));
          // we need to resolve any transform calls and load up the transforms as we seem them

          if (transformResolver) {
            ::std::list<GroupNodeCPtr> importedDecls;
            for (auto fqn : transformResolver->getRequiredImports()) {
              append(importedDecls, importObject(node, fqn));
            }
            GroupNodes transforms = visitGroups(
                GroupNodes(importedDecls.begin(), importedDecls.end()));
            content.insert(content.end(), transforms.begin(), transforms.end());
          }
          // TODO: generics cause problems if they have transforms

          return ast.createRoot(content);
        }

        GroupNodeCPtr visit_import(const GroupNodeCPtr&)
        override
        {
          return nullptr;
        }

        GroupNodeCPtr visit_literal_bit_sequence(const GroupNodeCPtr &node)
        override
        {
          /* we should never actually get here due to the grammar */
          throw ValidationError(node, "unexpected bit sequence found");
        }

        GroupNodeCPtr visit_literal_byte_sequence(const GroupNodeCPtr &node)
        override
        {
          /* we should never actually get here due to the grammar */
          throw ValidationError(node, "unexpected byte sequence found");
        }

        GroupNodeCPtr visit_literal_bit(const GroupNodeCPtr &node)
        override
        {
          auto literal = getToken(node, mylang::grammar::PAst::TOK_LITERAL);
          auto res = ast.createLiteralBitExpression(literal);
          return res;
        }

        GroupNodeCPtr visit_literal_boolean(const GroupNodeCPtr &node)
        override
        {
          auto literal = getToken(node, mylang::grammar::PAst::TOK_LITERAL);
          auto res = ast.createLiteralBooleanExpression(literal);
          return res;
        }

        GroupNodeCPtr visit_literal_byte(const GroupNodeCPtr &node)
        override
        {
          auto literal = getToken(node, mylang::grammar::PAst::TOK_LITERAL);
          auto res = ast.createLiteralByteExpression(literal);
          return res;
        }

        GroupNodeCPtr visit_literal_char(const GroupNodeCPtr &node)
        override
        {
          auto literal = getToken(node, mylang::grammar::PAst::TOK_LITERAL);
          auto unescaped = unescape_chars(node, literal->text);
          auto normalized = TokenNode::create(unescaped, literal->span());

          auto res = ast.createLiteralCharExpression(normalized);
          return res;
        }

        GroupNodeCPtr visit_literal_string(const GroupNodeCPtr &node)
        override
        {
          auto literal = getToken(node, mylang::grammar::PAst::TOK_LITERAL);
          auto unescaped = unescape_chars(node, literal->text);
          auto normalized = TokenNode::create(unescaped, literal->span());
          auto res = ast.createLiteralStringExpression(normalized);
          return res;
        }

        GroupNodeCPtr visit_literal_decimal_integer(const GroupNodeCPtr &node)
        override
        {
          auto literal = getToken(node, mylang::grammar::PAst::TOK_LITERAL);
          auto res = ast.createLiteralIntegerExpression(literal);
          return res;
        }

        GroupNodeCPtr visit_literal_hex_integer(const GroupNodeCPtr &node)
        override
        {
          auto literal = getToken(node, mylang::grammar::PAst::TOK_LITERAL);
          // the number will be treated as unsigned, even if it starts with 0xF
          auto val = BigInt::parseHex(literal->text.substr(2));
          auto res = ast.createLiteralInteger(val, literal->span());
          return res;
        }

        GroupNodeCPtr visit_literal_octal_integer(const GroupNodeCPtr &node)
        override
        {
          auto literal = getToken(node, mylang::grammar::PAst::TOK_LITERAL);
          auto val = BigInt::parseOctal(literal->text.substr(1));
          auto res = ast.createLiteralInteger(val, literal->span());
          return res;
        }

        GroupNodeCPtr visit_literal_nil(const GroupNodeCPtr &node)
        override
        {
          return ast.createLiteralNilExpression(node->span());
        }

        GroupNodeCPtr visit_literal_real(const GroupNodeCPtr &node)
        override
        {
          auto literal = getToken(node, mylang::grammar::PAst::TOK_LITERAL);
          auto res = ast.createLiteralRealExpression(literal);
          return res;
        }

        GroupNodeCPtr visit_literal_array(const GroupNodeCPtr &node)
        override
        {
          GroupNodes elements;
          try {
            /* we need to manually process the array elements, because they may contain the special bit-sequence token */
            for (auto expr : idioma::astutils::getGroups(node,
                mylang::grammar::PAst::EXPR_EXPRESSION)) {
              if (expr->hasType(mylang::grammar::PAst::GRP_LITERAL_BIT_SEQUENCE)) {
                auto literal = getToken(expr, mylang::grammar::PAst::TOK_LITERAL);
                // drop the first two characters 0b from the literal
                for (char c : literal->text.substr(2)) {
                  elements.push_back(ast.createLiteralBit(c == '1', node->span()));
                }
              } else if (expr->hasType(mylang::grammar::PAst::GRP_LITERAL_BYTE_SEQUENCE)) {
                auto literal = getToken(expr, mylang::grammar::PAst::TOK_LITERAL);
                // drop the first two characters 0bx from the literal
                for (size_t i = 3; i < literal->text.length(); i += 2) {
                  auto tok = TokenNode::create("0bx" + literal->text.substr(i, 2), literal->span());
                  elements.push_back(ast.createLiteralByteExpression(tok));
                }
              } else {
                auto e = visit(expr);
                if (!typeOf(e)) {
                  throw ValidationError(expr, "not an expression");
                }
                elements.push_back(e);
              }
            }

            return ast.createLiteralArrayExpression(elements, node->span());
          } catch (const ValidationError &e) {
            notifyValidationError(e);
            return ast.createLiteralEmptyArrayExpression(node->span());
          }
        }

        GroupNodeCPtr visit_literal_tuple(const GroupNodeCPtr &node)
        override
        {
          auto members = visitGroups(node, mylang::grammar::PAst::REL_MEMBERS);
          return ast.createLiteralTupleExpression(members);
        }

        GroupNodeCPtr visit_literal_tuple_member(const GroupNodeCPtr &node)
        override
        {
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          return ast.createLiteralTupleMember(type, expr);
        }

        GroupNodeCPtr visit_literal_struct(const GroupNodeCPtr &node)
        override
        {
          auto members = visitGroups(node, mylang::grammar::PAst::REL_MEMBERS);
          return ast.createLiteralStructExpression(members);
        }

        GroupNodeCPtr visit_literal_struct_member(const GroupNodeCPtr &node)
        override
        {
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          auto name = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          return ast.createLiteralStructMember(name, type, expr);
        }

        GroupNodeCPtr visit_interpolate_text_expression(const GroupNodeCPtr &node)
        override
        {
          auto exprs = visitExpressions(node, mylang::grammar::PAst::EXPR_ARGUMENTS);
          return ast.createInterpolateTextExpression(exprs);
        }

        GroupNodeCPtr visit_quote_text_expression(const GroupNodeCPtr &node)
        override
        {
          auto literal = getToken(node, mylang::grammar::PAst::TOK_LITERAL);
          auto res = ast.createLiteralStringExpression(literal);
          return res;
        }

        GroupNodeCPtr visit_stringify_expression(const GroupNodeCPtr &node)
        override
        {
          auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          auto res = ast.createStringifyExpression(expr);
          return res;
        }

        GroupNodeCPtr visit_arithmetic_expression(const GroupNodeCPtr &node)
        override
        {
          auto lhs = visitExpression(node, mylang::grammar::PAst::EXPR_LHS);
          auto rhs = visitExpression(node, mylang::grammar::PAst::EXPR_RHS);
          auto op = getToken(node, mylang::grammar::PAst::TOK_OP);

          if (typeOf<IntegerType>(lhs) || typeOf<IntegerType>(rhs)) {
            if (op->text == "+") {
              return ast.createIntegerAddExpression(lhs, rhs);
            } else if (op->text == "-") {
              return ast.createIntegerSubtractExpression(lhs, rhs);
            } else if (op->text == "*") {
              return ast.createIntegerMultiplyExpression(lhs, rhs);
            } else if (op->text == "/") {
              return ast.createIntegerDivideExpression(lhs, rhs);
            } else if (op->text == "/") {
              return ast.createIntegerDivideExpression(lhs, rhs);
            } else if (op->text == "%") {
              return ast.createIntegerModulusExpression(lhs, rhs);
            } else if (op->text == "%%") {
              return ast.createIntegerRemainderExpression(lhs, rhs);
            }
          }

          if (typeOf<RealType>(lhs) || typeOf<RealType>(rhs)) {
            if (op->text == "+") {
              return ast.createRealAddExpression(lhs, rhs);
            } else if (op->text == "-") {
              return ast.createRealSubtractExpression(lhs, rhs);
            } else if (op->text == "*") {
              return ast.createRealMultiplyExpression(lhs, rhs);
            } else if (op->text == "/") {
              return ast.createRealDivideExpression(lhs, rhs);
            } else if (op->text == "/") {
              return ast.createRealDivideExpression(lhs, rhs);
            } else if (op->text == "%%") {
              return ast.createRealRemainderExpression(lhs, rhs);
            }
          }
          throw ValidationError(op, "unexpected operator");
        }

        GroupNodeCPtr visit_negate_expression(const GroupNodeCPtr &node)
        override
        {
          auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          auto op = getToken(node, mylang::grammar::PAst::TOK_OP);
          if (op->text == "-") {
            if (typeOf<IntegerType>(expr)) {
              return ast.createIntegerNegateExpression(expr);
            }
            if (typeOf<RealType>(expr)) {
              return ast.createRealNegateExpression(expr);
            }
          } else if (op->text == "!" && typeOf<BooleanType>(expr)) {
            return ast.createNotExpression(expr);
          } else if (op->text == "~" && typeOf<BitType>(expr)) {
            return ast.createNotExpression(expr);
          } else if (op->text == "~" && typeOf<ByteType>(expr)) {
            return ast.createNotExpression(expr);
          }

          throw ValidationError(op, "Unexpected token " + op->text);
        }

        GroupNodeCPtr visit_bitop_expression(const GroupNodeCPtr &node)
        override
        {
          auto lhs = visitExpression(node, mylang::grammar::PAst::EXPR_LHS);
          auto rhs = visitExpression(node, mylang::grammar::PAst::EXPR_RHS);
          auto op = getToken(node, mylang::grammar::PAst::TOK_OP);

          if (op->text == "&&") {
            return ast.createShortCircuitAndExpression(lhs, rhs);
          } else if (op->text == "||") {
            return ast.createShortCircuitOrExpression(lhs, rhs);
          } else if (op->text == "&") {
            return ast.createAndExpression(lhs, rhs);
          } else if (op->text == "|") {
            return ast.createOrExpression(lhs, rhs);
          } else if (op->text == "^") {
            return ast.createXorExpression(lhs, rhs);
          } else {
            throw ValidationError(op, "Unexpected token " + op->text);
          }
        }

        GroupNodeCPtr visit_try_expression(const GroupNodeCPtr &node)
        override
        {
          auto lhs = visitExpression(node, mylang::grammar::PAst::EXPR_LHS);
          auto rhs = visitExpression(node, mylang::grammar::PAst::EXPR_RHS);

          return ast.createTryCatchExpression(lhs, rhs);
        }

        GroupNodeCPtr visit_comparison_expression(const GroupNodeCPtr &node)
        override
        {
          auto op = getToken(node, mylang::grammar::PAst::TOK_OP);
          try {
            auto lhs = visitExpression(node, mylang::grammar::PAst::EXPR_LHS);
            auto rhs = visitExpression(node, mylang::grammar::PAst::EXPR_RHS);

            if (op->text == "==") {
              return ast.createComparisonEQExpression(lhs, rhs);
            } else if (op->text == "!=") {
              return ast.createComparisonNEQExpression(lhs, rhs);
            } else if (op->text == "<") {
              return ast.createComparisonLTExpression(lhs, rhs);
            } else if (op->text == "<=") {
              return ast.createComparisonLTEExpression(lhs, rhs);
            } else if (op->text == ">") {
              return ast.createComparisonGTExpression(lhs, rhs);
            } else if (op->text == ">=") {
              return ast.createComparisonGTEExpression(lhs, rhs);
            }
          } catch (const ValidationError &e) {
            notifyValidationError(e);
            return ast.createLiteralBoolean(false, node->span());
          }

          throw ValidationError(op, "Unexpected token " + op->text);
        }

        GroupNodeCPtr visit_orelse_expression(const GroupNodeCPtr &node)
        override
        {
          auto lhs = visitExpression(node, mylang::grammar::PAst::EXPR_LHS);
          auto rhs = visitExpression(node, mylang::grammar::PAst::EXPR_RHS);

          try {
            return ast.createGetOptionalValueOrElseExpression(lhs, rhs);
          } catch (const ValidationError &e) {
            notifyValidationError(e);
            return rhs;
          }
        }

        GroupNodeCPtr visit_concatenate_expression(const GroupNodeCPtr &node)
        override
        {
          auto lhs = visitExpression(node, mylang::grammar::PAst::EXPR_LHS);
          auto rhs = visitExpression(node, mylang::grammar::PAst::EXPR_RHS);

          if (typeOf<ArrayType>(lhs) || typeOf<ArrayType>(rhs)) {
            return ast.createConcatenateArraysExpression(lhs, rhs);
          } else {
            return ast.createConcatenateStringsExpression(lhs, rhs);
          }
        }

        GroupNodeCPtr visit_merge_expression(const GroupNodeCPtr &node)
        override
        {
          auto lhs = visitExpression(node, mylang::grammar::PAst::EXPR_LHS);
          auto rhs = visitExpression(node, mylang::grammar::PAst::EXPR_RHS);

          return ast.createMergeTuplesExpression(lhs, rhs);
        }

        GroupNodeCPtr visit_zip_expression(const GroupNodeCPtr &node)
        override
        {
          auto lhs = visitExpression(node, mylang::grammar::PAst::EXPR_LHS);
          auto rhs = visitExpression(node, mylang::grammar::PAst::EXPR_RHS);

          return ast.createZipArraysExpression(lhs, rhs);
        }

        GroupNodeCPtr visit_transform_expression(const GroupNodeCPtr &node)
        override
        {

          auto ty = visitType(node, mylang::grammar::PAst::REL_TYPE);
          auto args = visitExpressions(node, mylang::grammar::PAst::EXPR_ARGUMENTS);

          // create the result before we attempt to resolve it
          // this way, if there is an error, there is no point in resolving it
          auto res = ast.createTransformExpression(ty, args);

          // keep track of the transforms we'll have to resolve later on
          if (transformResolver) {

            // we need a named type
            ConstrainedTypePtr retTy = getConstraintAnnotation(ty);
            ::std::vector<ConstrainedTypePtr> argTypes;
            for (auto arg : args) {
              auto argTy = getConstraintAnnotation(arg);
              argTypes.push_back(argTy);
            }

            auto fn = ConstrainedFunctionType::get(retTy, argTypes);
            if (!transformResolver->resolve(fn)) {
              notifyError(node,
                  stream
                      << "failed to construct a suitable transformation chain with the signature "
                      << fn->toString());
            }
          }
          return res;
        }

        GroupNodeCPtr visit_safe_cast_expression(const GroupNodeCPtr &node)
        override
        {
          auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          return ast.createSafeCastExpression(type, expr);
        }

        GroupNodeCPtr visit_unsafe_cast_expression(const GroupNodeCPtr &node)
        override
        {
          auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          return ast.createUnsafeTypeCastExpression(type, expr);
        }

        GroupNodeCPtr visit_clamp_cast_expression(const GroupNodeCPtr &node)
        override
        {
          auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          return ast.createIntegerClampExpression(type, expr);
        }

        GroupNodeCPtr visit_wrap_cast_expression(const GroupNodeCPtr &node)
        override
        {
          auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          return ast.createIntegerWrapExpression(type, expr);
        }

        GroupNodeCPtr visit_conditional_expression(const GroupNodeCPtr &node)
        override
        {
          auto iftrue = visitExpression(node, mylang::grammar::PAst::EXPR_IFTRUE);
          auto iffalse = visitExpression(node, mylang::grammar::PAst::EXPR_IFFALSE);
          GroupNodeCPtr cond;
          try {
            cond = visitExpression(node, mylang::grammar::PAst::EXPR_CONDITION);
          } catch (const ValidationError &e) {
            notifyValidationError(e);
            cond = ast.createLiteralBoolean(false, node->span());
          }
          try {
            return ast.createConditionalExpression(cond, iftrue, iffalse);
          } catch (const ValidationError &e) {
            notifyValidationError(e);
            return iftrue;
          }
        }

        GroupNodeCPtr visit_optify_expression(const GroupNodeCPtr &node)
        override
        {
          auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          return ast.createOptifyExpression(expr);
        }

        GroupNodeCPtr visit_deref_expression(const GroupNodeCPtr &node)
        override
        {
          auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          if (constraintOf<ConstrainedInputType>(expr)) {
            return ast.createGetInputExpression(expr);
          } else if (constraintOf<ConstrainedOutputType>(expr)) {
            return ast.createGetOutputExpression(expr);
          } else if (constraintOf<ConstrainedMutableType>(expr)) {
            return ast.createGetMutableValueExpression(expr);
          } else {
            return ast.createGetOptionalValueExpression(expr);
          }
        }

        GroupNodeCPtr createIndexExpression(const GroupNodeCPtr &expr, const GroupNodeCPtr &index)
        {
          auto ty = rootType(expr);
          if (!ty) {
            throw ValidationError(expr, "missing type annotation");
          }

          if (ty->self<TupleType>()) {
            return ast.createTupleMemberExpression(expr, index);
          } else if (ty->self<StringType>()) {
            return ast.createStringIndexExpression(expr, index);
          } else if (ty->self<ArrayType>()) {
            return ast.createArrayIndexExpression(expr, index);
          } else {
            throw ValidationError(expr,
                "indexing cannot be applied to " + typeOf(expr)->toString());
          }
        }

        GroupNodeCPtr createSubrangeExpression(const GroupNodeCPtr &expr,
            const GroupNodeCPtr &begin, const GroupNodeCPtr &end)
        {
          auto ty = rootType(expr);
          if (!ty) {
            throw ValidationError(expr, "missing type annotation");
          }

          if (ty->self<StringType>()) {
            return ast.createStringSubrangeExpression(expr, begin, end);
          } else if (ty->self<ArrayType>()) {
            return ast.createArraySubrangeExpression(expr, begin, end);
          } else {
            throw ValidationError(expr,
                "subrange cannot be applied to " + typeOf(expr)->toString());
          }
        }

        GroupNodeCPtr visit_index_expression(const GroupNodeCPtr &node)
        override
        {
          auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_LHS);
          auto index = visitExpression(node, mylang::grammar::PAst::EXPR_RHS);

          if (typeOf<OptType>(expr)) {
            auto tmp = createTemporary(expr);
            auto tmpDecl = tmp.first;
            auto tmpRef = tmp.second;
            auto value = createIndexExpression(ast.createGetOptionalValueExpression(tmpRef), index);
            return createApplyExpr(tmpDecl, tmpRef, value);
          } else {
            return createIndexExpression(expr, index);
          }
        }

        GroupNodeCPtr visit_subrange_expression(const GroupNodeCPtr &node)
        override
        {
          auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          auto begin = visitExpression(node, mylang::grammar::PAst::EXPR_BEGIN);
          auto end = visitExpression(node, mylang::grammar::PAst::EXPR_END);

          if (typeOf<OptType>(expr)) {
            auto tmp = createTemporary(expr);
            auto tmpDecl = tmp.first;
            auto tmpRef = tmp.second;
            auto value = createSubrangeExpression(ast.createGetOptionalValueExpression(tmpRef),
                begin, end);
            return createApplyExpr(tmpDecl, tmpRef, value);
          } else {
            return createSubrangeExpression(expr, begin, end);
          }
        }

        GroupNodeCPtr visit_instantiate_expression(const GroupNodeCPtr &node)
        override
        {
          auto fnExpr = visitGroup(node, mylang::grammar::PAst::EXPR_EXPRESSION);

          auto defExpr = fnExpr->self<DefinitionGroup>();
          if (!defExpr) {
            throw ValidationError(fnExpr, "not a generic function reference");
          }
          auto fnDef = defExpr->definition->self<DefFunctionContext>();
          if (!fnDef) {
            throw ValidationError(fnExpr, "expression does not reference a generic function");
          }

          auto types = visitTypes(node, mylang::grammar::PAst::REL_PARAMETER_TYPES);
          ::std::vector<ConstrainedTypePtr> specializations;
          for (auto n : types) {
            auto ty = constraintOf(n);
            if (!ty) {
              throw ValidationError(fnExpr, "non-type specializations are not supported");
            }
            specializations.push_back(ty);
          }
          GroupNodeCPtr res;
          if (specializations.empty()) {
            auto general = fnDef->generalize();
            res = ::std::make_shared<DefinitionGroup>(general);
          } else {
            auto special = fnDef->specialize(specializations);
            res = ::std::make_shared<DefinitionGroup>(special);
          }
          return res;
        }

        GroupNodeCPtr visit_call_expression(const GroupNodeCPtr &node)
        override
        {
          auto fnExpr = visitGroup(node, mylang::grammar::PAst::EXPR_FUNCTION);
          auto args = visitExpressions(node, mylang::grammar::PAst::EXPR_ARGUMENTS);

          GroupNodeCPtr expr = fnExpr;

          auto internalGrp = expr->self<DefinitionGroup>();
          if (internalGrp) {
            Definition::VisitFN fn = [this](ScopePtr s, GroupNodeCPtr n) {
              return withNewScope(s, [&](Converter &conv) {
                    return conv.visit(n);
                  });
            };
            Definition::RegisterVariableFN registerVarFN = [this](GroupNodeCPtr vardecl) {
              declaredVariables.push_back(vardecl);
            };
            return internalGrp->definition->create_call(scope, node, args, registerVarFN, fn);
          }

          // remove any typedefs we might have

          const ConstrainedTypePtr exprTy = constraintOf(expr);
          ConstrainedTypePtr exprRootTy = exprTy->roottype();

          auto optFnTy = exprRootTy->self<ConstrainedOptType>();
          if (optFnTy) {
            // define a local variable with the value of the expression
            auto fnTy = optFnTy->element->roottype()->self<ConstrainedFunctionType>();
            if (fnTy) {
              auto tmp = createTemporary(expr);
              auto tmpDecl = tmp.first;
              auto tmpRef = tmp.second;
              try {
                auto callExpr = ast.createFunctionCallExpression(
                    ast.createGetOptionalValueExpression(tmpRef), args);
                return createApplyExpr(tmpDecl, tmpRef, callExpr);
              } catch (const ValidationError &e) {
                notifyValidationError(e);
                return ast.createLiteralNilExpression(fnExpr->span());
              }
            }
          } else {
            auto mutableFnTy = exprRootTy->self<ConstrainedMutableType>();
            if (mutableFnTy) {
              expr = ast.createGetMutableValueExpression(expr);
              exprRootTy = constraintOf(expr)->roottype();
            }
          }
          auto fnTy = exprRootTy->self<ConstrainedFunctionType>();
          if (!fnTy) {
            throw ValidationError(expr, "not a function type:" + exprTy->toString());
          }
          // if it's a function expression we can just directly generate call expression
          try {
            return ast.createFunctionCallExpression(expr, args);
          } catch (const ValidationError &e) {
            notifyValidationError(e);
            return createError(expr, fnTy->returnType);
          }
        }

        GroupNodeCPtr visit_with_expression(const GroupNodeCPtr &node)
        override
        {
          return withNewScope([&](Converter &local) {
            auto decls = local.visitGroups(node, mylang::grammar::PAst::REL_DECLS);
            auto expr = local.visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
            return ast.createWithExpression(decls, expr);
          });
        }

        GroupNodeCPtr visit_loop_expression(const GroupNodeCPtr &node)
        override
        {
          return withNewScope(
              [&](
                  Converter &local) {
                    auto state = local.visitGroup(node, mylang::grammar::PAst::REL_DECL);
                    auto cond = local.visitExpression(node, mylang::grammar::PAst::EXPR_CONDITION);
                    return local.withNewScope([&](Converter &bodyLocal) {
                          auto body = bodyLocal.visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
                          return ast.createLoopExpression(state, cond, body);
                        });
                  });
        }

        GroupNodeCPtr visit_sub_expression(const GroupNodeCPtr &node)
        override
        {
          return visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
        }

        GroupNodeCPtr visit_lambda_expression(const GroupNodeCPtr &node)
        override
        {

          return withNewScope(::std::make_shared<ContainerContext>(), [&](Converter &local) {
            auto params = local.visitGroups(node, mylang::grammar::PAst::REL_PARAMETERS);
            auto type = local.visitType(node, mylang::grammar::PAst::REL_TYPE);
            auto body = local.visitExpression(node, mylang::grammar::PAst::REL_BODY);
            return ast.createLambdaExpression(type, params, body);
          });
        }

        GroupNodeCPtr visit_new_expression(const GroupNodeCPtr &node)
        override
        {
          // there must at least be 1 named argument, otherwise it we need to have expr args
          GroupNodes args = visitExpressions(node, mylang::grammar::PAst::EXPR_ARGUMENTS);

          auto type = idioma::astutils::getGroup(node, mylang::grammar::PAst::REL_TYPE);
          // we need to check if the new TYPE is actual a process name
          if (type->hasType(mylang::grammar::PAst::GRP_TYPENAME)) {
            auto def = findDefinition(getTokens(type, mylang::grammar::PAst::REL_QN));
            auto proc = ::std::dynamic_pointer_cast<const DefProcessContext>(def);
            if (proc) {
              ::std::vector<ConstrainedTypePtr> argTypes;
              for (auto arg : args) {
                argTypes.push_back(constraintOf(arg));
              }
              if (!proc->hasConstructor(argTypes)) {
                notifyError(node, stream << "no such constructor");
              }

              // check the arguments
              return ast.createNewProcessExpression(proc->type(), proc->name, args);
            }
          }
          auto constructType = visitType(node, mylang::grammar::PAst::REL_TYPE);
          return ast.createNewObject(constructType, args);
        }

        GroupNodeCPtr get_member_expr(const GroupNodeCPtr &expr, const TokenNodeCPtr &n)
        {
          auto exprTy = constraintOf(expr);
          auto ty = rootType(expr);
          if (ty) {
            if (hasMember(ty, n->text)) {
              if (ty->self<UnionType>()) {
                return ast.createUnionMemberExpression(expr, n);
              } else if (ty->self<StructType>()) {
                return ast.createStructMemberExpression(expr, n);
              } else if (ty->self<ProcessType>()) {
                return ast.createProcessMemberExpression(expr, n);
              } else {
                notifyInternalError(n, stream << "member found, but not handled");
              }
            }

            if (ty) {
              auto def = builtins->findInstanceBuiltin(expr, n->text, n->span());
              if (def) {
                return def->create_reference(scope, n);
              }
              def = library->findInstanceBuiltin(expr, n->text, n->span());
              if (def) {
                return def->create_reference(scope, n);
              }

              // look for an extension function
              auto fn = ::std::dynamic_pointer_cast<const DefFunctionContext>(
                  searchDefinition(n->text));
              if (fn && fn->isExtension && fn->hasExtension(exprTy)) {
                struct ExtensionDefinition: public Definition
                {
                  ExtensionDefinition(const GroupNodeCPtr &ext,
                      ::std::shared_ptr<const DefFunctionContext> fnDef)
                      : extensionExpr(ext), fn(fnDef)
                  {
                  }

                  ~ExtensionDefinition()
                  {
                  }

                  GroupNodeCPtr create_call(const ScopePtr &s, const SourceLocation &src,
                      const GroupNodes &args, RegisterVariableFN unused1, VisitFN unused2) const
                      override
                  {
                    GroupNodes xargs;
                    xargs.push_back(extensionExpr);
                    xargs.insert(xargs.end(), args.begin(), args.end());
                    return fn->create_call(s, src, xargs, unused1, unused2);
                  }

                private:
                  const GroupNodeCPtr extensionExpr;
                  ::std::shared_ptr<const DefFunctionContext> fn;
                };

                GroupNodePtr delayed = DefinitionGroup::create(
                    ::std::make_shared<ExtensionDefinition>(expr, fn));
                TypePtr type = FunctionLikeType::get([=](const ::std::vector<TypePtr> &argv) {
                  ::std::vector<TypePtr> params;
                  params.push_back(ty);
                  params.insert(params.end(), argv.begin(), argv.end());
                  return def->get_function_type(argv);
                });
                setTypeAnnotation(delayed, type);

                return delayed;
              }
            }
          }

          // should we search for a type builtin?
          if (expr->self<const DefinitionGroup>()) {
            auto def = expr->self<const DefinitionGroup>()->definition;
            auto subdef = def->find(n->text);
            if (subdef) {
              auto ref = subdef->create_reference(scope, n);
              if (!ref) {
                ref = ::std::make_shared<DefinitionGroup>(subdef);
              }
              return ref;
            }
            throw ValidationError(n, "cannot find: " + n->text);
          }
          throw ValidationError(n, "no such member, builtin, or extension: " + n->text);
        }

        GroupNodeCPtr visit_apply_expression(const GroupNodeCPtr &node)
        override
        {
          auto n = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          auto lhs = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          auto lhsTy = constraintOf(lhs)->roottype();

          auto mutableTy = lhsTy->self<ConstrainedMutableType>();
          if (mutableTy) {
            return get_member_expr(ast.createGetMutableValueExpression(lhs), n);
          }

          // define a local variable with the value of the expression
          auto tmp = createTemporary(lhs);
          auto tmpDecl = tmp.first;
          auto tmpRef = tmp.second;

          auto optionalValue = ast.createGetOptionalValueExpression(tmpRef);
          auto expr = get_member_expr(optionalValue, n);

          auto internal = expr->self<const DefinitionGroup>();
          if (internal) {
            auto def = ::std::make_shared<OptionalBuiltinDefinition>(tmpDecl, tmpRef,
                internal->definition);
            return ::std::make_shared<DefinitionGroup>(def);
          } else {
            return createApplyExpr(tmpDecl, tmpRef, expr);
          }

        }

        GroupNodeCPtr get_type_chain_expression(ConstrainedTypePtr ty,
            const ::std::vector<::std::shared_ptr<const TokenNode>> &chain) const
        {
          ::std::shared_ptr<const Definition> def;
          TokenNodeCPtr n;
          for (size_t i = 0; i < chain.size(); ++i) {
            n = chain.at(i);
            def = builtins->findTypeBuiltin(ty, n->text, n->span());
            if (!def) {
              notifyError(n, stream << "type " << ty->toString() << " has no member " << n->text);
              return ast.createLiteralNilExpression(n->span());
            }
            if (i < chain.size() - 1) {
              // the interior of the chain must a type reference
              ty = def->get_type();
            }
          }

          return def->create_reference(scope, n);
        }

        GroupNodeCPtr visit_type_member_chain_expression(const GroupNodeCPtr &node)
        override
        {
          auto chain = idioma::astutils::getTokens(node, mylang::grammar::PAst::TOK_IDENTIFIERS);
          ConstrainedTypePtr ty;

          auto name = idioma::astutils::getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          if (name) {
            // if we have a variable expression, check if the expression references a variable or a type
            auto def = findDefinition(name);
            auto tyAlias = def->self<DefTypeAliasContext>();
            auto tyDef = def->self<DefTypeContext>();
            if (tyAlias) {
              ty = constraintOf(tyAlias->alias);
            } else if (tyDef) {
              ty = tyDef->getType();
            } else {
              auto g = get_variable_reference_expression(name);
              ty = constraintOf(g);
            }
          } else {
            auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
            ty = constraintOf(type);
          }
          if (!ty) {
            notifyError(node, stream << "failed to determine type");
            return ast.createLiteralNilExpression(node->span());
          }
          return get_type_chain_expression(ty, chain);
        }

        GroupNodeCPtr visit_member_expression(const GroupNodeCPtr &node)
        override
        {
          auto g = visitGroup(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          auto n = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          return get_member_expr(g, n);
        }

        GroupNodeCPtr visit_named_expression(const GroupNodeCPtr &node)
        override
        {
          auto e = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          auto n = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);

          return ast.createNamedExpression(n, e);
        }

        GroupNodeCPtr get_variable_reference_expression(const TokenNodeCPtr name)
        {
          auto ctx = searchContext<StatementContext>();

          // the object may not necessary be a variable; we might reference a function name or a unit
          auto def = findDefinition(name);

          // if it's a variable or value, make sure it has been initialized
          auto vardef = def->self<const VariableDefinition>();
          if (vardef) {
            if (!vardef->hasInitialValue()) {
              if (vardef->info->kind == Variable::KIND_VALUE) {
                if (getValueDisposition(ctx, vardef->name)
                    != StatementContext::VariableDisposition::INITIALIZED) {
                  notifyError(name, stream << "value '" << name << "' may not be initialized");
                }
              } else if (vardef->info->kind == Variable::KIND_VARIABLE) {
                if (getVariableDisposition(ctx, vardef->name)
                    != StatementContext::VariableDisposition::INITIALIZED) {
                  notifyError(name, stream << "variable '" << name << "' may not be initialized");
                }
              }
            }
            if (vardef->info->scope != Variable::SCOPE_PROCESS
                && vardef->info->kind == Variable::KIND_VARIABLE
                && containerContext() != vardef->containerContext) {
              notifyError(name,
                  stream << "variable '" << name << "' is not accessible in this context");
            }
          }

          auto ref = def->create_reference(scope, name);
          if (!ref) {
            ref = ::std::make_shared<DefinitionGroup>(def);
          }

          return ref;
        }

        GroupNodeCPtr visit_variable_reference_expression(const GroupNodeCPtr &node)
        override
        {
          return get_variable_reference_expression(
              getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER));
        }

        GroupNodeCPtr visit_typename(const GroupNodeCPtr &node)
        override
        {
          auto def = findDefinition(getTokens(node, mylang::grammar::PAst::REL_QN));
          auto defalias = ::std::dynamic_pointer_cast<const DefTypeAliasContext>(def);
          if (defalias) {
            return defalias->alias;
          }

          auto deftype = ::std::dynamic_pointer_cast<const DefTypeContext>(def);
          if (deftype) {
            // FIXME: this should lead to memory leak to due cyclic reference when type is recursive
            auto ty = deftype->getType();
            return ast.createType(ty, node->span());
          }

          throw ValidationError(node, "expected a type");
        }

        GroupNodeCPtr visit_constrained_type(const GroupNodeCPtr &node)
        override
        {
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          try {
            auto constraintNodes = visitGroups(node, mylang::grammar::PAst::REL_CONSTRAINTS);
            if (constraintNodes.empty()) {
              return type;
            }
            /**
             * since at this time we don't consider the constraints, we just use
             * the type being constrained as the type of this expression.
             */
            auto src_ty = constraintOf(type);

            Constraints constraints;
            ::std::set<::std::string> names;
            for (auto c : constraintNodes) {
              auto name = idioma::astutils::getToken(c, VAst::Token::IDENTIFIER);
              auto expr = idioma::astutils::getGroup(c, VAst::Expr::EXPRESSION);
              if (!names.insert(name->text).second) {
                throw ValidationError(name, "duplicate constraint name");
              }
              auto eTy = constraintOf(expr);
              if (!eTy->isConcrete()) {
                throw ValidationError(expr, "constraint value must be concrete");
              }
              auto lit = getConstAnnotation(expr);
              if (lit) {
                constraints.put(name->text, LiteralConstraint::get(lit));
              } else {
                throw ValidationError(name, "dynamic constraints are unsupported at");
              }
            }

            auto ty = src_ty->constrain(constraints);

            return ast.createType(ty, node->span(), { { VAst::Relation::TYPE, type }, {
                VAst::Relation::CONSTRAINTS, constraintNodes } });
          } catch (const ValidationError &e) {
            notifyValidationError(e);
            return type;
          }
        }

        GroupNodeCPtr visit_named_constraint(const GroupNodeCPtr &node)
        override
        {
          auto tok = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          return createNamedConstraint(tok, expr);
        }

        GroupNodeCPtr visit_type_member(const GroupNodeCPtr &node)
        override
        {
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          auto lhsTy = constraintOf(type);
          if (!lhsTy) {
            // fake it out with an integer
            return ast.createType(ConstrainedPrimitiveType::getInteger(), node->span());
          }

          auto n = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          auto def = builtins->findTypeBuiltin(lhsTy, n->text, n->span());
          if (!def) {
            notifyError(n, stream << "type " << lhsTy << " has no member " << n->text);
            // fake it out with an integer
            return ast.createType(ConstrainedPrimitiveType::getInteger(), node->span());
          }

          auto ty = def->get_type();
          if (!ty) {
            notifyError(node, stream << "expected a type");
            // fake it out with an integer
            return ast.createType(ConstrainedPrimitiveType::getInteger(), node->span());
          }

          return ast.createType(ty, node->span());
        }

        GroupNodeCPtr visit_typeof(const GroupNodeCPtr &node)
        override
        {
          auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          auto ty = constraintOf(expr);
          return ast.createType(ty, node->span(), { { VAst::Expr::EXPRESSION, expr } });
        }

        GroupNodeCPtr visit_type_bit(const GroupNodeCPtr &node)
        override
        {
          auto token = getToken(node, mylang::grammar::PAst::TOK_TYPE);
          auto ty = ConstrainedPrimitiveType::getBit();
          auto t = token ? token : TokenNode::create("bit", node->span());
          return ast.createType(ty, node->span(), { { VAst::Token::TYPE, t } });
        }

        GroupNodeCPtr visit_type_boolean(const GroupNodeCPtr &node)
        override
        {
          auto token = getToken(node, mylang::grammar::PAst::TOK_TYPE);
          auto ty = ConstrainedPrimitiveType::getBoolean();
          auto t = token ? token : TokenNode::create("boolean", node->span());
          return ast.createType(ty, node->span(), { { VAst::Token::TYPE, t } });
        }

        GroupNodeCPtr visit_type_byte(const GroupNodeCPtr &node)
        override
        {
          auto token = getToken(node, mylang::grammar::PAst::TOK_TYPE);
          auto ty = ConstrainedPrimitiveType::getByte();
          auto t = token ? token : TokenNode::create("byte", node->span());
          return ast.createType(ty, node->span(), { { VAst::Token::TYPE, t } });
        }

        GroupNodeCPtr visit_type_char(const GroupNodeCPtr &node)
        override
        {
          auto token = getToken(node, mylang::grammar::PAst::TOK_TYPE);
          auto ty = ConstrainedPrimitiveType::getChar();
          auto t = token ? token : TokenNode::create("char", node->span());
          return ast.createType(ty, node->span(), { { VAst::Token::TYPE, t } });
        }

        GroupNodeCPtr visit_type_real(const GroupNodeCPtr &node)
        override
        {
          auto token = getToken(node, mylang::grammar::PAst::TOK_TYPE);
          auto ty = ConstrainedPrimitiveType::getReal();
          auto t = token ? token : TokenNode::create("real", node->span());
          return ast.createType(ty, node->span(), { { VAst::Token::TYPE, t } });
        }

        GroupNodeCPtr visit_type_integer(const GroupNodeCPtr &node)
        override
        {
          auto token = getToken(node, mylang::grammar::PAst::TOK_TYPE);
          auto ty = ConstrainedPrimitiveType::getInteger();
          auto t = token ? token : TokenNode::create("integer", node->span());
          return ast.createType(ty, node->span(), { { VAst::Token::TYPE, t } });
        }

        GroupNodeCPtr visit_type_string(const GroupNodeCPtr &node)
        override
        {
          auto token = getToken(node, mylang::grammar::PAst::TOK_TYPE);
          auto ty = ConstrainedPrimitiveType::getString();
          auto t = token ? token : TokenNode::create("string", node->span());
          return ast.createType(ty, node->span(), { { VAst::Token::TYPE, t } });
        }

        GroupNodeCPtr visit_type_void(const GroupNodeCPtr &node)
        override
        {
          auto token = getToken(node, mylang::grammar::PAst::TOK_TYPE);
          auto ty = ConstrainedPrimitiveType::getVoid();
          auto t = token ? token : TokenNode::create("void", node->span());
          return ast.createType(ty, node->span(), { { VAst::Token::TYPE, t } });
        }

        GroupNodeCPtr visit_type_generic(const GroupNodeCPtr &node)
        override
        {
          auto token = getToken(node, mylang::grammar::PAst::TOK_TYPE);
          auto ty = ConstrainedGenericType::create();
          auto t = token ? token : TokenNode::create("generic", node->span());
          return ast.createType(ty, node->span(), { { VAst::Token::TYPE, t } });
        }

        GroupNodeCPtr visit_type_tuple(const GroupNodeCPtr &node)
        override
        {
          auto elements = visitTypes(node, mylang::grammar::PAst::REL_MEMBERS);
          ::std::vector<ConstrainedTypePtr> elementTypes;
          auto voidTy = ConstrainedVoidType::create();
          for (size_t i = 0; i < elements.size(); ++i) {
            const auto &e = elements.at(i);
            auto eTy = constraintOf(e);
            if (!eTy->isConcrete()) {
              throw ValidationError(e,
                  "tuple element " + ::std::to_string(i) + " must be concrete");
            }
            if (voidTy->isSameConstraint(*eTy)) {
              throw ValidationError(e,
                  "tuple element " + ::std::to_string(i) + " must not be void");
            }
            elementTypes.push_back(eTy);
          }
          auto ty = ConstrainedTupleType::get(elementTypes);
          return ast.createType(ty, node->span(), { { VAst::Relation::MEMBERS, elements } });
        }

        ConstAnnotation getConstantInteger(const GroupNodeCPtr &node)
        {
          if (!node) {
            return nullptr;
          }
          auto value = getConstAnnotation(node);
          if (!value || !::std::dynamic_pointer_cast<const IntegerType>(value->type())) {
            throw ValidationError(node, "expected an integer literal");
          }
          return value;
        }

        Constraints createArrayConstraints(const GroupNodeCPtr &dimension)
        {
          ConstAnnotation len, min_len, max_len;
          len = getConstantInteger(visitExpression(dimension, mylang::grammar::PAst::REL_SIZE));
          min_len = getConstantInteger(
              visitExpression(dimension, mylang::grammar::PAst::REL_MIN_SIZE));
          max_len = getConstantInteger(
              visitExpression(dimension, mylang::grammar::PAst::REL_MAX_SIZE));

          if (len) {
            min_len = max_len = len;
          }
          Constraints C;
          if (min_len) {
            C.put("min", LiteralConstraint::get(min_len));
          }
          if (max_len) {
            C.put("max", LiteralConstraint::get(max_len));
          }
          return C;
        }

        GroupNodeCPtr visit_type_array(const GroupNodeCPtr &node)
        override
        {
          ::std::vector<GroupNodeCPtr> dimensions;
          auto element_node = node;

          // parsing the array type: E[i][j] produces would produce the array (E[i])[j],
          // but we want i to be the first dimension and j the second dimension; so we
          // adjust during validation to produce the desired dimensions
          while (element_node->type.value<mylang::grammar::PAst::GroupType>()->key
              == mylang::grammar::PAst::GRP_TYPE_ARRAY) {
            dimensions.push_back(
                idioma::astutils::getGroup(element_node, mylang::grammar::PAst::REL_DIMENSION));
            element_node = idioma::astutils::getGroup(element_node,
                mylang::grammar::PAst::REL_TYPE);
          }

          auto element = visitType(element_node);

          auto elementTy = constraintOf(element);
          if (!elementTy->isConcrete()) {
            throw ValidationError(element, "array element must have a concrete type");
          }

          ConstrainedTypePtr ty = elementTy;

          for (auto dim = dimensions.begin(); dim != dimensions.end(); ++dim) {
            try {
              Constraints C;
              try {
                C = createArrayConstraints(*dim);
              } catch (const ValidationError &e) {
                notifyValidationError(e);
                return ast.createType(ty, node->span());
              }
              ty = ConstrainedArrayType::getUnboundedArray(ty);
              ty = ty->constrain(C);
            } catch (...) {
              notifyError(node, stream << "invalid array bounds");
            }
          }

          auto res = ast.createType(ty, node->span(), { { VAst::Relation::TYPE, element } });
          return res;
        }

        GroupNodeCPtr visit_type_mutable(const GroupNodeCPtr &node)
        override
        {
          auto element = visitType(node, mylang::grammar::PAst::REL_TYPE);
          auto elementTy = constraintOf(element);
          if (!elementTy->isConcrete()) {
            throw ValidationError(element, "mutable element must have a concrete type");
          }
          auto ty = ConstrainedMutableType::get(elementTy);

          return ast.createType(ty, node->span(), { { VAst::Relation::TYPE, element } });
        }

        GroupNodeCPtr visit_type_opt(const GroupNodeCPtr &node)
        override
        {
          auto element = visitType(node, mylang::grammar::PAst::REL_TYPE);
          auto elementTy = constraintOf(element);
          if (!elementTy->isConcrete()) {
            throw ValidationError(element, "optional element must have a concrete type");
          }
          auto ty = ConstrainedOptType::get(elementTy);

          return ast.createType(ty, node->span(), { { VAst::Relation::TYPE, element } });
        }

        GroupNodeCPtr visit_type_function(const GroupNodeCPtr &node)
        override
        {
          auto retTy = visitType(node, mylang::grammar::PAst::REL_TYPE);
          auto parameters = visitTypes(node, mylang::grammar::PAst::REL_PARAMETER_TYPES);

          auto returnType = constraintOf(retTy);
          if (!returnType->isConcrete()) {
            throw ValidationError(retTy, "function return type must be concrete");
          }

          ::std::vector<ConstrainedTypePtr> params;
          for (auto p : parameters) {
            auto pTy = constraintOf(p);
            if (!pTy->isConcrete()) {
              throw ValidationError(p, "function parameter type must be concrete");
            }
            params.push_back(pTy);
          }
          auto ty = ConstrainedFunctionType::get(returnType, params);
          return ast.createType(ty, node->span(), { { VAst::Relation::PARAMETER_TYPES, parameters },
              { VAst::Relation::TYPE, retTy } });
        }

        GroupNodeCPtr visit_type_grouped(const GroupNodeCPtr &node)
        override
        {
          auto types = visitTypes(node, mylang::grammar::PAst::REL_TYPE);
          if (types.size() != 1) {
            throw ValidationError(node, "expected a single type");
          }

          return types.at(0);
        }

        GroupNodeCPtr visit_struct_member(const GroupNodeCPtr &node)
        override
        {
          auto name = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          return createTypeStructMember(name, type);
        }

        GroupNodeCPtr visit_union_member(const GroupNodeCPtr &node)
        override
        {
          auto discriminant = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          auto name = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          return createTypeUnionMember(discriminant, name, type);
        }

        GroupNodeCPtr visit_discriminant(const GroupNodeCPtr &node)
        override
        {
          auto name = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          return createTypeUnionDiscriminant(name, type);
        }

        GroupNodeCPtr visit_type_process(const GroupNodeCPtr &node)
        override
        {
          auto members = visitGroups(node, mylang::grammar::PAst::REL_MEMBERS);
          ConstrainedProcessType::Inputs inputs;
          ConstrainedProcessType::Outputs outputs;
          for (auto m : members) {
            auto eTy = constraintOf(m);
            auto name = idioma::astutils::getToken(m, VAst::Token::IDENTIFIER)->text;
            if (inputs.count(name) != 0 || outputs.count(name) != 0) {
              notifyError(node, stream << "duplicate process input/output variable " << name);
            }
            if (eTy->self<ConstrainedInputType>()) {
              inputs.insert( { name, eTy->self<ConstrainedInputType>() });
            } else {
              outputs.insert( { name, eTy->self<ConstrainedOutputType>() });
            }
          }
          auto ty = ConstrainedProcessType::get(inputs, outputs);
          return ast.createType(ty, node->span(), { { VAst::Relation::MEMBERS, members } });
        }

        GroupNodeCPtr visit_type_process_input_member(const GroupNodeCPtr &node)
        override
        {
          auto name = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          auto ty = ConstrainedInputType::get(constraintOf(type));
          return createTypeProcessMember(name, ast.createType(ty, node->span()));
        }

        GroupNodeCPtr visit_type_process_output_member(const GroupNodeCPtr &node)
        override
        {
          auto name = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          auto ty = ConstrainedOutputType::get(constraintOf(type));
          return createTypeProcessMember(name, ast.createType(ty, node->span()));
        }

        GroupNodeCPtr visit_type_struct(const GroupNodeCPtr &node)
        override
        {
          auto members = visitGroups(node, mylang::grammar::PAst::REL_MEMBERS);
          ::std::vector<ConstrainedStructType::Member> M;
          ::std::set<::std::string> names;
          auto voidTy = ConstrainedVoidType::create();
          for (auto m : members) {
            auto mType = constraintOf(m);
            auto mName = idioma::astutils::getToken(m, VAst::Token::IDENTIFIER);
            if (!names.insert(mName->text).second) {
              throw ValidationError(mName, "duplicate member name '" + mName->text + "'");
            }
            if (!mType->isConcrete()) {
              throw ValidationError(m,
                  "struct member '" + mName->text + "' is not a concrete type");
            }
            if (voidTy->isSameConstraint(*mType)) {
              throw ValidationError(m,
                  "struct member '" + mName->text + "' must not be a void type");
            }

            M.push_back(ConstrainedStructType::Member(mName->text, mType));
          }
          auto ty = ConstrainedStructType::get(M);
          return ast.createType(ty, node->span(), { { VAst::Relation::MEMBERS, members } });
        }

        GroupNodeCPtr visit_type_union(const GroupNodeCPtr &node)
        override
        {
          auto discriminant = visitGroup(node, mylang::grammar::PAst::REL_DISCRIMINANT);
          auto members = visitGroups(node, mylang::grammar::PAst::REL_MEMBERS);
          auto discriminantType = constraintOf(discriminant);
          auto discriminantName = idioma::astutils::getToken(discriminant, VAst::Token::IDENTIFIER);

          ConstrainedUnionType::Discriminant D(discriminantName->text, discriminantType);

          ::std::set<::std::string> names;
          names.insert(discriminantName->text);

          ::std::vector<ConstrainedUnionType::Member> M;
          for (auto m : members) {
            auto mType = constraintOf(m);
            if (!mType->isConcrete()) {
              throw ValidationError(m, "union member must be concrete");
            }
            auto mName = idioma::astutils::getToken(m, VAst::Token::IDENTIFIER);
            if (!names.insert(mName->text).second) {
              throw ValidationError(mName, "duplicate member name " + mName->text);
            }
            auto mDiscrimnantNode = idioma::astutils::getGroup(m, VAst::Expr::EXPRESSION);
            auto mDiscriminant = getConstAnnotation(mDiscrimnantNode);
            if (!mDiscriminant) {
              throw ValidationError(mDiscrimnantNode, "discriminant is not a constant");
            }
            auto mDiscriminantType = mDiscriminant->constraint();
            if (!discriminantType->canImplicitlyCastFrom(*mDiscriminantType)) {
              throw ValidationError(m, "value does not have the type of the discriminant");
            }

            M.push_back(ConstrainedUnionType::Member(mDiscriminant, mName->text, mType));
          }
          auto ty = ConstrainedUnionType::get(D, M);
          return ast.createType(ty, node->span(), { { VAst::Relation::DISCRIMINANT, discriminant },
              { VAst::Relation::MEMBERS, members } });
        }

        GroupNodeCPtr visit_macro_body(const GroupNodeCPtr &node)
        override
        {
          // ignore the decls; just visit them
          return withNewScope([&](Converter &local) {
            local.visitGroups(node, mylang::grammar::PAst::REL_DECLS);
            return local.visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          });
        }

        GroupNodeCPtr visit_function_body(const GroupNodeCPtr &node)
        override
        {
          auto curCtx = searchContext<StatementContext>();
          auto res = withNewScope(curCtx, [&](Converter &local) {
            auto stmts = local.visitStatements(node,
                mylang::grammar::PAst::REL_STATEMENTS);
            auto body = local.visitExpression(node,
                mylang::grammar::PAst::EXPR_EXPRESSION);
            if (body) {
              stmts.push_back(ast.createReturnStatement(body));
              if (curCtx) {
                curCtx->setReturnDisposition(
                    StatementContext::ReturnDisposition::WILL_RETURN);
              }
            }
            auto fnBody = ast.createFunctionBody(stmts);
            if (constraintOf<ConstrainedVoidType>(fnBody)) {
              if (curCtx && curCtx->getThrowDisposition() !=
                  StatementContext::ThrowDisposition::WILL_THROW) {
                curCtx->setReturnDisposition(
                    StatementContext::ReturnDisposition::WILL_RETURN);
              }
            }
            return fnBody;
          });
          return res;
        }

        GroupNodeCPtr visit_lambda_body(const GroupNodeCPtr &node)
        override
        {
          auto fnBody = withNewScope([&](Converter &local) {
            auto expr = local.visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
            GroupNodeCPtr body;
            if (expr) {
              body = ast.createReturnStatement(expr);
            } else {
              auto statementCtx = ::std::make_shared<StatementContext>(nullptr);
              body = withNewScope(statementCtx, [&](Converter &slocal) {
                    return slocal.visitGroup(node, mylang::grammar::PAst::REL_STATEMENTS);
                  });
            }
            return body;
          });
          auto res = ast.createFunctionBody( { fnBody });

          return res;
        }

        GroupNodeCPtr visit_def_macro(const GroupNodeCPtr &node)
        override
        {
          auto name = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          // do not visit parameters or body just yet
          auto params = ::idioma::astutils::getGroups(node, mylang::grammar::PAst::REL_VARIABLES);
          auto body = ::idioma::astutils::getGroup(node, mylang::grammar::PAst::REL_BODY);
          TokenNodes paramNames;
          ::std::set<std::string> unique;
          for (auto p : params) {
            auto tok = getToken(p, mylang::grammar::PAst::TOK_IDENTIFIER);
            paramNames.push_back(tok);
            if (!unique.insert(tok->text).second) {
              notifyError(tok, stream << "duplicate parameter name " << tok);
              return nullptr;
            }
          }
          auto macro = ::std::make_shared<MacroDefinition>(paramNames, body);
          putDefinition(name, macro, Shadowing::DISALLOW);
          return nullptr;
        }

        ::std::shared_ptr<DefFunctionContext> getFunctionContext(const TokenNodeCPtr &name,
            bool isExtension, bool isExported, bool isTransform)
        {
          auto def = findLocalDefinition(name);
          auto fnDef = ::std::dynamic_pointer_cast<DefFunctionContext>(def);
          if (def && !fnDef) {
            notifyError(name, stream << "name already defined : " << name->text);
            return nullptr;
          }

          if (fnDef) {
            if (fnDef->isExtension && !isExtension) {
              notifyError(name, stream << "name already defined as an extension : " << name->text);
            } else if (!fnDef->isExtension && isExtension) {
              notifyError(name, stream << "name already defined as a function : " << name->text);
            }
          } else {
            auto fnName = createContextName(name->text);

            if ((isTransform || isExported) && !fnName->isFQN()) {
              notifyError(name,
                  stream << "an exported function must be a top-level function : " << name->text);
              return nullptr;
            }
            auto vinfo = createVariable(false, name);

            fnDef = ::std::make_shared<DefFunctionContext>(fnName, vinfo, isExtension, isExported,
                isTransform);
            if (!putDefinition(name, fnDef, Shadowing::DISALLOW_AND_CONTINUE)) {
              return nullptr;
            }
          }
          return fnDef;
        }

        GroupNodeCPtr define_function(const GroupNodeCPtr &node, bool isExtension, bool isTransform)
        {
          bool isExported = node->getNode(mylang::grammar::PAst::REL_EXPORTED) != nullptr;

          auto name = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);

          // transforms have a qname
          auto transformName = getTokens(node, mylang::grammar::PAst::REL_QN);
          if (!transformName.empty()) {
            name = transformName.back();
          }

          auto fnDef = getFunctionContext(name, isExtension, isExported, isTransform);
          if (!fnDef) {
            return nullptr;
          }
          // we must have fnDef be exported
          if (fnDef->isTransform != isTransform) {
            notifyError(node, stream << "functions marked as transforms cannot be overloaded");
            return nullptr;
          }
          if (fnDef->isExported != isExported) {
            notifyError(node, stream << "functions marked for export cannot be overloaded");
            return nullptr;
          }

          return withNewScope(fnDef,
              [&](
                  Converter &fnScope) {
                    GroupNodePtr res;
                    auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
                    auto parameters = fnScope.visitGroups(node,
                        mylang::grammar::PAst::REL_PARAMETERS);

                    if (isExtension) {
                      auto paramTy = getConstraintAnnotation(parameters.at(0));
                      if (hasMember(paramTy->roottype()->type(), name->text)) {
                        notifyError(name, stream << "extension " << name
                            << " conflicts with a member of type "
                            << paramTy);
                      } else if (builtins->hasInstanceBuiltin(paramTy, name->text)) {
                        notifyError(name, stream << "extension " << name
                            << " conflicts with a builtin definition of type "
                            << paramTy);
                      } else if (library->hasInstanceBuiltin(paramTy, name->text)) {
                        notifyError(name, stream << "extension " << name
                            << " conflicts with a library definition of type "
                            << paramTy);
                      }
                    }
                    ConstrainedTypePtr returnType = type ? getConstraintAnnotation(type)
                    : nullptr;

                    ::std::vector<ConstrainedTypePtr> parameterTypes;
                    for (auto p : parameters) {
                      parameterTypes.push_back(getConstraintAnnotation(p));
                    }

                    if (isTransform) {
                      assert(returnType && "Internal error; return type required for transforms");
                      ::std::set<NamePtr> transformArgs;
                      for (auto p : parameterTypes) {
                        auto namedP = p->self<ConstrainedNamedType>();
                        if (!namedP) {
                          notifyError(name, stream << "transform arguments must be named types: " << name
                              << p);
                          break;
                        }
                        if (namedP->isSameConstraint(*returnType)) {
                          notifyError(name, stream << "transform target type must not also be a parameter type: " << name
                              << p);
                        }
                        transformArgs.insert(namedP->name());
                      }
                      if (transformArgs.size() != parameterTypes.size()) {
                        notifyError(name, stream << "transform parameters must have different types: " << name);
                      }
                    }

                    auto b = ::idioma::astutils::getGroup(node,
                        mylang::grammar::PAst::REL_BODY);

                    if (!b && !returnType) {
                      notifyError(node,
                          stream << "forward declaration must have a return type");
                      return res;
                    }
                    auto n = fnDef->addEntry(name, parameterTypes, returnType, b != nullptr);
                    if (!n) {
                      if (fnDef->isExported || fnDef->isTransform) {
                        notifyError(node, stream
                            << "functions marked for export cannot be overloaded");
                      } else {
                        notifyError(node, stream << "ambiguous function definition");
                      }
                      return res;
                    }
                    if (!b) {
                      return res;
                    }
                    auto statementCtx = ::std::make_shared<StatementContext>(nullptr);
                    auto body = fnScope.withNewScope(statementCtx, [&](Converter &conv) {
                          return conv.visit(b);
                        });

                    if (statementCtx->getReturnDisposition() ==
                        StatementContext::ReturnDisposition::MAY_RETURN) {
                      if (statementCtx->getThrowDisposition() !=
                          StatementContext::ThrowDisposition::WILL_THROW) {
                        notifyError(node, stream << "missing return");
                      }
                    }

                    DefFunctionContext::Entry &e = fnDef->getEntry(n);
                    if (isExported) {
                      res = ast.createDefineExportedFunction(name, n, type, parameters, body);

                      // as the last step, verify the paramter and result types
                      auto fnTy = constraintOf<ConstrainedFunctionType>(res);

                      if (!isExportable(fnTy->returnType)) {
                        notifyError(node,stream << "return type is not exportable");
                      }
                      for (size_t i=0;i<fnTy->parameters.size();++i) {
                        if (!isExportable(fnTy->parameters.at(i))) {
                          notifyError(node,stream << "parameter at position " << (i+1) << " is not exportable");
                        }
                      }
                    } else {
                      res = ast.createDefineFunction(name, n,fnDef->vinfo, type, parameters, body);
                    }
                    if (isTransform) {

                      // as the last step, verify the paramter and result types
                      auto fnTy = constraintOf<ConstrainedFunctionType>(res);
                      if (!isSerializable(fnTy->returnType)) {
                        notifyError(node,stream << "return type is not serializable");
                      }
                      for (size_t i=0;i<fnTy->parameters.size();++i) {
                        if (!isSerializable(fnTy->parameters.at(i))) {
                          notifyError(node,stream << "parameter at position " << (i+1) << " is not serializable");
                        }
                      }

                      setTagAnnotation(res, "transform");
                    }
                    e.setReturnType(constraintOf<ConstrainedFunctionType>(res)->returnType);

                    return res;
                  });
        }

        GroupNodeCPtr visit_def_extension_function(const GroupNodeCPtr &node)
        override
        {
          return define_function(node, true, false);
        }

        GroupNodeCPtr visit_def_function(const GroupNodeCPtr &node)
        override
        {
          return define_function(node, false, false);
        }

        GroupNodeCPtr visit_def_transform(const ::std::vector<TokenNodeCPtr> &names, size_t i,
            const GroupNodeCPtr &node)
        {
          auto name = names.at(i);
          if (i == names.size() - 1) {
            // create the function
            auto tx = define_function(node, false, true);

            return tx;
          } else {
            ScopePtr unitScope;
            ::std::shared_ptr<DefUnitContext> defUnit;
            auto unitName = createContextName(name->text);
            auto curDef = findLocalDefinition(name);
            if (curDef) {
              defUnit = curDef->self<DefUnitContext>();
            }
            if (!defUnit) {
              defUnit = DefUnitContext::makeContext(unitName, scope);
              if (!putDefinition(name, defUnit, Shadowing::DISALLOW)) {
                return nullptr;
              }
            }

            return withNewScope(defUnit->getScope(), [&](Converter &local) {
              assert (local.current_unit == defUnit);
              auto decl = local.visit_def_transform(names,i+1,node);
              return ast.createDefineNamespace(name, unitName, {decl});
            });
          }
        }

        GroupNodeCPtr visit_def_transform(const GroupNodeCPtr &node)
        override
        {
          auto transformName = getTokens(node, mylang::grammar::PAst::REL_QN);
          return visit_def_transform(transformName, 0, node);
        }

        GroupNodeCPtr visit_pattern_any(const GroupNodeCPtr &node)
        override
        {
          auto pattern = Pattern::create();
          auto res = GroupNode::create(LocalNode::DEF_TYPE_PATTERN, {
              { LocalRelation::SOURCE, node } });
          setPatternAnnotation(res, pattern);
          return res;
        }

        GroupNodeCPtr visit_pattern_primitive(const GroupNodeCPtr &node)
        override
        {
          auto ty = visitType(node, mylang::grammar::PAst::REL_TYPE);
          auto pattern = ConstrainedTypePattern::create(constraintOf(ty));
          auto res = GroupNode::create(LocalNode::DEF_TYPE_PATTERN, {
              { LocalRelation::SOURCE, node } });
          setPatternAnnotation(res, pattern);
          return res;
        }

        GroupNodeCPtr visit_pattern_mutable(const GroupNodeCPtr &node)
        override
        {
          auto element = visitPattern(node, mylang::grammar::PAst::REL_PATTERN);
          auto pattern = MutablePattern::create(patternOf(element));
          auto res = GroupNode::create(LocalNode::DEF_TYPE_PATTERN, {
              { LocalRelation::SOURCE, node } });
          setPatternAnnotation(res, pattern);
          return res;
        }

        GroupNodeCPtr visit_pattern_opt(const GroupNodeCPtr &node)
        override
        {
          auto element = visitPattern(node, mylang::grammar::PAst::REL_PATTERN);
          auto pattern = OptPattern::create(patternOf(element));
          auto res = GroupNode::create(LocalNode::DEF_TYPE_PATTERN, {
              { LocalRelation::SOURCE, node } });
          setPatternAnnotation(res, pattern);
          return res;
        }

        ::std::shared_ptr<const ArrayPattern::Range> createDimensionPattern(
            const GroupNodeCPtr &dimension)
        {
          auto dimension_kind = dimension->type.value<mylang::grammar::PAst::GroupType>();

          auto minSize = getConstantInteger(
              visitExpression(dimension, mylang::grammar::PAst::REL_MIN_SIZE));
          auto maxSize = getConstantInteger(
              visitExpression(dimension, mylang::grammar::PAst::REL_MAX_SIZE));
          auto fixedSize = getConstantInteger(
              visitExpression(dimension, mylang::grammar::PAst::REL_SIZE));

          if (dimension_kind->key == mylang::grammar::PAst::GRP_ARRAY_ANY_DIMENSION_PATTERN) {
            return nullptr;
          } else if (fixedSize) {
            auto sz = fixedSize->getUnsignedInteger();
            return ::std::make_shared<ArrayPattern::Range>(sz, sz);
          } else if (minSize && maxSize) {
            auto min = minSize->getUnsignedInteger();
            auto max = maxSize->getUnsignedInteger();
            return ::std::make_shared<ArrayPattern::Range>(min, max);
          } else if (minSize) {
            auto min = minSize->getUnsignedInteger();
            return ::std::make_shared<ArrayPattern::Range>(min, ::std::nullopt);
          } else if (maxSize) {
            auto max = maxSize->getUnsignedInteger();
            return ::std::make_shared<ArrayPattern::Range>(0, max);
          } else {
            return ::std::make_shared<ArrayPattern::Range>(0, std::nullopt);
          }
        }

        GroupNodeCPtr visit_pattern_array(const GroupNodeCPtr &node)
        override
        {
          ::std::vector<GroupNodeCPtr> dimensions;
          GroupNodeCPtr element_node = node;

          // see the corresponding code in visit_type_array!
          while (element_node->type.value<mylang::grammar::PAst::GroupType>()->key
              == mylang::grammar::PAst::GRP_PATTERN_ARRAY) {

            dimensions.push_back(
                idioma::astutils::getGroup(element_node, mylang::grammar::PAst::REL_DIMENSION));

            element_node = idioma::astutils::getGroup(element_node,
                mylang::grammar::PAst::REL_PATTERN);
          }
          auto element = visitPattern(element_node);

          // figure out which kind of pattern
          Pattern::Ptr pattern = patternOf(element);
          for (auto dim = dimensions.begin(); dim != dimensions.end(); ++dim) {
            auto range = createDimensionPattern(*dim);
            pattern = ArrayPattern::create(pattern, range);
          }
          auto res = GroupNode::create(LocalNode::DEF_TYPE_PATTERN, {
              { LocalRelation::SOURCE, node } });
          setPatternAnnotation(res, pattern);
          return res;
        }

        GroupNodeCPtr visit_pattern_grouped(const GroupNodeCPtr &node)
        override
        {
          auto patterns = visitTypes(node, mylang::grammar::PAst::REL_PATTERNS);
          if (patterns.size() != 1) {
            throw ValidationError(node, "expected a single pattern");
          }

          return patterns.at(0);
        }

        GroupNodeCPtr visit_pattern_function(const GroupNodeCPtr &node)
        override
        {
          auto retTy = visitPattern(node, mylang::grammar::PAst::REL_PATTERN);
          auto parameters = visitPatterns(node, mylang::grammar::PAst::REL_PATTERNS);

          auto returnType = patternOf(retTy);
          ::std::vector<PatternPtr> params;
          for (auto p : parameters) {
            params.push_back(patternOf(p));
          }
          auto pattern = FunctionPattern::create(returnType, params);
          auto res = GroupNode::create(LocalNode::DEF_TYPE_PATTERN, {
              { LocalRelation::SOURCE, node } });
          setPatternAnnotation(res, pattern);
          return res;
        }

        GroupNodeCPtr visit_pattern_bit(const GroupNodeCPtr &node)
        override
        {
          auto pattern = BitPattern::create();
          auto res = GroupNode::create(LocalNode::DEF_TYPE_PATTERN, {
              { LocalRelation::SOURCE, node } });
          setPatternAnnotation(res, pattern);
          return res;
        }

        GroupNodeCPtr visit_pattern_boolean(const GroupNodeCPtr &node)
        override
        {
          auto pattern = BooleanPattern::create();
          auto res = GroupNode::create(LocalNode::DEF_TYPE_PATTERN, {
              { LocalRelation::SOURCE, node } });
          setPatternAnnotation(res, pattern);
          return res;
        }

        GroupNodeCPtr visit_pattern_byte(const GroupNodeCPtr &node)
        override
        {
          auto pattern = BytePattern::create();
          auto res = GroupNode::create(LocalNode::DEF_TYPE_PATTERN, {
              { LocalRelation::SOURCE, node } });
          setPatternAnnotation(res, pattern);
          return res;
        }

        GroupNodeCPtr visit_pattern_char(const GroupNodeCPtr &node)
        override
        {
          auto pattern = CharPattern::create();
          auto res = GroupNode::create(LocalNode::DEF_TYPE_PATTERN, {
              { LocalRelation::SOURCE, node } });
          setPatternAnnotation(res, pattern);
          return res;
        }

        GroupNodeCPtr visit_pattern_integer(const GroupNodeCPtr &node)
        override
        {
          auto pattern = IntegerPattern::create();
          auto res = GroupNode::create(LocalNode::DEF_TYPE_PATTERN, {
              { LocalRelation::SOURCE, node } });
          setPatternAnnotation(res, pattern);
          return res;
        }

        GroupNodeCPtr visit_pattern_real(const GroupNodeCPtr &node)
        override
        {
          auto pattern = RealPattern::create();
          auto res = GroupNode::create(LocalNode::DEF_TYPE_PATTERN, {
              { LocalRelation::SOURCE, node } });
          setPatternAnnotation(res, pattern);
          return res;
        }

        GroupNodeCPtr visit_pattern_string(const GroupNodeCPtr &node)
        override
        {
          auto pattern = StringPattern::create();
          auto res = GroupNode::create(LocalNode::DEF_TYPE_PATTERN, {
              { LocalRelation::SOURCE, node } });
          setPatternAnnotation(res, pattern);
          return res;
        }

        GroupNodeCPtr visit_pattern_struct(const GroupNodeCPtr &node)
        override
        {
          auto pattern = StructPattern::create();
          auto res = GroupNode::create(LocalNode::DEF_TYPE_PATTERN, {
              { LocalRelation::SOURCE, node } });
          setPatternAnnotation(res, pattern);
          return res;
        }

        GroupNodeCPtr visit_pattern_tuple(const GroupNodeCPtr &node)
        override
        {
          auto pattern = TuplePattern::create();
          auto res = GroupNode::create(LocalNode::DEF_TYPE_PATTERN, {
              { LocalRelation::SOURCE, node } });
          setPatternAnnotation(res, pattern);
          return res;
        }

        GroupNodeCPtr visit_pattern_union(const GroupNodeCPtr &node)
        override
        {
          auto pattern = UnionPattern::create();
          auto res = GroupNode::create(LocalNode::DEF_TYPE_PATTERN, {
              { LocalRelation::SOURCE, node } });
          setPatternAnnotation(res, pattern);
          return res;
        }

        GroupNodeCPtr visit_named_type_pattern(const GroupNodeCPtr &node)
        override
        {
          auto def = findDefinition(getTokens(node, mylang::grammar::PAst::REL_QN));
          auto patCtx = ::std::dynamic_pointer_cast<const DefPatternContext>(def);
          GroupNodeCPtr pattern;
          if (patCtx) {
            pattern = patCtx->getPattern();
          } else {
            ConstrainedTypePtr ty;
            auto defalias = ::std::dynamic_pointer_cast<const DefTypeAliasContext>(def);
            if (defalias) {
              ty = constraintOf(defalias->alias);
            } else {
              auto typeCtx = ::std::dynamic_pointer_cast<const DefTypeContext>(def);
              if (typeCtx) {
                ty = typeCtx->getType();
              }
            }
            if (ty) {
              auto p = GroupNode::create(LocalNode::DEF_TYPE_PATTERN, { { LocalRelation::SOURCE,
                  node } });
              setPatternAnnotation(p, ConstrainedTypePattern::create(ty));
              pattern = p;
            }
          }
          if (!pattern) {
            notifyInternalError(node, stream << "no such pattern or type");
          }
          return pattern;
        }

        GroupNodeCPtr visit_constrained_type_pattern(const GroupNodeCPtr &node)
        override
        {
          auto pat = visitPattern(node, mylang::grammar::PAst::REL_PATTERN);
          notifyError(node, stream << "constrained pattern unsupported");
          return pat;
        }

        GroupNodeCPtr visit_def_type_pattern(const GroupNodeCPtr &node)
        override
        {
          auto tok = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          auto name = mylang::names::Name::create(tok->text);
          auto def = ::std::make_shared<DefPatternContext>(name);
          putDefinition(tok, def, Shadowing::WARN);
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          auto pattern = visitPattern(node, mylang::grammar::PAst::REL_PATTERN);
          if (!pattern) {
            auto p = GroupNode::create(LocalNode::DEF_TYPE_PATTERN,
                { { LocalRelation::SOURCE, tok } });
            setPatternAnnotation(p, Pattern::create());
            pattern = p;
          }
          def->setPattern(pattern);
          auto res = GroupNode::create(LocalNode::GENERIC_PARAMETER);
          setPatternAnnotation(res, getPatternAnnotation(pattern));
          setNameAnnotation(res, name);
          return res; // nothing needs to be returned at this time
        }

        GroupNodeCPtr visit_def_value_pattern(const GroupNodeCPtr &node)
        override
        {
          auto tok = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          auto name = mylang::names::Name::create(tok->text);
          auto def = ::std::make_shared<DefPatternContext>(name);
          putDefinition(tok, def, Shadowing::WARN);
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          auto p = GroupNode::create(LocalNode::DEF_VALUE_PATTERN,
              { { LocalRelation::SOURCE, tok } });
          setPatternAnnotation(p, ConstrainedTypePattern::create(constraintOf(type)));
          def->setPattern(p);
          notifyError(node, stream << "value patterns are not yet supported");
          return nullptr;
        }

        GroupNodeCPtr visit_def_generic_parameter(const GroupNodeCPtr&)
        override
        {
          throw ::std::runtime_error("unexpected call to visit_def_generic_parameter");
        }

        GenericParameter create_generic_parameter(const GroupNodeCPtr &node)
        {
          GenericParameter param;
          param.id = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          param.pattern = visitPattern(node, mylang::grammar::PAst::REL_PATTERN);
          return param;
        }

        // a declaration (no body) or definition of a generic function
        //
        GroupNodeCPtr define_generic_function(const GroupNodeCPtr &node, bool isExtension)
        {
          auto name = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);

          auto fnDef = getFunctionContext(name, isExtension, false, false);
          if (!fnDef) {
            return nullptr;
          }
          if (fnDef->isExported) {
            notifyError(node,
                stream
                    << "functions marked for export cannot a have generic functions of the same name");
            return nullptr;
          }

          return withNewScope(fnDef, [&](Converter &genericScope) {
            GroupNodeCPtr res;
            /* first, define the type variables */
            auto genericParameters = genericScope.visitGroups(node,
                mylang::grammar::PAst::REL_DEF_PATTERNS);

            /* pick up the parameters */
            ::std::vector<::std::pair<TokenNodeCPtr, PatternPtr>> genericPatternParams;
            ::std::set<::std::string> paramNames;
            ::std::vector<GenericParameter> params;
            ::std::vector<PatternPtr> paramPatterns;

            for (auto gp : genericParameters) {
              auto tok = TokenNode::create(getNameAnnotation(gp)->localName(),
                  gp->span());
              auto anno = patternOf(gp);
              if (anno) {
                genericPatternParams.emplace_back(tok, anno);
              } else {
                notifyError(gp, stream
                    << "non-pattern generic parameters are not yet supported");
              }
            }

            for (auto p : ::idioma::astutils::getGroups(node,
                    mylang::grammar::PAst::REL_PARAMETERS)) {
              auto param = genericScope.create_generic_parameter(p);
              if (param.pattern == nullptr) {
                return res;
              }
              if (paramNames.count(param.id->text) == 1) {
                notifyError(param.id, stream
                    << "duplicate parameter name: " + param.id->text);
              }
              params.push_back(param);
              paramNames.insert(param.id->text);
              paramPatterns.push_back(patternOf(param.pattern));
            }

            /* pick up the return type pattern, if any */
            GroupNodeCPtr returnTypePattern = genericScope.visitPattern(node,
                mylang::grammar::PAst::REL_TYPE);
            GroupNodeCPtr original_body = ::idioma::astutils::getGroup(node,
                mylang::grammar::PAst::REL_BODY);

            if (!original_body && !returnTypePattern) {
              notifyError(node,
                  stream << "forward declaration must have a return type");
              return res;
            }

            // associate the generic with the function name
              auto n = fnDef->getGenerics().addEntry(name, genericPatternParams,
                  patternOf(returnTypePattern),
                  paramPatterns,
                  original_body != nullptr);
              if (!n) {
                notifyError(node, stream << "ambiguous generic function definition");
                return res;
              }

              // if we have a body, then we can generate an instantiator for the node.
              if (original_body) {
                auto provider = ::std::make_shared<GenericInstantiationProvider>();
                auto &e = fnDef->getGenerics().getEntry(n);
                provider->key = n;
                provider->body = original_body;
                provider->defName = e.defName;
                provider->extension = isExtension;
                provider->parameters = params;
                provider->returnType = returnTypePattern;
                provider->scope = ::std::make_unique<Converter>(*this);

                generics->emplace(provider->key, provider);
                res = ast.createDefineGenericFunction(e.defName);

                fnDef->getGenerics().getEntry(n).provider = [genericScope](
                    const GenericInstantiationRequest &req) {
                  return genericScope.instantiateFunction(req);
                };
              }
              return res;
            });
        }

        GroupNodeCPtr visit_def_generic_function(const GroupNodeCPtr &node)
        override
        {
          return define_generic_function(node, false);
        }

        GroupNodeCPtr visit_def_generic_extension_function(const GroupNodeCPtr &node)
        override
        {
          return define_generic_function(node, true);
        }

        GroupNodeCPtr define_namespace(const ::std::vector<TokenNodeCPtr> &names, size_t i,
            const GroupNodeCPtr &node)
        {
          // unfortunately, the recursion needs to be terminated in this way
          // and not using a case
          assert(i < names.size());

          ::std::shared_ptr<DefUnitContext> defUnit;
          auto name = names.at(i);
          auto curDef = findLocalDefinition(name);
          if (curDef) {
            defUnit = curDef->self<DefUnitContext>();
          }
          if (!defUnit) {
            auto unitName = createContextName(name->text);
            defUnit = DefUnitContext::makeContext(unitName, scope);
            if (!putDefinition(name, defUnit, Shadowing::DISALLOW)) {
              return nullptr;
            }
          }

          return withNewScope(defUnit->getScope(), [&](Converter &local) {
            assert (local.current_unit == defUnit);
            ++i;
            if (i==names.size()) {
              auto decls = local.visitGroups(node, mylang::grammar::PAst::REL_DECLS);
              return ast.createDefineNamespace(name, defUnit->name, decls);
            } else {
              auto decl = local.define_namespace(names,i,node);
              return ast.createDefineNamespace(name, defUnit->name, {decl});
            }
          });
        }

        GroupNodeCPtr visit_def_namespace(const GroupNodeCPtr &node)
        override
        {
          ::std::shared_ptr<DefUnitContext> defUnit;
          auto names = getTokens(node, mylang::grammar::PAst::REL_QN);
          return define_namespace(names, 0, node);
        }

        GroupNodeCPtr define_variable_or_value(const TokenNodeCPtr &name, GroupNodeCPtr type,
            GroupNodeCPtr init, const VariablePtr &vinfo)
        {
          auto declName = createContextName(name->text);

          // TODO: needs to be more granular; consider passing a lambda instead of constant
          // to determine if shadowing is allowed
          auto shadowing = Shadowing::DISALLOW;
          switch (vinfo->kind) {
          case Variable::KIND_PARAMETER:
            shadowing = Shadowing::ALLOW;
            break;
          default:
            // ignore
            break;
          }

          // define the variable, before visiting the expression, but don't define the type yet
          // this will cause any use of the variable in the initializer expression to fail

          auto res = ast.createDefineVariable(name, declName, vinfo, type, init);
          declaredVariables.push_back(res);
          auto decl = ::std::make_shared<VariableDefinition>(res, vinfo, containerContext());
          if (!putDefinition(name, decl, shadowing)) {
            return nullptr;
          }

          // if we don't have an initializer, then we need to check first
          if (!init && vinfo->scope == Variable::SCOPE_GLOBAL
              && vinfo->kind == Variable::KIND_VALUE) {
            notifyError(name, stream << "global values must have an initializer");
          }

          return res;
        }

        GroupNodeCPtr define_variable_or_value(const GroupNodeCPtr &node, GroupNodeCPtr type,
            const VariablePtr &vinfo)
        {
          auto name = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          auto init = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          return define_variable_or_value(name, type, init, vinfo);
        }

        GroupNodeCPtr visit_def_parameter(const GroupNodeCPtr &node)
        override
        {
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          return define_variable_or_value(node, type, Variable::createParameter());
        }

        VariablePtr createVariable(bool isMutable, const NodeCPtr &sourceLocation)
        {
          if (isGlobalContext()) {
            if (isMutable) {
              notifyInternalError(sourceLocation, stream << "Global value requested to be mutable");
            }
            return Variable::createGlobal();
          }
          auto process = parentContext<DefProcessContext>();
          if (process) {
            return Variable::createProcessVariable(isMutable);
          }
          return Variable::createLocal(isMutable);
        }

        GroupNodeCPtr visit_def_value(const GroupNodeCPtr &node)
        override
        {
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          auto var = createVariable(false, node);
          return define_variable_or_value(node, type, var);
        }

        GroupNodeCPtr visit_def_variable(const GroupNodeCPtr &node)
        override
        {
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          auto var = createVariable(true, node);
          return define_variable_or_value(node, type, var);
        }

        GroupNodeCPtr visit_def_alias(const GroupNodeCPtr &node)
        override
        {
          auto tokens = getTokens(node, mylang::grammar::PAst::REL_QN);
          if (tokens.size() < 2) {
            notifyError(node, stream << "a qualified name is required");
          }
          auto def = findDefinition(tokens);

          if (!def) {
            return nullptr;
          }

          // an alias always introduces the last component
          auto name = tokens.at(tokens.size() - 1);

          // save the type
          auto alias = ::std::make_shared<DefAliasContext>(def);
          if (!putDefinition(name, alias, Shadowing::DISALLOW)) {
            return nullptr;
          }
          // we do not store type aliases in the ast
          return nullptr;
        }

        GroupNodeCPtr visit_def_typename(const GroupNodeCPtr &node)
        override
        {
          auto name = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);

          // save the type
          auto alias = ::std::make_shared<DefTypeAliasContext>(type);
          if (!putDefinition(name, alias, Shadowing::DISALLOW)) {
            return nullptr;
          }
          // we do not store type aliases in the ast
          return nullptr;
        }

        GroupNodeCPtr visit_def_type(const GroupNodeCPtr &node)
        override
        {
          auto name = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          auto def = findLocalDefinition(name);
          auto decl = ::std::dynamic_pointer_cast<DefTypeContext>(def);

          auto typeNode = idioma::astutils::getGroup(node, mylang::grammar::PAst::REL_TYPE);

          if (!decl) {
            auto declName = createContextName(name->text);
            decl = ::std::make_shared<DefTypeContext>(declName);
            if (!putDefinition(name, decl, Shadowing::DISALLOW)) {
              return nullptr;
            }
          }

          if (!typeNode) {
            // definition is redundant
            return nullptr;
          }

          if (decl->getImplType()) {
            notifyError(node, stream << "type " << name << " already defined");
            return nullptr;
          }

          auto impl = visitType(node, mylang::grammar::PAst::REL_TYPE);
          // visit the definition after the type name has been declared
          // so that the we can write recursive types
          decl->setImplType(impl);

          return ast.createDefineType(name, decl->name, decl->getImplType());
        }

        GroupNodeCPtr visit_def_process(const GroupNodeCPtr &node)
        override
        {
          auto name = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          auto declName = createContextName(name->text);
          auto ctx = ::std::make_shared<DefProcessContext>(declName, name->span());
          auto sctx = ::std::make_shared<StatementContext>(nullptr);

          if (!putDefinition(name, ctx, Shadowing::DISALLOW)) {
            return nullptr;
          }

          return withNewScope( { ctx, sctx }, [&](Converter &local) {
            auto statements = ::idioma::astutils::getGroups(node,
                mylang::grammar::PAst::REL_STATEMENTS);

            GroupNodes body;

            auto visitGroupsByType = [&](Converter &conv,
                const ::std::vector<mylang::grammar::PAst::GroupType> &gtypes) {
              bool visited = false;
              for (auto s : statements) {
                for (auto t : gtypes) {
                  if (s->hasType(t)) {
                    auto g = conv.visit(s);
                    if (g) {
                      visited = true;
                      body.push_back(g);
                    }
                  }
                }
              }
              return visited;
            };

            // generated nested processes first, so we don't have to deal with shadowing issues
              visitGroupsByType(local, {mylang::grammar::PAst::GRP_DEF_PROCESS});

              // first, visit the input and output definitions, because they are needed to construct the type of the process
              visitGroupsByType(local, {mylang::grammar::PAst::GRP_PROCESS_INPUT,
                    mylang::grammar::PAst::GRP_PROCESS_OUTPUT});

              // we have enough info to create the type for the process
              ctx->createType();

              // get all type and variable definitions so they are available for use in the functions, variables, etc.
              visitGroupsByType(local, {mylang::grammar::PAst::GRP_DEF_TYPE,
                    mylang::grammar::PAst::GRP_DEF_TYPENAME,
                    mylang::grammar::PAst::GRP_VAL_STMT,
                    mylang::grammar::PAst::GRP_VAR_STMT,
                    mylang::grammar::PAst::GRP_DEF_MACRO,
                    mylang::grammar::PAst::GRP_DEF_FUNCTION,
                    mylang::grammar::PAst::GRP_DEF_EXTENSION_FUNCTION,
                    mylang::grammar::PAst::GRP_DEF_GENERIC_FUNCTION,
                    mylang::grammar::PAst::GRP_DEF_GENERIC_EXTENSION_FUNCTION
                  });

              // process blocks have access to all functions and variables, regardless of where they are defined
              visitGroupsByType(local, {mylang::grammar::PAst::GRP_PROCESS_BLOCK});

              // setup the statement context with the initialization status for each variable
              if (!visitGroupsByType(local,
                      { mylang::grammar::PAst::GRP_PROCESS_CONSTRUCTOR})) {
                for (auto v : ctx->unitializedStateValues) {
                  if (sctx->getValueDisposition(v->name) !=
                      StatementContext::VariableDisposition::INITIALIZED) {
                    notifyError(v->decl,
                        stream << "Process value " << v->name->localName()
                        << " may not be initialized");
                  }
                }
                for (auto v : ctx->unitializedStateVariables) {
                  if (sctx->getVariableDisposition(v->name) !=
                      StatementContext::VariableDisposition::INITIALIZED) {
                    notifyError(v->decl, stream << "Process variable "
                        << v->name->localName()
                        << " may not be initialized");
                  }
                }
                // make sure all variables and values have been initialized
              }

              return ast.createDefineProcess(name, declName, ctx->type(), body);
            });
        }

        GroupNodeCPtr visit_process_input(const GroupNodeCPtr &node)
        override
        {
          auto proc = parentContext<DefProcessContext>();
          auto name = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER)->text;
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          auto ty = ConstrainedInputType::get(getConstraintAnnotation(type));
          type = ast.createType(ty, node->span(), { { VAst::Relation::TYPE, node } });
          auto info = Variable::createInputVariable();
          auto var = define_variable_or_value(node, type, info);
          auto def = searchDefinition<VariableDefinition>(getNameAnnotation(var));
          proc->inputs[name] = ty;
          return var;
        }

        GroupNodeCPtr visit_process_output(const GroupNodeCPtr &node)
        override
        {
          auto proc = parentContext<DefProcessContext>();
          auto name = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER)->text;
          auto type = visitType(node, mylang::grammar::PAst::REL_TYPE);
          auto ty = ConstrainedOutputType::get(getConstraintAnnotation(type));
          type = ast.createType(ty, node->span(), { { VAst::Relation::TYPE, node } });
          auto info = Variable::createOutputVariable();
          auto var = define_variable_or_value(node, type, info);
          auto def = searchDefinition<VariableDefinition>(getNameAnnotation(var));
          proc->outputs[name] = ty;
          return var;
        }

        GroupNodeCPtr visit_process_constructor(const GroupNodeCPtr &node)
        override
        {
          auto sctx = currentContext<StatementContext>();
          auto process = parentContext<DefProcessContext>();
          if (!process) {
            notifyInternalError(node, stream << "constructor definition in an unknown context");
            return nullptr;
          }
          auto name = mylang::names::Name::create("constructor", process->name);

          auto ctor = withNewScope([&](Converter &local) {
            auto parameters = local.visitGroups(node,
                mylang::grammar::PAst::REL_PARAMETERS);
            auto ctor_ctx = ::std::make_shared<ProcessConstructorContext>();

            auto call_ctor = local.withNewScope(ctor_ctx, [&](Converter &bodyScope) {
                  return bodyScope.visitGroup(node,
                      mylang::grammar::PAst::REL_INIT_STATEMENT);
                });

            auto stmt_ctx = ::std::make_shared<StatementContext>(sctx);

            auto stmts = local.withNewScope( {ctor_ctx, stmt_ctx},
                [&](Converter &bodyScope) {
                  GroupNodes body = bodyScope.visitStatements(
                      node,
                      mylang::grammar::PAst::REL_STATEMENTS);
                  // ensure that all uninitialized variables have been initialized
              if (!ctor_ctx->call_to_own_constructor) {
                for (auto v : process->unitializedStateValues) {
                  if (bodyScope.getValueDisposition(
                          stmt_ctx, v->name) !=
                      StatementContext::VariableDisposition::INITIALIZED) {
                    notifyError(v->decl, stream
                        << "Process value "
                        << v->name->localName()
                        << " may not be initialized");
                  }
                }
                for (auto v : process->unitializedStateVariables) {
                  if (bodyScope.getVariableDisposition(
                          stmt_ctx, v->name) !=
                      StatementContext::VariableDisposition::INITIALIZED) {
                    notifyError(v->decl, stream
                        << "Process variable "
                        << v->name->localName()
                        << " may not be initialized");
                  }
                }
              }
              return ast.createStatementBlock(body);
            });

        return ast.createProcessConstructor(name, parameters, call_ctor, stmts);
      });

          if (ctor && !process->addConstructor(ctor)) {
            notifyError(node, stream << "duplicate constructor");
          }
          return ctor;
        }

        GroupNodeCPtr visit_process_block(const GroupNodeCPtr &node)
        override
        {
          auto sctx = currentContext<StatementContext>();
          auto process = parentContext<DefProcessContext>();
          if (!process) {
            notifyInternalError(node, stream << "processing block outside a process definition");
            return nullptr;
          }

          // create a statement context to prevent statements from interfering with each other
          auto ctx = ::std::make_shared<StatementContext>(sctx);

          auto name = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          NamePtr declName;
          if (name) {
            declName = createContextName(name->text);
          } else {
            declName = createContextName("anonymous");
          }

          auto cond = visitExpression(node, mylang::grammar::PAst::EXPR_CONDITION);

          auto block = withNewScope(ctx, [&](Converter &local) {
            auto body = local.visitGroup(node, mylang::grammar::PAst::REL_BODY);
            return ast.createProcessBlock(name, declName, cond, body);
          });
          if (name) {
            if (!scope->put(name->text, block)) {
              notifyError(name, stream << "name already defined in process " << name);
            }
          }
          return block;
        }

        GroupNodeCPtr visit_val_stmt(const GroupNodeCPtr &node)
        override
        {
          auto procCtx = parentContext<DefProcessContext>();
          auto curCtx = searchContext<StatementContext>();
          auto res = visit_def_value(node);
          auto name = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);

          auto vardef = ::std::dynamic_pointer_cast<VariableDefinition>(findLocalDefinition(name));
          if (curCtx) {
            curCtx->setValueDisposition(vardef->name,
                vardef->hasInitialValue() ?
                    StatementContext::VariableDisposition::INITIALIZED :
                    StatementContext::VariableDisposition::UNINITIALIZED);
          }
          if (procCtx && !vardef->hasInitialValue()) {
            procCtx->unitializedStateValues.push_back(vardef);
          }
          return res;
        }

        GroupNodeCPtr visit_var_stmt(const GroupNodeCPtr &node)
        override
        {
          auto procCtx = parentContext<DefProcessContext>();
          auto curCtx = searchContext<StatementContext>();
          auto res = visit_def_variable(node);
          auto name = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);

          auto vardef = ::std::dynamic_pointer_cast<VariableDefinition>(findLocalDefinition(name));
          if (curCtx) {
            curCtx->setVariableDisposition(vardef->name,
                vardef->hasInitialValue() ?
                    StatementContext::VariableDisposition::INITIALIZED :
                    StatementContext::VariableDisposition::UNINITIALIZED);
          }
          if (procCtx && !vardef->hasInitialValue()) {
            procCtx->unitializedStateVariables.push_back(vardef);
          }

          return res;
        }

        GroupNodeCPtr visit_stmt_block(const GroupNodeCPtr &node)
        override
        {
          auto curCtx = searchContext<StatementContext>();
          auto blockCtx = ::std::make_shared<StatementContext>(curCtx);
          auto res = withNewScope(blockCtx, [&](Converter &local) {
            auto stmts = local.visitStatements(node, mylang::grammar::PAst::REL_STATEMENTS);
            return ast.createStatementBlock(stmts);
          });
          if (curCtx) {
            if (curCtx->getReturnDisposition() < blockCtx->getReturnDisposition()) {
              curCtx->setReturnDisposition(blockCtx->getReturnDisposition());
            }
            if (curCtx->getThrowDisposition() < blockCtx->getThrowDisposition()) {
              curCtx->setThrowDisposition(blockCtx->getThrowDisposition());
            }
            if (curCtx->getContinueDisposition() < blockCtx->getContinueDisposition()) {
              curCtx->setContinueDisposition(blockCtx->getContinueDisposition());
            }
            if (curCtx->getBreakDisposition() < blockCtx->getBreakDisposition()) {
              curCtx->setBreakDisposition(blockCtx->getBreakDisposition());
            }
            for (auto e : blockCtx->inheritedValueDispositions()) {
              curCtx->setValueDisposition(e.first, e.second);
            }
            for (auto e : blockCtx->inheritedVariableDispositions()) {
              curCtx->setVariableDisposition(e.first, e.second);
            }
          }
          return res;
        }

        GroupNodeCPtr visit_assert_stmt(const GroupNodeCPtr &node)
        override
        {
          auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_CONDITION);
          auto iffalse = visitExpression(node, mylang::grammar::PAst::EXPR_MESSAGE);
          return ast.createAssertStatement(expr, iffalse);
        }

        GroupNodeCPtr visit_call_constructor_stmt(const GroupNodeCPtr &node)
        override
        {
          auto ctx = currentContext<ProcessConstructorContext>();
          if (!ctx) {
            notifyError(node, stream << "call to own constructor outside of a constructor");
            return nullptr;
          }
          if (ctx->call_to_own_constructor) {
            notifyError(node, stream << "call to own constructor must be the first statement");
            return nullptr;
          }

          auto args = visitExpressions(node, mylang::grammar::PAst::EXPR_EXPRESSION);

          // TODO: check for incorrect arguments and recursion!!

          // update the context  AFTER we've processed the arguments
          ctx->call_to_own_constructor = true;
          return ast.createCallConstructorStatement(args);
        }

        GroupNodeCPtr visit_expression_stmt(const GroupNodeCPtr &node)
        override
        {
          try {
            auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
            return ast.createExpressionStatement(expr);
          } catch (const ValidationError &e) {
            notifyValidationError(e);
            return nullptr;
          }
        }

        GroupNodeCPtr visit_foreach_stmt(const GroupNodeCPtr &node)
        override
        {
          auto curCtx = searchContext<StatementContext>();
          auto name = getToken(node, mylang::grammar::PAst::TOK_IDENTIFIER);
          auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          auto setupLoopCtx = ::std::make_shared<StatementContext>(curCtx);
          auto loopCtx = ::std::make_shared<LoopStatementContext>(setupLoopCtx);

          // an array expression
          auto arrTy = constraintOf<ConstrainedArrayType>(expr);
          if (!arrTy) {
            notifyError(expr,
                stream << "expected an array expression, but found " << constraintOf(expr));
            return nullptr;
          }
          if (!arrTy->isConcrete()) {
            notifyError(expr, stream << "expected a concrete array");
            return nullptr;
          }

          auto type = ast.createType(arrTy->element, node->span());
          auto stmt = withNewScope(loopCtx, [&](Converter &local) {
            auto var = local.define_variable_or_value(name, type, nullptr,
                Variable::createLocal(false));
            auto varname = getNameAnnotation(var);
            // mark the variable as initialized within the loop body
              setupLoopCtx->setValueDisposition(varname,
                  StatementContext::VariableDisposition::INITIALIZED);

              auto block = local.visitGroup(node, mylang::grammar::PAst::REL_STATEMENTS);

              return ast.createForeachStatement(var, expr, block);
            });

          for (auto e : loopCtx->inheritedValueDispositions()) {
            notifyError(node,
                stream << "value " << e.first->localName()
                    << " may not be initialized within a loop");
          }
          if (curCtx) {
            if (arrTy->bounds.min().is0()) {
              if (loopCtx->getReturnDisposition() > curCtx->getReturnDisposition()) {
                curCtx->setReturnDisposition(StatementContext::ReturnDisposition::MAY_RETURN);
              }
              if (loopCtx->getThrowDisposition() > curCtx->getThrowDisposition()) {
                curCtx->setThrowDisposition(StatementContext::ThrowDisposition::MAY_THROW);
              }

              for (auto e : loopCtx->inheritedVariableDispositions()) {
                curCtx->setVariableDisposition(e.first,
                    StatementContext::VariableDisposition::MAYBE_INITIALIZED);
              }
            } else {
              if (loopCtx->getReturnDisposition() > curCtx->getReturnDisposition()) {
                curCtx->setReturnDisposition(loopCtx->getReturnDisposition());
              }
              if (loopCtx->getThrowDisposition() > curCtx->getThrowDisposition()) {
                curCtx->setThrowDisposition(loopCtx->getThrowDisposition());
              }
              if (loopCtx->getContinueDisposition() > curCtx->getContinueDisposition()) {
                curCtx->setContinueDisposition(loopCtx->getContinueDisposition());
              }
              if (loopCtx->getBreakDisposition() > curCtx->getBreakDisposition()) {
                curCtx->setBreakDisposition(loopCtx->getBreakDisposition());
              }
              for (auto e : loopCtx->inheritedVariableDispositions()) {
                curCtx->setVariableDisposition(e.first, e.second);
              }
            }
          }
          return stmt;
        }

        GroupNodeCPtr visit_throw_stmt(const GroupNodeCPtr&)
        override
        {
          auto curCtx = searchContext<StatementContext>();
          if (curCtx) {
            curCtx->setThrowDisposition(StatementContext::ThrowDisposition::WILL_THROW);
          }
          return ast.createThrowStatement();
        }

        GroupNodeCPtr visit_continue_stmt(const GroupNodeCPtr &node)
        override
        {
          auto loopCtx = searchContext<LoopStatementContext>(true);
          if (!loopCtx) {
            notifyError(node, stream << "continue cannot be used outside of a loop");
          }
          auto curCtx = searchContext<StatementContext>();
          if (curCtx) {
            curCtx->setContinueDisposition(StatementContext::ContinueDisposition::WILL_CONTINUE);
          }
          return ast.createContinueStatement();
        }

        GroupNodeCPtr visit_break_stmt(const GroupNodeCPtr &node)
        override
        {
          auto loopCtx = searchContext<LoopStatementContext>(true);
          if (!loopCtx) {
            notifyError(node, stream << "break cannot be used outside of a loop");
          }

          auto curCtx = searchContext<StatementContext>();
          if (curCtx) {
            curCtx->setBreakDisposition(StatementContext::BreakDisposition::WILL_BREAK);
          }

          return ast.createBreakStatement();
        }

        GroupNodeCPtr visit_try_stmt(const GroupNodeCPtr &node)
        override
        {
          auto curCtx = searchContext<StatementContext>();
          auto tmpCtx = ::std::make_shared<StatementContext>(curCtx);
          auto tryCtx = ::std::make_shared<StatementContext>(curCtx);
          auto catchCtx = ::std::make_shared<StatementContext>(tmpCtx);

          auto tryBlock = withNewScope(tryCtx, [&](Converter &conv) {
            return conv.visitGroup(node, mylang::grammar::PAst::STMT_TRY);
          });
          for (auto disp1 : tryCtx->getDispositions().getValueDispositions()) {
            // FIXME: how to get a correct error location here?
            if (disp1.second >= StatementContext::VariableDisposition::MAYBE_INITIALIZED) {
              notifyError(tryBlock, stream << "Values may not be initialized in a try block");
            }
          }
          for (auto disp1 : tryCtx->getDispositions().getVariableDispositions()) {
            if (disp1.second == StatementContext::VariableDisposition::INITIALIZED) {
              tmpCtx->setVariableDisposition(disp1.first,
                  StatementContext::VariableDisposition::MAYBE_INITIALIZED);
            }
          }
          auto catchBlock = withNewScope(catchCtx, [&](Converter &conv) {
            return conv.visitGroup(node, mylang::grammar::PAst::STMT_CATCH);
          });

          // check the return disposition
          if (tryCtx->getReturnDisposition() > curCtx->getReturnDisposition()
              && catchCtx->getReturnDisposition() == tryCtx->getReturnDisposition()) {
            curCtx->setReturnDisposition(tryCtx->getReturnDisposition());
          } else if (tryCtx->getReturnDisposition() > curCtx->getReturnDisposition()
              || catchCtx->getReturnDisposition() > curCtx->getReturnDisposition()) {
            curCtx->setReturnDisposition(StatementContext::ReturnDisposition::MAY_RETURN);
          }

          if (tryCtx->getThrowDisposition() > curCtx->getThrowDisposition()
              && catchCtx->getThrowDisposition() == tryCtx->getThrowDisposition()) {
            curCtx->setThrowDisposition(tryCtx->getThrowDisposition());
          } else if (tryCtx->getThrowDisposition() > curCtx->getThrowDisposition()
              || catchCtx->getThrowDisposition() > curCtx->getThrowDisposition()) {
            curCtx->setThrowDisposition(StatementContext::ThrowDisposition::MAY_THROW);
          }

          if (tryCtx->getContinueDisposition() > curCtx->getContinueDisposition()
              && catchCtx->getContinueDisposition() == tryCtx->getContinueDisposition()) {
            curCtx->setContinueDisposition(tryCtx->getContinueDisposition());
          } else if (tryCtx->getContinueDisposition() > curCtx->getContinueDisposition()
              || catchCtx->getContinueDisposition() > curCtx->getContinueDisposition()) {
            curCtx->setContinueDisposition(StatementContext::ContinueDisposition::MAY_CONTINUE);
          }

          if (tryCtx->getBreakDisposition() > curCtx->getBreakDisposition()
              && catchCtx->getBreakDisposition() == tryCtx->getBreakDisposition()) {
            curCtx->setBreakDisposition(tryCtx->getBreakDisposition());
          } else if (tryCtx->getBreakDisposition() > curCtx->getBreakDisposition()
              || catchCtx->getBreakDisposition() > curCtx->getBreakDisposition()) {
            curCtx->setBreakDisposition(StatementContext::BreakDisposition::MAY_BREAK);
          }

          for (auto disp1 : catchCtx->getDispositions().getValueDispositions()) {
            if (disp1.second >= StatementContext::VariableDisposition::MAYBE_INITIALIZED) {
              // FIXME: how to get a correct error location here?
              notifyError(catchBlock, stream << "Values may not be initialized in a catch block");
            }
          }

          // we still mark variables as initialized, but an error will have been generated
          for (auto disp1 : tryCtx->getDispositions().getVariableDispositions()) {
            if (getVariableDisposition(curCtx, disp1.first)
                != StatementContext::VariableDisposition::INITIALIZED) {
              auto d1 = disp1.second;
              auto d2 = catchCtx->getDispositions().getVariableDisposition(disp1.first);
              if (d2.has_value() && d1 == *d2
                  && d1 == StatementContext::VariableDisposition::INITIALIZED) {
                curCtx->setVariableDisposition(disp1.first, disp1.second);
              }
            }
          }
          for (auto disp1 : tryCtx->getDispositions().getVariableDispositions()) {
            if (getVariableDisposition(curCtx, disp1.first)
                != StatementContext::VariableDisposition::INITIALIZED
                && disp1.second == StatementContext::VariableDisposition::MAYBE_INITIALIZED) {
              curCtx->setVariableDisposition(disp1.first, disp1.second);
            }
          }
          for (auto disp1 : catchCtx->getDispositions().getVariableDispositions()) {
            if (getVariableDisposition(curCtx, disp1.first)
                != StatementContext::VariableDisposition::INITIALIZED
                && disp1.second == StatementContext::VariableDisposition::MAYBE_INITIALIZED) {
              curCtx->setVariableDisposition(disp1.first, disp1.second);
            }
          }

          return ast.createTryStatement(tryBlock, catchBlock);
        }

        GroupNodeCPtr visit_if_stmt(const GroupNodeCPtr &node)
        override
        {
          auto curCtx = searchContext<StatementContext>();

          auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_CONDITION);
          auto iftrueCtx = ::std::make_shared<StatementContext>(curCtx);
          auto iffalseCtx = ::std::make_shared<StatementContext>(curCtx);

          auto iftrue = withNewScope(iftrueCtx, [&](Converter &conv) {
            return conv.visitGroup(node, mylang::grammar::PAst::STMT_IFTRUE);
          });
          auto iffalse = withNewScope(iffalseCtx, [&](Converter &conv) {
            return conv.visitGroup(node, mylang::grammar::PAst::STMT_IFFALSE);
          });

          // both branches of the if will return, so the if-statement will return
          if (curCtx) {
            // deal with the return/throw dispositions first
            if (iffalse) {
              if (iftrueCtx->getReturnDisposition() > curCtx->getReturnDisposition()
                  && iffalseCtx->getReturnDisposition() == iftrueCtx->getReturnDisposition()) {
                curCtx->setReturnDisposition(iftrueCtx->getReturnDisposition());
              } else if (iftrueCtx->getReturnDisposition() > curCtx->getReturnDisposition()
                  || iffalseCtx->getReturnDisposition() > curCtx->getReturnDisposition()) {
                curCtx->setReturnDisposition(StatementContext::ReturnDisposition::MAY_RETURN);
              }

              if (iftrueCtx->getThrowDisposition() > curCtx->getThrowDisposition()
                  && iffalseCtx->getThrowDisposition() == iftrueCtx->getThrowDisposition()) {
                curCtx->setThrowDisposition(iftrueCtx->getThrowDisposition());
              } else if (iftrueCtx->getThrowDisposition() > curCtx->getThrowDisposition()
                  || iffalseCtx->getThrowDisposition() > curCtx->getThrowDisposition()) {
                curCtx->setThrowDisposition(StatementContext::ThrowDisposition::MAY_THROW);
              }

              if (iftrueCtx->getContinueDisposition() > curCtx->getContinueDisposition()
                  && iffalseCtx->getContinueDisposition() == iftrueCtx->getContinueDisposition()) {
                curCtx->setContinueDisposition(iftrueCtx->getContinueDisposition());
              } else if (iftrueCtx->getContinueDisposition() > curCtx->getContinueDisposition()
                  || iffalseCtx->getContinueDisposition() > curCtx->getContinueDisposition()) {
                curCtx->setContinueDisposition(StatementContext::ContinueDisposition::MAY_CONTINUE);
              }

              if (iftrueCtx->getBreakDisposition() > curCtx->getBreakDisposition()
                  && iffalseCtx->getBreakDisposition() == iftrueCtx->getBreakDisposition()) {
                curCtx->setBreakDisposition(iftrueCtx->getBreakDisposition());
              } else if (iftrueCtx->getBreakDisposition() > curCtx->getBreakDisposition()
                  || iffalseCtx->getBreakDisposition() > curCtx->getBreakDisposition()) {
                curCtx->setBreakDisposition(StatementContext::BreakDisposition::MAY_BREAK);
              }
            } else {
              if (iftrueCtx->getReturnDisposition() > curCtx->getReturnDisposition()) {
                // since there is no else statement, we can only ever getReturnDisposition() a MAY_RETURN
                curCtx->setReturnDisposition(StatementContext::ReturnDisposition::MAY_RETURN);
              }
              if (iftrueCtx->getThrowDisposition() > curCtx->getThrowDisposition()) {
                // since there is no else statement, we can only ever getThrowDisposition() a MAY_RETURN
                curCtx->setThrowDisposition(StatementContext::ThrowDisposition::MAY_THROW);
              }
              if (iftrueCtx->getContinueDisposition() > curCtx->getContinueDisposition()) {
                // since there is no else statement, we can only ever getThrowDisposition() a MAY_RETURN
                curCtx->setContinueDisposition(StatementContext::ContinueDisposition::MAY_CONTINUE);
              }
              if (iftrueCtx->getBreakDisposition() > curCtx->getBreakDisposition()) {
                // since there is no else statement, we can only ever getThrowDisposition() a MAY_RETURN
                curCtx->setBreakDisposition(StatementContext::BreakDisposition::MAY_BREAK);
              }
            }

            bool iffalseExits = iffalseCtx->getReturnDisposition()
                == StatementContext::ReturnDisposition::WILL_RETURN
                || iffalseCtx->getThrowDisposition()
                    == StatementContext::ThrowDisposition::WILL_THROW
                || iffalseCtx->getContinueDisposition()
                    == StatementContext::ContinueDisposition::WILL_CONTINUE
                || iffalseCtx->getBreakDisposition()
                    == StatementContext::BreakDisposition::WILL_BREAK;
            bool iftrueExits = iftrueCtx->getReturnDisposition()
                == StatementContext::ReturnDisposition::WILL_RETURN
                || iftrueCtx->getThrowDisposition()
                    == StatementContext::ThrowDisposition::WILL_THROW
                || iftrueCtx->getContinueDisposition()
                    == StatementContext::ContinueDisposition::WILL_CONTINUE
                || iftrueCtx->getBreakDisposition()
                    == StatementContext::BreakDisposition::WILL_BREAK;

            // values must be initialized in all branches, unless a branch returns
            if (iffalseExits && iftrueExits) {
              // since both branches return, we don't need to worry about the context variables
            } else if (iffalseExits && iftrue) {
              for (auto i : iftrueCtx->inheritedValueDispositions()) {
                curCtx->setValueDisposition(i.first, i.second);
              }
              for (auto i : iftrueCtx->inheritedVariableDispositions()) {
                curCtx->setVariableDisposition(i.first, i.second);
              }
            } else if (iftrueExits && iffalse) {
              for (auto i : iffalseCtx->inheritedValueDispositions()) {
                curCtx->setValueDisposition(i.first, i.second);
              }
              for (auto i : iffalseCtx->inheritedVariableDispositions()) {
                curCtx->setVariableDisposition(i.first, i.second);
              }
            } else {
              for (auto i : iffalseCtx->inheritedValueDispositions()) {
                if (getValueDisposition(iftrueCtx, i.first)
                    != StatementContext::VariableDisposition::INITIALIZED) {
                  notifyError(iffalse,
                      stream << "value " << i.first->localName()
                          << " must be initialized in all branches of the if-statement");
                }
                curCtx->setValueDisposition(i.first, i.second);
              }
              for (auto i : iftrueCtx->inheritedValueDispositions()) {
                if (getValueDisposition(iffalseCtx, i.first)
                    != StatementContext::VariableDisposition::INITIALIZED) {
                  notifyError(iftrue,
                      stream << "value " << i.first->localName()
                          << " must be initialized in all branches of the if-statement");
                }
                curCtx->setValueDisposition(i.first, i.second);
              }
              for (auto i : iffalseCtx->inheritedVariableDispositions()) {
                if (i.second != getVariableDisposition(iftrueCtx, i.first)) {
                  curCtx->setVariableDisposition(i.first,
                      StatementContext::VariableDisposition::MAYBE_INITIALIZED);
                } else {
                  curCtx->setVariableDisposition(i.first, i.second);
                  assert(getVariableDisposition(curCtx, i.first) == i.second);
                }
              }
              for (auto i : iftrueCtx->inheritedVariableDispositions()) {
                if (i.second != getVariableDisposition(iffalseCtx, i.first)) {
                  curCtx->setVariableDisposition(i.first,
                      StatementContext::VariableDisposition::MAYBE_INITIALIZED);
                } else {
                  curCtx->setVariableDisposition(i.first, i.second);
                  assert(getVariableDisposition(curCtx, i.first) == i.second);
                }
              }
            }
          }

          return ast.createIfStatement(expr, iftrue, iffalse);
        }

        GroupNodeCPtr visit_log_stmt(const GroupNodeCPtr &node)
        override
        {
          auto level = getToken(node, mylang::grammar::PAst::TOK_LOG_LEVEL);
          auto exprs = visitExpressions(node, mylang::grammar::PAst::EXPR_ARGUMENTS);
          if (exprs.size() == 0) {
            notifyError(node, stream << "missing log message");
            return nullptr;
          }
          GroupNodes data;
          for (size_t i = 1; i < exprs.size(); ++i) {
            data.push_back(exprs[i]);
          }
          return ast.createLogStatement(level, exprs.at(0), data);
        }

        GroupNodeCPtr visit_return_stmt(const GroupNodeCPtr &node)
        override
        {
          auto curCtx = searchContext<StatementContext>();
          auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          if (curCtx) {
            curCtx->setReturnDisposition(StatementContext::ReturnDisposition::WILL_RETURN);
          }
          return ast.createReturnStatement(expr);
        }

        GroupNodeCPtr set_variable_stmt(::std::shared_ptr<const VariableDefinition> vardef,
            const TokenNodeCPtr &lhs, const GroupNodeCPtr &rhs)
        {
          auto varTy = constraintOf(vardef->decl);
          auto target = ast.createVariableExpression(lhs, vardef->name, varTy);
          auto value = visitExpression(rhs);

          auto ctx = searchContext<StatementContext>();

          if (!vardef->hasInitialValue() && !ctx) {
// this really should not happen, ever
            notifyError(lhs, stream << "assignment is not allowed in this context");
            return nullptr;
          }

          // if the variable is a value and has been initialized, then we have an error
          if (vardef->info->kind == Variable::KIND_PARAMETER) {
            notifyError(lhs, stream << "cannot update parameters");
            return nullptr;
          } else if (vardef->info->kind == Variable::KIND_INPUT) {
            notifyError(lhs, stream << "cannot update inputs");
            return nullptr;
          } else if (vardef->info->kind == Variable::KIND_OUTPUT) {
            return ast.createExpressionStatement(ast.createWritePortExpression(target, value));
          } else if (vardef->info->kind == Variable::KIND_VALUE) {
            if (vardef->hasInitialValue()) {
              notifyError(lhs, stream << "value has already been initialized");
              return nullptr;
            }
            if (getValueDisposition(ctx, vardef->name)
                != StatementContext::VariableDisposition::UNINITIALIZED) {
              notifyError(lhs, stream << "value may already have been set");
              return nullptr;
            }
            ctx->setValueDisposition(vardef->name,
                StatementContext::VariableDisposition::INITIALIZED);
          } else if (vardef->info->kind == Variable::KIND_VARIABLE) {
            if (containerContext() != vardef->containerContext) {
              notifyError(lhs, stream << "variable declared in an enclosing scope");
            } else {
              // we can always update a variable
              ctx->setVariableDisposition(vardef->name,
                  StatementContext::VariableDisposition::INITIALIZED);
            }
          } else {
            notifyInternalError(lhs, stream << "unsupported variable kind");
            return nullptr;
          }

          //TODO: check that the reference points reference a variable defined in the current scope
          //TODO: updates within functions of variables outside the scope of the function is not allowed
          return ast.createUpdateStatement(target, value);
        }

        GroupNodeCPtr visit_set_stmt(const GroupNodeCPtr &node)
        override
        {
          auto dest = visitExpression(node, mylang::grammar::PAst::EXPR_LHS);
          auto refTy = constraintOf(dest);
          if (refTy && refTy->self<ConstrainedOutputType>()) {
            auto value = visitExpression(node, mylang::grammar::PAst::EXPR_RHS);
            return ast.createExpressionStatement(ast.createWritePortExpression(dest, value));
          }

          auto value = visitExpression(node, mylang::grammar::PAst::EXPR_RHS);
          return ast.createSetStatement(dest, value);
        }

        GroupNodeCPtr visit_update_stmt(const GroupNodeCPtr &node)
        override
        {
          auto lhs = idioma::astutils::getGroup(node, mylang::grammar::PAst::EXPR_LHS);
          auto rhs = idioma::astutils::getGroup(node, mylang::grammar::PAst::EXPR_RHS);

          // left hand side must be a variable reference
          if (lhs->hasType(mylang::grammar::PAst::GRP_VARIABLE_REFERENCE_EXPRESSION) == false) {
            notifyError(lhs, stream << "not an lvalue");
            return nullptr;
          }

          // get the name of the variable
          auto varname = getToken(lhs, mylang::grammar::PAst::TOK_IDENTIFIER);
          auto def = searchDefinition(varname->text);
          if (!def) {
            notifyError(lhs, stream << "no such variable: " << varname->text);
            return nullptr;
          }
          auto vardef = ::std::dynamic_pointer_cast<const VariableDefinition>(def);
          if (!vardef) {
            notifyError(lhs, stream << "not a variable: " << varname->text);
          }

          return set_variable_stmt(vardef, varname, rhs);
        }

        GroupNodeCPtr visit_wait_stmt(const GroupNodeCPtr &node)
        override
        {
          // in order to use the wait statement, we need to be in the context of a process
          auto exprs = visitExpressions(node, mylang::grammar::PAst::EXPR_EXPRESSION);
          auto proc = searchContext<const DefProcessContext>();
          if (!proc) {
            notifyError(node,
                stream << "wait statement must be executed with the context of a process");
            return nullptr;
          }
          return ast.createWaitStatement(exprs);
        }

        GroupNodeCPtr visit_while_stmt(const GroupNodeCPtr &node)
        override
        {
          auto curCtx = searchContext<StatementContext>();
          auto expr = visitExpression(node, mylang::grammar::PAst::EXPR_CONDITION);
          auto loopCtx = ::std::make_shared<LoopStatementContext>(curCtx);
          auto iftrue = withNewScope(loopCtx, [&](Converter &conv) {
            return conv.visitGroup(node, mylang::grammar::PAst::STMT_IFTRUE);
          });
          for (auto e : loopCtx->inheritedValueDispositions()) {
            notifyError(node,
                stream << "value " << e.first->localName()
                    << " may not be initialized within a loop");
          }
          if (curCtx) {
            if (loopCtx->getReturnDisposition() > curCtx->getReturnDisposition()) {
              curCtx->setReturnDisposition(StatementContext::ReturnDisposition::MAY_RETURN);
            }
            if (loopCtx->getThrowDisposition() > curCtx->getThrowDisposition()) {
              curCtx->setThrowDisposition(StatementContext::ThrowDisposition::MAY_THROW);
            }
            for (auto e : loopCtx->inheritedVariableDispositions()) {
              curCtx->setVariableDisposition(e.first,
                  StatementContext::VariableDisposition::MAYBE_INITIALIZED);
            }
          }

          return ast.createWhileStatement(expr, iftrue);
        }
      }
      ;

    }

    struct ResolveTransforms: public mylang::vast::VAstTransform
    {
      ResolveTransforms(Converter &xconverter)
          : converter(xconverter)
      {
      }
      ~ResolveTransforms()
      {
      }

      GroupNodeCPtr visit_transform_expression(GroupNodeCPtr node) override
      {
        ConstrainedTypePtr retTy = getConstraintAnnotation(node);
        ::std::vector<ConstrainedTypePtr> argTypes;
        auto args = ast.getExprs(node, VAst::Expr::ARGUMENTS);
        for (const auto &arg : args) {
          argTypes.push_back(getConstraintAnnotation(arg));
        }
        auto fnTy = ConstrainedFunctionType::get(retTy, argTypes);
        auto fn =
            converter.transformResolver->createFunction(fnTy, [&](const ::std::string &name) {

              auto fqn = mylang::names::Name::parseFQN(name, '.');
              auto def = converter.findDefinition(node,fqn);
              if (auto fndef = def->self<DefFunctionContext>();fndef) {
                return fndef->getEntry().id;
              }

              return def->name;
            },
                [&](
                    NamePtr name) {
                      TransformResolver::NamedTypePtr ty;
                      auto def = converter.findDefinition(node,name);

                      if (def==nullptr) {
                        converter.notifyError(node, converter.stream << "failed to locate type " << name->fullName());
                      } else if(auto ctx = def->self<DefTypeContext>();ctx) {
                        ty = ctx->getType();
                      } else {
                        converter.notifyError(node, converter.stream << "not a type name " << name->fullName());
                      }
                      return ty;
                    });
        if (fn == nullptr) {
          return nullptr;
        }
        return converter.ast.createFunctionCallExpression(fn, args);
      }

      GroupNodeCPtr rewrite(GroupNodeCPtr node)
      {
        auto res = visit(node);
        return res ? res : node;
      }

      Converter &converter;
    };

    struct LateInstantiations: public mylang::vast::VAstTransform
    {
      LateInstantiations(Converter &xconverter)
          : converter(xconverter), recursionCount(0)
      {
      }

      ~LateInstantiations()
      {
      }

      GroupNodeCPtr visit_instantiate_generic_function(GroupNodeCPtr node) override
      {
        auto defName = VAst::getToken(node, VAst::Token::GENERIC_DEFINITION_NAME);
        auto defKey = getNameAnnotation(defName);
        auto instanceName = VAst::getToken(node, VAst::Token::IDENTIFIER);
        auto instanceKey = getNameAnnotation(node);

        auto fndef = VAst::getRelation(node, VAst::Relation::GENERIC_INSTANCE_FUNCTION);
        if (fndef) {
          return mylang::vast::VAstTransform::visit_instantiate_generic_function(node);
        }

        auto proc = processed.find(instanceKey);
        if (proc != processed.end()) {
          return ast.createVariableExpression(instanceName, instanceKey, proc->second);
        }

        auto annotation = node->annotation(GenericInstantiationRequest::Annotation());
        if (!annotation) {
          converter.notifyInternalError(defName, converter.stream << "No such request");
          return nullptr;
        }
        auto req = annotation.cast<::std::shared_ptr<const GenericInstantiationRequest>>()->value();
        if (req->signature) {
          // this enables recursion
          processed.emplace(instanceKey, req->signature);
        }

        GroupNodeCPtr res = converter.instantiateFunction(*req);
        if (!res) {
          throw ValidationError(node,
              "cannot instantiate generic due to missing definition: " + instanceName->text);
        }
        processed.emplace(instanceKey, constraintOf(res));
        try {
          ++recursionCount;
          if (recursionCount > MAX_GENERIC_RECURSION) {
            throw ValidationError(node,
                "maxium recursion count of " + ::std::to_string(MAX_GENERIC_RECURSION)
                    + " reached during generic instantiation");
          }
          res = rewrite(res);
          --recursionCount;
          return res;
        } catch (...) {
          --recursionCount;
          throw;
        }
      }

      GroupNodeCPtr rewrite(GroupNodeCPtr node)
      {
        auto res = visit(node);
        return res ? res : node;
      }

      Converter &converter;
      ::std::map<NamePtr, ConstrainedTypePtr> processed;
      size_t recursionCount;
    };

    GroupNodeCPtr validateAST(const GroupNodeCPtr &ast, const FeedbackHandler &fb,
        mylang::filesystem::FileSystem::Ptr externals,
        mylang::transforms::TransformBase::Ptr transforms)
    {
      ::std::set<mylang::filesystem::FileSystem::Ptr> filesystems;
      TransformResolver::Ptr resolver;
      filesystems.insert(externals);
      if (transforms) {
        resolver = TransformResolver::create(transforms);
        filesystems.insert(transforms->getFileSystem());
      }
      auto fs = mylang::filesystem::FileSystem::unionOf(filesystems);

      size_t nErrors = 0;
      size_t nWarnings = 0;
      FeedbackHandler feedback;
      feedback = [&](FeedbackType t, const SourceLocation &loc, const ::std::string &msg) {
        if (t == FeedbackType::ERROR || t == FeedbackType::FIXME) {
          ++nErrors;
        }
        if (t == FeedbackType::WARNING) {
          ++nWarnings;
        }
        fb(t, loc, msg);
      };

      GroupNodeCPtr res;
      idioma::scope::Tree scopes;
      auto builtins = Builtins::get();
      auto library = Library::get();
      auto allDefs = ::std::make_shared<::std::map<NamePtr, DefinitionPtr>>();
      auto allDefsByFQN = ::std::make_shared<::std::map<mylang::names::FQN, DefinitionPtr>>();
      auto allGenerics = ::std::make_shared<
          ::std::map<NamePtr, ::std::shared_ptr<GenericInstantiationProvider>>>();
      auto scope = scopes.rootScope();
      auto tx_calls = ::std::make_shared<GroupNodes>();
      auto importedFiles = ::std::make_shared<std::set<mylang::names::FQN>>();

      Converter conv(feedback, fs, scope, builtins, library, allDefs, allDefsByFQN, allGenerics,
          importedFiles, resolver);
      try {
        res = conv.visit(ast);
        while (nErrors == 0) {
          GroupNodeCPtr res2 = res;
          if (resolver) {
            ResolveTransforms resolv(conv);
            res2 = resolv.rewrite(res);
          }
          LateInstantiations instantiator(conv);
          res2 = instantiator.rewrite(res2);
          if (res2 == res) {
            res = res2;
            break;
          } else {
            res = res2;
          }

        }
      } catch (const ValidationError &e) {
        conv.notifyValidationError(e);
      } catch (const ::std::exception &e) {
        // most likely, this error is due to a previous error; but if there
        // wasn't one, then we've got a problem
        if (nErrors == 0) {
          feedback(FeedbackType::ERROR, SOURCELOCATION,
              ::std::string("Unexpected exception: ") + e.what());
        }
      }
      // perform late instantiations
      if (nErrors != 0) {
        res = nullptr;
      }
      return res;
    }

  }
}
