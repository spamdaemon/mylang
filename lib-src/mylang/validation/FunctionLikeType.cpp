#include <mylang/validation/FunctionLikeType.h>
#include <mylang/types/TypeSet.h>

namespace mylang {
  namespace validation {
    using namespace mylang::types;

    FunctionLikeType::FunctionLikeType()
    {
    }

    FunctionLikeType::~FunctionLikeType()
    {
    }

    bool FunctionLikeType::isConcrete() const
    {
      return false;
    }

    void FunctionLikeType::accept(mylang::types::TypeVisitor&) const
    {
    }

    bool FunctionLikeType::isSameType(const Type &other) const
    {
      return this == &other;
    }

    ::std::string FunctionLikeType::toString() const
    {
      return "function-like";
    }

    Type::Ptr FunctionLikeType::normalize(mylang::types::TypeSet &ts) const
    {
      return ts.add(self());
    }

    ::std::shared_ptr<const FunctionLikeType> FunctionLikeType::get(ConcretizeFN f)
    {
      struct Impl: public FunctionLikeType
      {
        Impl(ConcretizeFN fn)
            : func(fn)
        {
        }

        ~Impl()
        {
        }

        std::shared_ptr<const FunctionType> concretize(const ::std::vector<TypePtr> &argv) const
        {
          return func(argv);
        }

      private:
        ConcretizeFN func;
      };
      if (!f) {
        throw ::std::invalid_argument("No function specified");
      }
      return ::std::make_shared<Impl>(f);
    }

  }
}
