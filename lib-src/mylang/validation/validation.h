#ifndef FILE_MYLANG_VALIDATION_VALIDATION_H
#define FILE_MYLANG_VALIDATION_VALIDATION_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef FILE_MYLANG_FEEDBACK_H
#include <mylang/feedback.h>
#endif

#ifndef CLASS_MYLANG_FILESYSTEM_FILESYSTEM_H
#include <mylang/filesystem/FileSystem.h>
#endif

#ifndef CLASS_MYLANG_TRANSFORMS_TRANSFORMBASE_H
#include <mylang/transforms/TransformBase.h>
#endif

namespace mylang {
  namespace validation {
    /**
     * Apply the validation rules to a parsed AST (PAst) and generate a validated AST (VAst).
     * @param past the ast to evaluate
     * @param fb a feedback handler for reporting errors
     * @param search an optional search context
     * @return a new validated ast
     */
    GroupNodeCPtr validateAST(const GroupNodeCPtr &past, const FeedbackHandler &fb,
        mylang::filesystem::FileSystem::Ptr filesystem,
        mylang::transforms::TransformBase::Ptr transforms);
  }
}
#endif
