#ifndef CLASS_MYLANG_VALIDATION_BUILTINS_H
#define CLASS_MYLANG_VALIDATION_BUILTINS_H
#include <mylang/defs.h>
#include <memory>
#include <string>

namespace mylang {
  namespace validation {
    class Definition;
  }
}

#ifndef CLASS_MYLANG_VALIDATION_DEFINITION_H
#include <mylang/validation/Definition.h>
#endif

namespace mylang {
  namespace validation {

    /**
     * The builtins functions and attributes that are attached to types or
     * type instances and which cannot be implemented using the language
     * itself.
     *
     * As the language improves, more of this should move into the library.
     */
    class Builtins
    {
    public:

      /** Constructor */
    private:
      Builtins();

    public:
      virtual ~Builtins() = 0;

      /**
       * Get the builtins.
       * @return an instanceof the builtins
       */
    public:
      static ::std::shared_ptr<Builtins> get();

      /**
       * Find a "static" member of a type.
       * @param type a pointer to a type
       * @param name the name of type member
       * @return a definition or nullptr if not found
       */
    public:
      virtual ::std::shared_ptr<const Definition> findTypeBuiltin(const ConstrainedTypePtr &type,
          const ::std::string &name, const ::idioma::ast::Node::Span &span) const = 0;

      /**
       * Find a builtin for an instanceof of the given type or the type itself.
       * @param expr an expression
       * @param name a a name
       * @return a definition or nullptr if not found.
       */
    public:
      virtual ::std::shared_ptr<const Definition> findInstanceBuiltin(const GroupNodeCPtr &expr,
          const ::std::string &name, const ::idioma::ast::Node::Span &span) const = 0;

      /**
       * Find a builtin for an instanceof of the given type or the type itself.
       * @param expr an expression
       * @param name a a name
       * @return a definition or nullptr if not found.
       */
    public:
      virtual bool hasInstanceBuiltin(const ConstrainedTypePtr &ty,
          const ::std::string &name) const = 0;
    };
  }
}
#endif
