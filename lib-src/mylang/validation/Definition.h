#ifndef CLASS_MYLANG_VALIDATION_DEFINITION_H
#define CLASS_MYLANG_VALIDATION_DEFINITION_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_SOURCELOCATION_H
#include <mylang/SourceLocation.h>
#endif

#ifndef CLASS_MYLANG_TYPES_FUNCTIONTYPE_H
#include <mylang/types/FunctionType.h>
#endif

#ifndef _IDIOMA_SCOPE_H
#include <idioma/scope.h>
#endif
#include <functional>
#include <vector>

namespace mylang {
  namespace validation {

    /** An abstract class models a definition of e.g. variables, functions, etc */
    class Definition: public ::std::enable_shared_from_this<Definition>
    {
      /** A pointer to a scope */
    public:
      typedef ::idioma::scope::Scope::Ptr ScopePtr;

      /** A visit function */
    public:
      typedef ::std::function<GroupNodeCPtr(ScopePtr scope, GroupNodeCPtr node)> VisitFN;

      /** A function to register a variable */
    public:
      typedef ::std::function<void(GroupNodeCPtr decl)> RegisterVariableFN;

      /**
       * Create a definition with a auto-generated unique name.
       */
    protected:
      Definition();

      /**
       * Create a definition with a specified name.
       * @param the_name a name
       */
    protected:
      Definition(const NamePtr &the_name);

      /** The destructor */
    public:
      virtual ~Definition();

      /**
       * Get this pattern as a specified type.
       * @return this pattern or nullptr if it's not if the requested type.
       */
    public:
      template<class T = Definition>
      inline ::std::shared_ptr<const T> self() const
      {
        return ::std::dynamic_pointer_cast<const T>(shared_from_this());
      }

      /**
       * Get this pattern as a specified type.
       * @return this pattern or nullptr if it's not if the requested type.
       */
    public:
      template<class T = Definition>
      inline ::std::shared_ptr<T> self()
      {
        return ::std::dynamic_pointer_cast<T>(shared_from_this());
      }

      /**
       * Find the definition.
       * @param name the name of the definition to search for
       * @return a definition or nullptr if not found
       */
    public:
      virtual ::std::shared_ptr<const Definition> find(const ::std::string &name) const;

      /**
       * Get the type for this definition.
       * @return the type
       */
    public:
      virtual ConstrainedTypePtr get_type() const;

      /**
       * Apply a parameters to this definition and to generate a new expression or node.
       * @note the arguments have already been converted as necessary using the fn function.
       * @param scope the scope to use
       * @param src a my_grammar  source node that provides information about the error
       * @param params the parameters to the call
       * @param fn a function that can be used to convert my_grammar nodes into Ast nodes.
       * @throws ValidationError if the call could not be created
       */
    public:
      virtual GroupNodeCPtr create_call(const ScopePtr &scope, const SourceLocation &src,
          const GroupNodes &params, RegisterVariableFN registerVar, VisitFN fn) const;

      /**
       * Get the function type that corresponds to this definition.
       * @param argv argument types
       * @return the function type that describes this definition (or nullptr if not applicable)
       */
    public:
      virtual ::std::shared_ptr<const mylang::types::FunctionType> get_function_type(
          const ::std::vector<TypePtr> &argv) const;

      /**
       * Create a reference from this definition
       * @param scope the scope to use
       * @param src a my_grammar  source node that provides information when declaring errors
       * @param params the parameters to the call
       * @param fn a function that can be used to convert my_grammar nodes into Ast nodes.
       * @throws ValidationError if the call could not be created
       */
      virtual GroupNodeCPtr create_reference(const ScopePtr&, const TokenNodeCPtr &src) const;

      /** The unique name for this definition */
    public:
      const NamePtr name;
    };
  }
}
#endif
