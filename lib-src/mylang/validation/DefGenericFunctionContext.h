#ifndef CLASS_MYLANG_VALIDATION_DEFGENERICFUNCTIONCONTEXT_H
#define CLASS_MYLANG_VALIDATION_DEFGENERICFUNCTIONCONTEXT_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_VALIDATION_COUNTER_H
#include <mylang/validation/Counter.h>
#endif

#ifndef CLASS_MYLANG_VALIDATION_CONTAINERCONTEXT_H
#include <mylang/validation/ContainerContext.h>
#endif

#ifndef CLASS_MYLANG_VALIDATION_GENERICINSTANTIATIONREQUEST_H
#include <mylang/validation/GenericInstantiationRequest.h>
#endif

#ifndef FILE_MYLANG_CONSTRAINTS_H
#include <mylang/constraints.h>
#endif

#ifndef FILE_MYLANG_PATTERNS_H
#include <mylang/patterns.h>
#endif

namespace mylang {
  namespace validation {

    struct DefGenericFunctionContext: public ContainerContext
    {
      struct Entry
      {
        Entry(TokenNodeCPtr xdefName,
            ::std::vector<::std::pair<TokenNodeCPtr, PatternPtr>> genericParams,
            PatternPtr xreturnType, ::std::vector<PatternPtr> xparameters, bool xhaveBody);

        ::std::shared_ptr<const mylang::patterns::FunctionPattern> signature() const;

        bool hasExtension(const ConstrainedTypePtr &ty) const;

        ::std::unique_ptr<Entry> specialize(::std::vector<ConstrainedTypePtr> specialization) const;

        /**
         * Determine the kind of match of the arguments with this entry.
         * @return a match context or nullptr if a match was not possible
         */
        int determineMatch(const ::std::vector<ConstrainedTypePtr> &args,
            mylang::patterns::MatchContext::Ptr &ctx) const;

        /**
         * Determine the kind of match of the arguments with this entry.
         * @return a match context or nullptr if a match was not possible
         */
        //  bool isMoreSpecificThan(const Entry &e) const;
        /**
         * Create a matchcontext for this entry
         */
        mylang::patterns::MatchContext::Ptr createMatchContext() const;

      public:
        const TokenNodeCPtr defName;
        const NamePtr id;
        /** The parameters for the generic itself */
        const ::std::vector<::std::pair<TokenNodeCPtr, PatternPtr>> genericParameters;
        /** The function return type pattern */
        const PatternPtr returnType;
        /** The function parameters as patterns */
        const ::std::vector<PatternPtr> parameters;
        const PatternPtr canonical;
        bool haveBody;
        mutable ::std::map<NamePtr, ::std::shared_ptr<const GenericInstantiationRequest>> requests;
        ::std::function<GroupNodeCPtr(const GenericInstantiationRequest&)> provider;
      private:
        const ::std::shared_ptr<const mylang::patterns::FunctionPattern> functionSignature;

        /** The specialization */
      private:
        ::std::vector<ConstrainedTypePtr> specializations;
      };

      DefGenericFunctionContext(const NamePtr &the_name, VariablePtr xvinfo, bool xisExtension,
          ::std::shared_ptr<Counter> nameCounter);

      ~DefGenericFunctionContext();

      ::std::shared_ptr<DefGenericFunctionContext> specialize(
          const ::std::vector<ConstrainedTypePtr> &types) const;

      ::std::shared_ptr<DefGenericFunctionContext> generalize() const;

      inline bool isEmpty() const
      {
        return generics.empty();
      }

      GroupNodeCPtr create_reference(const ScopePtr&, const TokenNodeCPtr &src) const override;

      GroupNodeCPtr create_call(const ScopePtr&, const SourceLocation &src, const GroupNodes &args,
          RegisterVariableFN, VisitFN) const override;

      NamePtr makeNewName();

      NamePtr addEntry(const TokenNodeCPtr &sourceName,
          const ::std::vector<::std::pair<TokenNodeCPtr, PatternPtr>> &genericParams,
          const PatternPtr &retTy, const ::std::vector<PatternPtr> &params, bool haveBody);

      ::std::shared_ptr<const mylang::constraints::ConstrainedFunctionType> getSignature(
          const NamePtr &key) const;

      Entry& getEntry(const NamePtr &key);

      const Entry& getEntry(const NamePtr &key) const;

      bool hasExtension(const ConstrainedTypePtr &ty) const;

    private:
      static bool isMoreGeneralThan(const ::std::vector<::std::pair<TokenNodeCPtr, PatternPtr>> &p1,
          const ::std::vector<::std::pair<TokenNodeCPtr, PatternPtr>> &p2);

      static bool isMoreSpecificThan(
          const ::std::vector<::std::pair<TokenNodeCPtr, PatternPtr>> &p1,
          const ::std::vector<::std::pair<TokenNodeCPtr, PatternPtr>> &p2);

      static bool isEquivalent(const ::std::vector<::std::pair<TokenNodeCPtr, PatternPtr>> &p1,
          const ::std::vector<::std::pair<TokenNodeCPtr, PatternPtr>> &p2);

      GroupNodeCPtr instantiateGeneric(const SourceLocation &sloc,
          const ::std::vector<ConstrainedTypePtr> &args) const;

    public:
      const bool isExtension;
      const VariablePtr vinfo;
    private:
      mutable ::std::vector<Entry> generics;

      ::std::shared_ptr<Counter> nameCounter;
    };

  }
}
#endif
