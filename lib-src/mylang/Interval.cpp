#include <mylang/Interval.h>
#include <initializer_list>
#include <iostream>
#include <cmath>
#include <limits>
#include <cassert>

namespace mylang {

  namespace {
    ::std::optional<Interval> neg(const ::std::optional<Interval> &a)
    {
      if (a) {
        return a->negate();
      } else {
        return a;
      }
    }
  }

  const Interval& Interval::INFINITE()
  {
    static const Interval value;
    return value;
  }
  const Interval& Interval::POSITIVE()
  {
    static const Interval value(1, Integer::PLUS_INFINITY());
    return value;
  }

  const Interval& Interval::NEGATIVE()
  {
    static const Interval value(Integer::MINUS_INFINITY(), -1);
    ;
    return value;
  }

  const Interval& Interval::NON_POSITIVE()
  {
    static const Interval value(Integer::MINUS_INFINITY(), 0);
    ;
    return value;
  }

  const Interval& Interval::NON_NEGATIVE()
  {
    static const Interval value(0, Integer::PLUS_INFINITY());
    ;
    return value;
  }

  const Interval& Interval::ZERO()
  {
    static const Interval value(Integer::ZERO());
    ;
    return value;
  }
  const Interval& Interval::PLUS_ONE()
  {
    static const Interval value(Integer::PLUS_ONE());
    ;
    return value;
  }
  const Interval& Interval::MINUS_ONE()
  {
    static const Interval value(Integer::MINUS_ONE());
    ;
    return value;
  }

  Interval::Interval()
      : _min(Integer::MINUS_INFINITY()), _max(Integer::PLUS_INFINITY())
  {
  }

  Interval::Interval(const Integer &l, const Integer &r)
      : _min(l), _max(r)
  {
    if (l > r || (l.isInfinite() && r.isInfinite() && l.sign() == r.sign())) {
      throw ::std::invalid_argument(
          "Interval " + l.toString() + ";" + r.toString() + " is not valid");
    }
  }

  Interval::Interval(const Integer &value)
      : _min(value), _max(value)
  {
    if (_min.isInfinite()) {
      throw ::std::invalid_argument(
          "Interval " + _min.toString() + ";" + _max.toString() + " is not valid");
    }
  }

  Interval Interval::pairwiseMin(const Interval &that) const
  {
    auto l = _min.min(that._min);
    auto r = _max.min(that._max);
    return Interval(l, r);
  }

  Interval Interval::pairwiseMax(const Interval &that) const
  {
    auto l = _min.max(that._min);
    auto r = _max.max(that._max);
    return Interval(l, r);
  }

  Interval Interval::unionWith(const Interval &that) const
  {
    auto l = _min.min(that._min);
    auto r = _max.max(that._max);
    return Interval(l, r);
  }

  ::std::optional<Interval> Interval::unionOf(const ::std::optional<Interval> &a,
      const ::std::optional<Interval> &b)
  {
    if (a) {
      if (b) {
        return a->unionWith(*b);
      } else {
        return a;
      }
    } else {
      return b;
    }
  }

  ::std::optional<Interval> Interval::intersectWith(const Interval &that) const
  {
    auto l = _min.max(that._min);
    auto r = _max.min(that._max);
    auto cmp = l.compare(r);
    if (cmp < 0) {
      return Interval(l, r);
    }
    if (cmp > 0) {
      return ::std::nullopt;
    }
    if (l.isInfinite()) {
      // we cannot have an interval (+inf,+inf)
      // or (-inf,-inf)
      return ::std::nullopt;
    }
    return Interval(l);
  }

  ::std::optional<Interval> Interval::intersectionOf(const ::std::optional<Interval> &a,
      const ::std::optional<Interval> &b)
  {
    if (a.has_value()) {
      if (b.has_value()) {
        return a->intersectWith(*b);
      }
    }
    return ::std::nullopt;
  }

  ::std::optional<Interval> Interval::joinWith(const Interval &that) const
  {
    if (!intersectWith(that)) {
      // check if they are adjacent
      if (_max.increment() != that._min && _min.decrement() != that._max) {
        return ::std::nullopt;
      }
    }
    return unionWith(that);
  }

  ::std::optional<Interval> Interval::makeInterval(const Integer &x,
      const ::std::initializer_list<Integer> &list)
  {
    Integer l(x);
    Integer r(x);
    for (const Integer &i : list) {
      l = i.min(l);
      r = i.max(r);
    }
    if (l.isInfinite() && l == r) {
      return ::std::nullopt;
    }
    return Interval(l, r);
  }

  Integer Interval::size() const
  {
    if (isInfinite()) {
      return Integer::PLUS_INFINITY();
    }
    return _max.subtract(_min)->add(1).value();
  }

  bool Interval::contains(const Interval &that) const
  {
    return _min <= that._min && _max >= that._max;
  }

  bool Interval::equals(const Interval &ival) const
  {
    return _min == ival._min && _max == ival._max;
  }

  Interval Interval::insert0() const
  {
    if (contains0()) {
      return *this;
    }
    // {-4,-2} -> l={ -4, 0}, r={-2,0}, --> { -4,0 }
    Interval l(_min.min(Integer::ZERO()), _min.max(Integer::ZERO()));
    Interval r(_max.min(Integer::ZERO()), _max.max(Integer::ZERO()));
    return Interval(l._min, r._max);
  }

  ::std::optional<Interval> Interval::multiplesOf(const Integer &n) const
  {
    if (n == ZERO()) {
      if (contains0()) {
        return n;
      } else {
        return ::std::nullopt;
      }
    }
    if (isFixed()) {
      auto rem = _min.remainder(n);
      if (rem.has_value() && rem->is0()) {
        return *this;
      } else {
        return ::std::nullopt;
      }
    }

    // NOTE: n!=0, so divide(n) is always defined
    if (_min.isInfinite() && _max.isInfinite()) {
      return *this;
    } else if (_min.isInfinite()) {
      auto r = _max.divide(n)->multiply(n);
      return Interval(_min, r);
    } else if (_max.isInfinite()) {
      auto l = _min.divide(n)->multiply(n);
      return Interval(l, _max);
    } else {
      auto l = _min.divide(n)->multiply(n);
      auto r = _max.divide(n)->multiply(n);
      return Interval(l, r);
    }
  }

  Interval Interval::negate() const
  {
    auto l = _max.negate();
    auto r = _min.negate();
    return Interval(l, r);
  }

  Interval Interval::abs() const
  {
    auto l = _max.abs();
    auto r = _min.abs();
    if (contains0()) {
      return Interval(Integer::ZERO(), l.max(r));
    } else if (l < r) {
      return Interval(l, r);
    } else {
      return Interval(r, l);
    }
  }

  Interval Interval::add(const Interval &op2) const
  {
    auto l = _min + op2._min;
    auto r = _max + op2._max;
    // _min (_max) can never be PLUS_INFINITY (MINUS_INFINITY)
    assert(l && r);
    return Interval(*l, *r);
  }

  Interval Interval::multiply(const Interval &op2) const
  {
    // we need to compute all possible products  of the endpoints
    // of the intervals and pick the min and max of those products
    return *makeInterval(_min * op2._min, { //
        _min * op2._max, //
        _max * op2._min, //
        _max * op2._max });
  }

  ::std::optional<Interval> Interval::divide(const Interval &op2) const
  {
    // division by 0
    if (op2.is0()) {
      return ::std::nullopt;
    }

    // most common case ought to be division fixed numbers
    if (isFixed() && op2.isFixed()) {
      return Integer(_max.divide(op2._max).value());
    }

    Interval op(op2);
    // division by a positive number that is bounded by 0
    // is just the numerator, since the smallest denominator
    // is 1.
    if (op2._min.is0()) {
      op = { 1, op._max };
    } else if (op2._max.is0()) {
      op = { op2._min, -1 };
    }

    // division by a number that is strictly positive or negative
    // because it does not contain 0
    if (!op.contains0()) {
      return *makeInterval((_min / op._min).value_or(0), { //
          (_min / op._max).value_or(0), //
          (_max / op._min).value_or(0), //
          (_max / op._max).value_or(0) });
    }

    // if it contains 0, then the result is the maximum
    // of the numerator's bounds and the negative thereof.
    // e.g. [-5,2] / (interval containing 0) ->
    // min(-5/-1,2/-1,-5,1,2/1) = -5
    // max(-5/-1,2/-1,-5,1,2/1) = 5
    auto r = _min.abs().max(_max.abs());
    auto l = r.negate();

    return Interval(l, r);
  }

  ::std::optional<Interval> Interval::remainder(const Interval &op) const
  {
    if (op.is0()) {
      // division by 0 is undefined
      return ::std::nullopt;
    }
    // ensure the denominator is positive
    // because its sign doesn't matter
    {
      if (op._max.sign() < 0) {
        return remainder(op.negate());
      }
      if (op._min.sign() < 0) {
        return remainder(Interval(1, op._max.max(-op._min)));
      }
    }

    // ensure we only work with positive numerators
    {
      if (_max.sign() < 0) {
        // negative numerator
        return neg(negate().remainder(op));
      }
      if (_min.sign() < 0) {
        // numerator is crossing 0, so we just split the interval
        // and recombine them afterwards
        Interval l(_min, -1);
        Interval r(Integer::ZERO(), _max);
        return unionOf(l.remainder(op), r.remainder(op));
      }
    }

    if (op.isFixed()) {
      if (isFixed()) {
        // fixed implies !isInfinite()
        auto r = _min.remainder(op._max);
        if (r) {
          return Interval(r.value());
        } else {
          return ::std::nullopt;
        }
      }
      if (size() < op._max) {
        auto l = _min.remainder(op._max).value();
        auto r = _max.remainder(op._max).value();
        if (l < r) {
          return Interval(l, r);
        }
      }

      return Interval(0, op._max.decrement());
    }

    // checking interval based on size
    {
      auto sz = size();
      if (sz >= op._max) {
        // we have more than enough values in the interval
        // to guarantee that we have a remainder of 0 for some
        // value.
        return Interval(0, op._max.decrement());
      }
      if (sz >= op._min) {
        Interval l(0, sz.decrement());
        auto r = remainder(Interval(sz.increment(), op._max));
        return unionOf(l, r);
      }
    }

    if (op._min > _max) {
      return *this;
    }
    if (op._max > _max) {
      return Interval(0, _max);
    }
    // an approximation; we would have to try all
    // possible values in the interval to find the largest and smallest values
    return Interval(0, _max.decrement());
  }

  ::std::optional<Interval> Interval::mod(const Interval &op2) const
  {
// mod is only supported for positive numbers
    if (_max.sign() < 0 || op2._max.sign() <= 0) {
      return ::std::nullopt;
    }
// since mod won't work for negative numbers we can ignore
// the portion of the numerator's interval that is negative
    auto l = _min.max(Integer::ZERO());
    return Interval(l, _max).remainder(op2);
  }

  Interval Interval::pow(::std::uint32_t op2) const
  {
    if (op2 == 0) {
      return PLUS_ONE();
    }

    ::std::cerr << "CP #1" << ::std::endl;
    auto l = _min.pow(op2);
    auto r = _max.pow(op2);

    ::std::cerr << "CP #2" << ::std::endl;
    Interval res;
    if (l <= r) {
      ::std::cerr << "CP #3" << ::std::endl;
      res = Interval(l, r);
    } else {
      ::std::cerr << "CP #4" << ::std::endl;
      res = Interval(r, l);
    }
    if (contains0()) {
      ::std::cerr << "CP #5" << ::std::endl;
      res = ZERO().unionWith(res);
    }
    return res;
  }

  ::std::ostream& Interval::write(::std::ostream &out) const
  {
    return out << '(' << _min << ';' << _max << ')';
  }
  ::std::string Interval::toString() const
  {
    return '(' + _min.toString() + ';' + _max.toString() + ')';
  }
}
