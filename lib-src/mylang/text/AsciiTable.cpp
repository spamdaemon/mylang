#include <mylang/text/AsciiTable.h>
#include <iostream>
#include <cassert>

namespace mylang {
  namespace text {
    namespace {
      struct Entry
      {
        const char *name;
        const char *unicode;
        const char *language[AsciiTable::NUM_LANGUAGES];
      };

      static const Entry ESCAPE_SEQUENCES[] = {
      /* 0x00 */{ "NUL", "\\u0000", { nullptr, "\\0" } },
      /* 0x01 */
      { "SOH", "\\u0001", { nullptr, "\\x01" } },
      /* 0x02 */
      { "STX", "\\u0002", { nullptr, "\\x02" } },
      /* 0x03 */
      { "ETX", "\\u0003", { nullptr, "\\x03" } },
      /* 0x04 */
      { "EOT", "\\u0004", { nullptr, "\\x04" } },
      /* 0x05 */
      { "ENQ", "\\u0005", { nullptr, "\\x05" } },
      /* 0x06 */
      { "ACK", "\\u0006", { nullptr, "\\x06" } },
      /* 0x07 */
      { "BEL", "\\u0007", { nullptr, "\\a" } },
      /* 0x08 */
      { "BS", "\\u0008", { "\\b", "\\b" } },
      /* 0x09 */
      { "TAB", "\\u0009", { "\\t", "\\t" } },
      /* 0x0a */
      { "LF", "\\u000a", { "\\n", "\\n" } },
      /* 0x0b */
      { "VT", "\\u000b", { nullptr, "\\v" } },
      /* 0x0c */
      { "FF", "\\u000c", { "\\f", "\\f" } },
      /* 0x0d */
      { "CR", "\\u000d", { "\\r", "\\r" } },
      /* 0x0e */
      { "SO", "\\u000e", { nullptr, "\\x0e" } },
      /* 0x0f */
      { "SI", "\\u000f", { nullptr, "\\x0f" } },

      /* 0x10 */
      { "DLE", "\\u0010", { nullptr, "\\x10" } },
      /* 0x11 */
      { "DC1", "\\u0011", { nullptr, "\\x11" } },
      /* 0x12 */
      { "DC2", "\\u0012", { nullptr, "\\x12" } },
      /* 0x13 */
      { "DC3", "\\u0013", { nullptr, "\\x13" } },
      /* 0x14 */
      { "DC4", "\\u0014", { nullptr, "\\x14" } },
      /* 0x15 */
      { "NAK", "\\u0015", { nullptr, "\\x15" } },
      /* 0x16 */
      { "SYW", "\\u0016", { nullptr, "\\x16" } },
      /* 0x17 */
      { "ETB", "\\u0017", { nullptr, "\\x17" } },
      /* 0x18 */
      { "CAN", "\\u0018", { nullptr, "\\x18" } },
      /* 0x19 */
      { "EM", "\\u0019", { nullptr, "\\x19" } },
      /* 0x1a */
      { "SUB", "\\u001a", { nullptr, "\\x1a" } },
      /* 0x1b */
      { "ESC", "\\u001b", { nullptr, "\\e" } },
      /* 0x1c */
      { "FS", "\\u001c", { nullptr, "\\x1b" } },
      /* 0x1d */
      { "GS", "\\u001d", { nullptr, "\\x1c" } },
      /* 0x1e */
      { "RS", "\\u001e", { nullptr, "\\x1d" } },
      /* 0x1f */
      { "US", "\\u001f", { nullptr, "\\x1e" } },

      /* 0x20 */
      { "Space", nullptr, { " ", " " } },
      /* 0x21 */
      { "!", nullptr, { nullptr, nullptr } },
      /* 0x22 */
      { "\"", nullptr, { "\\\"", "\\\"" } },
      /* 0x23 */
      { "#", nullptr, { nullptr, nullptr } },
      /* 0x24 */
      { "$", nullptr, { nullptr, nullptr } },
      /* 0x25 */
      { "%", nullptr, { nullptr, nullptr } },
      /* 0x26 */
      { "&", nullptr, { nullptr, nullptr } },
      /* 0x27 */
      { "'", nullptr, { "\\'", "\\'" } },
      /* 0x28 */
      { "(", nullptr, { nullptr, nullptr } },
      /* 0x29 */
      { ")", nullptr, { nullptr, nullptr } },
      /* 0x2a */
      { "*", nullptr, { nullptr, nullptr } },
      /* 0x2b */
      { "+", nullptr, { nullptr, nullptr } },
      /* 0x2c */
      { ",", nullptr, { nullptr, nullptr } },
      /* 0x2d */
      { "-", nullptr, { nullptr, nullptr } },
      /* 0x2e */
      { ".", nullptr, { nullptr, nullptr } },
      /* 0x2f */
      { "/", nullptr, { nullptr, nullptr } },

      /* 0x30 */
      { "0", nullptr, { nullptr, nullptr } },
      /* 0x31 */
      { "1", nullptr, { nullptr, nullptr } },
      /* 0x32 */
      { "2", nullptr, { nullptr, nullptr } },
      /* 0x33 */
      { "3", nullptr, { nullptr, nullptr } },
      /* 0x34 */
      { "4", nullptr, { nullptr, nullptr } },
      /* 0x35 */
      { "5", nullptr, { nullptr, nullptr } },
      /* 0x36 */
      { "6", nullptr, { nullptr, nullptr } },
      /* 0x37 */
      { "7", nullptr, { nullptr, nullptr } },
      /* 0x38 */
      { "8", nullptr, { nullptr, nullptr } },
      /* 0x39 */
      { "9", nullptr, { nullptr, nullptr } },
      /* 0x3a */
      { ":", nullptr, { nullptr, nullptr } },
      /* 0x3b */
      { ";", nullptr, { nullptr, nullptr } },
      /* 0x3c */
      { "<", nullptr, { nullptr, nullptr } },
      /* 0x3d */
      { "=", nullptr, { nullptr, nullptr } },
      /* 0x3e */
      { ">", nullptr, { nullptr, nullptr } },
      /* 0x3f */
      { "?", nullptr, { nullptr, "\\?" } },

      /* 0x40 */
      { "@", nullptr, { nullptr, nullptr } },
      /* 0x41 */
      { "A", nullptr, { nullptr, nullptr } },
      /* 0x42 */
      { "B", nullptr, { nullptr, nullptr } },
      /* 0x43 */
      { "C", nullptr, { nullptr, nullptr } },
      /* 0x44 */
      { "D", nullptr, { nullptr, nullptr } },
      /* 0x45 */
      { "E", nullptr, { nullptr, nullptr } },
      /* 0x46 */
      { "F", nullptr, { nullptr, nullptr } },
      /* 0x47 */
      { "G", nullptr, { nullptr, nullptr } },
      /* 0x48 */
      { "H", nullptr, { nullptr, nullptr } },
      /* 0x49 */
      { "I", nullptr, { nullptr, nullptr } },
      /* 0x4a */
      { "J", nullptr, { nullptr, nullptr } },
      /* 0x4b */
      { "K", nullptr, { nullptr, nullptr } },
      /* 0x4c */
      { "L", nullptr, { nullptr, nullptr } },
      /* 0x4d */
      { "M", nullptr, { nullptr, nullptr } },
      /* 0x4e */
      { "N", nullptr, { nullptr, nullptr } },
      /* 0x4f */
      { "O", nullptr, { nullptr, nullptr } },

      /* 0x50 */
      { "P", nullptr, { nullptr, nullptr } },
      /* 0x51 */
      { "Q", nullptr, { nullptr, nullptr } },
      /* 0x52 */
      { "R", nullptr, { nullptr, nullptr } },
      /* 0x53 */
      { "S", nullptr, { nullptr, nullptr } },
      /* 0x54 */
      { "T", nullptr, { nullptr, nullptr } },
      /* 0x55 */
      { "U", nullptr, { nullptr, nullptr } },
      /* 0x56 */
      { "V", nullptr, { nullptr, nullptr } },
      /* 0x57 */
      { "W", nullptr, { nullptr, nullptr } },
      /* 0x58 */
      { "X", nullptr, { nullptr, nullptr } },
      /* 0x59 */
      { "Y", nullptr, { nullptr, nullptr } },
      /* 0x5a */
      { "Z", nullptr, { nullptr, nullptr } },
      /* 0x5b */
      { "[", nullptr, { nullptr, nullptr } },
      /* 0x5c */
      { "\\", nullptr, { "\\\\", "\\\\" } },
      /* 0x5d */
      { "]", nullptr, { nullptr, nullptr } },
      /* 0x5e */
      { "^", nullptr, { nullptr, nullptr } },
      /* 0x5f */
      { "_", nullptr, { nullptr, nullptr } },

      /* 0x60 */
      { "`", nullptr, { nullptr, nullptr } },
      /* 0x61 */
      { "a", nullptr, { nullptr, nullptr } },
      /* 0x62 */
      { "b", nullptr, { nullptr, nullptr } },
      /* 0x63 */
      { "c", nullptr, { nullptr, nullptr } },
      /* 0x64 */
      { "d", nullptr, { nullptr, nullptr } },
      /* 0x65 */
      { "e", nullptr, { nullptr, nullptr } },
      /* 0x66 */
      { "f", nullptr, { nullptr, nullptr } },
      /* 0x67 */
      { "g", nullptr, { nullptr, nullptr } },
      /* 0x68 */
      { "h", nullptr, { nullptr, nullptr } },
      /* 0x69 */
      { "i", nullptr, { nullptr, nullptr } },
      /* 0x6a */
      { "j", nullptr, { nullptr, nullptr } },
      /* 0x6b */
      { "k", nullptr, { nullptr, nullptr } },
      /* 0x6c */
      { "l", nullptr, { nullptr, nullptr } },
      /* 0x6d */
      { "m", nullptr, { nullptr, nullptr } },
      /* 0x6e */
      { "n", nullptr, { nullptr, nullptr } },
      /* 0x6f */
      { "o", nullptr, { nullptr, nullptr } },

      /* 0x70 */
      { "p", nullptr, { nullptr, nullptr } },
      /* 0x71 */
      { "q", nullptr, { nullptr, nullptr } },
      /* 0x72 */
      { "r", nullptr, { nullptr, nullptr } },
      /* 0x73 */
      { "s", nullptr, { nullptr, nullptr } },
      /* 0x74 */
      { "t", nullptr, { nullptr, nullptr } },
      /* 0x75 */
      { "u", nullptr, { nullptr, nullptr } },
      /* 0x76 */
      { "v", nullptr, { nullptr, nullptr } },
      /* 0x77 */
      { "w", nullptr, { nullptr, nullptr } },
      /* 0x78 */
      { "x", nullptr, { nullptr, nullptr } },
      /* 0x79 */
      { "y", nullptr, { nullptr, nullptr } },
      /* 0x7a */
      { "z", nullptr, { nullptr, nullptr } },
      /* 0x7b */
      { "{", nullptr, { nullptr, nullptr } },
      /* 0x7c */
      { "|", nullptr, { nullptr, nullptr } },
      /* 0x7d */
      { "}", nullptr, { nullptr, nullptr } },
      /* 0x7e */
      { "~", nullptr, { nullptr, nullptr } },
      /* 0x7f */
      { "DEL", "\\u007f", { nullptr, "\\x7f" } },

      /* 0x80 */
      { nullptr, "\\u0080", { nullptr, nullptr } },
      /* 0x81 */
      { nullptr, "\\u0081", { nullptr, nullptr } },
      /* 0x82 */
      { nullptr, "\\u0082", { nullptr, nullptr } },
      /* 0x83 */
      { nullptr, "\\u0083", { nullptr, nullptr } },
      /* 0x84 */
      { nullptr, "\\u0084", { nullptr, nullptr } },
      /* 0x85 */
      { nullptr, "\\u0085", { nullptr, nullptr } },
      /* 0x86 */
      { nullptr, "\\u0086", { nullptr, nullptr } },
      /* 0x87 */
      { nullptr, "\\u0087", { nullptr, nullptr } },
      /* 0x88 */
      { nullptr, "\\u0088", { nullptr, nullptr } },
      /* 0x89 */
      { nullptr, "\\u0089", { nullptr, nullptr } },
      /* 0x8a */
      { nullptr, "\\u008a", { nullptr, nullptr } },
      /* 0x8b */
      { nullptr, "\\u008b", { nullptr, nullptr } },
      /* 0x8c */
      { nullptr, "\\u008c", { nullptr, nullptr } },
      /* 0x8d */
      { nullptr, "\\u008d", { nullptr, nullptr } },
      /* 0x8e */
      { nullptr, "\\u008e", { nullptr, nullptr } },
      /* 0x8f */
      { nullptr, "\\u008f", { nullptr, nullptr } },

      /* 0x90 */
      { nullptr, "\\u0090", { nullptr, nullptr } },
      /* 0x91 */
      { nullptr, "\\u0091", { nullptr, nullptr } },
      /* 0x92 */
      { nullptr, "\\u0092", { nullptr, nullptr } },
      /* 0x93 */
      { nullptr, "\\u0093", { nullptr, nullptr } },
      /* 0x94 */
      { nullptr, "\\u0094", { nullptr, nullptr } },
      /* 0x95 */
      { nullptr, "\\u0095", { nullptr, nullptr } },
      /* 0x96 */
      { nullptr, "\\u0096", { nullptr, nullptr } },
      /* 0x97 */
      { nullptr, "\\u0097", { nullptr, nullptr } },
      /* 0x98 */
      { nullptr, "\\u0098", { nullptr, nullptr } },
      /* 0x99 */
      { nullptr, "\\u0099", { nullptr, nullptr } },
      /* 0x9a */
      { nullptr, "\\u009a", { nullptr, nullptr } },
      /* 0x9b */
      { nullptr, "\\u009b", { nullptr, nullptr } },
      /* 0x9c */
      { nullptr, "\\u009c", { nullptr, nullptr } },
      /* 0x9d */
      { nullptr, "\\u009d", { nullptr, nullptr } },
      /* 0x9e */
      { nullptr, "\\u009e", { nullptr, nullptr } },
      /* 0x9f */
      { nullptr, "\\u009f", { nullptr, nullptr } },

      /* 0xa0 */
      { nullptr, "\\u00a0", { nullptr, nullptr } },
      /* 0xa1 */
      { nullptr, "\\u00a1", { nullptr, nullptr } },
      /* 0xa2 */
      { nullptr, "\\u00a2", { nullptr, nullptr } },
      /* 0xa3 */
      { nullptr, "\\u00a3", { nullptr, nullptr } },
      /* 0xa4 */
      { nullptr, "\\u00a4", { nullptr, nullptr } },
      /* 0xa5 */
      { nullptr, "\\u00a5", { nullptr, nullptr } },
      /* 0xa6 */
      { nullptr, "\\u00a6", { nullptr, nullptr } },
      /* 0xa7 */
      { nullptr, "\\u00a7", { nullptr, nullptr } },
      /* 0xa8 */
      { nullptr, "\\u00a8", { nullptr, nullptr } },
      /* 0xa9 */
      { nullptr, "\\u00a9", { nullptr, nullptr } },
      /* 0xaa */
      { nullptr, "\\u00aa", { nullptr, nullptr } },
      /* 0xab */
      { nullptr, "\\u00ab", { nullptr, nullptr } },
      /* 0xac */
      { nullptr, "\\u00ac", { nullptr, nullptr } },
      /* 0xad */
      { nullptr, "\\u00ad", { nullptr, nullptr } },
      /* 0xae */
      { nullptr, "\\u00ae", { nullptr, nullptr } },
      /* 0xaf */
      { nullptr, "\\u00af", { nullptr, nullptr } },

      /* 0xb0 */
      { nullptr, "\\u00b0", { nullptr, nullptr } },
      /* 0xb1 */
      { nullptr, "\\u00b1", { nullptr, nullptr } },
      /* 0xb2 */
      { nullptr, "\\u00b2", { nullptr, nullptr } },
      /* 0xb3 */
      { nullptr, "\\u00b3", { nullptr, nullptr } },
      /* 0xb4 */
      { nullptr, "\\u00b4", { nullptr, nullptr } },
      /* 0xb5 */
      { nullptr, "\\u00b5", { nullptr, nullptr } },
      /* 0xb6 */
      { nullptr, "\\u00b6", { nullptr, nullptr } },
      /* 0xb7 */
      { nullptr, "\\u00b7", { nullptr, nullptr } },
      /* 0xb8 */
      { nullptr, "\\u00b8", { nullptr, nullptr } },
      /* 0xb9 */
      { nullptr, "\\u00b9", { nullptr, nullptr } },
      /* 0xba */
      { nullptr, "\\u00ba", { nullptr, nullptr } },
      /* 0xbb */
      { nullptr, "\\u00bb", { nullptr, nullptr } },
      /* 0xbc */
      { nullptr, "\\u00bc", { nullptr, nullptr } },
      /* 0xbd */
      { nullptr, "\\u00bd", { nullptr, nullptr } },
      /* 0xbe */
      { nullptr, "\\u00be", { nullptr, nullptr } },
      /* 0xbf */
      { nullptr, "\\u00bf", { nullptr, nullptr } },

      /* 0xc0 */
      { nullptr, "\\u00c0", { nullptr, nullptr } },
      /* 0xc1 */
      { nullptr, "\\u00c1", { nullptr, nullptr } },
      /* 0xc2 */
      { nullptr, "\\u00c2", { nullptr, nullptr } },
      /* 0xc3 */
      { nullptr, "\\u00c3", { nullptr, nullptr } },
      /* 0xc4 */
      { nullptr, "\\u00c4", { nullptr, nullptr } },
      /* 0xc5 */
      { nullptr, "\\u00c5", { nullptr, nullptr } },
      /* 0xc6 */
      { nullptr, "\\u00c6", { nullptr, nullptr } },
      /* 0xc7 */
      { nullptr, "\\u00c7", { nullptr, nullptr } },
      /* 0xc8 */
      { nullptr, "\\u00c8", { nullptr, nullptr } },
      /* 0xc9 */
      { nullptr, "\\u00c9", { nullptr, nullptr } },
      /* 0xca */
      { nullptr, "\\u00ca", { nullptr, nullptr } },
      /* 0xcb */
      { nullptr, "\\u00cb", { nullptr, nullptr } },
      /* 0xcc */
      { nullptr, "\\u00cc", { nullptr, nullptr } },
      /* 0xcd */
      { nullptr, "\\u00cd", { nullptr, nullptr } },
      /* 0xce */
      { nullptr, "\\u00ce", { nullptr, nullptr } },
      /* 0xcf */
      { nullptr, "\\u00cf", { nullptr, nullptr } },

      /* 0xd0 */
      { nullptr, "\\u00d0", { nullptr, nullptr } },
      /* 0xd1 */
      { nullptr, "\\u00d1", { nullptr, nullptr } },
      /* 0xd2 */
      { nullptr, "\\u00d2", { nullptr, nullptr } },
      /* 0xd3 */
      { nullptr, "\\u00d3", { nullptr, nullptr } },
      /* 0xd4 */
      { nullptr, "\\u00d4", { nullptr, nullptr } },
      /* 0xd5 */
      { nullptr, "\\u00d5", { nullptr, nullptr } },
      /* 0xd6 */
      { nullptr, "\\u00d6", { nullptr, nullptr } },
      /* 0xd7 */
      { nullptr, "\\u00d7", { nullptr, nullptr } },
      /* 0xd8 */
      { nullptr, "\\u00d8", { nullptr, nullptr } },
      /* 0xd9 */
      { nullptr, "\\u00d9", { nullptr, nullptr } },
      /* 0xda */
      { nullptr, "\\u00da", { nullptr, nullptr } },
      /* 0xdb */
      { nullptr, "\\u00db", { nullptr, nullptr } },
      /* 0xdc */
      { nullptr, "\\u00dc", { nullptr, nullptr } },
      /* 0xdd */
      { nullptr, "\\u00dd", { nullptr, nullptr } },
      /* 0xde */
      { nullptr, "\\u00de", { nullptr, nullptr } },
      /* 0xdf */
      { nullptr, "\\u00df", { nullptr, nullptr } },

      /* 0xe0 */
      { nullptr, "\\u00e0", { nullptr, nullptr } },
      /* 0xe1 */
      { nullptr, "\\u00e1", { nullptr, nullptr } },
      /* 0xe2 */
      { nullptr, "\\u00e2", { nullptr, nullptr } },
      /* 0xe3 */
      { nullptr, "\\u00e3", { nullptr, nullptr } },
      /* 0xe4 */
      { nullptr, "\\u00e4", { nullptr, nullptr } },
      /* 0xe5 */
      { nullptr, "\\u00e5", { nullptr, nullptr } },
      /* 0xe6 */
      { nullptr, "\\u00e6", { nullptr, nullptr } },
      /* 0xe7 */
      { nullptr, "\\u00e7", { nullptr, nullptr } },
      /* 0xe8 */
      { nullptr, "\\u00e8", { nullptr, nullptr } },
      /* 0xe9 */
      { nullptr, "\\u00e9", { nullptr, nullptr } },
      /* 0xea */
      { nullptr, "\\u00ea", { nullptr, nullptr } },
      /* 0xeb */
      { nullptr, "\\u00eb", { nullptr, nullptr } },
      /* 0xec */
      { nullptr, "\\u00ec", { nullptr, nullptr } },
      /* 0xed */
      { nullptr, "\\u00ed", { nullptr, nullptr } },
      /* 0xee */
      { nullptr, "\\u00ee", { nullptr, nullptr } },
      /* 0xef */
      { nullptr, "\\u00ef", { nullptr, nullptr } },

      /* 0xf0 */
      { nullptr, "\\u00f0", { nullptr, nullptr } },
      /* 0xf1 */
      { nullptr, "\\u00f1", { nullptr, nullptr } },
      /* 0xf2 */
      { nullptr, "\\u00f2", { nullptr, nullptr } },
      /* 0xf3 */
      { nullptr, "\\u00f3", { nullptr, nullptr } },
      /* 0xf4 */
      { nullptr, "\\u00f4", { nullptr, nullptr } },
      /* 0xf5 */
      { nullptr, "\\u00f5", { nullptr, nullptr } },
      /* 0xf6 */
      { nullptr, "\\u00f6", { nullptr, nullptr } },
      /* 0xf7 */
      { nullptr, "\\u00f7", { nullptr, nullptr } },
      /* 0xf8 */
      { nullptr, "\\u00f8", { nullptr, nullptr } },
      /* 0xf9 */
      { nullptr, "\\u00f9", { nullptr, nullptr } },
      /* 0xfa */
      { nullptr, "\\u00fa", { nullptr, nullptr } },
      /* 0xfb */
      { nullptr, "\\u00fb", { nullptr, nullptr } },
      /* 0xfc */
      { nullptr, "\\u00fc", { nullptr, nullptr } },
      /* 0xfd */
      { nullptr, "\\u00fd", { nullptr, nullptr } },
      /* 0xfe */
      { nullptr, "\\u00fe", { nullptr, nullptr } },
      /* 0xff */
      { nullptr, "\\u00ff", { nullptr, nullptr } }, };

    }
    ::std::string AsciiTable::escapeText(Language lang, char ch)
    {
      ::std::string str(&ch, 1);
      return escapeText(lang, str);
    }

    ::std::string AsciiTable::escapeText(Language lang, const ::std::string &str)
    {
      ::std::string res;
      for (unsigned char ch : str) {
        const Entry &e = ESCAPE_SEQUENCES[ch];
        const char *seq = e.language[lang];
        if (!seq) {
          seq = e.unicode;
          if (!seq) {
            seq = e.name;
          }
        }
        if (!seq) {
          ::std::cerr << "failed to map character " << (int) ch << " to an escape sequence"
              << ::std::endl;
          seq = "?";
        }
        res += seq;
      }
      return res;
    }
  }
}
