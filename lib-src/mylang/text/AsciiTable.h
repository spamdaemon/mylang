#ifndef CLASS_MYLANG_TEXT_ASCIITABLE_H
#define CLASS_MYLANG_TEXT_ASCIITABLE_H
#include <string>

namespace mylang {
  namespace text {

    /**
     * The ascii table defines escape sequences for different languages for all ASCII characters.
     */
    class AsciiTable
    {
    public:
      enum Language
      {
        JAVA = 0, C = 1, NUM_LANGUAGES = 2
      };

    private:
      AsciiTable() = delete;

      ~AsciiTable() = delete;

      /**
       * Escape the specified string so that it may be used in a string or character literal.
       * Note: quotes are not added
       * @param lang the language to be escaped to
       * @param str the string to escape
       * @return a new string that can be passed a literal.
       */
    public:
      static ::std::string escapeText(Language lang, const ::std::string &str);

      /**
       * Escape the specified character so that it may be used in character literal.
       * Note: quotes are not added
       * @param lang the language to be escaped to
       * @param ch a char to escape
       * @return a new string that can be passed a literal.
       */
    public:
      static ::std::string escapeText(Language lang, char ch);
    };
  }
}
#endif
