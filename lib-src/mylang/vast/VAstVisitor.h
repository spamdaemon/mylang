#ifndef CLASS_MYLANG_VALIDATION_VASTVISITOR_H
#define CLASS_MYLANG_VALIDATION_VASTVISITOR_H
#include <idioma/ast/GroupNode.h>
#include <idioma/util/Key.h>
#include <mylang/defs.h>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>

#ifndef CLASS_MYLANG_VALIDATION_VAST_H
#include <mylang/vast/VAst.h>
#endif

namespace mylang {
  namespace vast {

    /**
     * This is a generic visitor for the the AST.
     */
    template<class T>
    class VAstVisitor
    {
    protected:
      VAstVisitor()
      {
      }

    public:
      virtual ~VAstVisitor()
      {
      }

      /**
       * Visit the specified node. If a new was generated in response to the visit
       * then a new node is returned, otherwise nullptr must be return indicate the the node was not modified.
       * @param node the node to visit (can be nullptr)
       * @return a new node or nullptr if no change
       */
    public:
      virtual T visit(GroupNodeCPtr node)
      {
        if (!node) {
          throw ::std::runtime_error("missing node");
        }

        auto key = node->type.value<VAst::GroupType>();
        if (!key) {
          ::std::cerr << "Missing key" << __FILE__ << ":" << __LINE__ << ::std::endl;
          throw ::std::runtime_error("Missing key");
        }
        if (key) {
          // ::std::cerr << "NODE : " << (int)key->key << ::std::endl;
          switch (key->key) {
          case VAst::GroupType::ROOT:
            return visit_root(node);
          case VAst::GroupType::INLINE_GROUP:
            return visit_inline_group(node);
          case VAst::GroupType::TYPE:
            return visit_type(node);
          case VAst::GroupType::VOID_EXPRESSION:
            return visit_void_expression(node);
          case VAst::GroupType::NAMED_EXPRESSION:
            return visit_named_expression(node);
          case VAst::GroupType::GUARD_EXPRESSION:
            return visit_guard_expression(node);
          case VAst::GroupType::GET_BIT_FROM_BITS_EXPRESSION:
            return visit_get_bit_from_bits_expression(node);
          case VAst::GroupType::GET_BOOLEAN_FROM_BITS_EXPRESSION:
            return visit_get_boolean_from_bits_expression(node);
          case VAst::GroupType::GET_BYTE_FROM_BITS_EXPRESSION:
            return visit_get_byte_from_bits_expression(node);
          case VAst::GroupType::GET_CHAR_FROM_BITS_EXPRESSION:
            return visit_get_char_from_bits_expression(node);
          case VAst::GroupType::GET_INTEGER_FROM_BITS_EXPRESSION:
            return visit_get_integer_from_bits_expression(node);
          case VAst::GroupType::GET_REAL_FROM_BITS_EXPRESSION:
            return visit_get_real_from_bits_expression(node);
          case VAst::GroupType::GET_BITS_FROM_BIT_EXPRESSION:
            return visit_get_bits_from_bit_expression(node);
          case VAst::GroupType::GET_BITS_FROM_BOOLEAN_EXPRESSION:
            return visit_get_bits_from_boolean_expression(node);
          case VAst::GroupType::GET_BITS_FROM_BYTE_EXPRESSION:
            return visit_get_bits_from_byte_expression(node);
          case VAst::GroupType::GET_BITS_FROM_CHAR_EXPRESSION:
            return visit_get_bits_from_char_expression(node);
          case VAst::GroupType::GET_BITS_FROM_INTEGER_EXPRESSION:
            return visit_get_bits_from_integer_expression(node);
          case VAst::GroupType::GET_BITS_FROM_REAL_EXPRESSION:
            return visit_get_bits_from_real_expression(node);
          case VAst::GroupType::GET_BYTE_AS_UNSIGNED_INTEGER_EXPRESSION:
            return visit_get_byte_as_unsigned_integer_expression(node);
          case VAst::GroupType::GET_BYTE_AS_SIGNED_INTEGER_EXPRESSION:
            return visit_get_byte_as_signed_integer_expression(node);
          case VAst::GroupType::BYTE_ENCODE_BIT_EXPRESSION:
            return visit_encode_bit_expression(node);
          case VAst::GroupType::BYTE_ENCODE_BOOLEAN_EXPRESSION:
            return visit_encode_boolean_expression(node);
          case VAst::GroupType::BYTE_ENCODE_BYTE_EXPRESSION:
            return visit_encode_byte_expression(node);
          case VAst::GroupType::BYTE_ENCODE_CHAR_EXPRESSION:
            return visit_encode_char_expression(node);
          case VAst::GroupType::BYTE_ENCODE_INTEGER_EXPRESSION:
            return visit_encode_integer_expression(node);
          case VAst::GroupType::BYTE_ENCODE_REAL_EXPRESSION:
            return visit_encode_real_expression(node);
          case VAst::GroupType::BYTE_ENCODE_STRING_EXPRESSION:
            return visit_encode_string_expression(node);
          case VAst::GroupType::BYTE_DECODE_BIT_EXPRESSION:
            return visit_decode_bit_expression(node);
          case VAst::GroupType::BYTE_DECODE_BOOLEAN_EXPRESSION:
            return visit_decode_boolean_expression(node);
          case VAst::GroupType::BYTE_DECODE_BYTE_EXPRESSION:
            return visit_decode_byte_expression(node);
          case VAst::GroupType::BYTE_DECODE_CHAR_EXPRESSION:
            return visit_decode_char_expression(node);
          case VAst::GroupType::BYTE_DECODE_INTEGER_EXPRESSION:
            return visit_decode_integer_expression(node);
          case VAst::GroupType::BYTE_DECODE_REAL_EXPRESSION:
            return visit_decode_real_expression(node);
          case VAst::GroupType::BYTE_DECODE_STRING_EXPRESSION:
            return visit_decode_string_expression(node);
          case VAst::GroupType::NOT_EXPRESSION:
            return visit_not_expression(node);
          case VAst::GroupType::REAL_NEGATE_EXPRESSION:
            return visit_real_negate_expression(node);
          case VAst::GroupType::REAL_ADD_EXPRESSION:
            return visit_real_add_expression(node);
          case VAst::GroupType::REAL_SUBTRACT_EXPRESSION:
            return visit_real_subtract_expression(node);
          case VAst::GroupType::REAL_MULTIPLY_EXPRESSION:
            return visit_real_multiply_expression(node);
          case VAst::GroupType::REAL_DIVIDE_EXPRESSION:
            return visit_real_divide_expression(node);
          case VAst::GroupType::REAL_REMAINDER_EXPRESSION:
            return visit_real_remainder_expression(node);
          case VAst::GroupType::INTEGER_WRAP_EXPRESSION:
            return visit_integer_wrap_expression(node);
          case VAst::GroupType::INTEGER_CLAMP_EXPRESSION:
            return visit_integer_clamp_expression(node);

          case VAst::GroupType::INTEGER_NEGATE_EXPRESSION:
            return visit_integer_negate_expression(node);
          case VAst::GroupType::INTEGER_ADD_EXPRESSION:
            return visit_integer_add_expression(node);
          case VAst::GroupType::INTEGER_SUBTRACT_EXPRESSION:
            return visit_integer_subtract_expression(node);
          case VAst::GroupType::INTEGER_MULTIPLY_EXPRESSION:
            return visit_integer_multiply_expression(node);
          case VAst::GroupType::INTEGER_DIVIDE_EXPRESSION:
            return visit_integer_divide_expression(node);
          case VAst::GroupType::INTEGER_MODULUS_EXPRESSION:
            return visit_integer_modulus_expression(node);
          case VAst::GroupType::INTEGER_REMAINDER_EXPRESSION:
            return visit_integer_remainder_expression(node);
          case VAst::GroupType::NEW_PROCESS_EXPRESSION:
            return visit_new_process_expression(node);
          case VAst::GroupType::NEW_BIT_EXPRESSION:
            return visit_new_bit_expression(node);
          case VAst::GroupType::NEW_BOOLEAN_EXPRESSION:
            return visit_new_boolean_expression(node);
          case VAst::GroupType::NEW_BYTE_EXPRESSION:
            return visit_new_byte_expression(node);
          case VAst::GroupType::NEW_CHAR_EXPRESSION:
            return visit_new_char_expression(node);
          case VAst::GroupType::NEW_INTEGER_EXPRESSION:
            return visit_new_integer_expression(node);
          case VAst::GroupType::NEW_REAL_EXPRESSION:
            return visit_new_real_expression(node);
          case VAst::GroupType::NEW_STRING_EXPRESSION:
            return visit_new_string_expression(node);
          case VAst::GroupType::NEW_ARRAY_EXPRESSION:
            return visit_new_array_expression(node);
          case VAst::GroupType::NEW_FUNCTION_EXPRESSION:
            return visit_new_function_expression(node);
          case VAst::GroupType::NEW_OPTIONAL_EXPRESSION:
            return visit_new_optional_expression(node);
          case VAst::GroupType::NEW_MUTABLE_EXPRESSION:
            return visit_new_mutable_expression(node);
          case VAst::GroupType::NEW_STRUCT_EXPRESSION:
            return visit_new_struct_expression(node);
          case VAst::GroupType::NEW_TUPLE_EXPRESSION:
            return visit_new_tuple_expression(node);
          case VAst::GroupType::NEW_UNION_EXPRESSION:
            return visit_new_union_expression(node);
          case VAst::GroupType::NEW_NAMEDTYPE_EXPRESSION:
            return visit_new_namedtype_expression(node);
          case VAst::GroupType::WITH_EXPRESSION:
            return visit_with_expression(node);
          case VAst::GroupType::LOOP_EXPRESSION:
            return visit_loop_expression(node);
          case VAst::GroupType::CONCATENATE_STRINGS_EXPRESSION:
            return visit_concatenate_strings_expression(node);
          case VAst::GroupType::CONCATENATE_ARRAYS_EXPRESSION:
            return visit_concatenate_arrays_expression(node);
          case VAst::GroupType::MERGE_TUPLES_EXPRESSION:
            return visit_merge_tuples_expression(node);
          case VAst::GroupType::ZIP_ARRAYS_EXPRESSION:
            return visit_zip_arrays_expression(node);
          case VAst::GroupType::TRANSFORM_EXPRESSION:
            return visit_transform_expression(node);
          case VAst::GroupType::CONDITIONAL_EXPRESSION:
            return visit_conditional_expression(node);
          case VAst::GroupType::SHORT_CIRCUIT_AND_EXPRESSION:
            return visit_short_circuit_and_expression(node);
          case VAst::GroupType::SHORT_CIRCUIT_OR_EXPRESSION:
            return visit_short_circuit_or_expression(node);
          case VAst::GroupType::AND_EXPRESSION:
            return visit_and_expression(node);
          case VAst::GroupType::OR_EXPRESSION:
            return visit_or_expression(node);
          case VAst::GroupType::XOR_EXPRESSION:
            return visit_xor_expression(node);
          case VAst::GroupType::COMPARISON_EQ_EXPRESSION:
            return visit_comparison_eq_expression(node);
          case VAst::GroupType::COMPARISON_NEQ_EXPRESSION:
            return visit_comparison_neq_expression(node);
          case VAst::GroupType::COMPARISON_LT_EXPRESSION:
            return visit_comparison_lt_expression(node);
          case VAst::GroupType::COMPARISON_LTE_EXPRESSION:
            return visit_comparison_lte_expression(node);
          case VAst::GroupType::COMPARISON_GT_EXPRESSION:
            return visit_comparison_gt_expression(node);
          case VAst::GroupType::COMPARISON_GTE_EXPRESSION:
            return visit_comparison_gte_expression(node);
          case VAst::GroupType::UNION_MEMBER_EXPRESSION:
            return visit_union_member_expression(node);
          case VAst::GroupType::STRUCT_MEMBER_EXPRESSION:
            return visit_struct_member_expression(node);
          case VAst::GroupType::PROCESS_MEMBER_EXPRESSION:
            return visit_process_member_expression(node);
          case VAst::GroupType::GET_MUTABLE_VALUE_EXPRESSION:
            return visit_get_mutable_value_expression(node);
          case VAst::GroupType::GET_OPTIONAL_VALUE_EXPRESSION:
            return visit_get_optional_value_expression(node);
          case VAst::GroupType::OPTIFY_EXPRESSION:
            return visit_optify_expression(node);
          case VAst::GroupType::INDEX_STRING_EXPRESSION:
            return visit_index_string_expression(node);
          case VAst::GroupType::INDEX_ARRAY_EXPRESSION:
            return visit_index_array_expression(node);
          case VAst::GroupType::SUBRANGE_STRING_EXPRESSION:
            return visit_subrange_string_expression(node);
          case VAst::GroupType::SUBRANGE_ARRAY_EXPRESSION:
            return visit_subrange_array_expression(node);
          case VAst::GroupType::TUPLE_MEMBER_EXPRESSION:
            return visit_tuple_member_expression(node);
          case VAst::GroupType::CALL_EXPRESSION:
            return visit_call_expression(node);
          case VAst::GroupType::LAMBDA_EXPRESSION:
            return visit_lambda_expression(node);
          case VAst::GroupType::CHECKED_CAST_EXPRESSION:
            return visit_checked_cast_expression(node);
          case VAst::GroupType::SAFE_TYPECAST_EXPRESSION:
            return visit_safe_cast_expression(node);
          case VAst::GroupType::IMPLICIT_TYPECAST_EXPRESSION:
            return visit_implicit_cast_expression(node);
          case VAst::GroupType::IMPLICIT_BASETYPECAST_EXPRESSION:
            return visit_implicit_basetypecast_expression(node);
          case VAst::GroupType::UNSAFE_TYPECAST_EXPRESSION:
            return visit_unsafe_cast_expression(node);
          case VAst::GroupType::GET_AS_BASETYPE_EXPRESSION:
            return visit_get_as_basetype_expression(node);
          case VAst::GroupType::GET_AS_ROOTTYPE_EXPRESSION:
            return visit_get_as_roottype_expression(node);
          case VAst::GroupType::TRY_EXPRESSION:
            return visit_try_expression(node);
          case VAst::GroupType::ORELSE_EXPRESSION:
            return visit_orelse_expression(node);
          case VAst::GroupType::LITERAL_INTEGER:
            return visit_literal_integer(node);
          case VAst::GroupType::LITERAL_REAL:
            return visit_literal_real(node);
          case VAst::GroupType::LITERAL_NIL:
            return visit_literal_nil(node);
          case VAst::GroupType::LITERAL_EMPTY_ARRAY:
            return visit_literal_empty_array(node);
          case VAst::GroupType::LITERAL_BIT:
            return visit_literal_bit(node);
          case VAst::GroupType::LITERAL_BOOLEAN:
            return visit_literal_boolean(node);
          case VAst::GroupType::LITERAL_BYTE:
            return visit_literal_byte(node);
          case VAst::GroupType::LITERAL_CHAR:
            return visit_literal_char(node);
          case VAst::GroupType::LITERAL_STRING:
            return visit_literal_string(node);
          case VAst::GroupType::LITERAL_ARRAY:
            return visit_literal_array(node);
          case VAst::GroupType::LITERAL_TUPLE:
            return visit_literal_tuple(node);
          case VAst::GroupType::LITERAL_TUPLE_MEMBER:
            return visit_literal_tuple_member(node);
          case VAst::GroupType::LITERAL_STRUCT:
            return visit_literal_struct(node);
          case VAst::GroupType::LITERAL_STRUCT_MEMBER:
            return visit_literal_struct_member(node);
          case VAst::GroupType::VARIABLE_REFERENCE_EXPRESSION:
            return visit_variable_reference_expression(node);
          case VAst::GroupType::READ_PORT_EXPRESSION:
            return visit_read_port_expression(node);
          case VAst::GroupType::CLEAR_PORT_EXPRESSION:
            return visit_clear_port_expression(node);
          case VAst::GroupType::WRITE_PORT_EXPRESSION:
            return visit_write_port_expression(node);
          case VAst::GroupType::WAIT_PORT_EXPRESSION:
            return visit_wait_port_expression(node);
          case VAst::GroupType::CLOSE_PORT_EXPRESSION:
            return visit_close_port_expression(node);
          case VAst::GroupType::BLOCK_READ_EVENTS_EXPRESSION:
            return visit_block_read_events_expression(node);
          case VAst::GroupType::BLOCK_WRITE_EVENTS_EXPRESSION:
            return visit_block_write_events_expression(node);
          case VAst::GroupType::GET_INPUT_EXPRESSION:
            return visit_get_input_expression(node);
          case VAst::GroupType::GET_OUTPUT_EXPRESSION:
            return visit_get_output_expression(node);
          case VAst::GroupType::DEF_VARIABLE:
            return visit_def_variable(node);
          case VAst::GroupType::DEF_TYPE:
            return visit_def_type(node);
          case VAst::GroupType::DEF_EXPORTED_FUNCTION:
            return visit_def_exported_function(node);
          case VAst::GroupType::DEF_FUNCTION:
            return visit_def_function(node);
          case VAst::GroupType::DEF_PROCESS:
            return visit_def_process(node);
          case VAst::GroupType::PROCESS_CONSTRUCTOR:
            return visit_process_constructor(node);
          case VAst::GroupType::PROCESS_BLOCK:
            return visit_process_block(node);
          case VAst::GroupType::DEF_NAMESPACE:
            return visit_def_namespace(node);
          case VAst::GroupType::FUNCTION_BODY:
            return visit_function_body(node);
          case VAst::GroupType::HAS_NEW_INPUT_EXPRESSION:
            return visit_has_new_input_expression(node);
          case VAst::GroupType::IS_PRESENT_EXPRESSION:
            return visit_is_present_expression(node);
          case VAst::GroupType::IS_OUTPUT_CLOSED_EXPRESSION:
            return visit_is_output_closed_expression(node);
          case VAst::GroupType::IS_INPUT_CLOSED_EXPRESSION:
            return visit_is_input_closed_expression(node);
          case VAst::GroupType::IS_PORT_READABLE_EXPRESSION:
            return visit_is_port_readable_expression(node);
          case VAst::GroupType::IS_PORT_WRITABLE_EXPRESSION:
            return visit_is_port_writable_expression(node);
          case VAst::GroupType::STRING_TO_CHARS_EXPRESSION:
            return visit_string_to_chars_expression(node);
          case VAst::GroupType::CHAR_TO_STRING_EXPRESSION:
            return visit_char_to_string_expression(node);
          case VAst::GroupType::INTERPOLATE_TEXT_EXPRESSION:
            return visit_interpolate_text_expression(node);
          case VAst::GroupType::STRINGIFY_EXPRESSION:
            return visit_stringify_expression(node);
          case VAst::GroupType::STRING_LENGTH_EXPRESSION:
            return visit_string_length_expression(node);
          case VAst::GroupType::ARRAY_LENGTH_EXPRESSION:
            return visit_array_length_expression(node);
          case VAst::GroupType::PAD_ARRAY_EXPRESSION:
            return visit_pad_array_expression(node);
          case VAst::GroupType::TRIM_ARRAY_EXPRESSION:
            return visit_trim_array_expression(node);
          case VAst::GroupType::DROP_ARRAY_EXPRESSION:
            return visit_drop_array_expression(node);
          case VAst::GroupType::TAKE_ARRAY_EXPRESSION:
            return visit_take_array_expression(node);
          case VAst::GroupType::MAP_ARRAY_EXPRESSION:
            return visit_map_array_expression(node);
          case VAst::GroupType::FILTER_ARRAY_EXPRESSION:
            return visit_filter_array_expression(node);
          case VAst::GroupType::FIND_ARRAY_EXPRESSION:
            return visit_find_array_expression(node);
          case VAst::GroupType::MAP_OPTIONAL_EXPRESSION:
            return visit_map_optional_expression(node);
          case VAst::GroupType::PARTITION_ARRAY_EXPRESSION:
            return visit_partition_array_expression(node);
          case VAst::GroupType::FLATTEN_ARRAY_EXPRESSION:
            return visit_flatten_array_expression(node);
          case VAst::GroupType::REVERSE_ARRAY_EXPRESSION:
            return visit_reverse_array_expression(node);
          case VAst::GroupType::FLATTEN_OPTIONAL_EXPRESSION:
            return visit_flatten_optional_expression(node);
          case VAst::GroupType::FOLD_ARRAY_EXPRESSION:
            return visit_fold_array_expression(node);
          case VAst::GroupType::CONVERT_TO_TUPLE_EXPRESSION:
            return visit_convert_to_tuple_expression(node);
          case VAst::GroupType::STRUCT_FROM_TUPLE_EXPRESSION:
            return visit_struct_from_tuple_expression(node);
          case VAst::GroupType::GET_HEAD_EXPRESSION:
            return visit_get_head_expression(node);
          case VAst::GroupType::GET_TAIL_EXPRESSION:
            return visit_get_tail_expression(node);
          case VAst::GroupType::ASSERT_STMT:
            return visit_assert_statement(node);
          case VAst::GroupType::BREAK_STMT:
            return visit_break_statement(node);
          case VAst::GroupType::CONTINUE_STMT:
            return visit_continue_statement(node);
          case VAst::GroupType::CALL_CONSTRUCTOR_STMT:
            return visit_call_constructor_statement(node);
          case VAst::GroupType::EXPRESSION_STMT:
            return visit_expression_statement(node);
          case VAst::GroupType::FOREACH_STMT:
            return visit_foreach_statement(node);
          case VAst::GroupType::IF_STMT:
            return visit_if_statement(node);
          case VAst::GroupType::LOG_STMT:
            return visit_log_statement(node);
          case VAst::GroupType::STATEMENT_LIST:
            return visit_statement_list(node);
          case VAst::GroupType::STATEMENT_BLOCK:
            return visit_statement_block(node);
          case VAst::GroupType::RETURN_STMT:
            return visit_return_statement(node);
          case VAst::GroupType::SET_STMT:
            return visit_set_statement(node);
          case VAst::GroupType::TRY_STMT:
            return visit_try_statement(node);
          case VAst::GroupType::THROW_STMT:
            return visit_throw_statement(node);
          case VAst::GroupType::UPDATE_STMT:
            return visit_update_statement(node);
          case VAst::GroupType::WAIT_STMT:
            return visit_wait_statement(node);
          case VAst::GroupType::WHILE_STMT:
            return visit_while_statement(node);
          case VAst::GroupType::DEF_GENERIC_FUNCTION:
            return visit_def_generic_function(node);
          case VAst::GroupType::INSTANTIATE_GENERIC_FUNCTION:
            return visit_instantiate_generic_function(node);
          default:
            break;
          }
        }
        throw ::std::runtime_error(
            "Unexpected group " + ::std::to_string((int) key->key) + ":" + __FILE__);
      }

    protected:
      virtual T visit_root(GroupNodeCPtr node) = 0;

      virtual T visit_literal_byte(GroupNodeCPtr node) = 0;

      virtual T visit_literal_bit(GroupNodeCPtr node) = 0;

      virtual T visit_literal_integer(GroupNodeCPtr node) = 0;

      virtual T visit_literal_real(GroupNodeCPtr node) = 0;

      virtual T visit_literal_boolean(GroupNodeCPtr node) = 0;

      virtual T visit_literal_array(GroupNodeCPtr node) = 0;

      virtual T visit_literal_tuple(GroupNodeCPtr node) = 0;

      virtual T visit_literal_tuple_member(GroupNodeCPtr node) = 0;

      virtual T visit_literal_struct(GroupNodeCPtr node) = 0;

      virtual T visit_literal_struct_member(GroupNodeCPtr node) = 0;

      virtual T visit_literal_string(GroupNodeCPtr node) = 0;

      virtual T visit_literal_char(GroupNodeCPtr node) = 0;

      virtual T visit_literal_nil(GroupNodeCPtr node) = 0;

      virtual T visit_literal_empty_array(GroupNodeCPtr node) = 0;

      virtual T visit_void_expression(GroupNodeCPtr node) = 0;

      virtual T visit_named_expression(GroupNodeCPtr node) = 0;

      virtual T visit_guard_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_bit_from_bits_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_boolean_from_bits_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_byte_from_bits_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_char_from_bits_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_integer_from_bits_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_real_from_bits_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_bits_from_bit_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_bits_from_boolean_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_bits_from_byte_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_bits_from_char_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_bits_from_integer_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_bits_from_real_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_input_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_output_expression(GroupNodeCPtr node) = 0;

      virtual T visit_variable_reference_expression(GroupNodeCPtr node) = 0;

      virtual T visit_new_process_expression(GroupNodeCPtr node) = 0;

      virtual T visit_new_bit_expression(GroupNodeCPtr node) = 0;

      virtual T visit_new_boolean_expression(GroupNodeCPtr node) = 0;

      virtual T visit_new_byte_expression(GroupNodeCPtr node) = 0;

      virtual T visit_new_char_expression(GroupNodeCPtr node) = 0;

      virtual T visit_new_integer_expression(GroupNodeCPtr node) = 0;

      virtual T visit_new_real_expression(GroupNodeCPtr node) = 0;

      virtual T visit_new_string_expression(GroupNodeCPtr node) = 0;

      virtual T visit_new_function_expression(GroupNodeCPtr node) = 0;

      virtual T visit_new_array_expression(GroupNodeCPtr node) = 0;

      virtual T visit_new_mutable_expression(GroupNodeCPtr node) = 0;

      virtual T visit_new_optional_expression(GroupNodeCPtr node) = 0;

      virtual T visit_new_struct_expression(GroupNodeCPtr node) = 0;

      virtual T visit_new_tuple_expression(GroupNodeCPtr node) = 0;

      virtual T visit_new_union_expression(GroupNodeCPtr node) = 0;

      virtual T visit_new_namedtype_expression(GroupNodeCPtr node) = 0;

      virtual T visit_integer_wrap_expression(GroupNodeCPtr node) = 0;

      virtual T visit_integer_clamp_expression(GroupNodeCPtr node) = 0;

      virtual T visit_integer_negate_expression(GroupNodeCPtr node) = 0;

      virtual T visit_integer_add_expression(GroupNodeCPtr node) = 0;

      virtual T visit_integer_subtract_expression(GroupNodeCPtr node) = 0;

      virtual T visit_integer_multiply_expression(GroupNodeCPtr node) = 0;

      virtual T visit_integer_divide_expression(GroupNodeCPtr node) = 0;

      virtual T visit_integer_modulus_expression(GroupNodeCPtr node) = 0;

      virtual T visit_integer_remainder_expression(GroupNodeCPtr node) = 0;

      virtual T visit_real_negate_expression(GroupNodeCPtr node) = 0;

      virtual T visit_real_add_expression(GroupNodeCPtr node) = 0;

      virtual T visit_real_subtract_expression(GroupNodeCPtr node) = 0;

      virtual T visit_real_multiply_expression(GroupNodeCPtr node) = 0;

      virtual T visit_real_divide_expression(GroupNodeCPtr node) = 0;

      virtual T visit_real_remainder_expression(GroupNodeCPtr node) = 0;

      virtual T visit_loop_expression(GroupNodeCPtr node) = 0;

      virtual T visit_with_expression(GroupNodeCPtr node) = 0;

      virtual T visit_and_expression(GroupNodeCPtr node) = 0;

      virtual T visit_or_expression(GroupNodeCPtr node) = 0;

      virtual T visit_xor_expression(GroupNodeCPtr node) = 0;

      virtual T visit_conditional_expression(GroupNodeCPtr node) = 0;

      virtual T visit_short_circuit_and_expression(GroupNodeCPtr node) = 0;

      virtual T visit_short_circuit_or_expression(GroupNodeCPtr node) = 0;

      virtual T visit_not_expression(GroupNodeCPtr node) = 0;

      virtual T visit_comparison_eq_expression(GroupNodeCPtr node) = 0;

      virtual T visit_comparison_neq_expression(GroupNodeCPtr node) = 0;

      virtual T visit_comparison_lt_expression(GroupNodeCPtr node) = 0;

      virtual T visit_comparison_lte_expression(GroupNodeCPtr node) = 0;

      virtual T visit_comparison_gt_expression(GroupNodeCPtr node) = 0;

      virtual T visit_comparison_gte_expression(GroupNodeCPtr node) = 0;

      virtual T visit_def_variable(GroupNodeCPtr node) = 0;

      virtual T visit_get_byte_as_unsigned_integer_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_byte_as_signed_integer_expression(GroupNodeCPtr node) = 0;

      virtual T visit_encode_bit_expression(GroupNodeCPtr expr) = 0;

      virtual T visit_encode_boolean_expression(GroupNodeCPtr expr) = 0;

      virtual T visit_encode_byte_expression(GroupNodeCPtr expr) = 0;

      virtual T visit_encode_char_expression(GroupNodeCPtr expr) = 0;

      virtual T visit_encode_integer_expression(GroupNodeCPtr expr) = 0;

      virtual T visit_encode_real_expression(GroupNodeCPtr expr) = 0;

      virtual T visit_encode_string_expression(GroupNodeCPtr expr) = 0;

      virtual T visit_decode_bit_expression(GroupNodeCPtr expr) = 0;

      virtual T visit_decode_boolean_expression(GroupNodeCPtr expr) = 0;

      virtual T visit_decode_byte_expression(GroupNodeCPtr expr) = 0;

      virtual T visit_decode_char_expression(GroupNodeCPtr expr) = 0;

      virtual T visit_decode_integer_expression(GroupNodeCPtr expr) = 0;

      virtual T visit_decode_real_expression(GroupNodeCPtr expr) = 0;

      virtual T visit_decode_string_expression(GroupNodeCPtr expr) = 0;

      virtual T visit_process_member_expression(GroupNodeCPtr node) = 0;

      virtual T visit_union_member_expression(GroupNodeCPtr node) = 0;

      virtual T visit_struct_member_expression(GroupNodeCPtr node) = 0;

      virtual T visit_tuple_member_expression(GroupNodeCPtr node) = 0;

      virtual T visit_optify_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_optional_value_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_mutable_value_expression(GroupNodeCPtr node) = 0;

      virtual T visit_index_string_expression(GroupNodeCPtr node) = 0;

      virtual T visit_index_array_expression(GroupNodeCPtr node) = 0;

      virtual T visit_subrange_string_expression(GroupNodeCPtr node) = 0;

      virtual T visit_subrange_array_expression(GroupNodeCPtr node) = 0;

      virtual T visit_call_expression(GroupNodeCPtr node) = 0;

      virtual T visit_lambda_expression(GroupNodeCPtr node) = 0;

      virtual T visit_concatenate_arrays_expression(GroupNodeCPtr node) = 0;

      virtual T visit_concatenate_strings_expression(GroupNodeCPtr node) = 0;

      virtual T visit_merge_tuples_expression(GroupNodeCPtr node) = 0;

      virtual T visit_zip_arrays_expression(GroupNodeCPtr node) = 0;

      virtual T visit_transform_expression(GroupNodeCPtr node) = 0;

      virtual T visit_checked_cast_expression(GroupNodeCPtr node) = 0;

      virtual T visit_safe_cast_expression(GroupNodeCPtr node) = 0;

      virtual T visit_implicit_cast_expression(GroupNodeCPtr node) = 0;

      virtual T visit_implicit_basetypecast_expression(GroupNodeCPtr node) = 0;

      virtual T visit_unsafe_cast_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_as_roottype_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_as_basetype_expression(GroupNodeCPtr node) = 0;

      virtual T visit_orelse_expression(GroupNodeCPtr node) = 0;

      virtual T visit_try_expression(GroupNodeCPtr node) = 0;

      virtual T visit_def_type(GroupNodeCPtr node) = 0;

      virtual T visit_def_namespace(GroupNodeCPtr node) = 0;

      virtual T visit_def_exported_function(GroupNodeCPtr node) = 0;

      virtual T visit_def_function(GroupNodeCPtr node) = 0;

      virtual T visit_def_process(GroupNodeCPtr node) = 0;

      virtual T visit_process_block(GroupNodeCPtr node) = 0;

      virtual T visit_process_constructor(GroupNodeCPtr node) = 0;

      virtual T visit_function_body(GroupNodeCPtr node) = 0;

      virtual T visit_read_port_expression(GroupNodeCPtr node) = 0;

      virtual T visit_clear_port_expression(GroupNodeCPtr node) = 0;

      virtual T visit_write_port_expression(GroupNodeCPtr node) = 0;

      virtual T visit_close_port_expression(GroupNodeCPtr node) = 0;

      virtual T visit_wait_port_expression(GroupNodeCPtr node) = 0;

      virtual T visit_block_read_events_expression(GroupNodeCPtr node) = 0;

      virtual T visit_block_write_events_expression(GroupNodeCPtr node) = 0;

      virtual T visit_has_new_input_expression(GroupNodeCPtr node) = 0;

      virtual T visit_is_present_expression(GroupNodeCPtr node) = 0;

      virtual T visit_is_input_closed_expression(GroupNodeCPtr node) = 0;

      virtual T visit_is_output_closed_expression(GroupNodeCPtr node) = 0;

      virtual T visit_is_port_readable_expression(GroupNodeCPtr node) = 0;

      virtual T visit_is_port_writable_expression(GroupNodeCPtr node) = 0;

      virtual T visit_array_length_expression(GroupNodeCPtr node) = 0;

      virtual T visit_string_length_expression(GroupNodeCPtr node) = 0;

      virtual T visit_string_to_chars_expression(GroupNodeCPtr node) = 0;

      virtual T visit_char_to_string_expression(GroupNodeCPtr node) = 0;

      virtual T visit_interpolate_text_expression(GroupNodeCPtr node) = 0;

      virtual T visit_stringify_expression(GroupNodeCPtr node) = 0;

      virtual T visit_fold_array_expression(GroupNodeCPtr node) = 0;

      virtual T visit_map_array_expression(GroupNodeCPtr node) = 0;

      virtual T visit_pad_array_expression(GroupNodeCPtr node) = 0;
      virtual T visit_trim_array_expression(GroupNodeCPtr node) = 0;
      virtual T visit_drop_array_expression(GroupNodeCPtr node) = 0;
      virtual T visit_take_array_expression(GroupNodeCPtr node) = 0;

      virtual T visit_filter_array_expression(GroupNodeCPtr node) = 0;

      virtual T visit_find_array_expression(GroupNodeCPtr node) = 0;

      virtual T visit_map_optional_expression(GroupNodeCPtr node) = 0;

      virtual T visit_reverse_array_expression(GroupNodeCPtr node) = 0;

      virtual T visit_flatten_array_expression(GroupNodeCPtr node) = 0;

      virtual T visit_partition_array_expression(GroupNodeCPtr node) = 0;

      virtual T visit_flatten_optional_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_head_expression(GroupNodeCPtr node) = 0;

      virtual T visit_get_tail_expression(GroupNodeCPtr node) = 0;

      virtual T visit_convert_to_tuple_expression(GroupNodeCPtr node) = 0;

      virtual T visit_struct_from_tuple_expression(GroupNodeCPtr node) = 0;

      virtual T visit_type(GroupNodeCPtr node) = 0;

      virtual T visit_statement_list(GroupNodeCPtr node) = 0;

      virtual T visit_statement_block(GroupNodeCPtr node) = 0;

      virtual T visit_assert_statement(GroupNodeCPtr node) = 0;

      virtual T visit_break_statement(GroupNodeCPtr node) = 0;

      virtual T visit_continue_statement(GroupNodeCPtr node) = 0;

      virtual T visit_call_constructor_statement(GroupNodeCPtr node) = 0;

      virtual T visit_expression_statement(GroupNodeCPtr node) = 0;

      virtual T visit_foreach_statement(GroupNodeCPtr node) = 0;

      virtual T visit_if_statement(GroupNodeCPtr node) = 0;

      virtual T visit_log_statement(GroupNodeCPtr node) = 0;

      virtual T visit_return_statement(GroupNodeCPtr node) = 0;

      virtual T visit_set_statement(GroupNodeCPtr node) = 0;

      virtual T visit_throw_statement(GroupNodeCPtr node) = 0;

      virtual T visit_try_statement(GroupNodeCPtr node) = 0;

      virtual T visit_update_statement(GroupNodeCPtr node) = 0;

      virtual T visit_wait_statement(GroupNodeCPtr node) = 0;

      virtual T visit_while_statement(GroupNodeCPtr node) = 0;

      virtual T visit_def_generic_function(GroupNodeCPtr node) = 0;

      virtual T visit_instantiate_generic_function(GroupNodeCPtr node) = 0;

      virtual T visit_inline_group(GroupNodeCPtr node) = 0;
    };
  }
}
#endif
