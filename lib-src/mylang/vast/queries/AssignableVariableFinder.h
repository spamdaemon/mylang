#ifndef CLASS_MYLANG_VAST_QUERIES_ASSIGNABLEVARIABLEFINDER_H
#define CLASS_MYLANG_VAST_QUERIES_ASSIGNABLEVARIABLEFINDER_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

namespace mylang {
  namespace vast {
    namespace queries {

      /** This class is used to locate the variable expression that is the basis for an expression */
      class AssignableVariableFinder
      {

      private:
        AssignableVariableFinder() = delete;

        ~AssignableVariableFinder() = delete;

        /**
         * Find the variable expression that serves as a basis for an assignable expression.
         * @param root a root node at which to check
         * @return a list of group nodes that are function definitions
         */
      public:
        static GroupNodeCPtr find(GroupNodeCPtr node);

      };

    }
  }
}
#endif
