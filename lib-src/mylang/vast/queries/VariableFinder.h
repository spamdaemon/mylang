#ifndef CLASS_MYLANG_VAST_QUERIES_VARIABLEFINDER_H
#define CLASS_MYLANG_VAST_QUERIES_VARIABLEFINDER_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#include <map>

namespace mylang {
  namespace vast {
    namespace queries {

      class VariableFinder
      {
        VariableFinder() = delete;

        ~VariableFinder() = delete;

        /**
         * Find all variable annotations for those nodes that also have a name annotation.
         */
      public:
        static ::std::map<NamePtr, VariablePtr> find(GroupNodeCPtr root);
      };

    }
  }
}
#endif
