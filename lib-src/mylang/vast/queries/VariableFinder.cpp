#include <mylang/vast/queries/VariableFinder.h>
#include <mylang/annotations.h>
#include <mylang/vast/VAstQuery.h>
#include <mylang/variables/Variable.h>
#include <mylang/names/Name.h>

namespace mylang {
  namespace vast {
    namespace queries {

      ::std::map<NamePtr, VariablePtr> VariableFinder::find(GroupNodeCPtr tree)
      {
        struct Finder: public VAstQuery
        {

          ~Finder()
          {
          }

          void visit(GroupNodeCPtr node) override final
          {
            auto var = getVariableAnnotation(node);
            if (var) {
              auto name = getNameAnnotation(node);
              if (name) {
                results[name] = var;
              }
            }
            VAstQuery::visit(node);
          }

          ::std::map<NamePtr, VariablePtr> results;
        };

        Finder f;
        f.visit(tree);
        return f.results;
      }
    }
  }
}
