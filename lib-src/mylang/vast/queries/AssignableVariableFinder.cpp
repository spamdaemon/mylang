#include <idioma/ast/GroupNode.h>
#include <mylang/annotations.h>
#include <mylang/names/Name.h>
#include <mylang/vast/queries/AssignableVariableFinder.h>
#include <mylang/vast/VAst.h>
#include <mylang/vast/VAstQuery.h>
#include <mylang/vast/VAstVisitor.h>
#include <memory>

namespace mylang {
  namespace vast {
    namespace queries {

      GroupNodeCPtr AssignableVariableFinder::find(GroupNodeCPtr node)
      {
        // Find all function that have been introduced by the transform.
        struct Finder: public VAstQuery
        {
          Finder()
          {
          }

          ~Finder()
          {
          }

          void visit_variable_reference_expression(GroupNodeCPtr node)
          {
            rootVariable = node;
          }

          void visit_struct_member_expression(GroupNodeCPtr node)
          {
            rootVariable = find(VAst::getExpr(node, VAst::Expr::EXPRESSION));
          }

          void visit_index_array_expression(GroupNodeCPtr node)
          {
            rootVariable = find(VAst::getExpr(node, VAst::Expr::LHS));
          }

          void visitChildNodes(const GroupNodeCPtr&)
          {
            // do not visit any child nodes
          }

          GroupNodeCPtr rootVariable;

        };

        Finder finder;
        finder.visit(node);
        return finder.rootVariable;
      }
    }
  }
}
