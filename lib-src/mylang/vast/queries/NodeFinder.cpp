#include <mylang/annotations.h>
#include <mylang/vast/queries/NodeFinder.h>
#include <mylang/vast/VAstQuery.h>

namespace mylang {
  namespace vast {
    namespace queries {

      GroupNodes NodeFinder::find(GroupNodeCPtr tree, SelectFN selectFN, RecurseFN recurseFN)
      {
        // Find all function that have been introduced by the transform.
        struct Finder: public VAstQuery
        {
          Finder(SelectFN xselectFN, RecurseFN xrecurseFN)
              : select(xselectFN), recurse(xrecurseFN)
          {
          }

          ~Finder()
          {
          }

          void visit(GroupNodeCPtr node) override final
          {
            if (!select || select(node)) {
              nodes.push_back(node);
            }
            if (!recurse || recurse(node)) {
              VAstQuery::visit(node);
            }
          }

          SelectFN select;
          RecurseFN recurse;
          ::std::vector<GroupNodeCPtr> nodes;
        };

        Finder f(selectFN, recurseFN);
        f.visit(tree);
        return ::std::move(f.nodes);
      }
    }
  }
}
