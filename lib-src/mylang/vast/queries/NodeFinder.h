#ifndef CLASS_MYLANG_VAST_QUERIES_NODEFINDER_H
#define CLASS_MYLANG_VAST_QUERIES_NODEFINDER_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif
#include <functional>
#include <vector>

namespace mylang {
  namespace vast {
    namespace queries {

      class NodeFinder
      {
        /** The function to determine if a node should be selected */
      public:
        typedef ::std::function<bool(const GroupNodeCPtr&)> SelectFN;

        /** This function determine whether to traverse into a node */
      public:
        typedef ::std::function<bool(const GroupNodeCPtr&)> RecurseFN;

      public:
        NodeFinder() = delete;

        ~NodeFinder() = delete;

        /**
         * Find all named functions defined in the tree. Lambda functions may not be found.
         * @param root a root node at which to check
         * @return a list of group nodes that are function definitions
         */
      public:
        static GroupNodes find(GroupNodeCPtr root, SelectFN select = SelectFN(), RecurseFN recurse =
            RecurseFN());

      };

    }
  }
}
#endif
