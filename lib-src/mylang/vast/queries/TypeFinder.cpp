#include <mylang/annotations.h>
#include <mylang/vast/queries/TypeFinder.h>
#include <mylang/vast/VAstQuery.h>
#include <mylang/constraints/ConstrainedTypeSet.h>

namespace mylang {
  namespace vast {
    namespace queries {

      ::std::vector<ConstrainedTypePtr> TypeFinder::find(GroupNodeCPtr tree,
          const ::std::function<bool(const ConstrainedTypePtr&)> filter)
      {
        struct Finder: public VAstQuery
        {

          ~Finder()
          {
          }

          void visit(GroupNodeCPtr node) override final
          {
            auto ty = getConstraintAnnotation(node);
            if (ty) {
              constraints.add(ty);
              ty->addRecursive(constraints);
            }
            VAstQuery::visit(node);
          }

          mylang::constraints::ConstrainedTypeSet constraints;
        };

        Finder f;
        f.visit(tree);
        ::std::vector<ConstrainedTypePtr> res;
        for (auto ty : f.constraints.types()) {
          if (filter(ty)) {
            res.push_back(ty);
          }
        }
        return res;
      }

      ::std::vector<ConstrainedTypePtr> TypeFinder::find(GroupNodeCPtr tree)
      {
        return find(tree, [&](const ConstrainedTypePtr&) -> bool {return true;});
      }
    }
  }
}
