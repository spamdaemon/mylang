#ifndef CLASS_MYLANG_VAST_QUERIES_TYPEFINDER_H
#define CLASS_MYLANG_VAST_QUERIES_TYPEFINDER_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPES_H
#include <mylang/constraints/ConstrainedType.h>
#endif
#include <functional>
#include <vector>

namespace mylang {
  namespace vast {
    namespace queries {

      class TypeFinder
      {
        TypeFinder() = delete;

        ~TypeFinder() = delete;

        /**
         * Find the set of types that are used in the specified tree.
         * @param root a tree
         * @return a set of types
         */
      public:
        static ::std::vector<ConstrainedTypePtr> find(GroupNodeCPtr root);

        /**
         * Find the set of types that are used in the specified tree.
         * @param root a tree
         * @param filter a way to reduce the set of functions
         * @return a set of types
         */
      public:
        static ::std::vector<ConstrainedTypePtr> find(GroupNodeCPtr root,
            const ::std::function<bool(const ConstrainedTypePtr&)> filter);
      };

    }
  }
}
#endif
