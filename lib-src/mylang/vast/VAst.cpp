#include <idioma/ast/GroupNode.h>
#include <idioma/astutils/utils.h>
#include <idioma/util/Key.h>
#include <mylang/annotations.h>
#include <mylang/constraints.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/expressions/Constant.h>
#include <mylang/names/Name.h>
#include <mylang/types.h>
#include <mylang/types/SerializableTypeChecker.h>
#include <mylang/vast/TypeRecursionChecker.h>
#include <mylang/vast/VAst.h>
#include <mylang/vast/VAstQuery.h>
#include <mylang/vast/VAstTransform.h>
#include <mylang/vast/InternalError.h>
#include <mylang/vast/ValidationError.h>
#include <mylang/variables/Variable.h>
#include <cstddef>
#include <cassert>
#include <iomanip>
#include <iostream>
#include <memory>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <typeinfo>
#include <utility>

namespace mylang {
  namespace vast {
    using namespace mylang::types;
    using namespace mylang::constraints;

    namespace {
#if 0
                                                                                                                                    static void trace_create_node(const char *file, size_t line, const char *function)
      {
        ::std::cerr << file << ':' << line << ':' << function << ":transform applied"
            << ::std::endl;
      }
#else

      static void trace_create_node(const char*, size_t, const char*)
      {
      }
#endif

      static ConstrainedIntegerType::Range createRange(BigInt from, BigInt to)
      {
        return ConstrainedIntegerType::Range(from, to);
      }

      /**
       * Cast each node to the specified constraint.
       * @param ty a type constraint
       * @param nodes the nodes to cast
       */
      static GroupNodes implicit_cast(const ConstrainedTypePtr &ty, const GroupNodes &nodes)
      {
        VAst ast;
        GroupNodes tmp;
        if (!nodes.empty()) {
          tmp.reserve(nodes.size());
          auto toType = ast.createType(ty, nodes.at(0)->span());

          for (auto n : nodes) {
            if (ty->isSameConstraint(*getConstraintAnnotation(n))) {
              tmp.push_back(n);
            } else {
              tmp.push_back(ast.createImplicitCastExpression(toType, n));
            }
          }
        }
        return tmp;
      }

      static GroupNodeCPtr implicit_cast(const ConstrainedTypePtr &ty, const GroupNodeCPtr &node)
      {
        GroupNodeCPtr res;
        if (node) {
          if (!ty) {
            throw ValidationError(node, "internal error: missing type");
          }
          auto ety = getConstraintAnnotation(node);
          if (ety->isSameConstraint(*ty)) {
            res = node;
          } else {
            VAst ast;
            res = ast.createImplicitCastExpression(ty, node);
          }
        }
        return res;
      }

      template<class T>
      static ::std::string type2string()
      {
        assert("Unsupported type");
        return typeid(T).name();
      }

      template<>
      ::std::string type2string<ConstrainedBitType>()
      {
        return "bit";
      }

      template<>
      ::std::string type2string<ConstrainedBooleanType>()
      {
        return "boolean";
      }

      template<>
      ::std::string type2string<ConstrainedByteType>()
      {
        return "byte";
      }

      template<>
      ::std::string type2string<ConstrainedCharType>()
      {
        return "char";
      }

      template<>
      ::std::string type2string<ConstrainedIntegerType>()
      {
        return "integer";
      }

      template<>
      ::std::string type2string<ConstrainedRealType>()
      {
        return "real";
      }

      template<>
      ::std::string type2string<ConstrainedStringType>()
      {
        return "string";
      }

      template<>
      ::std::string type2string<ConstrainedArrayType>()
      {
        return "array";
      }

      template<>
      ::std::string type2string<ConstrainedStructType>()
      {
        return "struct";
      }

      template<>
      ::std::string type2string<ConstrainedTupleType>()
      {
        return "tuple";
      }

      template<>
      ::std::string type2string<ConstrainedUnionType>()
      {
        return "union";
      }

      template<>
      ::std::string type2string<ConstrainedProcessType>()
      {
        return "process";
      }

      template<>
      ::std::string type2string<ConstrainedOptType>()
      {
        return "optional";
      }

      template<>
      ::std::string type2string<ConstrainedFunctionType>()
      {
        return "function";
      }

      template<>
      ::std::string type2string<ConstrainedInputType>()
      {
        return "input";
      }

      template<>
      ::std::string type2string<ConstrainedOutputType>()
      {
        return "output";
      }

      template<>
      ::std::string type2string<ConstrainedNamedType>()
      {
        return "named";
      }

      static void annotateConstraint(const GroupNodePtr &g, const ConstrainedTypePtr &c)
      {
        setConstraintAnnotation(g, c);
        setTypeAnnotation(g, c->type());
      }

      template<class T = mylang::constraints::ConstrainedType>
      static ::std::shared_ptr<const T> rootOf(ConstrainedTypePtr ty)
      {
        while (!ty->self<T>()) {
          auto b = ty->basetype();
          if (!b) {
            return nullptr;
          }
          ty = b;
        }
        return ty->self<T>();
      }

      template<class T = mylang::constraints::ConstrainedType>
      static ::std::shared_ptr<const T> constraintOf(const GroupNodeCPtr &g)
      {
        if (g) {
          auto ty = getConstraintAnnotation(g);
          if (!ty) {
            throw ValidationError(g, "internal error: missing constraint annotation");
          }
          return rootOf<T>(ty);
        }
        return nullptr;
      }

      enum class ReturnStatementDisposition
      {
        NONE, // no return statement
        PARTIAL, // there is return statement
        FULL // the statement is guaranteed to return
      };

      enum class ThrowStatementDisposition
      {
        NONE, // no throw statement
        PARTIAL, // there is throw statement
        FULL // the statement is guaranteed to throw
      };

      enum class BreakStatementDisposition
      {
        NONE, // no break statement
        PARTIAL, // there is break statement
        FULL // the statement is guaranteed to break
      };

      enum class ContinueStatementDisposition
      {
        NONE, // no continue statement
        PARTIAL, // there is continue statement
        FULL // the statement is guaranteed to continue
      };

      // find all return statements in the body, union them, and return the type
      static ThrowStatementDisposition throwStatementDisposition(const GroupNodes &statements)
      {
        struct V: public VAstQuery
        {
          V()
              : disposition(ThrowStatementDisposition::NONE)
          {
          }

          ~V()
          {
          }

          // do not traverse into functions
          void visit_function_body(GroupNodeCPtr) override
          {

          }

          void visit_foreach_statement(GroupNodeCPtr node) override
          {
            auto arrTy = constraintOf(VAst::getExpr(node, VAst::Expr::EXPRESSION))->self<
                ConstrainedArrayType>();

            visit(VAst::getStmt(node, VAst::Stmt::LOOP_BODY));
            if (!arrTy || arrTy->bounds.min() == 0) {
              if (disposition == ThrowStatementDisposition::FULL) {
                disposition = ThrowStatementDisposition::PARTIAL;
              }
            }
          }

          void visit_while_statement(GroupNodeCPtr node) override
          {
            visit(VAst::getStmt(node, VAst::Stmt::LOOP_BODY));
            if (disposition == ThrowStatementDisposition::FULL) {
              disposition = ThrowStatementDisposition::PARTIAL;
            }
          }

          void visit_if_statement(GroupNodeCPtr node) override
          {
            disposition = ThrowStatementDisposition::NONE;
            visit(VAst::getStmt(node, VAst::Stmt::IFTRUE));
            auto iftrue = disposition;
            disposition = ThrowStatementDisposition::NONE;
            visit(VAst::getStmt(node, VAst::Stmt::IFFALSE));
            auto iffalse = disposition;
            if (iffalse == iftrue) {
              disposition = iftrue;
            } else {
              disposition = ThrowStatementDisposition::PARTIAL;
            }
          }

          void visit_statement_block(GroupNodeCPtr node) override
          {
            disposition = throwStatementDisposition(
                VAst::getRelations(node, VAst::Relation::STATEMENTS));
          }

          void visit_statement_list(GroupNodeCPtr node) override
          {
            disposition = throwStatementDisposition(
                VAst::getRelations(node, VAst::Relation::STATEMENTS));
          }

          void visit_throw_statement(GroupNodeCPtr) override
          {
            disposition = ThrowStatementDisposition::FULL;
          }

          ThrowStatementDisposition disposition;
        };
        V v;
        for (auto s : statements) {
          // this is an error, the previous statement was already a throw statement
          if (v.disposition == ThrowStatementDisposition::FULL) {
            throw ValidationError(s, "unreachable statement");
          }
          v.disposition = ThrowStatementDisposition::NONE;
          v.visit(s);
        }
        return v.disposition;
      }

      // find all return statements in the body, union them, and return the type
      static BreakStatementDisposition breakStatementDisposition(const GroupNodes &statements)
      {
        struct V: public VAstQuery
        {
          V()
              : disposition(BreakStatementDisposition::NONE)
          {
          }

          ~V()
          {
          }

          // do not traverse into functions
          void visit_function_body(GroupNodeCPtr) override
          {
            // functions are already checked
          }

          void visit_if_statement(GroupNodeCPtr node) override
          {
            disposition = BreakStatementDisposition::NONE;
            visit(VAst::getStmt(node, VAst::Stmt::IFTRUE));
            auto iftrue = disposition;
            disposition = BreakStatementDisposition::NONE;
            visit(VAst::getStmt(node, VAst::Stmt::IFFALSE));
            auto iffalse = disposition;
            if (iffalse == iftrue) {
              disposition = iftrue;
            } else {
              disposition = BreakStatementDisposition::PARTIAL;
            }
          }

          void visit_statement_block(GroupNodeCPtr node) override
          {
            disposition = breakStatementDisposition(
                VAst::getRelations(node, VAst::Relation::STATEMENTS));
          }

          void visit_statement_list(GroupNodeCPtr node) override
          {
            disposition = breakStatementDisposition(
                VAst::getRelations(node, VAst::Relation::STATEMENTS));
          }

          void visit_break_statement(GroupNodeCPtr) override
          {
            disposition = BreakStatementDisposition::FULL;
          }
          void visit_foreach_statement(GroupNodeCPtr) override
          {
            // we need to ignore loops, which of can contain breaks and continue statements
          }
          void visit_while_statement(GroupNodeCPtr) override
          {
            // we need to ignore loops, which of can contain breaks and continue statements
          }

          BreakStatementDisposition disposition;
        };
        V v;
        for (auto s : statements) {
          // this is an error, the previous statement was already a throw statement
          if (v.disposition == BreakStatementDisposition::FULL) {
            throw ValidationError(s, "unreachable statement");
          }
          v.disposition = BreakStatementDisposition::NONE;
          v.visit(s);
        }
        return v.disposition;
      }

      // find all return statements in the body, union them, and return the type
      static ContinueStatementDisposition continueStatementDisposition(const GroupNodes &statements)
      {
        struct V: public VAstQuery
        {
          V()
              : disposition(ContinueStatementDisposition::NONE)
          {
          }

          ~V()
          {
          }

          // do not traverse into functions
          void visit_function_body(GroupNodeCPtr) override
          {

          }

          void visit_if_statement(GroupNodeCPtr node) override
          {
            disposition = ContinueStatementDisposition::NONE;
            visit(VAst::getStmt(node, VAst::Stmt::IFTRUE));
            auto iftrue = disposition;
            disposition = ContinueStatementDisposition::NONE;
            visit(VAst::getStmt(node, VAst::Stmt::IFFALSE));
            auto iffalse = disposition;
            if (iffalse == iftrue) {
              disposition = iftrue;
            } else {
              disposition = ContinueStatementDisposition::PARTIAL;
            }
          }

          void visit_statement_block(GroupNodeCPtr node) override
          {
            disposition = continueStatementDisposition(
                VAst::getRelations(node, VAst::Relation::STATEMENTS));
          }

          void visit_statement_list(GroupNodeCPtr node) override
          {
            disposition = continueStatementDisposition(
                VAst::getRelations(node, VAst::Relation::STATEMENTS));
          }

          void visit_continue_statement(GroupNodeCPtr) override
          {
            disposition = ContinueStatementDisposition::FULL;
          }
          void visit_foreach_statement(GroupNodeCPtr) override
          {
            // we need to ignore loops, which of can contain breaks and continue statements
          }
          void visit_while_statement(GroupNodeCPtr) override
          {
            // we need to ignore loops, which of can contain breaks and continue statements
          }

          ContinueStatementDisposition disposition;
        };
        V v;
        for (auto s : statements) {
          // this is an error, the previous statement was already a throw statement
          if (v.disposition == ContinueStatementDisposition::FULL) {
            throw ValidationError(s, "unreachable statement");
          }
          v.disposition = ContinueStatementDisposition::NONE;
          v.visit(s);
        }
        return v.disposition;
      }

      static ThrowStatementDisposition throwStatementDispositionOfBody(const GroupNodeCPtr &body)
      {
        assert(body->hasType(VAst::GroupType::FUNCTION_BODY));
        GroupNodes stmts = VAst::getRelations(body, VAst::Relation::STATEMENTS);
        return throwStatementDisposition(stmts);
      }

      static ReturnStatementDisposition returnStatementDisposition(const GroupNodes &statements)
      {
        struct V: public VAstQuery
        {
          V()
              : disposition(ReturnStatementDisposition::NONE)
          {
          }

          ~V()
          {
          }

          // do not traverse into functions
          void visit_function_body(GroupNodeCPtr) override
          {

          }

          void visit_foreach_statement(GroupNodeCPtr node) override
          {
            auto arrTy = constraintOf(VAst::getExpr(node, VAst::Expr::EXPRESSION))->self<
                ConstrainedArrayType>();

            visit(VAst::getStmt(node, VAst::Stmt::LOOP_BODY));
            if (!arrTy || arrTy->bounds.min() == 0) {
              if (disposition == ReturnStatementDisposition::FULL) {
                disposition = ReturnStatementDisposition::PARTIAL;
              }
            }
          }

          void visit_while_statement(GroupNodeCPtr node) override
          {
            visit(VAst::getStmt(node, VAst::Stmt::LOOP_BODY));
            if (disposition == ReturnStatementDisposition::FULL) {
              disposition = ReturnStatementDisposition::PARTIAL;
            }
          }

          void visit_if_statement(GroupNodeCPtr node) override
          {
            disposition = ReturnStatementDisposition::NONE;
            visit(VAst::getStmt(node, VAst::Stmt::IFTRUE));
            auto iftrue = disposition;
            disposition = ReturnStatementDisposition::NONE;
            visit(VAst::getStmt(node, VAst::Stmt::IFFALSE));
            auto iffalse = disposition;
            if (iffalse == iftrue) {
              disposition = iftrue;
            } else {
              disposition = ReturnStatementDisposition::PARTIAL;
            }
          }

          void visit_statement_block(GroupNodeCPtr node) override
          {
            disposition = returnStatementDisposition(
                VAst::getRelations(node, VAst::Relation::STATEMENTS));
          }

          void visit_statement_list(GroupNodeCPtr node) override
          {
            disposition = returnStatementDisposition(
                VAst::getRelations(node, VAst::Relation::STATEMENTS));
          }

          void visit_return_statement(GroupNodeCPtr) override
          {
            disposition = ReturnStatementDisposition::FULL;
          }

          ReturnStatementDisposition disposition;
        };
        V v;
        for (auto s : statements) {
          // this is an error, the previous statement was already a return statement
          if (v.disposition == ReturnStatementDisposition::FULL) {
            throw ValidationError(s, "unreachable statement");
          }
          v.disposition = ReturnStatementDisposition::NONE;
          v.visit(s);
        }
        return v.disposition;
      }

      // find all return statements in the body, union them, and return the type
      static ConstrainedTypePtr returnTypeOf(const GroupNodes &statements,
          const ConstrainedTypePtr &defaultType)
      {
        struct V: public VAstQuery
        {
          V()
          {
          }

          ~V()
          {
          }

          // do not traverse into functions
          void visit_function_body(GroupNodeCPtr) override
          {
          }

          void visit_return_statement(GroupNodeCPtr node) override
          {
            auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
            auto ty = expr ? constraintOf(expr) : ConstrainedVoidType::create();
            if (!res) {
              res = ty;
            } else {
              auto unionTy = ty->unionWith(res);
              if (!unionTy) {
                throw ValidationError(node,
                    "incompatible return types " + ty->toString() + " and " + res->toString());
              }
              res = unionTy;
            }
          }

          ConstrainedTypePtr res;
        };
        V v;
        for (auto s : statements) {
          v.visit(s);
        }
        return v.res ? v.res : defaultType;
      }

      static ReturnStatementDisposition returnStatementDispositionOfBody(const GroupNodeCPtr &body)
      {
        assert(body->hasType(VAst::GroupType::FUNCTION_BODY));
        GroupNodes stmts = VAst::getRelations(body, VAst::Relation::STATEMENTS);
        return returnStatementDisposition(stmts);
      }

      // find all return statements in the body, union them, and return the type
      static ConstrainedTypePtr returnTypeOf(const GroupNodeCPtr &body)
      {
        return returnTypeOf(VAst::getRelations(body, VAst::Relation::STATEMENTS),
            ConstrainedVoidType::create());
      }

      // perform an implicit cast of each return statement to the specified type
      static GroupNodeCPtr implicit_cast_function_body(const ConstrainedTypePtr &ty,
          const GroupNodeCPtr &body)
      {
        struct V: public VAstTransform
        {
          V(ConstrainedTypePtr ty)
              : inbody(false), nReturns(0), toTy(ty)
          {
          }

          ~V()
          {
          }

          // do not traverse into functions
          GroupNodeCPtr visit_function_body(GroupNodeCPtr node) override
          {
            if (!inbody) {
              inbody = true;
              return VAstTransform::visit_function_body(node);
            } else {
              return nullptr;
            }
          }

          GroupNodeCPtr visit_return_statement(GroupNodeCPtr node) override
          {
            auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
            auto retTy = expr ? constraintOf(expr) : ConstrainedVoidType::create();
            ++nReturns;
            if (toTy->isSameConstraint(*retTy)) {
              return nullptr;
            }
            if (expr) {
              expr = implicit_cast(toTy, expr);
            }
            return ast.createReturnStatement(expr);
          }

          bool inbody;
          size_t nReturns;
          ConstrainedTypePtr toTy;
        };
        V v(ty);
        auto res = v.visit(body);
        return res ? res : body;
      }

      static ::std::map<::std::string, GroupNodeCPtr> getNamedExpressions(const GroupNodes &argv)
      {
        ::std::map<::std::string, GroupNodeCPtr> args;
        for (auto arg : argv) {
          assert(arg->hasType(VAst::GroupType::NAMED_EXPRESSION));
          auto name = idioma::astutils::getToken(arg, VAst::Token::IDENTIFIER);
          auto expr = idioma::astutils::getGroup(arg, VAst::Expr::EXPRESSION);
          if (args.count(name->text) == 1) {
            throw ValidationError(name, "duplicate named parameter");
          }
          args[name->text] = expr;
        }
        return args;
      }

      static void ensureNoNamedExpressions(const GroupNodes &exprs)
      {
        for (auto e : exprs) {
          if (e->hasType(VAst::GroupType::NAMED_EXPRESSION)) {
            throw ValidationError(e, "unexpected named expression");
          }
        }
      }

      // returns true if named expressions, false if unnamed expressions
      static bool ensureNoMixedExpressions(const GroupNodes &exprs)
      {
        int isNamedExpr = 0;
        for (auto e : exprs) {
          if (isNamedExpr >= 0 && e->hasType(VAst::GroupType::NAMED_EXPRESSION)) {
            isNamedExpr = 1;
          } else if (isNamedExpr <= 0 && !e->hasType(VAst::GroupType::NAMED_EXPRESSION)) {
            isNamedExpr = -1;
          } else {
            throw ValidationError(e, "named and unnamed expressions cannot be mixed");
          }
        }
        return isNamedExpr >= 1;
      }

      static GroupNodePtr createCopyConstructor(GroupNodeCPtr type, GroupNodes argv)
      {
        if (argv.size() != 1) {
          return nullptr;
        }
        if (argv.at(0)->hasType(VAst::GroupType::NAMED_EXPRESSION)) {
          return nullptr;
        }

        auto ty = constraintOf(type);
        auto argTy = constraintOf(argv.at(0));
        if (!ty->canImplicitlyCastFrom(*argTy)) {
          return nullptr;
        }
        VAst ast;
        return ast.createImplicitCastExpression(type, argv.at(0));
      }

      static void expectPort(GroupNodeCPtr node)
      {
        auto ty = constraintOf(node);
        if (nullptr == ty->self<ConstrainedInputType>()
            && nullptr == ty->self<ConstrainedOutputType>()) {
          throw ValidationError(node, "input or output port expected");
        }
      }

      static void expectOneOf(const ::std::vector<TypePtr> &types, const GroupNodeCPtr &actual)
      {
        auto aty = constraintOf(actual);
        ::std::string message = "unexpected " + aty->toString() + " type; expected one of ";
        for (size_t i = 0; i < types.size(); ++i) {
          auto t = types.at(i);
          for (auto c = aty; c;) {
            if (t->isSameType(*c->type())) {
              return;
            }
            auto b = c->basetype();
            if (!b) {
              break;
            }
            c = b;
          }
          if (i > 0) {
            message += ", ";
          }
          message += t->toString();
        }
        throw ValidationError(actual, message);
      }

      void expectBitlike(const GroupNodeCPtr &expr)
      {
        auto bitTy = PrimitiveType::getBit();
        auto boolTy = PrimitiveType::getBoolean();
        auto byteTy = PrimitiveType::getByte();
        //		auto bitsTy = ArrayType::get(bitTy);
        //		auto boolsTy = ArrayType::get(boolTy);
        return expectOneOf( { bitTy, boolTy, byteTy }, expr);
      }

      // ensure that the node has a type constraint annotation
      static ConstrainedTypePtr ensureConstrainedType(const GroupNodeCPtr &n)
      {
        auto ty = getConstraintAnnotation(n);
        if (!ty) {
          throw ValidationError(n, "internal error: missing type annotation");
        }
        return ty;
      }

      template<class T>
      static void expect(const GroupNodeCPtr &n)
      {
        auto ty = ensureConstrainedType(n);

        if (!rootOf<T>(ty)) {
          throw ValidationError(n,
              ::std::string("expected ") + type2string<T>() + " type, but found " + ty->toString());
        }
      }

      template<class T = ConstrainedType>
      static ::std::shared_ptr<const T> expectExact(const GroupNodeCPtr &n)
      {
        auto ty = ensureConstrainedType(n);
        auto resTy = ty->self<T>();
        if (!resTy) {
          throw ValidationError(n,
              ::std::string("expected ") + type2string<T>() + " type, but found " + ty->toString());
        }
        return resTy;
      }

      template<class T = ConstrainedType>
      static void expectConcreteConstraint(const GroupNodeCPtr &actual)
      {
        expectExact<T>(actual);
        auto ty = constraintOf(actual);
        if (!ty->isConcrete()) {
          throw ValidationError(actual, "not a concrete type: " + ty->toString());
        }

      }

      static void expectSerializableConstraint(const GroupNodeCPtr &actual)
      {
        ensureConstrainedType(actual);
        auto ty = constraintOf(actual);
        if (!mylang::types::SerializableTypeChecker::isSerializable(ty->type())) {
          throw ValidationError(actual, "not a serializable type: " + ty->toString());
        }
      }

      /**
       * Implicitly cast each node to the union of the types.
       * @param nodes nodes to be casted
       */
      static GroupNodes union_cast(const GroupNodes &nodes)
      {
        const size_t sz = nodes.size();
        if (sz < 2) {
          return nodes;
        }

        ConstrainedTypePtr initial_ty = constraintOf(nodes.at(0));
        auto ty = initial_ty;
        for (size_t i = 1; i < sz; ++i) {
          auto i_ty = constraintOf(nodes.at(i));
          ty = ty->unionWith(i_ty);
          if (!ty) {
            throw ValidationError(nodes.at(i),
                "cannot compute union type of " + i_ty->toString() + " and "
                    + initial_ty->toString());
          }
        }
        return implicit_cast(ty, nodes);
      }

      static void union_cast(GroupNodeCPtr &a, GroupNodeCPtr &b)
      {
        auto tmp = union_cast( { a, b });
        a = tmp.at(0);
        b = tmp.at(1);
      }

      static void intersection_cast(GroupNodeCPtr &a, GroupNodeCPtr &b)
      {
        auto aty = constraintOf(a);
        auto bty = constraintOf(b);
        auto ity = aty->intersectWith(bty);
        if (!ity) {
          throw ValidationError(b,
              "incompatible types" + aty->toString() + " and " + bty->toString());
        }
        a = implicit_cast(ity, a);
        b = implicit_cast(ity, b);
      }

      static void asRootType(GroupNodeCPtr &expr)
      {
        VAst ast;
        expr = ast.castToRootType(expr);
      }
    }

    GroupNodePtr VAst::createType(const ConstrainedTypePtr &type,
        const ::idioma::ast::Node::Span &span,
        const ::std::vector<idioma::ast::GroupNode::Rel> &rels)
    {
      if (!type) {
        throw ::std::invalid_argument("Missing type");
      }
      ::std::vector<idioma::ast::GroupNode::Rel> relations(rels);
      relations.push_back(idioma::ast::GroupNode::Rel(TokenNode::create(type->toString(), span)));

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto grp = GroupNode::create(VAst::GroupType::TYPE, relations);
      annotateConstraint(grp, type);
      return grp;
    }

    GroupNodePtr VAst::createRoot(GroupNodes decls)
    {
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::ROOT, { { VAst::Relation::DECLS, decls } });
      return res;
    }

    GroupNodePtr VAst::createVoidExpression()
    {
      auto ty = ConstrainedVoidType::create();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::VOID_EXPRESSION, { });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createFoldArrayExpression(GroupNodeCPtr arrayExpr,
        GroupNodeCPtr initialValue,
        ::std::function<GroupNodes(GroupNodeCPtr elem, GroupNodeCPtr accum)> foldFn)
    {
      asRootType(arrayExpr);
      expectConcreteConstraint<ConstrainedArrayType>(arrayExpr);

      auto retTy = constraintOf(initialValue);
      auto arg0Ty = constraintOf<ConstrainedArrayType>(arrayExpr)->element;
      auto arg1Ty = retTy;
      auto fnTy = ConstrainedFunctionType::get(retTy, { arg0Ty, arg1Ty });

      auto lambda = createLambdaExpression(*fnTy, arrayExpr->span(), [&](GroupNodes args) {
        return foldFn(args.at(0), args.at(1));
      });
      return createFoldArrayExpression(arrayExpr, initialValue, lambda);
    }

    GroupNodePtr VAst::createFoldArrayExpression(GroupNodeCPtr arrayExpr,
        GroupNodeCPtr initialValue, GroupNodeCPtr foldFn)
    {
      asRootType(arrayExpr);
      asRootType(foldFn);

      expectConcreteConstraint<ConstrainedArrayType>(arrayExpr);
      expect<ConstrainedFunctionType>(foldFn);
      auto fnTy = constraintOf<ConstrainedFunctionType>(foldFn);

      if (fnTy->parameters.size() != 2) {
        throw ValidationError(foldFn,
            "expected 2 parameters, but found " + std::to_string(fnTy->parameters.size()));
      }
      auto arrTy = constraintOf<ConstrainedArrayType>(arrayExpr);
      auto elementTy = arrTy->element;
      auto accumTy = fnTy->parameters.at(1);

      fnTy = ConstrainedFunctionType::get(accumTy, { elementTy, accumTy });
      foldFn = implicit_cast(fnTy, foldFn);
      initialValue = implicit_cast(accumTy, initialValue);

      auto ty = accumTy;
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::FOLD_ARRAY_EXPRESSION, { {
          VAst::Expr::EXPRESSION, arrayExpr }, { VAst::Expr::INITIALIZER, initialValue }, {
          VAst::Expr::FUNCTION, foldFn } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createFlattenOptionalExpression(GroupNodeCPtr optionalExpr)
    {
      asRootType(optionalExpr);
      expectConcreteConstraint<ConstrainedOptType>(optionalExpr);

      auto optTy = constraintOf<ConstrainedOptType>(optionalExpr);
      auto elemTy = optTy->element->self<ConstrainedOptType>();
      while (elemTy) {
        optTy = elemTy;
        elemTy = optTy->element->self<ConstrainedOptType>();
      }
      auto ty = optTy;
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::FLATTEN_OPTIONAL_EXPRESSION, { {
          VAst::Expr::EXPRESSION, optionalExpr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createReverseArrayExpression(GroupNodeCPtr arrayExpr)
    {
      asRootType(arrayExpr);
      expectConcreteConstraint<ConstrainedArrayType>(arrayExpr);
      auto ty = constraintOf<ConstrainedArrayType>(arrayExpr);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::REVERSE_ARRAY_EXPRESSION, { {
          VAst::Expr::EXPRESSION, arrayExpr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createFlattenArrayExpression(GroupNodeCPtr arrayExpr)
    {
      asRootType(arrayExpr);
      expectConcreteConstraint<ConstrainedArrayType>(arrayExpr);

      auto arrTy = constraintOf<ConstrainedArrayType>(arrayExpr);
      auto elemTy = arrTy->element->self<ConstrainedArrayType>();
      if (!elemTy) {
        throw ValidationError(arrayExpr, "expected a 2-dimensional array");
      }
      auto ty = ConstrainedArrayType::getBoundedArray(elemTy->element,
          arrTy->bounds * elemTy->bounds);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::FLATTEN_ARRAY_EXPRESSION, { {
          VAst::Expr::EXPRESSION, arrayExpr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createPartitionArrayExpression(GroupNodeCPtr arrayExpr,
        GroupNodeCPtr partitionSizeExpr)
    {
      asRootType(arrayExpr);
      expectConcreteConstraint<ConstrainedArrayType>(arrayExpr);
      expect<ConstrainedIntegerType>(partitionSizeExpr);

      auto arrTy = constraintOf<ConstrainedArrayType>(arrayExpr);
      auto partTy = constraintOf<ConstrainedIntegerType>(partitionSizeExpr);

      if (partTy->range.min() < 1) {
        throw ValidationError(arrayExpr, "partition size must at least be 1");
      }
      if (arrTy->bounds.max().isFinite() && arrTy->bounds.max() > 0) {
        if (partTy->range.min() > arrTy->bounds.max()) {
          throw ValidationError(partitionSizeExpr,
              "minimum partition size exceeds the maximum array size");
        }
      }

      if (arrTy->bounds.isFixed() && partTy->range.isFixed()) {
        auto mod = arrTy->bounds.min() % partTy->range.min();
        if (*mod != 0) {
          throw ValidationError(arrayExpr, "array size must be a multiple of the partition size");
        }
      }

      // FIXME: tighten up the bounds even more
      auto range = partTy->range;

      // we can conveniently use the partitionSize's bounds for the bounds of inner array
      auto elemTy = ConstrainedArrayType::getBoundedArray(arrTy->element, partTy->range);

      ConstrainedArrayType::Range::EndPoint min;

      if (arrTy->bounds.min() == 0) {
        min = 0;
      } else if (range.isInfinite()) {
        min = 1;
      } else {
        auto div = arrTy->bounds.min() / range.max();
        min = div->max(1);
      }
      auto max = *(arrTy->bounds.max() / range.min());

      auto ty = ConstrainedArrayType::getBoundedArray(elemTy,
          ConstrainedArrayType::Range(min, max));
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::PARTITION_ARRAY_EXPRESSION, { {
          VAst::Expr::EXPRESSION, arrayExpr }, { VAst::Expr::SIZE, partitionSizeExpr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createPadArrayExpression(GroupNodeCPtr arrayExpr, GroupNodeCPtr padToSize,
        GroupNodeCPtr initializer)
    {
      asRootType(arrayExpr);
      expectConcreteConstraint<ConstrainedArrayType>(arrayExpr);
      expect<ConstrainedIntegerType>(padToSize);

      auto arrTy = constraintOf<ConstrainedArrayType>(arrayExpr);
      auto padToSizeTy = constraintOf<ConstrainedIntegerType>(padToSize);
      auto initTy = constraintOf(initializer);

      if (!padToSizeTy->isUnsigned()) {
        throw ValidationError(padToSize, "padToSize must be a non-negative integer");
      }
      auto newBounds = arrTy->bounds.pairwiseMax(padToSizeTy->range);

      auto ty = ConstrainedArrayType::getBoundedArray(arrTy->element, newBounds);
      if (ty->element->isAnyType()) {
        ty = ty->copy(initTy);
      }
      auto init = implicit_cast(ty->element, initializer);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::PAD_ARRAY_EXPRESSION, { {
          VAst::Expr::EXPRESSION, arrayExpr }, { VAst::Expr::SIZE, padToSize }, {
          VAst::Expr::INITIALIZER, init } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createTrimArrayExpression(GroupNodeCPtr arrayExpr, GroupNodeCPtr trimToSize)
    {
      asRootType(arrayExpr);
      expectConcreteConstraint<ConstrainedArrayType>(arrayExpr);
      expect<ConstrainedIntegerType>(trimToSize);

      auto arrTy = constraintOf<ConstrainedArrayType>(arrayExpr);
      auto trimToSizeTy = constraintOf<ConstrainedIntegerType>(trimToSize);

      if (!trimToSizeTy->isUnsigned()) {
        throw ValidationError(trimToSize, "trimToSize must be a non-negative integer");
      }
      auto newBounds = arrTy->bounds.pairwiseMin(trimToSizeTy->range);

      auto ty = ConstrainedArrayType::getBoundedArray(arrTy->element, newBounds);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::TRIM_ARRAY_EXPRESSION, { {
          VAst::Expr::EXPRESSION, arrayExpr }, { VAst::Expr::SIZE, trimToSize } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createDropArrayExpression(GroupNodeCPtr arrayExpr, GroupNodeCPtr count)
    {
      asRootType(arrayExpr);
      expectConcreteConstraint<ConstrainedArrayType>(arrayExpr);
      expect<ConstrainedIntegerType>(count);

      auto arrTy = constraintOf<ConstrainedArrayType>(arrayExpr);
      auto countTy = constraintOf<ConstrainedIntegerType>(count);

      auto newBounds = (arrTy->bounds.subtract(countTy->range.abs())).pairwiseMax(0);

      auto ty = ConstrainedArrayType::getBoundedArray(arrTy->element, newBounds);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::DROP_ARRAY_EXPRESSION, { {
          VAst::Expr::EXPRESSION, arrayExpr }, { VAst::Expr::SIZE, count } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createTakeArrayExpression(GroupNodeCPtr arrayExpr, GroupNodeCPtr count)
    {
      asRootType(arrayExpr);
      expectConcreteConstraint<ConstrainedArrayType>(arrayExpr);
      expect<ConstrainedIntegerType>(count);

      auto arrTy = constraintOf<ConstrainedArrayType>(arrayExpr);
      auto countTy = constraintOf<ConstrainedIntegerType>(count);

      auto newBounds = arrTy->bounds.pairwiseMin(countTy->range.abs());

      auto ty = ConstrainedArrayType::getBoundedArray(arrTy->element, newBounds);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::TAKE_ARRAY_EXPRESSION, { {
          VAst::Expr::EXPRESSION, arrayExpr }, { VAst::Expr::SIZE, count } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createMapArrayExpression(GroupNodeCPtr mapFn, GroupNodeCPtr arrayExpr)
    {
      asRootType(arrayExpr);
      asRootType(mapFn);

      expect<ConstrainedFunctionType>(mapFn);
      expectConcreteConstraint<ConstrainedArrayType>(arrayExpr);

      auto fnTy = constraintOf<ConstrainedFunctionType>(mapFn);

      if (fnTy->parameters.size() != 1) {
        throw ValidationError(mapFn, "expected one argument");
      }
      if (fnTy->returnType->isSameConstraint(*ConstrainedPrimitiveType::getVoid())) {
        throw ValidationError(mapFn, "function cannot be void");
      }
      auto arrTy = constraintOf<ConstrainedArrayType>(arrayExpr);
      auto elementTy = arrTy->element;
      fnTy = ConstrainedFunctionType::get(fnTy->returnType, { elementTy });
      mapFn = implicit_cast(fnTy, mapFn);

      auto ty = arrTy->copy(fnTy->returnType);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::MAP_ARRAY_EXPRESSION, { {
          VAst::Expr::EXPRESSION, arrayExpr }, { VAst::Expr::FUNCTION, mapFn } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createMapArrayExpression(GroupNodeCPtr arrayExpr,
        ::std::function<GroupNodes(GroupNodeCPtr)> fn)
    {
      asRootType(arrayExpr);
      expectConcreteConstraint<ConstrainedArrayType>(arrayExpr);

      auto argTy = constraintOf<ConstrainedArrayType>(arrayExpr)->element;
      auto lambda = createLambdaExpression( { argTy }, nullptr, arrayExpr->span(),
          [&](GroupNodes args) {
            return fn(args.at(0));
          });
      return createMapArrayExpression(lambda, arrayExpr);
    }

    GroupNodePtr VAst::createFilterArrayExpression(GroupNodeCPtr filterFn, GroupNodeCPtr arrayExpr)
    {
      asRootType(arrayExpr);
      asRootType(filterFn);

      expect<ConstrainedFunctionType>(filterFn);
      expectConcreteConstraint<ConstrainedArrayType>(arrayExpr);

      auto fnTy = constraintOf<ConstrainedFunctionType>(filterFn);

      if (fnTy->parameters.size() != 1) {
        throw ValidationError(filterFn, "expected one argument");
      }
      if (!fnTy->returnType->isSameConstraint(*ConstrainedPrimitiveType::getBoolean())) {
        throw ValidationError(filterFn, "filter function must return a boolean");
      }
      auto elementTy = fnTy->parameters.at(0);
      auto arrTy = constraintOf<ConstrainedArrayType>(arrayExpr);
      fnTy = ConstrainedFunctionType::get(fnTy->returnType, { arrTy->element });
      filterFn = implicit_cast(fnTy, filterFn);

      auto ty = ConstrainedArrayType::getBoundedArray(arrTy->element, 0, arrTy->bounds.max());

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::FILTER_ARRAY_EXPRESSION, { {
          VAst::Expr::EXPRESSION, arrayExpr }, { VAst::Expr::FUNCTION, filterFn } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createFindArrayExpression(GroupNodeCPtr findFn, GroupNodeCPtr arrayExpr)
    {
      return createFindArrayExpression(findFn, arrayExpr, nullptr);
    }

    GroupNodePtr VAst::createFindArrayExpression(GroupNodeCPtr findFn, GroupNodeCPtr arrayExpr,
        GroupNodeCPtr initialIndexExpr)
    {
      asRootType(arrayExpr);
      asRootType(findFn);

      if (!initialIndexExpr) {
        initialIndexExpr = createLiteralInteger(Integer::ZERO(), arrayExpr->span());
      }

      expect<ConstrainedIntegerType>(initialIndexExpr);
      expect<ConstrainedFunctionType>(findFn);
      expect<ConstrainedArrayType>(arrayExpr);

      auto fnTy = constraintOf<ConstrainedFunctionType>(findFn);

      if (fnTy->parameters.size() != 1) {
        throw ValidationError(findFn, "expected one argument");
      }
      if (!fnTy->returnType->isSameConstraint(*ConstrainedPrimitiveType::getBoolean())) {
        throw ValidationError(findFn, "find function must return a boolean");
      }
      auto arrTy = constraintOf<ConstrainedArrayType>(arrayExpr);
      auto elementTy = arrTy->element;
      if (!arrTy->index) {
        throw ValidationError(findFn,
            "find function cannot be applied to array of type " + arrTy->toString());
      }
      fnTy = ConstrainedFunctionType::get(fnTy->returnType, { elementTy });
      findFn = implicit_cast(fnTy, findFn);
      initialIndexExpr = implicit_cast(arrTy->counter, initialIndexExpr);

      auto ty = ConstrainedOptType::get(arrTy->index);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::FIND_ARRAY_EXPRESSION, { {
          VAst::Expr::EXPRESSION, arrayExpr }, { VAst::Expr::FUNCTION, findFn }, {
          VAst::Expr::INDEX, initialIndexExpr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGuardExpression(GroupNodeCPtr cond, GroupNodeCPtr expr,
        GroupNodeCPtr optErrorMessage)
    {
      expect<ConstrainedBooleanType>(cond);
      if (optErrorMessage) {
        expect<ConstrainedStringType>(optErrorMessage);
      }
      auto ty = constraintOf(expr);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GUARD_EXPRESSION, { { VAst::Expr::EXPRESSION,
          expr }, { VAst::Expr::CONDITION, cond }, { VAst::Expr::MESSAGE, optErrorMessage } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGuardExpression(GroupNodeCPtr cond, GroupNodeCPtr expr,
        const ::std::string &errMessage)
    {
      GroupNodeCPtr message;
      if (!errMessage.empty()) {
        message = createLiteralString(errMessage, cond->span());
      }
      return createGuardExpression(cond, expr, message);
    }

    GroupNodePtr VAst::createMapOptionalExpression(GroupNodeCPtr mapFn, GroupNodeCPtr optExpr)
    {
      asRootType(mapFn);
      asRootType(optExpr);

      expect<ConstrainedFunctionType>(mapFn);
      expectConcreteConstraint<ConstrainedOptType>(optExpr);

      auto fnTy = constraintOf<ConstrainedFunctionType>(mapFn);
      auto optTy = constraintOf<ConstrainedOptType>(optExpr);

      if (fnTy->parameters.size() != 1) {
        throw ValidationError(mapFn, "expected one argument");
      }
      if (fnTy->returnType->isSameConstraint(*ConstrainedPrimitiveType::getVoid())) {
        throw ValidationError(mapFn, "function cannot be void");
      }
      auto elementTy = optTy->element;
      fnTy = ConstrainedFunctionType::get(fnTy->returnType, { elementTy });
      mapFn = implicit_cast(fnTy, mapFn);
      auto ty = optTy->copy(fnTy->returnType);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::MAP_OPTIONAL_EXPRESSION, { {
          VAst::Expr::EXPRESSION, optExpr }, { VAst::Expr::FUNCTION, mapFn } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetArrayLengthExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedArrayType>(expr);

      auto arrTy = constraintOf<ConstrainedArrayType>(expr);
      auto ty = arrTy->length;
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::ARRAY_LENGTH_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetStringLengthExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedStringType>(expr);
      auto strTy = constraintOf<ConstrainedStringType>(expr);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::STRING_LENGTH_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, strTy->length);
      return res;
    }

    GroupNodePtr VAst::createInterpolateTextExpression(GroupNodes exprs)
    {
      GroupNodes parts;
      for (auto expr : exprs) {
        parts.push_back(createStringifyExpression(expr));
      }

      auto ty = ConstrainedPrimitiveType::getString();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::INTERPOLATE_TEXT_EXPRESSION, { {
          VAst::Expr::EXPRESSIONS, parts } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createStringifyExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);

      auto exprTy = constraintOf(expr);
      auto ety = constraintOf(expr);

      auto optTy = ety->self<ConstrainedOptType>();
      if (optTy) {
        ety = optTy->flatten();
      }

      // the following primitive types are supported for now
      auto supported = ety->self<ConstrainedBooleanType>() || ety->self<ConstrainedCharType>()
          || ety->self<ConstrainedIntegerType>() || ety->self<ConstrainedStringType>();

      if (!supported) {
        throw ValidationError(
            "expression of type " + exprTy->toString() + " cannot be stringified");
      }

      auto ty = ConstrainedPrimitiveType::getString();

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::STRINGIFY_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createStringToCharsExpression(GroupNodeCPtr expr)
    {
      expr = implicit_cast(ConstrainedPrimitiveType::getString(), expr);
      auto ty = ConstrainedArrayType::getBoundedArray(ConstrainedCharType::create(),
          Integer::ZERO(), ::std::nullopt);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::STRING_TO_CHARS_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createCharToStringExpression(GroupNodeCPtr expr)
    {
      expr = implicit_cast(ConstrainedPrimitiveType::getChar(), expr);
      auto ty = ConstrainedPrimitiveType::getString();

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::CHAR_TO_STRING_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLambdaExpression(const ConstrainedFunctionType &fnTy,
        const ::idioma::ast::Node::Span &span, ::std::function<GroupNodes(GroupNodes)> bodyFN)
    {
      GroupNodeCPtr retTy = createType(fnTy.returnType, { });
      GroupNodes args;
      GroupNodes fnParams;
      auto fnParamType = ::mylang::variables::Variable::createParameter();
      for (auto p : fnTy.parameters) {
        auto pname = mylang::names::Name::create("arg");
        args.push_back(
            createVariableExpression(TokenNode::create(pname->uniqueLocalName(), span), pname, p));
        fnParams.push_back(
            createDefineVariable(nullptr, pname, fnParamType, createType(p, span), nullptr));
      }

      auto fnBody = createFunctionBody(bodyFN(args));
      return createLambdaExpression(retTy, ::std::move(fnParams), fnBody);
    }

    GroupNodePtr VAst::createLambdaExpression(const ::std::vector<ConstrainedTypePtr> &params,
        ConstrainedTypePtr returnTy, const ::idioma::ast::Node::Span &span,
        ::std::function<GroupNodes(GroupNodes)> bodyFN)
    {
      GroupNodes args;
      GroupNodes fnParams;
      auto fnParamType = ::mylang::variables::Variable::createParameter();
      for (auto p : params) {
        auto pname = mylang::names::Name::create("arg");
        args.push_back(
            createVariableExpression(TokenNode::create(pname->uniqueLocalName(), span), pname, p));
        fnParams.push_back(
            createDefineVariable(nullptr, pname, fnParamType, createType(p, span), nullptr));
      }

      auto fnBody = createFunctionBody(bodyFN(args));
      GroupNodeCPtr retTy;
      if (returnTy) {
        retTy = createType(returnTy, { });
      } else {
        retTy = createType(constraintOf(fnBody), { });
      }
      return createLambdaExpression(retTy, ::std::move(fnParams), fnBody);
    }

    GroupNodePtr VAst::createLambdaExpression(GroupNodeCPtr type, GroupNodes parameters,
        GroupNodeCPtr body)
    {
      ConstrainedTypePtr retTy;
      if (type) {
        expectConcreteConstraint(type);
        retTy = returnTypeOf(body);
      } else {
        expectConcreteConstraint(body);
        retTy = returnTypeOf(body);
        type = createType(retTy, body->span());
      }
      assert(body->hasType(VAst::GroupType::FUNCTION_BODY));
      if (retTy->isSameConstraint(*ConstrainedVoidType::create())) {
        throw ValidationError(body, "lambda must be not be a void type");
      }
      assert(retTy);
      body = implicit_cast_function_body(retTy, body);
      if (throwStatementDispositionOfBody(body) != ThrowStatementDisposition::FULL
          && returnStatementDispositionOfBody(body) != ReturnStatementDisposition::FULL) {
        if (!retTy->isSameConstraint(*ConstrainedVoidType::create())) {
          throw ValidationError(body, "missing return in lambda function");
        }
      }

      ::std::vector<ConstrainedTypePtr> parms;
      for (auto p : parameters) {
        expectConcreteConstraint(p);
        parms.push_back(constraintOf(p));
      }
      auto ty = ConstrainedFunctionType::get(retTy, parms);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LAMBDA_EXPRESSION, { {
          VAst::Relation::PARAMETERS, parameters }, { VAst::Relation::TYPE, type }, {
          VAst::Relation::BODY, body } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createNewProcessExpression(GroupNodeCPtr type, NamePtr processName,
        GroupNodes argv)
    {
      expect<ConstrainedProcessType>(type);
      ensureNoNamedExpressions(argv);
      auto ty = constraintOf(type);
      if (!processName) {
        throw ::std::invalid_argument("missing process name");
      }
      for (auto arg : argv) {
        // TODO: once we have a process type that knows the signature of the constructors
        // we should check it here.
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::NEW_PROCESS_EXPRESSION, { {
          VAst::Relation::TYPE, type }, { VAst::Expr::ARGUMENTS, argv } });
      setNameAnnotation(res, processName);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createNewBitExpression(GroupNodeCPtr type, GroupNodes argv)
    {
      expect<ConstrainedBitType>(type);
      expectConcreteConstraint(type);

      ensureNoNamedExpressions(argv);
      auto copy = createCopyConstructor(type, argv);
      if (copy) {
        return copy;
      }

      auto ty = constraintOf<ConstrainedBitType>(type);
      if (argv.size() != 1) {
        throw ValidationError(type, "no such bit constructor: unexpected number of arguments");
      }
      auto argTy = constraintOf(argv.at(0));
      GroupNodes args;
      if (args.empty()) {
        auto tmp = ConstrainedIntegerType::create(createRange(0, 1));
        if (tmp->canSafeCastFrom(*argTy)) {
          args.push_back(implicit_cast(tmp, argv.at(0)));
        }
      }

      if (args.empty()) {
        throw ValidationError(type, "no such bit constructor");
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::NEW_BIT_EXPRESSION, { { VAst::Relation::TYPE,
          type }, { VAst::Expr::ARGUMENTS, args } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createNewBooleanExpression(GroupNodeCPtr type, GroupNodes argv)
    {
      expect<ConstrainedBooleanType>(type);
      expectConcreteConstraint(type);
      ensureNoNamedExpressions(argv);

      auto copy = createCopyConstructor(type, argv);
      if (copy) {
        return copy;
      }

      auto ty = constraintOf<ConstrainedBooleanType>(type);

      // TODO: check argument for the specified type
      if (!true) {
        throw ValidationError(type, "no such boolean constructor");
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::NEW_BOOLEAN_EXPRESSION, { {
          VAst::Relation::TYPE, type }, { VAst::Expr::ARGUMENTS, argv } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createNewByteExpression(GroupNodeCPtr type, GroupNodes argv)
    {
      expect<ConstrainedByteType>(type);
      expectConcreteConstraint(type);

      ensureNoNamedExpressions(argv);
      auto copy = createCopyConstructor(type, argv);
      if (copy) {
        return copy;
      }

      auto ty = constraintOf<ConstrainedByteType>(type);
      if (argv.size() != 1) {
        throw ValidationError(type, "no such byte constructor: unexpected number of arguments");
      }
      auto argTy = constraintOf(argv.at(0));
      GroupNodes args;
      if (args.empty()) {
        auto tmp = ConstrainedByteType::getPeerBitArray();
        if (tmp->canImplicitlyCastFrom(*argTy)) {
          args.push_back(implicit_cast(tmp, argv.at(0)));
        }
      }
      if (args.empty()) {
        auto tmp = ConstrainedIntegerType::create(createRange(0, 255));
        if (tmp->canSafeCastFrom(*argTy)) {
          args.push_back(implicit_cast(tmp, argv.at(0)));
        }
      }
      if (args.empty()) {
        auto tmp = ConstrainedIntegerType::create(createRange(-128, 127));
        if (tmp->canSafeCastFrom(*argTy)) {
          args.push_back(implicit_cast(tmp, argv.at(0)));
        }
      }
      if (args.empty()) {
        throw ValidationError(type, "no such byte constructor");
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::NEW_BYTE_EXPRESSION, { { VAst::Relation::TYPE,
          type }, { VAst::Expr::ARGUMENTS, args } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createNewCharExpression(GroupNodeCPtr type, GroupNodes argv)
    {
      expect<ConstrainedCharType>(type);
      expectConcreteConstraint(type);

      ensureNoNamedExpressions(argv);
      auto copy = createCopyConstructor(type, argv);
      if (copy) {
        return copy;
      }

      auto ty = constraintOf<ConstrainedCharType>(type);

      if (true) {
        throw ValidationError(type, "no such char constructor");
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::NEW_CHAR_EXPRESSION, { { VAst::Relation::TYPE,
          type }, { VAst::Expr::ARGUMENTS, argv } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createNewIntegerExpression(GroupNodeCPtr type, GroupNodes argv)
    {
      expect<ConstrainedIntegerType>(type);
      expectConcreteConstraint(type);

      ensureNoNamedExpressions(argv);
      auto copy = createCopyConstructor(type, argv);
      if (copy) {
        return copy;
      }

      auto ty = constraintOf<ConstrainedIntegerType>(type);

      if (true) {
        throw ValidationError(type, "no such integer constructor");
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::NEW_INTEGER_EXPRESSION, { {
          VAst::Relation::TYPE, type }, { VAst::Expr::ARGUMENTS, argv } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createNewRealExpression(GroupNodeCPtr type, GroupNodes argv)
    {
      expect<ConstrainedRealType>(type);
      expectConcreteConstraint(type);

      ensureNoNamedExpressions(argv);
      auto copy = createCopyConstructor(type, argv);
      if (copy) {
        return copy;
      }

      auto ty = constraintOf<ConstrainedRealType>(type);
      if (argv.size() != 1) {
        throw ValidationError(type, "no such string constructor: unexpected number of arguments");
      }
      GroupNodes args;
      if (ConstrainedIntegerType::create()->canImplicitlyCastFrom(*constraintOf(argv.at(0)))) {
        args.push_back(implicit_cast(ConstrainedIntegerType::create(), argv.at(0)));
      } else {
        throw ValidationError(type, "no such real constructor");
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::NEW_REAL_EXPRESSION, { { VAst::Relation::TYPE,
          type }, { VAst::Expr::ARGUMENTS, args } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createNewStringExpression(GroupNodeCPtr type, GroupNodes argv)
    {
      expect<ConstrainedStringType>(type);
      expectConcreteConstraint(type);

      ensureNoNamedExpressions(argv);
      auto copy = createCopyConstructor(type, argv);
      if (copy) {
        return copy;
      }

      auto ty = constraintOf<ConstrainedStringType>(type);

      if (argv.size() != 1) {
        throw ValidationError(type, "no such string constructor: unexpected number of arguments");
      }
      GroupNodes args;
      if (ConstrainedCharType::create()->canImplicitlyCastFrom(*constraintOf(argv.at(0)))) {
        args.push_back(implicit_cast(ConstrainedCharType::create(), argv.at(0)));
      } else {
        throw ValidationError(type, "no such string constructor");
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::NEW_STRING_EXPRESSION, { { VAst::Relation::TYPE,
          type }, { VAst::Expr::ARGUMENTS, args } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createNewArrayExpression(GroupNodeCPtr type, GroupNodes argv)
    {
      expect<ConstrainedArrayType>(type);
      expectConcreteConstraint(type);

      ensureNoNamedExpressions(argv);
      auto copy = createCopyConstructor(type, argv);
      if (copy) {
        return copy;
      }

      auto ty = constraintOf<ConstrainedArrayType>(type);
      if (!ty->bounds.contains(argv.size())) {
        throw ValidationError(type, "no such array constructor: unexpected number of elements");
      }
      GroupNodes args;
      for (size_t i = 0; i < argv.size(); ++i) {
        args.push_back(implicit_cast(ty->element, argv.at(i)));
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::NEW_ARRAY_EXPRESSION, { { VAst::Relation::TYPE,
          type }, { VAst::Expr::ARGUMENTS, args } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createNewFunctionExpression(GroupNodeCPtr type, GroupNodes argv)
    {
      expect<ConstrainedFunctionType>(type);
      expectConcreteConstraint(type);

      ensureNoNamedExpressions(argv);
      auto copy = createCopyConstructor(type, argv);
      if (copy) {
        return copy;
      }

      // there is no way to create a function in another way
      throw ValidationError(type, "no such function constructor");
    }

    GroupNodePtr VAst::createNewOptionalExpression(GroupNodeCPtr type, GroupNodes argv)
    {
      expectConcreteConstraint<ConstrainedOptType>(type);

      ensureNoNamedExpressions(argv);

      auto copy = createCopyConstructor(type, argv);
      if (copy) {
        return copy;
      }

      auto ty = constraintOf<ConstrainedOptType>(type);

      GroupNodes args;
      if (argv.size() > 1) {
        throw ValidationError(type, "no such optional constructor: too many arguments");
      }

      if (argv.size() == 1) {
        args.push_back(implicit_cast(ty->element, argv.at(0)));
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::NEW_OPTIONAL_EXPRESSION, { {
          VAst::Relation::TYPE, type }, { VAst::Expr::ARGUMENTS, args } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createNewMutableExpression(GroupNodeCPtr type, GroupNodes argv)
    {
      expect<ConstrainedMutableType>(type);
      expectConcreteConstraint(type);

      ensureNoNamedExpressions(argv);

      auto copy = createCopyConstructor(type, argv);
      if (copy) {
        return copy;
      }

      auto ty = constraintOf<ConstrainedMutableType>(type);

      GroupNodes args;
      if (argv.size() != 1) {
        if (argv.size() == 0) {
          throw ValidationError(type, "no such mutable constructor: too few arguments");
        } else {
          throw ValidationError(type, "no such mutable constructor: too many arguments");
        }
      }

      args.push_back(implicit_cast(ty->element, argv.at(0)));

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::NEW_MUTABLE_EXPRESSION, { {
          VAst::Relation::TYPE, type }, { VAst::Expr::ARGUMENTS, args } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createStructFromTupleExpression(GroupNodeCPtr type, GroupNodeCPtr tuple)
    {
      asRootType(tuple);
      expect<ConstrainedStructType>(type);
      expect<ConstrainedTupleType>(tuple);
      expectConcreteConstraint(type);
      auto ty = constraintOf<ConstrainedStructType>(type);
      auto expr = implicit_cast(ty->getPeerTuple(), tuple);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::STRUCT_FROM_TUPLE_EXPRESSION, { {
          VAst::Relation::TYPE, type }, { VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createNewStructExpression(GroupNodeCPtr type, GroupNodes argv)
    {
      expect<ConstrainedStructType>(type);
      expectConcreteConstraint(type);

      auto ty = constraintOf<ConstrainedStructType>(type);

      auto copy = createCopyConstructor(type, argv);
      if (copy) {
        return copy;
      }

      if (argv.size() == 1) {
        auto argTy = constraintOf(argv.at(0));
        if (ty->getPeerTuple()->canImplicitlyCastFrom(*argTy)) {
          return createStructFromTupleExpression(type, argv[0]);
        }
      }

      // FIXME: support these eventually
      bool namedExprs = ensureNoMixedExpressions(argv);
      GroupNodes args;

      if (argv.size() > ty->members.size()) {
        throw ValidationError(type, "no such struct constructor: too many arguments");
      }

      if (namedExprs) {
        ::std::map<::std::string, GroupNodeCPtr> namedArgs = getNamedExpressions(argv);
        for (size_t i = 0; i < ty->members.size(); ++i) {
          auto arg = namedArgs.find(ty->members.at(i).name);
          if (arg == namedArgs.end()) {
            // if argument  is optional or an allows empty arrays, then use those as the default
            args.push_back(createDefaultLiteral(ty->members.at(i).constraint, type->span()));
          } else {
            args.push_back(implicit_cast(ty->members.at(i).constraint, arg->second));
            namedArgs.erase(arg);
          }
        }
        if (!namedArgs.empty()) {
          throw ValidationError(namedArgs.begin()->second,
              "unknown member " + namedArgs.begin()->first);
        }
      } else {
        for (size_t i = 0; i < argv.size(); ++i) {
          args.push_back(implicit_cast(ty->members.at(i).constraint, argv.at(i)));
        }
      }

      // fill in missing args with defaults
      for (size_t i = args.size(); i < ty->members.size(); ++i) {
        args.push_back(createDefaultLiteral(ty->members.at(i).constraint, type->span()));
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::NEW_STRUCT_EXPRESSION, { { VAst::Relation::TYPE,
          type }, { VAst::Expr::ARGUMENTS, args } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createNewTupleExpression(GroupNodeCPtr type, GroupNodes argv)
    {
      expect<ConstrainedTupleType>(type);
      expectConcreteConstraint(type);
      ensureNoNamedExpressions(argv);

      auto copy = createCopyConstructor(type, argv);
      if (copy) {
        return copy;
      }

      auto ty = constraintOf<ConstrainedTupleType>(type);
      if (argv.size() != ty->tuple.size()) {
        throw ValidationError(type, "no such tuple constructor: unexpected number of arguments");
      }

      GroupNodes args;
      for (size_t i = 0; i < argv.size(); ++i) {
        args.push_back(implicit_cast(ty->types.at(i), argv.at(i)));
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::NEW_TUPLE_EXPRESSION, { { VAst::Relation::TYPE,
          type }, { VAst::Expr::ARGUMENTS, args } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createNewUnionExpression(GroupNodeCPtr type, GroupNodeCPtr discriminant,
        GroupNodeCPtr value)
    {
      expect<ConstrainedUnionType>(type);
      expectConcreteConstraint(type);

      auto ty = constraintOf(type)->self<ConstrainedUnionType>();
      ConstrainedTypePtr memberTy;
      if (discriminant) {
        ensureNoNamedExpressions( { value });
        auto literal = getConstAnnotation(discriminant);
        if (!literal) {
          throw ValidationError(discriminant, "discriminant must be a constant");
        }
        if (!ty->discriminant.constraint->canImplicitlyCastFrom(*literal->constraint())) {
          throw ValidationError(discriminant, "discriminant values not compatible with union");
        }
        memberTy = ty->getMemberType(*literal);
        if (!memberTy) {
          throw ValidationError(discriminant, "discriminant does not identify a field");
        }
      } else if (value->hasType(GroupType::NAMED_EXPRESSION)) {
        auto name = idioma::astutils::getToken(value, VAst::Token::IDENTIFIER);
        auto expr = idioma::astutils::getGroup(value, VAst::Expr::EXPRESSION);
        auto index = ty->getMemberIndex(name->text);
        if (!index.has_value()) {
          throw ValidationError(name, "no such member " + name->text);
        }
        value = expr;
        discriminant = createLiteral(ty->members.at(*index).discriminant, value->span());
        memberTy = ty->members.at(*index).constraint;
      } else {
        auto valueTy = getConstraintAnnotation(value);
        for (auto t : ty->members) {
          if (t.constraint->canImplicitlyCastFrom(*valueTy)) {
            if (discriminant) {
              throw ValidationError(value, "ambiguous match with union member " + t.name);
            }
            discriminant = createLiteral(t.discriminant, value->span());
            memberTy = t.constraint;
          }
        }
        if (!discriminant) {
          throw ValidationError(value,
              "Union has no member that is compatible with " + valueTy->toString());
        }
      }

      value = implicit_cast(memberTy, value);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::NEW_UNION_EXPRESSION,
          { { VAst::Relation::TYPE, type }, { VAst::Relation::DISCRIMINANT, discriminant }, {
              VAst::Expr::EXPRESSION, value } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createNewUnionExpression(GroupNodeCPtr type, GroupNodes argv)
    {
      expect<ConstrainedUnionType>(type);
      expectConcreteConstraint(type);

      auto copy = createCopyConstructor(type, argv);
      if (copy) {
        return copy;
      }
      GroupNodeCPtr discriminant, value;

      if (argv.size() == 2) {
        ensureNoNamedExpressions(argv);
        discriminant = argv.at(0);
        value = argv.at(1);
      } else if (argv.size() == 1) {
        value = argv.at(0);
      } else {
        throw ValidationError(type,
            "no such union constructor: unexpected number of arguments "
                + ::std::to_string(argv.size()));
      }
      return createNewUnionExpression(type, discriminant, value);
    }

    GroupNodePtr VAst::createNewNamedTypeExpression(GroupNodeCPtr type, GroupNodes argv)
    {
      expect<ConstrainedNamedType>(type);
      expectConcreteConstraint(type);

      auto copy = createCopyConstructor(type, argv);
      if (copy) {
        return copy;
      }

      auto ty = constraintOf<ConstrainedNamedType>(type);
      auto bt = ty->basetype();

      GroupNodes args;
      if (argv.size() == 1 && !argv.at(0)->hasType(GroupType::NAMED_EXPRESSION)) {
        auto argTy = constraintOf(argv.at(0));
        if (bt->canImplicitlyCastFrom(*argTy)) {
          args.push_back(implicit_cast(bt, argv.at(0)));
        }
      }
      if (args.empty()) {
        auto ctor = createNewObject(createType(bt, type->span()), argv);
        args.push_back(ctor);
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::NEW_NAMEDTYPE_EXPRESSION, { {
          VAst::Relation::TYPE, type }, { VAst::Expr::ARGUMENTS, args } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createProcessMemberExpression(GroupNodeCPtr expr,
        const ::std::string &member)
    {
      auto token = TokenNode::create(member, expr->span());
      return createProcessMemberExpression(expr, token);
    }

    GroupNodePtr VAst::createProcessMemberExpression(GroupNodeCPtr expr, TokenNodeCPtr member)
    {
      asRootType(expr);
      expect<ConstrainedProcessType>(expr);
      auto exprTy = constraintOf<ConstrainedProcessType>(expr);
      ConstrainedTypePtr ty;

      if (!ty) {
        auto i = exprTy->inputs.find(member->text);
        if (i != exprTy->inputs.end()) {
          ty = ConstrainedOutputType::get(i->second->element);
        }
      }

      if (!ty) {
        auto i = exprTy->outputs.find(member->text);
        if (i != exprTy->outputs.end()) {
          ty = i->second;
          ty = ConstrainedInputType::get(i->second->element);
        }
      }

      if (!ty) {
        throw ValidationError(expr,
            "type " + exprTy->toString() + " has no such port " + member->text);
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::PROCESS_MEMBER_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr }, { VAst::Token::IDENTIFIER, member } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createUnionMemberExpression(GroupNodeCPtr expr, TokenNodeCPtr member)
    {
      asRootType(expr);
      expect<ConstrainedUnionType>(expr);
      auto exprTy = constraintOf<ConstrainedUnionType>(expr);
      ConstrainedTypePtr ty;

      if (exprTy->discriminant.name == member->text) {
        ty = exprTy->discriminant.constraint;
      } else {
        ty = exprTy->getMemberType(member->text);
        if (ty) {
          if (ConstrainedVoidType::create()->isSameConstraint(*ty)) {
            throw ValidationError(expr,
                "Cannot dereference member '" + member->text + "', because its type is void");
          }
          ty = ConstrainedOptType::get(ty);
        }
      }
      if (!ty) {
        throw ValidationError(expr,
            "type " + exprTy->toString() + " has no member or discriminant : " + member->text);
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::UNION_MEMBER_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr }, { VAst::Token::IDENTIFIER, member } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createUnionMemberExpression(GroupNodeCPtr expr, const ::std::string &member)
    {
      auto token = TokenNode::create(member, expr->span());
      return createUnionMemberExpression(expr, token);
    }

    GroupNodePtr VAst::createStructMemberExpression(GroupNodeCPtr expr, TokenNodeCPtr member)
    {
      asRootType(expr);
      expect<ConstrainedStructType>(expr);
      auto exprTy = constraintOf<ConstrainedStructType>(expr);
      auto ty = exprTy->getMemberType(member->text);
      if (!ty) {
        throw ValidationError(expr,
            "type " + exprTy->toString() + " has no member " + member->text);
      }
      if (ConstrainedVoidType::create()->isSameConstraint(*ty)) {
        throw ValidationError(expr,
            "Cannot dereference member '" + member->text + "', because its type is void");
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::STRUCT_MEMBER_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr }, { VAst::Token::IDENTIFIER, member } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createStructMemberExpression(GroupNodeCPtr expr, const ::std::string &member)
    {
      auto token = TokenNode::create(member, expr->span());
      return createStructMemberExpression(expr, token);
    }

    GroupNodePtr VAst::createStringIndexExpression(GroupNodeCPtr expr, GroupNodeCPtr index)
    {
      asRootType(expr);
      expect<ConstrainedStringType>(expr);

      auto s_ty = constraintOf<ConstrainedStringType>(expr);
      index = implicit_cast(s_ty->index, index);

      auto ty = ConstrainedPrimitiveType::getChar();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::INDEX_STRING_EXPRESSION, { { VAst::Expr::LHS,
          expr }, { VAst::Expr::RHS, index } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createArrayIndexExpression(GroupNodeCPtr expr, GroupNodeCPtr index)
    {
      asRootType(expr);
      expectConcreteConstraint<ConstrainedArrayType>(expr);
      auto aty = constraintOf<ConstrainedArrayType>(expr);

      if (!aty->index) {
        throw ValidationError(index, "cannot index into an array of type " + aty->toString());
      }

      index = implicit_cast(aty->index, index);
      auto ty = aty->element;
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::INDEX_ARRAY_EXPRESSION, { { VAst::Expr::LHS,
          expr }, { VAst::Expr::RHS, index } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createStringSubrangeExpression(GroupNodeCPtr expr, GroupNodeCPtr begin,
        GroupNodeCPtr end)
    {
      asRootType(expr);
      expect<ConstrainedStringType>(expr);

      auto s_ty = constraintOf<ConstrainedStringType>(expr);
      auto ty = s_ty;

      if (!begin && !end) {
        throw ValidationError(expr, "missing subrange bounds");
      }
      if (!begin) {
        begin = createLiteralInteger(0, expr->span());
      }
      if (!end) {
        end = createGetStringLengthExpression(expr);
      }
      begin = implicit_cast(ty->length, begin);
      end = implicit_cast(ty->length, end);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::SUBRANGE_STRING_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr }, { VAst::Expr::BEGIN, begin }, { VAst::Expr::END, end } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createArraySubrangeExpression(GroupNodeCPtr expr, GroupNodeCPtr begin,
        GroupNodeCPtr end)
    {
      asRootType(expr);
      expectConcreteConstraint<ConstrainedArrayType>(expr);
      auto aty = constraintOf<ConstrainedArrayType>(expr);

      if (!begin && !end) {
        throw ValidationError(expr, "missing subrange bounds");
      }
      if (!begin) {
        begin = createLiteralInteger(0, expr->span());
      }
      if (!end) {
        end = createGetArrayLengthExpression(expr);
      }

      // create the new output array type first, because we need to define the length
      ::std::shared_ptr<const ConstrainedArrayType> ty;
      if (aty->isUnbounded()) {
        ty = ConstrainedArrayType::getUnboundedArray(aty->element);
      } else {
        ty = ConstrainedArrayType::getBoundedArray(aty->element,
            ConstrainedArrayType::Range(Integer::ZERO(), aty->bounds.max()));
      }

      begin = implicit_cast(ty->length, begin);
      end = implicit_cast(ty->length, end);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::SUBRANGE_ARRAY_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr }, { VAst::Expr::BEGIN, begin }, { VAst::Expr::END, end } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createTupleMemberExpression(GroupNodeCPtr expr, GroupNodeCPtr index)
    {
      asRootType(expr);
      expect<ConstrainedTupleType>(expr);
      expect<ConstrainedIntegerType>(index);

      auto tty = constraintOf<ConstrainedTupleType>(expr);
      auto constant = getConstAnnotation(index);
      if (!constant) {
        throw ValidationError(index, "expected an integer constant");
      }
      BigInt indexValue = constant->getSignedInteger();

      if (indexValue < 0) {
        throw ValidationError(index, "tuple index may not be negative");
      }
      if (indexValue >= BigInt::unsignedValueOf(tty->types.size())) {
        throw ValidationError(index, "no such tuple index : " + indexValue.toString());
      }
      auto ty = tty->types.at(indexValue.unsignedValue());
      if (ConstrainedVoidType::create()->isSameConstraint(*ty)) {
        throw ValidationError(expr,
            "Cannot dereference member " + indexValue.toString() + ", because its type is void");
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::TUPLE_MEMBER_EXPRESSION, { { VAst::Expr::LHS,
          expr }, { VAst::Expr::RHS, index } });
      annotateConstraint(res, ty);

      return res;
    }

    GroupNodePtr VAst::createTupleMemberExpression(GroupNodeCPtr expr, size_t member)
    {
      auto index = createLiteralInteger(member, expr->span());
      return createTupleMemberExpression(expr, index);
    }

    GroupNodePtr VAst::createConvertToTupleExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedStructType>(expr);
      auto ty = constraintOf<ConstrainedStructType>(expr)->getPeerTuple();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::CONVERT_TO_TUPLE_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);

      return res;
    }

    GroupNodePtr VAst::createGetHeadExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      auto exprTy = constraintOf(expr);
      auto tupleTy = exprTy->self<ConstrainedTupleType>();
      ConstrainedTypePtr ty;
      if (tupleTy) {
        if (tupleTy->types.empty()) {
          throw ValidationError(expr, "cannot apply head expression to an empty tuple");
        }
        ty = tupleTy->types.at(0);
      } else {
        auto arrTy = exprTy->self<ConstrainedArrayType>();
        if (!arrTy || arrTy->bounds.max() == 0) {
          throw ValidationError(expr, "cannot apply head to type " + exprTy->toString());
        }

        // FIXME: why would arrTy not have an element??
        ty = arrTy->element;
        if (!ty) {
          throw ValidationError(expr, "cannot apply head to type");
        }
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_HEAD_EXPRESSION, { { VAst::Expr::EXPRESSION,
          expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetTailExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      auto exprTy = constraintOf(expr);
      ConstrainedTypePtr ty;
      auto tupleTy = exprTy->self<ConstrainedTupleType>();
      if (tupleTy) {
        if (tupleTy->types.empty()) {
          throw ValidationError(expr, "cannot apply tail expression to an empty tuple");
        }
        ::std::vector<ConstrainedTypePtr> types;
        types.insert(types.end(), tupleTy->types.begin() + 1, tupleTy->types.end());
        ty = ConstrainedTupleType::get(types);
      } else {
        auto arrTy = exprTy->self<ConstrainedArrayType>();
        if (!arrTy || arrTy->bounds.max() == 0) {
          throw ValidationError(expr, "cannot apply tail to type " + exprTy->toString());
        }
        auto min = arrTy->bounds.min().towards0();
        auto max = arrTy->bounds.max().decrement();
        ty = ConstrainedArrayType::getBoundedArray(arrTy->element,
            ConstrainedArrayType::Range(min, max));
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_TAIL_EXPRESSION, { { VAst::Expr::EXPRESSION,
          expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetMutableValueExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedMutableType>(expr);
      expectConcreteConstraint(expr);
      auto ty = constraintOf<ConstrainedMutableType>(expr)->element;

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_MUTABLE_VALUE_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetOptionalValueExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expectConcreteConstraint<ConstrainedOptType>(expr);
      auto ty = constraintOf<ConstrainedOptType>(expr)->element;

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_OPTIONAL_VALUE_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createOptifyExpression(GroupNodeCPtr expr)
    {
      expectConcreteConstraint(expr);
      auto ty = ConstrainedOptType::get(constraintOf(expr));

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::OPTIFY_EXPRESSION, { { VAst::Expr::EXPRESSION,
          expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetOptionalValueOrElseExpression(GroupNodeCPtr expr,
        GroupNodeCPtr orElse)
    {
      asRootType(expr);
      expectConcreteConstraint<ConstrainedOptType>(expr);
      ensureConstrainedType(orElse);

      auto optTy = constraintOf<ConstrainedOptType>(expr);
      auto ty = optTy->element->unionWith(constraintOf(orElse));
      if (!ty) {
        throw ValidationError(orElse, "incompatible types");
      }

      expr = implicit_cast(optTy->copy(ty), expr);
      orElse = implicit_cast(ty, orElse);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::ORELSE_EXPRESSION, { { VAst::Expr::LHS, expr },
          { VAst::Expr::RHS, orElse } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetBitsFromBitExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedBitType>(expr);
      auto bitTy = ConstrainedBitType::create();
      auto ty = ConstrainedArrayType::getFixedArray(bitTy, 1);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_BITS_FROM_BIT_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetBitFromBitsExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      auto bitTy = ConstrainedBitType::create();
      auto arrTy = ConstrainedArrayType::getFixedArray(bitTy, 1);
      expr = implicit_cast(arrTy, expr);

      auto ty = ConstrainedBitType::create();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_BIT_FROM_BITS_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetBitsFromBooleanExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedBooleanType>(expr);
      auto bitTy = ConstrainedBitType::create();
      auto ty = ConstrainedArrayType::getFixedArray(bitTy, 1);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_BITS_FROM_BOOLEAN_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetBooleanFromBitsExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      auto bitTy = ConstrainedBitType::create();
      auto arrTy = ConstrainedArrayType::getFixedArray(bitTy, 1);
      expr = implicit_cast(arrTy, expr);

      auto ty = ConstrainedBooleanType::create();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_BOOLEAN_FROM_BITS_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetBitsFromByteExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedByteType>(expr);
      auto bitTy = ConstrainedBitType::create();
      auto ty = ConstrainedArrayType::getFixedArray(bitTy, 8);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_BITS_FROM_BYTE_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetByteFromBitsExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      auto bitTy = ConstrainedBitType::create();
      auto arrTy = ConstrainedArrayType::getFixedArray(bitTy, 8);
      expr = implicit_cast(arrTy, expr);

      auto ty = ConstrainedByteType::create();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_BYTE_FROM_BITS_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetBitsFromCharExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedCharType>(expr);
      auto bitTy = ConstrainedBitType::create();
      auto ty = ConstrainedArrayType::getBoundedArray(bitTy, 8, 6 * 8);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_BITS_FROM_CHAR_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetCharFromBitsExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      auto bitTy = ConstrainedBitType::create();
      auto arrTy = ConstrainedArrayType::getBoundedArray(bitTy, 8, 8 * 6);
      expr = implicit_cast(arrTy, expr);

      auto ty = ConstrainedCharType::create();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_CHAR_FROM_BITS_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetBitsFromIntegerExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedIntegerType>(expr);
      auto ety = constraintOf<ConstrainedIntegerType>(expr);
      auto bitTy = ConstrainedBitType::create();

      auto max = Integer::PLUS_INFINITY();
      size_t min = 1;
      if (ety->range.isFinite()) {
        // FIXME: how many bits are needed to represent a value?
        // for now, just leave unbounded
      }
      // FIXME: figure out the maximum bounds based on the integer
      auto ty = ConstrainedArrayType::getBoundedArray(bitTy, min, max);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_BITS_FROM_INTEGER_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetIntegerFromBitsExpression(GroupNodeCPtr type, GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedIntegerType>(type);

      auto intTy = constraintOf<ConstrainedIntegerType>(type);
      auto bitTy = ConstrainedBitType::create();
      auto max = Integer::PLUS_INFINITY();
      size_t min = 1;
      if (intTy->range.isFinite()) {
        // FIXME: how many bits are needed to represent a value?
        // for now, just leave unbounded
      }
      auto arrTy = ConstrainedArrayType::getBoundedArray(bitTy, min, max);
      expr = implicit_cast(arrTy, expr);

      auto ty = intTy;
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_INTEGER_FROM_BITS_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetBitsFromRealExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedRealType>(expr);
      auto bitTy = ConstrainedBitType::create();
      auto ty = ConstrainedArrayType::getBoundedArray(bitTy, 1, ::std::nullopt);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_BITS_FROM_REAL_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetRealFromBitsExpression(GroupNodeCPtr type, GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedRealType>(type);

      auto realTy = constraintOf<ConstrainedRealType>(type);
      auto bitTy = ConstrainedBitType::create();
      auto arrTy = ConstrainedArrayType::getBoundedArray(bitTy, 1, ::std::nullopt);
      expr = implicit_cast(arrTy, expr);

      auto ty = realTy;
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_REAL_FROM_BITS_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetByteAsSignedIntegerExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedByteType>(expr);
      ConstrainedIntegerType::Range range(-128, 127);
      auto ty = ConstrainedIntegerType::create(range);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_BYTE_AS_SIGNED_INTEGER_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetByteAsUnsignedIntegerExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedByteType>(expr);
      ConstrainedIntegerType::Range range(0, 255);
      auto ty = ConstrainedIntegerType::create(range);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_BYTE_AS_UNSIGNED_INTEGER_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createEncodeBitExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expr = implicit_cast(ConstrainedPrimitiveType::getBit(), expr);
      auto ty = ConstrainedArrayType::getFixedArray(ConstrainedByteType::create(), 1);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::BYTE_ENCODE_BIT_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createEncodeBooleanExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expr = implicit_cast(ConstrainedPrimitiveType::getBoolean(), expr);
      auto ty = ConstrainedArrayType::getFixedArray(ConstrainedByteType::create(), 1);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::BYTE_ENCODE_BOOLEAN_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createEncodeByteExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expr = implicit_cast(ConstrainedPrimitiveType::getByte(), expr);
      auto ty = ConstrainedArrayType::getFixedArray(ConstrainedByteType::create(), 1);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::BYTE_ENCODE_BYTE_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createEncodeCharExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expr = implicit_cast(ConstrainedPrimitiveType::getChar(), expr);
      auto ty = ConstrainedArrayType::getBoundedArray(ConstrainedByteType::create(), 1, 6); // UTF8 encoded
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::BYTE_ENCODE_CHAR_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createEncodeIntegerExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expr = implicit_cast(ConstrainedPrimitiveType::getInteger(), expr);
      auto maxlen = ::std::nullopt;
      auto ty = ConstrainedArrayType::getBoundedArray(ConstrainedByteType::create(), 1, maxlen);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::BYTE_ENCODE_INTEGER_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createEncodeRealExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expr = implicit_cast(ConstrainedPrimitiveType::getReal(), expr);
      auto ty = ConstrainedArrayType::getBoundedArray(ConstrainedByteType::create(), 1,
          ::std::nullopt);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::BYTE_ENCODE_REAL_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createEncodeStringExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expr = implicit_cast(ConstrainedPrimitiveType::getString(), expr);
      auto ty = ConstrainedArrayType::getBoundedArray(ConstrainedByteType::create(), BigInt(0),
          ::std::nullopt);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::BYTE_ENCODE_STRING_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createDecodeBitExpression(GroupNodeCPtr expr)
    {
      auto ety = ConstrainedArrayType::getBoundedArray(ConstrainedByteType::create(), BigInt(1),
          ::std::nullopt);
      expr = implicit_cast(ety, expr);
      auto ty = ConstrainedPrimitiveType::getBit();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::BYTE_DECODE_BIT_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createDecodeBooleanExpression(GroupNodeCPtr expr)
    {
      auto ety = ConstrainedArrayType::getBoundedArray(ConstrainedByteType::create(), 1,
          ::std::nullopt);
      expr = implicit_cast(ety, expr);
      auto ty = ConstrainedPrimitiveType::getBoolean();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::BYTE_DECODE_BOOLEAN_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createDecodeByteExpression(GroupNodeCPtr expr)
    {
      auto ety = ConstrainedArrayType::getBoundedArray(ConstrainedByteType::create(), 1,
          ::std::nullopt);
      expr = implicit_cast(ety, expr);
      auto ty = ConstrainedPrimitiveType::getByte();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::BYTE_DECODE_BYTE_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createDecodeCharExpression(GroupNodeCPtr expr)
    {
      auto ety = ConstrainedArrayType::getBoundedArray(ConstrainedByteType::create(), 1,
          ::std::nullopt);
      expr = implicit_cast(ety, expr);
      auto ty = ConstrainedPrimitiveType::getChar();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::BYTE_DECODE_CHAR_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createDecodeIntegerExpression(GroupNodeCPtr expr)
    {
      auto ety = ConstrainedArrayType::getBoundedArray(ConstrainedByteType::create(), 1,
          ::std::nullopt);
      expr = implicit_cast(ety, expr);
      auto ty = ConstrainedPrimitiveType::getInteger();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::BYTE_DECODE_INTEGER_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createDecodeRealExpression(GroupNodeCPtr expr)
    {
      auto ety = ConstrainedArrayType::getBoundedArray(ConstrainedByteType::create(), 1,
          ::std::nullopt);
      expr = implicit_cast(ety, expr);
      auto ty = ConstrainedPrimitiveType::getReal();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::BYTE_DECODE_REAL_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createDecodeStringExpression(GroupNodeCPtr expr)
    {
      auto ety = ConstrainedArrayType::getBoundedArray(ConstrainedByteType::create(), 1,
          ::std::nullopt);
      expr = implicit_cast(ety, expr);
      auto ty = ConstrainedPrimitiveType::getString();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::BYTE_DECODE_STRING_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createBlockReadEventsExpression(GroupNodeCPtr port, GroupNodeCPtr enabled)
    {
      expect<ConstrainedInputType>(port);
      expect<ConstrainedBooleanType>(enabled);
      auto ty = ConstrainedVoidType::create();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(GroupType::BLOCK_READ_EVENTS_EXPRESSION, { { Expr::PORT, port },
          { Expr::EXPRESSION, enabled } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createBlockWriteEventsExpression(GroupNodeCPtr port, GroupNodeCPtr enabled)
    {
      expect<ConstrainedOutputType>(port);
      expect<ConstrainedBooleanType>(enabled);
      auto ty = ConstrainedVoidType::create();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(GroupType::BLOCK_WRITE_EVENTS_EXPRESSION, { { Expr::PORT, port },
          { Expr::EXPRESSION, enabled } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createWaitPortExpression(GroupNodeCPtr portExpr)
    {
      auto portTy = constraintOf(portExpr);
      if (nullptr == portTy->self<ConstrainedInputType>()
          && nullptr == portTy->self<ConstrainedOutputType>()) {
        throw ValidationError(portExpr, "input or output port expected");
      }

      auto ty = ConstrainedVoidType::create();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::WAIT_PORT_EXPRESSION,
          { { Expr::PORT, portExpr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createClosePortExpression(GroupNodeCPtr portExpr)
    {
      auto portTy = constraintOf(portExpr);
      if (nullptr == portTy->self<ConstrainedInputType>()
          && nullptr == portTy->self<ConstrainedOutputType>()) {
        throw ValidationError(portExpr, "input or output port expected");
      }

      auto ty = ConstrainedVoidType::create();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::CLOSE_PORT_EXPRESSION, {
          { Expr::PORT, portExpr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createWritePortExpression(GroupNodeCPtr portExpr, GroupNodeCPtr valueExpr)
    {
      expect<ConstrainedOutputType>(portExpr);
      auto portTy = constraintOf<ConstrainedOutputType>(portExpr);

      valueExpr = implicit_cast(portTy->element, valueExpr);

      auto ty = ConstrainedVoidType::create();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::WRITE_PORT_EXPRESSION, {
          { Expr::PORT, portExpr }, { Expr::EXPRESSION, valueExpr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createReadPortExpression(GroupNodeCPtr portExpr)
    {
      asRootType(portExpr);
      expect<ConstrainedInputType>(portExpr);
      auto portTy = constraintOf<ConstrainedInputType>(portExpr);

      auto ty = ConstrainedOptType::get(portTy->element);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::READ_PORT_EXPRESSION,
          { { Expr::PORT, portExpr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createClearPortExpression(GroupNodeCPtr portExpr)
    {
      asRootType(portExpr);
      expect<ConstrainedInputType>(portExpr);
      auto portTy = constraintOf<ConstrainedInputType>(portExpr);

      auto ty = ConstrainedVoidType::create();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::CLEAR_PORT_EXPRESSION, {
          { Expr::PORT, portExpr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createHasNewInputExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedInputType>(expr);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::HAS_NEW_INPUT_EXPRESSION, { { VAst::Expr::PORT,
          expr } });
      annotateConstraint(res, ConstrainedPrimitiveType::getBoolean());
      return res;
    }

    GroupNodePtr VAst::createIsPortReadableExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedInputType>(expr);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::IS_PORT_READABLE_EXPRESSION, { {
          VAst::Expr::PORT, expr } });
      annotateConstraint(res, ConstrainedPrimitiveType::getBoolean());
      return res;
    }

    GroupNodePtr VAst::createIsOutputClosedExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedInputType>(expr);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::IS_OUTPUT_CLOSED_EXPRESSION, { {
          VAst::Expr::PORT, expr } });
      annotateConstraint(res, ConstrainedPrimitiveType::getBoolean());
      return res;
    }

    GroupNodePtr VAst::createIsInputClosedExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedInputType>(expr);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::IS_INPUT_CLOSED_EXPRESSION, { {
          VAst::Expr::PORT, expr } });
      annotateConstraint(res, ConstrainedPrimitiveType::getBoolean());
      return res;
    }

    GroupNodePtr VAst::createIsPortWritableExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedOutputType>(expr);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::IS_PORT_WRITABLE_EXPRESSION, { {
          VAst::Expr::PORT, expr } });
      annotateConstraint(res, ConstrainedPrimitiveType::getBoolean());
      return res;
    }

    GroupNodePtr VAst::createGetInputExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedInputType>(expr);
      auto inputTy = constraintOf<ConstrainedInputType>(expr);
      auto ty = ConstrainedOptType::get(inputTy->element);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_INPUT_EXPRESSION, {
          { VAst::Expr::PORT, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetOutputExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedOutputType>(expr);
      auto outputTy = constraintOf<ConstrainedOutputType>(expr);
      auto ty = ConstrainedOptType::get(outputTy->element);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_OUTPUT_EXPRESSION, { { VAst::Expr::PORT,
          expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createIsOptionalValuePresentExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedOptType>(expr);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::IS_PRESENT_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ConstrainedPrimitiveType::getBoolean());
      return res;
    }

    GroupNodePtr VAst::createTransformExpression(GroupNodeCPtr toType, GroupNodes arguments)
    {
      auto resTy = constraintOf(toType);
      auto ty = resTy;
      if (auto optTy = ty->self<ConstrainedOptType>(); optTy) {
        ty = optTy->element;
      }
      if (ty->self<ConstrainedNamedType>() == nullptr) {
        throw ValidationError(toType,
            "expected named type or an optional named type, but got " + resTy->toString());
      }
      if (!::mylang::types::SerializableTypeChecker::isSerializable(ty->type())) {
        throw ValidationError(toType, "expected a serializable type");
      }

      if (arguments.empty()) {
        throw ValidationError(toType, "transformation requires at least 1 argument");
      }
      ::std::set<NamePtr> typenames;
      for (auto arg : arguments) {
        auto t = expectExact<ConstrainedNamedType>(arg);
        if (!::mylang::types::SerializableTypeChecker::isSerializable(t->type())) {
          throw ValidationError(toType, "expected a serializable type");
        }
        typenames.insert(t->name());
        if (arguments.size() != 1 && t->isSameConstraint(*ty)) {
          throw ValidationError(toType,
              "the output type of transformation may not also be a parameter");
        }
      }
      if (typenames.size() != arguments.size()) {
        throw ValidationError(toType, "transformation arguments must have different types");
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::TRANSFORM_EXPRESSION, { { VAst::Relation::TYPE,
          toType }, { VAst::Expr::ARGUMENTS, arguments } });
      annotateConstraint(res, resTy);
      return res;
    }

    GroupNodePtr VAst::createZipArraysExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);
      expectConcreteConstraint<ConstrainedArrayType>(left);
      expectConcreteConstraint<ConstrainedArrayType>(right);

      auto leftTy = constraintOf<ConstrainedArrayType>(left);
      auto rightTy = constraintOf<ConstrainedArrayType>(right);

      auto tupleTy = ConstrainedTupleType::get( { leftTy->element, rightTy->element });

      auto bounds = leftTy->bounds.intersectWith(rightTy->bounds);
      if (!bounds) {
        throw ValidationError(right,
            "arrays do not overlap: " + leftTy->toString() + ", " + rightTy->toString());
      }

      auto ty = ConstrainedArrayType::getBoundedArray(tupleTy, bounds.value());
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::ZIP_ARRAYS_EXPRESSION, {
          { VAst::Expr::LHS, left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createMergeTuplesExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);

      expect<ConstrainedTupleType>(left);
      expect<ConstrainedTupleType>(right);

      auto leftTy = constraintOf<ConstrainedTupleType>(left);
      auto rightTy = constraintOf<ConstrainedTupleType>(right);

      auto ty = ConstrainedTupleType::merge( { leftTy, rightTy });

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::MERGE_TUPLES_EXPRESSION, { { VAst::Expr::LHS,
          left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createConcatenateArraysExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);

      expect<ConstrainedArrayType>(left);
      expect<ConstrainedArrayType>(right);

      auto leftTy = constraintOf<ConstrainedArrayType>(left);
      auto rightTy = constraintOf<ConstrainedArrayType>(right);

      auto b = leftTy->bounds + rightTy->bounds;

      auto ty = leftTy->element->unionWith(rightTy->element);
      if (!ty) {
        throw ValidationError(right, "incompatible type");
      }
      left = implicit_cast(leftTy->copy(ty), left);
      right = implicit_cast(rightTy->copy(ty), right);

      ty = ConstrainedArrayType::getBoundedArray(ty, b);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::CONCATENATE_ARRAYS_EXPRESSION, { {
          VAst::Expr::LHS, left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createConcatenateStringsExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);

      expectOneOf( { PrimitiveType::getString(), PrimitiveType::getChar() }, left);
      expectOneOf( { PrimitiveType::getString(), PrimitiveType::getChar() }, right);

      // FIXME: eventually we need to apply proper constraints
      auto ty = ConstrainedStringType::create();

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::CONCATENATE_STRINGS_EXPRESSION, { {
          VAst::Expr::LHS, left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createIntegerClampExpression(::std::shared_ptr<ConstrainedIntegerType> type,
        GroupNodeCPtr expr)
    {
      return createIntegerClampExpression(createType(type, expr->span()), expr);
    }

    GroupNodePtr VAst::createIntegerClampExpression(GroupNodeCPtr type, GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedIntegerType>(expr);
      expect<ConstrainedIntegerType>(type);

      auto tty = constraintOf<ConstrainedIntegerType>(type);
      auto ety = constraintOf<ConstrainedIntegerType>(expr);

      // we could intersect, but then would have to deal with an empty interestion
      auto ty = tty;

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::INTEGER_CLAMP_EXPRESSION, { {
          VAst::Relation::TYPE, type }, { VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createIntegerWrapExpression(::std::shared_ptr<ConstrainedIntegerType> type,
        GroupNodeCPtr expr)
    {
      return createIntegerWrapExpression(createType(type, expr->span()), expr);
    }

    GroupNodePtr VAst::createIntegerWrapExpression(GroupNodeCPtr type, GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedIntegerType>(expr);
      expect<ConstrainedIntegerType>(type);
      auto ty = constraintOf<ConstrainedIntegerType>(type);

      if (!ty->range.isFinite()) {
        throw ValidationError(type, "target type for a integer wrapping must be finite");
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::INTEGER_WRAP_EXPRESSION, { {
          VAst::Relation::TYPE, type }, { VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createIntegerNegateExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedIntegerType>(expr);
      auto ety = constraintOf<ConstrainedIntegerType>(expr);
      auto lit = getConstAnnotation(expr);

      // if we have a literal, then we want to immediately apply the
      // negation, as the literal may be used in a lot of context where we need
      // to know the value at compile time
      if (lit) {
        lit = lit->negate();
        if (!lit) {
          throw ValidationError(expr, "failed to negate a numeric constant");
        }
        auto tok = TokenNode::create(lit->value(), expr->span());
        auto res = GroupNode::create(VAst::GroupType::LITERAL_INTEGER, {
            { VAst::Token::LITERAL, tok } });
        annotateConstraint(res, lit->constraint());
        setConstAnnotation(res, lit);
        return res;
      }
      auto ty = ConstrainedIntegerType::create(ety->range.negate());
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::INTEGER_NEGATE_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createIntegerAddExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);
      expect<ConstrainedIntegerType>(left);
      expect<ConstrainedIntegerType>(right);
      auto leftTy = constraintOf<ConstrainedIntegerType>(left);
      auto rightTy = constraintOf<ConstrainedIntegerType>(right);

      auto ty = ConstrainedIntegerType::create(leftTy->range + rightTy->range);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::INTEGER_ADD_EXPRESSION, { { VAst::Expr::LHS,
          left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createIntegerSubtractExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);
      expect<ConstrainedIntegerType>(left);
      expect<ConstrainedIntegerType>(right);
      auto leftTy = constraintOf<ConstrainedIntegerType>(left);
      auto rightTy = constraintOf<ConstrainedIntegerType>(right);

      auto ty = ConstrainedIntegerType::create(leftTy->range - rightTy->range);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::INTEGER_SUBTRACT_EXPRESSION, { {
          VAst::Expr::LHS, left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createIntegerMultiplyExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);
      expect<ConstrainedIntegerType>(left);
      expect<ConstrainedIntegerType>(right);
      auto leftTy = constraintOf<ConstrainedIntegerType>(left);
      auto rightTy = constraintOf<ConstrainedIntegerType>(right);

      auto ty = ConstrainedIntegerType::create(leftTy->range * rightTy->range);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::INTEGER_MULTIPLY_EXPRESSION, { {
          VAst::Expr::LHS, left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createIntegerDivideExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);
      expect<ConstrainedIntegerType>(left);
      expect<ConstrainedIntegerType>(right);
      auto leftTy = constraintOf<ConstrainedIntegerType>(left);
      auto rightTy = constraintOf<ConstrainedIntegerType>(right);
      auto div = leftTy->range / rightTy->range;
      if (!div.has_value()) {
        throw ValidationError(right, "division by zero");
      }
      auto ty = ConstrainedIntegerType::create(*div);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::INTEGER_DIVIDE_EXPRESSION, { { VAst::Expr::LHS,
          left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createIntegerModulusExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);
      expect<ConstrainedIntegerType>(left);
      expect<ConstrainedIntegerType>(right);
      auto leftTy = constraintOf<ConstrainedIntegerType>(left);
      auto rightTy = constraintOf<ConstrainedIntegerType>(right);
      auto mod = leftTy->range.mod(rightTy->range);
      if (!mod.has_value()) {
        throw ValidationError(right, "invalid modulus");
      }

      auto ty = ConstrainedIntegerType::create(*mod);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::INTEGER_MODULUS_EXPRESSION, { { VAst::Expr::LHS,
          left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createIntegerRemainderExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);
      expect<ConstrainedIntegerType>(left);
      expect<ConstrainedIntegerType>(right);
      auto leftTy = constraintOf<ConstrainedIntegerType>(left);
      auto rightTy = constraintOf<ConstrainedIntegerType>(right);

      auto rem = leftTy->range.remainder(rightTy->range);
      if (!rem.has_value()) {
        throw ValidationError(right, "invalid remainder");
      }

      auto ty = ConstrainedIntegerType::create(*rem);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::INTEGER_REMAINDER_EXPRESSION, { {
          VAst::Expr::LHS, left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createRealNegateExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expect<ConstrainedRealType>(expr);
      auto ety = constraintOf<ConstrainedRealType>(expr);

      auto ty = ety;
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::REAL_NEGATE_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createRealAddExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);
      expect<ConstrainedRealType>(left);
      expect<ConstrainedRealType>(right);
      auto ty = constraintOf<ConstrainedRealType>(left);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::REAL_ADD_EXPRESSION, {
          { VAst::Expr::LHS, left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createRealSubtractExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);
      expect<ConstrainedRealType>(left);
      expect<ConstrainedRealType>(right);
      auto ty = constraintOf<ConstrainedRealType>(left);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::REAL_SUBTRACT_EXPRESSION, { { VAst::Expr::LHS,
          left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createRealMultiplyExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);
      expect<ConstrainedRealType>(left);
      expect<ConstrainedRealType>(right);
      auto ty = constraintOf<ConstrainedRealType>(left);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::REAL_MULTIPLY_EXPRESSION, { { VAst::Expr::LHS,
          left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createRealDivideExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);
      expect<ConstrainedRealType>(left);
      expect<ConstrainedRealType>(right);
      auto ty = constraintOf<ConstrainedRealType>(left);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::REAL_DIVIDE_EXPRESSION, { { VAst::Expr::LHS,
          left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createRealRemainderExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);
      expect<ConstrainedRealType>(left);
      expect<ConstrainedRealType>(right);
      auto ty = constraintOf<ConstrainedRealType>(left);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::REAL_REMAINDER_EXPRESSION, { { VAst::Expr::LHS,
          left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createNotExpression(GroupNodeCPtr expr)
    {
      asRootType(expr);
      expectBitlike(expr);

      auto ty = constraintOf(expr);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::NOT_EXPRESSION, {
          { VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createShortCircuitAndExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);
      expect<ConstrainedBooleanType>(left);
      expect<ConstrainedBooleanType>(right);

      intersection_cast(left, right);
      auto ty = constraintOf(left);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::SHORT_CIRCUIT_AND_EXPRESSION, { {
          VAst::Expr::LHS, left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createShortCircuitOrExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);
      expect<ConstrainedBooleanType>(left);
      expect<ConstrainedBooleanType>(right);

      intersection_cast(left, right);
      auto ty = constraintOf(left);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::SHORT_CIRCUIT_OR_EXPRESSION, { {
          VAst::Expr::LHS, left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createAndExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);
      expectBitlike(left);
      expectBitlike(right);

      intersection_cast(left, right);
      auto ty = constraintOf(left);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::AND_EXPRESSION, { { VAst::Expr::LHS, left }, {
          VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createOrExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);
      expectBitlike(left);
      expectBitlike(right);

      intersection_cast(left, right);
      auto ty = constraintOf(left);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::OR_EXPRESSION, { { VAst::Expr::LHS, left }, {
          VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createXorExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);
      expectBitlike(left);
      expectBitlike(right);

      intersection_cast(left, right);

      auto ty = constraintOf(left);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::XOR_EXPRESSION, { { VAst::Expr::LHS, left }, {
          VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createComparisonEQExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);
      union_cast(left, right);
      auto ty = ConstrainedBooleanType::create();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::COMPARISON_EQ_EXPRESSION, { { VAst::Expr::LHS,
          left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createComparisonNEQExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);
      union_cast(left, right);

      auto ty = ConstrainedBooleanType::create();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::COMPARISON_NEQ_EXPRESSION, { { VAst::Expr::LHS,
          left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createComparisonLTExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);

      expectSerializableConstraint(left);
      expectSerializableConstraint(right);
      union_cast(left, right);

      auto ty = ConstrainedBooleanType::create();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::COMPARISON_LT_EXPRESSION, { { VAst::Expr::LHS,
          left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createComparisonLTEExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);

      expectSerializableConstraint(left);
      expectSerializableConstraint(right);
      union_cast(left, right);

      auto ty = ConstrainedBooleanType::create();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::COMPARISON_LTE_EXPRESSION, { { VAst::Expr::LHS,
          left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createComparisonGTExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);

      expectSerializableConstraint(left);
      expectSerializableConstraint(right);
      union_cast(left, right);

      auto ty = ConstrainedBooleanType::create();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::COMPARISON_GT_EXPRESSION, { { VAst::Expr::LHS,
          left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createComparisonGTEExpression(GroupNodeCPtr left, GroupNodeCPtr right)
    {
      asRootType(left);
      asRootType(right);

      expectSerializableConstraint(left);
      expectSerializableConstraint(right);
      union_cast(left, right);

      auto ty = ConstrainedBooleanType::create();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::COMPARISON_GTE_EXPRESSION, { { VAst::Expr::LHS,
          left }, { VAst::Expr::RHS, right } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createTryCatchExpression(GroupNodeCPtr tryExpr, GroupNodeCPtr onErrorExpr)
    {
      union_cast(tryExpr, onErrorExpr);
      auto ty = constraintOf(tryExpr);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::TRY_EXPRESSION, { { VAst::Expr::LHS, tryExpr },
          { VAst::Expr::RHS, onErrorExpr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createConditionalExpression(GroupNodeCPtr condition, GroupNodeCPtr ifTrue,
        GroupNodeCPtr ifFalse)
    {
      asRootType(condition);

      expect<ConstrainedBooleanType>(condition);
      union_cast(ifTrue, ifFalse);
      auto ty = constraintOf(ifTrue);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::CONDITIONAL_EXPRESSION, { {
          VAst::Expr::CONDITION, condition }, { VAst::Expr::IFTRUE, ifTrue }, { VAst::Expr::IFFALSE,
          ifFalse } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLoopExpression(GroupNodeCPtr initialState,
        ::std::function<GroupNodeCPtr(GroupNodeCPtr ref)> condition,
        ::std::function<GroupNodeCPtr(GroupNodeCPtr ref)> body)
    {
      auto var = mylang::variables::Variable::createLocal(true);
      auto name = mylang::names::Name::create("loopstate");
      auto decl = createDefineVariable(nullptr, name, var, nullptr, initialState);
      auto ref = createVariableExpression(nullptr, name, constraintOf(decl));
      return createLoopExpression(decl, condition(ref), body(ref));
    }

    GroupNodePtr VAst::createLoopExpression(GroupNodeCPtr state, GroupNodeCPtr condition,
        GroupNodeCPtr body)
    {
      asRootType(condition);
      expectConcreteConstraint(state);
      expect<ConstrainedBooleanType>(condition);

      auto ty = constraintOf(state);
      body = implicit_cast(ty, body);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LOOP_EXPRESSION, {
          { VAst::Relation::DECL, state }, { VAst::Expr::CONDITION, condition }, {
              VAst::Expr::EXPRESSION, body } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createWithExpression(GroupNodeCPtr wrapExpr,
        ::std::function<GroupNodeCPtr(GroupNodeCPtr ref)> wrapper)
    {
      auto name = mylang::names::Name::create("tmp");
      auto var = mylang::variables::Variable::createLocal(false);
      auto decl = createDefineVariable(nullptr, name, var, nullptr, wrapExpr);
      auto ref = createVariableExpression(nullptr, name, getConstraintAnnotation(wrapExpr));
      return createWithExpression(decl, wrapper(ref));
    }

    GroupNodePtr VAst::createWithExpression(GroupNodeCPtr vardecl, GroupNodeCPtr expr)
    {
      GroupNodes nodes( { vardecl });
      return createWithExpression(nodes, expr);
    }

    GroupNodePtr VAst::createWithExpression(GroupNodes variables, GroupNodeCPtr expr)
    {
      auto ty = constraintOf(expr);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::WITH_EXPRESSION, { { VAst::Relation::DECLS,
          variables }, { VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createWithExpression(GroupNodeCPtr expr,
        const ::std::vector<::std::pair<NamePtr, GroupNodeCPtr>> &variables)
    {
      GroupNodes vars;
      auto vinfo = mylang::variables::Variable::createLocal(false);
      for (auto v : variables) {
        TokenNodePtr tok = TokenNode::create(v.first->uniqueLocalName(), expr->span());
        vars.emplace_back(createDefineVariable(tok, v.first, vinfo, nullptr, v.second));
      }
      return createWithExpression(vars, expr);
    }

    GroupNodeCPtr VAst::castToBaseType(GroupNodeCPtr expr)
    {
      if (!expr) {
        return nullptr;
      }
      auto eTy = constraintOf(expr);
      auto ty = eTy->basetype();
      if (!ty) {
        return expr;
      }
      return createImplicitBaseTypeCastExpression(expr);
    }

    GroupNodeCPtr VAst::castToRootType(GroupNodeCPtr expr)
    {
      if (!expr) {
        return nullptr;
      }

      GroupNodeCPtr res = expr;
      while (true) {
        GroupNodeCPtr tmp = res;
        res = castToBaseType(tmp);
        if (res == tmp) {
          break;
        }
      }
      return res;
    }

    GroupNodePtr VAst::createGetAsRootTypeExpression(GroupNodeCPtr expr)
    {
      auto eTy = constraintOf(expr);
      auto ty = eTy->roottype();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_AS_ROOTTYPE_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createGetAsBaseTypeExpression(GroupNodeCPtr expr)
    {
      auto eTy = constraintOf(expr);
      auto ty = eTy->basetype();

      if (!ty) {
        ty = eTy;
      }
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::GET_AS_BASETYPE_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createImplicitBaseTypeCastExpression(GroupNodeCPtr expr)
    {
      auto eTy = constraintOf(expr);
      auto ty = eTy->basetype();

      if (!ty) {
        throw ValidationError(expr, "type has no basetype : " + eTy->toString());
      }
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::IMPLICIT_BASETYPECAST_EXPRESSION, { {
          VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodeCPtr VAst::applyImplicitCast(ConstrainedTypePtr toType, GroupNodeCPtr expr)
    {
      return applyImplicitCast(createType(toType, expr->span()), expr);
    }

    GroupNodeCPtr VAst::applyImplicitCast(GroupNodeCPtr toType, GroupNodeCPtr expr)
    {
      auto ty = constraintOf(toType);
      auto exprTy = constraintOf(expr);
      if (ty->isSameConstraint(*exprTy)) {
        return expr;
      } else {
        return createImplicitCastExpression(toType, expr);
      }
    }

    GroupNodePtr VAst::applyImplicitCast(ConstrainedTypePtr toType, GroupNodePtr expr)
    {
      return applyImplicitCast(createType(toType, expr->span()), expr);
    }

    GroupNodePtr VAst::applyImplicitCast(GroupNodeCPtr toType, GroupNodePtr expr)
    {
      auto ty = constraintOf(toType);
      auto exprTy = constraintOf(expr);
      if (ty->isSameConstraint(*exprTy)) {
        return expr;
      } else {
        return createImplicitCastExpression(toType, expr);
      }
    }

    GroupNodePtr VAst::createImplicitCastExpression(ConstrainedTypePtr toType, GroupNodeCPtr expr)
    {
      return createImplicitCastExpression(createType(toType, expr->span()), expr);
    }

    GroupNodePtr VAst::createImplicitCastExpression(GroupNodeCPtr toType, GroupNodeCPtr expr)
    {
      auto ty = constraintOf(toType);
      auto exprTy = constraintOf(expr);

      if (!ty->canImplicitlyCastFrom(*exprTy)) {
        throw ValidationError(expr,
            "cannot implicitly cast " + exprTy->toString() + " to " + ty->toString());
      }
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::IMPLICIT_TYPECAST_EXPRESSION, { {
          VAst::Relation::TYPE, toType }, { VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createCheckedCastExpression(GroupNodeCPtr toType, GroupNodeCPtr expr)
    {
      auto ty = constraintOf(toType);
      auto exprTy = constraintOf(expr);

      if (!ty->canCastFrom(*exprTy)) {
        throw ValidationError(expr,
            "cannot safely or unsafely cast " + exprTy->toString() + " to " + ty->toString());
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::CHECKED_CAST_EXPRESSION, { {
          VAst::Relation::TYPE, toType }, { VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createSafeCastExpression(GroupNodeCPtr toType, GroupNodeCPtr expr)
    {
      auto ty = constraintOf(toType);
      auto exprTy = constraintOf(expr);

      if (!ty->canSafeCastFrom(*exprTy)) {
        throw ValidationError(expr,
            "cannot safely cast " + exprTy->toString() + " to " + ty->toString());
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::SAFE_TYPECAST_EXPRESSION, { {
          VAst::Relation::TYPE, toType }, { VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createUnsafeTypeCastExpression(GroupNodeCPtr toType, GroupNodeCPtr expr)
    {
      expectConcreteConstraint(toType);

      auto ty = constraintOf(toType);
      auto exprTy = constraintOf(expr);

      if (!ty->canCastFrom(*exprTy)) {
        throw ValidationError(expr, "cannot cast " + exprTy->toString() + " to " + ty->toString());
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::UNSAFE_TYPECAST_EXPRESSION, { {
          VAst::Relation::TYPE, toType }, { VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createNamedExpression(TokenNodeCPtr name, GroupNodeCPtr expr)
    {
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::NAMED_EXPRESSION, { { VAst::Token::IDENTIFIER,
          name }, { VAst::Expr::EXPRESSION, expr } });
      annotateConstraint(res, constraintOf(expr));
      return res;
    }

    GroupNodePtr VAst::createFunctionCallExpression(GroupNodeCPtr expr, GroupNodes arguments)
    {
      asRootType(expr);
      auto fnTy = constraintOf<ConstrainedFunctionType>(expr);

      if (fnTy->parameters.size() != arguments.size()) {
        throw ValidationError(expr,
            "expected " + std::to_string(fnTy->parameters.size()) + ", but found "
                + std::to_string(arguments.size()));
      }
      GroupNodes argv;
      for (size_t i = 0; i < arguments.size(); ++i) {
        auto paramTy = fnTy->parameters.at(i);
        argv.push_back(implicit_cast(paramTy, arguments.at(i)));
      }

      auto ty = fnTy->returnType;

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::CALL_EXPRESSION, {
          { VAst::Expr::FUNCTION, expr }, { VAst::Expr::ARGUMENTS, argv } });
      annotateConstraint(res, ty);
      return res;
    }

    // FIXME: TokenNodeCPtr is optional and should be the last paramter with a default of nullptr
    GroupNodePtr VAst::createVariableExpression(TokenNodeCPtr name, const NamePtr &globalName,
        const ConstrainedTypePtr &ty)
    {
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::VARIABLE_REFERENCE_EXPRESSION, { {
          VAst::Token::IDENTIFIER, name } });
      annotateConstraint(res, ty);
      setNameAnnotation(res, globalName);
      return res;
    }

    GroupNodePtr VAst::createLiteralBitExpression(ConstantPtr lit,
        const ::idioma::ast::Node::Span &span)
    {
      auto ty = lit->constraint()->self<ConstrainedBitType>();
      if (!ty) {
        throw ValidationError(span, "not a bit literal " + lit->value());
      }
      auto literal = TokenNode::create(lit->value(), span);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_BIT,
          { { VAst::Token::LITERAL, literal } });
      setConstAnnotation(res, lit);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralBooleanExpression(ConstantPtr lit,
        const ::idioma::ast::Node::Span &span)
    {
      auto ty = lit->constraint()->self<ConstrainedBooleanType>();
      if (!ty) {
        throw ValidationError(span, "not a boolean literal " + lit->value());
      }
      auto literal = TokenNode::create(lit->value(), span);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_BOOLEAN, { { VAst::Token::LITERAL,
          literal } });
      setConstAnnotation(res, lit);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralByteExpression(ConstantPtr lit,
        const ::idioma::ast::Node::Span &span)
    {
      auto ty = lit->constraint()->self<ConstrainedByteType>();
      if (!ty) {
        throw ValidationError(span, "not a byte literal " + lit->value());
      }
      auto literal = TokenNode::create(lit->value(), span);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_BYTE, {
          { VAst::Token::LITERAL, literal } });
      setConstAnnotation(res, lit);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralCharExpression(ConstantPtr lit,
        const ::idioma::ast::Node::Span &span)
    {
      auto ty = lit->constraint()->self<ConstrainedCharType>();
      if (!ty) {
        throw ValidationError(span, "not a char literal " + lit->value());
      }
      auto literal = TokenNode::create(lit->value(), span);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_CHAR, {
          { VAst::Token::LITERAL, literal } });
      setConstAnnotation(res, lit);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralIntegerExpression(ConstantPtr lit,
        const ::idioma::ast::Node::Span &span)
    {
      auto ty = lit->constraint()->self<ConstrainedIntegerType>();
      if (!ty) {
        throw ValidationError(span, "not an integer literal " + lit->value());
      }
      auto literal = TokenNode::create(lit->value(), span);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_INTEGER, { { VAst::Token::LITERAL,
          literal } });
      setConstAnnotation(res, lit);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralRealExpression(ConstantPtr lit,
        const ::idioma::ast::Node::Span &span)
    {
      auto ty = lit->constraint()->self<ConstrainedRealType>();
      if (!ty) {
        throw ValidationError(span, "not a real literal " + lit->value());
      }
      auto literal = TokenNode::create(lit->value(), span);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_REAL, {
          { VAst::Token::LITERAL, literal } });
      setConstAnnotation(res, lit);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralStringExpression(ConstantPtr lit,
        const ::idioma::ast::Node::Span &span)
    {
      auto ty = lit->constraint()->self<ConstrainedStringType>();
      if (!ty) {
        throw ValidationError(span, "not a string literal " + lit->value());
      }
      auto literal = TokenNode::create(lit->value(), span);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_STRING, { { VAst::Token::LITERAL,
          literal } });
      setConstAnnotation(res, lit);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralNilExpression(ConstantPtr lit,
        const ::idioma::ast::Node::Span &span)
    {
      auto ty = lit->constraint()->self<ConstrainedOptType>();
      if (!ty) {
        throw ValidationError(span, "not an nil literal " + lit->value());
      }
      if (ty->isConcrete()) {
        auto typeNode = createType(ty, span);
        return createNewOptionalExpression(typeNode, { });
      }
      auto literal = TokenNode::create(lit->value(), span);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_NIL,
          { { VAst::Token::LITERAL, literal } });
      setConstAnnotation(res, lit);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralEmptyArrayExpression(ConstantPtr lit,
        const ::idioma::ast::Node::Span &span)
    {
      auto ty = lit->constraint()->self<ConstrainedArrayType>();
      if (!ty) {
        throw ValidationError(span, "not an empty array literal " + lit->value());
      }
      if (ty->isConcrete()) {
        auto typeNode = createType(ty, span);
        return createNewArrayExpression(typeNode, { });
      }
      auto literal = TokenNode::create(lit->value(), span);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_EMPTY_ARRAY, { { VAst::Token::LITERAL,
          literal } });
      setConstAnnotation(res, lit);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralIntegerExpression(TokenNodeCPtr literal)
    {
      auto litTy = PrimitiveType::getInteger();
      auto lit = mylang::expressions::Constant::get(litTy, literal->text);
      auto ty = lit->constraint();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_INTEGER, { { VAst::Token::LITERAL,
          literal } });
      setConstAnnotation(res, lit);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralRealExpression(TokenNodeCPtr literal)
    {
      auto litTy = PrimitiveType::getReal();
      auto lit = mylang::expressions::Constant::get(litTy, literal->text);
      auto ty = lit->constraint();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_REAL, {
          { VAst::Token::LITERAL, literal } });
      setConstAnnotation(res, lit);

      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralBitExpression(TokenNodeCPtr literal)
    {
      auto litTy = PrimitiveType::getBit();
      auto lit = mylang::expressions::Constant::get(litTy, literal->text);
      auto ty = lit->constraint();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_BIT,
          { { VAst::Token::LITERAL, literal } });
      setConstAnnotation(res, lit);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralBooleanExpression(TokenNodeCPtr literal)
    {
      auto litTy = PrimitiveType::getBoolean();
      auto lit = mylang::expressions::Constant::get(litTy, literal->text);
      auto ty = lit->constraint();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_BOOLEAN, { { VAst::Token::LITERAL,
          literal } });
      setConstAnnotation(res, lit);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralByteExpression(TokenNodeCPtr literal)
    {
      auto litTy = PrimitiveType::getByte();
      auto lit = mylang::expressions::Constant::get(litTy, literal->text);
      auto ty = lit->constraint();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_BYTE, {
          { VAst::Token::LITERAL, literal } });
      setConstAnnotation(res, lit);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralCharExpression(TokenNodeCPtr literal)
    {
      // FIXME: verify that the string represents a single unicode character
      auto litTy = PrimitiveType::getChar();
      auto lit = mylang::expressions::Constant::get(litTy, literal->text);
      auto ty = lit->constraint();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_CHAR, {
          { VAst::Token::LITERAL, literal } });
      setConstAnnotation(res, lit);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralStringExpression(TokenNodeCPtr literal)
    {
      // FIXME: verify that the string represents is properly escaped
      auto litTy = PrimitiveType::getString();
      auto lit = mylang::expressions::Constant::get(litTy, literal->text);
      auto ty = lit->constraint();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_STRING, { { VAst::Token::LITERAL,
          literal } });
      setConstAnnotation(res, lit);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralNilExpression(const ::idioma::ast::Node::Span &span)
    {
      auto literal = TokenNode::create("nil", span);
      auto litTy = OptType::getEmpty();
      auto lit = mylang::expressions::Constant::get(litTy, literal->text);
      auto ty = lit->constraint();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_NIL,
          { { VAst::Token::LITERAL, literal } });
      setConstAnnotation(res, lit);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralEmptyArrayExpression(const ::idioma::ast::Node::Span &span)
    {
      auto literal = TokenNode::create("[]", span);
      auto litTy = ArrayType::get(AnyType::get());
      auto lit = mylang::expressions::Constant::get(litTy, literal->text);

      auto ty = lit->constraint();
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_EMPTY_ARRAY, { { VAst::Token::LITERAL,
          literal } });
      setConstAnnotation(res, lit);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralNilExpression(
        const ::std::shared_ptr<const ConstrainedOptType> &ty,
        const ::idioma::ast::Node::Span &span)
    {
      auto literal = TokenNode::create("nil", span);
      auto lit = mylang::expressions::Constant::get(ty->type(), literal->text);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_NIL,
          { { VAst::Token::LITERAL, literal } });
      setConstAnnotation(res, lit);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralEmptyArrayExpression(
        const ::std::shared_ptr<const ConstrainedArrayType> &ty,
        const ::idioma::ast::Node::Span &span)
    {
      auto literal = TokenNode::create("[]", span);
      auto lit = mylang::expressions::Constant::get(ty->type(), literal->text);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_EMPTY_ARRAY, { { VAst::Token::LITERAL,
          literal } });
      setConstAnnotation(res, lit);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralArrayExpression(GroupNodes elements,
        const ::idioma::ast::Node::Span &span)
    {
      if (elements.empty()) {
        return createLiteralEmptyArrayExpression(span);
      }

      elements = union_cast(elements);
      auto ty = ConstrainedArrayType::getFixedArray(constraintOf(elements.at(0)), elements.size());

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_ARRAY, { { VAst::Expr::EXPRESSIONS,
          elements } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralTupleExpression(GroupNodes elements)
    {
      std::vector<ConstrainedTypePtr> types;
      for (auto e : elements) {
        expectConcreteConstraint(e);
        types.push_back(constraintOf(e));
      }
      auto ty = ConstrainedTupleType::get(types);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_TUPLE, { { VAst::Relation::MEMBERS,
          elements } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralTupleMember(GroupNodeCPtr optType, GroupNodeCPtr memberValue)
    {
      ConstrainedTypePtr ty;
      if (optType) {
        expectConcreteConstraint(optType);
        ty = constraintOf(optType);
        memberValue = implicit_cast(ty, memberValue);
      } else {
        expectConcreteConstraint(memberValue);
        ty = constraintOf(memberValue);
        optType = createType(ty, memberValue->span());
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_TUPLE_MEMBER, { { VAst::Relation::TYPE,
          optType }, { VAst::Expr::EXPRESSION, memberValue } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralStructExpression(GroupNodes members)
    {
      std::set<::std::string> names;
      std::vector<ConstrainedStructType::Member> M;
      for (auto m : members) {
        auto name = idioma::astutils::getToken(m, VAst::Token::IDENTIFIER);
        auto expr = idioma::astutils::getGroup(m, VAst::Expr::EXPRESSION);

        if (!names.insert(name->text).second) {
          throw ValidationError(name, "duplicate name");
        }
        expectConcreteConstraint(expr);
        M.emplace_back(name->text, constraintOf(expr));
      }
      auto ty = ConstrainedStructType::get(M);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_STRUCT, { { VAst::Relation::MEMBERS,
          members } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createLiteralStructMember(TokenNodeCPtr memberName, GroupNodeCPtr optType,
        GroupNodeCPtr memberValue)
    {
      ConstrainedTypePtr ty;
      if (optType) {
        expectConcreteConstraint(optType);
        ty = constraintOf(optType);
        memberValue = implicit_cast(ty, memberValue);
      } else {
        expectConcreteConstraint(memberValue);
        ty = constraintOf(memberValue);
        optType = createType(ty, memberName->span());
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::LITERAL_STRUCT_MEMBER, { {
          VAst::Token::IDENTIFIER, memberName }, { VAst::Relation::TYPE, optType }, {
          VAst::Expr::EXPRESSION, memberValue } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createDefineNamespace(TokenNodeCPtr name, const NamePtr &globalName,
        GroupNodes members)
    {
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::DEF_NAMESPACE, {
          { VAst::Token::IDENTIFIER, name }, { VAst::Relation::DECLS, members } });
      setNameAnnotation(res, globalName);
      return res;
    }

    GroupNodePtr VAst::createDefineFunction(NamePtr name, const VariablePtr &vinfo,
        const ConstrainedFunctionType &fnTy, const ::idioma::ast::Node::Span &span,
        ::std::function<GroupNodes(GroupNodes)> bodyFN)
    {
      GroupNodeCPtr retTy = createType(fnTy.returnType, { });
      GroupNodes args;
      GroupNodes fnParams;
      auto fnParamType = ::mylang::variables::Variable::createParameter();
      for (auto p : fnTy.parameters) {
        auto pname = mylang::names::Name::create("arg");
        args.push_back(
            createVariableExpression(TokenNode::create(pname->uniqueLocalName(), span), pname, p));
        fnParams.push_back(
            createDefineVariable(nullptr, pname, fnParamType, createType(p, span), nullptr));
      }

      auto fnBody = createFunctionBody(bodyFN(args));
      return createDefineFunction(nullptr, name, vinfo, retTy, ::std::move(fnParams), fnBody);
    }

    GroupNodePtr VAst::createDefineFunction(TokenNodeCPtr name, const NamePtr &globalName,
        const VariablePtr &vinfo, GroupNodeCPtr retTy, GroupNodes parameters, GroupNodeCPtr body)
    {

      ::std::vector<ConstrainedTypePtr> parameterTypes;
      for (auto p : parameters) {
        expectConcreteConstraint(p);
        parameterTypes.push_back(constraintOf(p));
      }
      ConstrainedTypePtr returnTy;
      if (retTy) {
        expectConcreteConstraint(retTy);
        returnTy = constraintOf(retTy);
      } else {
        expectConcreteConstraint(body);
        returnTy = returnTypeOf(body);
        retTy = createType(returnTy, body->span());
      }
      if (body) {
        body = implicit_cast_function_body(returnTy, body);
        if (throwStatementDispositionOfBody(body) != ThrowStatementDisposition::FULL
            && returnStatementDispositionOfBody(body) != ReturnStatementDisposition::FULL) {
          if (!returnTy->isSameConstraint(*ConstrainedVoidType::create())) {
            throw ValidationError(body, "missing return in non-void function");
          }
        }
      }

      auto ty = ConstrainedFunctionType::get(returnTy, parameterTypes);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::DEF_FUNCTION, {
          { VAst::Token::IDENTIFIER, name }, { VAst::Relation::PARAMETERS, parameters }, {
              VAst::Relation::TYPE, retTy }, { VAst::Relation::BODY, body } });
      setNameAnnotation(res, globalName);
      annotateConstraint(res, ty);
      setVariableAnnotation(res, vinfo);
      return res;
    }

    GroupNodePtr VAst::createDefineExportedFunction(TokenNodeCPtr name, const NamePtr &globalName,
        GroupNodeCPtr retTy, GroupNodes parameters, GroupNodeCPtr body)
    {
      if (!globalName->isFQN()) {
        throw ValidationError(name, "an exported function must have a FQN");
      }
      auto vinfo = mylang::variables::Variable::createGlobal();

      auto fn = createDefineFunction(name, globalName, vinfo, retTy, parameters, body);

      auto N = getToken(fn, VAst::Token::IDENTIFIER);
      auto P = getRelations(fn, VAst::Relation::PARAMETERS);
      auto R = getRelation(fn, VAst::Relation::TYPE);
      auto B = getRelation(fn, VAst::Relation::BODY);

      auto ty = constraintOf(fn);
      auto gn = getNameAnnotation(fn);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::DEF_EXPORTED_FUNCTION, { {
          VAst::Token::IDENTIFIER, N }, { VAst::Relation::PARAMETERS, P },
          { VAst::Relation::TYPE, R }, { VAst::Relation::BODY, B } });
      setNameAnnotation(res, gn);
      annotateConstraint(res, ty);
      setVariableAnnotation(res, vinfo);

      return res;
    }

    GroupNodePtr VAst::createFunctionBody(GroupNodes statements)
    {
      auto voidTy = ConstrainedVoidType::create();
      ConstrainedTypePtr ty = returnTypeOf(statements, voidTy);

      if (!ty->isSameConstraint(*voidTy)) {
        if (throwStatementDisposition(statements) != ThrowStatementDisposition::FULL) {
          auto returnStmtDisposition = returnStatementDisposition(statements);
          if (returnStmtDisposition != ReturnStatementDisposition::FULL) {
            throw ValidationError("missing return");
          }
        }
      }

      if (breakStatementDisposition(statements) != BreakStatementDisposition::NONE) {
        throw ValidationError("found break statements outside of loops");
      }
      if (continueStatementDisposition(statements) != ContinueStatementDisposition::NONE) {
        throw ValidationError("found continue statements outside of loops");
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::FUNCTION_BODY, { { VAst::Relation::STATEMENTS,
          statements } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createDefineVariable(TokenNodeCPtr name, const NamePtr &globalName,
        const VariablePtr &vinfo, GroupNodeCPtr optType, GroupNodeCPtr optInitializer)
    {
      VAst ast;
      auto ty = constraintOf(optType);
      if (ty && optInitializer) {
        expectConcreteConstraint(optType);
        optInitializer = implicit_cast(ty, optInitializer);
      } else if (!ty && optInitializer) {
        expectConcreteConstraint(optInitializer);
        ty = constraintOf(optInitializer);
        optType = ast.createType(ty, optInitializer->span());
      } else if (!ty && !optInitializer) {
        throw ValidationError(name,
            "failed to determine variable type; missing type and/or initializer expression");
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::DEF_VARIABLE, {
          { VAst::Token::IDENTIFIER, name }, { VAst::Relation::TYPE, optType }, {
              VAst::Expr::INITIALIZER, optInitializer } });
      annotateConstraint(res, ty);
      setNameAnnotation(res, globalName);
      setVariableAnnotation(res, vinfo);
      return res;
    }

    GroupNodePtr VAst::createDefineType(TokenNodeCPtr name, const NamePtr &globalName,
        GroupNodeCPtr type)
    {
      auto implTy = constraintOf(type);

      if (implTy->self<ConstrainedVoidType>()) {
        throw ValidationError(name, "void cannot be used in this context");
      }

      auto ty = ConstrainedNamedType::get(globalName, [=]() {return implTy;});

      if (!TypeRecursionChecker::check(ty)) {
        throw ValidationError(name, "infinite-recursion type");
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::DEF_TYPE, { { VAst::Token::IDENTIFIER, name }, {
          VAst::Relation::TYPE, type } });
      setNameAnnotation(res, globalName);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createDefineProcess(TokenNodeCPtr name, const NamePtr &globalName,
        GroupNodeCPtr type, GroupNodes statements)
    {
      const ::std::string MAIN = "main";

      expect<ConstrainedProcessType>(type);
      ::std::vector<::std::string> blocks;
      for (auto s : statements) {
        if (s->hasType(GroupType::PROCESS_BLOCK)) {
          auto blockName = getToken(s, Token::IDENTIFIER);
          assert(blockName);
          blocks.push_back(blockName->text);
          if (blocks.size() > 1) {
            if (blockName->text == MAIN || blocks.at(0) == MAIN) {
              throw ValidationError(s, "'" + MAIN + "' must be the only block");
            }
          }
          if (blockName->text == MAIN && getExpr(s, Expr::CONDITION)) {
            throw ValidationError(s, "'" + MAIN + "' must not a condition");
          }
        }
      }
      if (blocks.empty()) {
        throw ValidationError(name, "a process must have at least one processing block");
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::DEF_PROCESS, {
          { VAst::Token::IDENTIFIER, name }, { VAst::Relation::TYPE, type }, {
              VAst::Relation::STATEMENTS, statements } });
      setNameAnnotation(res, globalName);
      auto ty = constraintOf(type);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createProcessBlock(TokenNodeCPtr name, const NamePtr &globalName,
        GroupNodeCPtr cond, GroupNodeCPtr body)
    {
      if (cond) {
        expect<ConstrainedBooleanType>(cond);
      }
      auto voidTy = ConstrainedVoidType::create();
      auto bodyTy = returnTypeOf( { body }, voidTy);
      if (bodyTy && !voidTy->isSameConstraint(*bodyTy)) {
        throw ValidationError(body, "process block must not have a return type");
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::PROCESS_BLOCK, {
          { VAst::Token::IDENTIFIER, name }, { VAst::Expr::CONDITION, cond }, {
              VAst::Relation::BODY, body } });
      setNameAnnotation(res, globalName);
      return res;
    }

    GroupNodePtr VAst::createProcessConstructor(const NamePtr &globalName, GroupNodes parameters,
        GroupNodeCPtr init, GroupNodeCPtr bodyBlock)
    {
      ::std::vector<ConstrainedTypePtr> parameterTypes;
      for (auto p : parameters) {
        expectConcreteConstraint(p);
        expectSerializableConstraint(p);
        parameterTypes.push_back(constraintOf(p));
      }

      auto voidTy = ConstrainedVoidType::create();
      auto bodyTy = returnTypeOf( { bodyBlock }, voidTy);
      if (bodyTy && !voidTy->isSameConstraint(*bodyTy)) {
        throw ValidationError(bodyBlock, "constructor must not have a return type");
      }

      // check the init statement
      if (init && !init->hasType(GroupType::CALL_CONSTRUCTOR_STMT)) {
        throw ValidationError(init,
            "a constructor init statement must be another constructor call");
      }

      // check the body

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::PROCESS_CONSTRUCTOR, { {
          VAst::Relation::PARAMETERS, parameters }, { VAst::Relation::INIT, init }, {
          VAst::Relation::BODY, bodyBlock } });
      setNameAnnotation(res, globalName);
      return res;
    }

    GroupNodePtr VAst::createTypeReference(TokenNodeCPtr name, const NamePtr &globalName,
        const ConstrainedTypePtr &type)
    {
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::TYPE_REFERENCE, { { VAst::Token::IDENTIFIER,
          name } });
      setNameAnnotation(res, globalName);
      annotateConstraint(res, type);
      return res;
    }

    GroupNodePtr VAst::createUnitReference(TokenNodeCPtr name)
    {
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(VAst::GroupType::UNIT_REFERENCE, { { VAst::Token::IDENTIFIER,
          name } });
      return res;
    }

    GroupNodePtr VAst::createReturnStatement(GroupNodeCPtr expr)
    {
      auto exprTy = constraintOf(expr);
      if (exprTy && exprTy->isSameConstraint(*ConstrainedVoidType::create())) {
        throw ValidationError(expr, "return expression cannot be void");
      }
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      return GroupNode::create(GroupType::RETURN_STMT, { { Expr::EXPRESSION, expr } });
    }

    GroupNodePtr VAst::createThrowStatement()
    {
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      return GroupNode::create(GroupType::THROW_STMT, { });
    }

    GroupNodePtr VAst::createBreakStatement()
    {
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      return GroupNode::create(GroupType::BREAK_STMT, { });
    }

    GroupNodePtr VAst::createContinueStatement()
    {
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      return GroupNode::create(GroupType::CONTINUE_STMT, { });
    }

    GroupNodePtr VAst::createTryStatement(GroupNodeCPtr tryBranch, GroupNodeCPtr catchBranch)
    {
      if (!tryBranch) {
        tryBranch = createStatementBlock(GroupNodes());
      }
      if (!catchBranch) {
        catchBranch = createStatementBlock(GroupNodes());
      }
      if (!tryBranch->hasType(VAst::GroupType::STATEMENT_BLOCK)) {
        tryBranch = createStatementBlock( { tryBranch });
      }
      if (!catchBranch->hasType(VAst::GroupType::STATEMENT_BLOCK)) {
        catchBranch = createStatementBlock( { catchBranch });
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      return GroupNode::create(GroupType::TRY_STMT, { { Stmt::TRY, tryBranch }, { Stmt::CATCH,
          catchBranch } });
    }

    GroupNodePtr VAst::createStatementBlock(GroupNodes statements)
    {
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      return GroupNode::create(GroupType::STATEMENT_BLOCK, { { Relation::STATEMENTS, statements } });
    }

    GroupNodePtr VAst::createStatementList(GroupNodes statements)
    {
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      return GroupNode::create(GroupType::STATEMENT_LIST, { { Relation::STATEMENTS, statements } });
    }

    GroupNodePtr VAst::createLogStatement(TokenNodeCPtr level, GroupNodeCPtr message,
        GroupNodes data)
    {
      asRootType(message);
      expect<ConstrainedStringType>(message);
      for (auto e : data) {
        expectConcreteConstraint(e);
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      return GroupNode::create(GroupType::LOG_STMT, { { Token::LOG_LEVEL, level }, { Expr::MESSAGE,
          message }, { Expr::ARGUMENTS, data } });
    }

    GroupNodePtr VAst::createSetStatement(GroupNodeCPtr mutableDest, GroupNodeCPtr src)
    {
      expect<ConstrainedMutableType>(mutableDest);
      auto ty = getConstraintAnnotation(mutableDest)->self<ConstrainedMutableType>();
      auto val = implicit_cast(ty->element, src);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(GroupType::SET_STMT, { { Expr::LHS, mutableDest }, { Expr::RHS,
          val } });
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createUpdateStatement(GroupNodeCPtr ref, GroupNodeCPtr expr)
    {
      auto ty = getConstraintAnnotation(ref);
      auto val = implicit_cast(ty, expr);

      if (!ref->hasType(GroupType::VARIABLE_REFERENCE_EXPRESSION)) {
        throw ValidationError(ref, "not an assignable expression");
      }

      auto name = getNameAnnotation(ref);

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(GroupType::UPDATE_STMT,
          { { Expr::LHS, ref }, { Expr::RHS, val } });
      setNameAnnotation(res, name);
      annotateConstraint(res, ty);
      return res;
    }

    GroupNodePtr VAst::createIfStatement(GroupNodeCPtr condition, GroupNodeCPtr iftrue,
        GroupNodeCPtr iffalse)
    {
      asRootType(condition);
      expect<ConstrainedBooleanType>(condition);

      if (!iftrue) {
        iftrue = createStatementBlock(GroupNodes());
      }
      if (!iffalse) {
        iffalse = createStatementBlock(GroupNodes());
      }
      if (!iftrue->hasType(VAst::GroupType::STATEMENT_BLOCK)) {
        iftrue = createStatementBlock( { iftrue });
      }
      if (!iffalse->hasType(VAst::GroupType::STATEMENT_BLOCK)) {
        iffalse = createStatementBlock( { iffalse });
      }

      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      return GroupNode::create(GroupType::IF_STMT, { { Expr::CONDITION, condition }, { Stmt::IFTRUE,
          iftrue }, { Stmt::IFFALSE, iffalse } });
    }

    GroupNodePtr VAst::createAssertStatement(GroupNodeCPtr condition, GroupNodeCPtr iffalse)
    {
      asRootType(condition);
      expect<ConstrainedBooleanType>(condition);
      if (iffalse) {
        expect<ConstrainedStringType>(iffalse);
      }
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      return GroupNode::create(GroupType::ASSERT_STMT, { { Expr::CONDITION, condition }, {
          Expr::MESSAGE, iffalse } });
    }

    GroupNodePtr VAst::createAssertStatement(GroupNodeCPtr condition, const ::std::string &errMsg)
    {
      GroupNodeCPtr msg;
      if (!errMsg.empty()) {
        msg = createLiteralString(errMsg, condition->span());
      }
      return createAssertStatement(condition, msg);
    }

    GroupNodePtr VAst::createCallConstructorStatement(GroupNodes args)
    {
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      return GroupNode::create(GroupType::CALL_CONSTRUCTOR_STMT, { { Expr::ARGUMENTS, args } });
    }

    GroupNodePtr VAst::createExpressionStatement(GroupNodeCPtr voidexpr)
    {
      expect<ConstrainedVoidType>(voidexpr);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      return GroupNode::create(GroupType::EXPRESSION_STMT, { { Expr::EXPRESSION, voidexpr } });
    }

    GroupNodePtr VAst::createForeachStatement(GroupNodeCPtr varDecl, GroupNodeCPtr expression,
        GroupNodeCPtr block)
    {
      asRootType(expression);
      expect<ConstrainedArrayType>(expression);
      auto arrTy = constraintOf<ConstrainedArrayType>(expression);
      auto elemTy = arrTy->element;
      auto varTy = constraintOf(varDecl);
      if (!varTy->canImplicitlyCastFrom(*elemTy)) {
        throw ValidationError(varDecl,
            arrTy->toString() + " is incompatible with " + varTy->toString());
      }
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      return GroupNode::create(GroupType::FOREACH_STMT, { { Relation::DECL, varDecl }, {
          Expr::EXPRESSION, expression }, { Stmt::LOOP_BODY, block } });
    }

    GroupNodePtr VAst::createWhileStatement(GroupNodeCPtr condition, GroupNodeCPtr iftrue)
    {
      asRootType(condition);
      expect<ConstrainedBooleanType>(condition);
      if (!iftrue) {
        iftrue = createStatementBlock(GroupNodes());
      }
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      return GroupNode::create(GroupType::WHILE_STMT, { { Expr::CONDITION, condition }, {
          Stmt::LOOP_BODY, iftrue } });
    }

    GroupNodePtr VAst::createWaitStatement(GroupNodes ports)
    {
      for (auto port : ports) {
        expectPort(port);
      }
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      return GroupNode::create(GroupType::WAIT_STMT, { { Expr::PORT, ports } });
    }

    GroupNodePtr VAst::createDefaultLiteral(ConstrainedTypePtr xty,
        const ::idioma::ast::Node::Span &span)
    {

      struct V: public ConstrainedTypeVisitor
      {
        V(VAst &xast, const ::idioma::ast::Node::Span &xspan)
            : ast(xast), span(xspan)
        {
        }

        ~V()
        {
        }

        virtual void visitAny(const ::std::shared_ptr<const ConstrainedAnyType>&) override
        {
        }

        void visitBit(const ::std::shared_ptr<const ConstrainedBitType>&) override
        {
        }

        void visitBoolean(const ::std::shared_ptr<const ConstrainedBooleanType>&) override
        {
        }

        void visitByte(const ::std::shared_ptr<const ConstrainedByteType>&) override
        {
        }

        void visitChar(const ::std::shared_ptr<const ConstrainedCharType>&) override
        {
        }

        void visitReal(const ::std::shared_ptr<const ConstrainedRealType>&) override
        {
        }

        void visitInteger(const ::std::shared_ptr<const ConstrainedIntegerType>&) override
        {
        }

        void visitString(const ::std::shared_ptr<const ConstrainedStringType>&) override
        {
        }

        void visitVoid(const ::std::shared_ptr<const ConstrainedVoidType>&) override
        {
        }

        void visitGeneric(const ::std::shared_ptr<const ConstrainedGenericType>&) override
        {
        }

        void visitArray(const ::std::shared_ptr<const ConstrainedArrayType> &ty) override
        {
          if (ty->bounds.min() == 0) {
            newexpr = ast.applyImplicitCast(ty, ast.createLiteralEmptyArrayExpression(span));
          }
        }

        void visitUnion(const ::std::shared_ptr<const ConstrainedUnionType>&) override
        {
        }

        void visitStruct(const ::std::shared_ptr<const ConstrainedStructType>&) override
        {
        }

        void visitTuple(const ::std::shared_ptr<const ConstrainedTupleType>&) override
        {
        }

        void visitFunction(const ::std::shared_ptr<const ConstrainedFunctionType>&) override
        {
        }

        void visitMutable(const ::std::shared_ptr<const ConstrainedMutableType>&) override
        {
        }

        void visitOpt(const ::std::shared_ptr<const ConstrainedOptType> &ty) override
        {
          newexpr = ast.applyImplicitCast(ty, ast.createLiteralNilExpression(span));
        }

        void visitNamedType(const ::std::shared_ptr<const ConstrainedNamedType>&) override
        {
        }

        void visitInputType(const ::std::shared_ptr<const ConstrainedInputType>&) override
        {
        }

        void visitOutputType(const ::std::shared_ptr<const ConstrainedOutputType>&) override
        {
        }

        void visitProcessType(const ::std::shared_ptr<const ConstrainedProcessType>&) override
        {
        }

        VAst &ast;
        GroupNodePtr newexpr;
        const ::idioma::ast::Node::Span &span;
      };

      V v(*this, span);
      xty->accept(v);
      if (v.newexpr) {
        return v.newexpr;
      }
      throw ValidationError(span, "type " + xty->toString() + " has no default literal");
    }

    GroupNodePtr VAst::createLiteral(ConstantPtr lit, const ::idioma::ast::Node::Span &span)
    {

      struct V: public ConstrainedTypeVisitor
      {
        V(VAst &xast, const ConstantPtr &xlit, const ::idioma::ast::Node::Span &xspan)
            : ast(xast), constant(xlit), span(xspan)
        {
        }

        ~V()
        {
        }

        virtual void visitAny(const ::std::shared_ptr<const ConstrainedAnyType>&) override
        {
        }

        void visitBit(const ::std::shared_ptr<const ConstrainedBitType>&) override
        {
          newexpr = ast.createLiteralBitExpression(constant, span);
        }

        void visitBoolean(const ::std::shared_ptr<const ConstrainedBooleanType>&) override
        {
          newexpr = ast.createLiteralBooleanExpression(constant, span);
        }

        void visitByte(const ::std::shared_ptr<const ConstrainedByteType>&) override
        {
          newexpr = ast.createLiteralByteExpression(constant, span);
        }

        void visitChar(const ::std::shared_ptr<const ConstrainedCharType>&) override
        {
          newexpr = ast.createLiteralCharExpression(constant, span);
        }

        void visitReal(const ::std::shared_ptr<const ConstrainedRealType>&) override
        {
          newexpr = ast.createLiteralRealExpression(constant, span);
        }

        void visitInteger(const ::std::shared_ptr<const ConstrainedIntegerType>&) override
        {
          newexpr = ast.createLiteralIntegerExpression(constant, span);
        }

        void visitString(const ::std::shared_ptr<const ConstrainedStringType>&) override
        {
          newexpr = ast.createLiteralStringExpression(constant, span);
        }

        void visitVoid(const ::std::shared_ptr<const ConstrainedVoidType>&) override
        {
        }

        void visitGeneric(const ::std::shared_ptr<const ConstrainedGenericType>&) override
        {
        }

        void visitArray(const ::std::shared_ptr<const ConstrainedArrayType>&) override
        {
        }

        void visitUnion(const ::std::shared_ptr<const ConstrainedUnionType>&) override
        {
        }

        void visitStruct(const ::std::shared_ptr<const ConstrainedStructType>&) override
        {
        }

        void visitTuple(const ::std::shared_ptr<const ConstrainedTupleType>&) override
        {
        }

        void visitFunction(const ::std::shared_ptr<const ConstrainedFunctionType>&) override
        {
        }

        void visitMutable(const ::std::shared_ptr<const ConstrainedMutableType>&) override
        {
        }

        void visitOpt(const ::std::shared_ptr<const ConstrainedOptType>&) override
        {
        }

        void visitNamedType(const ::std::shared_ptr<const ConstrainedNamedType>&) override
        {
        }

        void visitInputType(const ::std::shared_ptr<const ConstrainedInputType>&) override
        {
        }

        void visitOutputType(const ::std::shared_ptr<const ConstrainedOutputType>&) override
        {
        }

        void visitProcessType(const ::std::shared_ptr<const ConstrainedProcessType>&) override
        {
        }

        VAst &ast;
        GroupNodePtr newexpr;
        const ConstantPtr &constant;
        const ::idioma::ast::Node::Span &span;
      };

      V v(*this, lit, span);
      lit->constraint()->accept(v);
      if (v.newexpr) {
        return v.newexpr;
      }
      throw ValidationError(span, "Internal error: Cannot create a literal constant");
    }

    GroupNodePtr VAst::createLiteralInteger(const Integer &value,
        const ::idioma::ast::Node::Span &span)
    {
      auto v = value.value();
      if (v.has_value()) {
        ::std::ostringstream ss;
        ss << *v;
        auto tok = idioma::ast::TokenNode::create(ss.str(), span);
        auto n = createLiteralIntegerExpression(tok);
        return n;
      }
      throw ValidationError(span, "Cannot create an infinite integer constant");
    }

    GroupNodePtr VAst::createLiteralReal(const BigReal &value,
        const ::idioma::ast::Node::Span &span)
    {
      ::std::ostringstream ss;
      ss << value;
      auto tok = idioma::ast::TokenNode::create(ss.str(), span);
      auto n = createLiteralRealExpression(tok);
      return n;
    }

    GroupNodePtr VAst::createLiteralBoolean(bool value, const ::idioma::ast::Node::Span &span)
    {
      auto tok = idioma::ast::TokenNode::create(value ? "true" : "false", span);
      auto n = createLiteralBooleanExpression(tok);
      return n;
    }

    GroupNodePtr VAst::createLiteralBit(bool value, const ::idioma::ast::Node::Span &span)
    {
      auto tok = idioma::ast::TokenNode::create(value ? "0b1" : "0b0", span);
      auto n = createLiteralBitExpression(tok);
      return n;
    }

    GroupNodePtr VAst::createLiteralByte(uint32_t value, const ::idioma::ast::Node::Span &span)
    {
      ::std::ostringstream ss;
      ss << "0bx" << ::std::hex << std::setfill('0') << ::std::setw(2) << value;
      auto tok = idioma::ast::TokenNode::create(ss.str(), span);
      auto n = createLiteralByteExpression(tok);
      return n;
    }

    GroupNodePtr VAst::createLiteralChar(const ::std::string &utf8char,
        const ::idioma::ast::Node::Span &span)
    {
      auto tok = idioma::ast::TokenNode::create(utf8char, span);
      auto n = createLiteralCharExpression(tok);
      return n;
    }

    GroupNodePtr VAst::createLiteralString(const ::std::string &utf8String,
        const ::idioma::ast::Node::Span &span)
    {
      auto tok = idioma::ast::TokenNode::create(utf8String, span);
      auto n = createLiteralStringExpression(tok);
      return n;
    }

    GroupNodeCPtr VAst::createExpression(const ConstantPtr &constant,
        const ::idioma::ast::Node::Span &span)
    {
      if (constant->constraint()->self<ConstrainedBitType>()) {
        return createLiteralBitExpression(constant, span);
      }
      if (constant->constraint()->self<ConstrainedBooleanType>()) {
        return createLiteralBooleanExpression(constant, span);
      }
      if (constant->constraint()->self<ConstrainedByteType>()) {
        return createLiteralByteExpression(constant, span);
      }
      if (constant->constraint()->self<ConstrainedCharType>()) {
        return createLiteralCharExpression(constant, span);
      }
      if (constant->constraint()->self<ConstrainedIntegerType>()) {
        return createLiteralIntegerExpression(constant, span);
      }
      if (constant->constraint()->self<ConstrainedRealType>()) {
        return createLiteralRealExpression(constant, span);
      }
      if (constant->constraint()->self<ConstrainedStringType>()) {
        return createLiteralStringExpression(constant, span);
      }
      if (constant->constraint()->self<ConstrainedOptType>()) {
        return createLiteralNilExpression(constant, span);
      }
      if (constant->constraint()->self<ConstrainedArrayType>()) {
        return createLiteralEmptyArrayExpression(constant, span);
      }
      throw ::std::runtime_error("unsupported constant " + constant->constraint()->toString());
    }

    GroupNodePtr VAst::createDefineGenericFunction(TokenNodeCPtr name)
    {
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(GroupType::DEF_GENERIC_FUNCTION,
          { { Token::IDENTIFIER, name } });
      setNameAnnotation(res, getNameAnnotation(name));
      return res;
    }

    GroupNodePtr VAst::createInstantiateGenericFunction(GroupNodeCPtr fnType, TokenNodeCPtr defName,
        TokenNodeCPtr instanceName)
    {
      expect<ConstrainedFunctionType>(fnType);

      auto ty = constraintOf<ConstrainedFunctionType>(fnType);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(GroupType::INSTANTIATE_GENERIC_FUNCTION, { {
          Token::GENERIC_DEFINITION_NAME, defName }, { Token::IDENTIFIER, instanceName }, {
          Relation::TYPE, fnType } });
      annotateConstraint(res, ty);
      setNameAnnotation(res, getNameAnnotation(instanceName));
      return res;
    }

    GroupNodePtr VAst::createInstantiateGenericFunction(TokenNodeCPtr defName,
        TokenNodeCPtr instanceName, GroupNodeCPtr fnDef)
    {
      if (!fnDef->hasType(GroupType::DEF_FUNCTION)) {
        throw ValidationError("expected a DEF_FUNCTION");
      }
      expect<ConstrainedFunctionType>(fnDef);

      auto ty = constraintOf<ConstrainedFunctionType>(fnDef);
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(GroupType::INSTANTIATE_GENERIC_FUNCTION, { {
          Token::GENERIC_DEFINITION_NAME, defName }, { Token::IDENTIFIER, instanceName }, {
          Relation::GENERIC_INSTANCE_FUNCTION, fnDef } });
      annotateConstraint(res, ty);
      setNameAnnotation(res, getNameAnnotation(instanceName));
      return res;
    }

    GroupNodeCPtr VAst::createInlineGroup(GroupNodes nodes)
    {
      trace_create_node(__FILE__, __LINE__, __FUNCTION__);
      auto res = GroupNode::create(GroupType::INLINE_GROUP, { { Relation::INLINE_GROUP_MEMBERS,
          nodes } });
      return res;
    }

    GroupNodeCPtr VAst::createNewObject(GroupNodeCPtr type, GroupNodes argv)
    {
      struct V: public ConstrainedTypeVisitor
      {
        V(VAst &xast, GroupNodeCPtr xtype, GroupNodes xargs)
            : ast(xast), type(xtype), args(xargs)
        {
        }

        ~V()
        {
        }

        virtual void visitAny(const ::std::shared_ptr<const ConstrainedAnyType>&) override
        {
          throw ValidationError(type, "not constructible");
        }

        void visitBit(const ::std::shared_ptr<const ConstrainedBitType>&) override
        {
          newexpr = ast.createNewBitExpression(type, args);
        }

        void visitBoolean(const ::std::shared_ptr<const ConstrainedBooleanType>&) override
        {
          newexpr = ast.createNewBooleanExpression(type, args);
        }

        void visitByte(const ::std::shared_ptr<const ConstrainedByteType>&) override
        {
          newexpr = ast.createNewByteExpression(type, args);
        }

        void visitChar(const ::std::shared_ptr<const ConstrainedCharType>&) override
        {
          newexpr = ast.createNewCharExpression(type, args);
        }

        void visitReal(const ::std::shared_ptr<const ConstrainedRealType>&) override
        {
          newexpr = ast.createNewRealExpression(type, args);
        }

        void visitInteger(const ::std::shared_ptr<const ConstrainedIntegerType>&) override
        {
          newexpr = ast.createNewIntegerExpression(type, args);
        }

        void visitString(const ::std::shared_ptr<const ConstrainedStringType>&) override
        {
          newexpr = ast.createNewStringExpression(type, args);
        }

        void visitVoid(const ::std::shared_ptr<const ConstrainedVoidType>&) override
        {
          throw ValidationError(type, "not constructible");
        }

        void visitGeneric(const ::std::shared_ptr<const ConstrainedGenericType>&) override
        {
          throw ValidationError(type, "not constructible");
        }

        void visitArray(const ::std::shared_ptr<const ConstrainedArrayType>&) override
        {
          newexpr = ast.createNewArrayExpression(type, args);
        }

        void visitUnion(const ::std::shared_ptr<const ConstrainedUnionType>&) override
        {
          newexpr = ast.createNewUnionExpression(type, args);
        }

        void visitStruct(const ::std::shared_ptr<const ConstrainedStructType>&) override
        {
          newexpr = ast.createNewStructExpression(type, args);
        }

        void visitTuple(const ::std::shared_ptr<const ConstrainedTupleType>&) override
        {
          newexpr = ast.createNewTupleExpression(type, args);
        }

        void visitFunction(const ::std::shared_ptr<const ConstrainedFunctionType>&) override
        {
          newexpr = ast.createNewFunctionExpression(type, args);
        }

        void visitMutable(const ::std::shared_ptr<const ConstrainedMutableType>&) override
        {
          newexpr = ast.createNewMutableExpression(type, args);
        }

        void visitOpt(const ::std::shared_ptr<const ConstrainedOptType>&) override
        {
          newexpr = ast.createNewOptionalExpression(type, args);
        }

        void visitNamedType(const ::std::shared_ptr<const ConstrainedNamedType>&) override
        {
          newexpr = ast.createNewNamedTypeExpression(type, args);
        }

        void visitInputType(const ::std::shared_ptr<const ConstrainedInputType>&) override
        {
          throw ValidationError(type, "not constructible");
        }

        void visitOutputType(const ::std::shared_ptr<const ConstrainedOutputType>&) override
        {
          throw ValidationError(type, "not constructible");
        }

        void visitProcessType(const ::std::shared_ptr<const ConstrainedProcessType>&) override
        {
          throw ValidationError(type, "not constructible");
        }

        VAst &ast;
        GroupNodeCPtr newexpr;
        GroupNodeCPtr type;
        GroupNodes args;
      };

      V v(*this, type, argv);
      constraintOf(type)->accept(v);
      if (v.newexpr) {
        return v.newexpr;
      }
      throw ValidationError(type, "no such constructor");
    }

    TokenNodeCPtr VAst::getToken(const GroupNodeCPtr &g, const VAst::Token &token)
    {
      return ::idioma::astutils::getToken(g, token);
    }

    ::std::vector<TokenNodeCPtr> VAst::getTokens(const GroupNodeCPtr &g, const VAst::Token &token)
    {
      return ::idioma::astutils::getTokens(g, token);
    }

    GroupNodeCPtr VAst::getExpr(const GroupNodeCPtr &g, const VAst::Expr &rel)
    {
      return ::idioma::astutils::getGroup(g, rel);
    }

    GroupNodes VAst::getExprs(const GroupNodeCPtr &g, const VAst::Expr &rel)
    {
      return ::idioma::astutils::getGroups(g, rel);
    }

    GroupNodeCPtr VAst::getStmt(const GroupNodeCPtr &g, const VAst::Stmt &rel)
    {
      return ::idioma::astutils::getGroup(g, rel);
    }

    GroupNodes VAst::getStmts(const GroupNodeCPtr &g, const VAst::Stmt &rel)
    {
      return ::idioma::astutils::getGroups(g, rel);
    }

    GroupNodeCPtr VAst::getRelation(const GroupNodeCPtr &g, const VAst::Relation &rel)
    {
      return ::idioma::astutils::getGroup(g, rel);
    }

    GroupNodes VAst::getRelations(const GroupNodeCPtr &g, const VAst::Relation &rel)
    {
      return ::idioma::astutils::getGroups(g, rel);
    }

    static ::std::vector<::std::string> getName(const GroupNodeCPtr &node)
    {
      auto tok = VAst::getToken(node, mylang::vast::VAst::Token::IDENTIFIER);
      if (tok) {
        return {tok->text};
      } else {
        return {};
      }
    }

    GroupNodeCPtr VAst::findDefinition(const GroupNodeCPtr &root, const ::mylang::names::FQN &fqn)
    {
      struct ExtractVisitor: public VAstQuery
      {
        enum MatchType
        {
          NONE, FULL, PARTIAL
        };

        ExtractVisitor(const mylang::names::FQN &xfqn)
            : reverseFQN(xfqn.segments()), count(0)
        {
          ::std::reverse(reverseFQN.begin(), reverseFQN.end());
        }

        ~ExtractVisitor()
        {
        }

        /**
         * Match the name components of the specified node.
         * @param node a node
         * @return true if the node's name was matched, false otherwise
         */
        MatchType matchNames(const GroupNodeCPtr &node)
        {
          auto names = getName(node);
          if (names.empty()) {
            return NONE;
          }
          for (const auto &n : names) {
            if (reverseFQN.empty()) {
              return NONE;
            }
            if (n != reverseFQN.back().localName()) {
              return NONE;
            }
            reverseFQN.pop_back();
          }
          return reverseFQN.empty() ? FULL : PARTIAL;
        }

        MatchType visit_def(const GroupNodeCPtr &node)
        {
          switch (matchNames(node)) {
          case FULL:
            ++count;
            objectDefinition = node;
            return FULL;
          case PARTIAL:
            return PARTIAL;
          default:
            return NONE;
          }
        }

        void visit_root(GroupNodeCPtr node) override
        {
          auto children = getRelations(node, VAst::Relation::DECLS);
          for (auto c : children) {
            auto bak = reverseFQN;
            visit(c);
            if (objectDefinition == nullptr) {
              //back up
              reverseFQN = bak;
            }
          }
        }

        void visit_def_namespace(GroupNodeCPtr node) override
        {
          if (visit_def(node) == PARTIAL) {
            auto bak = reverseFQN;
            auto children = getRelations(node, VAst::Relation::DECLS);
            if (children.size() == 1) {
              visit(children.at(0));
            }
            if (objectDefinition == nullptr) {
              //back up
              reverseFQN = bak;
            }
          }
        }

        void visit_def_exported_function(GroupNodeCPtr node) override
        {
          visit_def(node);
          return;
        }

        void visit_def_function(GroupNodeCPtr node) override
        {
          if (getTagAnnotation(node) == "transform") {
            visit_def(node);
          }
          return;
        }

        void visit_def_variable(GroupNodeCPtr node) override
        {
          auto vinfo = getVariableAnnotation(node);
          if (vinfo->kind == mylang::variables::Variable::KIND_VALUE) {
            if (vinfo->scope == mylang::variables::Variable::SCOPE_GLOBAL) {
              visit_def(node);
            }
          }
          return;
        }

        void visit_def_type(GroupNodeCPtr node) override
        {
          visit_def(node);
          return;
        }

        void visit_def_process(GroupNodeCPtr node) override
        {
          visit_def(node);
          return;
        }

        ::std::vector<mylang::names::FQN> reverseFQN;
        size_t count;
        GroupNodeCPtr objectDefinition;
      };

      ExtractVisitor v(fqn);
      v.visit(root);
      if (v.count == 1) {
        return v.objectDefinition;
      } else {
        return nullptr;
      }
    }

#define TO_CASE(C,X) case C::X : return ""# X;

    ::std::string VAst::toString(GroupType grp)
    {
      switch (grp) {
      TO_CASE(GroupType, ROOT)
      TO_CASE(GroupType, INLINE_GROUP)
      TO_CASE(GroupType, DEF_NAMESPACE)
      TO_CASE(GroupType, DEF_TYPE)
      TO_CASE(GroupType, DEF_FUNCTION)
      TO_CASE(GroupType, DEF_VARIABLE)
      TO_CASE(GroupType, DEF_PROCESS)
      TO_CASE(GroupType, DEF_GENERIC_FUNCTION)
      TO_CASE(GroupType, INSTANTIATE_GENERIC_FUNCTION)
      TO_CASE(GroupType, DEF_EXPORTED_FUNCTION)
      TO_CASE(GroupType, PROCESS_CONSTRUCTOR)
      TO_CASE(GroupType, PROCESS_BLOCK)
      TO_CASE(GroupType, STATEMENT_BLOCK)
      TO_CASE(GroupType, ASSERT_STMT)
      TO_CASE(GroupType, FOREACH_STMT)
      TO_CASE(GroupType, IF_STMT)
      TO_CASE(GroupType, LOG_STMT)
      TO_CASE(GroupType, CALL_CONSTRUCTOR_STMT)
      TO_CASE(GroupType, RETURN_STMT)
      TO_CASE(GroupType, THROW_STMT)
      TO_CASE(GroupType, TRY_STMT)
      TO_CASE(GroupType, UPDATE_STMT)
      TO_CASE(GroupType, WHILE_STMT)
      TO_CASE(GroupType, WAIT_STMT)
      TO_CASE(GroupType, EXPRESSION_STMT)
      TO_CASE(GroupType, SET_STMT)
      TO_CASE(GroupType, STATEMENT_LIST)
      TO_CASE(GroupType, BREAK_STMT)
      TO_CASE(GroupType, CONTINUE_STMT)
      TO_CASE(GroupType, TYPE)
      TO_CASE(GroupType, LITERAL_BYTE)
      TO_CASE(GroupType, LITERAL_BIT)
      TO_CASE(GroupType, LITERAL_BOOLEAN)
      TO_CASE(GroupType, LITERAL_CHAR)
      TO_CASE(GroupType, LITERAL_REAL)
      TO_CASE(GroupType, LITERAL_INTEGER)
      TO_CASE(GroupType, LITERAL_STRING)
      TO_CASE(GroupType, LITERAL_ARRAY)
      TO_CASE(GroupType, LITERAL_TUPLE)
      TO_CASE(GroupType, LITERAL_STRUCT)
      TO_CASE(GroupType, LITERAL_STRUCT_MEMBER)
      TO_CASE(GroupType, LITERAL_TUPLE_MEMBER)
      TO_CASE(GroupType, LITERAL_NIL)
      TO_CASE(GroupType, LITERAL_EMPTY_ARRAY)
      TO_CASE(GroupType, NAMED_EXPRESSION)
      TO_CASE(GroupType, GUARD_EXPRESSION)
      TO_CASE(GroupType, VARIABLE_REFERENCE_EXPRESSION)
      TO_CASE(GroupType, LOOP_EXPRESSION)
      TO_CASE(GroupType, NEW_BYTE_EXPRESSION)
      TO_CASE(GroupType, NEW_BIT_EXPRESSION)
      TO_CASE(GroupType, NEW_BOOLEAN_EXPRESSION)
      TO_CASE(GroupType, NEW_CHAR_EXPRESSION)
      TO_CASE(GroupType, NEW_INTEGER_EXPRESSION)
      TO_CASE(GroupType, NEW_REAL_EXPRESSION)
      TO_CASE(GroupType, NEW_STRING_EXPRESSION)
      TO_CASE(GroupType, NEW_FUNCTION_EXPRESSION)
      TO_CASE(GroupType, NEW_OPTIONAL_EXPRESSION)
      TO_CASE(GroupType, NEW_ARRAY_EXPRESSION)
      TO_CASE(GroupType, NEW_STRUCT_EXPRESSION)
      TO_CASE(GroupType, NEW_TUPLE_EXPRESSION)
      TO_CASE(GroupType, NEW_UNION_EXPRESSION)
      TO_CASE(GroupType, NEW_NAMEDTYPE_EXPRESSION)
      TO_CASE(GroupType, NEW_PROCESS_EXPRESSION)
      TO_CASE(GroupType, NEW_MUTABLE_EXPRESSION)
      TO_CASE(GroupType, TRY_EXPRESSION)
      TO_CASE(GroupType, ORELSE_EXPRESSION)
      TO_CASE(GroupType, INTEGER_ADD_EXPRESSION)
      TO_CASE(GroupType, INTEGER_SUBTRACT_EXPRESSION)
      TO_CASE(GroupType, INTEGER_MULTIPLY_EXPRESSION)
      TO_CASE(GroupType, INTEGER_DIVIDE_EXPRESSION)
      TO_CASE(GroupType, INTEGER_MODULUS_EXPRESSION)
      TO_CASE(GroupType, INTEGER_REMAINDER_EXPRESSION)
      TO_CASE(GroupType, INTEGER_NEGATE_EXPRESSION)
      TO_CASE(GroupType, REAL_ADD_EXPRESSION)
      TO_CASE(GroupType, REAL_SUBTRACT_EXPRESSION)
      TO_CASE(GroupType, REAL_MULTIPLY_EXPRESSION)
      TO_CASE(GroupType, REAL_DIVIDE_EXPRESSION)
      TO_CASE(GroupType, REAL_REMAINDER_EXPRESSION)
      TO_CASE(GroupType, REAL_NEGATE_EXPRESSION)
      TO_CASE(GroupType, INTEGER_WRAP_EXPRESSION)
      TO_CASE(GroupType, INTEGER_CLAMP_EXPRESSION)
      TO_CASE(GroupType, WITH_EXPRESSION)
      TO_CASE(GroupType, CONDITIONAL_EXPRESSION)
      TO_CASE(GroupType, VOID_EXPRESSION)
      TO_CASE(GroupType, SHORT_CIRCUIT_AND_EXPRESSION)
      TO_CASE(GroupType, SHORT_CIRCUIT_OR_EXPRESSION)
      TO_CASE(GroupType, AND_EXPRESSION)
      TO_CASE(GroupType, OR_EXPRESSION)
      TO_CASE(GroupType, XOR_EXPRESSION)
      TO_CASE(GroupType, NOT_EXPRESSION)
      TO_CASE(GroupType, COMPARISON_EQ_EXPRESSION)
      TO_CASE(GroupType, COMPARISON_NEQ_EXPRESSION)
      TO_CASE(GroupType, COMPARISON_LT_EXPRESSION)
      TO_CASE(GroupType, COMPARISON_LTE_EXPRESSION)
      TO_CASE(GroupType, COMPARISON_GT_EXPRESSION)
      TO_CASE(GroupType, COMPARISON_GTE_EXPRESSION)
      TO_CASE(GroupType, STRUCT_MEMBER_EXPRESSION)
      TO_CASE(GroupType, UNION_MEMBER_EXPRESSION)
      TO_CASE(GroupType, TUPLE_MEMBER_EXPRESSION)
      TO_CASE(GroupType, PROCESS_MEMBER_EXPRESSION)
      TO_CASE(GroupType, GET_OPTIONAL_VALUE_EXPRESSION)
      TO_CASE(GroupType, OPTIFY_EXPRESSION)
      TO_CASE(GroupType, IS_PRESENT_EXPRESSION)
      TO_CASE(GroupType, INDEX_ARRAY_EXPRESSION)
      TO_CASE(GroupType, INDEX_STRING_EXPRESSION)
      TO_CASE(GroupType, SUBRANGE_ARRAY_EXPRESSION)
      TO_CASE(GroupType, SUBRANGE_STRING_EXPRESSION)
      TO_CASE(GroupType, GET_MUTABLE_VALUE_EXPRESSION)
      TO_CASE(GroupType, CONVERT_TO_TUPLE_EXPRESSION)
      TO_CASE(GroupType, GET_HEAD_EXPRESSION)
      TO_CASE(GroupType, GET_TAIL_EXPRESSION)
      TO_CASE(GroupType, STRUCT_FROM_TUPLE_EXPRESSION)
      TO_CASE(GroupType, CALL_EXPRESSION)
      TO_CASE(GroupType, LAMBDA_EXPRESSION)
      TO_CASE(GroupType, GET_AS_ROOTTYPE_EXPRESSION)
      TO_CASE(GroupType, GET_AS_BASETYPE_EXPRESSION)
      TO_CASE(GroupType, SAFE_TYPECAST_EXPRESSION)
      TO_CASE(GroupType, UNSAFE_TYPECAST_EXPRESSION)
      TO_CASE(GroupType, IMPLICIT_TYPECAST_EXPRESSION)
      TO_CASE(GroupType, IMPLICIT_BASETYPECAST_EXPRESSION)
      TO_CASE(GroupType, CHECKED_CAST_EXPRESSION)
      TO_CASE(GroupType, ARRAY_LENGTH_EXPRESSION)
      TO_CASE(GroupType, STRING_LENGTH_EXPRESSION)
      TO_CASE(GroupType, STRING_TO_CHARS_EXPRESSION)
      TO_CASE(GroupType, CHAR_TO_STRING_EXPRESSION)
      TO_CASE(GroupType, FIND_ARRAY_EXPRESSION)
      TO_CASE(GroupType, FILTER_ARRAY_EXPRESSION)
      TO_CASE(GroupType, MAP_ARRAY_EXPRESSION)
      TO_CASE(GroupType, MAP_OPTIONAL_EXPRESSION)
      TO_CASE(GroupType, FLATTEN_ARRAY_EXPRESSION)
      TO_CASE(GroupType, PARTITION_ARRAY_EXPRESSION)
      TO_CASE(GroupType, FLATTEN_OPTIONAL_EXPRESSION)
      TO_CASE(GroupType, STRINGIFY_EXPRESSION)
      TO_CASE(GroupType, INTERPOLATE_TEXT_EXPRESSION)
      TO_CASE(GroupType, REVERSE_ARRAY_EXPRESSION)
      TO_CASE(GroupType, FOLD_ARRAY_EXPRESSION)
      TO_CASE(GroupType, CONCATENATE_ARRAYS_EXPRESSION)
      TO_CASE(GroupType, CONCATENATE_STRINGS_EXPRESSION)
      TO_CASE(GroupType, MERGE_TUPLES_EXPRESSION)
      TO_CASE(GroupType, ZIP_ARRAYS_EXPRESSION)
      TO_CASE(GroupType, GET_INPUT_EXPRESSION)
      TO_CASE(GroupType, GET_OUTPUT_EXPRESSION)
      TO_CASE(GroupType, HAS_NEW_INPUT_EXPRESSION)
      TO_CASE(GroupType, CLOSE_PORT_EXPRESSION)
      TO_CASE(GroupType, IS_OUTPUT_CLOSED_EXPRESSION)
      TO_CASE(GroupType, IS_INPUT_CLOSED_EXPRESSION)
      TO_CASE(GroupType, IS_PORT_READABLE_EXPRESSION)
      TO_CASE(GroupType, IS_PORT_WRITABLE_EXPRESSION)
      TO_CASE(GroupType, WAIT_PORT_EXPRESSION)
      TO_CASE(GroupType, WRITE_PORT_EXPRESSION)
      TO_CASE(GroupType, READ_PORT_EXPRESSION)
      TO_CASE(GroupType, CLEAR_PORT_EXPRESSION)
      TO_CASE(GroupType, BLOCK_READ_EVENTS_EXPRESSION)
      TO_CASE(GroupType, BLOCK_WRITE_EVENTS_EXPRESSION)
      TO_CASE(GroupType, GET_BYTE_AS_UNSIGNED_INTEGER_EXPRESSION)
      TO_CASE(GroupType, GET_BYTE_AS_SIGNED_INTEGER_EXPRESSION)
      TO_CASE(GroupType, GET_BITS_FROM_BIT_EXPRESSION)
      TO_CASE(GroupType, GET_BITS_FROM_BOOLEAN_EXPRESSION)
      TO_CASE(GroupType, GET_BITS_FROM_BYTE_EXPRESSION)
      TO_CASE(GroupType, GET_BITS_FROM_CHAR_EXPRESSION)
      TO_CASE(GroupType, GET_BITS_FROM_INTEGER_EXPRESSION)
      TO_CASE(GroupType, GET_BITS_FROM_REAL_EXPRESSION)
      TO_CASE(GroupType, GET_BIT_FROM_BITS_EXPRESSION)
      TO_CASE(GroupType, GET_BOOLEAN_FROM_BITS_EXPRESSION)
      TO_CASE(GroupType, GET_BYTE_FROM_BITS_EXPRESSION)
      TO_CASE(GroupType, GET_CHAR_FROM_BITS_EXPRESSION)
      TO_CASE(GroupType, GET_INTEGER_FROM_BITS_EXPRESSION)
      TO_CASE(GroupType, GET_REAL_FROM_BITS_EXPRESSION)
      TO_CASE(GroupType, BYTE_ENCODE_BIT_EXPRESSION)
      TO_CASE(GroupType, BYTE_ENCODE_BOOLEAN_EXPRESSION)
      TO_CASE(GroupType, BYTE_ENCODE_BYTE_EXPRESSION)
      TO_CASE(GroupType, BYTE_ENCODE_CHAR_EXPRESSION)
      TO_CASE(GroupType, BYTE_ENCODE_INTEGER_EXPRESSION)
      TO_CASE(GroupType, BYTE_ENCODE_REAL_EXPRESSION)
      TO_CASE(GroupType, BYTE_ENCODE_STRING_EXPRESSION)
      TO_CASE(GroupType, BYTE_DECODE_BIT_EXPRESSION)
      TO_CASE(GroupType, BYTE_DECODE_BOOLEAN_EXPRESSION)
      TO_CASE(GroupType, BYTE_DECODE_BYTE_EXPRESSION)
      TO_CASE(GroupType, BYTE_DECODE_CHAR_EXPRESSION)
      TO_CASE(GroupType, BYTE_DECODE_INTEGER_EXPRESSION)
      TO_CASE(GroupType, BYTE_DECODE_REAL_EXPRESSION)
      TO_CASE(GroupType, BYTE_DECODE_STRING_EXPRESSION)
      TO_CASE(GroupType, FUNCTION_BODY)
      TO_CASE(GroupType, MEMBER_FUNCTION)
      TO_CASE(GroupType, MACRO_REFERENCE)
      TO_CASE(GroupType, TYPE_REFERENCE)
      TO_CASE(GroupType, UNIT_REFERENCE)
      default:
        throw InternalError("Unknown GroupType");
      };
    }

    ::std::string VAst::toString(Expr grp)
    {
      switch (grp) {
      TO_CASE(Expr, LHS)
      TO_CASE(Expr, RHS)
      TO_CASE(Expr, PORT)
      TO_CASE(Expr, EXPRESSION)
      TO_CASE(Expr, EXPRESSIONS)
      TO_CASE(Expr, CONDITION)
      TO_CASE(Expr, IFTRUE)
      TO_CASE(Expr, IFFALSE)
      TO_CASE(Expr, FUNCTION)
      TO_CASE(Expr, ARGUMENTS)
      TO_CASE(Expr, INITIALIZER)
      TO_CASE(Expr, SIZE)
      TO_CASE(Expr, INDEX)
      TO_CASE(Expr, BEGIN)
      TO_CASE(Expr, END)
      TO_CASE(Expr, MESSAGE)
      default:
        throw InternalError("Unknown Expr");
      };
    }
    ::std::string VAst::toString(Stmt grp)
    {
      switch (grp) {
      TO_CASE(Stmt, IFTRUE)
      TO_CASE(Stmt, IFFALSE)
      TO_CASE(Stmt, TRY)
      TO_CASE(Stmt, CATCH)
      TO_CASE(Stmt, LOOP_BODY)
      default:
        throw InternalError("Unknown Stmt");
      };
    }

    ::std::string VAst::toString(Relation grp)
    {
      switch (grp) {
      TO_CASE(Relation, GENERIC_INSTANCE_FUNCTION)
      TO_CASE(Relation, INLINE_GROUP_MEMBERS)
      TO_CASE(Relation, CONSTRAINTS)
      TO_CASE(Relation, STATEMENTS)
      TO_CASE(Relation, INIT)
      TO_CASE(Relation, PARAMETERS)
      TO_CASE(Relation, PARAMETER_TYPES)
      TO_CASE(Relation, VAR_KIND)
      TO_CASE(Relation, WRAPPED)
      TO_CASE(Relation, MEMBERS)
      TO_CASE(Relation, DISCRIMINANT)
      TO_CASE(Relation, TYPE)
      TO_CASE(Relation, DECL)
      TO_CASE(Relation, DECLS)
      TO_CASE(Relation, BODY)
      TO_CASE(Relation, IFTRUE)
      TO_CASE(Relation, IFFALSE)
      TO_CASE(Relation, FQN)
      TO_CASE(Relation, QN)
      default:
        throw InternalError("Unknown Relation");
      };
    }
    ::std::string VAst::toString(Token grp)
    {
      switch (grp) {
      TO_CASE(Token, IDENTIFIER)
      TO_CASE(Token, OP)
      TO_CASE(Token, LITERAL)
      TO_CASE(Token, TYPE)
      TO_CASE(Token, LOG_LEVEL)
      TO_CASE(Token, GENERIC_DEFINITION_NAME)
      default:
        throw InternalError("Unknown Token");
      };
    }

  }
}
