#ifndef CLASS_MYLANG_VALIDATION_VAST_H
#define CLASS_MYLANG_VALIDATION_VAST_H
#include <mylang/defs.h>
#include <mylang/constraints/ConstrainedFunctionType.h>
#include <mylang/constraints/ConstrainedIntegerType.h>
#include <mylang/constraints/ConstrainedArrayType.h>
#include <mylang/constraints/ConstrainedOptType.h>
#include <mylang/names/FQN.h>
#include <vector>

namespace mylang {
  namespace vast {
    /**
     * This defined the Validated AST or VAst for short.
     */
    struct VAst
    {
      enum class Token
      {
        IDENTIFIER, OP, LITERAL, TYPE, LOG_LEVEL, GENERIC_DEFINITION_NAME
      };

      enum class Relation
      {
        GENERIC_INSTANCE_FUNCTION,
        INLINE_GROUP_MEMBERS,

        CONSTRAINTS,
        STATEMENTS,
        INIT,
        PARAMETERS,
        PARAMETER_TYPES,
        VAR_KIND,
        WRAPPED,
        MEMBERS,
        DISCRIMINANT,
        TYPE,
        DECL,
        DECLS,
        BODY,
        //  or statement
        IFTRUE, // true branch in a conditional statement or expression
        IFFALSE, // false branch in a conditional statement or expression
        FQN,   // a fully qualified name
        QN   // a qualified name

      };

      enum class Expr
      {
        // expressions
        LHS, // LHS in a binary expression
        RHS, // RHS in a binary expression
        PORT, // an expression for a port
        EXPRESSION, // an expression
        EXPRESSIONS, // a list of expressions (same as arguments)
        CONDITION, // a conditional expression (used in ternary)
        IFTRUE, // true branch in a conditional statement or expression
        IFFALSE, // false branch in a conditional statement or expression
        FUNCTION, // an expression whose constraint must be a function
        ARGUMENTS, // function arguments FIXME: use EXPRESSIONS when more appropriate
        INITIALIZER, // the initializer passed to the fold function
        SIZE, // an expression representing an integer
        INDEX, // an expression the represents a non-negative index
        BEGIN,
        END,
        MESSAGE
      };

      enum class Stmt
      {
        IFTRUE, // true branch in a conditional statement or expression
        IFFALSE, // false branch in a conditional statement or expression
        TRY,    // try-branch of try statement
        CATCH,  // catch branch of a try-statement
        LOOP_BODY, // the body of a loop is a sequence of statements
      };

      enum class GroupType
      {
        ROOT = 0,

        // a special node, that does nothing
        INLINE_GROUP = 22222,

        // definitions
        DEF_NAMESPACE = 300,
        DEF_TYPE = 301,
        DEF_FUNCTION = 302,
        DEF_VARIABLE = 303,
        DEF_PROCESS = 304,

        DEF_GENERIC_FUNCTION = 305,
        INSTANTIATE_GENERIC_FUNCTION = 306,
        DEF_EXPORTED_FUNCTION = 307,

        PROCESS_CONSTRUCTOR = 398,
        PROCESS_BLOCK = 399,

        STATEMENT_BLOCK = 200,
        ASSERT_STMT = 201,
        FOREACH_STMT = 202,
        IF_STMT = 203,
        LOG_STMT = 204,
        CALL_CONSTRUCTOR_STMT = 205,
        RETURN_STMT = 206,
        THROW_STMT = 207,
        TRY_STMT = 208,
        UPDATE_STMT = 209,
        WHILE_STMT = 210,
        WAIT_STMT = 211,
        EXPRESSION_STMT = 212,
        SET_STMT = 213,
        STATEMENT_LIST = 214,
        BREAK_STMT = 215,
        CONTINUE_STMT = 216,

        // a type group; must have a constraint annotation
        TYPE = 5,

        // literals (expressions)
        LITERAL_BYTE = 19,
        LITERAL_BIT = 20,
        LITERAL_BOOLEAN = 21,
        LITERAL_CHAR = 23,
        LITERAL_REAL = 24,
        LITERAL_INTEGER = 25,
        LITERAL_STRING = 26,
        // arrays are semi-literals; size is compiled time fixed, but contents are expressions
        LITERAL_ARRAY = 27,
        LITERAL_TUPLE = 28,
        LITERAL_STRUCT = 29,
        LITERAL_STRUCT_MEMBER = 30,
        LITERAL_TUPLE_MEMBER = 31,
        LITERAL_NIL = 32,
        LITERAL_EMPTY_ARRAY = 33,

        // expressions
        NAMED_EXPRESSION = 36,
        GUARD_EXPRESSION = 37,
        // VALUE_REFERENCE_EXPRESSION = 38,
        VARIABLE_REFERENCE_EXPRESSION = 39,
        LOOP_EXPRESSION = 40,
        NEW_BYTE_EXPRESSION = 409,
        NEW_BIT_EXPRESSION = 410,
        NEW_BOOLEAN_EXPRESSION = 411,
        NEW_CHAR_EXPRESSION = 412,
        NEW_INTEGER_EXPRESSION = 413,
        NEW_REAL_EXPRESSION = 414,
        NEW_STRING_EXPRESSION = 415,
        NEW_FUNCTION_EXPRESSION = 416,
        NEW_OPTIONAL_EXPRESSION = 417,
        NEW_ARRAY_EXPRESSION = 418,
        NEW_STRUCT_EXPRESSION = 419,
        NEW_TUPLE_EXPRESSION = 420,
        NEW_UNION_EXPRESSION = 421,
        NEW_NAMEDTYPE_EXPRESSION = 422,
        NEW_PROCESS_EXPRESSION = 423,
        NEW_MUTABLE_EXPRESSION = 424,
        TRY_EXPRESSION = 42,
        ORELSE_EXPRESSION = 43,

        INTEGER_ADD_EXPRESSION = 440,
        INTEGER_SUBTRACT_EXPRESSION = 441,
        INTEGER_MULTIPLY_EXPRESSION = 442,
        INTEGER_DIVIDE_EXPRESSION = 443,
        INTEGER_MODULUS_EXPRESSION = 444,
        INTEGER_REMAINDER_EXPRESSION = 445,
        INTEGER_NEGATE_EXPRESSION = 446,
        REAL_ADD_EXPRESSION = 447,
        REAL_SUBTRACT_EXPRESSION = 448,
        REAL_MULTIPLY_EXPRESSION = 449,
        REAL_DIVIDE_EXPRESSION = 450,
        REAL_REMAINDER_EXPRESSION = 451,
        REAL_NEGATE_EXPRESSION = 452,
        INTEGER_WRAP_EXPRESSION = 453,
        INTEGER_CLAMP_EXPRESSION = 454,
        WITH_EXPRESSION = 46,
        CONDITIONAL_EXPRESSION = 47,
        VOID_EXPRESSION = 48,
        SHORT_CIRCUIT_AND_EXPRESSION = 480,
        SHORT_CIRCUIT_OR_EXPRESSION = 481,
        AND_EXPRESSION = 490,
        OR_EXPRESSION = 491,
        XOR_EXPRESSION = 492,
        NOT_EXPRESSION = 493,
        COMPARISON_EQ_EXPRESSION = 500,
        COMPARISON_NEQ_EXPRESSION = 501,
        COMPARISON_LT_EXPRESSION = 502,
        COMPARISON_LTE_EXPRESSION = 503,
        COMPARISON_GT_EXPRESSION = 504,
        COMPARISON_GTE_EXPRESSION = 505,
        STRUCT_MEMBER_EXPRESSION = 510,
        UNION_MEMBER_EXPRESSION = 511,
        TUPLE_MEMBER_EXPRESSION = 512,
        PROCESS_MEMBER_EXPRESSION = 513,
        GET_OPTIONAL_VALUE_EXPRESSION = 520,
        OPTIFY_EXPRESSION = 521,
        IS_PRESENT_EXPRESSION = 522,
        INDEX_ARRAY_EXPRESSION = 530,
        INDEX_STRING_EXPRESSION = 531,
        SUBRANGE_ARRAY_EXPRESSION = 532,
        SUBRANGE_STRING_EXPRESSION = 533,
        GET_MUTABLE_VALUE_EXPRESSION = 534,
        CONVERT_TO_TUPLE_EXPRESSION = 535,
        GET_HEAD_EXPRESSION = 536,
        GET_TAIL_EXPRESSION = 537,
        STRUCT_FROM_TUPLE_EXPRESSION = 538,
        TRANSFORM_EXPRESSION = 539,

        CALL_EXPRESSION = 54,
        LAMBDA_EXPRESSION = 55,
        GET_AS_ROOTTYPE_EXPRESSION = 558,
        GET_AS_BASETYPE_EXPRESSION = 559,
        SAFE_TYPECAST_EXPRESSION = 560,
        UNSAFE_TYPECAST_EXPRESSION = 561,
        IMPLICIT_TYPECAST_EXPRESSION = 562,
        IMPLICIT_BASETYPECAST_EXPRESSION = 563,
        // all preconditions to safely perform the cast have been applied.
        CHECKED_CAST_EXPRESSION = 565,
        ARRAY_LENGTH_EXPRESSION = 590,
        STRING_LENGTH_EXPRESSION = 591,
        STRING_TO_CHARS_EXPRESSION = 592,
        CHAR_TO_STRING_EXPRESSION = 593,
        FIND_ARRAY_EXPRESSION = 598,
        FILTER_ARRAY_EXPRESSION = 599,
        MAP_ARRAY_EXPRESSION = 600,
        MAP_OPTIONAL_EXPRESSION = 601,
        FLATTEN_ARRAY_EXPRESSION = 602,
        PARTITION_ARRAY_EXPRESSION = 603,
        FLATTEN_OPTIONAL_EXPRESSION = 604,
        STRINGIFY_EXPRESSION = 605,
        INTERPOLATE_TEXT_EXPRESSION = 606,
        REVERSE_ARRAY_EXPRESSION = 607,
        FOLD_ARRAY_EXPRESSION = 610,
        PAD_ARRAY_EXPRESSION = 611,
        TRIM_ARRAY_EXPRESSION = 612,
        DROP_ARRAY_EXPRESSION = 613,
        TAKE_ARRAY_EXPRESSION = 614,
        CONCATENATE_ARRAYS_EXPRESSION = 62,
        CONCATENATE_STRINGS_EXPRESSION = 67,
        MERGE_TUPLES_EXPRESSION = 631,
        ZIP_ARRAYS_EXPRESSION = 660,
        GET_INPUT_EXPRESSION = 670,
        GET_OUTPUT_EXPRESSION = 671,
        HAS_NEW_INPUT_EXPRESSION = 672,
        CLOSE_PORT_EXPRESSION = 667,
        IS_OUTPUT_CLOSED_EXPRESSION = 668,
        IS_INPUT_CLOSED_EXPRESSION = 669,
        IS_PORT_READABLE_EXPRESSION = 673,
        IS_PORT_WRITABLE_EXPRESSION = 674,
        WAIT_PORT_EXPRESSION = 675,
        WRITE_PORT_EXPRESSION = 676,
        READ_PORT_EXPRESSION = 677,
        CLEAR_PORT_EXPRESSION = 678,
        BLOCK_READ_EVENTS_EXPRESSION = 679,
        BLOCK_WRITE_EVENTS_EXPRESSION = 680,
        GET_BYTE_AS_UNSIGNED_INTEGER_EXPRESSION = 690,
        GET_BYTE_AS_SIGNED_INTEGER_EXPRESSION = 691,

        GET_BITS_FROM_BIT_EXPRESSION = 801,
        GET_BITS_FROM_BOOLEAN_EXPRESSION = 802,
        GET_BITS_FROM_BYTE_EXPRESSION = 803,
        GET_BITS_FROM_CHAR_EXPRESSION = 804,
        GET_BITS_FROM_INTEGER_EXPRESSION = 805,
        GET_BITS_FROM_REAL_EXPRESSION = 806,

        GET_BIT_FROM_BITS_EXPRESSION = 810,
        GET_BOOLEAN_FROM_BITS_EXPRESSION = 811,
        GET_BYTE_FROM_BITS_EXPRESSION = 812,
        GET_CHAR_FROM_BITS_EXPRESSION = 813,
        GET_INTEGER_FROM_BITS_EXPRESSION = 814,
        GET_REAL_FROM_BITS_EXPRESSION = 815,

        // FIXME: implement these
        BYTE_ENCODE_BIT_EXPRESSION = 694,
        BYTE_ENCODE_BOOLEAN_EXPRESSION = 695,
        BYTE_ENCODE_BYTE_EXPRESSION = 696,
        BYTE_ENCODE_CHAR_EXPRESSION = 697,
        BYTE_ENCODE_INTEGER_EXPRESSION = 698,
        BYTE_ENCODE_REAL_EXPRESSION = 699,
        BYTE_ENCODE_STRING_EXPRESSION = 700,
        BYTE_DECODE_BIT_EXPRESSION = 711,
        BYTE_DECODE_BOOLEAN_EXPRESSION = 712,
        BYTE_DECODE_BYTE_EXPRESSION = 713,
        BYTE_DECODE_CHAR_EXPRESSION = 714,
        BYTE_DECODE_INTEGER_EXPRESSION = 715,
        BYTE_DECODE_REAL_EXPRESSION = 716,
        BYTE_DECODE_STRING_EXPRESSION = 717,

        // helpers
        FUNCTION_BODY = 74,

        MEMBER_FUNCTION = 80,
        // references
        MACRO_REFERENCE = 82,   // internal expression
        TYPE_REFERENCE = 83,
        UNIT_REFERENCE = 84,
      };

      static ::std::string toString(GroupType grp);
      static ::std::string toString(Expr grp);
      static ::std::string toString(Stmt grp);
      static ::std::string toString(Relation grp);
      static ::std::string toString(Token grp);

      /**
       * Create a constant.
       * @param constant
       * @return an expression that represents the specified constant
       */
      GroupNodeCPtr createExpression(const ConstantPtr &constant,
          const ::idioma::ast::Node::Span &span);

      /**
       * Create the root node
       * @param decls the declarations
       * @return a root node
       */
      GroupNodePtr createRoot(GroupNodes decls);

      /**
       * Create an expression that initializes an object of the specified
       * type with a default.
       */
      GroupNodePtr createDefaultLiteral(ConstrainedTypePtr ty,
          const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteral(ConstantPtr lit, const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteralInteger(const Integer &value,
          const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteralReal(const BigReal &value, const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteralBoolean(bool value, const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteralBit(bool value, const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteralByte(uint32_t value, const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteralChar(const ::std::string &utf8char,
          const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteralString(const ::std::string &utf8String,
          const ::idioma::ast::Node::Span &span);

      GroupNodePtr createFoldArrayExpression(GroupNodeCPtr arrayExpr, GroupNodeCPtr initialValue,
          GroupNodeCPtr foldFn);

      GroupNodePtr createFoldArrayExpression(GroupNodeCPtr arrayExpr, GroupNodeCPtr initialValue,
          ::std::function<GroupNodes(GroupNodeCPtr elem, GroupNodeCPtr accum)

          > foldFn);

      GroupNodePtr createReverseArrayExpression(GroupNodeCPtr arrayExpr);

      GroupNodePtr createFlattenArrayExpression(GroupNodeCPtr arrayExpr);

      GroupNodePtr createPartitionArrayExpression(GroupNodeCPtr arrayExpr,
          GroupNodeCPtr partitionSize);

      GroupNodePtr createPadArrayExpression(GroupNodeCPtr arrayExpr, GroupNodeCPtr padToSize,
          GroupNodeCPtr element);

      GroupNodePtr createTrimArrayExpression(GroupNodeCPtr arrayExpr, GroupNodeCPtr trimToSize);
      GroupNodePtr createDropArrayExpression(GroupNodeCPtr arrayExpr, GroupNodeCPtr count);
      GroupNodePtr createTakeArrayExpression(GroupNodeCPtr arrayExpr, GroupNodeCPtr count);

      GroupNodePtr createMapArrayExpression(GroupNodeCPtr mapFn, GroupNodeCPtr arrayExpr);

      GroupNodePtr createMapArrayExpression(GroupNodeCPtr arrayExpr,
          ::std::function<GroupNodes(GroupNodeCPtr elem)

          > f);

      GroupNodePtr createFilterArrayExpression(GroupNodeCPtr mapFn, GroupNodeCPtr arrayExpr);

      GroupNodePtr createFindArrayExpression(GroupNodeCPtr mapFn, GroupNodeCPtr arrayExpr);

      GroupNodePtr createFindArrayExpression(GroupNodeCPtr mapFn, GroupNodeCPtr arrayExpr,
          GroupNodeCPtr initialIndexExpr);

      GroupNodePtr createFlattenOptionalExpression(GroupNodeCPtr optionalExpr);

      GroupNodePtr createMapOptionalExpression(GroupNodeCPtr mapFn, GroupNodeCPtr optExpr);

      GroupNodePtr createGuardExpression(GroupNodeCPtr cond, GroupNodeCPtr expr,
          GroupNodeCPtr optErrorMessage);

      GroupNodePtr createGuardExpression(GroupNodeCPtr cond, GroupNodeCPtr expr,
          const ::std::string &errMessag);

      GroupNodePtr createGetArrayLengthExpression(GroupNodeCPtr expr);

      GroupNodePtr createGetStringLengthExpression(GroupNodeCPtr expr);

      GroupNodePtr createStringToCharsExpression(GroupNodeCPtr expr);

      GroupNodePtr createCharToStringExpression(GroupNodeCPtr expr);

      GroupNodePtr createStringifyExpression(GroupNodeCPtr expr);

      GroupNodePtr createInterpolateTextExpression(GroupNodes exprs);

      GroupNodePtr createStructFromTupleExpression(GroupNodeCPtr type, GroupNodeCPtr expr);

      GroupNodePtr createLambdaExpression(GroupNodeCPtr type, GroupNodes variables,
          GroupNodeCPtr body);

      GroupNodePtr createLambdaExpression(const mylang::constraints::ConstrainedFunctionType &fnTy,
          const ::idioma::ast::Node::Span &span, ::std::function<GroupNodes(GroupNodes)> body);

      GroupNodePtr createLambdaExpression(const ::std::vector<ConstrainedTypePtr> &params,
          ConstrainedTypePtr retTy, const ::idioma::ast::Node::Span &span,
          ::std::function<GroupNodes(GroupNodes)> body);

      GroupNodePtr createNewProcessExpression(GroupNodeCPtr type, NamePtr globalName,
          GroupNodes argv);

      GroupNodePtr createNewBitExpression(GroupNodeCPtr type, GroupNodes argv);

      GroupNodePtr createNewBooleanExpression(GroupNodeCPtr type, GroupNodes argv);

      GroupNodePtr createNewByteExpression(GroupNodeCPtr type, GroupNodes argv);

      GroupNodePtr createNewCharExpression(GroupNodeCPtr type, GroupNodes argv);

      GroupNodePtr createNewIntegerExpression(GroupNodeCPtr type, GroupNodes argv);

      GroupNodePtr createNewRealExpression(GroupNodeCPtr type, GroupNodes argv);

      GroupNodePtr createNewStringExpression(GroupNodeCPtr type, GroupNodes argv);

      GroupNodePtr createNewFunctionExpression(GroupNodeCPtr type, GroupNodes argv);

      GroupNodePtr createNewArrayExpression(GroupNodeCPtr type, GroupNodes argv);

      GroupNodePtr createNewMutableExpression(GroupNodeCPtr type, GroupNodes argv);

      GroupNodePtr createNewOptionalExpression(GroupNodeCPtr type, GroupNodes argv);

      GroupNodePtr createNewStructExpression(GroupNodeCPtr type, GroupNodes argv);

      GroupNodePtr createNewTupleExpression(GroupNodeCPtr type, GroupNodes argv);

      GroupNodePtr createNewUnionExpression(GroupNodeCPtr type, GroupNodes argv);

      GroupNodePtr createNewUnionExpression(GroupNodeCPtr type, GroupNodeCPtr discriminant,
          GroupNodeCPtr value);

      GroupNodePtr createNewNamedTypeExpression(GroupNodeCPtr type, GroupNodes argv);

      GroupNodePtr createProcessMemberExpression(GroupNodeCPtr expr, TokenNodeCPtr member);

      GroupNodePtr createProcessMemberExpression(GroupNodeCPtr expr, const ::std::string &member);

      GroupNodePtr createUnionMemberExpression(GroupNodeCPtr expr, TokenNodeCPtr member);

      GroupNodePtr createUnionMemberExpression(GroupNodeCPtr expr, const ::std::string &member);

      GroupNodePtr createStructMemberExpression(GroupNodeCPtr expr, TokenNodeCPtr member);

      GroupNodePtr createStructMemberExpression(GroupNodeCPtr expr, const ::std::string &member);

      GroupNodePtr createArrayIndexExpression(GroupNodeCPtr expr, GroupNodeCPtr index);

      GroupNodePtr createStringIndexExpression(GroupNodeCPtr expr, GroupNodeCPtr index);

      GroupNodePtr createArraySubrangeExpression(GroupNodeCPtr expr, GroupNodeCPtr begin,
          GroupNodeCPtr end);

      GroupNodePtr createStringSubrangeExpression(GroupNodeCPtr expr, GroupNodeCPtr begin,
          GroupNodeCPtr end);

      GroupNodePtr createTupleMemberExpression(GroupNodeCPtr expr, GroupNodeCPtr index);

      GroupNodePtr createTupleMemberExpression(GroupNodeCPtr expr, size_t index);

      GroupNodePtr createGetMutableValueExpression(GroupNodeCPtr expr);

      GroupNodePtr createGetOptionalValueExpression(GroupNodeCPtr expr);

      GroupNodePtr createOptifyExpression(GroupNodeCPtr expr);

      GroupNodePtr createGetOptionalValueOrElseExpression(GroupNodeCPtr expr, GroupNodeCPtr orElse);

      GroupNodePtr createZipArraysExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createMergeTuplesExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createConcatenateArraysExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createConcatenateStringsExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createNotExpression(GroupNodeCPtr expr);

      GroupNodePtr createIntegerClampExpression(GroupNodeCPtr type, GroupNodeCPtr expr);

      GroupNodePtr createIntegerClampExpression(
          ::std::shared_ptr<mylang::constraints::ConstrainedIntegerType> type, GroupNodeCPtr expr);

      GroupNodePtr createIntegerWrapExpression(GroupNodeCPtr type, GroupNodeCPtr expr);

      GroupNodePtr createIntegerWrapExpression(
          ::std::shared_ptr<mylang::constraints::ConstrainedIntegerType> type, GroupNodeCPtr expr);

      GroupNodePtr createIntegerNegateExpression(GroupNodeCPtr expr);

      GroupNodePtr createIntegerAddExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createIntegerSubtractExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createIntegerMultiplyExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createIntegerDivideExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createIntegerModulusExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createIntegerRemainderExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createRealNegateExpression(GroupNodeCPtr expr);

      GroupNodePtr createRealAddExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createRealSubtractExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createRealMultiplyExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createRealDivideExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createRealRemainderExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createShortCircuitOrExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createShortCircuitAndExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createAndExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createOrExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createXorExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createComparisonEQExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createComparisonNEQExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createComparisonLTExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createComparisonLTEExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createComparisonGTExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createComparisonGTEExpression(GroupNodeCPtr left, GroupNodeCPtr right);

      GroupNodePtr createGetByteAsUnsignedIntegerExpression(GroupNodeCPtr expr);

      GroupNodePtr createGetByteAsSignedIntegerExpression(GroupNodeCPtr expr);

      GroupNodePtr createGetBitsFromBitExpression(GroupNodeCPtr expr);

      GroupNodePtr createGetBitsFromBooleanExpression(GroupNodeCPtr expr);

      GroupNodePtr createGetBitsFromByteExpression(GroupNodeCPtr expr);

      GroupNodePtr createGetBitsFromCharExpression(GroupNodeCPtr expr);

      GroupNodePtr createGetBitsFromIntegerExpression(GroupNodeCPtr expr);

      GroupNodePtr createGetBitsFromRealExpression(GroupNodeCPtr expr);

      GroupNodePtr createGetBitFromBitsExpression(GroupNodeCPtr expr);

      GroupNodePtr createGetBooleanFromBitsExpression(GroupNodeCPtr expr);

      GroupNodePtr createGetByteFromBitsExpression(GroupNodeCPtr expr);

      GroupNodePtr createGetCharFromBitsExpression(GroupNodeCPtr expr);

      GroupNodePtr createGetIntegerFromBitsExpression(GroupNodeCPtr type, GroupNodeCPtr expr);

      GroupNodePtr createGetRealFromBitsExpression(GroupNodeCPtr type, GroupNodeCPtr expr);

      GroupNodePtr createEncodeBitExpression(GroupNodeCPtr expr);

      GroupNodePtr createEncodeBooleanExpression(GroupNodeCPtr expr);

      GroupNodePtr createEncodeByteExpression(GroupNodeCPtr expr);

      GroupNodePtr createEncodeCharExpression(GroupNodeCPtr expr);

      GroupNodePtr createEncodeIntegerExpression(GroupNodeCPtr expr);

      GroupNodePtr createEncodeRealExpression(GroupNodeCPtr expr);

      GroupNodePtr createEncodeStringExpression(GroupNodeCPtr expr);

      GroupNodePtr createDecodeBitExpression(GroupNodeCPtr expr);

      GroupNodePtr createDecodeBooleanExpression(GroupNodeCPtr expr);

      GroupNodePtr createDecodeByteExpression(GroupNodeCPtr expr);

      GroupNodePtr createDecodeCharExpression(GroupNodeCPtr expr);

      GroupNodePtr createDecodeIntegerExpression(GroupNodeCPtr expr);

      GroupNodePtr createDecodeRealExpression(GroupNodeCPtr expr);

      GroupNodePtr createDecodeStringExpression(GroupNodeCPtr expr);

      GroupNodePtr createTryCatchExpression(GroupNodeCPtr tryExpr, GroupNodeCPtr onErrorExpr);

      GroupNodePtr createGetHeadExpression(GroupNodeCPtr expr);

      GroupNodePtr createGetTailExpression(GroupNodeCPtr expr);

      GroupNodePtr createConvertToTupleExpression(GroupNodeCPtr expr);

      GroupNodePtr createConditionalExpression(GroupNodeCPtr condition, GroupNodeCPtr ifTrue,
          GroupNodeCPtr ifFalse);

      GroupNodePtr createLoopExpression(GroupNodeCPtr state, GroupNodeCPtr condition,
          GroupNodeCPtr body);

      GroupNodePtr createLoopExpression(GroupNodeCPtr initialState,
          ::std::function<GroupNodeCPtr(GroupNodeCPtr ref)

          > condition,

          ::std::function<GroupNodeCPtr(GroupNodeCPtr ref)

          > body);

      GroupNodePtr createWithExpression(GroupNodeCPtr vardecl, GroupNodeCPtr expr);

      GroupNodePtr createWithExpression(GroupNodes variables, GroupNodeCPtr expr);

      GroupNodePtr createWithExpression(GroupNodeCPtr expr,
          const ::std::vector<::std::pair<NamePtr, GroupNodeCPtr>> &variables);

      GroupNodePtr createWithExpression(GroupNodeCPtr wrapExpr,
          ::std::function<GroupNodeCPtr(GroupNodeCPtr ref)

          > wrapper);

      // returns an expression that returns a void value
      GroupNodePtr createVoidExpression();

      GroupNodePtr createNamedExpression(TokenNodeCPtr name, GroupNodeCPtr expr);

      GroupNodePtr createGetAsRootTypeExpression(GroupNodeCPtr expr);

      GroupNodePtr createGetAsBaseTypeExpression(GroupNodeCPtr expr);

      GroupNodeCPtr applyImplicitCast(GroupNodeCPtr toType, GroupNodeCPtr expr);

      GroupNodeCPtr applyImplicitCast(ConstrainedTypePtr toType, GroupNodeCPtr expr);

      GroupNodePtr applyImplicitCast(GroupNodeCPtr toType, GroupNodePtr expr);

      GroupNodePtr applyImplicitCast(ConstrainedTypePtr toType, GroupNodePtr expr);

      GroupNodePtr createImplicitCastExpression(GroupNodeCPtr toType, GroupNodeCPtr expr);

      GroupNodePtr createImplicitCastExpression(ConstrainedTypePtr toType, GroupNodeCPtr expr);

      GroupNodePtr createImplicitBaseTypeCastExpression(GroupNodeCPtr expr);

      GroupNodePtr createCheckedCastExpression(GroupNodeCPtr toType, GroupNodeCPtr expr);

      // NOTE: if the safe-cast involves literals 'nil' or '[]' then they are not casted, but replaced
      // with the corresponding NEW expression
      GroupNodePtr createSafeCastExpression(GroupNodeCPtr toType, GroupNodeCPtr expr);

      GroupNodePtr createUnsafeTypeCastExpression(GroupNodeCPtr toType, GroupNodeCPtr expr);

      GroupNodePtr createFunctionCallExpression(GroupNodeCPtr expr, GroupNodes arguments);

      GroupNodePtr createIsOptionalValuePresentExpression(GroupNodeCPtr expr);

      GroupNodePtr createTransformExpression(GroupNodeCPtr toType, GroupNodes arguments);

      GroupNodePtr createIsInputClosedExpression(GroupNodeCPtr expr);

      GroupNodePtr createIsOutputClosedExpression(GroupNodeCPtr expr);

      GroupNodePtr createIsPortReadableExpression(GroupNodeCPtr expr);

      GroupNodePtr createIsPortWritableExpression(GroupNodeCPtr expr);

      GroupNodePtr createHasNewInputExpression(GroupNodeCPtr expr);

      GroupNodePtr createGetInputExpression(GroupNodeCPtr ref);

      GroupNodePtr createGetOutputExpression(GroupNodeCPtr ref);

      GroupNodePtr createClosePortExpression(GroupNodeCPtr node);

      GroupNodePtr createWaitPortExpression(GroupNodeCPtr node);

      GroupNodePtr createReadPortExpression(GroupNodeCPtr portExpr);

      GroupNodePtr createClearPortExpression(GroupNodeCPtr portExpr);

      GroupNodePtr createWritePortExpression(GroupNodeCPtr portExpr, GroupNodeCPtr valueExpr);

      GroupNodePtr createBlockReadEventsExpression(GroupNodeCPtr port, GroupNodeCPtr enabled);

      GroupNodePtr createBlockWriteEventsExpression(GroupNodeCPtr port, GroupNodeCPtr enabled);

      GroupNodePtr createVariableExpression(TokenNodeCPtr name, const NamePtr &globalName,
          const ConstrainedTypePtr &ty);

      GroupNodePtr createLiteralIntegerExpression(TokenNodeCPtr literal);

      GroupNodePtr createLiteralRealExpression(TokenNodeCPtr literal);

      GroupNodePtr createLiteralBooleanExpression(TokenNodeCPtr literal);

      GroupNodePtr createLiteralBitExpression(TokenNodeCPtr literal);

      GroupNodePtr createLiteralByteExpression(TokenNodeCPtr literal);

      GroupNodePtr createLiteralCharExpression(TokenNodeCPtr literal);

      GroupNodePtr createLiteralStringExpression(TokenNodeCPtr literal);

      GroupNodePtr createLiteralNilExpression(const ::idioma::ast::Node::Span &span);
      GroupNodePtr createLiteralNilExpression(
          const ::std::shared_ptr<const mylang::constraints::ConstrainedOptType> &ty,
          const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteralEmptyArrayExpression(const ::idioma::ast::Node::Span &span);
      GroupNodePtr createLiteralEmptyArrayExpression(
          const ::std::shared_ptr<const mylang::constraints::ConstrainedArrayType> &ty,
          const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteralIntegerExpression(ConstantPtr literal,
          const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteralRealExpression(ConstantPtr literal,
          const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteralBooleanExpression(ConstantPtr literal,
          const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteralBitExpression(ConstantPtr literal,
          const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteralByteExpression(ConstantPtr literal,
          const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteralCharExpression(ConstantPtr literal,
          const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteralStringExpression(ConstantPtr literal,
          const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteralNilExpression(ConstantPtr literal,
          const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteralEmptyArrayExpression(ConstantPtr literal,
          const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteralArrayExpression(GroupNodes elements,
          const ::idioma::ast::Node::Span &span);

      GroupNodePtr createLiteralTupleExpression(GroupNodes elements);

      GroupNodePtr createLiteralTupleMember(GroupNodeCPtr optType, GroupNodeCPtr memberValue);

      GroupNodePtr createLiteralStructExpression(GroupNodes members);

      GroupNodePtr createLiteralStructMember(TokenNodeCPtr memberName, GroupNodeCPtr optType,
          GroupNodeCPtr memberValue);

      GroupNodePtr createDefineNamespace(TokenNodeCPtr name, const NamePtr &globalName,
          GroupNodes members);

      GroupNodePtr createDefineExportedFunction(TokenNodeCPtr name, const NamePtr &globalName,
          GroupNodeCPtr retTy, GroupNodes parameters, GroupNodeCPtr body);

      GroupNodePtr createDefineFunction(TokenNodeCPtr name, const NamePtr &globalName,
          const VariablePtr &vinfo, GroupNodeCPtr retTy, GroupNodes parameters, GroupNodeCPtr body);

      /**
       * Create a function with a given name and a according to some signature.
       * @param name the name of the function
       * @param fnTy the function signature
       * @param body a function that expressions referencing the function arguments and returns a list of statements
       */
      GroupNodePtr createDefineFunction(NamePtr name, const VariablePtr &vinfo,
          const mylang::constraints::ConstrainedFunctionType &fnTy,
          const ::idioma::ast::Node::Span &span, ::std::function<GroupNodes(GroupNodes)> body);

      GroupNodePtr createFunctionBody(GroupNodes statements);

      GroupNodePtr createDefineVariable(TokenNodeCPtr name, const NamePtr &globalName,
          const VariablePtr &vinfo, GroupNodeCPtr optType, GroupNodeCPtr optInitializer);

      GroupNodePtr createDefineType(TokenNodeCPtr name, const NamePtr &globalName,
          GroupNodeCPtr type);

      GroupNodePtr createDefineProcess(TokenNodeCPtr name, const NamePtr &globalName,
          GroupNodeCPtr type, GroupNodes statements);

      GroupNodePtr createProcessBlock(TokenNodeCPtr name, const NamePtr &globalName,
          GroupNodeCPtr condition, GroupNodeCPtr block);

      GroupNodePtr createProcessConstructor(const NamePtr &constructorName, GroupNodes parameters,
          GroupNodeCPtr callOwnConstructor, GroupNodeCPtr constructorBlock);

      GroupNodePtr createTypeReference(TokenNodeCPtr name, const NamePtr &globalName,
          const ConstrainedTypePtr &type);

      GroupNodePtr createUnitReference(TokenNodeCPtr name);

      GroupNodePtr createStatementBlock(GroupNodes statements);

      GroupNodePtr createStatementList(GroupNodes statements);

      GroupNodePtr createAssertStatement(GroupNodeCPtr condition, GroupNodeCPtr optError);

      GroupNodePtr createAssertStatement(GroupNodeCPtr condition, const ::std::string &errMessage);

      GroupNodePtr createBreakStatement();

      GroupNodePtr createContinueStatement();

      GroupNodePtr createCallConstructorStatement(GroupNodes args);

      GroupNodePtr createExpressionStatement(GroupNodeCPtr voidexpr);

      GroupNodePtr createForeachStatement(GroupNodeCPtr varDecl, GroupNodeCPtr expression,
          GroupNodeCPtr block);

      GroupNodePtr createIfStatement(GroupNodeCPtr condition, GroupNodeCPtr iftrue,
          GroupNodeCPtr iffalse);

      GroupNodePtr createLogStatement(TokenNodeCPtr level, GroupNodeCPtr message, GroupNodes data);

      GroupNodePtr createReturnStatement(GroupNodeCPtr expr);

      GroupNodePtr createSetStatement(GroupNodeCPtr ref, GroupNodeCPtr val);

      GroupNodePtr createThrowStatement();

      GroupNodePtr createTryStatement(GroupNodeCPtr tryBranch, GroupNodeCPtr catchBranch);

      GroupNodePtr createUpdateStatement(GroupNodeCPtr ref, GroupNodeCPtr val);

      GroupNodePtr createWaitStatement(GroupNodes ports);

      GroupNodePtr createWhileStatement(GroupNodeCPtr condition, GroupNodeCPtr block);

      /**
       * Create a placeholder for where a generic function can be instantiated
       */
      GroupNodePtr createDefineGenericFunction(TokenNodeCPtr name);

      /**
       * Instantiate a generic function
       * @param defName the name of the logical generic function define (see createDefineGenericFunction)
       * @param instanceName the name of the instance
       * @param fnDef a function definition
       * @return a node that acts like a variable definition
       */
      GroupNodePtr createInstantiateGenericFunction(TokenNodeCPtr defName,
          TokenNodeCPtr instanceName, GroupNodeCPtr fnDef);

      /**
       * Mark a location where an instance of a generic function is required, but it's definition
       * has not yet occurred.
       * @param fnTy the function type
       * @param defName the name of the logical generic function define (see createDefineGenericFunction)
       * @param instanceName the name of the instance
       * @return a node that acts like a variable definition
       */
      GroupNodePtr createInstantiateGenericFunction(GroupNodeCPtr fnTy, TokenNodeCPtr defName,
          TokenNodeCPtr instanceName);

      /**
       * Create a null node
       */
      GroupNodeCPtr createInlineGroup(GroupNodes nodes);

      /**
       * Create a new type node. The will be annotated with the specified type
       * constraint.
       * @return a node that represents a type constraint
       */
      GroupNodePtr createType(const ConstrainedTypePtr &type, const ::idioma::ast::Node::Span &span,
          const ::std::vector<idioma::ast::GroupNode::Rel> &rels = ::std::vector<
              idioma::ast::GroupNode::Rel>());

      GroupNodeCPtr castToBaseType(GroupNodeCPtr expr);

      GroupNodeCPtr castToRootType(GroupNodeCPtr expr);

      GroupNodeCPtr createNewObject(GroupNodeCPtr type, GroupNodes argv);

    public:
      static TokenNodeCPtr getToken(const GroupNodeCPtr &g, const VAst::Token &token);

      static ::std::vector<TokenNodeCPtr> getTokens(const GroupNodeCPtr &g,
          const VAst::Token &token);

      static GroupNodeCPtr getExpr(const GroupNodeCPtr &g, const VAst::Expr &rel);

      static GroupNodes getExprs(const GroupNodeCPtr &g, const VAst::Expr &rel);

      static GroupNodeCPtr getStmt(const GroupNodeCPtr &g, const VAst::Stmt &rel);

      static GroupNodes getStmts(const GroupNodeCPtr &g, const VAst::Stmt &rel);

      static GroupNodeCPtr getRelation(const GroupNodeCPtr &g, const VAst::Relation &rel);

      static GroupNodes getRelations(const GroupNodeCPtr &g, const VAst::Relation &rel);

      /**
       * Find a top-level definition with the specified name.
       * @param root the root of an AST
       * @return the definition of the node with the specified fqn
       */
      static GroupNodeCPtr findDefinition(const GroupNodeCPtr &root,
          const ::mylang::names::FQN &fqn);
    };
  }
}
#endif
