#ifndef CLASS_MYLANG_VAST_TRANSFORMS_ADDDEFAULTCONSTRUCTORS_H
#define CLASS_MYLANG_VAST_TRANSFORMS_ADDDEFAULTCONSTRUCTORS_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_VALIDATION_VASTTRANSFORM_H
#include <mylang/vast/VAstTransform.h>
#endif

namespace mylang {
  namespace vast {
    namespace transforms {

      /**
       * Adds default constructors where none exist.
       */
      class AddDefaultConstructors: public VAstTransform
      {
        /**
         * Create a code builder.
         */
      public:
        AddDefaultConstructors();

        /**
         * Destructor
         */
      public:
        ~AddDefaultConstructors();

        /**
         * Add guards to all nodes recursively. If ignoreGuards
         * is true, then nodes below a guard expression are ignored.
         * @param node a node
         * @return a new node or the same if no changes were made
         */
      public:
        static GroupNodeCPtr apply(const GroupNodeCPtr &node);

      private:
        GroupNodeCPtr visit_def_process(GroupNodeCPtr node) override;

      };
    }
  }
}
#endif
