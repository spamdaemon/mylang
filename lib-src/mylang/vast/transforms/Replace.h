#ifndef CLASS_MYLANG_VAST_TRANSFORMS_REPLACE_H
#define CLASS_MYLANG_VAST_TRANSFORMS_REPLACE_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_VALIDATION_VASTTRANSFORM_H
#include <mylang/vast/VAstTransform.h>
#endif

namespace mylang {
  namespace vast {
    namespace transforms {

      /**
       * Replace occurrences of certain node types with replacement nodes. If a node type
       * is replaced, it will not appear anywhere in the AST. The purpose of this transform
       * is to reduce the number of types of nodes in the AST.
       */
      class Replace: public VAstTransform
      {

        /**
         * Create a code builder.
         */
      public:
        Replace();

        /**
         * Destructor
         */
      public:
        ~Replace();

        /**
         * Rewrite the specified node.
         * @param node a node
         * @return a rewritten node, or node if no rewrite was performed.
         */
      public:
        static GroupNodeCPtr apply(const GroupNodeCPtr &node);

        /** The nodes that are simplified */
        GroupNodeCPtr visit_literal_array(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_literal_tuple(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_literal_struct(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_new_bit_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_new_boolean_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_new_char_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_new_integer_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_new_real_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_new_string_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_new_function_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_short_circuit_and_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_short_circuit_or_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_comparison_eq_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_comparison_neq_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_comparison_lte_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_comparison_gt_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_comparison_gte_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_optify_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_merge_tuples_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_orelse_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_zip_arrays_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_fold_array_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_map_array_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_filter_array_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_find_array_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_reverse_array_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_partition_array_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_flatten_array_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_map_optional_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_flatten_optional_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_get_bit_from_bits_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_get_boolean_from_bits_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_get_byte_from_bits_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_get_char_from_bits_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_get_real_from_bits_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_get_bits_from_bit_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_get_bits_from_boolean_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_get_bits_from_byte_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_get_bits_from_char_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_get_bits_from_real_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_interpolate_text_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_encode_bit_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_encode_boolean_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_encode_byte_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_decode_bit_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_decode_boolean_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_decode_byte_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_integer_clamp_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_integer_wrap_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_convert_to_tuple_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_struct_from_tuple_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_get_head_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_get_tail_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_get_as_roottype_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_foreach_statement(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit_assert_statement(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit(GroupNodeCPtr node) override final;

      private:
        GroupNodeCPtr replace(GroupNodeCPtr node);
      };
    }
  }
}
#endif
