#include <mylang/vast/transforms/RegisterProcessEvents.h>
#include <mylang/constraints.h>
#include <mylang/variables/Variable.h>
#include <mylang/annotations.h>
#include <idioma/ast/GroupNode.h>

namespace mylang {
  namespace vast {
    namespace transforms {

      RegisterProcessEvents::RegisterProcessEvents()
      {
      }

      RegisterProcessEvents::~RegisterProcessEvents()
      {
      }

      GroupNodeCPtr RegisterProcessEvents::visit_def_process(GroupNodeCPtr node)
      {
        GroupNodeCPtr res;
        GroupNodeCPtr proc = currentProcess;
        currentProcess = node;
        res = VAstTransform::visit_def_process(node);
        currentProcess = proc;
        return res;
      }

      GroupNodeCPtr RegisterProcessEvents::visit_process_constructor(GroupNodeCPtr node)
      {
        // if we have an call to another another constructor then we don't need to do anything
        // for this constructor
        if (VAst::getRelation(node, VAst::Relation::INIT)) {
          return VAstTransform::visit_process_constructor(node);
        }

        // insert an unblock statement for each input
        GroupNodes statements;
        for (auto s : VAst::getRelations(currentProcess, VAst::Relation::STATEMENTS)) {
          if (s->hasType(VAst::GroupType::DEF_VARIABLE)) {
            auto vinfo = getVariableAnnotation(s);
            if (vinfo && vinfo->kind == mylang::variables::Variable::KIND_INPUT) {
              auto name = getNameAnnotation(s);
              auto id = VAst::getToken(s, VAst::Token::IDENTIFIER);
              auto ref = ast.createVariableExpression(id, name, getConstraintAnnotation(s));
              auto unblock = ast.createLiteralBoolean(false, s->span());
              statements.push_back(
                  ast.createExpressionStatement(ast.createBlockReadEventsExpression(ref, unblock)));
            }
          }
        }

        if (statements.empty()) {
          return VAstTransform::visit_process_constructor(node);
        }

        statements.push_back(VAst::getRelation(node, VAst::Relation::BODY));

        auto ctor = ast.createProcessConstructor(getNameAnnotation(node),
            VAst::getRelations(node, VAst::Relation::PARAMETERS), nullptr,
            ast.createStatementBlock(statements));

        auto res = VAstTransform::visit_process_constructor(ctor);
        return res ? res : ctor;
      }

      GroupNodeCPtr RegisterProcessEvents::apply(const GroupNodeCPtr &node)
      {
        RegisterProcessEvents rw;
        GroupNodeCPtr res = rw.visit(node);
        return res ? res : node;
      }
    }
  }
}
