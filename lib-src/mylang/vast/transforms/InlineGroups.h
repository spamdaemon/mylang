#ifndef CLASS_MYLANG_VAST_TRANSFORMS_INLINEGROUPS_H
#define CLASS_MYLANG_VAST_TRANSFORMS_INLINEGROUPS_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

namespace mylang {
  namespace vast {
    namespace transforms {

      /**
       * Move generic instantiations to their proper spot in the AST.
       */
      class InlineGroups
      {

        /**
         * Create a code builder.
         */
      public:
        InlineGroups();

        /**
         * Destructor
         */
      public:
        ~InlineGroups();

        /**
         * Apply the normalization to the specified node.
         * @param node a node
         * @return a normalized node
         */
      public:
        static GroupNodeCPtr apply(const GroupNodeCPtr &node);
      };
    }
  }
}
#endif
