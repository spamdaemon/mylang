#include <idioma/ast/GroupNode.h>
#include <idioma/ast/TokenNode.h>
#include <mylang/annotations.h>
#include <mylang/constraints/ConstrainedArrayType.h>
#include <mylang/constraints/ConstrainedCharType.h>
#include <mylang/constraints/ConstrainedIntegerType.h>
#include <mylang/constraints/ConstrainedNamedType.h>
#include <mylang/constraints/ConstrainedOptType.h>
#include <mylang/constraints/ConstrainedStringType.h>
#include <mylang/constraints/ConstrainedStructType.h>
#include <mylang/constraints/ConstrainedTupleType.h>
#include <mylang/constraints/ConstrainedType.h>
#include <mylang/names/Name.h>
#include <mylang/variables/Variable.h>
#include <mylang/vast/transforms/Loops.h>
#include <mylang/vast/transforms/Replace.h>
#include <mylang/vast/transforms/Temporary.h>
#include <mylang/vast/VAst.h>
#include <mylang/vast/VAstVisitor.h>
#include <cstddef>
#include <functional>
#include <iterator>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <vector>

namespace mylang {
  namespace vast {
    namespace transforms {

      using namespace mylang::constraints;

      Replace::Replace()
      {
      }

      Replace::~Replace()
      {
      }

      GroupNodeCPtr Replace::visit(GroupNodeCPtr node)
      {
        GroupNodeCPtr rewritten = VAstTransform::visit(node);
#if 0
                if (rewritten) {
                  auto fromkey = node->type.value<VAst::GroupType>();
                  auto tokey = rewritten->type.value<VAst::GroupType>();
                  ::std::cerr << "REWRITE " << (int) fromkey->key << " -> " << (int) tokey->key
                      << ::std::endl;
                }
#endif
        return rewritten;
      }

      GroupNodeCPtr Replace::replace(GroupNodeCPtr node)
      {
        auto res = visit(node);
        return res ? res : node;
      }

      GroupNodeCPtr Replace::visit_literal_array(GroupNodeCPtr node)
      {
        auto ty = getConstraintAnnotation(node);
        auto res = ast.createNewArrayExpression(ast.createType(ty, node->span()),
            VAst::getExprs(node, VAst::Expr::EXPRESSIONS));
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_literal_tuple(GroupNodeCPtr node)
      {
        auto ty = getConstraintAnnotation(node);
        ::std::vector<GroupNodeCPtr> members;
        for (auto m : VAst::getRelations(node, VAst::Relation::MEMBERS)) {
          auto e = VAst::getExpr(m, VAst::Expr::EXPRESSION);
          members.push_back(e);
        }

        auto res = ast.createNewTupleExpression(ast.createType(ty, node->span()), members);
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_literal_struct(GroupNodeCPtr node)
      {
        auto ty = getConstraintAnnotation(node)->self<ConstrainedStructType>();
        GroupNodes members;

        ::std::map<::std::string, GroupNodeCPtr> memberValues;
        for (auto m : VAst::getRelations(node, VAst::Relation::MEMBERS)) {
          auto n = VAst::getToken(m, VAst::Token::IDENTIFIER)->text;
          auto e = VAst::getExpr(m, VAst::Expr::EXPRESSION);
          memberValues[n] = e;
        }
        for (auto m : ty->members) {
          members.push_back(memberValues[m.name]);
        }

        auto res = ast.createNewStructExpression(ast.createType(ty, node->span()), members);
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_new_bit_expression(GroupNodeCPtr node)
      {
        GroupNodeCPtr res;
        auto arg = VAst::getExpr(node, VAst::Expr::ARGUMENTS);
        auto intTy = getConstraintAnnotation(arg)->self<ConstrainedIntegerType>();
        if (intTy) {
          auto cond = ast.createComparisonEQExpression(arg,
              ast.createLiteralInteger(Integer::ZERO(), node->span()));
          res = ast.createConditionalExpression(cond, ast.createLiteralBit(0, node->span()),
              ast.createLiteralBit(1, node->span()));
        } else {
          res = arg;
        }
        // only copying allowed, but that is a no-op
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_new_boolean_expression(GroupNodeCPtr node)
      {
        // only copying allowed, but that is a no-op
        auto res = VAst::getExpr(node, VAst::Expr::ARGUMENTS);
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_new_char_expression(GroupNodeCPtr node)
      {
        // only copying allowed, but that is a no-op
        auto res = VAst::getExpr(node, VAst::Expr::ARGUMENTS);
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_new_integer_expression(GroupNodeCPtr node)
      {
        // only copying allowed, but that is a no-op
        auto res = VAst::getExpr(node, VAst::Expr::ARGUMENTS);
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_new_real_expression(GroupNodeCPtr node)
      {
        auto res = VAst::getExpr(node, VAst::Expr::ARGUMENTS);
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_new_string_expression(GroupNodeCPtr node)
      {
        // only copying allowed, but that is a no-op
        auto arg = VAst::getExpr(node, VAst::Expr::ARGUMENTS);
        auto newArg = replace(arg);

        auto stringTy = getConstraintAnnotation(newArg)->self<ConstrainedStringType>();
        if (stringTy) {
          return newArg; // new string(string x) ==> x
        }
        if (getConstraintAnnotation(newArg)->self<ConstrainedCharType>()) {
          return ast.createCharToStringExpression(newArg);
        }
        if (arg != newArg) {
          auto ty = VAst::getRelation(node, VAst::Relation::TYPE);
          // argument has been replaced,
          return ast.createNewStringExpression(ty, { newArg });
        }

        // no change
        return nullptr;
      }

      GroupNodeCPtr Replace::visit_new_function_expression(GroupNodeCPtr node)
      {
        auto res = VAst::getExpr(node, VAst::Expr::ARGUMENTS);
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_short_circuit_and_expression(GroupNodeCPtr node)
      {
        auto lhs = VAst::getExpr(node, VAst::Expr::LHS);
        auto rhs = VAst::getExpr(node, VAst::Expr::RHS);
        auto res = ast.createConditionalExpression(lhs, rhs,
            ast.createLiteralBoolean(false, node->span()));
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_short_circuit_or_expression(GroupNodeCPtr node)
      {
        auto lhs = VAst::getExpr(node, VAst::Expr::LHS);
        auto rhs = VAst::getExpr(node, VAst::Expr::RHS);
        auto res = ast.createConditionalExpression(lhs,
            ast.createLiteralBoolean(true, node->span()), rhs);
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_comparison_eq_expression(GroupNodeCPtr node)
      {
        auto lhs = VAst::getExpr(node, VAst::Expr::LHS);
        auto rhs = VAst::getExpr(node, VAst::Expr::RHS);

        auto lhsTy = getConstraintAnnotation(lhs);
        auto rhsTy = getConstraintAnnotation(rhs);

        GroupNodeCPtr res;

        // for named types, just compare the root types
        if (lhsTy->isSameConstraint(*rhsTy)) {
          if (lhsTy->self<ConstrainedNamedType>()) {
            lhs = ast.applyImplicitCast(lhsTy->roottype(), lhs);
            rhs = ast.applyImplicitCast(rhsTy->roottype(), rhs);
            res = ast.createComparisonEQExpression(lhs, rhs);
            res = replace(res);
          }
        } else if (lhsTy->isBaseTypeOf(*rhsTy) || rhsTy->isBaseTypeOf(*lhsTy)) {
          lhs = ast.applyImplicitCast(lhsTy->roottype(), lhs);
          rhs = ast.applyImplicitCast(rhsTy->roottype(), rhs);
          res = ast.createComparisonEQExpression(lhs, rhs);
          res = replace(res);
        }

        if (!res) {
          res = VAstTransform::visit_comparison_eq_expression(node);
        }
        return res;
      }

      GroupNodeCPtr Replace::visit_comparison_neq_expression(GroupNodeCPtr node)
      {
        auto lhs = VAst::getExpr(node, VAst::Expr::LHS);
        auto rhs = VAst::getExpr(node, VAst::Expr::RHS);
        auto res = ast.createNotExpression(ast.createComparisonEQExpression(lhs, rhs));
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_comparison_lte_expression(GroupNodeCPtr node)
      {
        auto lhs = VAst::getExpr(node, VAst::Expr::LHS);
        auto rhs = VAst::getExpr(node, VAst::Expr::RHS);
        auto res = ast.createNotExpression(ast.createComparisonGTExpression(lhs, rhs));
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_comparison_gt_expression(GroupNodeCPtr node)
      {
        auto lhs = VAst::getExpr(node, VAst::Expr::LHS);
        auto rhs = VAst::getExpr(node, VAst::Expr::RHS);
        auto res = ast.createComparisonLTExpression(rhs, lhs);
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_comparison_gte_expression(GroupNodeCPtr node)
      {
        auto lhs = VAst::getExpr(node, VAst::Expr::LHS);
        auto rhs = VAst::getExpr(node, VAst::Expr::RHS);
        auto res = ast.createNotExpression(ast.createComparisonLTExpression(lhs, rhs));
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_optify_expression(GroupNodeCPtr node)
      {
        auto ty = ast.createType(getConstraintAnnotation(node), node->span());
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto res = ast.createNewOptionalExpression(ty, { expr });
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_merge_tuples_expression(GroupNodeCPtr node)
      {
        GroupNodes exprs;
        auto ty = getConstraintAnnotation(node);
        auto left = VAst::getExpr(node, VAst::Expr::LHS);
        auto right = VAst::getExpr(node, VAst::Expr::RHS);

        return ast.createWithExpression(left, [&](GroupNodeCPtr lhs) {
          return ast.createWithExpression(right, [&](GroupNodeCPtr rhs) {
                auto lty = getConstraintAnnotation(lhs)->self<ConstrainedTupleType>();
                for (size_t i = 0; i < lty->types.size(); ++i) {
                  exprs.push_back(ast.createTupleMemberExpression(lhs, i));
                }
                auto rty = getConstraintAnnotation(rhs)->self<ConstrainedTupleType>();
                for (size_t i = 0; i < rty->types.size(); ++i) {
                  exprs.push_back(ast.createTupleMemberExpression(rhs, i));
                }
                auto res = ast.createNewTupleExpression(ast.createType(ty, node->span()), exprs);
                return replace(res);
              });
        });
      }

      GroupNodeCPtr Replace::visit_orelse_expression(GroupNodeCPtr node)
      {
        auto left = VAst::getExpr(node, VAst::Expr::LHS);
        auto right = VAst::getExpr(node, VAst::Expr::RHS);

        return ast.createWithExpression(left, [&](GroupNodeCPtr lhs) {
          auto ifPresent = ast.createIsOptionalValuePresentExpression(lhs);
          auto iftrue = ast.createGetOptionalValueExpression(lhs);
          auto iffalse = right;

          auto res = ast.createConditionalExpression(ifPresent, iftrue, iffalse);
          return replace(res);
        });
      }

      GroupNodeCPtr Replace::visit_zip_arrays_expression(GroupNodeCPtr node)
      {
        auto ty = getConstraintAnnotation(node)->self<ConstrainedArrayType>();
        auto left = VAst::getExpr(node, VAst::Expr::LHS);
        auto right = VAst::getExpr(node, VAst::Expr::RHS);
        auto indexTy = ty->counter;
        if (ty->index == nullptr) {
          return ast.createLiteralEmptyArrayExpression(ty, node->span());
        }
        auto indexType = ast.createType(indexTy, node->span());
        auto loopStateTy = ConstrainedTupleType::get( { indexTy,
            ConstrainedArrayType::getUnboundedArray(ty->element) });
        auto initialLoopState = ast.createNewTupleExpression(
            ast.createType(loopStateTy, node->span()),
            { ast.createLiteralInteger(Integer::ZERO(), node->span()), ast.createNewArrayExpression(
                ast.createType(loopStateTy->types[1], node->span()), { }) });

        return ast.createWithExpression(left, [&](GroupNodeCPtr lhs) {
          return ast.createWithExpression(right, [&](GroupNodeCPtr rhs) {
                return ast.createWithExpression(
                    ast.createGetArrayLengthExpression(lhs),
                    [&](GroupNodeCPtr len) {

                      Temporary tmp(initialLoopState);
                      auto i = ast.createUnsafeTypeCastExpression(
                          indexType,
                          ast.createTupleMemberExpression(tmp.ref,
                              0));
                      auto yield = ast.createTupleMemberExpression(
                          tmp.ref, 1);

                      auto cond = ast.createComparisonLTExpression(i,
                          len);
                      auto zip = ast.createNewTupleExpression(
                          ast.createType(ty->element, node->span()),
                          { ast.createArrayIndexExpression(lhs, i),
                            ast.createArrayIndexExpression(rhs, i)});
                      auto inc = ast.createUnsafeTypeCastExpression(
                          indexType,
                          ast.createIntegerAddExpression(i,
                              ast.createLiteralInteger(
                                  1,
                                  node->span())));

                      auto update = ast.createNewTupleExpression(
                          ast.createType(loopStateTy, node->span()),
                          { inc, ast.createConcatenateArraysExpression(
                                yield,
                                ast.createLiteralArrayExpression( {
                                      zip},
                                    node->span()))});

                      auto doLoop = ast.createLoopExpression(tmp.decl,
                          cond,
                          update);
                      auto res = ast.createTupleMemberExpression(doLoop,
                          1);
                      return replace(res);
                    });
              });
        });
      }

      GroupNodeCPtr Replace::visit_fold_array_expression(GroupNodeCPtr node)
      {
        auto ty = getConstraintAnnotation(node);
        auto arrExpr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto arrTy = getConstraintAnnotation(arrExpr)->self<ConstrainedArrayType>();

        auto init = VAst::getExpr(node, VAst::Expr::INITIALIZER);
        auto fnExpr = VAst::getExpr(node, VAst::Expr::FUNCTION);

        if (arrTy->index == nullptr) {
          return init;
        }

        auto indexTy = arrTy->counter;
        auto indexType = ast.createType(indexTy, node->span());
        auto loopStateTy = ConstrainedTupleType::get( { indexTy, ty });

        auto initialLoopState = ast.createNewTupleExpression(
            ast.createType(loopStateTy, node->span()),
            { ast.createLiteralInteger(Integer::ZERO(), node->span()), init });

        Temporary tmp(initialLoopState);
        return ast.createWithExpression(arrExpr, [&](GroupNodeCPtr arr) {
          return ast.createWithExpression(fnExpr, [&](GroupNodeCPtr fn) {
                return ast.createWithExpression(
                    ast.createGetArrayLengthExpression(arr),
                    [&](GroupNodeCPtr len) {

                      auto i = ast.createUnsafeTypeCastExpression(
                          indexType,
                          ast.createTupleMemberExpression(tmp.ref,
                              0));
                      auto accum = ast.createTupleMemberExpression(
                          tmp.ref, 1);

                      auto cond = ast.createComparisonLTExpression(i,
                          len);
                      auto apply = ast.createFunctionCallExpression(fn,
                          { ast.createArrayIndexExpression(
                                arr,
                                i),
                            accum});
                      auto inc = ast.createUnsafeTypeCastExpression(
                          indexType,
                          ast.createIntegerAddExpression(i,
                              ast.createLiteralInteger(
                                  1,
                                  node->span())));

                      auto update = ast.createNewTupleExpression(
                          ast.createType(loopStateTy, node->span()),
                          { inc, apply});

                      auto doLoop = ast.createLoopExpression(tmp.decl,
                          cond,
                          update);
                      auto res = ast.createTupleMemberExpression(doLoop,
                          1);
                      return replace(res);
                    });
              });
        });
      }

      GroupNodeCPtr Replace::visit_map_array_expression(GroupNodeCPtr node)
      {
        auto ty = getConstraintAnnotation(node)->self<ConstrainedArrayType>();
        auto arrExpr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto arrTy = getConstraintAnnotation(arrExpr)->self<ConstrainedArrayType>();

        if (ty->index == nullptr) {
          return ast.createLiteralEmptyArrayExpression(ty, node->span());
        }
        auto fnExpr = VAst::getExpr(node, VAst::Expr::FUNCTION);
        auto indexTy = arrTy->counter;
        auto indexType = ast.createType(indexTy, node->span());
        auto loopStateTy = ConstrainedTupleType::get( { indexTy,
            ConstrainedArrayType::getUnboundedArray(ty->element) });

        auto initialLoopState = ast.createNewTupleExpression(
            ast.createType(loopStateTy, node->span()),
            { ast.createLiteralInteger(Integer::ZERO(), node->span()), ast.createNewArrayExpression(
                ast.createType(loopStateTy->types[1], node->span()), { }) });

        Temporary tmp(initialLoopState);
        return ast.createWithExpression(arrExpr, [&](GroupNodeCPtr arr) {
          return ast.createWithExpression(fnExpr, [&](GroupNodeCPtr fn) {
                return ast.createWithExpression(
                    ast.createGetArrayLengthExpression(arr),
                    [&](GroupNodeCPtr len) {

                      auto i = ast.createUnsafeTypeCastExpression(
                          indexType,
                          ast.createTupleMemberExpression(tmp.ref,
                              0));
                      auto yield = ast.createTupleMemberExpression(
                          tmp.ref, 1);
                      auto inc = ast.createUnsafeTypeCastExpression(
                          indexType,
                          ast.createIntegerAddExpression(i,
                              ast.createLiteralInteger(
                                  1,
                                  node->span())));

                      auto cond = ast.createComparisonLTExpression(i,
                          len);
                      auto apply = ast.createFunctionCallExpression(fn,
                          { ast.createArrayIndexExpression(
                                arr,
                                i)});

                      auto updated_array = ast.createConcatenateArraysExpression(
                          yield, ast.createLiteralArrayExpression( {
                                apply},
                              node->span()));

                      auto update = ast.createNewTupleExpression(
                          ast.createType(loopStateTy, node->span()),
                          { inc, updated_array});

                      auto doLoop = ast.createLoopExpression(tmp.decl,
                          cond,
                          update);
                      auto res = ast.createTupleMemberExpression(doLoop,
                          1);
                      return replace(res);
                    });
              });
        });
      }

      GroupNodeCPtr Replace::visit_filter_array_expression(GroupNodeCPtr node)
      {
        auto ty = getConstraintAnnotation(node)->self<ConstrainedArrayType>();
        auto arrExpr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto arrTy = getConstraintAnnotation(arrExpr)->self<ConstrainedArrayType>();

        auto fnExpr = VAst::getExpr(node, VAst::Expr::FUNCTION);
        if (ty->index == nullptr) {
          return ast.createLiteralEmptyArrayExpression(ty, node->span());
        }

        auto indexTy = arrTy->counter;
        auto indexType = ast.createType(indexTy, node->span());
        auto loopStateTy = ConstrainedTupleType::get( { indexTy,
            ConstrainedArrayType::getUnboundedArray(ty->element) });

        auto initialLoopState = ast.createNewTupleExpression(
            ast.createType(loopStateTy, node->span()),
            { ast.createLiteralInteger(Integer::ZERO(), node->span()), ast.createNewArrayExpression(
                ast.createType(loopStateTy->types[1], node->span()), { }) });

        Temporary tmp(initialLoopState);
        return ast.createWithExpression(arrExpr, [&](GroupNodeCPtr arr) {
          return ast.createWithExpression(fnExpr, [&](GroupNodeCPtr fn) {
                return ast.createWithExpression(
                    ast.createGetArrayLengthExpression(arr),
                    [&](GroupNodeCPtr len) {

                      auto i = ast.createUnsafeTypeCastExpression(
                          indexType,
                          ast.createTupleMemberExpression(tmp.ref,
                              0));
                      auto yield = ast.createTupleMemberExpression(
                          tmp.ref, 1);
                      auto inc = ast.createUnsafeTypeCastExpression(
                          indexType,
                          ast.createIntegerAddExpression(i,
                              ast.createLiteralInteger(
                                  1,
                                  node->span())));

                      auto cond = ast.createComparisonLTExpression(i,
                          len);
                      auto value = ast.createArrayIndexExpression(arr, i);
                      auto apply = ast.createFunctionCallExpression(fn,
                          { value});

                      GroupNodeCPtr updated_array =
                      ast.createConditionalExpression(apply,
                          ast.createConcatenateArraysExpression(
                              yield,
                              ast.createLiteralArrayExpression(
                                  { value},
                                  node->span())),
                          yield);

                      auto update = ast.createNewTupleExpression(
                          ast.createType(loopStateTy, node->span()),
                          { inc, updated_array});

                      auto doLoop = ast.createLoopExpression(tmp.decl,
                          cond,
                          update);
                      auto res = ast.createTupleMemberExpression(doLoop,
                          1);
                      return replace(res);
                    });
              });
        });
      }

      GroupNodeCPtr Replace::visit_reverse_array_expression(GroupNodeCPtr node)
      {
        auto ty = getConstraintAnnotation(node)->self<ConstrainedOptType>();
        auto arrExpr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto arrTy = getConstraintAnnotation(arrExpr)->self<ConstrainedArrayType>();

        auto accumTy = arrTy->getEmptyableArray();
        auto initialValue = ast.applyImplicitCast(accumTy,
            ast.createLiteralEmptyArrayExpression(arrExpr->span()));

        auto res = ast.createFoldArrayExpression(arrExpr, initialValue,
            [&](GroupNodeCPtr value, GroupNodeCPtr accum) {
              GroupNodes e;
              auto v = ast.createLiteralArrayExpression( {value},
                  value->span());
              e.push_back(ast.createReturnStatement(
                      ast.applyImplicitCast(accumTy,
                          ast.createConcatenateArraysExpression(
                              v, accum))));
              return e;
            });

        return replace(res);
      }

      GroupNodeCPtr Replace::visit_find_array_expression(GroupNodeCPtr node)
      {
        auto ty = getConstraintAnnotation(node)->self<ConstrainedOptType>();
        auto arrExpr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto arrTy = getConstraintAnnotation(arrExpr)->self<ConstrainedArrayType>();
        auto initialIndexExpr = VAst::getExpr(node, VAst::Expr::INDEX);

        auto fnExpr = VAst::getExpr(node, VAst::Expr::FUNCTION);
        if (arrTy->index == nullptr) {
          return ast.createLiteralNilExpression(node->span());
        }
        auto indexTy = arrTy->counter;
        auto indexType = ast.createType(indexTy, node->span());
        auto loopStateTy = ConstrainedTupleType::get( { indexTy, ty });

        auto initialLoopState = ast.createNewTupleExpression(
            ast.createType(loopStateTy, node->span()),
            { initialIndexExpr, ast.createLiteralNilExpression(node->span()) });

        Temporary tmp(initialLoopState);
        return ast.createWithExpression(arrExpr, [&](GroupNodeCPtr arr) {
          return ast.createWithExpression(fnExpr, [&](GroupNodeCPtr fn) {
                return ast.createWithExpression(
                    ast.createGetArrayLengthExpression(arr),
                    [&](GroupNodeCPtr len) {

                      auto i = ast.createUnsafeTypeCastExpression(
                          indexType,
                          ast.createTupleMemberExpression(tmp.ref,
                              0));
                      auto yield = ast.createTupleMemberExpression(
                          tmp.ref, 1);
                      auto inc = ast.createUnsafeTypeCastExpression(
                          indexType,
                          ast.createIntegerAddExpression(i,
                              ast.createLiteralInteger(
                                  1,
                                  node->span())));

                      auto cond = ast.createAndExpression(
                          ast.createComparisonLTExpression(i, len),
                          ast.createNotExpression(
                              ast.createIsOptionalValuePresentExpression(
                                  yield)));

                      auto value = ast.createArrayIndexExpression(arr, i);
                      auto apply = ast.createFunctionCallExpression(fn,
                          { value});

                      GroupNodeCPtr result =
                      ast.createConditionalExpression(apply,
                          ast.createOptifyExpression(
                              i),
                          yield);

                      auto update = ast.createNewTupleExpression(
                          ast.createType(loopStateTy, node->span()),
                          { inc, result});

                      auto doLoop = ast.createLoopExpression(tmp.decl,
                          cond,
                          update);
                      auto res = ast.createTupleMemberExpression(doLoop,
                          1);
                      return replace(res);
                    });
              });
        });
      }

      GroupNodeCPtr Replace::visit_map_optional_expression(GroupNodeCPtr node)
      {
        auto optExpr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto fnExpr = VAst::getExpr(node, VAst::Expr::FUNCTION);
        return ast.createWithExpression(optExpr, [&](GroupNodeCPtr expr) {
          return ast.createWithExpression(fnExpr, [&](GroupNodeCPtr fn) {

                auto ifPresent = ast.createIsOptionalValuePresentExpression(expr);
                auto iftrue = ast.createOptifyExpression(ast.createFunctionCallExpression(fn,
                        { ast.createGetOptionalValueExpression(
                              expr)}));
                auto iffalse = ast.createLiteralNilExpression(node->span());
                auto res = ast.createConditionalExpression(ifPresent, iftrue, iffalse);
                return replace(res);
              });
        });
      }

      GroupNodeCPtr Replace::visit_get_bits_from_bit_expression(GroupNodeCPtr node)
      {
        auto bit = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto ty = ast.createType(getConstraintAnnotation(node), node->span());
        auto res = ast.createLiteralArrayExpression( { bit }, bit->span());
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_get_bit_from_bits_expression(GroupNodeCPtr node)
      {
        auto arr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto res = ast.createArrayIndexExpression(arr,
            ast.createLiteralInteger(Integer::ZERO(), node->span()));
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_get_bits_from_boolean_expression(GroupNodeCPtr node)
      {
        auto boolVal = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto ty = ast.createType(getConstraintAnnotation(node), node->span());
        auto bitZero = ast.createLiteralBit(0, node->span());
        auto bitOne = ast.createLiteralBit(1, node->span());
        auto byteZero = ast.createLiteralByte(0, node->span());
        auto bit = ast.createConditionalExpression(boolVal, bitOne, bitZero);
        auto res = ast.createLiteralArrayExpression( { bit }, bit->span());
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_get_boolean_from_bits_expression(GroupNodeCPtr node)
      {
        auto bitOne = ast.createLiteralBit(1, node->span());
        auto arr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto res = ast.createComparisonEQExpression(
            ast.createArrayIndexExpression(arr,
                ast.createLiteralInteger(Integer::ZERO(), node->span())), bitOne);
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_get_bits_from_byte_expression(GroupNodeCPtr node)
      {
        auto srcbyte = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto ty = ast.createType(getConstraintAnnotation(node), node->span());
        auto bitZero = ast.createLiteralBit(0, node->span());
        auto bitOne = ast.createLiteralBit(1, node->span());
        auto byteZero = ast.createLiteralByte(0, node->span());
        auto res = ast.createWithExpression(srcbyte, [&](GroupNodeCPtr byte) {
          GroupNodes bits;
          for (size_t i = 0; i < 8; ++i) {
            auto bitValue = ast.createLiteralByte(1 << i, node->span());
            auto cond = ast.createComparisonEQExpression(byteZero,
                ast.createAndExpression(bitValue, byte));
            bits.push_back(ast.createConditionalExpression(cond, bitZero, bitOne));
          }
          return ast.createLiteralArrayExpression(bits, node->span());
        });
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_get_byte_from_bits_expression(GroupNodeCPtr node)
      {
        auto byteZero = ast.createLiteralByte(0, node->span());
        auto bitOne = ast.createLiteralBit(1, node->span());
        auto srcbits = VAst::getExpr(node, VAst::Expr::EXPRESSION);

        auto res =
            ast.createWithExpression(srcbits,
                [&](
                    GroupNodeCPtr bits) {
                      GroupNodeCPtr ret = byteZero;
                      for (size_t i = 0; i < 8; ++i) {
                        auto bit = ast.createArrayIndexExpression(bits,
                            ast.createLiteralInteger(i, node->span()));
                        auto byte = ast.createLiteralByte(1 << i, node->span());
                        auto cond = ast.createComparisonEQExpression(bit, bitOne);
                        ret = ast.createOrExpression(ret, ast.createConditionalExpression(cond, byte, byteZero));
                      }
                      return ret;
                    });
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_get_bits_from_char_expression(GroupNodeCPtr node)
      {
        auto ch = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto ty = ast.createType(getConstraintAnnotation(node), node->span());

        auto bytes = ast.createEncodeCharExpression(ch);
        GroupNodeCPtr bits = ast.createMapArrayExpression(bytes, [&](GroupNodeCPtr e) {
          GroupNodes nodes;
          nodes.push_back(ast.createReturnStatement(ast.createGetBitsFromByteExpression(e)));
          return nodes;
        });
        auto res = ast.createFlattenArrayExpression(bits);
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_get_char_from_bits_expression(GroupNodeCPtr node)
      {
        auto arr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto parted = ast.createPartitionArrayExpression(arr,
            ast.createLiteralInteger(8, node->span()));
        GroupNodeCPtr bytes = ast.createMapArrayExpression(parted, [&](GroupNodeCPtr e) {
          GroupNodes nodes;
          nodes.push_back(ast.createReturnStatement(ast.createGetByteFromBitsExpression(e)));
          return nodes;
        });
        auto res = ast.createDecodeCharExpression(bytes);
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_get_bits_from_real_expression(GroupNodeCPtr node)
      {
        auto ch = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto ty = ast.createType(getConstraintAnnotation(node), node->span());

        auto bytes = ast.createEncodeRealExpression(ch);
        GroupNodeCPtr bits = ast.createMapArrayExpression(bytes, [&](GroupNodeCPtr e) {
          GroupNodes nodes;
          nodes.push_back(ast.createReturnStatement(ast.createGetBitsFromByteExpression(e)));
          return nodes;
        });
        auto res = ast.createFlattenArrayExpression(bits);
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_get_real_from_bits_expression(GroupNodeCPtr node)
      {
        auto arr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto parted = ast.createPartitionArrayExpression(arr,
            ast.createLiteralInteger(8, node->span()));
        GroupNodeCPtr bytes = ast.createMapArrayExpression(parted, [&](GroupNodeCPtr e) {
          GroupNodes nodes;
          nodes.push_back(ast.createReturnStatement(ast.createGetByteFromBitsExpression(e)));
          return nodes;
        });
        auto res = ast.createDecodeRealExpression(bytes);
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_encode_bit_expression(GroupNodeCPtr node)
      {
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto zero = ast.createLiteralByte(0, node->span());
        auto one = ast.createLiteralByte(255, node->span());
        auto cond = ast.createComparisonEQExpression(expr,
            ast.createLiteralBit(true, node->span()));
        return replace(
            ast.createLiteralArrayExpression( { ast.createConditionalExpression(cond, one, zero) },
                node->span()));
      }

      GroupNodeCPtr Replace::visit_encode_boolean_expression(GroupNodeCPtr node)
      {
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto zero = ast.createLiteralByte(0, node->span());
        auto one = ast.createLiteralByte(255, node->span());
        return replace(
            ast.createLiteralArrayExpression( { ast.createConditionalExpression(expr, one, zero) },
                node->span()));
      }

      GroupNodeCPtr Replace::visit_encode_byte_expression(GroupNodeCPtr node)
      {
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        return replace(ast.createLiteralArrayExpression( { expr }, node->span()));
      }

      GroupNodeCPtr Replace::visit_decode_bit_expression(GroupNodeCPtr node)
      {
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        expr = ast.createArrayIndexExpression(expr,
            ast.createLiteralInteger(Integer::ZERO(), node->span()));
        auto zero = ast.createLiteralByte(0, node->span());
        auto cond = ast.createComparisonEQExpression(expr, zero);
        return replace(
            ast.createConditionalExpression(cond, ast.createLiteralBit(false, node->span()),
                ast.createLiteralBit(true, node->span())));
      }

      GroupNodeCPtr Replace::visit_decode_boolean_expression(GroupNodeCPtr node)
      {
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        expr = ast.createArrayIndexExpression(expr,
            ast.createLiteralInteger(Integer::ZERO(), node->span()));
        auto zero = ast.createLiteralByte(0, node->span());
        auto cond = ast.createComparisonNEQExpression(expr, zero);
        return replace(cond);
      }

      GroupNodeCPtr Replace::visit_decode_byte_expression(GroupNodeCPtr node)
      {
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        expr = ast.createArrayIndexExpression(expr,
            ast.createLiteralInteger(Integer::ZERO(), node->span()));
        return replace(expr);
      }

      GroupNodeCPtr Replace::visit_flatten_array_expression(GroupNodeCPtr node)
      {
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto arrTy = getConstraintAnnotation(node)->self<ConstrainedArrayType>();
        auto accumTy = ConstrainedArrayType::getBoundedArray(arrTy->element,
            ConstrainedArrayType::Range(Integer::ZERO(), arrTy->bounds.max()));
        auto initial = ast.applyImplicitCast(accumTy,
            ast.createLiteralEmptyArrayExpression(node->span()));

        auto folded = ast.createFoldArrayExpression(expr, initial,
            [&](GroupNodeCPtr elem, GroupNodeCPtr accum) {
              GroupNodes block;
              auto concat = ast.createConcatenateArraysExpression(
                  accum, elem);
              block.push_back(ast.createReturnStatement(concat));
              return block;
            });
        auto res = ast.applyImplicitCast(arrTy, folded);
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_partition_array_expression(GroupNodeCPtr node)
      {
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto srcArrTy = getConstraintAnnotation(expr)->self<ConstrainedArrayType>();
        auto sz = VAst::getExpr(node, VAst::Expr::SIZE);
        auto arrTy = getConstraintAnnotation(node)->self<ConstrainedArrayType>();
        auto innerArrTy = arrTy->element->self<ConstrainedArrayType>();
        auto outerTy = ConstrainedArrayType::getBoundedArray(innerArrTy,
            ConstrainedArrayType::Range(Integer::ZERO(), arrTy->bounds.max()));

        if (arrTy->index == nullptr) {
          return ast.createLiteralEmptyArrayExpression(arrTy, node->span());
        }
        auto stateTy = ConstrainedTupleType::get(
            { outerTy, ConstrainedIntegerType::create(
                ConstrainedIntegerType::Range(Integer::ZERO(), srcArrTy->bounds.max())) });
        auto state0Ty = ast.createType(stateTy->types.at(0), node->span());
        auto state1Ty = ast.createType(stateTy->types.at(1), node->span());

        auto initial = ast.createLiteralTupleExpression(
            { ast.createLiteralTupleMember(state0Ty,
                ast.createLiteralEmptyArrayExpression(node->span())), ast.createLiteralTupleMember(
                state1Ty, ast.createLiteralInteger(Integer::ZERO(), node->span())) });

        auto loopResult = ast.createWithExpression(expr, [&](const GroupNodeCPtr arrayRef) {
          return ast.createWithExpression(sz,
              [&](const GroupNodeCPtr szRef) {
                return ast.createLoopExpression(
                    initial,
                    [&](GroupNodeCPtr state) {
                      return ast.createComparisonLTExpression(
                          ast.createTupleMemberExpression(
                              state,
                              1),
                          ast.createGetArrayLengthExpression(
                              arrayRef));
                    },
                    [&](GroupNodeCPtr state) {
                      auto array = ast.createTupleMemberExpression(
                          state, 0);
                      auto counter = ast.createTupleMemberExpression(
                          state, 1);
                      auto begin = counter;
                      auto end = ast.createIntegerAddExpression(
                          begin,
                          szRef);
                      auto subrange = ast.createLiteralArrayExpression(
                          { ast.createArraySubrangeExpression(
                                arrayRef,
                                begin,
                                end)},
                          node->span());

                      auto nextState0 = ast.createConcatenateArraysExpression(
                          array,
                          subrange);
                      auto nextState1 = end;

                      return ast.createLiteralTupleExpression(
                          {
                            ast.createLiteralTupleMember(
                                state0Ty,
                                nextState0),
                            ast.createLiteralTupleMember(
                                state1Ty,
                                nextState1)});

                    });
              });
        });
        auto res = ast.createUnsafeTypeCastExpression(ast.createType(arrTy, node->span()),
            ast.createTupleMemberExpression(loopResult, 0));
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_flatten_optional_expression(GroupNodeCPtr node)
      {
        // transform into a while statement
        auto optExpr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto optTy = getConstraintAnnotation(optExpr)->self<ConstrainedOptType>();
        auto elemTy = optTy->element->self<ConstrainedOptType>();

        GroupNodeCPtr res;
        if (elemTy) {
          res = ast.createWithExpression(optExpr, [&](GroupNodeCPtr ref) {
            auto check = ast.createIsOptionalValuePresentExpression(ref);
            auto get = ast.createGetOptionalValueExpression(ref);
            auto recurse = ast.createFlattenOptionalExpression(get);
            auto none = ast.createLiteralNilExpression(node->span());
            return ast.createConditionalExpression(check, recurse, none);
          });
        } else {
          res = optExpr;
        }
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_foreach_statement(GroupNodeCPtr node)
      {
        // transform into a while statement

        auto arrExpr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto decl = VAst::getRelation(node, VAst::Relation::DECL);
        auto body = VAst::getStmt(node, VAst::Stmt::LOOP_BODY);

        auto res = Loops::createWhileLoop(arrExpr, [&](GroupNodeCPtr element, GroupNodeCPtr) {
          GroupNodes whilebody;
          whilebody.push_back(ast.createDefineVariable(ast.getToken(decl, VAst::Token::IDENTIFIER),
                  getNameAnnotation(decl), getVariableAnnotation(decl),
                  nullptr, element));
          whilebody.push_back(body);
          return whilebody;
        });
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_assert_statement(GroupNodeCPtr node)
      {
        auto msgExpr = VAst::getExpr(node, VAst::Expr::MESSAGE);
        auto cond = ast.createGuardExpression(VAst::getExpr(node, VAst::Expr::CONDITION),
            ast.createLiteralBoolean(true, node->span()), msgExpr);
        auto res = ast.createIfStatement(cond, nullptr, nullptr);
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_integer_clamp_expression(GroupNodeCPtr node)
      {
        auto type = VAst::getRelation(node, VAst::Relation::TYPE);
        auto value = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto ety = getConstraintAnnotation(value)->self<ConstrainedIntegerType>();
        auto ty = getConstraintAnnotation(node)->self<ConstrainedIntegerType>();

        auto res = ast.createWithExpression(value, [&](GroupNodeCPtr valueRef) {
          auto expr = valueRef;
          if (ety->range.min() < ty->range.min()) {
            auto min = ast.createLiteralInteger(ty->range.min(), type->span());
            auto cond = ast.createComparisonLTExpression(valueRef, min);
            expr = ast.createConditionalExpression(cond, min, expr);
          }
          if ( ety->range.max() > ty->range.max()) {
            auto max = ast.createLiteralInteger(ty->range.max(), type->span());
            auto cond = ast.createComparisonLTExpression(max, valueRef);
            expr = ast.createConditionalExpression(cond, max, expr);
          }
          return ast.createCheckedCastExpression(type, expr);
        });
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_integer_wrap_expression(GroupNodeCPtr node)
      {
        auto type = VAst::getRelation(node, VAst::Relation::TYPE);
        auto value = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto ety = getConstraintAnnotation(value)->self<ConstrainedIntegerType>();
        auto ty = getConstraintAnnotation(node)->self<ConstrainedIntegerType>();

        auto range = ast.createLiteralInteger(ty->range.size(), type->span());
        auto min = ast.createLiteralInteger(ty->range.min(), type->span());
        auto max = ast.createLiteralInteger(ty->range.max(), type->span());
        auto one = ast.createLiteralInteger(Integer::PLUS_ONE(), type->span());

        auto res = ast.createWithExpression(value, [&](GroupNodeCPtr valueRef) {
          auto expr = valueRef;
          if (ety->range.min().isInfinite() || ety->range.min() < ty->range.min()) {
            auto wrapped = ast.createIntegerSubtractExpression(max,
                ast.createIntegerSubtractExpression(
                    ast.createIntegerModulusExpression(
                        ast.createIntegerSubtractExpression(min, valueRef), range), one));
            auto cond = ast.createComparisonLTExpression(valueRef, min);
            expr = ast.createConditionalExpression(cond, wrapped, expr);
          }
          if (ety->range.max().isInfinite() || ety->range.max() > ty->range.max()) {
            auto wrapped = ast.createIntegerAddExpression(min,
                ast.createIntegerSubtractExpression(
                    ast.createIntegerModulusExpression(
                        ast.createIntegerSubtractExpression(valueRef, max), range), one));
            auto cond = ast.createComparisonLTExpression(max, valueRef);
            expr = ast.createConditionalExpression(cond, wrapped, expr);
          }
          return ast.createCheckedCastExpression(type, expr);
        });
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_interpolate_text_expression(GroupNodeCPtr node)
      {
        auto exprs = VAst::getExprs(node, VAst::Expr::EXPRESSIONS);
        GroupNodeCPtr res;
        if (exprs.empty()) {
          res = ast.createLiteralString("", node->span());
        } else if (exprs.size() == 1) {
          res = exprs.at(0);
        } else {
          size_t mid = exprs.size() / 2;
          auto begin = exprs.begin();
          auto end = exprs.end();
          auto left = ast.createInterpolateTextExpression( { begin, begin + mid });
          auto right = ast.createInterpolateTextExpression( { begin + mid, end });
          res = ast.createConcatenateStringsExpression(left, right);
        }
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_convert_to_tuple_expression(GroupNodeCPtr node)
      {
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);

        GroupNodeCPtr res = ast.createWithExpression(expr, [&](GroupNodeCPtr e) {
          auto structTy = getConstraintAnnotation(
              e)->self<ConstrainedStructType>();
          GroupNodes args;
          for (auto m : structTy->members) {
            args.push_back(ast.createLiteralTupleMember(nullptr,
                    ast.createStructMemberExpression(
                        e,
                        m.name)));
          }
          return ast.createLiteralTupleExpression(args);
        });
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_struct_from_tuple_expression(GroupNodeCPtr node)
      {
        auto structTy = VAst::getRelation(node, VAst::Relation::TYPE);
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto exprTy = getConstraintAnnotation(expr)->self<ConstrainedTupleType>();

        GroupNodeCPtr res = ast.createWithExpression(expr, [&](GroupNodeCPtr e) {
          GroupNodes args;
          for (size_t i = 0; i < exprTy->types.size(); ++i) {
            args.push_back(ast.createTupleMemberExpression(e, i));
          }
          return ast.createNewStructExpression(structTy, args);
        });
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_get_head_expression(GroupNodeCPtr node)
      {
        GroupNodeCPtr res;
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto ty = getConstraintAnnotation(expr);
        if (ty->self<ConstrainedTupleType>()) {
          res = ast.createTupleMemberExpression(expr, 0);
        } else {
          auto index = ast.createLiteralInteger(Integer::ZERO(), node->span());
          res = ast.createArrayIndexExpression(expr, index);
        }

        return replace(res);
      }

      GroupNodeCPtr Replace::visit_get_tail_expression(GroupNodeCPtr node)
      {
        GroupNodeCPtr res;
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto ty = getConstraintAnnotation(expr);
        auto tupleTy = ty->self<ConstrainedTupleType>();
        if (tupleTy) {
          res = ast.createWithExpression(expr, [&](GroupNodeCPtr e) {
            GroupNodes args;
            for (size_t i = 1; i < tupleTy->types.size(); ++i) {
              args.push_back(ast.createLiteralTupleMember(nullptr,
                      ast.createTupleMemberExpression(
                          e,
                          i)));
            }
            return ast.createLiteralTupleExpression(args);
          });
        } else {
          res = ast.createArraySubrangeExpression(expr,
              ast.createLiteralInteger(Integer::PLUS_ONE(), expr->span()), nullptr);
        }
        return replace(res);
      }

      GroupNodeCPtr Replace::visit_get_as_roottype_expression(GroupNodeCPtr node)
      {
        GroupNodeCPtr res;
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto ty = getConstraintAnnotation(expr);
        if (getConstraintAnnotation(node)->isSameConstraint(*ty)) {
          res = expr;
        } else {
          res = ast.createGetAsRootTypeExpression(ast.createGetAsBaseTypeExpression(expr));
        }
        return replace(res);
      }

      GroupNodeCPtr Replace::apply(const GroupNodeCPtr &node)
      {
        Replace rw;
        return rw.replace(node);
      }
    }

  }
}
