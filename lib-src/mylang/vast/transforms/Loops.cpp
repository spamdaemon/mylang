#include <idioma/ast/GroupNode.h>
#include <idioma/ast/TokenNode.h>
#include <mylang/annotations.h>
#include <mylang/constraints/ConstrainedArrayType.h>
#include <mylang/constraints/ConstrainedIntegerType.h>
#include <mylang/constraints/ConstrainedType.h>
#include <mylang/names/Name.h>
#include <mylang/vast/transforms/Loops.h>
#include <mylang/vast/VAst.h>
#include <mylang/variables/Variable.h>
#include <memory>
#include <vector>

namespace mylang {
  namespace vast {
    namespace transforms {

      Loops::Loops(const GroupNodeCPtr &expr)
      {
        auto arrTy =
            getConstraintAnnotation(expr)->self<mylang::constraints::ConstrainedArrayType>();
        if (arrTy->bounds.max().is0()) {
          // loop will never be executed
          return;
        }

        VAst ast;
        auto ty = arrTy->counter;
        auto vloops = mylang::variables::Variable::createLocal(true);

        /** Create a local temporary for the array expression as well */
        auto arrayType = ast.createType(arrTy, expr->span());
        auto arrayTmpVar = mylang::names::Name::create("temp");
        auto arrayDecl = ast.createDefineVariable(nullptr, arrayTmpVar, vloops, arrayType, expr);
        arrayRef = ast.createVariableExpression(VAst::getToken(arrayDecl, VAst::Token::IDENTIFIER),
            arrayTmpVar, getConstraintAnnotation(arrayDecl));
        decls.push_back(arrayDecl);

        /** Create loop var decl */
        auto tmpVar = mylang::names::Name::create("temp");
        type = ast.createType(ty, expr->span());
        auto decl = ast.createDefineVariable(nullptr, tmpVar, vloops, type,
            ast.createLiteralInteger(0, expr->span()));
        decls.push_back(decl);

        indexRef = ast.createVariableExpression(VAst::getToken(decl, VAst::Token::IDENTIFIER),
            tmpVar, getConstraintAnnotation(decl));
        elementRef = ast.createArrayIndexExpression(arrayRef, indexRef);
      }

      GroupNodeCPtr Loops::createWhileLoop(const GroupNodeCPtr &expr,
          ::std::function<GroupNodes(GroupNodeCPtr element, GroupNodeCPtr index)> bodyFN)
      {
        VAst ast;
        Loops loops(expr);
        GroupNodes nodes(loops.decls);

        if (!nodes.empty()) {
          auto lessThanLength = ast.createComparisonLTExpression(loops.indexRef,
              ast.createGetArrayLengthExpression(loops.arrayRef));

          auto increment = ast.createUpdateStatement(loops.indexRef,
              ast.createCheckedCastExpression(loops.type,
                  ast.createIntegerAddExpression(ast.createLiteralInteger(1, expr->span()),
                      loops.indexRef)));

          GroupNodes body = bodyFN(loops.elementRef, loops.indexRef);
          body.push_back(increment);
          nodes.push_back(ast.createWhileStatement(lessThanLength, ast.createStatementBlock(body)));
        }
        return ast.createStatementBlock(nodes);
      }
    }
  }
}
