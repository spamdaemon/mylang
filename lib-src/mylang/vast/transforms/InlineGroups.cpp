#include <idioma/ast/GroupNode.h>
#include <idioma/ast/Node.h>
#include <mylang/vast/transforms/InlineGroups.h>
#include <mylang/vast/VAst.h>
#include <mylang/vast/VAstQuery.h>
#include <mylang/vast/VAstTransform.h>
#include <mylang/vast/VAstVisitor.h>
#include <memory>
#include <stdexcept>
#include <iostream>
#include <vector>

namespace mylang {
  namespace vast {
    namespace transforms {

      using namespace mylang::constraints;

      namespace {

        struct Inliner: public VAstTransform
        {
          ~Inliner()
          {
          }

          GroupNodeCPtr visitChildNodes(const GroupNodeCPtr &node) override
          {
            ::std::vector<::idioma::ast::GroupNode::Rel> entries;
            bool changed = false;
            for (auto e : node->entries()) {
              auto g = e.node->self<const GroupNode>();
              if (g && g->hasType(VAst::GroupType::INLINE_GROUP)) {
                auto members = VAst::getRelations(g, VAst::Relation::INLINE_GROUP_MEMBERS);
                entries.emplace_back(e.rel, members);
                changed = true;
              } else {
                entries.emplace_back(e.rel, e.node);
              }
            }
            GroupNodeCPtr res;
            if (changed) {
              res = GroupNode::create(node->type, entries);
              res = rewrite(res);
            } else {
              res = VAstTransform::visitChildNodes(node);
            }
            return res;
          }

          GroupNodeCPtr rewrite(GroupNodeCPtr node)
          {
            auto res = visit(node);
            return res ? res : node;
          }
        };

        struct CheckNoGroups: public VAstQuery
        {
          CheckNoGroups()
              : fail(false)
          {
          }

          ~CheckNoGroups()
          {
          }

          void visit_inline_group(GroupNodeCPtr) override
          {
            fail = true;
          }

          bool fail;
        };

      }

      InlineGroups::InlineGroups()
      {
      }

      InlineGroups::~InlineGroups()
      {
      }

      GroupNodeCPtr InlineGroups::apply(const GroupNodeCPtr &node)
      {
        Inliner tx;
        auto res = tx.rewrite(node);
        CheckNoGroups verify;
        verify.visit(res);
        if (verify.fail) {
          throw ::std::runtime_error("Failed to remove all inline groups");
        }
        return res;
      }
    }
  }
}
