#include <idioma/ast/GroupNode.h>
#include <mylang/annotations.h>
#include <mylang/constraints.h>
#include <mylang/vast/transforms/InlineGroups.h>
#include <mylang/vast/transforms/InstantiateGenerics.h>
#include <mylang/vast/VAst.h>
#include <mylang/vast/VAstVisitor.h>
#include <mylang/vast/VAstTransform.h>
#include <memory>
#include <vector>

namespace mylang {
  namespace vast {
    namespace transforms {

      using namespace mylang::constraints;

      namespace {

        typedef ::std::map<NamePtr, ::std::map<NamePtr, GroupNodeCPtr>> Instances;

        struct RewriteInstantiations: public VAstTransform
        {
          RewriteInstantiations(Instances &xinstances)
              : instances(xinstances)
          {
          }

          ~RewriteInstantiations()
          {
          }

          GroupNodeCPtr visit_instantiate_generic_function(GroupNodeCPtr node)
          {
            auto defName = VAst::getToken(node, VAst::Token::GENERIC_DEFINITION_NAME);
            auto defKey = getNameAnnotation(defName);
            auto instanceName = VAst::getToken(node, VAst::Token::IDENTIFIER);
            auto instanceKey = getNameAnnotation(node);

            auto fundef = VAst::getRelation(node, VAst::Relation::GENERIC_INSTANCE_FUNCTION);
            if (!fundef) {
              throw ::std::runtime_error(
                  "generic function " + instanceKey->fullName()
                      + " cannot be instantiated, because there is no body");
            }

            if (instances[defKey].count(instanceKey) == 0) {
              // this increases the count by 1, so that we can do this recursively
              instances[defKey][instanceKey] = nullptr;
              fundef = rewrite(fundef);
              instances[defKey][instanceKey] = fundef;
            }
            return ast.createVariableExpression(instanceName, instanceKey,
                getConstraintAnnotation(node));
          }

          GroupNodeCPtr rewrite(GroupNodeCPtr node)
          {
            auto res = visit(node);
            return res ? res : node;
          }

          /** All the instantiation nodes of a generic function */
          Instances &instances;
        };

        struct MoveInstantiations: public VAstTransform
        {
          MoveInstantiations(Instances &xinstances)
              : instances(xinstances)
          {
          }

          ~MoveInstantiations()
          {
          }

          GroupNodeCPtr visit_def_generic_function(GroupNodeCPtr node)
          {
            auto key = getNameAnnotation(node);
            GroupNodes nodes;
            for (auto i : instances[key]) {
              nodes.push_back(i.second);
            }
            auto res = ast.createInlineGroup(nodes);
            return rewrite(res);
          }

          GroupNodeCPtr rewrite(GroupNodeCPtr node)
          {
            auto res = visit(node);
            return res ? res : node;
          }

          /** All the instantiation nodes of a generic function */
          Instances &instances;
        };

      }

      InstantiateGenerics::InstantiateGenerics()
      {
      }

      InstantiateGenerics::~InstantiateGenerics()
      {
      }

      GroupNodeCPtr InstantiateGenerics::apply(const GroupNodeCPtr &node)
      {
        Instances instances;
        RewriteInstantiations phase1(instances);
        MoveInstantiations phase2(instances);
        InlineGroups phase3;
        auto res = node;
        res = phase1.rewrite(res);
        res = phase2.rewrite(res);
        res = phase3.apply(res);
        return res;
      }
    }
  }
}
