#ifndef CLASS_MYLANG_VAST_TRANSFORMS_TEMPORARY_H
#define CLASS_MYLANG_VAST_TRANSFORMS_TEMPORARY_H
#include <mylang/defs.h>

namespace mylang {
  namespace vast {
    namespace transforms {

      /**
       * This helper class can be used for temporary variables that are needed when apply AST transforms.
       */
      class Temporary
      {
      public:
        Temporary() = delete;

      public:
        Temporary(const ConstrainedTypePtr &ty, const ::idioma::ast::Node::Span &span,
            const GroupNodeCPtr &expr, bool isMutable = false);

      public:
        Temporary(const GroupNodeCPtr &expr, bool isMutable = false);

        /** The declaration */
      public:
        GroupNodeCPtr decl;

        /** The reference to the decl */
      public:
        GroupNodeCPtr ref;

        /** The decl type */
      public:
        GroupNodeCPtr type;
      };
    }
  }
}
#endif
