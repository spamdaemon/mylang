#ifndef CLASS_MYLANG_VAST_TRANSFORMS_REWRITE_H
#define CLASS_MYLANG_VAST_TRANSFORMS_REWRITE_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_VALIDATION_VASTTRANSFORM_H
#include <mylang/vast/VAstTransform.h>
#endif

namespace mylang {
  namespace vast {
    namespace transforms {

      /**
       * Rewrite node in terms of other, simpler node types.
       */
      class Rewrite: public VAstTransform
      {

        /**
         * Create a code builder.
         */
      public:
        Rewrite();

        /**
         * Destructor
         */
      public:
        ~Rewrite();

        /**
         * Apply the normalization to the specified node.
         * @param node a node
         * @return a normalized node
         */
      public:
        static GroupNodeCPtr apply(const GroupNodeCPtr &node);

        /**
         * Rewrite a checked cast if the expression to be casted is the nil or [] (empty array) literals.
         */
        GroupNodeCPtr visit_new_byte_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_comparison_eq_expression(GroupNodeCPtr node) override final;

        /**
         * A stringify for an optional can be rewritten.
         */
        GroupNodeCPtr visit_stringify_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_get_as_roottype_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_get_as_basetype_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_expression_statement(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_literal_nil(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_literal_empty_array(GroupNodeCPtr node) override;

        GroupNodeCPtr visit(GroupNodeCPtr node) override final;

      private:
        GroupNodeCPtr rewrite(GroupNodeCPtr node);

      };
    }
  }
}
#endif
