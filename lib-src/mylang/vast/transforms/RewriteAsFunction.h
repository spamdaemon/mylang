#ifndef CLASS_MYLANG_VAST_TRANSFORMS_REWRITEASFUNCTION_H
#define CLASS_MYLANG_VAST_TRANSFORMS_REWRITEASFUNCTION_H
#include <string>
#include <vector>

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_VALIDATION_VASTTRANSFORM_H
#include <mylang/vast/VAstTransform.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDFUNCTIONTYPE_H
#include <mylang/constraints/ConstrainedFunctionType.h>
#endif
#include <map>

namespace mylang {
  namespace vast {
    namespace transforms {

      /**
       * This transform is similar to rewrite, but replaces individual expressions
       * with calls to new well-known function. For example, struct equality will cause
       * an appropriate function to be emitted.
       */
      class RewriteAsFunction: public VAstTransform
      {
      private:
        RewriteAsFunction();

      private:
        ~RewriteAsFunction();

        /**
         * Rewrite the specified node.
         * @param node a node
         * @return a rewritten node, or node if no rewrite was performed.
         */
      public:
        static GroupNodeCPtr apply(const GroupNodeCPtr &node);

        GroupNodeCPtr visit_comparison_eq_expression(GroupNodeCPtr node) override final;

        GroupNodeCPtr visit(GroupNodeCPtr node) override final;

      private:
        GroupNodeCPtr rewrite(GroupNodeCPtr node);

        GroupNodes rewrite(GroupNodes node);

        /**
         * Find a function that has the specified signature and opcode
         * @param opcode an opcode
         * @param sig the function signature
         * @return the function whose name matches the opcode and signature (nullptr if not found)
         */
      private:
        GroupNodeCPtr findFunction(const ::std::string &subkey,
            const mylang::constraints::ConstrainedFunctionType &fnType) const;

        /** The key */
      private:
        ::std::string _key;

        /**
         * Functions that were created by this transform.
         * This set contains functions created prior apply and functions created during apply.
         */
      private:
        ::std::vector<GroupNodeCPtr> functions;

        /** The number of functions that existed before the current call to apply */
      private:
        size_t preApplyFunctionCount;
      };
    }
  }
}
#endif
