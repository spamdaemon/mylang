#include <idioma/ast/GroupNode.h>
#include <mylang/annotations.h>
#include <mylang/constraints/ConstrainedType.h>
#include <mylang/defs.h>
#include <mylang/names/Name.h>
#include <mylang/vast/transforms/Temporary.h>
#include <mylang/variables/Variable.h>
#include <mylang/vast/VAst.h>

namespace mylang {
  namespace vast {
    namespace transforms {

      Temporary::Temporary(const ConstrainedTypePtr &ty, const ::idioma::ast::Node::Span &span,
          const GroupNodeCPtr &expr, bool isMutable)
      {
        VAst ast;
        auto tmpVar = mylang::names::Name::create("temp");
        auto vinfo = mylang::variables::Variable::createLocal(isMutable);
        type = ast.createType(ty, span);
        decl = ast.createDefineVariable(nullptr, tmpVar, vinfo, ast.createType(ty, span), expr);
        ref = ast.createVariableExpression(VAst::getToken(decl, VAst::Token::IDENTIFIER), tmpVar,
            getConstraintAnnotation(decl));
      }

      Temporary::Temporary(const GroupNodeCPtr &expr, bool isMutable)
          : Temporary(getConstraintAnnotation(expr), expr->span(), expr, isMutable)
      {
      }

    }
  }
}
