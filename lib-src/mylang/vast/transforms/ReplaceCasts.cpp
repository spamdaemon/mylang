#include <idioma/ast/GroupNode.h>
#include <idioma/ast/TokenNode.h>
#include <mylang/annotations.h>
#include <mylang/constraints/ConstrainedArrayType.h>
#include <mylang/constraints/ConstrainedIntegerType.h>
#include <mylang/constraints/ConstrainedFunctionType.h>
#include <mylang/constraints/ConstrainedOptType.h>
#include <mylang/constraints/ConstrainedRealType.h>
#include <mylang/constraints/ConstrainedStructType.h>
#include <mylang/constraints/ConstrainedTupleType.h>
#include <mylang/constraints/ConstrainedType.h>
#include <mylang/names/Name.h>
#include <mylang/vast/transforms/ReplaceCasts.h>
#include <mylang/vast/transforms/Temporary.h>
#include <mylang/vast/VAst.h>
#include <mylang/vast/VAstVisitor.h>
#include <mylang/variables/Variable.h>
#include <stddef.h>
#include <memory>
#include <optional>
#include <stdexcept>
#include <string>
#include <vector>

namespace mylang {
  namespace vast {
    namespace transforms {

      using namespace mylang::constraints;

      namespace {
        static GroupNodeCPtr replace_unsafe_integer_typecast_expression(VAst &ast,
            const GroupNodeCPtr &toType, GroupNodeCPtr e)
        {
          auto ty = getConstraintAnnotation(toType)->self<ConstrainedIntegerType>();
          auto ety = getConstraintAnnotation(e)->self<ConstrainedIntegerType>();

          GroupNodeCPtr res = ast.createCheckedCastExpression(toType, e);

          if (ety->range.min() < ty->range.min()) {
            res = ast.createGuardExpression(
                ast.createComparisonLTEExpression(
                    ast.createLiteralInteger(ty->range.min(), e->span()), e), res, "cannot cast1");
          }

          if (ety->range.max() > ty->range.max()) {
            res = ast.createGuardExpression(
                ast.createComparisonLTEExpression(e,
                    ast.createLiteralInteger(ty->range.max(), e->span())), res, "cannot cast2");
          }
          return res;
        }

        static GroupNodeCPtr replace_unsafe_array_typecast_expression(VAst &ast,
            const GroupNodeCPtr &toType, GroupNodeCPtr e)
        {
          auto ty = getConstraintAnnotation(toType)->self<ConstrainedArrayType>();
          auto ety = getConstraintAnnotation(e)->self<ConstrainedArrayType>();

          GroupNodeCPtr res = e;

          if (!ety->element->isSameConstraint(*ty->element)) {
            auto elemType = ast.createType(ety->element, e->span());
            auto toElemTy = ast.createType(ty->element, e->span());
            auto tmpVar = mylang::names::Name::create("x");
            auto vinfo = mylang::variables::Variable::createParameter();
            auto vardecl = ast.createDefineVariable(nullptr, tmpVar, vinfo, elemType, nullptr);
            auto varref = ast.createVariableExpression(
                VAst::getToken(vardecl, VAst::Token::IDENTIFIER), tmpVar,
                getConstraintAnnotation(vardecl));
            auto body = ast.createUnsafeTypeCastExpression(toElemTy, varref);
            auto lambdaBody = ast.createFunctionBody( { ast.createReturnStatement(body) });
            auto lambda = ast.createLambdaExpression(toElemTy, { vardecl }, lambdaBody);
            res = ast.createMapArrayExpression(lambda, res);
          }

          res = ast.createCheckedCastExpression(toType, res);

          auto len = ast.createGetArrayLengthExpression(e);
          if (ety->bounds.min() < ty->bounds.min()) {
            res = ast.createGuardExpression(
                ast.createComparisonLTEExpression(
                    ast.createLiteralInteger(ty->bounds.min(), e->span()), len), res,
                "cannot cast3");
          }

          if (ety->bounds.max() > ty->bounds.max()) {
            res = ast.createGuardExpression(
                ast.createComparisonLTEExpression(len,
                    ast.createLiteralInteger(ty->bounds.max(), e->span())), res, "cannot cast4");
          }

          return res;
        }

        static GroupNodeCPtr replace_unsafe_opt_typecast_expression(VAst &ast,
            const GroupNodeCPtr &toType, GroupNodeCPtr e)
        {
          auto ety = getConstraintAnnotation(e)->self<ConstrainedOptType>();
          auto to = getConstraintAnnotation(toType)->self<ConstrainedOptType>();

          auto nil = ast.createNewOptionalExpression(toType, { });
          auto present = ast.createIsOptionalValuePresentExpression(e);
          auto get = ast.createGetOptionalValueExpression(e);
          auto cast = ast.createUnsafeTypeCastExpression(ast.createType(to->element, e->span()),
              get);
          auto some = ast.createNewOptionalExpression(toType, { cast });
          auto guard = ast.createConditionalExpression(present, some, nil);
          return guard;
        }

        static GroupNodeCPtr replace_unsafe_tuple_typecast_expression(VAst &ast,
            const GroupNodeCPtr &toType, GroupNodeCPtr e)
        {
          auto ety = getConstraintAnnotation(e)->self<ConstrainedTupleType>();
          auto to = getConstraintAnnotation(toType)->self<ConstrainedTupleType>();

          // add implicit type casts
          GroupNodes tmp;
          tmp.reserve(to->types.size());
          for (size_t i = 0, sz = to->types.size(); i < sz; ++i) {
            tmp.push_back(
                ast.createUnsafeTypeCastExpression(ast.createType(to->types.at(i), e->span()),
                    ast.createTupleMemberExpression(e, i)));
          }
          return ast.createNewTupleExpression(toType, tmp);
        }

        static GroupNodeCPtr replace_unsafe_struct_typecast_expression(VAst &ast,
            const GroupNodeCPtr &toType, GroupNodeCPtr e)
        {
          auto ety = getConstraintAnnotation(e)->self<ConstrainedStructType>();
          auto to = getConstraintAnnotation(toType)->self<ConstrainedStructType>();

          // add implicit type casts
          GroupNodes tmp;
          tmp.reserve(to->members.size());
          for (size_t i = 0, sz = to->members.size(); i < sz; ++i) {
            const ConstrainedStructType::Member &member = to->members.at(i);
            tmp.push_back(
                ast.createUnsafeTypeCastExpression(ast.createType(member.constraint, e->span()),
                    ast.createStructMemberExpression(e, member.name)));
          }
          return ast.createNewStructExpression(toType, tmp);
        }

        static GroupNodeCPtr replace_unsafe_function_typecast_expression(VAst &ast,
            const GroupNodeCPtr &toType, GroupNodeCPtr e)
        {
          auto ety = getConstraintAnnotation(e)->self<ConstrainedFunctionType>();
          auto to = getConstraintAnnotation(toType)->self<ConstrainedFunctionType>();

          // we need to create the argument names
          GroupNodes params;
          GroupNodes fwdParams;
          auto paramKind = mylang::variables::Variable::createParameter();
          for (size_t i = 0; i < ety->parameters.size(); ++i) {
            auto name = mylang::names::Name::create("arg");
            auto decl = ast.createDefineVariable(nullptr, name, paramKind,
                ast.createType(to->parameters.at(i), e->span()), nullptr);
            params.push_back(decl);
            auto fwd = ast.createUnsafeTypeCastExpression(
                ast.createType(ety->parameters.at(i), e->span()),
                ast.createVariableExpression(nullptr, name, to->parameters.at(i)));
            fwdParams.push_back(fwd);
          }
          auto call = ast.createFunctionCallExpression(e, fwdParams);
          auto expr = ast.createUnsafeTypeCastExpression(ast.createType(to->returnType, e->span()),
              call);
          auto body = ast.createFunctionBody( { ast.createReturnStatement(expr) });
          return ast.createLambdaExpression(toType, params, body);
        }

      }

      ReplaceCasts::ReplaceCasts()
      {
      }

      ReplaceCasts::~ReplaceCasts()
      {
      }

      GroupNodeCPtr ReplaceCasts::replace(GroupNodeCPtr node)
      {
        auto res = visit(node);
        return res ? res : node;
      }

      GroupNodeCPtr ReplaceCasts::visit(GroupNodeCPtr node)
      {
        GroupNodeCPtr rewritten = VAstTransform::visit(node);
#if 0
                if (rewritten) {
                  auto fromkey = node->type.value<VAst::GroupType>();
                  auto tokey = rewritten->type.value<VAst::GroupType>();
                  ::std::cerr << "ReplaceCasts " << (int) fromkey->key << " -> " << (int) tokey->key
                      << ::std::endl;
                }
#endif
        return rewritten;
      }

      GroupNodeCPtr ReplaceCasts::visit_implicit_basetypecast_expression(GroupNodeCPtr node)
      {
        GroupNodeCPtr res;
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto ty = getConstraintAnnotation(node);
        auto ety = getConstraintAnnotation(expr);
        if (ty->isSameConstraint(*ety)) {
          res = expr;
        } else {
          res = ast.createGetAsBaseTypeExpression(expr);
        }
        return replace(res);
      }

      GroupNodeCPtr ReplaceCasts::visit_safe_cast_expression(GroupNodeCPtr node)
      {
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto ty = getConstraintAnnotation(node);
        auto toType = ast.createType(ty, node->span());

        auto res = ast.createUnsafeTypeCastExpression(toType, expr);
        return replace(res);
      }

      GroupNodeCPtr ReplaceCasts::visit_implicit_cast_expression(GroupNodeCPtr node)
      {
        auto toType = VAst::getRelation(node, VAst::Relation::TYPE);
        auto e = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto res = ast.createUnsafeTypeCastExpression(toType, e);
        return replace(res);
      }

      GroupNodeCPtr ReplaceCasts::visit_unsafe_cast_expression(GroupNodeCPtr node)
      {
        GroupNodeCPtr res;
        auto toType = VAst::getRelation(node, VAst::Relation::TYPE);
        auto e = VAst::getExpr(node, VAst::Expr::EXPRESSION);

        auto ty = getConstraintAnnotation(toType);
        auto ety = getConstraintAnnotation(e);
        auto bt = ety->basetype();

        if (ty->isSameConstraint(*ety)) {
          res = e;
        } else if (bt && ty->canCastFrom(*bt)) {
          res = ast.createUnsafeTypeCastExpression(toType,
              ast.createImplicitBaseTypeCastExpression(e));
        } else if (e->hasType(VAst::GroupType::LITERAL_NIL)) {
          // two special cases need to be considered to deal with 'nil' and '[]' literals
          res = ast.createNewOptionalExpression(toType, { });
        } else if (e->hasType(VAst::GroupType::LITERAL_EMPTY_ARRAY)) {
          // two special cases need to be considered to deal with 'nil' and '[]' literals
          res = ast.createNewArrayExpression(toType, { });
        } else if (ty->self<ConstrainedRealType>() && ety->self<ConstrainedIntegerType>()) {
          res = ast.createCheckedCastExpression(toType, e);
        } else {
          res = ast.createWithExpression(e, [&](GroupNodeCPtr ref) {
            if (ty->self<ConstrainedIntegerType>() &&
                ety->self<ConstrainedIntegerType>()) {
              return replace_unsafe_integer_typecast_expression(ast,
                  toType,
                  ref);
            } else if (ty->self<ConstrainedArrayType>() &&
                ety->self<ConstrainedArrayType>()) {
              return replace_unsafe_array_typecast_expression(ast,
                  toType,
                  ref);
            } else if (ty->self<ConstrainedOptType>() &&
                ety->self<ConstrainedOptType>()) {
              return replace_unsafe_opt_typecast_expression(ast, toType,
                  ref);
            } else if (ty->self<ConstrainedStructType>() &&
                ety->self<ConstrainedStructType>()) {
              return replace_unsafe_struct_typecast_expression(ast,
                  toType,
                  ref);
            } else if (ty->self<ConstrainedTupleType>() &&
                ety->self<ConstrainedTupleType>()) {
              return replace_unsafe_tuple_typecast_expression(ast,
                  toType,
                  ref);
            } else if (ty->self<ConstrainedFunctionType>() &&
                ety->self<ConstrainedFunctionType>()) {
              return replace_unsafe_function_typecast_expression(ast,
                  toType,
                  ref);
            } else {
              throw ::std::runtime_error(
                  "Cannot cast from " + ety->toString() + " to " +
                  ty->toString());
            }
          });
        }
        return replace(res);
      }

      GroupNodeCPtr ReplaceCasts::apply(const GroupNodeCPtr &node)
      {
        ReplaceCasts rw;
        return rw.replace(node);
      }
    }
  }
}
