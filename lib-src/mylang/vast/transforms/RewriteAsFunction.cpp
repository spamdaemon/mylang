#include <idioma/ast/GroupNode.h>
#include <idioma/ast/TokenNode.h>
#include <mylang/annotations.h>
#include <mylang/constraints/ConstrainedArrayType.h>
#include <mylang/constraints/ConstrainedFunctionType.h>
#include <mylang/constraints/ConstrainedStructType.h>
#include <mylang/constraints/ConstrainedTupleType.h>
#include <mylang/constraints/ConstrainedOptType.h>
#include <mylang/constraints/ConstrainedUnionType.h>
#include <mylang/names/Name.h>
#include <mylang/vast/transforms/RewriteAsFunction.h>
#include <mylang/vast/transforms/Loops.h>
#include <mylang/vast/transforms/Temporary.h>
#include <mylang/vast/VAst.h>
#include <mylang/vast/VAstQuery.h>
#include <mylang/variables/Variable.h>
#include <mylang/vast/queries/TypeFinder.h>
#include <mylang/vast/queries/NodeFinder.h>
#include <map>
#include <memory>
#include <string>
#include <vector>
#include <functional>

namespace mylang {
  namespace vast {
    namespace transforms {

      using namespace mylang::constraints;

      namespace {

        // create struct equality function
        static GroupNodes createOptionalEqualityFunction(VAst ast, GroupNodeCPtr lhs,
            GroupNodeCPtr rhs)
        {
          auto structTy = getConstraintAnnotation(lhs)->self<ConstrainedOptType>();

          GroupNodes statements;
          auto notEqual = ast.createReturnStatement(ast.createLiteralBoolean(false, lhs->span()));
          auto isPresent_lhs = ast.createIsOptionalValuePresentExpression(lhs);
          auto isPresent_rhs = ast.createIsOptionalValuePresentExpression(rhs);
          auto get_lhs = ast.createGetOptionalValueExpression(lhs);
          auto get_rhs = ast.createGetOptionalValueExpression(rhs);

          // if (lhs.present != rhs.present) { return false };
          auto eq = ast.createComparisonNEQExpression(isPresent_lhs, isPresent_rhs);
          auto ifexpr = ast.createIfStatement(eq, notEqual, nullptr);
          statements.push_back(ifexpr);

          // if (lhs.present) { return *lhs == *rhs } else { return false; }
          eq = ast.createComparisonEQExpression(get_lhs, get_rhs);
          ifexpr = ast.createIfStatement(isPresent_lhs, ast.createReturnStatement(eq),
              ast.createReturnStatement(ast.createLiteralBoolean(true, lhs->span())));
          statements.push_back(ifexpr);
          return statements;
        }

        // create struct equality function
        static GroupNodes createArrayEqualityFunction(VAst ast, GroupNodeCPtr lhs,
            GroupNodeCPtr rhs)
        {
          auto ty = getConstraintAnnotation(lhs)->self<ConstrainedArrayType>();

          GroupNodes statements;
          auto notEqual = ast.createReturnStatement(ast.createLiteralBoolean(false, lhs->span()));
          auto length_lhs = ast.createGetArrayLengthExpression(lhs);
          auto length_rhs = ast.createGetArrayLengthExpression(rhs);

          // if (lhs.length != rhs.length) { return false };
          {
            auto eq = ast.createComparisonNEQExpression(length_lhs, length_rhs);
            auto ifexpr = ast.createIfStatement(eq, notEqual, nullptr);
            statements.push_back(ifexpr);
          }

          Loops::createWhileLoop(lhs, [&](GroupNodeCPtr element_lhs, GroupNodeCPtr index) {
            GroupNodes body;
            auto element_rhs = ast.createArrayIndexExpression(rhs, index);
            auto eq = ast.createComparisonNEQExpression(element_lhs, element_rhs);
            auto ifexpr = ast.createIfStatement(eq, notEqual, nullptr);
            body.push_back(ifexpr);
            return body;
          });

          statements.push_back(
              ast.createReturnStatement(ast.createLiteralBoolean(true, lhs->span())));
          return statements;
        }

        // create struct equality function
        static GroupNodes createStructEqualityFunction(VAst ast, GroupNodeCPtr lhs,
            GroupNodeCPtr rhs)
        {
          auto structTy = getConstraintAnnotation(lhs)->self<ConstrainedStructType>();

          GroupNodes statements;
          auto notEqual = ast.createReturnStatement(ast.createLiteralBoolean(false, lhs->span()));
          for (auto m : structTy->members) {
            auto left = ast.createStructMemberExpression(lhs, m.name);
            auto right = ast.createStructMemberExpression(rhs, m.name);
            auto eq = ast.createComparisonNEQExpression(left, right);
            auto ifexpr = ast.createIfStatement(eq, notEqual, nullptr);
            statements.push_back(ifexpr);
          }
          // if we have no members, we need to return true
          statements.push_back(
              ast.createReturnStatement(ast.createLiteralBoolean(true, lhs->span())));
          return statements;
        }

        // create struct equality function
        static GroupNodes createTupleEqualityFunction(VAst ast, GroupNodeCPtr lhs,
            GroupNodeCPtr rhs)
        {
          auto tupleTy = getConstraintAnnotation(lhs)->self<ConstrainedTupleType>();

          GroupNodes statements;
          auto notEqual = ast.createReturnStatement(ast.createLiteralBoolean(false, lhs->span()));
          for (size_t i = 0; i < tupleTy->tuple.size(); ++i) {
            auto left = ast.createTupleMemberExpression(lhs, i);
            auto right = ast.createTupleMemberExpression(rhs, i);
            auto eq = ast.createComparisonNEQExpression(left, right);
            auto ifexpr = ast.createIfStatement(eq, notEqual, nullptr);
            statements.push_back(ifexpr);
          }
          // if we have no members, we need to return true
          statements.push_back(
              ast.createReturnStatement(ast.createLiteralBoolean(true, lhs->span())));
          return statements;
        }

        // create struct equality function
        static GroupNodes createUnionEqualityFunction(VAst ast, GroupNodeCPtr lhs,
            GroupNodeCPtr rhs)
        {
          auto unionTy = getConstraintAnnotation(lhs)->self<ConstrainedUnionType>();

          GroupNodes statements;
          auto notEqual = ast.createReturnStatement(ast.createLiteralBoolean(false, lhs->span()));

          // check the discriminant first
          auto discriminant = ast.createUnionMemberExpression(lhs, unionTy->discriminant.name);
          {
            auto left = discriminant;
            auto right = ast.createUnionMemberExpression(rhs, unionTy->discriminant.name);
            auto eq = ast.createComparisonNEQExpression(left, right);
            auto ifexpr = ast.createIfStatement(eq, notEqual, nullptr);
            statements.push_back(ifexpr);
          }
          for (auto m : unionTy->members) {

            auto memberEq = ast.createComparisonEQExpression(discriminant,
                ast.createLiteral(m.discriminant, lhs->span()));
            auto left = ast.createUnionMemberExpression(lhs, m.name);
            auto right = ast.createUnionMemberExpression(rhs, m.name);
            auto eq = ast.createComparisonEQExpression(left, right);
            auto ifexpr = ast.createIfStatement(memberEq, ast.createReturnStatement(eq), nullptr);
            statements.push_back(ifexpr);
          }
          statements.push_back(ast.createThrowStatement());
          return statements;
        }

        static ::std::string extractKey(const NamePtr &name)
        {
          auto root = name->root();
          if (root->isFQN()) {
            return root->localName();
          }
          return ::std::string();
        }

        // this is the beginning of unique function names based on types, like C++ name mangling
        static NamePtr createUniqueFunctionName(const ::std::string &key,
            const ::std::string &opcode, const ConstrainedFunctionType &fnType)
        {
          auto root = mylang::names::Name::createFQN(key);
          auto level2 = mylang::names::Name::createFQN(opcode, root);

          return mylang::names::Name::createFQN(fnType.name()->uniqueFullName("_"), level2);
        }

        // find all functions that may have previously been introduced, so we don't
        // reintroduce them.
        static ::std::vector<GroupNodeCPtr> collectFunctions(const ::std::string &key,
            GroupNodeCPtr root)
        {
          // find all known function types, because we only care about functions might have introduced
          auto types = mylang::vast::queries::TypeFinder::find(root,
              [&](const ConstrainedTypePtr &c) {
                return c->self<ConstrainedFunctionType>() !=
                nullptr;
              });

          ::std::map<::std::string, GroupNodeCPtr> res;
          auto selectFN = [&](const GroupNodeCPtr &node) -> bool {
            if (node->hasType(VAst::GroupType::DEF_FUNCTION)) {
              auto n = getNameAnnotation(node);
              return n && extractKey(n) == key;
            }
            return false;
          };
          auto recurseFN = [&](const GroupNodeCPtr &node) -> bool {
            return !node->hasType(VAst::GroupType::DEF_FUNCTION);
          };

          return mylang::vast::queries::NodeFinder::find(root, selectFN, recurseFN);
        }

      }

      RewriteAsFunction::RewriteAsFunction()
          : _key("extensions"), preApplyFunctionCount(0)
      {
      }

      RewriteAsFunction::~RewriteAsFunction()
      {
      }

      GroupNodeCPtr RewriteAsFunction::visit(GroupNodeCPtr node)
      {
        GroupNodeCPtr rewritten = VAstTransform::visit(node);
        return rewritten;
      }

      GroupNodeCPtr RewriteAsFunction::visit_comparison_eq_expression(GroupNodeCPtr node)
      {
        auto ty = getConstraintAnnotation(node);
        auto lhs = VAst::getExpr(node, VAst::Expr::LHS);
        auto rhs = VAst::getExpr(node, VAst::Expr::RHS);
        auto lhsTy = getConstraintAnnotation(lhs);
        auto rhsTy = getConstraintAnnotation(rhs);

        if (!lhsTy->isSameConstraint(*rhsTy)) {
          return VAstTransform::visit_comparison_eq_expression(node);
        }

        // the new function name
        auto fnTy = ConstrainedFunctionType::get(ty, { lhsTy, rhsTy });
        GroupNodeCPtr func = findFunction("eq", *fnTy);

        if (!func) {
          ::std::function<GroupNodes(GroupNodes)> fnGenerator;

          if (lhsTy->self<ConstrainedStructType>()) {
            fnGenerator = [&](GroupNodes args) {
              return createStructEqualityFunction(ast, args.at(0), args.at(1));
            };
          } else if (lhsTy->self<ConstrainedTupleType>()) {
            fnGenerator = [&](GroupNodes args) {
              return createTupleEqualityFunction(ast, args.at(0), args.at(1));
            };
          } else if (lhsTy->self<ConstrainedUnionType>()) {
            fnGenerator = [&](GroupNodes args) {
              return createUnionEqualityFunction(ast, args.at(0), args.at(1));
            };
          } else if (lhsTy->self<ConstrainedOptType>()) {
            fnGenerator = [&](GroupNodes args) {
              return createOptionalEqualityFunction(ast, args.at(0), args.at(1));
            };
          } else if (lhsTy->self<ConstrainedArrayType>()) {
            fnGenerator = [&](GroupNodes args) {
              return createArrayEqualityFunction(ast, args.at(0), args.at(1));
            };
          }
          if (fnGenerator) {
            auto name = createUniqueFunctionName(_key, "eq", *fnTy);
            func = ast.createDefineFunction(name, mylang::variables::Variable::createGlobal(),
                *fnTy, node->span(), fnGenerator);
            // make sure to update newFunctions, before recursing, to make it is available
            functions.push_back(func);
          }
        }

        if (func) {
          auto name = getNameAnnotation(func);
          auto var = ast.createVariableExpression(nullptr, name, fnTy);
          return ast.createFunctionCallExpression(var, rewrite( { lhs, rhs }));
        } else {
          return VAstTransform::visit_comparison_eq_expression(node);
        }
      }

      GroupNodeCPtr RewriteAsFunction::rewrite(GroupNodeCPtr node)
      {
        auto res = visit(node);
        return res ? res : node;
      }

      GroupNodes RewriteAsFunction::rewrite(GroupNodes nodes)
      {
        GroupNodes res;
        for (auto node : nodes) {
          res.push_back(rewrite(node));
        }
        return res;
      }

      // locate a function with the specified signature and opcode
      GroupNodeCPtr RewriteAsFunction::findFunction(const ::std::string &opcode,
          const ConstrainedFunctionType &fnType) const
      {
        for (auto node : functions) {
          auto ty = getConstraintAnnotation(node);
          if (ty->isSameConstraint(fnType)) {
            auto name = getNameAnnotation(node);
            if (name->segments()[1]->localName() == opcode) {
              return node;
            }
          }
        }
        return nullptr;
      }

      GroupNodeCPtr RewriteAsFunction::apply(const GroupNodeCPtr &node)
      {
        RewriteAsFunction rw;
        rw.functions = ::std::move(collectFunctions(rw._key, node));
        rw.preApplyFunctionCount = rw.functions.size();
        auto res = rw.rewrite(node);
        if (rw.functions.size() > rw.preApplyFunctionCount) {
          auto decls = VAst::getRelations(node, VAst::Relation::DECLS);
          for (size_t i = rw.preApplyFunctionCount; i < rw.functions.size(); ++i) {
            decls.push_back(rw.rewrite(rw.functions.at(i)));
          }
          res = rw.ast.createRoot(decls);
        }
        return res;
      }
    }
  }
}
