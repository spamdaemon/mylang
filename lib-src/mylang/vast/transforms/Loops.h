#ifndef CLASS_MYLANG_VAST_TRANSFORMS_LOOPS_H
#define CLASS_MYLANG_VAST_TRANSFORMS_LOOPS_H
#include <mylang/defs.h>
#include <functional>

namespace mylang {
  namespace vast {
    namespace transforms {

      /**
       * This helper class is used to serve as an array index.
       */
      class Loops
      {
      public:
        Loops() = delete;

        /**
         * Create an array index for an array expression.
         * @param expr an array expression
         */
      private:
        Loops(const GroupNodeCPtr &expr);

        /**
         * Create a loop. The current element and index are passed to the body.
         * @param body a body function
         * @return a statement block
         */
      public:
        static GroupNodeCPtr createWhileLoop(const GroupNodeCPtr &expr,
            ::std::function<GroupNodes(GroupNodeCPtr element, GroupNodeCPtr index)

            > body);

        /** The declarations */
      public:
        GroupNodes decls;

        /** The reference to array */
      public:
        GroupNodeCPtr arrayRef;

        /** The reference to the counter */
      public:
        GroupNodeCPtr indexRef;

        /** The expression for the current element */
      public:
        GroupNodeCPtr elementRef;

        /** The counter type */
      public:
        GroupNodeCPtr type;
      };
    }
  }
}
#endif
