#include <idioma/ast/GroupNode.h>
#include <mylang/annotations.h>
#include <mylang/constraints/ConstrainedType.h>
#include <mylang/vast/transforms/AddGuards.h>
#include <mylang/vast/VAst.h>
#include <mylang/vast/VAstVisitor.h>
#include <cassert>
#include <memory>

namespace mylang {
  namespace vast {
    namespace transforms {

      using namespace mylang::constraints;

      AddGuards::AddGuards(bool ignoreGuards)
          : _ignoreGuards(ignoreGuards)
      {
      }

      AddGuards::~AddGuards()
      {
      }

      GroupNodeCPtr AddGuards::addGuards(GroupNodeCPtr node)
      {
        auto res = visit(node);
        return res ? res : node;
      }

      GroupNodeCPtr AddGuards::visit(GroupNodeCPtr node)
      {
        GroupNodeCPtr rewritten = VAstTransform::visit(node);
#if 0
                if (rewritten) {
                  auto fromkey = node->type.value<VAst::GroupType>();
                  auto tokey = rewritten->type.value<VAst::GroupType>();
                  ::std::cerr << "AddGuards " << (int) fromkey->key << " -> " << (int) tokey->key
                      << ::std::endl;
                }
#endif
        return rewritten;
      }

      GroupNodeCPtr AddGuards::visit_guard_expression(GroupNodeCPtr node)
      {
        if (_ignoreGuards) {
          return nullptr;
        } else {
          return VAstTransform::visit_guard_expression(node);
        }

      }

      GroupNodeCPtr AddGuards::visit_index_string_expression(GroupNodeCPtr node)
      {
        GroupNodeCPtr expr = addGuards(VAst::getExpr(node, VAst::Expr::LHS));
        GroupNodeCPtr index = addGuards(VAst::getExpr(node, VAst::Expr::RHS));

        return ast.createWithExpression(expr, [&](GroupNodeCPtr exprRef) {
          return ast.createWithExpression(index, [&](GroupNodeCPtr indexRef) {
                auto res = ast.createStringIndexExpression(exprRef, indexRef);
                res = ast.createGuardExpression(
                    ast.createComparisonLTExpression(indexRef,
                        ast.createGetStringLengthExpression(
                            exprRef)), res,
                    "string index out of bounds");
                return res;
              });
        });
      }

      GroupNodeCPtr AddGuards::visit_index_array_expression(GroupNodeCPtr node)
      {
        GroupNodeCPtr expr = addGuards(VAst::getExpr(node, VAst::Expr::LHS));
        GroupNodeCPtr index = addGuards(VAst::getExpr(node, VAst::Expr::RHS));

        return ast.createWithExpression(expr, [&](GroupNodeCPtr exprRef) {
          return ast.createWithExpression(index, [&](GroupNodeCPtr indexRef) {
                auto res = ast.createArrayIndexExpression(exprRef, indexRef);
                res = ast.createGuardExpression(
                    ast.createComparisonLTExpression(indexRef,
                        ast.createGetArrayLengthExpression(
                            exprRef)), res,
                    "array index out of bounds");
                return res;
              });
        });
      }

      GroupNodeCPtr AddGuards::visit_subrange_string_expression(GroupNodeCPtr node)
      {
        GroupNodeCPtr expr = addGuards(VAst::getExpr(node, VAst::Expr::EXPRESSION));
        GroupNodeCPtr begin = addGuards(VAst::getExpr(node, VAst::Expr::BEGIN));
        GroupNodeCPtr end = addGuards(VAst::getExpr(node, VAst::Expr::END));

        return ast.createWithExpression(expr, [&](GroupNodeCPtr exprRef) {
          return ast.createWithExpression(begin, [&](GroupNodeCPtr fromRef) {
                return ast.createWithExpression(end, [&](GroupNodeCPtr toRef) {

                      auto op = ast.createStringSubrangeExpression(exprRef,
                          fromRef,
                          toRef);
                      auto check_valid_range = ast.createComparisonLTEExpression(
                          fromRef, toRef);
                      auto check_in_range = ast.createComparisonLTEExpression(
                          toRef,
                          ast.createGetStringLengthExpression(exprRef));

                      return ast.createGuardExpression(check_in_range,
                          ast.createGuardExpression(
                              check_valid_range,
                              op,
                              "invalid substring range"),
                          "string index out of bounds");
                    });
              });
        });
      }

      GroupNodeCPtr AddGuards::visit_subrange_array_expression(GroupNodeCPtr node)
      {
        GroupNodeCPtr expr = addGuards(VAst::getExpr(node, VAst::Expr::EXPRESSION));
        GroupNodeCPtr begin = addGuards(VAst::getExpr(node, VAst::Expr::BEGIN));
        GroupNodeCPtr end = addGuards(VAst::getExpr(node, VAst::Expr::END));

        return ast.createWithExpression(expr, [&](GroupNodeCPtr exprRef) {
          return ast.createWithExpression(begin, [&](GroupNodeCPtr fromRef) {
                return ast.createWithExpression(end, [&](GroupNodeCPtr toRef) {

                      auto op = ast.createArraySubrangeExpression(exprRef,
                          fromRef, toRef);
                      auto check_valid_range = ast.createComparisonLTEExpression(
                          fromRef, toRef);
                      auto check_in_range = ast.createComparisonLTEExpression(
                          toRef, ast.createGetArrayLengthExpression(exprRef));

                      return ast.createGuardExpression(check_in_range,
                          ast.createGuardExpression(
                              check_valid_range,
                              op,
                              "invalid subarray range"),
                          "array index out of bounds");
                    });
              });
        });
      }

      GroupNodeCPtr AddGuards::visit_get_optional_value_expression(GroupNodeCPtr node)
      {
        GroupNodeCPtr expr = addGuards(VAst::getExpr(node, VAst::Expr::EXPRESSION));

        return ast.createWithExpression(expr, [&](GroupNodeCPtr exprRef) {
          auto get = ast.createGetOptionalValueExpression(exprRef);
          auto check = ast.createIsOptionalValuePresentExpression(exprRef);
          return ast.createGuardExpression(check, get,
              "optional not present");
        });
      }

      GroupNodeCPtr AddGuards::visit_partition_array_expression(GroupNodeCPtr node)
      {
        GroupNodeCPtr arrExpr = addGuards(VAst::getExpr(node, VAst::Expr::EXPRESSION));
        GroupNodeCPtr partExpr = addGuards(VAst::getExpr(node, VAst::Expr::SIZE));

        return ast.createWithExpression(arrExpr, [&](GroupNodeCPtr arrayRef) {
          return ast.createWithExpression(partExpr,
              [&](GroupNodeCPtr partRef) {
                auto len = ast.createGetArrayLengthExpression(
                    arrayRef);
                auto mod = ast.createIntegerModulusExpression(
                    len, partRef);
                auto check = ast.createComparisonEQExpression(
                    mod,
                    ast.createLiteralInteger(
                        0,
                        node->span()));
                return ast.createGuardExpression(
                    check,
                    ast.createPartitionArrayExpression(
                        arrayRef,
                        partRef),
                    "array size is not a multiple of the partition size");
              });
        });
      }

      GroupNodeCPtr AddGuards::visit_zip_arrays_expression(GroupNodeCPtr node)
      {
        GroupNodeCPtr left = addGuards(VAst::getExpr(node, VAst::Expr::LHS));
        GroupNodeCPtr right = addGuards(VAst::getExpr(node, VAst::Expr::RHS));

        return ast.createWithExpression(left, [&](GroupNodeCPtr leftRef) {
          return ast.createWithExpression(right, [&](GroupNodeCPtr rightRef) {
                auto zip = ast.createZipArraysExpression(leftRef, rightRef);
                auto check = ast.createComparisonEQExpression(
                    ast.createGetArrayLengthExpression(leftRef),
                    ast.createGetArrayLengthExpression(rightRef));
                return ast.createGuardExpression(
                    check, zip, "array bounds differ");
              });
        });
      }

      GroupNodeCPtr AddGuards::visit_integer_divide_expression(GroupNodeCPtr node)
      {
        GroupNodeCPtr left = addGuards(VAst::getExpr(node, VAst::Expr::LHS));
        GroupNodeCPtr right = addGuards(VAst::getExpr(node, VAst::Expr::RHS));

        return ast.createWithExpression(right, [&](GroupNodeCPtr rightRef) {
          auto op = ast.createIntegerDivideExpression(left, rightRef);
          auto check = ast.createComparisonNEQExpression(rightRef,
              ast.createLiteralInteger(
                  0,
                  node->span()));
          return ast.createGuardExpression(check, op,
              "integer division by zero");
        });
      }

      GroupNodeCPtr AddGuards::visit_integer_modulus_expression(GroupNodeCPtr node)
      {
        GroupNodeCPtr left = addGuards(VAst::getExpr(node, VAst::Expr::LHS));
        GroupNodeCPtr right = addGuards(VAst::getExpr(node, VAst::Expr::RHS));

        return ast.createWithExpression(right, [&](GroupNodeCPtr rightRef) {
          auto op = ast.createIntegerModulusExpression(left, rightRef);
          auto check = ast.createComparisonNEQExpression(rightRef,
              ast.createLiteralInteger(
                  0,
                  node->span()));
          return ast.createGuardExpression(check, op,
              "integer modulus by zero");
        });
      }

      GroupNodeCPtr AddGuards::visit_integer_remainder_expression(GroupNodeCPtr node)
      {
        GroupNodeCPtr left = addGuards(VAst::getExpr(node, VAst::Expr::LHS));
        GroupNodeCPtr right = addGuards(VAst::getExpr(node, VAst::Expr::RHS));

        return ast.createWithExpression(right, [&](GroupNodeCPtr rightRef) {
          auto op = ast.createIntegerRemainderExpression(left, rightRef);
          auto check = ast.createComparisonNEQExpression(rightRef,
              ast.createLiteralInteger(
                  0,
                  node->span()));
          return ast.createGuardExpression(check, op,
              "integer remainder by zero");
        });
      }

      GroupNodeCPtr AddGuards::visit_real_divide_expression(GroupNodeCPtr node)
      {
        GroupNodeCPtr left = addGuards(VAst::getExpr(node, VAst::Expr::LHS));
        GroupNodeCPtr right = addGuards(VAst::getExpr(node, VAst::Expr::RHS));

        return ast.createWithExpression(right, [&](GroupNodeCPtr rightRef) {
          auto op = ast.createRealDivideExpression(left, rightRef);
          auto check = ast.createComparisonNEQExpression(rightRef,
              ast.createLiteralReal(
                  0,
                  node->span()));
          return ast.createGuardExpression(check, op,
              "real division by zero");
        });
      }

      GroupNodeCPtr AddGuards::visit_real_remainder_expression(GroupNodeCPtr node)
      {
        GroupNodeCPtr left = addGuards(VAst::getExpr(node, VAst::Expr::LHS));
        GroupNodeCPtr right = addGuards(VAst::getExpr(node, VAst::Expr::RHS));

        return ast.createWithExpression(right, [&](GroupNodeCPtr rightRef) {
          auto op = ast.createRealRemainderExpression(left, rightRef);
          auto check = ast.createComparisonNEQExpression(rightRef,
              ast.createLiteralReal(
                  0,
                  node->span()));
          return ast.createGuardExpression(check, op,
              "real remainder by zero");
        });
      }

      GroupNodeCPtr AddGuards::apply(const GroupNodeCPtr &node, bool ignoreGuards)
      {
        AddGuards rw(ignoreGuards);
        return rw.addGuards(node);
      }
    }
  }
}
