#ifndef CLASS_MYLANG_VAST_TRANSFORMS_ADDGUARDS_H
#define CLASS_MYLANG_VAST_TRANSFORMS_ADDGUARDS_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_VALIDATION_VASTTRANSFORM_H
#include <mylang/vast/VAstTransform.h>
#endif

namespace mylang {
  namespace vast {
    namespace transforms {

      /**
       * Rewrite node in terms of other, simpler node types.
       */
      class AddGuards: public VAstTransform
      {
        /**
         * Create a code builder.
         */
      public:
        AddGuards(bool ignoreGuards);

        /**
         * Destructor
         */
      public:
        ~AddGuards();

        /**
         * Add guards to all nodes recursively. If ignoreGuards
         * is true, then nodes below a guard expression are ignored.
         * @param node a node
         * @param ignoreGuards
         * @return a guarded node
         */
      public:
        static GroupNodeCPtr apply(const GroupNodeCPtr &node, bool ignoreGuards = false);

      private:
        GroupNodeCPtr visit_guard_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_index_string_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_index_array_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_subrange_string_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_subrange_array_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_get_optional_value_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_partition_array_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_zip_arrays_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_integer_divide_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_integer_modulus_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_integer_remainder_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_real_divide_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_real_remainder_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit(GroupNodeCPtr node) override final;

      private:
        GroupNodeCPtr addGuards(GroupNodeCPtr node);

        /** A flag to ignore guards */
      private:
        const bool _ignoreGuards;

      };
    }
  }
}
#endif
