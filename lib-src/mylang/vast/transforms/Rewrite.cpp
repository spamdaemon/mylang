#include <idioma/ast/GroupNode.h>
#include <mylang/annotations.h>
#include <mylang/constraints/ConstrainedNamedType.h>
#include <mylang/constraints/ConstrainedArrayType.h>
#include <mylang/constraints/ConstrainedByteType.h>
#include <mylang/constraints/ConstrainedOptType.h>
#include <mylang/constraints/ConstrainedType.h>
#include <mylang/vast/transforms/Rewrite.h>
#include <mylang/vast/VAst.h>
#include <mylang/vast/VAstVisitor.h>
#include <functional>
#include <memory>
#include <vector>

namespace mylang {
  namespace vast {
    namespace transforms {

      using namespace mylang::constraints;

      Rewrite::Rewrite()
      {
      }

      Rewrite::~Rewrite()
      {
      }

      GroupNodeCPtr Rewrite::rewrite(GroupNodeCPtr node)
      {
        auto res = visit(node);
        return res ? res : node;
      }

      GroupNodeCPtr Rewrite::visit(GroupNodeCPtr node)
      {
        GroupNodeCPtr rewritten = VAstTransform::visit(node);
#if 0
                if (rewritten) {
                  auto fromkey = node->type.value<VAst::GroupType>();
                  auto tokey = rewritten->type.value<VAst::GroupType>();
                  ::std::cerr << "Rewrite " << (int) fromkey->key << " -> " << (int) tokey->key
                      << ::std::endl;
                }
#endif
        return rewritten;
      }

      GroupNodeCPtr Rewrite::visit_new_byte_expression(GroupNodeCPtr node)
      {
        auto args = VAst::getExprs(node, VAst::Expr::ARGUMENTS);
        if (args.size() == 1) {
          auto arg = args.at(0);
          auto argTy = getConstraintAnnotation(arg);
          if (ConstrainedByteType::getPeerBitArray()->isSameConstraint(*argTy)) {
            return rewrite(ast.createGetByteFromBitsExpression(arg));
          }
        }
        return VAstTransform::visit_new_byte_expression(node);
      }

      GroupNodeCPtr Rewrite::visit_comparison_eq_expression(GroupNodeCPtr node)
      {
        auto lhs = VAst::getExpr(node, VAst::Expr::LHS);
        auto rhs = VAst::getExpr(node, VAst::Expr::RHS);

        auto lhsTy = getConstraintAnnotation(lhs);
        auto rhsTy = getConstraintAnnotation(rhs);

        GroupNodeCPtr res;

        // for named types, just compare the root types
        if (lhsTy->isSameConstraint(*rhsTy)) {
          if (lhsTy->self<ConstrainedNamedType>()) {
            lhs = ast.applyImplicitCast(lhsTy->roottype(), lhs);
            rhs = ast.applyImplicitCast(rhsTy->roottype(), rhs);
            res = ast.createComparisonEQExpression(lhs, rhs);
            res = rewrite(res);
          }
        } else if (lhsTy->isBaseTypeOf(*rhsTy) || rhsTy->isBaseTypeOf(*lhsTy)) {
          lhs = ast.applyImplicitCast(lhsTy->roottype(), lhs);
          rhs = ast.applyImplicitCast(rhsTy->roottype(), rhs);
          res = ast.createComparisonEQExpression(lhs, rhs);
          res = rewrite(res);
        }

        if (!res) {
          res = VAstTransform::visit_comparison_eq_expression(node);
        }
        return res;
      }

      GroupNodeCPtr Rewrite::visit_stringify_expression(GroupNodeCPtr node)
      {
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto optTy = getConstraintAnnotation(expr)->self<ConstrainedOptType>();
        if (!optTy) {
          return VAstTransform::visit_stringify_expression(node);
        }
        expr = ast.createFlattenOptionalExpression(expr);
        auto res = ast.createWithExpression(expr, [&](GroupNodeCPtr e) {
          auto cond = ast.createIsOptionalValuePresentExpression(e);
          auto get = ast.createStringifyExpression(ast.createGetOptionalValueExpression(e));
          auto blank = ast.createLiteralString("", node->span());
          return ast.createConditionalExpression(cond, get, blank);
        });
        return rewrite(res);
      }

      GroupNodeCPtr Rewrite::visit_get_as_roottype_expression(GroupNodeCPtr node)
      {
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto ty = getConstraintAnnotation(expr);
        if (ty->isSameConstraint(*ty->roottype())) {
          return rewrite(expr);
        } else {
          return VAstTransform::visit_get_as_roottype_expression(node);
        }
      }

      GroupNodeCPtr Rewrite::visit_get_as_basetype_expression(GroupNodeCPtr node)
      {
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        auto baseTy = getConstraintAnnotation(expr)->basetype();
        // if there is no basetype, then the expression's type is the same
        if (!baseTy) {
          return rewrite(expr);
        } else {
          return VAstTransform::visit_get_as_basetype_expression(node);
        }
      }

      GroupNodeCPtr Rewrite::visit_expression_statement(GroupNodeCPtr node)
      {
        auto expr = VAst::getExpr(node, VAst::Expr::EXPRESSION);
        if (expr->hasType(VAst::GroupType::WITH_EXPRESSION)) {
          ::std::vector<GroupNodeCPtr> S;
          for (auto decl : VAst::getRelations(expr, VAst::Relation::DECLS)) {
            auto tok = VAst::getToken(decl, VAst::Token::IDENTIFIER);
            auto id = getNameAnnotation(decl);
            auto vinfo = getVariableAnnotation(decl);
            auto ty = VAst::getRelation(decl, VAst::Relation::TYPE);
            auto initializer = VAst::getExpr(decl, VAst::Expr::INITIALIZER);
            S.push_back(ast.createDefineVariable(tok, id, vinfo, ty, initializer));
          }
          auto e = VAst::getExpr(expr, VAst::Expr::EXPRESSION);
          S.push_back(ast.createExpressionStatement(e));
          auto res = ast.createStatementBlock(S);
          return rewrite(res);
        } else if (expr->hasType(VAst::GroupType::CONDITIONAL_EXPRESSION)) {
          auto cond = VAst::getExpr(expr, VAst::Expr::CONDITION);
          auto iftrue = VAst::getExpr(expr, VAst::Expr::IFTRUE);
          auto iffalse = VAst::getExpr(expr, VAst::Expr::IFFALSE);
          iftrue = ast.createExpressionStatement(iftrue);
          iffalse = ast.createExpressionStatement(iffalse);
          auto res = ast.createIfStatement(cond, iftrue, iffalse);
          return rewrite(res);
        } else if (expr->hasType(VAst::GroupType::VOID_EXPRESSION)) {
          return ast.createStatementList( { });
        } else {
          return VAstTransform::visit_expression_statement(node);
        }
      }

      GroupNodeCPtr Rewrite::visit_literal_nil(GroupNodeCPtr node)
      {
        auto ty = getConstraintAnnotation(node)->self<ConstrainedOptType>();
        if (ty->element->isAnyType()) {
          return nullptr;
        }
        auto typeNode = ast.createType(ty, node->span());
        return ast.createNewOptionalExpression(typeNode, { });
      }

      GroupNodeCPtr Rewrite::visit_literal_empty_array(GroupNodeCPtr node)
      {
        auto ty = getConstraintAnnotation(node)->self<ConstrainedArrayType>();
        if (ty->element->isAnyType()) {
          return nullptr;
        }
        auto typeNode = ast.createType(ty, node->span());
        return ast.createNewArrayExpression(typeNode, { });
      }

      GroupNodeCPtr Rewrite::apply(const GroupNodeCPtr &node)
      {
        Rewrite rw;
        return rw.rewrite(node);
      }
    }
  }
}
