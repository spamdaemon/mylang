#include <mylang/vast/transforms/AddDefaultConstructors.h>
#include <mylang/annotations.h>
#include <idioma/ast/GroupNode.h>

namespace mylang {
  namespace vast {
    namespace transforms {

      AddDefaultConstructors::AddDefaultConstructors()
      {
      }

      AddDefaultConstructors::~AddDefaultConstructors()
      {
      }

      GroupNodeCPtr AddDefaultConstructors::visit_def_process(GroupNodeCPtr node)
      {
        GroupNodes statements;
        bool createDefaultConstructor = true;
        for (auto s : VAst::getRelations(node, VAst::Relation::STATEMENTS)) {
          statements.push_back(s);
          if (s->hasType(VAst::GroupType::PROCESS_CONSTRUCTOR)) {
            createDefaultConstructor = false;
            break;
          }
        }

        if (!createDefaultConstructor) {
          return VAstTransform::visit_def_process(node);
        }

        auto name = mylang::names::Name::create();
        GroupNodes parameters;
        GroupNodeCPtr callOwnConstructor = nullptr;
        GroupNodeCPtr constructorBlock = ast.createStatementBlock( { });

        auto ctor = ast.createProcessConstructor(name, parameters, callOwnConstructor,
            constructorBlock);
        statements.push_back(ctor);

        GroupNodeCPtr proc = ast.createDefineProcess(VAst::getToken(node, VAst::Token::IDENTIFIER),
            getNameAnnotation(node), VAst::getRelation(node, VAst::Relation::TYPE), statements);

        auto res = VAstTransform::visit_def_process(proc);
        return res ? res : proc;
      }

      GroupNodeCPtr AddDefaultConstructors::apply(const GroupNodeCPtr &node)
      {
        AddDefaultConstructors rw;
        GroupNodeCPtr res = rw.visit(node);
        return res ? res : node;
      }
    }
  }
}
