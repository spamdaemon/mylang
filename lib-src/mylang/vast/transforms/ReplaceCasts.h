#ifndef CLASS_MYLANG_VAST_TRANSFORMS_REPLACECASTS_H
#define CLASS_MYLANG_VAST_TRANSFORMS_REPLACECASTS_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_VALIDATION_VASTTRANSFORM_H
#include <mylang/vast/VAstTransform.h>
#endif

namespace mylang {
  namespace vast {
    namespace transforms {

      /**
       * Replace node in terms of other, simpler node types.
       */
      class ReplaceCasts: public VAstTransform
      {

        /**
         * Create a code builder.
         */
      public:
        ReplaceCasts();

        /**
         * Destructor
         */
      public:
        ~ReplaceCasts();

        /**
         * Apply the normalization to the specified node.
         * @param node a node
         * @return a normalized node
         */
      public:
        static GroupNodeCPtr apply(const GroupNodeCPtr &node);

        /**
         * Replace a checked cast if the expression to be casted is the nil or [] (empty array) literals.
         */
        GroupNodeCPtr visit_implicit_basetypecast_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_safe_cast_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_implicit_cast_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit_unsafe_cast_expression(GroupNodeCPtr node) override;

        GroupNodeCPtr visit(GroupNodeCPtr node) override final;

      private:
        GroupNodeCPtr replace(GroupNodeCPtr node);

      };
    }
  }
}
#endif
