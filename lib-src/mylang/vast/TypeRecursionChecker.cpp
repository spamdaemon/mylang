#include <mylang/constraints.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/vast/TypeRecursionChecker.h>
#include <cassert>
#include <set>

namespace mylang {
  namespace vast {

    namespace {
      using namespace mylang::constraints;

      struct Checker: public ConstrainedTypeVisitor
      {
        Checker()
            : ok(true)
        {
        }

        ~Checker()
        {
        }

        void visitAny(const ::std::shared_ptr<const ConstrainedAnyType>&)
        override
        {
        }

        void visitBit(const ::std::shared_ptr<const ConstrainedBitType>&)
        override
        {
        }

        void visitBoolean(const ::std::shared_ptr<const ConstrainedBooleanType>&)
        override
        {
        }

        void visitByte(const ::std::shared_ptr<const ConstrainedByteType>&)
        override
        {
        }

        void visitChar(const ::std::shared_ptr<const ConstrainedCharType>&)
        override
        {
        }

        void visitReal(const ::std::shared_ptr<const ConstrainedRealType>&)
        override
        {
        }

        void visitInteger(const ::std::shared_ptr<const ConstrainedIntegerType>&)
        override
        {
        }

        void visitString(const ::std::shared_ptr<const ConstrainedStringType>&)
        override
        {
        }

        void visitArray(const ::std::shared_ptr<const ConstrainedArrayType> &arrTy)
        override
        {
          if (arrTy->bounds.min() == 0) {
            return;
          }
          arrTy->element->accept(*this);
        }

        void visitVoid(const ::std::shared_ptr<const ConstrainedVoidType>&)
        override
        {
        }

        void visitGeneric(const ::std::shared_ptr<const ConstrainedGenericType>&)
        override
        {
        }

        void visitTuple(const ::std::shared_ptr<const ConstrainedTupleType> &tupleTy)
        override
        {
          for (auto t : tupleTy->types) {
            t->accept(*this);
          }
        }

        void visitStruct(const ::std::shared_ptr<const ConstrainedStructType> &structTy)
        override
        {
          for (auto m : structTy->members) {
            m.constraint->accept(*this);
          }
        }

        void visitUnion(const ::std::shared_ptr<const ConstrainedUnionType> &unionTy)
        {
          unionTy->discriminant.constraint->accept(*this);
          // special case happens when we have a single union member
          if (unionTy->members.size() == 1) {
            unionTy->members.at(0).constraint->accept(*this);
          }
        }

        void visitFunction(const ::std::shared_ptr<const ConstrainedFunctionType>&)
        override
        {
        }

        void visitOpt(const ::std::shared_ptr<const ConstrainedOptType>&)
        override
        {
        }

        void visitMutable(const ::std::shared_ptr<const ConstrainedMutableType> &mutableTy)
        override
        {
          // FIXME: once things are mutable, we can create cycles!!
          mutableTy->element->accept(*this);
        }

        void visitNamedType(const ::std::shared_ptr<const ConstrainedNamedType> &t)
        override
        {
          if (constraints.insert(t->name()).second) {
            if (t->isResolved()) {
              auto resolved = t->resolve();
              resolved->accept(*this);
              constraints.erase(t->name());
            }
          } else {
            // infinite recursion detected
            ok = false;
          }
        }

        void visitInputType(const ::std::shared_ptr<const ConstrainedInputType>&)
        override
        {
        }

        void visitOutputType(const ::std::shared_ptr<const ConstrainedOutputType>&)
        override
        {
        }

        void visitProcessType(const ::std::shared_ptr<const ConstrainedProcessType>&)
        override
        {
        }

        ::std::set<NamePtr> constraints;
        bool ok;
      };
    }

    bool TypeRecursionChecker::check(const ConstrainedTypePtr &type)
    {
      Checker ctor;
      type->accept(ctor);
      return ctor.ok;
    }
  }
}
