#include <mylang/vast/ValidationError.h>
#include <sstream>

namespace mylang {
  namespace vast {

    namespace {
      static ::std::string createErrorText(const SourceLocation &src, const ::std::string &text)
      {
        ::std::ostringstream out;
        src.print(out);
        out << ':' << text;
        return out.str();
      }
    }

    ValidationError::ValidationError(const SourceLocation &src, const ::std::string &text)
        : ::std::runtime_error(createErrorText(src, text)), source(src), message(text)
    {
    }

    ValidationError::ValidationError(const ::std::string &text)
        : ::std::runtime_error(text), message(text)
    {
    }

    ValidationError::~ValidationError()
    {
    }
  }
}
