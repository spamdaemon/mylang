#include <mylang/vast/InternalError.h>
#include <sstream>

namespace mylang {
  namespace vast {

    InternalError::InternalError(const ::idioma::ast::Node::CPtr &src, const ::std::string &text)
        : ValidationError(src, text)
    {
    }

    InternalError::InternalError(const ::std::string &text)
        : ValidationError(text)
    {
    }

    InternalError::~InternalError()
    {
    }
  }
}
