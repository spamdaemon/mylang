#ifndef CLASS_MYLANG_VALIDATION_VASTTRANSFORM_H
#define CLASS_MYLANG_VALIDATION_VASTTRANSFORM_H

#ifndef CLASS_MYLANG_VALIDATION_VAST_H
#include <mylang/vast/VAst.h>
#endif

#ifndef CLASS_MYLANG_VALIDATION_VASTVISITOR_H
#include <mylang/vast/VAstVisitor.h>
#endif

namespace mylang {
  namespace vast {

    /**
     * The AstTransform implements the visitor interface to enable tree transformations.
     * If a visit function does not modify the node, then it returns a nullptr to indicate that the tree was not modified.
     * If the tree was modified, then a new node must be returned which will cause the caller in turn to be regenerated.
     * If the visit function on the top-level node returns nullptr, then the tree is unmodified.
     */
    class VAstTransform: public VAstVisitor<GroupNodeCPtr>
    {
    public:
      VAstTransform();

      ~VAstTransform();

    protected:
      GroupNodeCPtr visit_root(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_literal_bit(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_literal_byte(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_literal_integer(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_literal_real(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_literal_boolean(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_literal_array(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_literal_tuple(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_literal_tuple_member(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_literal_struct(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_literal_struct_member(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_literal_string(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_literal_char(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_literal_nil(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_literal_empty_array(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_guard_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_void_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_named_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_input_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_output_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_variable_reference_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_new_process_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_new_bit_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_new_boolean_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_new_byte_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_new_char_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_new_integer_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_new_real_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_new_string_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_new_function_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_new_array_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_new_mutable_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_new_optional_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_new_struct_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_new_tuple_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_new_union_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_new_namedtype_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_integer_clamp_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_integer_wrap_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_integer_negate_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_integer_add_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_integer_subtract_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_integer_multiply_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_integer_divide_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_integer_modulus_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_integer_remainder_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_real_negate_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_real_add_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_real_subtract_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_real_multiply_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_real_divide_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_real_remainder_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_loop_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_with_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_and_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_or_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_xor_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_conditional_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_short_circuit_and_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_short_circuit_or_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_not_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_comparison_eq_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_comparison_neq_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_comparison_lt_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_comparison_lte_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_comparison_gt_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_comparison_gte_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_process_member_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_union_member_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_struct_member_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_tuple_member_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_optify_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_optional_value_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_mutable_value_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_index_string_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_index_array_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_subrange_string_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_subrange_array_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_call_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_lambda_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_concatenate_arrays_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_concatenate_strings_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_merge_tuples_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_zip_arrays_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_transform_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_as_roottype_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_as_basetype_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_checked_cast_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_safe_cast_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_implicit_cast_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_implicit_basetypecast_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_unsafe_cast_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_orelse_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_try_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_encode_bit_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_encode_boolean_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_encode_byte_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_encode_char_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_encode_integer_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_encode_real_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_encode_string_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_decode_bit_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_decode_boolean_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_decode_byte_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_decode_char_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_decode_integer_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_decode_real_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_decode_string_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_def_type(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_def_namespace(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_def_exported_function(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_def_function(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_def_process(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_def_variable(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_process_block(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_process_constructor(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_function_body(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_clear_port_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_read_port_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_write_port_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_wait_port_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_close_port_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_block_read_events_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_block_write_events_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_has_new_input_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_is_present_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_is_input_closed_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_is_output_closed_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_is_port_readable_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_is_port_writable_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_array_length_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_string_length_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_string_to_chars_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_char_to_string_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_interpolate_text_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_stringify_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_fold_array_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_map_array_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_pad_array_expression(GroupNodeCPtr node) override;
      GroupNodeCPtr visit_trim_array_expression(GroupNodeCPtr node) override;
      GroupNodeCPtr visit_take_array_expression(GroupNodeCPtr node) override;
      GroupNodeCPtr visit_drop_array_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_filter_array_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_find_array_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_reverse_array_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_map_optional_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_flatten_array_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_partition_array_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_flatten_optional_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_byte_as_unsigned_integer_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_byte_as_signed_integer_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_bit_from_bits_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_boolean_from_bits_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_byte_from_bits_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_char_from_bits_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_integer_from_bits_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_real_from_bits_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_bits_from_bit_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_bits_from_boolean_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_bits_from_byte_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_bits_from_char_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_bits_from_integer_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_bits_from_real_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_head_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_get_tail_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_convert_to_tuple_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_struct_from_tuple_expression(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_type(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_statement_list(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_statement_block(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_assert_statement(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_break_statement(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_continue_statement(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_call_constructor_statement(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_expression_statement(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_foreach_statement(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_if_statement(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_log_statement(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_return_statement(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_set_statement(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_throw_statement(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_try_statement(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_update_statement(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_wait_statement(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_while_statement(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_def_generic_function(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_instantiate_generic_function(GroupNodeCPtr node) override;

      GroupNodeCPtr visit_inline_group(GroupNodeCPtr node) override;

      /**
       * Visit the specified nodes and return a new group node with the same kinds of groups but new children.
       * @param nodes the current nodes
       * @return a node of some time or nullptr if no change
       * @return the number of nodes that were changed for which the visit function returned non-nullptr
       */
    protected:
      virtual GroupNodeCPtr visitChildNodes(const GroupNodeCPtr &nodes);

      /** An instance of an AST that can be used to create an new Ast */
    protected:
      VAst ast;
    };
  }
}
#endif
