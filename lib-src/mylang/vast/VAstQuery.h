#ifndef CLASS_MYLANG_VALIDATION_VASTQUERY_H
#define CLASS_MYLANG_VALIDATION_VASTQUERY_H
#include <mylang/defs.h>

#ifndef CLASS_MYLANG_VALIDATION_VASTVISITOR_H
#include <mylang/vast/VAstVisitor.h>
#endif

namespace mylang {
  namespace vast {

    /**
     * The VAstQuery implements the visitor interface to enable tree transformations.
     * If a visit function does not modify the node, then it returns a nullptr to indicate that the tree was not modified.
     * If the tree was modified, then a new node must be returned which will cause the caller in turn to be regenerated.
     * If the visit function on the top-level node returns nullptr, then the tree is unmodified.
     */
    class VAstQuery: public VAstVisitor<void>
    {
    public:
      VAstQuery();

      ~VAstQuery();

    protected:
      void visit_root(GroupNodeCPtr node) override;

      void visit_literal_bit(GroupNodeCPtr node) override;

      void visit_literal_byte(GroupNodeCPtr node) override;

      void visit_literal_integer(GroupNodeCPtr node) override;

      void visit_literal_real(GroupNodeCPtr node) override;

      void visit_literal_boolean(GroupNodeCPtr node) override;

      void visit_literal_array(GroupNodeCPtr node) override;

      void visit_literal_tuple(GroupNodeCPtr node) override;

      void visit_literal_tuple_member(GroupNodeCPtr node) override;

      void visit_literal_struct(GroupNodeCPtr node) override;

      void visit_literal_struct_member(GroupNodeCPtr node) override;

      void visit_literal_string(GroupNodeCPtr node) override;

      void visit_literal_char(GroupNodeCPtr node) override;

      void visit_literal_nil(GroupNodeCPtr node) override;

      void visit_literal_empty_array(GroupNodeCPtr node) override;

      void visit_void_expression(GroupNodeCPtr node) override;

      void visit_named_expression(GroupNodeCPtr node) override;

      void visit_guard_expression(GroupNodeCPtr node) override;

      void visit_get_input_expression(GroupNodeCPtr node) override;

      void visit_get_output_expression(GroupNodeCPtr node) override;

      void visit_variable_reference_expression(GroupNodeCPtr node) override;

      void visit_new_process_expression(GroupNodeCPtr node) override;

      void visit_new_bit_expression(GroupNodeCPtr node) override;

      void visit_new_boolean_expression(GroupNodeCPtr node) override;

      void visit_new_byte_expression(GroupNodeCPtr node) override;

      void visit_new_char_expression(GroupNodeCPtr node) override;

      void visit_new_integer_expression(GroupNodeCPtr node) override;

      void visit_new_real_expression(GroupNodeCPtr node) override;

      void visit_new_string_expression(GroupNodeCPtr node) override;

      void visit_new_function_expression(GroupNodeCPtr node) override;

      void visit_new_array_expression(GroupNodeCPtr node) override;

      void visit_new_mutable_expression(GroupNodeCPtr node) override;

      void visit_new_optional_expression(GroupNodeCPtr node) override;

      void visit_new_struct_expression(GroupNodeCPtr node) override;

      void visit_new_tuple_expression(GroupNodeCPtr node) override;

      void visit_new_union_expression(GroupNodeCPtr node) override;

      void visit_new_namedtype_expression(GroupNodeCPtr node) override;

      void visit_integer_clamp_expression(GroupNodeCPtr node) override;

      void visit_integer_wrap_expression(GroupNodeCPtr node) override;

      void visit_integer_negate_expression(GroupNodeCPtr node) override;

      void visit_integer_add_expression(GroupNodeCPtr node) override;

      void visit_integer_subtract_expression(GroupNodeCPtr node) override;

      void visit_integer_multiply_expression(GroupNodeCPtr node) override;

      void visit_integer_divide_expression(GroupNodeCPtr node) override;

      void visit_integer_modulus_expression(GroupNodeCPtr node) override;

      void visit_integer_remainder_expression(GroupNodeCPtr node) override;

      void visit_real_negate_expression(GroupNodeCPtr node) override;

      void visit_real_add_expression(GroupNodeCPtr node) override;

      void visit_real_subtract_expression(GroupNodeCPtr node) override;

      void visit_real_multiply_expression(GroupNodeCPtr node) override;

      void visit_real_divide_expression(GroupNodeCPtr node) override;

      void visit_real_remainder_expression(GroupNodeCPtr node) override;

      void visit_loop_expression(GroupNodeCPtr node) override;

      void visit_with_expression(GroupNodeCPtr node) override;

      void visit_and_expression(GroupNodeCPtr node) override;

      void visit_or_expression(GroupNodeCPtr node) override;

      void visit_xor_expression(GroupNodeCPtr node) override;

      void visit_conditional_expression(GroupNodeCPtr node) override;

      void visit_short_circuit_and_expression(GroupNodeCPtr node) override;

      void visit_short_circuit_or_expression(GroupNodeCPtr node) override;

      void visit_not_expression(GroupNodeCPtr node) override;

      void visit_comparison_eq_expression(GroupNodeCPtr node) override;

      void visit_comparison_neq_expression(GroupNodeCPtr node) override;

      void visit_comparison_lt_expression(GroupNodeCPtr node) override;

      void visit_comparison_lte_expression(GroupNodeCPtr node) override;

      void visit_comparison_gt_expression(GroupNodeCPtr node) override;

      void visit_comparison_gte_expression(GroupNodeCPtr node) override;

      void visit_process_member_expression(GroupNodeCPtr node) override;

      void visit_union_member_expression(GroupNodeCPtr node) override;

      void visit_struct_member_expression(GroupNodeCPtr node) override;

      void visit_tuple_member_expression(GroupNodeCPtr node) override;

      void visit_optify_expression(GroupNodeCPtr node) override;

      void visit_get_optional_value_expression(GroupNodeCPtr node) override;

      void visit_get_mutable_value_expression(GroupNodeCPtr node) override;

      void visit_index_string_expression(GroupNodeCPtr node) override;

      void visit_index_array_expression(GroupNodeCPtr node) override;

      void visit_subrange_string_expression(GroupNodeCPtr node) override;

      void visit_subrange_array_expression(GroupNodeCPtr node) override;

      void visit_call_expression(GroupNodeCPtr node) override;

      void visit_lambda_expression(GroupNodeCPtr node) override;

      void visit_concatenate_arrays_expression(GroupNodeCPtr node) override;

      void visit_concatenate_strings_expression(GroupNodeCPtr node) override;

      void visit_merge_tuples_expression(GroupNodeCPtr node) override;

      void visit_zip_arrays_expression(GroupNodeCPtr node) override;

      void visit_transform_expression(GroupNodeCPtr node) override;

      void visit_get_as_roottype_expression(GroupNodeCPtr node) override;

      void visit_get_as_basetype_expression(GroupNodeCPtr node) override;

      void visit_checked_cast_expression(GroupNodeCPtr node) override;

      void visit_safe_cast_expression(GroupNodeCPtr node) override;

      void visit_implicit_cast_expression(GroupNodeCPtr node) override;

      void visit_implicit_basetypecast_expression(GroupNodeCPtr node) override;

      void visit_unsafe_cast_expression(GroupNodeCPtr node) override;

      void visit_orelse_expression(GroupNodeCPtr node) override;

      void visit_try_expression(GroupNodeCPtr node) override;

      void visit_def_type(GroupNodeCPtr node) override;

      void visit_def_namespace(GroupNodeCPtr node) override;

      void visit_def_exported_function(GroupNodeCPtr node) override;

      void visit_def_function(GroupNodeCPtr node) override;

      void visit_def_process(GroupNodeCPtr node) override;

      void visit_def_variable(GroupNodeCPtr node) override;

      void visit_process_block(GroupNodeCPtr node) override;

      void visit_process_constructor(GroupNodeCPtr node) override;

      void visit_function_body(GroupNodeCPtr node) override;

      void visit_clear_port_expression(GroupNodeCPtr node) override;

      void visit_read_port_expression(GroupNodeCPtr node) override;

      void visit_write_port_expression(GroupNodeCPtr node) override;

      void visit_wait_port_expression(GroupNodeCPtr node) override;

      void visit_close_port_expression(GroupNodeCPtr node) override;

      void visit_block_read_events_expression(GroupNodeCPtr node) override;

      void visit_block_write_events_expression(GroupNodeCPtr node) override;

      void visit_is_present_expression(GroupNodeCPtr node) override;

      void visit_is_input_closed_expression(GroupNodeCPtr node) override;

      void visit_is_output_closed_expression(GroupNodeCPtr node) override;

      void visit_is_port_readable_expression(GroupNodeCPtr node) override;

      void visit_is_port_writable_expression(GroupNodeCPtr node) override;

      void visit_has_new_input_expression(GroupNodeCPtr node) override;

      void visit_array_length_expression(GroupNodeCPtr node) override;

      void visit_string_length_expression(GroupNodeCPtr node) override;

      void visit_string_to_chars_expression(GroupNodeCPtr node) override;

      void visit_char_to_string_expression(GroupNodeCPtr node) override;

      void visit_interpolate_text_expression(GroupNodeCPtr node) override;

      void visit_stringify_expression(GroupNodeCPtr node) override;

      void visit_fold_array_expression(GroupNodeCPtr node) override;

      void visit_map_array_expression(GroupNodeCPtr node) override;

      void visit_pad_array_expression(GroupNodeCPtr node) override;
      void visit_trim_array_expression(GroupNodeCPtr node) override;
      void visit_take_array_expression(GroupNodeCPtr node) override;
      void visit_drop_array_expression(GroupNodeCPtr node) override;

      void visit_filter_array_expression(GroupNodeCPtr node) override;

      void visit_find_array_expression(GroupNodeCPtr node) override;

      void visit_reverse_array_expression(GroupNodeCPtr node) override;

      void visit_map_optional_expression(GroupNodeCPtr node) override;

      void visit_flatten_array_expression(GroupNodeCPtr node) override;

      void visit_partition_array_expression(GroupNodeCPtr node) override;

      void visit_flatten_optional_expression(GroupNodeCPtr node) override;

      void visit_get_byte_as_unsigned_integer_expression(GroupNodeCPtr node) override;

      void visit_get_byte_as_signed_integer_expression(GroupNodeCPtr node) override;

      void visit_get_bit_from_bits_expression(GroupNodeCPtr node) override;

      void visit_get_boolean_from_bits_expression(GroupNodeCPtr node) override;

      void visit_get_byte_from_bits_expression(GroupNodeCPtr node) override;

      void visit_get_char_from_bits_expression(GroupNodeCPtr node) override;

      void visit_get_integer_from_bits_expression(GroupNodeCPtr node) override;

      void visit_get_real_from_bits_expression(GroupNodeCPtr node) override;

      void visit_get_bits_from_bit_expression(GroupNodeCPtr node) override;

      void visit_get_bits_from_boolean_expression(GroupNodeCPtr node) override;

      void visit_get_bits_from_byte_expression(GroupNodeCPtr node) override;

      void visit_get_bits_from_char_expression(GroupNodeCPtr node) override;

      void visit_get_bits_from_integer_expression(GroupNodeCPtr node) override;

      void visit_get_bits_from_real_expression(GroupNodeCPtr node) override;

      void visit_encode_bit_expression(GroupNodeCPtr node) override;

      void visit_encode_boolean_expression(GroupNodeCPtr node) override;

      void visit_encode_byte_expression(GroupNodeCPtr node) override;

      void visit_encode_char_expression(GroupNodeCPtr node) override;

      void visit_encode_integer_expression(GroupNodeCPtr node) override;

      void visit_encode_real_expression(GroupNodeCPtr node) override;

      void visit_encode_string_expression(GroupNodeCPtr node) override;

      void visit_decode_bit_expression(GroupNodeCPtr node) override;

      void visit_decode_boolean_expression(GroupNodeCPtr node) override;

      void visit_decode_byte_expression(GroupNodeCPtr node) override;

      void visit_decode_char_expression(GroupNodeCPtr node) override;

      void visit_decode_integer_expression(GroupNodeCPtr node) override;

      void visit_decode_real_expression(GroupNodeCPtr node) override;

      void visit_decode_string_expression(GroupNodeCPtr node) override;

      void visit_get_head_expression(GroupNodeCPtr node) override;

      void visit_get_tail_expression(GroupNodeCPtr node) override;

      void visit_convert_to_tuple_expression(GroupNodeCPtr node) override;

      void visit_struct_from_tuple_expression(GroupNodeCPtr node) override;

      void visit_type(GroupNodeCPtr node) override;

      void visit_statement_list(GroupNodeCPtr node) override;

      void visit_statement_block(GroupNodeCPtr node) override;

      void visit_assert_statement(GroupNodeCPtr node) override;

      void visit_break_statement(GroupNodeCPtr node) override;

      void visit_continue_statement(GroupNodeCPtr node) override;

      void visit_call_constructor_statement(GroupNodeCPtr node) override;

      void visit_expression_statement(GroupNodeCPtr node) override;

      void visit_foreach_statement(GroupNodeCPtr node) override;

      void visit_if_statement(GroupNodeCPtr node) override;

      void visit_log_statement(GroupNodeCPtr node) override;

      void visit_return_statement(GroupNodeCPtr node) override;

      void visit_set_statement(GroupNodeCPtr node) override;

      void visit_throw_statement(GroupNodeCPtr node) override;

      void visit_try_statement(GroupNodeCPtr node) override;

      void visit_update_statement(GroupNodeCPtr node) override;

      void visit_wait_statement(GroupNodeCPtr node) override;

      void visit_while_statement(GroupNodeCPtr node) override;

      void visit_def_generic_function(GroupNodeCPtr node) override;

      void visit_instantiate_generic_function(GroupNodeCPtr node) override;

      void visit_inline_group(GroupNodeCPtr node) override;

      /**
       * Visit the specified node.
       * @param node the nodes whose children to visit
       */
    protected:
      virtual void visitChildNodes(const GroupNodeCPtr &node);
    };
  }
}
#endif
