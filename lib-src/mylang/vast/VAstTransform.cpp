#include <idioma/ast/GroupNode.h>
#include <idioma/ast/Node.h>
#include <idioma/ast/TokenNode.h>
#include <mylang/annotations.h>
#include <mylang/constraints/ConstrainedType.h>
#include <mylang/defs.h>
#include <mylang/names/Name.h>
#include <mylang/variables/Variable.h>
#include <mylang/vast/InternalError.h>
#include <mylang/vast/VAstTransform.h>
#include <memory>
#include <vector>

namespace mylang {
  namespace vast {
#if 0
        static void trace_transform(const char *file, size_t line, const char* function)
        {
          ::std::cerr << file << ':' << line << ':' << function << ":transform applied" << ::std::endl;
        }
#else

    static void trace_transform(const char*, size_t, const char*)
    {
    }
#endif

    template<class T = mylang::constraints::ConstrainedType>
    static ::std::shared_ptr<const T> rootOf(ConstrainedTypePtr ty)
    {
      while (!ty->self<T>()) {
        auto b = ty->basetype();
        if (!b) {
          return nullptr;
        }
        ty = b;
      }
      return ty->self<T>();
    }

    template<class T = mylang::constraints::ConstrainedType>
    static ::std::shared_ptr<const T> constraintOf(const GroupNodeCPtr &g)
    {
      if (g) {
        auto ty = getConstraintAnnotation(g);
        if (!ty) {
          throw InternalError(g, "internal error: missing constraint annotation");
        }
        return rootOf<T>(ty);
      }
      return nullptr;
    }

    VAstTransform::VAstTransform()
    {
    }

    VAstTransform::~VAstTransform()
    {
    }

    GroupNodeCPtr VAstTransform::visitChildNodes(const GroupNodeCPtr &node)
    {
      bool modified = false;
      ::std::vector<idioma::ast::GroupNode::Rel> entries;
      entries.reserve(node->entries().size());
      for (auto e : node->entries()) {
        GroupNodeCPtr grp = e.node->self<idioma::ast::GroupNode>();
        if (grp) {
          auto modifiedGroup = visit(grp);
          if (modifiedGroup) {
            entries.emplace_back(e.rel, modifiedGroup);
            modified = true;
          } else {
            entries.emplace_back(e.rel, e.node);
          }
        } else {
          // for example, tokens
          entries.emplace_back(e.rel, e.node);
        }
      }
      if (modified) {
        struct DummyKey
        {
          bool operator==(const DummyKey&) const
          {
            return true;
          }
        };
        return idioma::ast::GroupNode::create(DummyKey(), entries);
      } else {
        return nullptr;
      }
    }

    GroupNodeCPtr VAstTransform::visit_type(GroupNodeCPtr)
    {
      // no change
      return nullptr;
    }

    GroupNodeCPtr VAstTransform::visit_root(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createRoot(VAst::getRelations(r, VAst::Relation::DECLS));
    }

    GroupNodeCPtr VAstTransform::visit_literal_bit(GroupNodeCPtr)
    {
      return nullptr;
    }

    GroupNodeCPtr VAstTransform::visit_literal_byte(GroupNodeCPtr)
    {
      return nullptr;
    }

    GroupNodeCPtr VAstTransform::visit_literal_integer(GroupNodeCPtr)
    {
      return nullptr;
    }

    GroupNodeCPtr VAstTransform::visit_literal_real(GroupNodeCPtr)
    {
      return nullptr;
    }

    GroupNodeCPtr VAstTransform::visit_literal_boolean(GroupNodeCPtr)
    {
      return nullptr;
    }

    GroupNodeCPtr VAstTransform::visit_literal_string(GroupNodeCPtr)
    {
      return nullptr;
    }

    GroupNodeCPtr VAstTransform::visit_literal_char(GroupNodeCPtr)
    {
      return nullptr;
    }

    GroupNodeCPtr VAstTransform::visit_literal_nil(GroupNodeCPtr)
    {
      return nullptr;
    }

    GroupNodeCPtr VAstTransform::visit_literal_empty_array(GroupNodeCPtr)
    {
      return nullptr;
    }

    GroupNodeCPtr VAstTransform::visit_literal_array(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createLiteralArrayExpression(VAst::getExprs(r, VAst::Expr::EXPRESSIONS),
          node->span());
    }

    GroupNodeCPtr VAstTransform::visit_literal_tuple(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createLiteralTupleExpression(VAst::getRelations(r, VAst::Relation::MEMBERS));
    }

    GroupNodeCPtr VAstTransform::visit_literal_tuple_member(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createLiteralTupleMember(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_literal_struct(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createLiteralStructExpression(VAst::getRelations(r, VAst::Relation::MEMBERS));
    }

    GroupNodeCPtr VAstTransform::visit_literal_struct_member(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createLiteralStructMember(VAst::getToken(r, VAst::Token::IDENTIFIER),
          VAst::getRelation(r, VAst::Relation::TYPE), VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_variable_reference_expression(GroupNodeCPtr)
    {
      // since it is just a token, we don't have to do anything
      return nullptr;
    }

    GroupNodeCPtr VAstTransform::visit_guard_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGuardExpression(VAst::getExpr(r, VAst::Expr::CONDITION),
          VAst::getExpr(r, VAst::Expr::EXPRESSION), VAst::getExpr(r, VAst::Expr::MESSAGE));
    }

    GroupNodeCPtr VAstTransform::visit_named_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createNamedExpression(VAst::getToken(r, VAst::Token::IDENTIFIER),
          VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_void_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createVoidExpression();
    }

    GroupNodeCPtr VAstTransform::visit_get_input_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetInputExpression(VAst::getExpr(r, VAst::Expr::PORT));
    }

    GroupNodeCPtr VAstTransform::visit_get_output_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetOutputExpression(VAst::getExpr(r, VAst::Expr::PORT));
    }

    GroupNodeCPtr VAstTransform::visit_new_process_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createNewProcessExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          getNameAnnotation(node), VAst::getExprs(r, VAst::Expr::ARGUMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_new_bit_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createNewBitExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExprs(r, VAst::Expr::ARGUMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_new_boolean_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createNewBooleanExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExprs(r, VAst::Expr::ARGUMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_new_byte_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createNewByteExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExprs(r, VAst::Expr::ARGUMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_new_char_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createNewCharExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExprs(r, VAst::Expr::ARGUMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_new_integer_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createNewIntegerExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExprs(r, VAst::Expr::ARGUMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_new_real_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createNewRealExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExprs(r, VAst::Expr::ARGUMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_new_string_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createNewStringExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExprs(r, VAst::Expr::ARGUMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_new_array_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createNewArrayExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExprs(r, VAst::Expr::ARGUMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_new_function_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createNewFunctionExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExprs(r, VAst::Expr::ARGUMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_new_optional_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createNewOptionalExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExprs(r, VAst::Expr::ARGUMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_new_mutable_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createNewMutableExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExprs(r, VAst::Expr::ARGUMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_new_struct_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createNewStructExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExprs(r, VAst::Expr::ARGUMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_new_tuple_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createNewTupleExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExprs(r, VAst::Expr::ARGUMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_new_union_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createNewUnionExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getRelation(r, VAst::Relation::DISCRIMINANT),
          VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_new_namedtype_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createNewNamedTypeExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExprs(r, VAst::Expr::ARGUMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_integer_clamp_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createIntegerClampExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_integer_wrap_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createIntegerWrapExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_integer_negate_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createIntegerNegateExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_integer_add_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createIntegerAddExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_integer_subtract_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createIntegerSubtractExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_integer_multiply_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createIntegerMultiplyExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_integer_divide_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createIntegerDivideExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_integer_modulus_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createIntegerModulusExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_integer_remainder_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createIntegerRemainderExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_real_negate_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createRealNegateExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_real_add_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createRealAddExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_real_subtract_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createRealSubtractExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_real_multiply_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createRealMultiplyExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_real_divide_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createRealDivideExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_real_remainder_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createRealRemainderExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_loop_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createLoopExpression(VAst::getRelation(r, VAst::Relation::DECL),
          VAst::getExpr(r, VAst::Expr::CONDITION), VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_with_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createWithExpression(VAst::getRelations(r, VAst::Relation::DECLS),
          VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_and_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createAndExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_or_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createOrExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_xor_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createXorExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_short_circuit_and_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createShortCircuitAndExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_short_circuit_or_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createShortCircuitOrExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_not_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createNotExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_comparison_eq_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);

      return ast.createComparisonEQExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_comparison_neq_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createComparisonNEQExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_comparison_lt_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createComparisonLTExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_comparison_lte_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createComparisonLTEExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_comparison_gt_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createComparisonGTExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_comparison_gte_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createComparisonGTEExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_conditional_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createConditionalExpression(VAst::getExpr(r, VAst::Expr::CONDITION),
          VAst::getExpr(r, VAst::Expr::IFTRUE), VAst::getExpr(r, VAst::Expr::IFFALSE));
    }

    GroupNodeCPtr VAstTransform::visit_def_variable(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createDefineVariable(VAst::getToken(r, VAst::Token::IDENTIFIER),
          getNameAnnotation(node), getVariableAnnotation(node),
          VAst::getRelation(r, VAst::Relation::TYPE), VAst::getExpr(r, VAst::Expr::INITIALIZER));
    }

    GroupNodeCPtr VAstTransform::visit_process_member_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createProcessMemberExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION),
          VAst::getToken(r, VAst::Token::IDENTIFIER));
    }

    GroupNodeCPtr VAstTransform::visit_union_member_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createUnionMemberExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION),
          VAst::getToken(r, VAst::Token::IDENTIFIER));
    }

    GroupNodeCPtr VAstTransform::visit_struct_member_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createStructMemberExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION),
          VAst::getToken(r, VAst::Token::IDENTIFIER));
    }

    GroupNodeCPtr VAstTransform::visit_tuple_member_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createTupleMemberExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_optify_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createOptifyExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_get_optional_value_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetOptionalValueExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_get_mutable_value_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetMutableValueExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_index_string_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createStringIndexExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_index_array_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createArrayIndexExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_subrange_string_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createStringSubrangeExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION),
          VAst::getExpr(r, VAst::Expr::BEGIN), VAst::getExpr(r, VAst::Expr::END));
    }

    GroupNodeCPtr VAstTransform::visit_subrange_array_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createArraySubrangeExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION),
          VAst::getExpr(r, VAst::Expr::BEGIN), VAst::getExpr(r, VAst::Expr::END));
    }

    GroupNodeCPtr VAstTransform::visit_call_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createFunctionCallExpression(VAst::getExpr(r, VAst::Expr::FUNCTION),
          VAst::getExprs(r, VAst::Expr::ARGUMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_lambda_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createLambdaExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getRelations(r, VAst::Relation::PARAMETERS),
          VAst::getRelation(r, VAst::Relation::BODY));
    }

    GroupNodeCPtr VAstTransform::visit_concatenate_arrays_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createConcatenateArraysExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_concatenate_strings_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createConcatenateStringsExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_merge_tuples_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createMergeTuplesExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_zip_arrays_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createZipArraysExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_transform_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createTransformExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExprs(r, VAst::Expr::ARGUMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_get_as_roottype_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetAsRootTypeExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_get_as_basetype_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetAsBaseTypeExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_checked_cast_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createCheckedCastExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_safe_cast_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createSafeCastExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_implicit_cast_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createImplicitCastExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_implicit_basetypecast_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createImplicitBaseTypeCastExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_unsafe_cast_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createUnsafeTypeCastExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_orelse_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetOptionalValueOrElseExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_try_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createTryCatchExpression(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_def_type(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createDefineType(VAst::getToken(r, VAst::Token::IDENTIFIER),
          getNameAnnotation(node), VAst::getRelation(r, VAst::Relation::TYPE));
    }

    GroupNodeCPtr VAstTransform::visit_def_namespace(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createDefineNamespace(VAst::getToken(r, VAst::Token::IDENTIFIER),
          getNameAnnotation(node), VAst::getRelations(r, VAst::Relation::DECLS));
    }

    GroupNodeCPtr VAstTransform::visit_def_exported_function(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createDefineExportedFunction(VAst::getToken(r, VAst::Token::IDENTIFIER),
          getNameAnnotation(node), VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getRelations(r, VAst::Relation::PARAMETERS),
          VAst::getRelation(r, VAst::Relation::BODY));
    }

    GroupNodeCPtr VAstTransform::visit_def_function(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      auto vinfo = getVariableAnnotation(node);
      return ast.createDefineFunction(VAst::getToken(r, VAst::Token::IDENTIFIER),
          getNameAnnotation(node), vinfo, VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getRelations(r, VAst::Relation::PARAMETERS),
          VAst::getRelation(r, VAst::Relation::BODY));
    }

    GroupNodeCPtr VAstTransform::visit_def_process(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createDefineProcess(VAst::getToken(r, VAst::Token::IDENTIFIER),
          getNameAnnotation(node), VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getRelations(r, VAst::Relation::STATEMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_process_block(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createProcessBlock(VAst::getToken(r, VAst::Token::IDENTIFIER),
          getNameAnnotation(node), VAst::getExpr(r, VAst::Expr::CONDITION),
          VAst::getRelation(r, VAst::Relation::BODY));
    }

    GroupNodeCPtr VAstTransform::visit_process_constructor(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createProcessConstructor(getNameAnnotation(node),
          VAst::getRelations(r, VAst::Relation::PARAMETERS),
          VAst::getRelation(r, VAst::Relation::INIT), VAst::getRelation(r, VAst::Relation::BODY));
    }

    GroupNodeCPtr VAstTransform::visit_function_body(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createFunctionBody(VAst::getRelations(r, VAst::Relation::STATEMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_clear_port_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createClearPortExpression(VAst::getExpr(r, VAst::Expr::PORT));
    }

    GroupNodeCPtr VAstTransform::visit_read_port_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createReadPortExpression(VAst::getExpr(r, VAst::Expr::PORT));
    }

    GroupNodeCPtr VAstTransform::visit_write_port_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createWritePortExpression(VAst::getExpr(r, VAst::Expr::PORT),
          VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_wait_port_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createWaitPortExpression(VAst::getExpr(r, VAst::Expr::PORT));
    }

    GroupNodeCPtr VAstTransform::visit_close_port_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createClosePortExpression(VAst::getExpr(r, VAst::Expr::PORT));
    }

    GroupNodeCPtr VAstTransform::visit_block_read_events_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createBlockReadEventsExpression(VAst::getExpr(r, VAst::Expr::PORT),
          VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_block_write_events_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createBlockWriteEventsExpression(VAst::getExpr(r, VAst::Expr::PORT),
          VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_has_new_input_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createHasNewInputExpression(VAst::getExpr(r, VAst::Expr::PORT));
    }

    GroupNodeCPtr VAstTransform::visit_is_output_closed_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createIsOutputClosedExpression(VAst::getExpr(r, VAst::Expr::PORT));
    }

    GroupNodeCPtr VAstTransform::visit_is_input_closed_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createIsInputClosedExpression(VAst::getExpr(r, VAst::Expr::PORT));
    }

    GroupNodeCPtr VAstTransform::visit_is_port_readable_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createIsPortReadableExpression(VAst::getExpr(r, VAst::Expr::PORT));
    }

    GroupNodeCPtr VAstTransform::visit_is_port_writable_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createIsPortWritableExpression(VAst::getExpr(r, VAst::Expr::PORT));
    }

    GroupNodeCPtr VAstTransform::visit_is_present_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createIsOptionalValuePresentExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_array_length_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetArrayLengthExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_string_length_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetStringLengthExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_string_to_chars_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createStringToCharsExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_char_to_string_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createCharToStringExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_interpolate_text_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createInterpolateTextExpression(VAst::getExprs(r, VAst::Expr::EXPRESSIONS));
    }

    GroupNodeCPtr VAstTransform::visit_stringify_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createStringifyExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_fold_array_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createFoldArrayExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION),
          VAst::getExpr(r, VAst::Expr::INITIALIZER), VAst::getExpr(r, VAst::Expr::FUNCTION));
    }

    GroupNodeCPtr VAstTransform::visit_map_array_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createMapArrayExpression(VAst::getExpr(r, VAst::Expr::FUNCTION),
          VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_pad_array_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createPadArrayExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION),
          VAst::getExpr(r, VAst::Expr::SIZE), VAst::getExpr(r, VAst::Expr::INITIALIZER));
    }

    GroupNodeCPtr VAstTransform::visit_trim_array_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createTrimArrayExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION),
          VAst::getExpr(r, VAst::Expr::SIZE));
    }

    GroupNodeCPtr VAstTransform::visit_take_array_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createTakeArrayExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION),
          VAst::getExpr(r, VAst::Expr::SIZE));
    }

    GroupNodeCPtr VAstTransform::visit_drop_array_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createDropArrayExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION),
          VAst::getExpr(r, VAst::Expr::SIZE));
    }

    GroupNodeCPtr VAstTransform::visit_filter_array_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createFilterArrayExpression(VAst::getExpr(r, VAst::Expr::FUNCTION),
          VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_find_array_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createFindArrayExpression(VAst::getExpr(r, VAst::Expr::FUNCTION),
          VAst::getExpr(r, VAst::Expr::EXPRESSION), VAst::getExpr(r, VAst::Expr::INDEX));
    }

    GroupNodeCPtr VAstTransform::visit_reverse_array_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createReverseArrayExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_map_optional_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createMapOptionalExpression(VAst::getExpr(r, VAst::Expr::FUNCTION),
          VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_partition_array_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createPartitionArrayExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION),
          VAst::getExpr(r, VAst::Expr::SIZE));
    }

    GroupNodeCPtr VAstTransform::visit_flatten_array_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createFlattenArrayExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_flatten_optional_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createFlattenOptionalExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_get_bit_from_bits_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetBitFromBitsExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_get_boolean_from_bits_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetBooleanFromBitsExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_get_byte_from_bits_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetByteFromBitsExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_get_char_from_bits_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetCharFromBitsExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_get_integer_from_bits_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      auto ity = constraintOf(node);
      return ast.createGetIntegerFromBitsExpression(ast.createType(ity, node->span()),
          VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_get_real_from_bits_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      auto ity = constraintOf(node);
      return ast.createGetRealFromBitsExpression(ast.createType(ity, node->span()),
          VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_get_bits_from_bit_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetBitsFromByteExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_get_bits_from_boolean_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetBitsFromByteExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_get_bits_from_byte_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetBitsFromByteExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_get_bits_from_char_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetBitsFromByteExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_get_bits_from_integer_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetBitsFromByteExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_get_bits_from_real_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetBitsFromByteExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_get_byte_as_unsigned_integer_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetByteAsUnsignedIntegerExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_get_byte_as_signed_integer_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetByteAsSignedIntegerExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_encode_bit_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createEncodeBitExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_encode_boolean_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createEncodeBooleanExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_encode_byte_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createEncodeByteExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_encode_char_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createEncodeCharExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_encode_integer_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createEncodeIntegerExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_encode_real_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createEncodeRealExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_encode_string_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createEncodeStringExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_decode_bit_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createDecodeBitExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_decode_boolean_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createDecodeBooleanExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_decode_byte_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createDecodeByteExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_decode_char_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createDecodeCharExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_decode_integer_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createDecodeIntegerExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_decode_real_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createDecodeRealExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_decode_string_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createDecodeStringExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_struct_from_tuple_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createStructFromTupleExpression(VAst::getRelation(r, VAst::Relation::TYPE),
          VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_convert_to_tuple_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createConvertToTupleExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_get_head_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetHeadExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_get_tail_expression(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createGetTailExpression(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_statement_block(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createStatementBlock(VAst::getRelations(r, VAst::Relation::STATEMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_statement_list(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createStatementList(VAst::getRelations(r, VAst::Relation::STATEMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_assert_statement(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createAssertStatement(VAst::getExpr(r, VAst::Expr::CONDITION),
          VAst::getExpr(r, VAst::Expr::MESSAGE));
    }

    GroupNodeCPtr VAstTransform::visit_call_constructor_statement(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createCallConstructorStatement(VAst::getExprs(r, VAst::Expr::ARGUMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_expression_statement(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createExpressionStatement(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_foreach_statement(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createForeachStatement(VAst::getRelation(r, VAst::Relation::DECL),
          VAst::getExpr(r, VAst::Expr::EXPRESSION), VAst::getStmt(r, VAst::Stmt::LOOP_BODY));
    }

    GroupNodeCPtr VAstTransform::visit_if_statement(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createIfStatement(VAst::getExpr(r, VAst::Expr::CONDITION),
          VAst::getStmt(r, VAst::Stmt::IFTRUE), VAst::getStmt(r, VAst::Stmt::IFFALSE));
    }

    GroupNodeCPtr VAstTransform::visit_log_statement(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createLogStatement(VAst::getToken(r, VAst::Token::LOG_LEVEL),
          VAst::getExpr(r, VAst::Expr::MESSAGE), VAst::getExprs(r, VAst::Expr::ARGUMENTS));
    }

    GroupNodeCPtr VAstTransform::visit_return_statement(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createReturnStatement(VAst::getExpr(r, VAst::Expr::EXPRESSION));
    }

    GroupNodeCPtr VAstTransform::visit_set_statement(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createSetStatement(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_try_statement(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createTryStatement(VAst::getStmt(r, VAst::Stmt::TRY),
          VAst::getStmt(r, VAst::Stmt::CATCH));
    }

    GroupNodeCPtr VAstTransform::visit_throw_statement(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createThrowStatement();
    }

    GroupNodeCPtr VAstTransform::visit_break_statement(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createBreakStatement();
    }

    GroupNodeCPtr VAstTransform::visit_continue_statement(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createContinueStatement();
    }

    GroupNodeCPtr VAstTransform::visit_update_statement(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createUpdateStatement(VAst::getExpr(r, VAst::Expr::LHS),
          VAst::getExpr(r, VAst::Expr::RHS));
    }

    GroupNodeCPtr VAstTransform::visit_wait_statement(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createWaitStatement(VAst::getExprs(r, VAst::Expr::PORT));
    }

    GroupNodeCPtr VAstTransform::visit_while_statement(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createWhileStatement(VAst::getExpr(r, VAst::Expr::CONDITION),
          VAst::getStmt(r, VAst::Stmt::LOOP_BODY));
    }

    GroupNodeCPtr VAstTransform::visit_def_generic_function(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createDefineGenericFunction(VAst::getToken(r, VAst::Token::IDENTIFIER));
    }

    GroupNodeCPtr VAstTransform::visit_instantiate_generic_function(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createInstantiateGenericFunction(
          VAst::getToken(r, VAst::Token::GENERIC_DEFINITION_NAME),
          VAst::getToken(r, VAst::Token::IDENTIFIER),
          VAst::getRelation(r, VAst::Relation::GENERIC_INSTANCE_FUNCTION));
    }

    GroupNodeCPtr VAstTransform::visit_inline_group(GroupNodeCPtr node)
    {
      auto r = visitChildNodes(node);
      if (!r) {
        return nullptr;
      }
      trace_transform(__FILE__, __LINE__, __FUNCTION__);
      return ast.createInlineGroup(VAst::getRelations(r, VAst::Relation::INLINE_GROUP_MEMBERS));
    }

  }
}
