#ifndef CLASS_MYLANG_VAST_TYPERECURSIONCHECKER_H
#define CLASS_MYLANG_VAST_TYPERECURSIONCHECKER_H

#include <mylang/defs.h>

namespace mylang {
  namespace vast {

    /**
     * The typechecker ensures that a type does not infinitely recurse.
     */
    class TypeRecursionChecker
    {
      ~TypeRecursionChecker() = delete;

      /**
       * Get a constructor for the specified type and arguments.
       */
    public:
      static bool check(const ConstrainedTypePtr &type);
    };
  }
}
#endif
