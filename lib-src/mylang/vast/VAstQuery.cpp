#include <idioma/ast/GroupNode.h>
#include <idioma/ast/Node.h>
#include <mylang/vast/VAstQuery.h>

namespace mylang {
  namespace vast {
    VAstQuery::VAstQuery()
    {
    }

    VAstQuery::~VAstQuery()
    {
    }

    void VAstQuery::visitChildNodes(const GroupNodeCPtr &node)
    {
      if (node) {
        for (auto e : node->entries()) {
          GroupNodeCPtr grp = e.node->self<idioma::ast::GroupNode>();
          if (grp) {
            visit(grp);
          }
        }
      }
    }

    void VAstQuery::visit_type(GroupNodeCPtr)
    {
    }

    void VAstQuery::visit_root(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_literal_bit(GroupNodeCPtr)
    {
    }

    void VAstQuery::visit_literal_byte(GroupNodeCPtr)
    {
    }

    void VAstQuery::visit_literal_integer(GroupNodeCPtr)
    {
    }

    void VAstQuery::visit_literal_real(GroupNodeCPtr)
    {
    }

    void VAstQuery::visit_literal_boolean(GroupNodeCPtr)
    {
    }

    void VAstQuery::visit_literal_string(GroupNodeCPtr)
    {
    }

    void VAstQuery::visit_literal_char(GroupNodeCPtr)
    {
    }

    void VAstQuery::visit_literal_nil(GroupNodeCPtr)
    {
    }

    void VAstQuery::visit_literal_empty_array(GroupNodeCPtr)
    {
    }

    void VAstQuery::visit_literal_array(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_literal_tuple(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_literal_tuple_member(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_literal_struct(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_literal_struct_member(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_guard_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_named_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_void_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_variable_reference_expression(GroupNodeCPtr)
    {
    }

    void VAstQuery::visit_get_head_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_tail_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_convert_to_tuple_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_struct_from_tuple_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_input_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_output_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_new_process_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_new_bit_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_new_boolean_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_new_byte_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_new_char_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_new_integer_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_new_real_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_new_string_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_new_array_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_new_function_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_new_mutable_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_new_optional_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_new_struct_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_new_tuple_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_new_union_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_new_namedtype_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_integer_clamp_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_integer_wrap_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_integer_negate_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_integer_add_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_integer_subtract_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_integer_multiply_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_integer_divide_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_integer_modulus_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_integer_remainder_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_real_negate_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_real_add_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_real_subtract_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_real_multiply_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_real_divide_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_real_remainder_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_loop_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_with_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_and_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_or_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_xor_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_short_circuit_and_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_short_circuit_or_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_not_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_comparison_eq_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_comparison_neq_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_comparison_lt_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_comparison_lte_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_comparison_gt_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_comparison_gte_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_conditional_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_def_variable(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_process_member_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_union_member_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_struct_member_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_tuple_member_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_optify_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_optional_value_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_mutable_value_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_index_string_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_index_array_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_subrange_string_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_subrange_array_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_call_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_lambda_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_concatenate_arrays_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_concatenate_strings_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_merge_tuples_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_zip_arrays_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_as_roottype_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_as_basetype_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_transform_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_checked_cast_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_safe_cast_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_implicit_cast_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_implicit_basetypecast_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_unsafe_cast_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_orelse_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_try_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_def_type(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_def_namespace(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_def_function(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_def_exported_function(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_def_process(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_process_block(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_process_constructor(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_function_body(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_clear_port_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_read_port_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_write_port_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_wait_port_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_close_port_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_block_read_events_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_block_write_events_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_has_new_input_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_is_present_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_is_input_closed_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_is_output_closed_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_is_port_readable_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_is_port_writable_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_array_length_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_string_length_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_string_to_chars_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_char_to_string_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_interpolate_text_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_stringify_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_fold_array_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_map_array_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_pad_array_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_trim_array_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_take_array_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_drop_array_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_filter_array_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_find_array_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_reverse_array_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_map_optional_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_flatten_optional_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_flatten_array_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_partition_array_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_byte_as_unsigned_integer_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_byte_as_signed_integer_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_bit_from_bits_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_boolean_from_bits_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_byte_from_bits_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_char_from_bits_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_integer_from_bits_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_real_from_bits_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_bits_from_bit_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_bits_from_boolean_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_bits_from_byte_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_bits_from_char_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_bits_from_integer_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_get_bits_from_real_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_encode_bit_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_encode_boolean_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_encode_byte_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_encode_char_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_encode_integer_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_encode_real_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_encode_string_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_decode_bit_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_decode_boolean_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_decode_byte_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_decode_char_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_decode_integer_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_decode_real_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_decode_string_expression(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_statement_list(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_statement_block(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_assert_statement(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_break_statement(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_continue_statement(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_call_constructor_statement(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_expression_statement(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_foreach_statement(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_if_statement(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_log_statement(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_return_statement(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_set_statement(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_try_statement(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_throw_statement(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_update_statement(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_wait_statement(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_while_statement(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_def_generic_function(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_instantiate_generic_function(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

    void VAstQuery::visit_inline_group(GroupNodeCPtr node)
    {
      visitChildNodes(node);
    }

  }
}
