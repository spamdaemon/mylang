#ifndef CLASS_MYLANG_VALIDATION_VALIDATIONERROR_H
#define CLASS_MYLANG_VALIDATION_VALIDATIONERROR_H

#ifndef IDIOMA_AST_NODE_H
#include <idioma/ast/Node.h>
#endif

#ifndef FILE_MYLANG_FEEDBACK_H
#include <mylang/feedback.h>
#endif
#include <stdexcept>

namespace mylang {
  namespace vast {
    /**
     * A validation error contains is produced when an AST is invalid.
     */
    class ValidationError: public ::std::runtime_error
    {
      /**
       * Create a new validation error.
       * @param loc the source location
       * @param text the error text
       */
    public:
      ValidationError(const ::mylang::SourceLocation &loc, const ::std::string &text);

      /**
       * Create a new validation error.
       * @param text the error text
       */
    public:
      ValidationError(const ::std::string &text);

      /**
       * Destructor
       */
    public:
      ~ValidationError();

      /** The node (may be nullptr) */
    public:
      const ::mylang::SourceLocation source;

      /** The message */
    public:
      const ::std::string message;
    };
  }
}
#endif
