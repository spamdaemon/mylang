#ifndef CLASS_MYLANG_VAST_INTERNALERROR_H
#define CLASS_MYLANG_VAST_INTERNALERROR_H

#ifndef CLASS_MYLANG_VALIDATION_VALIDATIONERROR_H
#include <mylang/vast/ValidationError.h>
#endif

namespace mylang {
  namespace vast {
    /**
     * A validation error contains is produced when an AST is invalid.
     */
    class InternalError: public ValidationError
    {

      /**
       * Create a new validation error.
       * @param node the source node
       * @param text the error text
       */
    public:
      InternalError(const ::idioma::ast::Node::CPtr &src, const ::std::string &text);

      /**
       * Create a new validation error.
       * @param text the error text
       */
    public:
      InternalError(const ::std::string &text);

      /**
       * Destructor
       */
    public:
      ~InternalError();

      /** The node (may be nullptr) */
    public:
      const ::idioma::ast::Node::CPtr source;

      /** The message */
    public:
      const ::std::string message;
    };
  }
}
#endif
