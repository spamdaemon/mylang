#include <mylang/transforms/repo/Repo.h>
#include <mylang/api/Parser.h>
#include <mylang/vast/VAstQuery.h>
#include <mylang/annotations.h>
#include <mylang/constraints/ConstrainedFunctionType.h>
#include <mylang/constraints/ConstrainedNamedType.h>
#include <fstream>

namespace mylang::transforms::repo {
  namespace {
    static Transform::Ptr createTransform(GroupNodeCPtr def)
    {
      struct TransformCallFinder: public mylang::vast::VAstQuery
      {
        ~TransformCallFinder()
        {
        }

        void visit_transform_expression(mylang::GroupNodeCPtr node) override
        {

          auto retTy = mylang::getConstraintAnnotation(node);
          ::std::vector<mylang::constraints::ConstrainedType::Ptr> args;

          for (auto arg : mylang::vast::VAst::getExprs(node, mylang::vast::VAst::Expr::ARGUMENTS)) {
            args.push_back(mylang::getConstraintAnnotation(arg));
          }
          signatures.push_back(mylang::constraints::ConstrainedFunctionType::get(retTy, args));
          mylang::vast::VAstQuery::visit_transform_expression(node);
        }

        ::std::vector<::std::shared_ptr<const mylang::constraints::ConstrainedFunctionType>> signatures;
      };

      auto tname = mylang::getNameAnnotation(def);
      auto type = mylang::getConstraintAnnotation(def)->self<
          mylang::constraints::ConstrainedFunctionType>();

      if (type == nullptr) {
        return nullptr;
      }

      TransformCallFinder calls;
      calls.visit(def);

      ::std::set<Call> nested;
      for (const auto &c : calls.signatures) {
        nested.insert(Call::create(*c));
      }

      return Transform::create(tname->fullName(), Call::create(*type), nested, 1);
    }

    static void serializeInt(::std::ostream &out, size_t n)
    {
      out << n;
    }

    static size_t deserializeInt(::std::istream &in)
    {
      size_t n;
      in >> n;
      return n;
    }

    static void serializeType(::std::ostream &out, const Type::Typename &c)
    {
      out << c.getName();
    }
    static Type::Ptr deserializeType(::std::istream &in)
    {
      ::std::string name;
      in >> name;
      return Type(name).name;
    }

    static void serializeCall(::std::ostream &out, const Call &c)
    {
      serializeType(out, *c.output.name);
      out << '\t';
      serializeInt(out, c.input.size());
      for (auto t : c.input) {
        out << '\t';
        serializeType(out, *t.name);
      }
    }

    static Call deserializeCall(::std::istream &in)
    {
      Type output(deserializeType(in));
      size_t n = deserializeInt(in);
      ::std::set<Type> input;
      for (size_t i = 0; i < n; ++i) {
        input.emplace(deserializeType(in));
      }
      return Call(output, ::std::move(input));
    }

    static void serializeTransform(::std::ostream &out, const Transform &tx)
    {
      // we don't serialize the name, because it's already stored as the key
      out << tx.name << '\t';
      serializeCall(out, tx.call);
      out << '\t';
      serializeInt(out, tx.nested.size());
      for (auto nest : tx.nested) {
        out << '\t' << nest;
      }
      out << '\t' << tx.cost;
      out << '\n';
    }

    static Transform::Ptr deserializeTransform(::std::istream &in)
    {
      ::std::string name;
      in >> name;

      auto c = deserializeCall(in);
      size_t n = deserializeInt(in);
      ::std::set<Call> nested;
      for (size_t i = 0; i < n; ++i) {
        nested.emplace(deserializeCall(in));
      }
      Transform::Cost cost;
      in >> cost;
      return Transform::create(::std::move(name), c, ::std::move(nested), cost);
    }

  }

  Repo::Repo(::std::filesystem::path xbase)
      : base(::std::filesystem::canonical(xbase)),
          fs(::mylang::filesystem::IterableFileSystem::create(xbase))
  {
  }

  Repo::~Repo()
  {
  }

  mylang::filesystem::FileSystem::Ptr Repo::getFileSystem() const
  {
    return fs;
  }

  size_t Repo::enumerateTransforms(const Type::Typename &output, EnumerationHandler cb) const
  {
    return impl.enumerateTransforms(output, cb);
  }

  void Repo::save(::std::ostream &out) const
  {
    auto types = impl.getTypes();
    auto transforms = impl.getTransforms();

    out << base << ::std::endl;
    out << types.size() << ::std::endl;
    for (const auto &t : types) {
      serializeType(out, *t.second);
      out << ::std::endl;
    }
    out << transforms.size() << ::std::endl;
    for (const auto &t : transforms) {
      serializeTransform(out, *t.second);
      out << ::std::endl;
    }
    out.flush();
  }

  ::std::shared_ptr<Repo> Repo::load(::std::istream &in)
  {
    ::std::filesystem::path xbase;

    in >> xbase;
    if (!in) {
      throw ::std::invalid_argument("Failed read from the database");
    }
    if (!::std::filesystem::is_directory(xbase)) {
      throw ::std::invalid_argument("Base directory " + xbase.string() + " not found");
    }
    auto db = ::std::make_shared<Repo>(::std::move(xbase));

    size_t nTypes, nTransforms;

    in >> nTypes;
    if (!in) {
      throw ::std::invalid_argument("Failed read from the database");
    }
    for (size_t i = 0; i < nTypes; ++i) {
      db->impl.addType(Type(deserializeType(in)));
    }

    in >> nTransforms;
    if (!in) {
      throw ::std::invalid_argument("Failed read from the database");
    }
    for (size_t i = 0; i < nTransforms; ++i) {
      db->impl.addTransform(deserializeTransform(in));
    }
    return db;
  }

  ::std::shared_ptr<Repo> Repo::load(const ::std::filesystem::path &path)
  {
    ::std::ifstream in(path);
    return load(in);
  }

  void Repo::save(const ::std::filesystem::path &path) const
  {
    ::std::ofstream out(path);
    save(out);
  }

  ::std::optional<Repo::Object> Repo::add(const ::std::filesystem::path &apath)
  {
    ::std::error_code ec;
    auto rpath = ::std::filesystem::relative(apath, base, ec);
    if (ec) {
      return {};
    }
    auto stem = rpath.parent_path() / rpath.stem();
    auto fqn = mylang::names::FQN::parseFQN(stem.string(), '/');
    if (!fqn) {
      return {};
    }

    mylang::api::Parser::Options opts;
    opts.feedback = nullptr;
    opts.filesystem = ::mylang::filesystem::FileSystem::create(base);
    auto p = mylang::api::Parser::create(opts);
    auto obj = p->parseObject(apath, fqn.value());
    if (!obj) {
      return ::std::nullopt;
    }
    if (obj->type == mylang::api::Parser::OBJECT_TRANSFORM) {
      impl.addTransform(createTransform(obj->definition));
    } else if (obj->type == mylang::api::Parser::OBJECT_TYPE) {
      impl.addType(obj->fqn.fullName("."));
    }

    return obj;
  }
}
