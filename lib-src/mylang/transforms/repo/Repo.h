#ifndef MYLANG_TRANSFORMS_REPO_REPO_H
#define MYLANG_TRANSFORMS_REPO_REPO_H

#ifndef MYLANG_API_PARSER_H
#include <mylang/api/Parser.h>
#endif

#ifndef MYLANG_TRANSFORMS_TRANSFORMBASE_H
#include <mylang/transforms/TransformBase.h>
#endif

#ifndef MYLANG_TRANSFORMS_SIMPLETXDATABASE_H
#include <mylang/transforms/SimpleTxDatabase.h>
#endif

#ifndef MYLANG_FILESYSTEM_ITERABLEFILESYSTEM_H
#include <mylang/filesystem/IterableFileSystem.h>
#endif

#include <filesystem>
#include <iosfwd>
#include <optional>
#include <set>

namespace mylang::transforms::repo {

  /**
   * A simple in-memory database for transforms
   */
  class Repo: public TransformBase
  {
    /** The object type that can be added to the database */
  public:
    typedef ::mylang::api::Parser::Object Object;
    /**
     * Construct a repo with a base path.
     */
  public:
    Repo(::std::filesystem::path dir);

    /** The destructor */
  public:
    ~Repo();

    /**
     * Add a transform or type to this repo.
     * @param path a path to an object
     * @return the type of object produces, or nothing on error
     */
  public:
    ::std::optional<Object> add(const ::std::filesystem::path &path);

    /**
     * Load a database from a file.
     * @param path the path to a file containing the database
     * @return a database
     */
  public:
    static ::std::shared_ptr<Repo> load(const ::std::filesystem::path &path);

    /**
     * Save this database to the specified file.
     * @param path the path to a file containing the database
     */
  public:
    void save(const ::std::filesystem::path &path) const;

    /**
     * Load a database from a stream.
     * @param in an input stream
     * @return a database
     */
  public:
    static ::std::shared_ptr<Repo> load(::std::istream &in);

    /**
     * Save this database to a stream.
     * @param out an output stream
     */
  public:
    void save(::std::ostream &out) const;

  public:
    size_t enumerateTransforms(const Type::Typename &output, EnumerationHandler cb) const override;
    mylang::filesystem::FileSystem::Ptr getFileSystem() const override;

    /** The base path */
  public:
    const ::std::filesystem::path base;

    /** The files in this database */
  private:
    ::std::set<::std::filesystem::path> files;

    /** The filesystem */
  private:
    mylang::filesystem::IterableFileSystem::Ptr fs;

    /** The a simple database */
  private:
    SimpleTxDatabase impl;
  };
}

#endif
