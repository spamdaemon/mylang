#include <mylang/transforms/repo/discover.h>
#include <mylang/transforms/filesystem/TransformFS.h>
#include <mylang/transforms/SimpleTxDatabase.h>

namespace mylang {
  namespace transforms {
    namespace repo {

      TransformBase::Ptr discoverTransforms(
          const ::mylang::filesystem::IterableFileSystem::Ptr &dirs, bool verbose)
      {
        using TransformFS = ::mylang::transforms::filesystem::TransformFS;

        struct Impl: public SimpleTxDatabase
        {
          ~Impl()
          {
          }

          ::mylang::filesystem::FileSystem::Ptr getFileSystem() const
          {
            return fs;
          }
          ::mylang::filesystem::IterableFileSystem::Ptr fs;
        };

        auto fs = TransformFS::create(dirs, verbose);
        auto db = ::std::make_shared<Impl>();
        db->fs = fs->getFileSystem();

        try {
          fs->enumerate([&](const TransformFS::FQN&, const TransformFS::TransformDetails &details) {
            db->addTransform(details.transform);
            return true;
          });
        } catch (const ::std::filesystem::filesystem_error &e) {
          ::std::cerr << "File system error " << e.what() << ::std::endl;
          return nullptr;
        }
        return db;
      }

    }
  }
}
