#ifndef MYLANG_TRANSFORMS_REPO_DISCOVER_H
#define MYLANG_TRANSFORMS_REPO_DISCOVER_H

#ifndef MYLANG_TRANSFORMS_TRANSFORMBASE_H
#include <mylang/transforms/TransformBase.h>
#endif

#ifndef MYLANG_FILESYSTEM_ITERABLEFILESYSTEM_H
#include <mylang/filesystem/IterableFileSystem.h>
#endif

#include <filesystem>
#include <string>
#include <vector>

namespace mylang {
  namespace transforms {
    namespace repo {

      /**
       * Discover the transforms in the specified folder and
       * build a database.
       */
      TransformBase::Ptr discoverTransforms(const mylang::filesystem::IterableFileSystem::Ptr &dirs,
          bool verbose = false);
    }
  }
}
#endif
