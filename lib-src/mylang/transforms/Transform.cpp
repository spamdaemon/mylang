#include <mylang/transforms/Transform.h>
#include <stdexcept>

namespace mylang {
  namespace transforms {

    Transform::Cost::Cost()
        : value(0)
    {
    }
    Transform::Cost::Cost(unsigned short xvalue)
        : value(xvalue)
    {
    }

    Transform::Transform(::std::string xname, Call c, ::std::set<Call> xnested, const Cost &xcost)
        : name(xname), call(c), nested(xnested), cost(xcost)
    {
    }
    Transform::~Transform()
    {
    }

    bool Transform::isIdentity() const
    {
      return call.isIdentity();
    }

    Transform::Ptr Transform::create(::std::string xname, Call c, ::std::set<Call> xnested,
        const Cost &xcost)
    {
      struct Impl: public Transform
      {
        Impl(::std::string xname, Call c, ::std::set<Call> xnested, const Cost &xcost)
            : Transform(::std::move(xname), c, ::std::move(xnested), xcost)
        {
        }
        ~Impl()
        {
        }
      };
      return ::std::make_shared<Impl>(::std::move(xname), c, ::std::move(xnested), xcost);
    }

    Transform::Ptr Transform::identity(const Type &type)
    {
      if (type.optional) {
        throw ::std::invalid_argument("Identity cannot be optional");
      }
      ::std::string name("$identity");
      ::std::set<Call> nested;
      // an identity transform is either T -> T
      // or T -> Maybe T
      return create(name, Call(type, type.asNonOptional()), nested, Cost());
    }

  }
}

