#ifndef CLASS_MYLANG_TRANSFORMS_TRANSFORMBASE_H
#define CLASS_MYLANG_TRANSFORMS_TRANSFORMBASE_H

#ifndef MYLANG_TRANSFORMS_TYPE_H
#include <mylang/transforms/Type.h>
#endif

#ifndef CLASS_MYLANG_FILESYSTEM_FILESYSTEM_H
#include <mylang/filesystem/FileSystem.h>
#endif

#ifndef MYLANG_TRANSFORMS_TRANSFORM_H
#include <mylang/transforms/Transform.h>
#endif

#ifndef MYLANG_TRANSFORMS_CHAIN_H
#include <mylang/transforms/Chain.h>
#endif

#include <memory>
#include <functional>

namespace mylang {
  namespace transforms {
    class Chain;

    /**
     * The transform database allows us to find chains of transforms
     * to convert between types. Each database is backed by a filesystem
     * that can is used to locate the transform when compiling.
     */
    class TransformBase: public ::std::enable_shared_from_this<TransformBase>
    {
      /** A pointer to a database */
    public:
      typedef ::std::shared_ptr<TransformBase> Ptr;

      /** The quality of the chain to be searched for */
    public:
      enum Quality
      {
        QUALITY_ANY = 0, QUALITY_BEST = 1
      };

    public:
      enum EnumerationStatus
      {
        ENUM_QUIT, ENUM_CONTINUE
      };

      /** The enumeration handler */
    public:
      typedef ::std::function<EnumerationStatus(const Transform::Ptr&)> EnumerationHandler;

      /** The default constructor */
    protected:
      TransformBase();

      /** The destructor */
    public:
      virtual ~TransformBase();

      /**
       * Enumerate the transforms that have the specified output type.
       * If the specified type has the optional flag set, then this method
       * MUST enumerate both the non-optional and the optional version of the type.
       * If not set, only the non-optional version may be enumerated.
       * @param output an output type
       * @param cb callback handler that returns false if to terminate enumeration
       * @return n number of times callback was invoked
       */
    public:
      virtual size_t enumerateTransforms(const Type::Typename &output,
          EnumerationHandler cb) const = 0;

      /**
       * Get the file system that can be used open any transforms or types needed to
       * compile the transforms
       * @return a file system
       */
    public:
      virtual mylang::filesystem::FileSystem::Ptr getFileSystem() const = 0;

      /**
       * Determine if some chain can resolve the specified call.
       * This function may perform much faster than the equivalent
       * <code>findChain(call)!=nulltr</code>.
       * @param call a transform call
       * @return true if findChain would return a result.
       */
    public:
      virtual bool existsChain(const Call &call) const;

      /**
       * Determine if the specified transform can be fully resolved.
       * This function may perform much faster than the equivalent
       * <code>findChain(t)!=nulltr</code>.
       * @param t  a transform
       * @return true if findChain would return a result.
       */
    public:
      virtual bool existsChain(const Transform::Ptr &t) const;

      /**
       * Find any transform chain that resolves the specified call.
       * @param call a transform call
       * @parma q the quality of the chain
       * @return chain for the transform call.
       */
    public:
      virtual ::std::shared_ptr<const Chain> findChain(const Call &call, Quality q) const;

      /**
       * Find the chain that can resolve the specified transform.
       * @param t a transform
       * @parma q the quality of the chain
       * @return a chain that starts with the specified transform or nullptr if not found
       */
    public:
      virtual ::std::shared_ptr<const Chain> findChain(const Transform::Ptr &t, Quality q) const;

    };
  }
}

#endif
