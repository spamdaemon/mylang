#ifndef MYLANG_TRANSFORMS_CALL_H
#define MYLANG_TRANSFORMS_CALL_H

#ifndef MYLANG_TRANSFORMS_TYPE_H
#include <mylang/transforms/Type.h>
#endif

#ifndef MYLANG_CONSTRAINTS_CONSTRAINEDFUNCTIONTYPE_H
#include <mylang/constraints/ConstrainedFunctionType.h>
#endif

#include <memory>
#include <string>
#include <set>
#include <iostream>

namespace mylang {
  namespace transforms {

    /**
     * This class represents the invocation of a transform.
     * The arguments to the call must be unique types, but
     * the order of arguments does not matter.
     */
    class Call
    {
      /**
       * Create a call that converts a set of inputs to outputs.
       * @param output an output
       * @param inputs a list of inputs
       * @throws ::std::invalid_argument if inputs is empty
       * @throws ::std::invalid_argument if any of the input types is optional
       */
    public:
      Call(const Type &output, ::std::set<Type> input);

      /**
       * Create a call that converts a set of inputs to outputs.
       * @param output an output
       * @param input an input
       * @throws ::std::invalid_argument if the input is optional
       */
    public:
      Call(const Type &output, const Type &input);

      /**
       * Create a call from a constrained function type
       * @param fn a function type
       * @throws ::std::invalid_argument if the function uses non-named types.
       */
    public:
      static Call create(const mylang::constraints::ConstrainedFunctionType &fn);

      /** Is this a call to an identity transform */
    public:
      bool isIdentity() const;

      /** Compare two calls
       * @param that another call */
    public:
      bool operator<(const Call &that) const;
      bool operator==(const Call &that) const;
      bool operator!=(const Call &that) const;

      /** Output operator */
      friend ::std::ostream& operator<<(::std::ostream &out, const Call &c);

      /** The output type */
    public:
      const Type output;

      /** The input types */
    public:
      const ::std::set<Type> input;
    };
  }
}

#endif
