#ifndef MYLANG_TRANSFORMS_TYPE_H
#define MYLANG_TRANSFORMS_TYPE_H

#include <string>
#include <memory>
#include <iostream>

namespace mylang {
  namespace transforms {

    /**
     * A type represent a named type.
     */
    class Type
    {
      /** The type's implementation */
    public:
      class Typename
      {
        /** Create a type and give it a unique name
         * @param name the type name
         */
      protected:
        Typename() = default;

        /** The destructor */
      public:
        virtual ~Typename() = default;

        /**
         * Get the name of this type.
         * @return the name of this type
         */
      public:
        virtual const ::std::string& getName() const =0;

        /**
         * Compare two types.
         * @param that another type
         * @return -1,0,1 if this type is less, equal, or greater to that type.
         * @throws std::exception if the types are not coparable
         */
      public:
        virtual int compareTo(const Typename &that) const = 0;

        inline bool operator<(const Typename &that) const
        {
          return compareTo(that) < 0;
        }

        inline bool operator==(const Typename &that) const
        {
          return compareTo(that) == 0;
        }
        inline bool operator!=(const Typename &that) const
        {
          return compareTo(that) != 0;
        }
      };

      /** A pointer to a type */
    public:
      typedef ::std::shared_ptr<Typename> Ptr;

      /**
       *  Create a type with the specified unique namee
       * @param impl name the type name
       * @param isOpt true to make the new type optional
       */
    public:
      Type(Ptr implPtr, bool isOpt = false);

      /**
       * Create a type and give it a unique name
       * @param  name the type name
       * @param isOpt true to make the new type optional
       */
    public:
      Type(::std::string name, bool isOpt = false);

      /**
       * Create a type and update the optional flag
       * @param t a type
       * @param isOpt true to make the new type optional
       */
    public:
      Type(const Type &t, bool isOpt);

      /** The destructor */
    public:
      ~Type();

      /** Compare two types */
    public:
      inline bool operator<(const Type &that) const
      {
        if (optional == that.optional) {
          return name->compareTo(*that.name) < 0;
        }
        return optional < that.optional;
      }

      inline bool operator==(const Type &that) const
      {
        return optional == that.optional && name->compareTo(*that.name) == 0;
      }
      inline bool operator!=(const Type &that) const
      {
        return optional != that.optional || name->compareTo(*that.name) != 0;
      }

      inline friend ::std::ostream& operator<<(::std::ostream &out, const Type &t)
      {
        out << t.name->getName();
        if (t.optional) {
          out << '?';
        }
        return out;
      }

      /**
       * Check if the specified types have the same name.
       * @param t a type
       * @param u a type
       * @return true if two types have the same name
       */
    public:
      inline int compareNames(const Type &that) const
      {
        return name->compareTo(*that.name);
      }

      /**
       * Get the name of this type.
       * @return the name of this type
       */
    public:
      inline ::std::string getName() const
      {
        auto n = name->getName();
        if (optional) {
          n += '?';
        }
        return n;
      }

      /**
       * Get this type as a non-optional
       * @return a type that is not optional
       */
    public:
      inline Type asNonOptional() const
      {
        return Type(name);
      }

      /**
       * Get this type as an optional
       * @return a type that is optional
       */
    public:
      inline Type asOptional() const
      {
        return Type(name, true);
      }

      /** The name of the type */
    public:
      Ptr name;

      /** True if the type represents an optional */
    public:
      bool optional;
    };
  }
}

#endif
