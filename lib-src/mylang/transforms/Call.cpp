#include <mylang/transforms/Call.h>
#include <mylang/transforms/Type.h>
#include <mylang/constraints/ConstrainedNamedType.h>
#include <mylang/constraints/ConstrainedOptType.h>
#include <mylang/constraints/ConstrainedFunctionType.h>
#include <mylang/constraints/ConstrainedType.h>
#include <mylang/names/Name.h>
#include <stdexcept>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <utility>

namespace mylang {
  namespace transforms {

    Call::Call(const Type &xoutput, ::std::set<Type> xinput)
        : output(xoutput), input(std::move(xinput))
    {
      if (input.empty()) {
        throw ::std::invalid_argument("Input types must not be empty");
      }
      for (auto t : input) {
        if (t.optional) {
          throw ::std::invalid_argument("Input type cannot be optional");
        }
      }
    }

    Call::Call(const Type &xoutput, const Type &xinput)
        : output(xoutput), input( { xinput })
    {
      if (xinput.optional) {
        throw ::std::invalid_argument("Input type cannot be optional");
      }
    }

    Call Call::create(const mylang::constraints::ConstrainedFunctionType &fn)
    {
      auto ty = fn.returnType;
      bool isOpt = false;
      if (auto optty = ty->self<mylang::constraints::ConstrainedOptType>(); optty) {
        ty = optty->element;
        isOpt = true;
      }
      auto retTy = ty->self<mylang::constraints::ConstrainedNamedType>();
      if (retTy == nullptr) {
        throw ::std::invalid_argument("Return type is not a named type: " + ty->toString());
      }
      const Type resTy(retTy->name()->fullName(), isOpt);
      ::std::set<Type> argTypes;
      for (auto p : fn.parameters) {
        ty = p->self<mylang::constraints::ConstrainedNamedType>();
        if (ty == nullptr) {
          throw ::std::invalid_argument("Parameter type is not a named type: " + ty->toString());
        }
        argTypes.emplace(ty->name()->fullName());
      }
      return Call(resTy, argTypes);
    }

    bool Call::isIdentity() const
    {
      return input.size() == 1 && input.begin()->compareNames(output) == 0;
    }

    bool Call::operator<(const Call &that) const
    {
      if (output < that.output) {
        return true;
      }
      if (that.output < output) {
        return false;
      }
      return input < that.input;
    }
    bool Call::operator==(const Call &that) const
    {
      return output == that.output && input == that.input;
    }

    bool Call::operator!=(const Call &that) const
    {
      return output != that.output || input != that.input;
    }

    ::std::ostream& operator<<(::std::ostream &out, const Call &c)
    {
      out << '(';
      bool sep = false;
      for (const auto &t : c.input) {
        if (sep) {
          out << ',';
        }
        sep = true;
        out << t;
      }
      out << ')';
      out << ':';
      out << c.output;
      return out;
    }

  }
}

