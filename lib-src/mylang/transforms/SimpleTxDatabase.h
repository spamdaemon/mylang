#ifndef MYLANG_TRANSFORMS_SIMPLETXDATABASE_H
#define MYLANG_TRANSFORMS_SIMPLETXDATABASE_H

#ifndef MYLANG_TRANSFORMS_TRANSFORMDATABASE_H
#include <mylang/transforms/TransformBase.h>
#endif

#include <map>

namespace mylang {
  namespace transforms {

    /**
     * A simple in-memory database for transforms
     */
    class SimpleTxDatabase: public TransformBase
    {
      /** The default constructor
       * @param fs a file system*/
    public:
      SimpleTxDatabase();

      /** The destructor */
    public:
      ~SimpleTxDatabase();

      /**
       * Add a transform
       */
    public:
      virtual void addTransform(Transform::Ptr tx);

      /**
       * Add a type
       */
    public:
      virtual void addType(const Type &tx);

      /**
       * Add the types of a call to this database.
       * @param c a call
       */
    public:
      void addCallTypes(const Call &c);

    public:
      Type::Ptr findType(const ::std::string &name) const;
      size_t enumerateTransforms(const Type::Typename &output, EnumerationHandler cb) const
          override;
      mylang::filesystem::FileSystem::Ptr getFileSystem() const override;

      /**
       * Get the transforms
       */
    public:
      ::std::map<::std::string, ::std::shared_ptr<const Transform>> getTransforms() const;

      /**
       * Get the types
       */
    public:
      ::std::map<::std::string, Type::Ptr> getTypes() const;

      /** A list of transforms */
    private:
      ::std::map<::std::string, ::std::shared_ptr<const Transform>> transforms;

      /** The names of known types */
    private:
      ::std::map<::std::string, Type::Ptr> types;

    };
  }
}

#endif
