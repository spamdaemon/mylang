#include <mylang/transforms/TransformBase.h>
#include <map>
#include <set>

namespace mylang {
  namespace transforms {

    namespace {

      /**
       * Resolve a transform
       * @param db the database
       * @param tx the transform to resolve
       * @param c the call being made. Output of the call must match the output of the transform.
       * @return a chain starting with the transform, or nullptr if not found
       */
      static Chain::Ptr resolveTransform(const TransformBase &db, const Transform::Ptr &tx,
          const Call &c, TransformBase::EnumerationStatus quitIfFound, ::std::set<Type> &types);

      /**
       * Find some chain that can resolve an equivalence relation.
       */
      static Chain::Ptr findChainPrivate(const TransformBase &db,
          TransformBase::EnumerationStatus quitIfFound, const Call &c, ::std::set<Type> &types);

      // TODO: use bread-first search to find the best chain, and not DFS. Use DFS if
      // we use FIND_ANY; for now, this will do

      static Chain::Ptr resolveTransform(const TransformBase &db, const Transform::Ptr &tx,
          const Call &c, TransformBase::EnumerationStatus quitIfFound, ::std::set<Type> &types)
      {
        auto nonOptOut = c.output.asNonOptional();
        // if we've detected the type already, then we have recursion
        // and cannot proceed, at least along the current transform
        if (types.insert(c.output).second == false) {
          return nullptr;
        }

        // determine how call arguments we can match to the transform's arguments

        Transform::Cost cost = tx->cost;

        // for each input, we compute a chain that transforms the respective call argument
        ::std::vector<Chain::Ptr> argChains;
        for (const auto &in : tx->call.input) {
          // see if we can find a chain using the non-optional type
          auto ch = findChainPrivate(db, quitIfFound, Call(in, c.input), types);
          if (!ch) {

            // if we're expecting an optional type, then we can deal with
            // inputs that are conditionally available
            if (c.output.optional) {
              ch = findChainPrivate(db, quitIfFound, Call(in.asOptional(), c.input), types);
            }
            if (!ch) {
              // failed to find the chain needed
              types.erase(c.output);
              return nullptr;
            }
          }
          cost += ch->getCost();
          argChains.push_back(::std::move(ch));
        }

        ::std::map<Call, Chain::Ptr> nested;
        for (auto nc : tx->nested) {
          if (nested.count(nc) == 0) {
            auto nestedChain = findChainPrivate(db, quitIfFound, nc, types);
            if (!nestedChain) {
              types.erase(c.output);
              return nullptr;
            }
            cost += nestedChain->getCost();
            nested.emplace(nc, nestedChain);
          }
        }

        types.erase(c.output);

        // we need to ensure that the chain is optional
        return Chain::create(tx, c.output.optional && tx->call.output.optional == false,
            ::std::move(argChains), ::std::move(nested), cost);
      }

      static Chain::Ptr findChainPrivate(const TransformBase &db,
          TransformBase::EnumerationStatus quitIfFound, const Call &c, ::std::set<Type> &types)
      {
        auto nonopt = c.output.asNonOptional();

        if (c.input.count(nonopt) != 0) {
          // since one of the arguments matches the output, we can ignore the other
          // arguments and just return an identity transform.
          //
          // this is really saying that the identity transform is always implicitly defined!
          //
          // also, we can always map a non-optional type to its optional form.
          return Chain::createIdentity(c.output);
        }

        Chain::Ptr bestChain;
        db.enumerateTransforms(*c.output.name, [&](const Transform::Ptr &tx) {
          if (c.output.optional==false && tx->call.output.optional) {
            // ignore transform that produces optionals
            return TransformBase::ENUM_CONTINUE;
          }
          Chain::Ptr chain = resolveTransform(db, tx,c, quitIfFound, types);
          if (chain == nullptr) {
            return TransformBase::ENUM_CONTINUE;
          }

          if (bestChain==nullptr) {
            bestChain=chain;
          } else if (chain->getCost()<bestChain->getCost()) {
            bestChain = chain;
          }
          return quitIfFound;
        });

        return bestChain;
      }

    }

    TransformBase::TransformBase()
    {
    }

    TransformBase::~TransformBase()
    {
    }

    bool TransformBase::existsChain(const Call &call) const
    {
      // this function could be specialized and avoid a lot of memory allocation
      // if we only want to check that a transform exists
      return findChain(call, QUALITY_ANY) != nullptr;
    }

    bool TransformBase::existsChain(const Transform::Ptr &t) const
    {
      // this function could be specialized and avoid a lot of memory allocation
      // if we only want to check that a transform exists
      return findChain(t, QUALITY_ANY) != nullptr;
    }

    ::std::shared_ptr<const Chain> TransformBase::findChain(const Call &call, Quality q) const
    {
      ::std::set<Type> types;
      EnumerationStatus disp = q == QUALITY_BEST ? ENUM_CONTINUE : ENUM_QUIT;
      return findChainPrivate(*this, disp, call, types);
    }

    ::std::shared_ptr<const Chain> TransformBase::findChain(const Transform::Ptr &t,
        Quality q) const
    {
      if (!t) {
        return nullptr;
      }
      ::std::set<Type> types;
      EnumerationStatus disp = q == QUALITY_BEST ? ENUM_CONTINUE : ENUM_QUIT;
      return resolveTransform(*this, t, t->call, disp, types);
    }
  }
}

