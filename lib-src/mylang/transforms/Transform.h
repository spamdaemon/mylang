#ifndef MYLANG_TRANSFORMS_TRANSFORM_H
#define MYLANG_TRANSFORMS_TRANSFORM_H

#ifndef MYLANG_TRANSFORMS_TYPE_H
#include <mylang/transforms/Type.h>
#endif

#ifndef MYLANG_TRANSFORMS_CALL_H
#include <mylang/transforms/Call.h>
#endif

#include <memory>
#include <string>
#include <vector>
#include <set>
#include <iostream>
#include <ostream>

namespace mylang {
  namespace transforms {

    class Transform
    {

      /** A transform pointer */
    public:
      typedef ::std::shared_ptr<const Transform> Ptr;

      /** The cost of a transform */
    public:
      class Cost
      {
      public:
        Cost();
        Cost(unsigned short value);

        /** Compare two costs
         * @param that another cost */
      public:
        inline bool operator<(const Cost &that) const
        {
          return value < that.value;
        }
        inline bool operator==(const Cost &that) const
        {
          return value == that.value;
        }
        inline bool operator!=(const Cost &that) const
        {
          return value != that.value;
        }

        inline Cost operator+(const Cost &that) const
        {
          Cost c(*this);
          return c += that;
        }
        inline Cost& operator+=(const Cost &that)
        {
          value += that.value;
          return *this;
        }

        friend inline ::std::ostream& operator<<(::std::ostream &out, const Cost &c)
        {
          return out << c.value;
        }
        friend inline ::std::istream& operator>>(::std::istream &in, Cost &c)
        {
          return in >> c.value;
        }

      private:
        double value;
      };

      /** Constructor */
    private:
      Transform(::std::string name, Call c, ::std::set<Call> nested, const Cost &cost);

      /** The destructor */
    public:
      virtual ~Transform();

      /**
       * Create a transform.
       */
    public:
      static Ptr create(::std::string name, Call c, ::std::set<Call> nested, const Cost &cost);

      /**
       * Get the identity transform
       */
    public:
      static Ptr identity(const Type &type);

      /**
       * Determine if this is an identity transform.
       * An identity transform has a single input that is also
       * the output.
       */
    public:
      bool isIdentity() const;

      /** The name of the transform */
    public:
      const ::std::string name;

      /** The call that represents the inputs and output for this transform */
    public:
      const Call call;

      /** Any nested calls to other transforms */
    public:
      const ::std::set<Call> nested;

      /** The intrinsic cost of this transform, not counting those for nested calls */
    public:
      const Cost cost;
    };
  }
}

#endif
