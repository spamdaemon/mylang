#ifndef MYLANG_TRANSFORMS_CHAIN_H
#define MYLANG_TRANSFORMS_CHAIN_H

#ifndef MYLANG_TRANSFORMS_TYPE_H
#include <mylang/transforms/Type.h>
#endif

#ifndef MYLANG_TRANSFORMS_TRANSFORM_H
#include <mylang/transforms/Transform.h>
#endif

#include <memory>
#include <vector>
#include <map>
#include <set>

namespace mylang {
  namespace transforms {

    /**
     * A chain represents the set of transforms that need to be applied
     * to transform one type into another.
     */
    class Chain
    {
      /** A pointer to a chain */
    public:
      typedef ::std::shared_ptr<const Chain> Ptr;

      /** The default constructor */
    protected:
      Chain();

      /** The destructor */
    public:
      virtual ~Chain() = 0;

      /**
       * Create the identity transform chain.
       * @param t the type being transformed
       * @return a chain
       */
    public:
      static Ptr createIdentity(const Type &t);

      /**
       * Create a default chain.
       * @param t a transform
       * @param optify true if the chain produces an optional
       * @param argChains the chains for the arguments of the transform
       * @param nestedChains the nested chains indexed by the transform's nested calls
       * @param cost the cost of the chain
       * @return a new chain
       */
    public:
      static Ptr create(Transform::Ptr t, bool optify, ::std::vector<Ptr> argChains,
          ::std::map<Call, ::std::shared_ptr<const Chain>> xnestedChains, Transform::Cost xcost);

      /**
       * Get the effective output type.
       * @return the effective output type
       */
    public:
      Type getOutputType() const;

      /**
       * Get the transform associated with this chain.
       * @return a transform
       */
    public:
      virtual Transform::Ptr getTransform() const =0;

      /**
       * Determine the result of the transform must be wrapped in an optional.
       * If the transform returns an optional, then this would be false
       */
    public:
      virtual bool isOptional() const = 0;

      /**
       * Get the chain that defines how to transform specified
       * argument.
       *
       * @param type the type that uniquely identifies the argument
       * @returns  a chain or a nullptr, if not found or the
       * transform is the identity transform.
       */
    public:
      virtual Ptr getArgChain(const Type::Typename &c) const = 0;

      /**
       * Get the chain associated with the specified nested call.
       * @param call a nested call
       * @return a chain or nullptr if not found
       */
    public:
      virtual Ptr getNestedChain(const Call &c) const =0;

      /**
       * Get the cost of this chain
       */
    public:
      virtual Transform::Cost getCost() const = 0;

      /**
       * Get a textual representation of this chain.
       * @param indent the current indent
       */
    public:
      virtual ::std::string toString(const ::std::string &indent) const;

      /**
       * Get a textual representation of the chain.
       */
    public:
      virtual ::std::string toString() const;
    };
  }
}

#endif
