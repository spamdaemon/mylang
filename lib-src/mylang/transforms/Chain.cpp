#include <mylang/transforms/Chain.h>
#include <sstream>
#include <mylang/transforms/Call.h>
#include <mylang/transforms/Transform.h>
#include <mylang/transforms/Type.h>
#include <algorithm>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>

namespace mylang {
  namespace transforms {
    namespace {

      static ::std::string chain2string(const Chain &chain, const ::std::string &indent,
          bool emitCost)
      {
        const ::std::string nextIndent = indent + "  ";

        ::std::ostringstream buf;
        const auto &t = chain.getTransform();
        if (emitCost) {
          buf << "[";
          buf << chain.getCost();
          buf << "] ";
        }

        if (t->isIdentity()) {
          buf << "id<" << t->call.output.getName() << '>';
        } else {
          if (chain.isOptional()) {
            buf << '&';
          }
          buf << t->name;
          buf << "(";
          for (auto in : t->call.input) {
            auto arg = chain.getArgChain(*in.name);
            if (t->call.input.size() > 1) {
              buf << "\n" << nextIndent;
            }
            if (arg->getOutputType().optional) {
              buf << '*';
            }
            buf << chain2string(*arg, nextIndent, false);
          }
          buf << ")";
          auto const &nestedChains = t->nested;
          if (nestedChains.empty() == false) {
            buf << " {";
            for (auto nestedCall : nestedChains) {
              auto nested = chain.getNestedChain(nestedCall);
              buf << "\n" << nextIndent;
              buf << chain2string(*nested, nextIndent, false);
            }
            buf << "}";
          }
        }
        return buf.str();
      }

    }
    Chain::Chain()
    {
    }
    Chain::~Chain()
    {
    }
    ::std::string Chain::toString() const
    {
      return toString("");
    }

    ::std::string Chain::toString(const ::std::string &indent) const
    {
      return chain2string(*this, indent, true);
    }

    Chain::Ptr Chain::createIdentity(const Type &t)
    {
      auto tx = Transform::identity(t.asNonOptional());
      ::std::vector<Chain::Ptr> argChains;
      ::std::map<Call, Chain::Ptr> nested;
      return create(tx, t.optional, argChains, nested, Transform::Cost(0));
    }

    Type Chain::getOutputType() const
    {
      auto tx = getTransform();
      if (isOptional()) {
        return Type(tx->call.output, true);
      } else {
        return tx->call.output;
      }
    }

    Chain::Ptr Chain::create(Transform::Ptr t, bool mkOpt, ::std::vector<Ptr> argChains,
        ::std::map<Call, ::std::shared_ptr<const Chain>> nestedChains, Transform::Cost cost)
    {
      if (t->call.output.optional) {
        mkOpt = false; // output is already optional, so ensure that the chain is not
      }
      struct Impl: public Chain
      {
        /** Constructor */
        Impl(Transform::Ptr t, bool mkOpt, ::std::vector<Ptr> xargChains,
            ::std::map<Call, ::std::shared_ptr<const Chain>> xnestedChains, Transform::Cost xcost)
            : transform(t), optional(mkOpt), argChains(::std::move(xargChains)),
                nestedChains(::std::move(xnestedChains)), cost(xcost)
        {
        }

        ~Impl()
        {
        }

        bool isOptional() const override
        {
          return optional;
        }

        Transform::Ptr getTransform() const override
        {
          return transform;
        }
        Ptr getArgChain(const Type::Typename &t) const override
        {
          for (auto chain : argChains) {
            if (t == *chain->getTransform()->call.output.name) {
              return chain;
            }
          }
          return nullptr;
        }
        Ptr getNestedChain(const Call &c) const override
        {
          auto i = nestedChains.find(c);
          if (i == nestedChains.end()) {
            return nullptr;
          } else {
            return i->second;
          }
        }
        Transform::Cost getCost() const override
        {
          return cost;
        }

        const ::std::shared_ptr<const Transform> transform;
        const bool optional;
        const ::std::vector<Ptr> argChains;
        const ::std::map<Call, ::std::shared_ptr<const Chain>> nestedChains;
        const Transform::Cost cost;
      };

      return ::std::make_shared<Impl>(::std::move(t), mkOpt, ::std::move(argChains),
          ::std::move(nestedChains), cost);
    }

  }
}
