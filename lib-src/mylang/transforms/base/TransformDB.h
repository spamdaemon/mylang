#ifndef CLASS_MYLANG_TRANSFORMS_BASE_TRANSFORMDB_H
#define CLASS_MYLANG_TRANSFORMS_BASE_TRANSFORMDB_H

#ifndef CLASS_MYLANG_TRANSFORMS_TRANSFORMBASE_H
#include <mylang/transforms/TransformBase.h>
#endif

#ifndef CLASS_MYLANG_TRANSFORMS_FILESYSTEM_TRANSFORMFS_H
#include <mylang/transforms/filesystem/TransformFS.h>
#endif

#ifndef CLASS_MYLANG_NAMES_FQN_H
#include <mylang/names/FQN.h>
#endif

#include <filesystem>
#include <map>
#include <memory>
#include <optional>

namespace mylang::transforms::base {

  /**
   * A transformFS is an iterable filesystem that enumerate the
   * transforms found by another filesystem.
   */
  class TransformDB: public TransformBase
  {
    /** The modification time */
  public:
    typedef ::mylang::filesystem::FileSystem::ModificationTime ModificationTime;

    /** A database diff entry */
  public:
    enum class Status
    {
      UNCHANGED = 0, ADDED, REMOVED, CHANGED,
    };

    /** The database configuration */
  public:
    struct Configuration
    {
      Configuration();

      /** True for verbose output */
      bool verbose;

      /** True to sync */
      bool sync;
    };

    /** The transform filesystem */
  private:
    typedef ::mylang::transforms::filesystem::TransformFS TransformFS;

    /** A pointer */
  public:
    typedef ::std::shared_ptr<TransformDB> Ptr;

    /** An FQN */
  public:
    typedef ::mylang::names::FQN FQN;

    /** The transforms */
  public:
    typedef ::std::map<FQN, TransformFS::TransformDetails> TransformsByFQN;

    /** The transforms */
  public:
    typedef ::std::multimap<::std::string, ::mylang::transforms::Transform::Ptr> TransformsByOutput;

    /**
     * Default constructor
     * @param dbFile the database file
     */
  public:
    TransformDB(::std::filesystem::path dbFile, ::std::filesystem::path fsBase,
        const Configuration &cfg);

    /** Destructor  */
  public:
    ~TransformDB();

    /**
     * Initialize a database.
     *
     * If the database already exists, it is opened.
     *
     * @param dbFile a file
     * @param base the base directory to monitor
     * @return a transformDB
     * @throws ::std::exception on error
     */
  public:
    static Ptr init(::std::filesystem::path dbFile, ::std::filesystem::path base,
        const Configuration &cfg = Configuration());

    /**
     * Load a transform database from the specified file.
     * If the dbFile does not exist, then a nullptr is returned.
     * @param dbFile a file
     * @return a transformDB
     * @param sycn if true, then also synchronize with the filesystem
     * @throws ::std::exception on error
     */
  public:
    static Ptr open(::std::filesystem::path dbFile, const Configuration &cfg = Configuration());

    /**
     * Synchronize the database with the filesystem.
     */
  public:
    void synchronize(const ::std::set<FQN> &prefixes = { });

    /**
     * Perform a diff of the database with the filesystem.
     */
  public:
    ::std::map<FQN, Status> status(const ::std::set<FQN> &prefix = { }) const;

    /**
     * Get the currently known transforms
     */
  public:
    inline const TransformsByFQN& transforms() const
    {
      return transformsByFQN;
    }

    size_t enumerateTransforms(const Type::Typename &output, EnumerationHandler cb) const override;
    mylang::filesystem::FileSystem::Ptr getFileSystem() const override;

    /** The database file */
  public:
    const ::std::filesystem::path dbFile;

    /** The file system base */
  public:
    const ::std::filesystem::path fsBase;

    /** The time of the last sync */
  private:
    ::std::optional<ModificationTime> timeOfLastSync;

    /** The current set of files and their modification times */
  private:
    TransformsByFQN transformsByFQN;

    /** The current set of files and their modification times */
  private:
    TransformsByOutput transformsByOutput;

    /** The file system */
  private:
    TransformFS::Ptr fs;

    /** True if verbose output is on */
  private:
    const Configuration configuration;
  };
}

#endif
