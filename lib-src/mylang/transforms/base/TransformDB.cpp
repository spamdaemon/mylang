#include <mylang/transforms/base/TransformDB.h>
#include <mylang/filesystem/IterableFileSystem.h>
#include <fstream>
#include <set>
#include <iomanip>

namespace mylang::transforms::base {
  namespace {
    static void serializeInt(::std::ostream &out, size_t n)
    {
      out << n;
    }

    static size_t deserializeInt(::std::istream &in)
    {
      size_t n;
      in >> n;
      return n;
    }

    static void serializeTime(::std::ostream &out,
        const mylang::filesystem::FileSystem::ModificationTime &c)
    {
      out << c.time_since_epoch().count();
    }

    static mylang::filesystem::FileSystem::ModificationTime deserializeTime(::std::istream &in)
    {
      mylang::filesystem::FileSystem::ModificationTime::duration::rep tm;
      in >> tm;
      mylang::filesystem::FileSystem::ModificationTime::duration duration(tm);
      return mylang::filesystem::FileSystem::ModificationTime(duration);
    }

    static void serializeText(::std::ostream &out, const ::std::string &c)
    {
      out << ::std::quoted(c);
    }

    static ::std::string deserializeText(::std::istream &in)
    {
      ::std::string text;
      in >> ::std::quoted(text);
      return text;
    }

    static void serializeFQN(::std::ostream &out, const mylang::names::FQN &c)
    {
      serializeText(out, c.fullName());
    }

    static mylang::names::FQN deserializeFQN(::std::istream &in)
    {
      ::std::string fqn = deserializeText(in);
      auto res = mylang::names::FQN::parseFQN(fqn);
      if (!res) {
        in.setstate(::std::ios::failbit);
        throw ::std::runtime_error("Failed to parse FQN " + fqn);
      }
      return *res;
    }

    static void serializeType(::std::ostream &out, const Type &c)
    {
      serializeText(out, c.getName());
    }
    static Type deserializeType(::std::istream &in)
    {

      ::std::string name = deserializeText(in);
      bool opt = false;
      if (*name.rbegin() == '?') {
        name = name.substr(0, name.size() - 1);
        opt = true;
      }
      return Type(name, opt);
    }

    static void serializeCall(::std::ostream &out, const Call &c)
    {
      serializeType(out, c.output);
      out << '\t';
      serializeInt(out, c.input.size());
      for (auto t : c.input) {
        out << '\t';
        serializeType(out, t);
      }
    }

    static Call deserializeCall(::std::istream &in)
    {
      Type output(deserializeType(in));
      size_t n = deserializeInt(in);
      ::std::set<Type> input;
      for (size_t i = 0; i < n; ++i) {
        input.emplace(deserializeType(in));
      }
      return Call(output, ::std::move(input));
    }

    static void serializeTransform(::std::ostream &out, const Transform &tx)
    {
      // we don't serialize the name, because it's already stored as the key
      serializeText(out, tx.name);
      out << '\t';
      serializeCall(out, tx.call);
      out << '\t';
      serializeInt(out, tx.nested.size());
      for (auto nest : tx.nested) {
        out << '\t';
        serializeCall(out, nest);
      }
      out << '\t' << tx.cost;
      out << '\n';
    }

    static Transform::Ptr deserializeTransform(::std::istream &in)
    {
      ::std::string name = deserializeText(in);
      auto c = deserializeCall(in);
      size_t n = deserializeInt(in);
      ::std::set<Call> nested;
      for (size_t i = 0; i < n; ++i) {
        nested.emplace(deserializeCall(in));
      }
      Transform::Cost cost;
      in >> cost;
      auto tx = Transform::create(::std::move(name), c, ::std::move(nested), cost);
      return tx;
    }

    static mylang::filesystem::FileSystem::Details deserializeFSDetails(::std::istream &in)
    {
      auto id = deserializeText(in);
      auto tm = deserializeTime(in);
      return {id,tm};
    }
    static void serializeFSDetails(::std::ostream &out,
        const mylang::filesystem::FileSystem::Details &details)
    {
      serializeText(out, details.identifier);
      out << '\t';
      serializeTime(out, details.lastModifiedTime);
    }

    static mylang::transforms::filesystem::TransformFS::TransformDetails deserializeTransformDetails(
        ::std::istream &in)
    {
      auto fsDetails = deserializeFSDetails(in);
      auto tx = deserializeTransform(in);
      if (!tx) {
        throw ::std::runtime_error("Failed to deserialize transform");
      }
      return {fsDetails,tx};
    }

    static void serializeTransformDetails(::std::ostream &out,
        const mylang::transforms::filesystem::TransformFS::TransformDetails &details)
    {
      serializeFSDetails(out, details.fsDetails);
      out << ::std::endl;
      serializeTransform(out, *details.transform);
    }

    static bool save(const TransformDB::TransformsByFQN &fqn,
        const ::std::filesystem::path &baseDir,
        ::std::optional<mylang::filesystem::FileSystem::ModificationTime> timeOfLastSync,
        ::std::ostream &out)
    {
      out << baseDir << ::std::endl;
      if (timeOfLastSync) {
        out << "1 ";
        serializeTime(out, *timeOfLastSync);
      } else {
        out << '0';
      }
      out << ::std::endl;
      out << fqn.size() << ::std::endl;
      for (const auto &e : fqn) {
        serializeFQN(out, e.first);
        out << ::std::endl;
        serializeTransformDetails(out, e.second);
        out << ::std::endl;
      }
      return true;
    }

    static bool load(::std::istream &in, TransformDB::TransformsByFQN &tx,
        ::std::filesystem::path &baseDir,
        ::std::optional<mylang::filesystem::FileSystem::ModificationTime> &timeOfLastSync)
    {
      in >> baseDir;
      if (deserializeInt(in) == 1) {
        timeOfLastSync = deserializeTime(in);
      } else {
        timeOfLastSync.reset();
      }
      size_t nTx = deserializeInt(in);
      try {
        for (size_t i = 0; i < nTx; ++i) {
          auto fqn = deserializeFQN(in);
          auto details = deserializeTransformDetails(in);
          tx.emplace(fqn, details);
        }
        return true;
      } catch (const ::std::exception &ex) {
        return false;
      }
    }
  }
  TransformDB::Configuration::Configuration()
      : verbose(false), sync(false)
  {

  }

  TransformDB::TransformDB(::std::filesystem::path f, ::std::filesystem::path baseDir,
      const Configuration &cfg)
      : dbFile(::std::move(f)), fsBase(::std::move(baseDir)), configuration(cfg)
  {
    auto tmpFS = ::mylang::filesystem::IterableFileSystem::create(fsBase);
    if (!tmpFS) {
      throw ::std::invalid_argument("Failed to access the filesystem " + baseDir.string());
    }
    fs = TransformFS::create(tmpFS, configuration.verbose);
  }

  TransformDB::~TransformDB()
  {
  }

  TransformDB::Ptr TransformDB::init(::std::filesystem::path dbFile,
      ::std::filesystem::path baseDir, const Configuration &cfg)
  {
    if (::std::filesystem::is_regular_file(dbFile)) {
      ::std::cerr << "Database already exists" << ::std::endl;
      return nullptr;
    }
    auto res = ::std::make_shared<TransformDB>(::std::move(dbFile), ::std::move(baseDir), cfg);
    res->synchronize();
    return res;
  }

  TransformDB::Ptr TransformDB::open(::std::filesystem::path dbFile, const Configuration &cfg)
  {
    ::std::ifstream in(dbFile);
    if (!in.is_open()) {
      ::std::cerr << "Failed to open database " << dbFile << ::std::endl;
      return nullptr;
    }
    TransformsByFQN tx;
    ::std::filesystem::path baseDir;
    ::std::optional<ModificationTime> tm;
    if (!load(in, tx, baseDir, tm)) {
      ::std::cerr << "Failed to load database from " << dbFile << ::std::endl;
      return nullptr;
    }
    TransformDB::Ptr db = ::std::make_shared<TransformDB>(::std::move(dbFile), ::std::move(baseDir),
        cfg);
    db->timeOfLastSync = tm;

    for (const auto &e : tx) {
      const auto &details = e.second;
      db->transformsByOutput.emplace(details.transform->call.output.name->getName(),
          details.transform);
    }
    ::std::swap(db->transformsByFQN, tx);

    if (cfg.sync) {
      db->synchronize();
    }
    return db;
  }

  void TransformDB::synchronize(const ::std::set<FQN> &prefixes)
  {
    ::std::optional<::mylang::filesystem::FileSystem::ModificationTime> lastTime(timeOfLastSync);

    TransformsByFQN tmpByFQN;
    TransformsByOutput tmpByOutput;

    ::std::set<FQN> copy;
    for (const auto &e : transformsByFQN) {
      // assume all entries are removed
      if (!prefixes.empty() && !e.first.findPrefix(prefixes)) {
        copy.insert(e.first);
      }
    }

    // basically, this is the same as status
    fs->getFileSystem()->enumerate(prefixes,
        [&](const FQN &fqn, const mylang::filesystem::FileSystem::Details &fsDetails) {
          auto i = transformsByFQN.find(fqn);
          if (i == transformsByFQN.end()) {
            const auto &details = fs->getDetails(fqn);
            // new FQN, get details to find out if it is a transform
            if (details) {
              tmpByFQN.emplace(fqn, *details);
              copy.erase(fqn);
            }
          } else if (i->second.fsDetails.lastModifiedTime < fsDetails.lastModifiedTime) {
            const auto& details = fs->getDetails(fqn);
            // the file for the FQN has been modified, but check if it's still a transform
            if (details) {
              tmpByFQN.emplace(fqn, *details);
              copy.erase(fqn);
            } else {
              // ignore; no longer a transform
            }
          } else {
            tmpByFQN.insert(*i);
            copy.erase(i->first);
          }
          return true;
        });

    for (const auto &e : transformsByFQN) {
      if (copy.count(e.first) != 0) {
        tmpByFQN.insert(e);
      }
    }
    timeOfLastSync = lastTime;
    for (const auto &e : tmpByFQN) {
      const auto &details = e.second;
      tmpByOutput.emplace(details.transform->call.output.name->getName(), details.transform);
    }
    ::std::swap(tmpByFQN, transformsByFQN);
    ::std::swap(tmpByOutput, transformsByOutput);

    ::std::ofstream out(dbFile);
    if (!save(transformsByFQN, fsBase, timeOfLastSync, out)) {
      ::std::cerr << "Failed to save the database " << ::std::endl;
    }
  }

  ::std::map<TransformDB::FQN, TransformDB::Status> TransformDB::status(
      const ::std::set<FQN> &prefixes) const
  {
    ::std::map<FQN, Status> res;
    for (const auto &e : transformsByFQN) {
      // assume all entries are removed
      if (prefixes.empty() || e.first.findPrefix(prefixes)) {
        res.emplace(e.first, Status::REMOVED);
      }
    }
    fs->getFileSystem()->enumerate(prefixes,
        [&](const FQN &fqn, const mylang::filesystem::FileSystem::Details &fsDetails) {
          auto i = transformsByFQN.find(fqn);
          if (i == transformsByFQN.end()) {
            // new FQN, get details to find out if it is a transform
            if (fs->getDetails(fqn)) {
              res.emplace(fqn, Status::ADDED);
            } else {
              // ignore it
            }
          } else if (i->second.fsDetails.lastModifiedTime < fsDetails.lastModifiedTime) {
            // the file for the FQN has been modified, but check if it's still a transform
            if (fs->getDetails(fqn)) {
              res.insert_or_assign(fqn, Status::CHANGED);
            } else {
              // ignore; no longer a transform
            }
          } else {
            res.insert_or_assign(fqn, Status::UNCHANGED);
          }
          return true;
        });

    return res;
  }

  size_t TransformDB::enumerateTransforms(const Type::Typename &output, EnumerationHandler cb) const
  {
    const ::std::string &ty = output.getName();
    auto i = transformsByOutput.lower_bound(ty);
    auto j = transformsByOutput.upper_bound(ty);
    size_t n = 0;
    while (i != j) {
      const auto &t = i->second;
      ++n;
      if (cb(t) == ENUM_QUIT) {
        break;
      }
      ++i;
    }
    return n;
  }

  mylang::filesystem::FileSystem::Ptr TransformDB::getFileSystem() const
  {
    return fs->getFileSystem();
  }

}
