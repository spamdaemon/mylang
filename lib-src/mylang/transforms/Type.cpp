#include <mylang/transforms/Type.h>

namespace mylang {
  namespace transforms {
    namespace {
      Type::Ptr create(::std::string name)
      {
        struct Impl: public Type::Typename
        {
          Impl(::std::string n)
              : name(::std::move(n))
          {
          }
          ~Impl()
          {
          }
          const ::std::string& getName() const
          {
            return name;
          }
          int compareTo(const Typename &that) const
          {
            auto &o = dynamic_cast<const Impl&>(that);
            return name.compare(o.name);
          }

          const ::std::string name;
        };

        return ::std::make_shared<Impl>(::std::move(name));
      }
    }
    Type::Type(Ptr ximpl, bool isopt)
        : name(::std::move(ximpl)), optional(isopt)
    {
      if (!name) {
        throw ::std::invalid_argument("null-name");
      }
    }
    Type::Type(::std::string n, bool isopt)
        : Type(create(::std::move(n)), isopt)
    {
    }
    Type::Type(const Type &t, bool isopt)
        : name(t.name), optional(isopt)
    {
    }

    Type::~Type()
    {
    }

  }
}

