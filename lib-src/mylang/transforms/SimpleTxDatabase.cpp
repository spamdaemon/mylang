#include <mylang/transforms/SimpleTxDatabase.h>

namespace mylang {
  namespace transforms {

    SimpleTxDatabase::SimpleTxDatabase()
    {
    }

    SimpleTxDatabase::~SimpleTxDatabase()
    {
    }

    mylang::filesystem::FileSystem::Ptr SimpleTxDatabase::getFileSystem() const
    {
      return mylang::filesystem::FileSystem::empty();
    }

    void SimpleTxDatabase::addCallTypes(const Call &c)
    {
      addType(c.output);
      for (auto t : c.input) {
        addType(t.asNonOptional());
      }
    }

    void SimpleTxDatabase::addType(const Type &t)
    {
      auto ptr = t.name;
      types.emplace(ptr->getName(), ptr);
    }

    Type::Ptr SimpleTxDatabase::findType(const ::std::string &n) const
    {
      auto i = types.find(n);
      if (i == types.end()) {
        return nullptr;
      } else {
        return i->second;
      }
    }

    void SimpleTxDatabase::addTransform(Transform::Ptr tx)
    {
      transforms.emplace(tx->name, tx);
      addCallTypes(tx->call);
      for (auto c : tx->nested) {
        addCallTypes(c);
      }
    }

    size_t SimpleTxDatabase::enumerateTransforms(const Type::Typename &output,
        EnumerationHandler cb) const
    {
      size_t n = 0;
      for (auto e : transforms) {
        if (output == *e.second->call.output.name) {
          ++n;
          if (cb(e.second) == EnumerationStatus::ENUM_QUIT) {
            break;
          }
        }
      }
      return n;
    }

    ::std::map<::std::string, ::std::shared_ptr<const Transform>> SimpleTxDatabase::getTransforms() const
    {
      return transforms;
    }

    ::std::map<::std::string, Type::Ptr> SimpleTxDatabase::getTypes() const
    {
      return types;
    }

  }
}
