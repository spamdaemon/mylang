#ifndef CLASS_MYLANG_TRANSFORMS_FILESYSTEM_TRANSFORMFS_H
#define CLASS_MYLANG_TRANSFORMS_FILESYSTEM_TRANSFORMFS_H

#ifndef CLASS_MYLANG_FILESYSTEM_ITERABLEFILESYSTEM_H
#include <mylang/filesystem/IterableFileSystem.h>
#endif

#ifndef CLASS_MYLANG_TRANSFORM_TRANSFORM_H
#include <mylang/transforms/Transform.h>
#endif

#ifndef CLASS_MYLANG_NAMES_FQN_H
#include <mylang/names/FQN.h>
#endif

#include <filesystem>
#include <functional>
#include <iosfwd>
#include <map>
#include <memory>
#include <optional>
#include <stdexcept>

namespace mylang::transforms::filesystem {

  /**
   * A transformFS is an iterable filesystem that enumerate the
   * transforms found by another filesystem.
   */
  class TransformFS
  {
    /** A pointer */
  public:
    using Ptr = ::std::shared_ptr<TransformFS>;

    /** An FQN */
  public:
    using FQN = ::mylang::names::FQN;

    /** The transform details */
  public:
    struct TransformDetails
    {
      /** The filesystem details about the transform */
      ::mylang::filesystem::FileSystem::Details fsDetails;

      /** The transform */
      Transform::Ptr transform;
    };

    /**
     * Callback for enumerations
     */
  public:
    /** The enumeration callback
     * When the callback returns false, no further fqns will be enumerated and false
     * is returned from the enumeration call.
     * @param fqn the FQN of a transform
     * @param details the filesystem details of the transform
     */
  public:
    typedef ::std::function<bool(const FQN &fqn, const TransformDetails &details)> Callback;

    /** Default constructor */
  protected:
    TransformFS();

    /** Destructor */
  public:
    virtual ~TransformFS() = 0;

    /**
     * A utility function to load the transform details of an FQN
     * from a file system
     * @param fqn an fqn
     * @param fs a filesystem
     * @return the transform details or nothing if FQN is not a transform
     */
  public:
    static ::std::optional<TransformDetails> loadTransform(const FQN &fqn,
        ::mylang::filesystem::FileSystem::Ptr fs, bool verbose = false);

    /**
     * Create a simple filesystem to discover transforms
     * in a file system
     *
     * @param path the root path
     * @param verbose if true emit debug message that might be helpful in diagnosing issues
     * @return a filesystem
     */
  public:
    static Ptr create(const ::mylang::filesystem::IterableFileSystem::Ptr &fs,
        bool verbose = false);

    /**
     * Get the details about the specified transform
     */
  public:
    virtual ::std::optional<TransformDetails> getDetails(const FQN &fqn) const = 0;

    /**
     * Enumerate all FQNs in the filesystem that correspond to transforms.
     * Enumeration stops when the first FQN returns false or all FQNs
     * have been enumerated.
     * @param cb a callback that is invoked with each known FQN
     * @return true if all callbacks returned true, false otherwise
     */
  public:
    virtual bool enumerate(const Callback &cb) const = 0;

    /**
     * Get the filesystem.
     * @return the filesystem for this transform FS
     */
  public:
    virtual ::mylang::filesystem::IterableFileSystem::Ptr getFileSystem() const = 0;
  };
}

#endif
