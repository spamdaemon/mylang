#include <mylang/transforms/filesystem/TransformFS.h>
#include <mylang/api/Parser.h>
#include <mylang/vast/VAstQuery.h>
#include <mylang/annotations.h>
#include <mylang/constraints/ConstrainedFunctionType.h>
#include <mylang/constraints/ConstrainedNamedType.h>
#include <set>

namespace mylang::transforms::filesystem {
  namespace {
    struct TransformCallFinder: public mylang::vast::VAstQuery
    {
      ~TransformCallFinder()
      {
      }

      void visit_transform_expression(mylang::GroupNodeCPtr node) override
      {

        auto retTy = mylang::getConstraintAnnotation(node);
        ::std::vector<mylang::constraints::ConstrainedType::Ptr> args;

        for (auto arg : mylang::vast::VAst::getExprs(node, mylang::vast::VAst::Expr::ARGUMENTS)) {
          args.push_back(mylang::getConstraintAnnotation(arg));
        }
        signatures.push_back(mylang::constraints::ConstrainedFunctionType::get(retTy, args));
        mylang::vast::VAstQuery::visit_transform_expression(node);
      }

      ::std::vector<::std::shared_ptr<const mylang::constraints::ConstrainedFunctionType>> signatures;
    };

  }
  TransformFS::TransformFS()
  {
  }
  TransformFS::~TransformFS()
  {
  }

  ::std::optional<TransformFS::TransformDetails> TransformFS::loadTransform(const FQN &fqn,
      ::mylang::filesystem::FileSystem::Ptr fs, bool verbose)
  {
    ::mylang::api::Parser::Options opts;
    if (!verbose) {
      opts.feedback = nullptr;
    }
    opts.filesystem = ::std::move(fs);
    auto parser = ::mylang::api::Parser::create(opts);

    auto details = opts.filesystem->getDetails(fqn);
    if (!details) {
      return ::std::nullopt;
    }
    auto obj = parser->parseObject(fqn);
    if (!obj || obj->type != ::mylang::api::Parser::OBJECT_TRANSFORM) {
      return ::std::nullopt;
    }

    auto tname = mylang::getNameAnnotation(obj->definition);
    auto type = mylang::getConstraintAnnotation(obj->definition)->self<
        mylang::constraints::ConstrainedFunctionType>();

    if (type == nullptr) {
      return ::std::nullopt;
    }

    TransformCallFinder calls;
    calls.visit(obj->definition);

    ::std::set<Call> nested;
    for (const auto &c : calls.signatures) {
      nested.insert(Call::create(*c));
    }

    auto tx = Transform::create(tname->fullName(), Call::create(*type), nested, 1);

    return TransformDetails { details.value(), tx };
  }

  TransformFS::Ptr TransformFS::create(const ::mylang::filesystem::IterableFileSystem::Ptr &fs,
      bool verbose)
  {
    struct Impl: public TransformFS
    {
      Impl(bool v, ::mylang::filesystem::IterableFileSystem::Ptr xfs)
          : verbose(v), fs(::std::move(xfs))
      {

      }

      ~Impl()
      {
      }

      ::mylang::filesystem::IterableFileSystem::Ptr getFileSystem() const
      {
        return fs;
      }

      ::std::optional<TransformDetails> getDetails(const FQN &fqn) const
      {
        return TransformFS::loadTransform(fqn, fs, verbose);
      }

      bool enumerate(const Callback &cb) const override
      {
        return fs->enumerate([&](const FQN &fqn, const mylang::filesystem::FileSystem::Details&) {
          auto details=getDetails(fqn);
          if (!details) {
            return true;
          }
          return cb(fqn,details.value());
        });
      }

      const bool verbose;
      const ::mylang::filesystem::IterableFileSystem::Ptr fs;
    }
    ;

    return ::std::make_shared<Impl>(verbose, ::std::move(fs));
  }

}
