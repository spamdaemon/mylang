#ifndef CLASS_MYLANG_INTEGER_H
#define CLASS_MYLANG_INTEGER_H

#ifndef CLASS_MYLANG_BIGINT_H
#include <mylang/BigInt.h>
#endif

#ifndef CLASS_MYLANG_BIGUINT_H
#include <mylang/BigUInt.h>
#endif

#include <functional>
#include <optional>
#include <iosfwd>
#include <string>
#include <cstdint>

namespace mylang {

  /**
   * A generate representation of arbitrary integers with support for infinity.
   *  */
  class Integer
  {
    /** The integer that represents +infinity */
  public:
    static const Integer& PLUS_INFINITY();

    /** The integer that represents -infinity */
  public:
    static const Integer& MINUS_INFINITY();

    /** The integer that represents 0 */
  public:
    static const Integer& ZERO();

    /** The integer that represents 1 */
  public:
    static const Integer& PLUS_ONE();

    /** The integer that represents -1 */
  public:
    static const Integer& MINUS_ONE();

    /** Copy constructor */
  public:
    Integer(const Integer&) = default;

    /** Copy operator */
  public:
    Integer& operator=(const Integer&) = default;

    /** Default constructor with the value 0 */
  public:
    Integer();

    /** Create an integer.
     *
     * The sign must agree with the value.
     * @param v the value or nothing is infinite
     * @param s the sign of the integer.
     */
  public:
    Integer(::std::optional<BigInt> v, int s);

    /**
     * Create an integer from a big int
     */
  public:
    Integer(BigInt value);

    /**
     * Construct this integer from a big uint
     * @param value a big uint */
  public:
    inline Integer(const BigUInt &value)
        : Integer(BigInt(value))
    {
    }

    /** Constructor with a builtin integer */
  public:
    inline Integer(long long int value)
        : Integer(BigInt(value))
    {
    }

    /**
     * Destructor.
     */
  public:
    ~Integer() = default;

    /**
     * Get the value. If the value is infinity, nothing is returned.
     * @return the value
     */
  public:
    inline const ::std::optional<BigInt> value() const
    {
      return _value;
    }

    /**
     * Deref the value of this integer.
     * @return a reference to the BigInt that represents this integer.
     * @throws ::std::runtime_exception if the !value().has_value()
     */
  public:
    inline const BigInt* operator->() const
    {
      return _value.operator->();
    }

    /**
     * Deref the value.
     * @throws ::std::runtime_exception if the !value().has_value()
     */
  public:
    inline const BigInt& operator*() const
    {
      return *_value;
    }

    /**
     * Get the sign of this value.
     * @return the sign of this integer (-1,0,1)
     */
  public:
    inline int sign() const
    {
      return _sign;
    }

    /**
     * Test if this number is zero.
     * @return true if sign() ==0
     */
  public:
    inline bool is0() const
    {
      return sign() == 0;
    }

    /**
     * Determine if this integer is a finite integer.
     * @return true if this integer is finite
     */
  public:
    inline bool isFinite() const
    {
      return _value.has_value();
    }

    /**
     * Determine if this integer is either positive or negative infinity.
     * @return true if this integer is infinity
     */
  public:
    inline bool isInfinite() const
    {
      return !isFinite();
    }

    /**
     * Comparing.
     */
  public:
    int compare(const Integer &op2) const;

    /**
     * Apply a function to an optional.
     * @param f a function to apply
     * @return a new integer whose value is transformed by f
     */
  private:
    template<class T>
    inline ::std::optional<BigInt> transform(T f) const
    {
      if (_value.has_value()) {
        return f(*_value);
      } else {
        return _value;
      }
    }

    /**
     * Negate this integer.
     * @return a new Integer
     */
  public:
    Integer negate() const;

    /**
     * Get the absolute value of this integer */
  public:
    Integer abs() const;

    /**
     * Add two integers. If the two integers are infinite with
     * opposite signs, then addition is not defined.
     * @param y an integer
     * @return a new Integer or nothing if both operands are infinite with opposite signs.
     */
  public:
    ::std::optional<Integer> add(const Integer &op2) const;

    /**
     * Subtract one integer from another. If the two integers are infinite
     * and have the same sign, then subtraction is not defined.
     * @param y an integer
     * @return a new Integer or nothing if both operands are infinite with the same sign.
     */
  public:
    ::std::optional<Integer> subtract(const Integer &op2) const;

    /**
     * Increment this integer by 1
     * @return an integer
     */
  public:
    Integer increment() const;

    /**
     * Decrement this integer by 1
     * @return an integer.
     */
  public:
    Integer decrement() const;

    /**
     * Change this integer by 1 towards 0.
     * If this integer is infinity, then infinity is returned.
     * If this integer is 0, then 0 is returned
     * If this integer is -v, then -v+1 is returned
     * If this integer is +v, then +v-1 is returned.
     * @return an integer.
     */
  public:
    Integer towards0() const;

    /**
     * Multiply two integers.
     * @param y an integer
     * @return a new Integer
     */
  public:
    Integer multiply(const Integer &op2) const;

    /**
     * Divide two integers.
     *
     * If the denominator is 0 or both integers are infinite, then the result is undefined.
     * If the numerator is infinite, the result is infinite, and if the denomiator
     * is infinte, the result is 0.
     * @param y an integer
     * @return a new Integer or Nothing if denominator is 0, or both operands are infinite.
     */
  public:
    ::std::optional<Integer> divide(const Integer &op2) const;

    /**
     * Compute the remainder of the division of two integers.
     * The remainder is defined as
     * <code>
     * a - (a/b)*b;
     * </code>
     * For example:
     *  * rem(-21,4) = -21 - (-21/4)*4 = -21 -  (-5) * 4 = -21 - (-20) = -1
     *  * rem(-21,-4) = -21 - (-21/-4)*(-4) = -21 -  (5) * (-4) = (-20) - (-20) = -1
     *  * rem(-21,inf) = -21 - (-21/inf) * inf = -21 - 0 * inf = -21
     * @param y an integer
     * @return a new Integer or Nothing if denominator is 0
     */
  public:
    ::std::optional<Integer> remainder(const Integer &op2) const;

    /**
     * Compute the modulus of the division of two integers. Both, the
     * numerator and the denominator must be positive.
     * @param y an integer
     * @return a new Integer or Nothing if denominator is 0 or any of the operands
     * is negative.
     */
  public:
    ::std::optional<Integer> mod(const Integer &op2) const;

    /**
     * Compute the power of this integer raised to the given integer.
     * For any value x, including x==0, x.pow(0) yields 1.
     * @param y an integer to raise
     * @return a new Integer
     */
  public:
    Integer pow(::std::uint32_t op2) const;

    /**
     * Min of this integer and that integer.
     * @param y an integer
     * @return a new Integer
     */
  public:
    Integer min(const Integer &op2) const;

    /**
     * Max of this integer and that integer.
     * @param y an integer
     * @return a new Integer
     */
  public:
    Integer max(const Integer &op2) const;

    /**
     * Determine the operands whose abs value is minimal.
     * Basically, this function implements <code>x.abs().compare(y.abs())<0 ? x : y</code>
     * If the abs values are the same, then max() is used to determine the result value, e.g.,
     * given x=5,y-5, x.absMin(y) yields y
     *
     * @param y an integer
     * @return *this or y
     */
  public:
    Integer absMin(const Integer &op2) const;

    /**
     * Determine the operands whose abs value is maximal.
     * Basically, this function implements <code>x.abs().compare(y.abs())>0 ? x : y</code>.
     * If the abs values are the same, then max() is used to determine the result value, e.g.,
     * given x=5,y-5, x. Max(y) yields x. This condition guarantees that x.absMax(y) == x.absMin(y) iff
     * x==y.
     * @param y an integer
     * @return *this or y
     */
  public:
    Integer absMax(const Integer &op2) const;

    /**
     * Write an int to a stream
     * @param out a stream
     * @return the outoput stream
     */
  public:
    ::std::ostream& write(::std::ostream &out) const;

    /**
     * Get the string representation.
     * @return a string
     */
  public:
    ::std::string toString() const;

    /** The operators */
  public:
    inline friend ::std::optional<Integer> operator+(const Integer &x, const Integer &y)
    {
      return x.add(y);
    }

    inline friend ::std::optional<Integer> operator-(const Integer &x, const Integer &y)
    {
      return x.subtract(y);
    }

    inline friend Integer operator*(const Integer &x, const Integer &y)
    {
      return x.multiply(y);
    }

    inline friend ::std::optional<Integer> operator/(const Integer &x, const Integer &y)
    {
      return x.divide(y);
    }

    inline friend ::std::optional<Integer> operator%(const Integer &x, const Integer &y)
    {
      return x.mod(y);
    }

    inline friend Integer operator-(const Integer &x)
    {
      return x.negate();
    }

    inline friend bool operator<(const Integer &x, const Integer &y)
    {
      return x.compare(y) < 0;
    }

    inline friend bool operator<=(const Integer &x, const Integer &y)
    {
      return x.compare(y) <= 0;
    }

    inline friend bool operator>(const Integer &x, const Integer &y)
    {
      return x.compare(y) > 0;
    }

    inline friend bool operator>=(const Integer &x, const Integer &y)
    {
      return x.compare(y) >= 0;
    }

    inline friend bool operator==(const Integer &x, const Integer &y)
    {
      return x.compare(y) == 0;
    }

    inline friend bool operator!=(const Integer &x, const Integer &y)
    {
      return x.compare(y) != 0;
    }

    inline friend ::std::ostream& operator<<(::std::ostream &s, const Integer &x)
    {
      return x.write(s);
    }

    /** The integer value or nothing if infinity */
  private:
    ::std::optional<BigInt> _value;

    /** The sign (-1,0,1) */
  private:
    char _sign;
  }
  ;

}
#endif
