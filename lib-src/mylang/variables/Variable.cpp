#include <mylang/variables/Variable.h>

namespace mylang {
  namespace variables {

    Variable::Variable(Kind k, Scope s)
        : kind(k), scope(s)
    {
    }

    Variable::~Variable()
    {
    }

    Variable::Ptr Variable::create(Kind k, Scope s)
    {
      struct Impl: public Variable
      {
        Impl(Kind k, Scope s)
            : Variable(k, s)
        {
        }

        ~Impl()
        {
        }
      };
      return ::std::make_shared<Impl>(k, s);
    }

    Variable::Ptr Variable::createParameter()
    {
      return create(KIND_PARAMETER, SCOPE_LOCAL);
    }

    Variable::Ptr Variable::createGlobal()
    {
      return create(KIND_VALUE, SCOPE_GLOBAL);
    }

    Variable::Ptr Variable::createLocal(bool isMutable)
    {
      return create(isMutable ? KIND_VARIABLE : KIND_VALUE, SCOPE_LOCAL);
    }

    Variable::Ptr Variable::createProcessVariable(bool isMutable)
    {
      return create(isMutable ? KIND_VARIABLE : KIND_VALUE, SCOPE_PROCESS);
    }

    Variable::Ptr Variable::createInputVariable()
    {
      return create(KIND_INPUT, SCOPE_PROCESS);
    }

    Variable::Ptr Variable::createOutputVariable()
    {
      return create(KIND_OUTPUT, SCOPE_PROCESS);
    }

  }
}
