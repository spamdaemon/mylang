#ifndef CLASS_MYLANG_VARIABLES_VARIABLE_H
#define CLASS_MYLANG_VARIABLES_VARIABLE_H
#include <memory>

namespace mylang {
  namespace variables {

    /**
     * Provide information about a variable or function.
     */
    class Variable: public std::enable_shared_from_this<Variable>
    {
      /** The scope in which the variable is defined */
    public:
      enum Scope
      {
        /** Defined at the global scope */
        SCOPE_GLOBAL,
        /** Defined a local function scope */
        SCOPE_LOCAL,
        /** Defined a the variable scope */
        SCOPE_PROCESS
      };

      /** The scope in which the variable is defined */
    public:
      enum Kind
      {
        /** Variable is a mutable variable */
        KIND_VARIABLE,
        /** Variable is an immutable value */
        KIND_VALUE,
        /** An input variable */
        KIND_INPUT,
        /** An output variable */
        KIND_OUTPUT,
        /** A parameter */
        KIND_PARAMETER
      };

      /** A shared pointer */
    public:
      typedef ::std::shared_ptr<const Variable> Ptr;

      /** Create a new name */
    private:
      Variable(Kind k, Scope s);

      /** Destructor */
    public:
      virtual ~Variable();

      /**
       * Create a global variable.
       */
    public:
      static Ptr createGlobal();

      /**
       * Create a local variable.
       * @param isMutable true if the variable is KIND_VARIABLE, false if it a KIND_VALUE
       */
    public:
      static Ptr createLocal(bool isMutable);

      /**
       * Create a process variable.
       * @param isMutable true if the variable is KIND_VARIABLE, false if it a KIND_VALUE
       */
    public:
      static Ptr createProcessVariable(bool isMutable);

      /**
       * Create a process input variable.
       */
    public:
      static Ptr createInputVariable();

      /**
       * Create a process input variable.
       */
    public:
      static Ptr createOutputVariable();

      /**
       * Create a function parameter
       */
    public:
      static Ptr createParameter();

      /**
       * Create a local variable.
       * @param k the kind of variable
       * @param k a scope
       */
    private:
      static Ptr create(Kind k, Scope s);

      /** THe variable's kind */
    public:
      const Kind kind;

      /** The scope in which the variable is defined */
    public:
      const Scope scope;
    };
  }
}
#endif
