#include <mylang/constraints/ConstrainedTypeSet.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/constraints/ConstrainedGenericType.h>
#include <mylang/types.h>

namespace mylang {
  namespace constraints {

    ConstrainedGenericType::ConstrainedGenericType()
    {
    }

    ConstrainedGenericType::~ConstrainedGenericType()
    {
    }

    void ConstrainedGenericType::addRecursive(ConstrainedTypeSet &ts) const
    {
      ts.add(self());
    }

    ConstrainedType::Ptr ConstrainedGenericType::normalize(ConstrainedTypeSet &ts) const
    {
      return ts.add(self()).first;
    }

    bool ConstrainedGenericType::isSameConstraint(const ConstrainedType &other) const
    {
      return dynamic_cast<const ConstrainedGenericType*>(&other) != nullptr;
    }

    ::std::string ConstrainedGenericType::toString() const
    {
      return "generic";
    }

    void ConstrainedGenericType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitGeneric(::std::dynamic_pointer_cast<const ConstrainedGenericType>(self()));
    }

    ::std::shared_ptr<const ConstrainedGenericType> ConstrainedGenericType::create()
    {
      struct Impl: public ConstrainedGenericType
      {
        Impl()
            : ConstrainedGenericType(), the_type(mylang::types::GenericType::create())
        {
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

      private:
        const TypePtr the_type;
      };
      return ::std::make_shared<Impl>();
    }

    ConstrainedTypePtr ConstrainedGenericType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::GenericType>&)
    {
      return create();
    }

  }
}
