#include <mylang/constraints/ConstrainedType.h>
#include <mylang/constraints.h>
#include <mylang/types/TypeVisitor.h>
#include <mylang/names/Name.h>

namespace mylang {
  namespace constraints {
    ConstrainedType::ConstrainedType()
        : ConstrainedType(mylang::names::Name::create("_"))
    {
    }

    ConstrainedType::ConstrainedType(const ::mylang::NamePtr &xid)
        : _id(xid)
    {
    }

    ConstrainedType::~ConstrainedType()
    {
    }

    bool ConstrainedType::isAnyType() const
    {
      return type()->isAnyType();
    }

    bool ConstrainedType::isConcrete() const
    {
      return type()->isConcrete();
    }

    Constraints ConstrainedType::constraints() const
    {
      return Constraints();
    }

    ConstrainedType::Ptr ConstrainedType::constrain(const Constraints &constraints) const
    {
      if (constraints.empty()) {
        return self();
      }
      throw ConstraintError(constraints, toString() + " cannot be constrained");
    }

    ConstrainedType::Ptr ConstrainedType::basetype() const
    {
      return nullptr;
    }

    ConstrainedType::Ptr ConstrainedType::roottype() const
    {
      ConstrainedType::Ptr tmp = self();
      while (true) {
        auto base = tmp->basetype();
        if (base) {
          tmp = base;
        } else {
          break;
        }
      }
      return tmp;
    }

    bool ConstrainedType::isBaseTypeOf(const ConstrainedType &derived) const
    {
      const ConstrainedType *t = &derived;
      do {
        if (isSameConstraint(*t)) {
          return true;
        }
        auto bt = t->basetype();
        if (!bt) {
          break;
        }
        t = bt.get();
      } while (t);
      return false;
    }

    bool ConstrainedType::canImplicitlyCastFrom(const ConstrainedType &from) const
    {
      // at a minimum, we must be able to perform a cast between the constraints
      if (!canCastFrom(from)) {
        return false;
      }

      auto thisTy = type();
      auto thatTy = from.type();

      // generally, do not allow an implicit cast between types, but
      // make an exception for base types
      if (isBaseTypeOf(from)) {
        return true;
      }

      auto res = thisTy->canSafeCastFrom(*thatTy);

      return res;
    }

    bool ConstrainedType::canSafeCastFrom(const ConstrainedType &from) const
    {
      if (from.isAnyType()) {
        return true;
      }
      if (isSameConstraint(from)) {
        return true;
      }

      auto bt = from.basetype();
      return bt && canSafeCastFrom(*bt);
    }

    bool ConstrainedType::canCastFrom(const ConstrainedType &from) const
    {
      if (canSafeCastFrom(from)) {
        return true;
      }
      auto bt = from.basetype();
      return bt && canCastFrom(*bt);
    }

    bool ConstrainedType::isConstructible() const
    {
      return false;
    }

    ::mylang::NamePtr ConstrainedType::name() const
    {
      if (!_id) {
        _id = mylang::names::Name::create();
      }
      return _id;
    }

    ConstrainedType::Ptr ConstrainedType::intersectWith(const Ptr &t) const
    {
      return isSameConstraint(*t) ? self() : nullptr;
    }

    ConstrainedType::Ptr ConstrainedType::unionWith(const Ptr &t) const
    {
      if (canSafeCastFrom(*t)) {
        return self();
      }
      if (t->canSafeCastFrom(*this)) {
        return t;
      }
      return nullptr;
    }

    ConstrainedType::Ptr ConstrainedType::createConstraints(const TypePtr &t)
    {
      struct Visitor: public mylang::types::TypeVisitor
      {

        void visit(const ::std::shared_ptr<const mylang::types::AnyType> &type) override
        {
          result = ConstrainedAnyType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::BitType> &type) override
        {
          result = ConstrainedBitType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::BooleanType> &type) override
        {
          result = ConstrainedBooleanType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::ByteType> &type) override
        {
          result = ConstrainedByteType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::CharType> &type) override
        {
          result = ConstrainedCharType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::RealType> &type) override
        {
          result = ConstrainedRealType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::IntegerType> &type) override
        {
          result = ConstrainedIntegerType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::StringType> &type) override
        {
          result = ConstrainedStringType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::VoidType> &type) override
        {
          result = ConstrainedVoidType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::GenericType> &type) override
        {
          result = ConstrainedGenericType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::ArrayType> &type) override
        {
          result = ConstrainedArrayType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::UnionType> &type) override
        {
          result = ConstrainedUnionType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::StructType> &type) override
        {
          result = ConstrainedStructType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::TupleType> &type) override
        {
          result = ConstrainedTupleType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::FunctionType> &type)
        override
        {
          result = ConstrainedFunctionType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::OptType> &type) override
        {
          result = ConstrainedOptType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::MutableType> &type) override
        {
          result = ConstrainedMutableType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::NamedType> &type) override
        {
          result = ConstrainedNamedType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::InputType> &type) override
        {
          result = ConstrainedInputType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::OutputType> &type) override
        {
          result = ConstrainedOutputType::createUnconstrained(type);
        }

        void visit(const ::std::shared_ptr<const mylang::types::ProcessType> &type) override
        {
          result = ConstrainedProcessType::createUnconstrained(type);
        }

        ConstrainedTypePtr result;
      };

      if (t) {
        Visitor v;
        t->accept(v);
        return v.result;
      } else {
        return nullptr;
      }
    }

  }
}
