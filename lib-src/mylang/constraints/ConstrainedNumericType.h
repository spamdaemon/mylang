#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDNUMERICTYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDNUMERICTYPE_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDPRIMITIVETYPE_H
#include <mylang/constraints/ConstrainedPrimitiveType.h>
#endif

namespace mylang {
  namespace constraints {

    class ConstrainedNumericType: public ConstrainedPrimitiveType
    {

    protected:
      ConstrainedNumericType();

    public:
      ~ConstrainedNumericType();
    };
  }
}
#endif
