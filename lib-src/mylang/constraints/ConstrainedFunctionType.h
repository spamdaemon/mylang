#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDFUNCTIONTYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDFUNCTIONTYPE_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPE_H
#include <mylang/constraints/ConstrainedType.h>
#endif
#ifndef FILE_MYLANG_TYPES_H
#include <mylang/types.h>
#endif
#include <memory>
#include <vector>

namespace mylang {
  namespace constraints {

    /** A constraint representing an error */
    class ConstrainedFunctionType: public ConstrainedType
    {

    private:
      ConstrainedFunctionType(const ConstrainedType::Ptr &ret,
          const ::std::vector<ConstrainedType::Ptr> &xparameters);

    public:
      ~ConstrainedFunctionType();

      /**
       * Create a type constraint.
       * @param t a type
       * @return an "unconstrained" constraint
       */
    public:
      static Ptr createUnconstrained(const ::std::shared_ptr<const mylang::types::FunctionType> &t);

      /** Two primitives are the same if their constraint ids are the same  */
    public:
      bool canSafeCastFrom(const ConstrainedType &t) const override;

      bool canCastFrom(const ConstrainedType &t) const override;

      Ptr normalize(ConstrainedTypeSet &ts) const override;

      void addRecursive(ConstrainedTypeSet &ts) const override;

      void accept(ConstrainedTypeVisitor &v) const override;

      bool isSameConstraint(const ConstrainedType &other) const final override;

      ::std::string toString() const final override;

      /**
       * Check if the argument match this signature. A null argument acts like a wildcard.
       * @param argument types
       * @return true if the arguments exactly match this signature
       */
    public:
      bool matchParameters(const ::std::vector<ConstrainedType::Ptr> &argv) const;

      /**
       * Create a function constraint.
       * @param res the return constraint
       * @param parameters the positional parameters
       */
    public:
      static ::std::shared_ptr<const ConstrainedFunctionType> get(const ConstrainedType::Ptr &ret,
          const ::std::vector<ConstrainedType::Ptr> &xparameters);

      /** The return constraint */
    public:
      const ConstrainedType::Ptr returnType;

      /** The return constraint */
    public:
      const ::std::vector<ConstrainedType::Ptr> parameters;
    };
  }
}
#endif
