#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDGENERICTYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDGENERICTYPE_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPE_H
#include <mylang/constraints/ConstrainedType.h>
#endif
#ifndef FILE_MYLANG_TYPES_H
#include <mylang/types.h>
#endif
#include <memory>

namespace mylang {
  namespace constraints {

    /** A constraint representing an error */
    class ConstrainedGenericType: public ConstrainedType
    {

    private:
      ConstrainedGenericType();

    public:
      ~ConstrainedGenericType();

      /**
       * Create a type constraint.
       * @param t a type
       * @return an "unconstrained" constraint
       */
    public:
      static Ptr createUnconstrained(const ::std::shared_ptr<const mylang::types::GenericType> &t);

      /** Two primitives are the same if their constraint ids are the same  */
    public:
      Ptr normalize(ConstrainedTypeSet &ts) const override;

      void addRecursive(ConstrainedTypeSet &ts) const override;

      void accept(ConstrainedTypeVisitor &v) const override;

      bool isSameConstraint(const ConstrainedType &other) const final override;

      ::std::string toString() const final override;

    public:
      static ::std::shared_ptr<const ConstrainedGenericType> create();
    };
  }
}
#endif
