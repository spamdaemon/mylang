#include <mylang/constraints/ConstrainedFunctionType.h>
#include <mylang/constraints/ConstrainedTypeSet.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/types.h>

namespace mylang {
  namespace constraints {

    ConstrainedFunctionType::ConstrainedFunctionType(const ConstrainedType::Ptr &ret,
        const ::std::vector<ConstrainedType::Ptr> &xparameters)
        : returnType(ret), parameters(xparameters)
    {
    }

    ConstrainedFunctionType::~ConstrainedFunctionType()
    {
    }

    void ConstrainedFunctionType::addRecursive(ConstrainedTypeSet &ts) const
    {
      returnType->addRecursive(ts);
      for (auto v : parameters) {
        v->addRecursive(ts);
      }
      ts.add(self());
    }

    bool ConstrainedFunctionType::canSafeCastFrom(const ConstrainedType &t) const
    {
      auto that = dynamic_cast<const ConstrainedFunctionType*>(&t);
      bool ok = false;
      if (that && that->parameters.size() == parameters.size()
          && returnType->canSafeCastFrom(*that->returnType)) {

        ok = true;
        // for the parameters, we need to invert the test and ensure that we can
        // cast the parameters to the of t!
        for (size_t i = 0; ok && i < parameters.size(); ++i) {
          ok = that->parameters.at(i)->canSafeCastFrom(*parameters.at(i));
        }
      }
      return ok || ConstrainedType::canSafeCastFrom(t);
    }

    bool ConstrainedFunctionType::canCastFrom(const ConstrainedType &t) const
    {
      // this kind of cast will most likely require a lambda function
      // that adapts the parameters
      auto that = dynamic_cast<const ConstrainedFunctionType*>(&t);
      bool ok = false;
      if (that && that->parameters.size() == parameters.size()
          && returnType->canCastFrom(*that->returnType)) {

        ok = true;
        // for the parameters, we need to invert the test and ensure that we can
        // cast the parameters to the of t!
        for (size_t i = 0; ok && i < parameters.size(); ++i) {
          ok = that->parameters.at(i)->canCastFrom(*parameters.at(i));
        }
      }
      return ok || ConstrainedType::canCastFrom(t);
    }

    ConstrainedType::Ptr ConstrainedFunctionType::normalize(ConstrainedTypeSet &ts) const
    {
      auto ret = returnType->normalize(ts);
      ::std::vector<ConstrainedType::Ptr> parms;
      for (auto v : parameters) {
        parms.push_back(v->normalize(ts));
      }
      return ts.add(get(ret, parms)).first;
    }

    void ConstrainedFunctionType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitFunction(::std::dynamic_pointer_cast<const ConstrainedFunctionType>(self()));
    }

    bool ConstrainedFunctionType::isSameConstraint(const ConstrainedType &other) const
    {
      auto t = dynamic_cast<const ConstrainedFunctionType*>(&other);
      if (!t) {
        return false;
      }

      if (t->parameters.size() != parameters.size()
          || !t->returnType->isSameConstraint(*returnType)) {
        return false;
      }
      for (size_t i = 0; i < parameters.size(); ++i) {
        if (!t->parameters[i]->isSameConstraint(*parameters[i])) {
          return false;
        }
      }
      return true;
    }

    ::std::string ConstrainedFunctionType::toString() const
    {
      ::std::string s;
      s += '(';
      for (auto &p : parameters) {
        if (s.size() > 1) {
          s += ',';
        }
        s += p->toString();
      }
      s += ')';
      s += ':';
      s += returnType->toString();
      return s;
    }

    bool ConstrainedFunctionType::matchParameters(
        const ::std::vector<ConstrainedType::Ptr> &argv) const
    {
      if (argv.size() != parameters.size()) {
        return false;
      }
      for (size_t i = 0; i < argv.size(); ++i) {
        if (!argv[i]) {
          throw ::std::runtime_error("Wildcard matching is not yet implemented");
        }
        if (argv[i] && !parameters[i]->isSameConstraint(*argv[i])) {
          return false;
        }
      }
      return true;
    }

    ::std::shared_ptr<const ConstrainedFunctionType> ConstrainedFunctionType::get(
        const ConstrainedType::Ptr &ret, const ::std::vector<ConstrainedType::Ptr> &xparameters)
    {
      struct Impl: public ConstrainedFunctionType
      {
        Impl(const ConstrainedType::Ptr &ret, const ::std::vector<ConstrainedType::Ptr> &args)
            : ConstrainedFunctionType(ret, args)
        {
          ::std::vector<TypePtr> argTypes;
          for (auto arg : args) {
            argTypes.push_back(arg->type());
          }
          the_type = mylang::types::FunctionType::get(ret->type(), argTypes);
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

      private:
        TypePtr the_type;
      };
      return ::std::make_shared<Impl>(ret, xparameters);
    }

    ConstrainedTypePtr ConstrainedFunctionType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::FunctionType> &t)
    {
      auto ret = createConstraints(t->returnType);
      ::std::vector<ConstrainedType::Ptr> params;
      for (auto p : t->parameters) {
        params.push_back(createConstraints(p));
      }
      return get(ret, params);
    }

  }
}
