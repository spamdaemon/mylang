#include <mylang/constraints/ConstrainedInputType.h>
#include <mylang/constraints/ConstrainedTypeSet.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/defs.h>
#include <mylang/types/InputType.h>
#include <stdexcept>
#include <string>

namespace mylang {
  namespace constraints {

    ConstrainedInputType::ConstrainedInputType(ConstrainedType::Ptr xelement)
        : element(xelement)
    {
      if (!element) {
        throw std::invalid_argument("Missing element");
      }
      if (!element->isAnyType() && !element->isConcrete()) {
        throw ::std::invalid_argument("Not a concrete element " + xelement->toString());
      }
    }

    ConstrainedInputType::~ConstrainedInputType()
    {
    }

    ::std::string ConstrainedInputType::toString() const
    {
      return "=>" + element->toString();
    }

    void ConstrainedInputType::addRecursive(ConstrainedTypeSet &ts) const
    {
      element->addRecursive(ts);
      ts.add(self());
    }

    ConstrainedType::Ptr ConstrainedInputType::normalize(ConstrainedTypeSet &ts) const
    {
      return ts.add(get(element->normalize(ts))).first;
    }

    bool ConstrainedInputType::isSameConstraint(const ConstrainedType &other) const
    {
      auto t = dynamic_cast<const ConstrainedInputType*>(&other);
      if (!t) {
        return false;
      }
      return element == t->element || element->isSameConstraint(*t->element);
    }

    void ConstrainedInputType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitInputType(self<ConstrainedInputType>());
    }

    ::std::shared_ptr<const ConstrainedInputType> ConstrainedInputType::get(
        ConstrainedType::Ptr xelement)
    {
      struct Impl: public ConstrainedInputType
      {
        Impl(ConstrainedType::Ptr t)
            : ConstrainedInputType(t), the_type(mylang::types::InputType::get(t->type()))
        {
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

      private:
        const TypePtr the_type;

      };
      if (!xelement) {
        throw ::std::invalid_argument("Missing element type");
      }
      return ::std::make_shared<const Impl>(xelement);
    }

    ::std::shared_ptr<const ConstrainedInputType> ConstrainedInputType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::InputType> &t)
    {
      return get(createConstraints(t->element));
    }

  }
}
