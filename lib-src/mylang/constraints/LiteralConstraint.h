#ifndef CLASS_MYLANG_CONSTRAINTS_LITERALCONSTRAINT_H
#define CLASS_MYLANG_CONSTRAINTS_LITERALCONSTRAINT_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINT_H
#include <mylang/constraints/Constraint.h>
#endif

#ifndef CLASS_MYLANG_EXPRESSIONS_CONSTANT_H
#include <mylang/expressions/Constant.h>
#endif

namespace mylang {
  namespace constraints {

    class LiteralConstraint: public mylang::constraints::Constraint
    {
    protected:
      LiteralConstraint(const mylang::expressions::Constant::Ptr &expr);

    public:
      ~LiteralConstraint();

      bool isSameConstraint(const Constraint &other) const;

      /**
       * Get a named constraint
       * @param name the name of the constraint
       * @param expr the expression
       * @return a named constraint
       */
    public:
      static ::std::shared_ptr<const LiteralConstraint> get(
          const mylang::expressions::Constant::Ptr &expr);

      /**
       * Expression
       */
    public:
      const mylang::expressions::Constant::Ptr expression;
    };
  }
}
#endif
