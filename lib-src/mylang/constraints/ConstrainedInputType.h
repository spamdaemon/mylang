#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDINPUTTYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDINPUTTYPE_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPE_H
#include <mylang/constraints/ConstrainedType.h>
#endif
#ifndef FILE_MYLANG_TYPES_H
#include <mylang/types.h>
#endif
#include <memory>

namespace mylang {
  namespace constraints {

    /** A constraint representing an error */
    class ConstrainedInputType: public ConstrainedType
    {

    private:
      ConstrainedInputType(ConstrainedType::Ptr xelement);

    public:
      ~ConstrainedInputType();

      /**
       * Create a type constraint.
       * @param t a type
       * @return an "unconstrained" constraint
       */
    public:
      static ::std::shared_ptr<const ConstrainedInputType> createUnconstrained(
          const ::std::shared_ptr<const mylang::types::InputType> &t);

    public:
      static ::std::shared_ptr<const ConstrainedInputType> get(ConstrainedType::Ptr xelement);

    public:
      Ptr normalize(ConstrainedTypeSet &ts) const override;

      void addRecursive(ConstrainedTypeSet &ts) const override;

      void accept(ConstrainedTypeVisitor &v) const override;

      ::std::string toString() const final override;

      bool isSameConstraint(const ConstrainedType &other) const override;

      /** The element constraint */
    public:
      const ConstrainedType::Ptr element;
    };
  }
}
#endif
