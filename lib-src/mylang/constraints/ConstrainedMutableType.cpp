#include <mylang/constraints/ConstrainedMutableType.h>
#include <mylang/constraints/ConstrainedTypeSet.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/defs.h>
#include <mylang/types/MutableType.h>
#include <stdexcept>
#include <string>

namespace mylang {
  namespace constraints {

    ConstrainedMutableType::ConstrainedMutableType(ConstrainedType::Ptr xelement)
        : element(xelement)
    {
      if (!element) {
        throw std::invalid_argument("Missing element");
      }
      if (!element->isConcrete()) {
        throw ::std::invalid_argument("Not a concrete element " + xelement->toString());
      }
    }

    ConstrainedMutableType::~ConstrainedMutableType()
    {
    }

    ConstrainedType::Ptr ConstrainedMutableType::flatten() const
    {
      auto opt = ::std::dynamic_pointer_cast<const ConstrainedMutableType>(element);
      if (opt) {
        return opt->flatten();
      } else {
        return element;
      }
    }

    ::std::string ConstrainedMutableType::toString() const
    {
      return element->toString() + '!';
    }

    void ConstrainedMutableType::addRecursive(ConstrainedTypeSet &ts) const
    {
      if (ts.add(self()).second) {
        ts.add(self());
        element->addRecursive(ts);
      }
    }

    ConstrainedType::Ptr ConstrainedMutableType::normalize(ConstrainedTypeSet &ts) const
    {
      return ts.add(get(element->normalize(ts))).first;
    }

    bool ConstrainedMutableType::isSameConstraint(const ConstrainedType &other) const
    {
      auto t = dynamic_cast<const ConstrainedMutableType*>(&other);
      if (!t) {
        return false;
      }
      return element == t->element || element->isSameConstraint(*t->element);
    }

    bool ConstrainedMutableType::isConstructible() const
    {
      return true;
    }

    void ConstrainedMutableType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitMutable(::std::dynamic_pointer_cast<const ConstrainedMutableType>(self()));
    }

    ::std::shared_ptr<const ConstrainedMutableType> ConstrainedMutableType::copy(
        const ConstrainedType::Ptr &newElement) const
    {
      return get(newElement);
    }

    ::std::shared_ptr<const ConstrainedMutableType> ConstrainedMutableType::get(
        ConstrainedType::Ptr xelement)
    {
      struct Impl: public ConstrainedMutableType
      {
        Impl(ConstrainedType::Ptr t)
            : ConstrainedMutableType(t), the_type(mylang::types::MutableType::get(t->type()))
        {
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

      private:
        const TypePtr the_type;

      };
      if (!xelement) {
        throw ::std::invalid_argument("Missing element type");
      }
      return ::std::make_shared<const Impl>(xelement);
    }

    ::std::shared_ptr<const ConstrainedMutableType> ConstrainedMutableType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::MutableType> &t)
    {
      return get(createConstraints(t->element));
    }

  }
}
