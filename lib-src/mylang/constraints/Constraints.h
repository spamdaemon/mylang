#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINTS_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINTS_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINT_H
#include <mylang/constraints/Constraint.h>
#endif
#include <map>
#include <functional>

namespace mylang {
  namespace constraints {

    /**
     * A constraint on a constraint.
     */
    class Constraints
    {
      /**  A map of constraints */
    public:
      typedef ::std::map<::std::string, Constraint::Ptr> ConstraintsByName;

      /**
       * An empty constraints object.
       */
    public:
      Constraints();

      /**
       * Create constraint with a single constraint
       * @param name constraint name
       * @param c the constraint
       * @param byname constraints by name
       */
    public:
      Constraints(const ::std::string &name, const Constraint::Ptr &c);

      /**
       * Create constraints.
       * @param byname constraints by name
       */
    public:
      Constraints(const ConstraintsByName &byname);

      /** Destructor */
    public:
      ~Constraints();

      /**
       * Get the constraint that are not among the allowed ones.
       * @param allowed a list of allowed constraint names
       * @return the constraint set only includes the named constraints
       */
    public:
      ::std::vector<::std::string> findUnknownConstraints(
          const ::std::vector<::std::string> &names) const;

      /**
       * True if there are no constraints
       */
    public:
      inline bool empty() const
      {
        return _constraints.empty();
      }

      /**
       * Determine if the specified constraint exists
       * @param name the name of a constraint
       * @return true if the constraint exists
       */
    public:
      bool hasConstraint(const ::std::string &name) const;

      /**
       * Get a constraint by name.
       * @param name a constraint name
       * @return the constraint or null if not specified
       */
    public:
      Constraint::Ptr get(const ::std::string &name) const;

      /**
       * Put a new constraint.
       * @param name a constraint name
       * @param c the constraint to add
       * @throws constraint error  if there are duplicates
       */
    public:
      void put(const ::std::string &name, const Constraint::Ptr &c);

      /**
       * Merge the constraints from the specified constraint object into this constraits object.
       * @param other constraints
       * @throws constraint error  if there are duplicates
       */
    public:
      void put(const Constraints &other);

      /** The constraints */
    private:
      ConstraintsByName _constraints;
    };
  }
}
#endif
