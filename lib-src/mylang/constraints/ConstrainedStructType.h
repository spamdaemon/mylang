#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDSTRUCTTYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDSTRUCTTYPE_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPE_H
#include <mylang/constraints/ConstrainedType.h>
#endif
#ifndef FILE_MYLANG_TYPES_H
#include <mylang/types.h>
#endif
#include <memory>
#include <map>
#include <vector>

namespace mylang {
  namespace constraints {

    class ConstrainedTupleType;

    /** A constraint representing an error */
    class ConstrainedStructType: public ConstrainedType
    {

    public:
      struct Member
      {

        Member(const ::std::string &xname, const ConstrainedType::Ptr &xtype);

        /** The name of the parameter */
        const ::std::string name;

        /** The parameter constraint */
        const ConstrainedType::Ptr constraint;
      };

    private:
      ConstrainedStructType(const ::std::vector<Member> &members);

    public:
      ~ConstrainedStructType();

      /**
       * Create a type constraint.
       * @param t a type
       * @return an "unconstrained" constraint
       */
    public:
      static Ptr createUnconstrained(const ::std::shared_ptr<const mylang::types::StructType> &t);

      /** Two primitives are the same if their constraint ids are the same  */
    public:
      Ptr normalize(ConstrainedTypeSet &ts) const override;

      void addRecursive(ConstrainedTypeSet &ts) const override;

      void accept(ConstrainedTypeVisitor &v) const override;

      bool isSameConstraint(const ConstrainedType &other) const final override;

      ::std::string toString() const final override;

      bool canSafeCastFrom(const ConstrainedType &t) const override;

      bool canCastFrom(const ConstrainedType &t) const override;

      bool isConstructible() const override;

      Ptr intersectWith(const Ptr &t) const override;

      Ptr unionWith(const Ptr &t) const override;

      /**
       * Get the corresponding tuple type.
       * @return the corresponding tuple type
       */
    public:
      ::std::shared_ptr<const ConstrainedTupleType> getPeerTuple() const;

      /**
       * Get a structure
       * @param members
       */
    public:
      static ::std::shared_ptr<const ConstrainedStructType> get(
          const ::std::vector<Member> &xmembers);

      /**
       * Get the constraint of the subfield with the specified name.
       * @param name a subfield
       * @return the constraint of the subfield or nullptr if not found
       */
    public:
      ConstrainedType::Ptr getMemberType(const ::std::string &field) const;

      /** The members */
    public:
      const ::std::vector<Member> members;

      /** The indexed members */
    private:
      ::std::map<::std::string, const Member> _indexedMembers;
    };
  }
}
#endif
