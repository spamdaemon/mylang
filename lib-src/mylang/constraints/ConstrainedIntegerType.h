#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDINTEGERTYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDINTEGERTYPE_H
#ifndef FILE_MYLANG_TYPES_H
#include <mylang/types.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDNUMERICTYPE_H
#include <mylang/constraints/ConstrainedNumericType.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_LITERALCONSTRAINT_H
#include <mylang/constraints/LiteralConstraint.h>
#endif

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_INTERVAL_H
#include <mylang/Interval.h>
#endif

namespace mylang {
  namespace constraints {

    /** The baseclass for all types */
    class ConstrainedIntegerType: public ConstrainedNumericType
    {
      /** The range associated with this constraint */
    public:
      using Range = Interval;

    private:
      ConstrainedIntegerType(const Range &xrng);

    public:
      ~ConstrainedIntegerType();

      /**
       * Create a type constraint.
       * @param t a type
       * @return an "unconstrained" constraint
       */
    public:
      static Ptr createUnconstrained(const ::std::shared_ptr<const mylang::types::IntegerType> &t);

      /**
       * Get the bit constraint
       * @return a primitive constraint for a INTEGER
       */
    public:
      static ::std::shared_ptr<const ConstrainedIntegerType> create();

      /**
       * Get an integer int the specified range.
       * @return a primitive constraint for a INTEGER
       */
    public:
      static ::std::shared_ptr<const ConstrainedIntegerType> create(const Range &range);

      /**
       * Get the constraint for a constant.
       * @param c a constant
       * @return the constraint for the specified constant
       */
    public:
      static ::std::shared_ptr<const ConstrainedIntegerType> create(const BigInt &c);

      /**
       * Create a new constraint that increments the upper bound by 1. This useful for indexing.
       * @return a new constraint
       */
    public:
      ::std::shared_ptr<const ConstrainedIntegerType> incrementMax() const;

    public:
      bool canSafeCastFrom(const ConstrainedType &t) const override;

      bool canCastFrom(const ConstrainedType &t) const override;

      void accept(ConstrainedTypeVisitor &v) const override;

      bool isSameConstraint(const ConstrainedType &other) const final override;

      ::std::string toString() const final override;

      Constraints constraints() const override;

      Ptr constrain(const Constraints &constraints) const override;

      Ptr unionWith(const Ptr &t) const override;

      Ptr intersectWith(const Ptr &t) const override;

      /**
       * True if the type is unsigned.
       */
    public:
      bool isUnsigned() const;
      /**
       * Get the negated type
       * @return the negated type
       */
    public:
      ::std::shared_ptr<const ConstrainedIntegerType> negate() const;

      /**
       * Get the closest signed integer type. A signed integer type must be
       * at least an 8bit, 16bits, 32bit and 64bit wide types.
       * @return the closest signed integer type for this type
       */
    public:
      ::std::shared_ptr<const ConstrainedIntegerType> getSignedInt() const;

      /**
       * Get the closest unsigned integer type. A signed integer type must be
       * at least an 8bit, 16bits, 32bit and 64bit wide types.
       * @return the closest signed integer type for this type
       */
    public:
      ::std::shared_ptr<const ConstrainedIntegerType> getUnsignedInt() const;

      /** The range of this integer constraint */
    public:
      const Range range;
    };
  }
}
#endif
