#include <mylang/constraints/ConstraintError.h>
#include <mylang/constraints/Constraints.h>

namespace mylang {
  namespace constraints {

    Constraints::Constraints()
    {
    }

    Constraints::Constraints(const ::std::string &name, const Constraint::Ptr &c)
    {
      _constraints[name] = c;
    }

    Constraints::Constraints(const ConstraintsByName &byname)
        : _constraints(byname)
    {
    }

    Constraints::~Constraints()
    {
    }

    ::std::vector<::std::string> Constraints::findUnknownConstraints(
        const ::std::vector<::std::string> &names) const
    {
      ::std::vector<::std::string> unknown;
      for (auto c : _constraints) {
        bool found = false;
        for (auto n : names) {
          if (c.first == n) {
            found = true;
            break;
          }
        }
        if (!found) {
          unknown.push_back(c.first);
        }
      }
      return unknown;
    }

    bool Constraints::hasConstraint(const ::std::string &name) const
    {
      return _constraints.find(name) != _constraints.end();
    }

    Constraint::Ptr Constraints::get(const ::std::string &name) const
    {
      auto c = _constraints.find(name);
      if (c != _constraints.end()) {
        return c->second;
      }
      return nullptr;
    }

    void Constraints::put(const Constraints &other)
    {
      for (auto that : other._constraints) {
        for (auto c : _constraints) {
          if (that.first == c.first) {
            throw ConstraintError(that.first, that.second, "duplicate constraint " + that.first);
          }
        }
      }
      _constraints.insert(other._constraints.begin(), other._constraints.end());
    }

    void Constraints::put(const ::std::string &name, const Constraint::Ptr &c)
    {
      if (name.empty()) {
        throw ::std::invalid_argument("Invalid name");
      }
      if (hasConstraint(name)) {
        throw ConstraintError(name, c, "duplicate constraint " + name);
      }
      _constraints[name] = c;
    }

  }
}
