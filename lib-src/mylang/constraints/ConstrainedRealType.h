#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDREALTYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDREALTYPE_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDNUMERICTYPE_H
#include <mylang/constraints/ConstrainedNumericType.h>
#endif
#ifndef FILE_MYLANG_TYPES_H
#include <mylang/types.h>
#endif

namespace mylang {
  namespace constraints {

    /** The baseclass for all types */
    class ConstrainedRealType: public ConstrainedNumericType
    {

    private:
      ConstrainedRealType();

    public:
      ~ConstrainedRealType();

      /**
       * Create a type constraint.
       * @param t a type
       * @return an "unconstrained" constraint
       */
    public:
      static Ptr createUnconstrained(const ::std::shared_ptr<const mylang::types::RealType> &t);

      /**
       * Get the real constraint
       * @return a primitive constraint for a REAL
       */
    public:
      static ::std::shared_ptr<const ConstrainedRealType> create();

    public:
      bool canSafeCastFrom(const ConstrainedType &t) const override;

      bool canCastFrom(const ConstrainedType &t) const override;

      void accept(ConstrainedTypeVisitor &v) const override;

      bool isSameConstraint(const ConstrainedType &other) const final override;

      ::std::string toString() const final override;

      /** Create the negated type. */
    public:
      ::std::shared_ptr<const ConstrainedRealType> negate() const;
    };
  }
}
#endif
