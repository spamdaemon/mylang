#include <mylang/constraints/ConstrainedTypeSet.h>

namespace mylang {
  namespace constraints {

    ConstrainedTypeSet::ConstrainedTypeSet()
        : _types(::std::make_shared<::std::vector<ConstrainedType::Ptr>>())
    {
    }

    ConstrainedTypeSet::~ConstrainedTypeSet()
    {
    }

    ConstrainedType::Ptr ConstrainedTypeSet::get(const ConstrainedType::Ptr &constraint) const
    {
      if (!constraint) {
        return nullptr;
      }
      for (auto i = _types->begin(); i != _types->end(); ++i) {
        if ((*i)->isSameConstraint(*constraint)) {
          return *i;
        }
      }
      return nullptr;
    }

    std::pair<ConstrainedType::Ptr, bool> ConstrainedTypeSet::add(
        const ConstrainedType::Ptr &constraint)
    {

      if (!constraint) {
        return {constraint, false};
      }
      auto c = get(constraint);
      if (c) {
        return {c, false};
      }
      _types->push_back(constraint);
      return {constraint, true};
    }
  }
}
