#include <mylang/constraints/ConstrainedFunctionType.h>
#include <mylang/constraints/ConstrainedTypeSet.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/constraints/ConstrainedUnionType.h>
#include <mylang/types.h>
#include <mylang/types/SerializableTypeChecker.h>

namespace mylang {
  namespace constraints {

    ConstrainedUnionType::Discriminant::Discriminant(const ::std::string &xname,
        const ConstrainedType::Ptr &xtype)
        : name(xname), constraint(xtype)
    {
      if (!mylang::types::SerializableTypeChecker::isSerializable(xtype->type())) {
        throw ::std::invalid_argument("Discriminant must be serializable");
      }
    }

    ConstrainedUnionType::Member::Member(const mylang::expressions::Constant::Ptr &xdiscriminant,
        const ::std::string &xname, const ConstrainedTypePtr &xtype)
        : discriminant(xdiscriminant), name(xname), constraint(xtype)
    {

    }

    ConstrainedUnionType::ConstrainedUnionType(const Discriminant &xdiscriminant,
        const ::std::vector<Member> &xmembers)
        : discriminant(xdiscriminant), members(xmembers)
    {
      if (members.empty()) {
        throw ::std::invalid_argument("Union must have members");
      }
      for (auto sf : members) {
        if (!sf.discriminant->type()->isSameType(*discriminant.constraint->type())) {
          throw ::std::invalid_argument("Discriminant type mismatch");
        }
        if (sf.name.empty()) {
          throw ::std::invalid_argument("Missing member name");
        }
        if (!sf.constraint) {
          throw ::std::invalid_argument("Missing member constraint");
        }
        if (_indexedMembers.count(sf.name) != 0 || sf.name == discriminant.name) {
          throw ::std::invalid_argument("Duplicate member " + sf.name);
        }

        _indexedMembers.insert(::std::make_pair(sf.name, sf));
      }
      for (size_t i = 0, sz = members.size(); i < sz; ++i) {
        for (size_t j = i + 1; j < sz; ++j) {
          if (members[i].discriminant->isSameConstant(*members[j].discriminant)) {
            throw ::std::invalid_argument(
                "Duplicate discriminant value  " + members[i].discriminant->value());
          }
        }
      }
    }

    ConstrainedUnionType::~ConstrainedUnionType()
    {
    }

    bool ConstrainedUnionType::isConstructible() const
    {
      return true;
    }

    void ConstrainedUnionType::addRecursive(ConstrainedTypeSet &ts) const
    {
      if (ts.add(self()).second) {
        discriminant.constraint->addRecursive(ts);
        for (auto m : members) {
          m.constraint->addRecursive(ts);
        }
      }
    }

    ConstrainedType::Ptr ConstrainedUnionType::normalize(ConstrainedTypeSet &ts) const
    {
      Discriminant d(discriminant.name, discriminant.constraint->normalize(ts));
      ::std::vector<Member> tmp;
      for (auto m : members) {
        auto optTy = ::std::dynamic_pointer_cast<const ConstrainedOptType>(
            m.constraint->normalize(ts));
        tmp.push_back(Member(m.discriminant, m.name, optTy));
      }
      return ts.add(get(d, tmp)).first;
    }

    void ConstrainedUnionType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitUnion(::std::dynamic_pointer_cast<const ConstrainedUnionType>(self()));
    }

    bool ConstrainedUnionType::isSameConstraint(const ConstrainedType &other) const
    {
      auto t = dynamic_cast<const ConstrainedUnionType*>(&other);
      if (!t || t->members.size() != members.size()) {
        return false;
      }
      if (discriminant.name != t->discriminant.name) {
        return false;
      }
      if (!discriminant.constraint->isSameConstraint(*t->discriminant.constraint)) {
        return false;
      }
      for (size_t i = 0; i < members.size(); ++i) {
        if (!members[i].discriminant->isSameConstant(*t->members[i].discriminant)) {
          return false;
        }
        if (members[i].name != t->members[i].name) {
          return false;
        }
        if (!members[i].constraint->isSameConstraint(*t->members[i].constraint)) {
          return false;
        }
      }

      return true;
    }

    ::std::string ConstrainedUnionType::toString() const
    {
      ::std::string str;
      str += "union(";
      str += discriminant.name;
      str += ":";
      str += discriminant.constraint->toString();
      str += ") {";
      if (!members.empty()) {
        str += '\n';
      }
      for (auto sf : members) {
        str += sf.name;
        str += ':';
        str += sf.constraint->toString();
        str += ";\n";
      }
      str += "}";
      return str;
    }

    ::std::shared_ptr<const ConstrainedUnionType> ConstrainedUnionType::get(const Discriminant &xd,
        const ::std::vector<Member> &members)
    {
      struct Impl: public ConstrainedUnionType
      {
        Impl(const Discriminant &xd, const ::std::vector<Member> &t)
            : ConstrainedUnionType(xd, t)
        {
          ::std::vector<mylang::types::UnionType::Member> M;
          mylang::types::UnionType::Discriminant D(discriminant.name,
              discriminant.constraint->type());
          for (auto m : members) {
            M.push_back(
                mylang::types::UnionType::Member(m.discriminant, m.name, m.constraint->type()));
          }
          the_type = mylang::types::UnionType::get(D, M);
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

      private:
        TypePtr the_type;
      };
      return ::std::make_shared<Impl>(xd, members);
    }

    ConstrainedTypePtr ConstrainedUnionType::getMemberType(const ::std::string &field) const
    {
      auto i = _indexedMembers.find(field);
      if (i == _indexedMembers.end()) {
        return nullptr;
      }
      return i->second.constraint;
    }

    ConstrainedTypePtr ConstrainedUnionType::getMemberType(
        const mylang::expressions::Constant &field) const
    {
      for (auto m : members) {
        if (m.discriminant->isSameConstant(field)) {
          return m.constraint;
        }
      }
      return nullptr;
    }

    size_t ConstrainedUnionType::getMemberIndex(const mylang::expressions::Constant &field) const
    {
      for (size_t i = 0, sz = members.size(); i < sz; ++i) {
        if (members[i].discriminant->isSameConstant(field)) {
          return i;
        }
      }
      throw ::std::invalid_argument("no such member index " + field.value());
    }

    ::std::optional<size_t> ConstrainedUnionType::getMemberIndex(const ::std::string &name) const
    {
      for (size_t i = 0, sz = members.size(); i < sz; ++i) {
        if (members[i].name == name) {
          return i;
        }
      }
      return ::std::nullopt;
    }

    ConstrainedTypePtr ConstrainedUnionType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::UnionType> &t)
    {
      ::std::vector<Member> members;
      for (auto m : t->members) {
        members.push_back(
            Member(m.discriminant, m.name, ConstrainedType::createConstraints(m.type)));
      }
      return get(Discriminant(t->discriminant.name, createConstraints(t->discriminant.type)),
          members);
    }

  }
}
