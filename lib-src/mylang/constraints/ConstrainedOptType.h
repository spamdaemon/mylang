#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDOPTTYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDOPTTYPE_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPE_H
#include <mylang/constraints/ConstrainedType.h>
#endif
#ifndef FILE_MYLANG_TYPES_H
#include <mylang/types.h>
#endif
#include <memory>

namespace mylang {
  namespace constraints {

    /** A constraint representing an error */
    class ConstrainedOptType: public ConstrainedType
    {

    private:
      ConstrainedOptType(ConstrainedType::Ptr xelement);

    public:
      ~ConstrainedOptType();

      /**
       * Create a type constraint.
       * @param t a type
       * @return an "unconstrained" constraint
       */
    public:
      static ::std::shared_ptr<const ConstrainedOptType> createUnconstrained(
          const ::std::shared_ptr<const mylang::types::OptType> &t);

      /**
       * Follow the element constraint until an element is found that is not a optional
       * @return an element constraint (maybe nullptr!)
       */
    public:
      ConstrainedType::Ptr flatten() const;

    public:
      static ::std::shared_ptr<const ConstrainedOptType> get(ConstrainedType::Ptr xelement);

    public:
      Ptr normalize(ConstrainedTypeSet &ts) const override;

      void addRecursive(ConstrainedTypeSet &ts) const override;

      void accept(ConstrainedTypeVisitor &v) const override;

      bool canSafeCastFrom(const ConstrainedType &t) const override;

      bool canCastFrom(const ConstrainedType &t) const override;

      ::std::string toString() const final override;

      bool isSameConstraint(const ConstrainedType &other) const override;

      bool isConstructible() const override;

      Ptr intersectWith(const Ptr &t) const override;

      Ptr unionWith(const Ptr &t) const override;

      /**
       * Create a new instanceof of this array, but change the return constraint
       * @param element the new element constraint
       */
    public:
      ::std::shared_ptr<const ConstrainedOptType> copy(
          const ConstrainedType::Ptr &newElement) const;

      /** The element constraint */
    public:
      const ConstrainedType::Ptr element;
    };
  }
}
#endif
