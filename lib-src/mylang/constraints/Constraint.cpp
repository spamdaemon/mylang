#include <mylang/constraints/Constraint.h>

namespace mylang {
  namespace constraints {

    Constraint::Constraint()
    {
    }

    Constraint::~Constraint()
    {
    }

    bool Constraint::compare(const Ptr &c1, const Ptr &c2)
    {
      if (c1 == c2) {
        return true;
      }
      if (c1 && c2) {
        return c1->isSameConstraint(*c2);
      }
      return false;
    }

    bool Constraint::isSameConstraint(const Constraint &other) const
    {
      return this == &other;
    }
  }
}
