#include <mylang/constraints/ConstrainedAnyType.h>
#include <mylang/constraints/ConstrainedArrayType.h>
#include <mylang/constraints/ConstrainedTypeSet.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/constraints/ConstraintError.h>
#include <mylang/constraints/Constraints.h>
#include <mylang/constraints/LiteralConstraint.h>
#include <mylang/expressions/Constant.h>
#include <mylang/types/ArrayType.h>
#include <cassert>
#include <cstddef>
#include <optional>
#include <stdexcept>
#include <string>
#include <vector>

namespace mylang {
  namespace constraints {
    namespace {
      ::std::shared_ptr<const ConstrainedIntegerType> range2length(
          const ConstrainedArrayType::Range &r)
      {
        if (r.min() < 0) {
          return nullptr;
        }
        return ConstrainedIntegerType::create(r);
      }

      ::std::shared_ptr<const ConstrainedIntegerType> length2index(
          const ::std::shared_ptr<const ConstrainedIntegerType> &len)
      {
        if (len->range.max() == 0) {
          return nullptr;
        } else {
          return ConstrainedIntegerType::create(
              ConstrainedIntegerType::Range(0, len->range.max().decrement()));
        }
      }

      ::std::shared_ptr<const ConstrainedIntegerType> length2counter(
          const ::std::shared_ptr<const ConstrainedIntegerType> &len)
      {
        if (len->range.max() == 0) {
          return len;
        } else {
          return ConstrainedIntegerType::create(ConstrainedIntegerType::Range(0, len->range.max()));
        }
      }
    }

    ConstrainedArrayType::ConstrainedArrayType(const Ptr &xelement,
        const ::std::shared_ptr<const ConstrainedIntegerType> &idx,
        const ::std::shared_ptr<const ConstrainedIntegerType> &len,
        const ::std::shared_ptr<const ConstrainedIntegerType> &cnt)
        : element(xelement), index(idx), length(len), counter(cnt), bounds(len->range)
    {
      if (!element) {
        throw std::invalid_argument("Missing element");
      }
      if (!element->isAnyType() && !element->isConcrete()) {
        throw ::std::invalid_argument("Not a concrete element " + xelement->toString());
      }
      assert(bounds.min() >= 0 && "array bounds must be non-negative");
    }

    ConstrainedArrayType::~ConstrainedArrayType()
    {
    }

    void ConstrainedArrayType::addRecursive(ConstrainedTypeSet &ts) const
    {
      if (ts.add(self()).second) {
        element->addRecursive(ts);
        length->addRecursive(ts);
        counter->addRecursive(ts);
        if (index) {
          index->addRecursive(ts);
        }
      }
    }

    ConstrainedType::Ptr ConstrainedArrayType::normalize(ConstrainedTypeSet &ts) const
    {
      auto e = element->normalize(ts);
      auto idx =
          index == nullptr ?
              nullptr :
              ::std::dynamic_pointer_cast<const ConstrainedIntegerType>(index->normalize(ts));
      auto len = ::std::dynamic_pointer_cast<const ConstrainedIntegerType>(length->normalize(ts));
      auto cnt = ::std::dynamic_pointer_cast<const ConstrainedIntegerType>(counter->normalize(ts));

      return ts.add(getArray(e, idx, len, counter)).first;
    }

    ConstrainedType::Ptr ConstrainedArrayType::intersectWith(const Ptr &t) const
    {
      auto that = ::std::dynamic_pointer_cast<const ConstrainedArrayType>(t);
      if (!that) {
        return nullptr;
      }
      auto newBounds = bounds.intersectWith(that->bounds);

      if (!newBounds.has_value()) {
        return nullptr;
      }

      auto e = element->intersectWith(that->element);
      if (!e) {
        return nullptr;
      }
      return getBoundedArray(e, *newBounds);
    }

    ConstrainedType::Ptr ConstrainedArrayType::unionWith(const Ptr &t) const
    {
      ConstrainedType::Ptr res = ConstrainedType::unionWith(t);
      if (res) {
        return res;
      }
      auto that = ::std::dynamic_pointer_cast<const ConstrainedArrayType>(t);
      if (!that) {
        return nullptr;
      }
      auto e = element->unionWith(that->element);
      if (!e) {
        return nullptr;
      }
      auto len = ::std::dynamic_pointer_cast<const ConstrainedIntegerType>(
          length->unionWith(that->length));
      auto idx = length2index(len);
      auto cnt = length2counter(len);
      return getArray(e, idx, len, cnt);
    }

    void ConstrainedArrayType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitArray(::std::dynamic_pointer_cast<const ConstrainedArrayType>(self()));
    }

    ConstrainedType::Ptr ConstrainedArrayType::flatten() const
    {
      auto arr = ::std::dynamic_pointer_cast<const ConstrainedArrayType>(element);
      if (arr) {
        return arr->flatten();
      } else {
        return element;
      }
    }

    ::std::string ConstrainedArrayType::toString() const
    {
      ::std::string res;
      res += element->toString();
      res += '[';
      if (bounds.isFixed()) {
        res += bounds.min().toString();
      } else if (bounds.min() > 0 || bounds.isFinite()) {
        if (bounds.min() > 0) {
          res += bounds.min().toString();
        }
        res += ':';
        if (bounds.isFinite()) {
          res += bounds.max().toString();
        }
      }
      res += ']';
      return res;
    }

    size_t ConstrainedArrayType::dimensionality() const
    {
      size_t dim = 0;
      auto c = self<ConstrainedArrayType>();
      while (c) {
        ++dim;
        c = c->element->self<ConstrainedArrayType>();
      }
      return dim;
    }

    ConstrainedType::Ptr ConstrainedArrayType::leafElement() const
    {
      Ptr res;
      auto c = self<ConstrainedArrayType>();
      while (c) {
        res = c->element;
        c = res->self<ConstrainedArrayType>();
      }
      assert(res);
      return res;
    }

    bool ConstrainedArrayType::isUnbounded() const
    {
      return bounds.isInfinite();
    }

    bool ConstrainedArrayType::isBounded() const
    {
      return bounds.isFinite();
    }

    bool ConstrainedArrayType::isSameConstraint(const ConstrainedType &other) const
    {
      auto that = dynamic_cast<const ConstrainedArrayType*>(&other);
      if (!that) {
        return false;
      }
      if (!element->isSameConstraint(*that->element)) {
        return false;
      }
      if (!(length->isSameConstraint(*that->length))) {
        return false;
      }
      return true;
    }

    bool ConstrainedArrayType::canSafeCastFrom(const ConstrainedType &from) const
    {
      auto other = dynamic_cast<const ConstrainedArrayType*>(&from);
      if (!other) {
        return ConstrainedType::canSafeCastFrom(from);
      }
      if (!bounds.contains(other->bounds)) {
        return false;
      }
      return element->canSafeCastFrom(*other->element);
    }

    bool ConstrainedArrayType::canCastFrom(const ConstrainedType &from) const
    {
      auto other = dynamic_cast<const ConstrainedArrayType*>(&from);
      if (!other) {
        return ConstrainedType::canCastFrom(from);
      }
      if (!bounds.intersectWith(other->bounds).has_value()) {
        return false;
      }
      return element->canCastFrom(*other->element);
    }

    bool ConstrainedArrayType::isConstructible() const
    {
      return true;
    }

    ConstrainedType::Ptr ConstrainedArrayType::constrain(const Constraints &newConstraints) const
    {
      auto min = bounds.min();
      auto max = bounds.max();

      auto unknown = newConstraints.findUnknownConstraints( { "min", "max" });
      if (!unknown.empty()) {
        throw ConstraintError(newConstraints, "unexpected constraint: " + unknown.at(0));
      }

      if (newConstraints.hasConstraint("min")) {
        auto cc = newConstraints.get("min");
        auto c = ::std::dynamic_pointer_cast<const LiteralConstraint>(cc);
        if (c) {
          min = c->expression->getUnsignedInteger();
        } else {
          throw ConstraintError("min", cc, "not a constant expression");
        }
      }
      if (newConstraints.hasConstraint("max")) {
        auto cc = newConstraints.get("max");
        if (cc) {
          auto c = ::std::dynamic_pointer_cast<const LiteralConstraint>(cc);
          if (c) {
            max = c->expression->getUnsignedInteger();
          } else {
            throw ConstraintError("max", cc, "not a constant expression");
          }
        } else {
          max = Integer::PLUS_INFINITY();
        }
      }

      Range b(min, max);
      if (!bounds.contains(b)) {
        throw ConstraintError(newConstraints, "incompatible bounds restriction");
      }
      if (b == bounds) {
        return self();
      }

      ::std::shared_ptr<const ConstrainedIntegerType> idx;
      if (b.max() == 0) {
        // stays nullptr
      } else {
        idx = ConstrainedIntegerType::create(Range(0, b.max().decrement()));
      }

      auto len = ConstrainedIntegerType::create(b);
      auto cnt = length2counter(len);

      return getArray(element, idx, len, cnt);
    }

    Constraints ConstrainedArrayType::constraints() const
    {
      auto t = ConstrainedIntegerType::getInteger();
      Constraints c;
      c.put("min",
          LiteralConstraint::get(
              mylang::expressions::Constant::get(t->type(), bounds.min().toString())));
      if (bounds.max().isFinite()) {
        c.put("max",
            LiteralConstraint::get(
                mylang::expressions::Constant::get(t->type(), bounds.max().toString())));
      }
      return c;
    }

    ::std::shared_ptr<const ConstrainedArrayType> ConstrainedArrayType::copy(
        const ConstrainedType::Ptr &newElement) const
    {
      // FIXME: we need to somehow avoid an infinite loop
      // this->copy(this);
      if (!newElement) {
        throw ::std::invalid_argument("Missing element");
      }
      if (self() == newElement) {
        throw ::std::invalid_argument("Infinite Recursion");
      }
      if (element == newElement) {
        return ::std::dynamic_pointer_cast<const ConstrainedArrayType>(self());
      }
      if (element->isSameConstraint(*newElement)) {
        return ::std::dynamic_pointer_cast<const ConstrainedArrayType>(self());
      }
      return getArray(newElement, index, length, counter);
    }

    ::std::shared_ptr<const ConstrainedArrayType> ConstrainedArrayType::getArray(
        const ConstrainedType::Ptr &xelement,
        const ::std::shared_ptr<const ConstrainedIntegerType> &idx,
        const ::std::shared_ptr<const ConstrainedIntegerType> &len,
        const ::std::shared_ptr<const ConstrainedIntegerType> &counter)
    {
      struct Impl: public ConstrainedArrayType
      {
        Impl(ConstrainedType::Ptr t, const ::std::shared_ptr<const ConstrainedIntegerType> &idx,
            const ::std::shared_ptr<const ConstrainedIntegerType> &len,
            const ::std::shared_ptr<const ConstrainedIntegerType> &cnt)
            : ConstrainedArrayType(t, idx, len, cnt),
                the_type(mylang::types::ArrayType::get(t->type()))
        {
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

      private:
        const TypePtr the_type;
      };
      if (!xelement) {
        throw ::std::invalid_argument("Missing element");
      }
      if (len == nullptr) {
        throw ::std::invalid_argument("Invalid array length");
      }
      return ::std::make_shared<const Impl>(xelement, idx, len, counter);
    }

    ::std::shared_ptr<const ConstrainedArrayType> ConstrainedArrayType::getBoundedArray(
        const ConstrainedType::Ptr &xelement, const Range &r)
    {
      auto len = range2length(r);
      auto idx = length2index(len);
      auto cnt = length2counter(len);
      return getArray(xelement, idx, len, cnt);
    }

    ::std::shared_ptr<const ConstrainedArrayType> ConstrainedArrayType::getBoundedArray(
        const ConstrainedType::Ptr &xelement, const ::std::optional<Range::EndPoint> &mn,
        const ::std::optional<Range::EndPoint> &mx)
    {
      return getBoundedArray(xelement, Range(mn, mx));
    }

    ::std::shared_ptr<const ConstrainedArrayType> ConstrainedArrayType::getUnboundedArray(
        const ConstrainedType::Ptr &xelement)
    {
      return getBoundedArray(xelement, Range(0, std::nullopt));
    }

    ::std::shared_ptr<const ConstrainedArrayType> ConstrainedArrayType::getFixedArray(
        const ConstrainedType::Ptr &xelement, BigInt n)
    {
      return getBoundedArray(xelement, Range(n, n));
    }

    ::std::shared_ptr<const ConstrainedArrayType> ConstrainedArrayType::getEmptyArray()
    {
      return getFixedArray(ConstrainedAnyType::get(), 0);
    }

    ConstrainedTypePtr ConstrainedArrayType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::ArrayType> &type)
    {
      if (type->element->isAnyType()) {
        return getEmptyArray();
      }
      if (!type->isConcrete()) {
        throw ::std::invalid_argument("Not a concrete array");
      }
      return getUnboundedArray(createConstraints(type->element));
    }

    ::std::shared_ptr<const ConstrainedArrayType> ConstrainedArrayType::getEmptyableArray() const
    {
      if (bounds.min() == 0) {
        return self<ConstrainedArrayType>();
      } else {
        return getBoundedArray(element, 0, bounds.max());
      }
    }

  }

}
