#include <mylang/constraints/ConstrainedBitType.h>
#include <mylang/constraints/ConstrainedBooleanType.h>
#include <mylang/constraints/ConstrainedByteType.h>
#include <mylang/constraints/ConstrainedCharType.h>
#include <mylang/constraints/ConstrainedFunctionType.h>
#include <mylang/constraints/ConstrainedIntegerType.h>
#include <mylang/constraints/ConstrainedPrimitiveType.h>
#include <mylang/constraints/ConstrainedRealType.h>
#include <mylang/constraints/ConstrainedStringType.h>
#include <mylang/constraints/ConstrainedTypeSet.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>

namespace mylang {
  namespace constraints {

    ConstrainedPrimitiveType::ConstrainedPrimitiveType()
    {
    }

    ConstrainedPrimitiveType::~ConstrainedPrimitiveType()
    {
    }

    bool ConstrainedPrimitiveType::isConstructible() const
    {
      return true;
    }

    void ConstrainedPrimitiveType::addRecursive(ConstrainedTypeSet &ts) const
    {
      ts.add(self());
    }

    ConstrainedType::Ptr ConstrainedPrimitiveType::normalize(ConstrainedTypeSet &ts) const
    {
      return ts.add(self()).first;
    }

    ::std::shared_ptr<const ConstrainedPrimitiveType> ConstrainedPrimitiveType::getBit()
    {
      return ConstrainedBitType::create();
    }

    ::std::shared_ptr<const ConstrainedPrimitiveType> ConstrainedPrimitiveType::getBoolean()
    {
      return ConstrainedBooleanType::create();
    }

    ::std::shared_ptr<const ConstrainedPrimitiveType> ConstrainedPrimitiveType::getByte()
    {
      return ConstrainedByteType::create();
    }

    ::std::shared_ptr<const ConstrainedPrimitiveType> ConstrainedPrimitiveType::getChar()
    {
      return ConstrainedCharType::create();
    }

    ::std::shared_ptr<const ConstrainedPrimitiveType> ConstrainedPrimitiveType::getReal()
    {
      return ConstrainedRealType::create();
    }

    ::std::shared_ptr<const ConstrainedPrimitiveType> ConstrainedPrimitiveType::getInteger()
    {
      return ConstrainedIntegerType::create();
    }

    ::std::shared_ptr<const ConstrainedPrimitiveType> ConstrainedPrimitiveType::getString()
    {
      return ConstrainedStringType::create();
    }

    ::std::shared_ptr<const ConstrainedPrimitiveType> ConstrainedPrimitiveType::getVoid()
    {
      return ConstrainedVoidType::create();
    }

  }
}
