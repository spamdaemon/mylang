#include <mylang/constraints/ConstrainedProcessType.h>
#include <mylang/constraints/ConstrainedTypeSet.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/defs.h>
#include <mylang/types/ProcessType.h>
#include <stdexcept>
#include <string>

namespace mylang {
  namespace constraints {

    ConstrainedProcessType::ConstrainedProcessType(Inputs in, Outputs out)
        : inputs(::std::move(in)), outputs(::std::move(out))
    {
      for (auto i : inputs) {
        if (i.second == nullptr) {
          throw ::std::invalid_argument("null value for input " + i.first);
        }
      }
      for (auto i : outputs) {
        if (i.second == nullptr) {
          throw ::std::invalid_argument("null value for output " + i.first);
        }
        if (inputs.count(i.first) > 0) {
          throw ::std::invalid_argument("duplicate name " + i.first);
        }
      }
    }

    ConstrainedProcessType::~ConstrainedProcessType()
    {
    }

    ::std::string ConstrainedProcessType::toString() const
    {
      return "process";
    }

    void ConstrainedProcessType::addRecursive(ConstrainedTypeSet &ts) const
    {
      for (auto i : inputs) {
        i.second->addRecursive(ts);
      }
      for (auto i : outputs) {
        i.second->addRecursive(ts);
      }
      ts.add(self());
    }

    ConstrainedType::Ptr ConstrainedProcessType::normalize(ConstrainedTypeSet &ts) const
    {
      Inputs in;
      Outputs out;
      for (auto i : inputs) {
        in[i.first] = i.second->normalize(ts)->self<ConstrainedInputType>();
      }
      for (auto i : outputs) {
        out[i.first] = i.second->normalize(ts)->self<ConstrainedOutputType>();
      }
      return ts.add(get(in, out)).first;
    }

    bool ConstrainedProcessType::isSameConstraint(const ConstrainedType &other) const
    {
      auto that = dynamic_cast<const ConstrainedProcessType*>(&other);
      if (!that || that->inputs.size() != inputs.size() || that->outputs.size() != outputs.size()) {
        return false;
      }
      for (auto i : that->inputs) {
        auto tmp = inputs.find(i.first);
        if (tmp == inputs.end() || !tmp->second->isSameConstraint(*i.second)) {
          return false;
        }
      }
      for (auto i : that->outputs) {
        auto tmp = outputs.find(i.first);
        if (tmp == outputs.end() || !tmp->second->isSameConstraint(*i.second)) {
          return false;
        }
      }

      return true;
    }

    bool ConstrainedProcessType::isConstructible() const
    {
      return false;
    }

    void ConstrainedProcessType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitProcessType(self<ConstrainedProcessType>());
    }

    ::std::shared_ptr<const ConstrainedProcessType> ConstrainedProcessType::get(Inputs in,
        Outputs out)
    {
      struct Impl: public ConstrainedProcessType
      {
        Impl(Inputs in, Outputs out)
            : ConstrainedProcessType(::std::move(in), ::std::move(out))
        {
          mylang::types::ProcessType::Inputs xin;
          mylang::types::ProcessType::Outputs xout;

          for (auto i : inputs) {
            xin[i.first] = i.second->type()->self<mylang::types::InputType>();
          }
          for (auto i : outputs) {
            xout[i.first] = i.second->type()->self<mylang::types::OutputType>();
          }
          the_type = mylang::types::ProcessType::get(xin, xout);
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

      private:
        TypePtr the_type;

      };
      return ::std::make_shared<const Impl>(::std::move(in), ::std::move(out));
    }

    ::std::shared_ptr<const ConstrainedProcessType> ConstrainedProcessType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::ProcessType> &t)
    {
      Inputs in;
      Outputs out;

      for (auto i : t->inputs) {
        in[i.first] = ConstrainedInputType::createUnconstrained(i.second);
      }
      for (auto i : t->outputs) {
        out[i.first] = ConstrainedOutputType::createUnconstrained(i.second);
      }

      return get(::std::move(in), ::std::move(out));
    }

  }
}
