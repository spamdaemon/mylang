#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDNAMEDTYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDNAMEDTYPE_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPE_H
#include <mylang/constraints/ConstrainedType.h>
#endif

#ifndef CLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif
#ifndef FILE_MYLANG_TYPES_H
#include <mylang/types.h>
#endif
#include <memory>
#include <functional>

namespace mylang {
  namespace constraints {

    /** A constraint representing an error */
    class ConstrainedNamedType: public ConstrainedType
    {

      /** A resolver function; returns null if the name cannot be resolved */
    public:
      typedef ::std::function<ConstrainedType::Ptr()> Resolver;

    private:
      ConstrainedNamedType(const mylang::NamePtr &name);

    public:
      ~ConstrainedNamedType();

      /**
       * Create a type constraint.
       * @param t a type
       * @return an "unconstrained" constraint
       */
    public:
      static Ptr createUnconstrained(const ::std::shared_ptr<const mylang::types::NamedType> &t);

    public:
      static ::std::shared_ptr<const ConstrainedNamedType> get(const mylang::NamePtr &name,
          Resolver resolver);

    public:
      void accept(ConstrainedTypeVisitor &v) const override;

      ConstrainedType::Ptr basetype() const override;

      bool canSafeCastFrom(const ConstrainedType &t) const override;

      bool canCastFrom(const ConstrainedType &t) const override;

      bool isSameConstraint(const ConstrainedType &other) const final override;

      ::std::string toString() const override;

      bool isConstructible() const override;

      TypePtr type() const override;

      /**
       * Create a constraint that can weakly reference
       */
    public:
      virtual ::std::shared_ptr<const ConstrainedNamedType> getRecursiveReference() const;

      /**
       * Determine if this name is resolved. If true, then resolve() will not throw an exception *
       */
    public:
      virtual bool isResolved() const = 0;

      /** Resolve this name to an actual constraint. Throws an exception if the name cannot be resolved. */
    public:
      virtual ConstrainedType::Ptr resolve() const = 0;
    };
  }
}
#endif
