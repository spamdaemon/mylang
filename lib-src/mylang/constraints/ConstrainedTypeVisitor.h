#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPEVISITOR_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPEVISITOR_H

#include <mylang/constraints/constraints.h>

namespace mylang {
  namespace constraints {

    /** A constraint visitor */
    class ConstrainedTypeVisitor
    {

      /** The constructor */
    protected:
      ConstrainedTypeVisitor();

      /** Destructor */
    public:
      virtual ~ConstrainedTypeVisitor() = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitAny(const ::std::shared_ptr<const ConstrainedAnyType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitBit(const ::std::shared_ptr<const ConstrainedBitType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitBoolean(
          const ::std::shared_ptr<const ConstrainedBooleanType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitByte(const ::std::shared_ptr<const ConstrainedByteType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitChar(const ::std::shared_ptr<const ConstrainedCharType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitReal(const ::std::shared_ptr<const ConstrainedRealType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitInteger(
          const ::std::shared_ptr<const ConstrainedIntegerType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitString(
          const ::std::shared_ptr<const ConstrainedStringType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitVoid(const ::std::shared_ptr<const ConstrainedVoidType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitGeneric(
          const ::std::shared_ptr<const ConstrainedGenericType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitArray(const ::std::shared_ptr<const ConstrainedArrayType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitUnion(const ::std::shared_ptr<const ConstrainedUnionType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitStruct(
          const ::std::shared_ptr<const ConstrainedStructType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitTuple(const ::std::shared_ptr<const ConstrainedTupleType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitFunction(
          const ::std::shared_ptr<const ConstrainedFunctionType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitOpt(const ::std::shared_ptr<const ConstrainedOptType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitMutable(
          const ::std::shared_ptr<const ConstrainedMutableType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitNamedType(
          const ::std::shared_ptr<const ConstrainedNamedType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitInputType(
          const ::std::shared_ptr<const ConstrainedInputType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitOutputType(
          const ::std::shared_ptr<const ConstrainedOutputType> &constraint) = 0;

      /**
       * Visit the specified constraint
       * @param p a primitive constraint
       */
    public:
      virtual void visitProcessType(
          const ::std::shared_ptr<const ConstrainedProcessType> &constraint) = 0;

    };
  }
}
#endif 
