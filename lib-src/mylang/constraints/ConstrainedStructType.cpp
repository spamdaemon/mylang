#include <mylang/constraints/ConstrainedFunctionType.h>
#include <mylang/constraints/ConstrainedStructType.h>
#include <mylang/constraints/ConstrainedTupleType.h>
#include <mylang/constraints/ConstrainedTypeSet.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/types.h>

namespace mylang {
  namespace constraints {
    ConstrainedStructType::Member::Member(const ::std::string &xname,
        const ConstrainedType::Ptr &xtype)
        : name(xname), constraint(xtype)
    {

    }

    ConstrainedStructType::ConstrainedStructType(const ::std::vector<Member> &xmembers)
        : members(xmembers)
    {
      for (auto sf : members) {
        if (sf.name.empty()) {
          throw ::std::invalid_argument("Missing member name");
        }
        if (!sf.constraint) {
          throw ::std::invalid_argument("Missing member constraint");
        }
        if (_indexedMembers.count(sf.name) != 0) {
          throw ::std::invalid_argument("Duplicate member " + sf.name);
        }
        _indexedMembers.insert(::std::make_pair(sf.name, sf));
      }
    }

    ConstrainedStructType::~ConstrainedStructType()
    {
    }

    ::std::shared_ptr<const ConstrainedTupleType> ConstrainedStructType::getPeerTuple() const
    {
      ::std::vector<ConstrainedTypePtr> m;
      for (auto member : members) {
        m.push_back(member.constraint);
      }
      return ConstrainedTupleType::get(m);
    }

    bool ConstrainedStructType::isConstructible() const
    {
      return true;
    }

    void ConstrainedStructType::addRecursive(ConstrainedTypeSet &ts) const
    {
      if (ts.add(self()).second) {
        for (auto m : members) {
          m.constraint->addRecursive(ts);
        }
      }
    }

    ConstrainedType::Ptr ConstrainedStructType::normalize(ConstrainedTypeSet &ts) const
    {
      ::std::vector<Member> tmp;
      for (auto m : members) {
        tmp.push_back(Member(m.name, m.constraint->normalize(ts)));
      }
      return ts.add(get(tmp)).first;
    }

    void ConstrainedStructType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitStruct(::std::dynamic_pointer_cast<const ConstrainedStructType>(self()));
    }

    ConstrainedType::Ptr ConstrainedStructType::intersectWith(const Ptr &t) const
    {
      ConstrainedType::Ptr res = ConstrainedType::unionWith(t);
      if (res) {
        return res;
      }
      auto that = ::std::dynamic_pointer_cast<const ConstrainedStructType>(t);
      if (!that) {
        return nullptr;
      }
      if (that->members.size() != members.size()) {
        return nullptr;
      }

      ::std::vector<Member> typeIntersection;
      for (size_t i = 0, sz = members.size(); i < sz; ++i) {
        const Member &thatMember = that->members.at(i);
        const Member &thisMember = members.at(i);

        if (thatMember.name != thisMember.name) {
          return nullptr;
        }
        auto tmp = thatMember.constraint->intersectWith(thisMember.constraint);
        if (!tmp) {
          return nullptr;
        }
        typeIntersection.push_back(Member(thatMember.name, tmp));
      }

      return get(typeIntersection);
    }

    ConstrainedType::Ptr ConstrainedStructType::unionWith(const Ptr &t) const
    {
      ConstrainedType::Ptr res = ConstrainedType::unionWith(t);
      if (res) {
        return res;
      }
      auto that = ::std::dynamic_pointer_cast<const ConstrainedStructType>(t);
      if (!that) {
        return nullptr;
      }
      if (that->members.size() != members.size()) {
        return nullptr;
      }

      ::std::vector<Member> typeUnion;
      for (size_t i = 0, sz = members.size(); i < sz; ++i) {
        const Member &thatMember = that->members.at(i);
        const Member &thisMember = members.at(i);

        if (thatMember.name != thisMember.name) {
          return nullptr;
        }
        auto tmp = thatMember.constraint->unionWith(thisMember.constraint);
        if (!tmp) {
          return nullptr;
        }
        typeUnion.push_back(Member(thatMember.name, tmp));
      }

      return get(typeUnion);
    }

    bool ConstrainedStructType::canSafeCastFrom(const ConstrainedType &from) const
    {
      auto thatStruct = dynamic_cast<const ConstrainedStructType*>(&from);
      if (!thatStruct) {
        return ConstrainedType::canSafeCastFrom(from);
      }
      if (members.size() != thatStruct->members.size()) {
        return false;
      }
      for (size_t i = 0; i < members.size(); ++i) {
        if (members[i].name != thatStruct->members[i].name) {
          return false;
        }
        if (!members[i].constraint->canSafeCastFrom(*thatStruct->members[i].constraint)) {
          return false;
        }
      }
      return true;
    }

    bool ConstrainedStructType::canCastFrom(const ConstrainedType &from) const
    {
      auto thatStruct = dynamic_cast<const ConstrainedStructType*>(&from);
      if (!thatStruct) {
        return ConstrainedType::canSafeCastFrom(from);
      }
      if (members.size() != thatStruct->members.size()) {
        return false;
      }
      for (size_t i = 0; i < members.size(); ++i) {
        if (members[i].name != thatStruct->members[i].name) {
          return false;
        }
        if (!members[i].constraint->canCastFrom(*thatStruct->members[i].constraint)) {
          return false;
        }
      }
      return true;
    }

    bool ConstrainedStructType::isSameConstraint(const ConstrainedType &other) const
    {
      auto t = dynamic_cast<const ConstrainedStructType*>(&other);
      if (!t || t->members.size() != members.size()) {
        return false;
      }
      for (size_t i = 0; i < members.size(); ++i) {
        if (members[i].name != t->members[i].name) {
          return false;
        }
        if (!members[i].constraint->isSameConstraint(*t->members[i].constraint)) {
          return false;
        }
      }

      return true;
    }

    ::std::string ConstrainedStructType::toString() const
    {
      ::std::string str;
      str += "struct {";
      if (!members.empty()) {
        str += '\n';
      }
      for (auto sf : members) {
        str += sf.name;
        str += ':';
        str += sf.constraint->toString();
        str += ";\n";
      }
      str += "}";
      return str;
    }

    ::std::shared_ptr<const ConstrainedStructType> ConstrainedStructType::get(
        const ::std::vector<Member> &members)
    {
      struct Impl: public ConstrainedStructType
      {
        Impl(const ::std::vector<Member> &t)
            : ConstrainedStructType(t)
        {
          ::std::vector<mylang::types::StructType::Member> M;
          for (auto m : members) {
            M.push_back(mylang::types::StructType::Member(m.name, m.constraint->type()));
          }
          the_type = mylang::types::StructType::get(M);
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

      private:
        TypePtr the_type;
      };
      return ::std::make_shared<Impl>(members);
    }

    ConstrainedType::Ptr ConstrainedStructType::getMemberType(const ::std::string &field) const
    {
      auto i = _indexedMembers.find(field);
      if (i == _indexedMembers.end()) {
        return nullptr;
      }
      return i->second.constraint;
    }

    ConstrainedTypePtr ConstrainedStructType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::StructType> &t)
    {
      ::std::vector<Member> members;
      for (auto m : t->members) {
        members.push_back(Member(m.name, createConstraints(m.type)));
      }
      return get(members);
    }

  }
}
