#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDPRIMITIVETYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDPRIMITIVETYPE_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPE_H
#include <mylang/constraints/ConstrainedType.h>
#endif
#include <memory>

namespace mylang {
  namespace constraints {

    /** The baseclass for all types */
    class ConstrainedPrimitiveType: public ConstrainedType
    {

    protected:
      ConstrainedPrimitiveType();

    public:
      ~ConstrainedPrimitiveType();

      bool isConstructible() const override;

      Ptr normalize(ConstrainedTypeSet &ts) const override;

      void addRecursive(ConstrainedTypeSet &ts) const override;

      /**
       * Get the bit constraint
       * @return a primitive constraint for a bit
       */
    public:
      static ::std::shared_ptr<const ConstrainedPrimitiveType> getBit();

      /**
       * Get the bit constraint
       * @return a primitive constraint for a boolean
       */
    public:
      static ::std::shared_ptr<const ConstrainedPrimitiveType> getBoolean();

      /**
       * Get the bit constraint
       * @return a primitive constraint for a char
       */
    public:
      static ::std::shared_ptr<const ConstrainedPrimitiveType> getByte();

      /**
       * Get the bit constraint
       * @return a primitive constraint for a char
       */
    public:
      static ::std::shared_ptr<const ConstrainedPrimitiveType> getChar();

      /**
       * Get the bit constraint
       * @return a primitive constraint for a real
       */
    public:
      static ::std::shared_ptr<const ConstrainedPrimitiveType> getReal();

      /**
       * Get the bit constraint
       * @return a primitive constraint for a integer
       */
    public:
      static ::std::shared_ptr<const ConstrainedPrimitiveType> getInteger();

      /**
       * Get the bit constraint
       * @return a primitive constraint for a string
       */
    public:
      static ::std::shared_ptr<const ConstrainedPrimitiveType> getString();

      /**
       * Get the bit constraint
       * @return a primitive constraint for a string
       */
    public:
      static ::std::shared_ptr<const ConstrainedPrimitiveType> getVoid();
    };
  }
}
#endif
