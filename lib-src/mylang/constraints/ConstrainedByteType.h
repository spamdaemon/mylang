#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDBYTETYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDBYTETYPE_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDPRIMITIVETYPE_H
#include <mylang/constraints/ConstrainedPrimitiveType.h>
#endif
#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDARRAYTYPE_H
#include <mylang/constraints/ConstrainedArrayType.h>
#endif
#ifndef FILE_MYLANG_TYPES_H
#include <mylang/types.h>
#endif

namespace mylang {
  namespace constraints {

    /** The baseclass for all types */
    class ConstrainedByteType: public ConstrainedPrimitiveType
    {

    private:
      ConstrainedByteType();

    public:
      ~ConstrainedByteType();

      /**
       * Create a type constraint.
       * @param t a type
       * @return an "unconstrained" constraint
       */
    public:
      static Ptr createUnconstrained(const ::std::shared_ptr<const mylang::types::ByteType> &t);

      /**
       * Get the bit constraint
       * @return a primitive constraint for a INTEGER
       */
    public:
      static ::std::shared_ptr<const ConstrainedByteType> create();

      /**
       * Get the the corresponding bit array. The corresponding bit
       * array is afixed array of size 8.
       */
    public:
      static ::std::shared_ptr<const ConstrainedArrayType> getPeerBitArray();

    public:
      void accept(ConstrainedTypeVisitor &v) const override;

      bool isSameConstraint(const ConstrainedType &other) const final override;

      ::std::string toString() const final override;
    };
  }
}
#endif
