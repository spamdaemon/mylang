#include <mylang/constraints/ConstraintError.h>

namespace mylang {
  namespace constraints {

    ConstraintError::ConstraintError(const ::std::string &xname, const Constraint::Ptr &c,
        const ::std::string &msg)
        : ConstraintError(Constraints(xname, c), msg)
    {
    }

    ConstraintError::ConstraintError(const Constraints &c, const ::std::string &msg)
        : ::std::runtime_error("constraint error: " + msg), constraints(c), error(msg)
    {
    }

    ConstraintError::~ConstraintError()
    {
    }
  }
}
