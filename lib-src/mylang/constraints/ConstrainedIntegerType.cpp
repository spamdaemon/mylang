#include <mylang/constraints/ConstrainedIntegerType.h>
#include <mylang/constraints/ConstrainedFunctionType.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/types.h>

namespace mylang {
  namespace constraints {

    ConstrainedIntegerType::ConstrainedIntegerType(const Range &r)
        : range(r)
    {
    }

    ConstrainedIntegerType::~ConstrainedIntegerType()
    {
    }

    bool ConstrainedIntegerType::isUnsigned() const
    {
      return range.min().sign() >= 0;
    }

    ::std::shared_ptr<const ConstrainedIntegerType> ConstrainedIntegerType::incrementMax() const
    {
      auto max = range.max().increment();
      const auto &min = range.min();
      return create(Range(min, max));
    }

    ConstrainedType::Ptr ConstrainedIntegerType::unionWith(const Ptr &t) const
    {
      ConstrainedType::Ptr res = ConstrainedPrimitiveType::unionWith(t);
      if (res) {
        return res;
      }
      auto that = ::std::dynamic_pointer_cast<const ConstrainedIntegerType>(t);
      if (that) {
        res = create(range.unionWith(that->range));
      }
      return res;
    }

    ConstrainedType::Ptr ConstrainedIntegerType::intersectWith(const Ptr &t) const
    {
      ConstrainedType::Ptr res = ConstrainedPrimitiveType::intersectWith(t);
      if (res) {
        return res;
      }

      auto that = ::std::dynamic_pointer_cast<const ConstrainedIntegerType>(t);
      if (that) {
        auto intersection = range.intersectWith(that->range);
        if (intersection.has_value()) {
          res = create(intersection.value());
        }
      }
      return res;
    }

    Constraints ConstrainedIntegerType::constraints() const
    {
      auto t = self();
      Constraints c;
      if (range.min().isFinite()) {
        c.put("min",
            LiteralConstraint::get(
                mylang::expressions::Constant::get(t->type(), range.min().toString())));
      }
      if (range.max().isFinite()) {
        c.put("max",
            LiteralConstraint::get(
                mylang::expressions::Constant::get(t->type(), range.max().toString())));
      }
      return c;
    }

    ConstrainedType::Ptr ConstrainedIntegerType::constrain(const Constraints &newConstraints) const
    {
      auto min = range.min();
      auto max = range.max();

      auto unknown = newConstraints.findUnknownConstraints( { "min", "max" });
      if (!unknown.empty()) {
        throw ConstraintError(newConstraints, "unexpected constraint: " + unknown.at(0));
      }

      if (newConstraints.hasConstraint("min")) {
        auto cc = newConstraints.get("min");
        auto c = ::std::dynamic_pointer_cast<const LiteralConstraint>(cc);
        if (c) {
          min = c->expression->getSignedInteger();
        } else {
          throw ConstraintError("min", cc, "not a constant expression");
        }
      } else {
        min = Integer::MINUS_INFINITY();
      }
      if (newConstraints.hasConstraint("max")) {
        auto cc = newConstraints.get("max");
        if (cc) {
          auto c = ::std::dynamic_pointer_cast<const LiteralConstraint>(cc);
          if (c) {
            max = c->expression->getSignedInteger();
          } else {
            throw ConstraintError("max", cc, "not a constant expression");
          }
        } else {
          max = Integer::PLUS_INFINITY();
        }
      }

      Range b(min, max);
      if (!range.contains(b)) {
        throw ConstraintError(newConstraints, "incompatible range restriction");
      }
      if (b == range) {
        return self();
      }

      return create(b);
    }

    void ConstrainedIntegerType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitInteger(::std::dynamic_pointer_cast<const ConstrainedIntegerType>(self()));
    }

    bool ConstrainedIntegerType::isSameConstraint(const ConstrainedType &other) const
    {
      if (this == &other) {
        return true;
      }

      const ConstrainedIntegerType *that = dynamic_cast<const ConstrainedIntegerType*>(&other);
      if (!that) {
        return false;
      }

      return this->range == that->range;
    }

    bool ConstrainedIntegerType::canSafeCastFrom(const ConstrainedType &t) const
    {
      auto that = dynamic_cast<const ConstrainedIntegerType*>(&t);
      if (that) {
        return range.contains(that->range);
      }
      return ConstrainedPrimitiveType::canSafeCastFrom(t);
    }

    bool ConstrainedIntegerType::canCastFrom(const ConstrainedType &t) const
    {
      auto that = dynamic_cast<const ConstrainedIntegerType*>(&t);
      if (that) {
        return range.intersectWith(that->range).has_value();
      }

      return ConstrainedPrimitiveType::canCastFrom(t);
    }

    ::std::shared_ptr<const ConstrainedIntegerType> ConstrainedIntegerType::negate() const
    {
      if (range.is0()) {
        return self<ConstrainedIntegerType>();
      } else {
        return create(range.negate());
      }
    }

    ::std::shared_ptr<const ConstrainedIntegerType> ConstrainedIntegerType::getSignedInt() const
    {
      if (range.isInfinite()) {
        // unbounded integers cannot be converted
        return nullptr;
      }

      BigInt M = BigInt::signedValueOf(256);
      for (int i = 0; i < 4; ++i, M = M * M) {
        const Range r(-M, M - 1);
        if (r == range) {
          return self<ConstrainedIntegerType>();
        }
        if (r.contains(range)) {
          return create(r);
        }
      }
      return nullptr;
    }

    ::std::shared_ptr<const ConstrainedIntegerType> ConstrainedIntegerType::getUnsignedInt() const
    {
      if (range.isInfinite()) {
        // unbounded integers cannot be converted
        return nullptr;
      }

      BigInt M = BigInt::signedValueOf(256);
      for (int i = 0; i < 4; ++i, M = M * M) {
        const Range r(0, ::std::make_optional(M));
        if (range.min().sign() >= 0) {
          if (r == range) {
            return self<ConstrainedIntegerType>();
          }
          if (r.contains(range)) {
            return create(r);
          }
        } else {
          const Range signed_range(-M, M - 1);
          if (signed_range == range || signed_range.contains(range)) {
            return create(r);
          }
        }
      }
      return nullptr;
    }

    ::std::string ConstrainedIntegerType::toString() const
    {
      ::std::string res;
      res += "integer";
      if (range.min().isFinite() || range.max().isFinite()) {
        res += '{';
        const char *sep = "";
        if (range.min().isFinite()) {
          res += sep;
          res += "min:";
          res += range.min().toString();
          sep = ";";
        }
        if (range.max().isFinite()) {
          res += sep;
          res += "max:";
          res += range.max().toString();
        }
        res += '}';
      }
      return res;
    }

    ::std::shared_ptr<const ConstrainedIntegerType> ConstrainedIntegerType::create()
    {
      return create(Range());
    }

    ::std::shared_ptr<const ConstrainedIntegerType> ConstrainedIntegerType::create(const BigInt &c)
    {
      return create(Range(c, c));
    }

    ::std::shared_ptr<const ConstrainedIntegerType> ConstrainedIntegerType::create(const Range &rng)
    {
      struct Impl: public ConstrainedIntegerType
      {
        Impl(const Range &xrng)
            : ConstrainedIntegerType(xrng), the_type(mylang::types::PrimitiveType::getInteger())
        {
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

      private:
        const TypePtr the_type;
      };

      return ::std::make_shared<Impl>(rng);
    }

    ConstrainedTypePtr ConstrainedIntegerType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::IntegerType>&)
    {
      return create();
    }

  }
}
