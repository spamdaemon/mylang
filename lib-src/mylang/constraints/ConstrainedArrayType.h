#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDARRAYTYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDARRAYTYPE_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPE_H
#include <mylang/constraints/ConstrainedType.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDINTEGERTYPE_H
#include <mylang/constraints/ConstrainedIntegerType.h>
#endif

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef FILE_MYLANG_TYPES_H
#include <mylang/types.h>
#endif

#include <memory>
#include <iostream>

namespace mylang {
  namespace constraints {

    /** A constraint representing an error */
    class ConstrainedArrayType: public ConstrainedType
    {
      /** The range type */
    public:
      typedef Interval Range;

      /** An integer type */

    private:
      ConstrainedArrayType(const Ptr &xelement,
          const ::std::shared_ptr<const ConstrainedIntegerType> &idx,
          const ::std::shared_ptr<const ConstrainedIntegerType> &len,
          const ::std::shared_ptr<const ConstrainedIntegerType> &counter);

    public:
      ~ConstrainedArrayType();

      /**
       * Create a type constraint.
       * @param t a type
       * @return an "unconstrained" constraint
       */
    public:
      static Ptr createUnconstrained(const ::std::shared_ptr<const mylang::types::ArrayType> &t);

      /**
       * Follow the element constraint until an element is found that is not a optional
       * @return an element constraint (maybe nullptr!)
       */
    public:
      ConstrainedType::Ptr flatten() const;

      /**
       * Turn this array into one whose min is 0
       */
    public:
      ::std::shared_ptr<const ConstrainedArrayType> getEmptyableArray() const;

    public:
      static ::std::shared_ptr<const ConstrainedArrayType> getUnboundedArray(
          const ConstrainedType::Ptr &xelement);

      static ::std::shared_ptr<const ConstrainedArrayType> getEmptyArray();

      static ::std::shared_ptr<const ConstrainedArrayType> getFixedArray(
          const ConstrainedType::Ptr &xelement, BigInt n);

      static ::std::shared_ptr<const ConstrainedArrayType> getBoundedArray(
          const ConstrainedType::Ptr &xelement, const Range &r);

      static ::std::shared_ptr<const ConstrainedArrayType> getBoundedArray(
          const ConstrainedType::Ptr &xelement, const ::std::optional<Range::EndPoint> &mn,
          const ::std::optional<Range::EndPoint> &mx);

      bool canSafeCastFrom(const ConstrainedType &t) const override;

      bool canCastFrom(const ConstrainedType &t) const override;

      Ptr constrain(const Constraints &constraints) const override;

      Constraints constraints() const override;

      Ptr normalize(ConstrainedTypeSet &ts) const override;

      void addRecursive(ConstrainedTypeSet &ts) const override;

      void accept(ConstrainedTypeVisitor &v) const override;

      ::std::string toString() const override;

      bool isSameConstraint(const ConstrainedType &other) const override;

      bool isConstructible() const override;

      Ptr intersectWith(const Ptr &t) const override;

      Ptr unionWith(const Ptr &t) const override;

      /**
       * Determine if this is an unbounded array
       * @return true if this is array constraint is unbounded.
       */
    public:
      bool isUnbounded() const;

      /**
       * Determine if this is an bounded array
       * @return true if this is array constraint is bounded.
       */
    public:
      bool isBounded() const;

      /**
       * Determine the dimension of this array. If the element is also an array
       * then the dimension is increased by 1.
       */
    public:
      size_t dimensionality() const;

      /**
       * Get the leaf element.
       */
    public:
      ConstrainedType::Ptr leafElement() const;

      /**
       * Create a new instanceof of this array, but change the return constraint
       * @param element the new element constraint
       */
    public:
      ::std::shared_ptr<const ConstrainedArrayType> copy(
          const ConstrainedType::Ptr &newElement) const;

    private:
      static ::std::shared_ptr<const ConstrainedArrayType> getArray(
          const ConstrainedType::Ptr &xelement,
          const ::std::shared_ptr<const ConstrainedIntegerType> &idx,
          const ::std::shared_ptr<const ConstrainedIntegerType> &len,
          const ::std::shared_ptr<const ConstrainedIntegerType> &counter);

      /** The element constraint; it may be null if not known */
    public:
      const ConstrainedType::Ptr element;

      /** The index constraint (0..length-1); if the array maximum bound of the array is 0, then
       * the index is a nullptr, since there no valid index positions */
    public:
      const ::std::shared_ptr<const ConstrainedIntegerType> index;

      /** The constraint on the length (the possible value of the length) */
    public:
      const ::std::shared_ptr<const ConstrainedIntegerType> length;

      /** A counter is the value (0..length) */
    public:
      const ::std::shared_ptr<const ConstrainedIntegerType> counter;

      /** The maximum bound for this array; The lower bound will always be set.*/
    public:
      const Range bounds;
    };
  }
}
#endif
