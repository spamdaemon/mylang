#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDVOIDTYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDVOIDTYPE_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDPRIMITIVETYPE_H
#include <mylang/constraints/ConstrainedPrimitiveType.h>
#endif
#ifndef FILE_MYLANG_TYPES_H
#include <mylang/types.h>
#endif
#include <memory>

namespace mylang {
  namespace constraints {

    /** A constraint representing an error */
    class ConstrainedVoidType: public ConstrainedPrimitiveType
    {

    private:
      ConstrainedVoidType();

    public:
      ~ConstrainedVoidType();

      /**
       * Create a type constraint.
       * @param t a type
       * @return an "unconstrained" constraint
       */
    public:
      static Ptr createUnconstrained(const ::std::shared_ptr<const mylang::types::VoidType> &t);

      /** Two primitives are the same if their constraint ids are the same  */
    public:
      void accept(ConstrainedTypeVisitor &v) const override;

      bool isSameConstraint(const ConstrainedType &other) const final override;

      ::std::string toString() const final override;

    public:
      static ::std::shared_ptr<const ConstrainedVoidType> create();
    };
  }
}
#endif
