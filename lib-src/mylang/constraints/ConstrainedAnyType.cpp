#include <mylang/constraints/ConstrainedAnyType.h>
#include <mylang/constraints/ConstrainedTypeSet.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>

namespace mylang {
  namespace constraints {

    ConstrainedAnyType::ConstrainedAnyType()
    {
    }

    ConstrainedAnyType::~ConstrainedAnyType()
    {
    }

    void ConstrainedAnyType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitAny(::std::dynamic_pointer_cast<const ConstrainedAnyType>(self()));
    }

    bool ConstrainedAnyType::isSameConstraint(const ConstrainedType &other) const
    {
      return dynamic_cast<const ConstrainedAnyType*>(&other) != nullptr;
    }

    ::std::string ConstrainedAnyType::toString() const
    {
      return "*";
    }

    bool ConstrainedAnyType::isConstructible() const
    {
      return false;
    }

    void ConstrainedAnyType::addRecursive(ConstrainedTypeSet &ts) const
    {
      ts.add(self());
    }

    ConstrainedType::Ptr ConstrainedAnyType::normalize(ConstrainedTypeSet &ts) const
    {
      return ts.add(self()).first;
    }

    ::std::shared_ptr<const ConstrainedAnyType> ConstrainedAnyType::get()
    {
      struct Impl: public ConstrainedAnyType
      {
        Impl()
            : the_type(mylang::types::AnyType::get())
        {
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

        TypePtr the_type;
      };
      return ::std::make_shared<Impl>();
    }

    ConstrainedTypePtr ConstrainedAnyType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::AnyType>&)
    {
      return get();
    }

  }
}
