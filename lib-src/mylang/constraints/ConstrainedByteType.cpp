#include <mylang/constraints/ConstrainedByteType.h>
#include <mylang/constraints/ConstrainedBitType.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/types.h>

namespace mylang {
  namespace constraints {

    ConstrainedByteType::ConstrainedByteType()
    {
    }

    ConstrainedByteType::~ConstrainedByteType()
    {
    }

    ::std::shared_ptr<const ConstrainedArrayType> ConstrainedByteType::getPeerBitArray()
    {
      return ConstrainedArrayType::getFixedArray(ConstrainedBitType::getBit(), 8);
    }

    void ConstrainedByteType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitByte(::std::dynamic_pointer_cast<const ConstrainedByteType>(self()));
    }

    bool ConstrainedByteType::isSameConstraint(const ConstrainedType &other) const
    {
      return dynamic_cast<const ConstrainedByteType*>(&other) != nullptr;
    }

    ::std::string ConstrainedByteType::toString() const
    {
      return "byte";
    }

    ::std::shared_ptr<const ConstrainedByteType> ConstrainedByteType::create()
    {
      struct Impl: public ConstrainedByteType
      {
        Impl()
            : the_type(mylang::types::ByteType::create())
        {
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

      private:
        const TypePtr the_type;
      };

      return ::std::make_shared<Impl>();
    }

    ConstrainedTypePtr ConstrainedByteType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::ByteType>&)
    {
      return create();
    }

  }
}
