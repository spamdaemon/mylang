#include <mylang/constraints/DynamicConstraint.h>

namespace mylang {
  namespace constraints {

    DynamicConstraint::DynamicConstraint(const mylang::expressions::Expression::Ptr &expr)
        : expression(expr)
    {
    }

    DynamicConstraint::~DynamicConstraint()
    {
    }

    bool DynamicConstraint::isSameConstraint(const Constraint &other) const
    {
      const DynamicConstraint *that = dynamic_cast<const DynamicConstraint*>(&other);
      if (!that) {
        return false;
      }
      if (that->expression == this->expression) {
        return true;
      }
      return false;
    }

    ::std::shared_ptr<const DynamicConstraint> DynamicConstraint::get(
        const mylang::expressions::Expression::Ptr &xe)
    {
      struct Impl: public DynamicConstraint
      {
        Impl(const mylang::expressions::Expression::Ptr &e)
            : DynamicConstraint(e)
        {
        }

        ~Impl()
        {
        }
      };
      return ::std::make_shared<Impl>(xe);
    }
  }
}
