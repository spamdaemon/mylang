#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDBITTYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDBITTYPE_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDPRIMITIVETYPE_H
#include <mylang/constraints/ConstrainedPrimitiveType.h>
#endif
#ifndef FILE_MYLANG_TYPES_H
#include <mylang/types.h>
#endif

namespace mylang {
  namespace constraints {

    /** The baseclass for all types */
    class ConstrainedBitType: public ConstrainedPrimitiveType
    {

    private:
      ConstrainedBitType();

    public:
      ~ConstrainedBitType();

      /**
       * Create a type constraint.
       * @param t a type
       * @return an "unconstrained" constraint
       */
    public:
      static Ptr createUnconstrained(const ::std::shared_ptr<const mylang::types::BitType> &t);

      /**
       * Get the bit constraint
       * @return a primitive constraint for a INTEGER
       */
    public:
      static ::std::shared_ptr<const ConstrainedBitType> create();

    public:
      void accept(ConstrainedTypeVisitor &v) const override;

      bool isSameConstraint(const ConstrainedType &other) const final override;

      ::std::string toString() const final override;
    };
  }
}
#endif
