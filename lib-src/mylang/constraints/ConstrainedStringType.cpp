#include <mylang/constraints/ConstrainedFunctionType.h>
#include <mylang/constraints/ConstrainedStringType.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/constraints/ConstrainedTypeSet.h>
#include <mylang/types.h>

namespace mylang {
  namespace constraints {

    ConstrainedStringType::ConstrainedStringType(
        const ::std::shared_ptr<const ConstrainedIntegerType> &idx,
        const ::std::shared_ptr<const ConstrainedIntegerType> &len)
        : index(idx), length(len)
    {
    }

    ConstrainedStringType::~ConstrainedStringType()
    {
    }

    ConstrainedType::Ptr ConstrainedStringType::normalize(ConstrainedTypeSet &ts) const
    {
      auto idx = index->normalize(ts)->self<ConstrainedIntegerType>();
      auto len = length->normalize(ts)->self<ConstrainedIntegerType>();
      return ts.add(createString(idx, len)).first;
    }

    ::std::shared_ptr<const ConstrainedStringType> ConstrainedStringType::createString(
        const ::std::shared_ptr<const ConstrainedIntegerType> &idx,
        const ::std::shared_ptr<const ConstrainedIntegerType> &len)
    {
      struct Impl: public ConstrainedStringType
      {
        Impl(const ::std::shared_ptr<const ConstrainedIntegerType> &idx,
            const ::std::shared_ptr<const ConstrainedIntegerType> &len)
            : ConstrainedStringType(idx, len), the_type(mylang::types::StringType::create())
        {
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

      private:
        const TypePtr the_type;
      };

      return ::std::make_shared<Impl>(idx, len);

    }

    void ConstrainedStringType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitString(::std::dynamic_pointer_cast<const ConstrainedStringType>(self()));
    }

    bool ConstrainedStringType::isSameConstraint(const ConstrainedType &other) const
    {
      auto that = dynamic_cast<const ConstrainedStringType*>(&other);
      return that && length->isSameConstraint(*that->length);
    }

    ::std::string ConstrainedStringType::toString() const
    {
      return "string";
    }

    ::std::shared_ptr<const ConstrainedStringType> ConstrainedStringType::create()
    {
      auto idx = ConstrainedIntegerType::create(ConstrainedIntegerType::Range(0, std::nullopt));
      auto len = idx;
      return createString(idx, len);
    }

    ConstrainedTypePtr ConstrainedStringType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::StringType>&)
    {
      return create();
    }

  }
}
