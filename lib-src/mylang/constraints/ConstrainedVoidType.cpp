#include <mylang/constraints/ConstrainedTypeSet.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/constraints/ConstrainedVoidType.h>
#include <mylang/types.h>

namespace mylang {
  namespace constraints {

    ConstrainedVoidType::ConstrainedVoidType()
    {
    }

    ConstrainedVoidType::~ConstrainedVoidType()
    {
    }

    bool ConstrainedVoidType::isSameConstraint(const ConstrainedType &other) const
    {
      return dynamic_cast<const ConstrainedVoidType*>(&other) != nullptr;
    }

    ::std::string ConstrainedVoidType::toString() const
    {
      return "void";
    }

    void ConstrainedVoidType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitVoid(::std::dynamic_pointer_cast<const ConstrainedVoidType>(self()));
    }

    ::std::shared_ptr<const ConstrainedVoidType> ConstrainedVoidType::create()
    {
      struct Impl: public ConstrainedVoidType
      {
        Impl()
            : ConstrainedVoidType(), the_type(mylang::types::VoidType::create())
        {
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

      private:
        const TypePtr the_type;
      };
      return ::std::make_shared<Impl>();
    }

    ConstrainedTypePtr ConstrainedVoidType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::VoidType>&)
    {
      return create();
    }

  }
}
