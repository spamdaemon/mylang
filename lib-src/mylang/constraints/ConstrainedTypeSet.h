#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPESET_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPESET_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPE_H
#include <mylang/constraints/ConstrainedType.h>
#endif

#ifndef CLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif
#include <vector>
#include <functional>

namespace mylang {
  namespace constraints {

    class ConstrainedTypeSet
    {
      /** The pair */

      /** Constructor */
    public:
      ConstrainedTypeSet();

      /** Destructor */
    public:
      virtual ~ConstrainedTypeSet();

      /**
       * Determine if the specified constraint already exists.
       * @param constraint
       * @return the constraint or nullptr if not found
       */
    public:
      ConstrainedType::Ptr get(const ConstrainedType::Ptr &constraint) const;

      /**
       * Add a new constraint to this set. If the constraint already exists, then
       * the existing constraint is returned.
       * @param constraint a constraint
       * @return the constraint or the constraint that already exists
       */
    public:
      ::std::pair<ConstrainedType::Ptr, bool> add(const ConstrainedType::Ptr &constraint);

      /**
       * Get each vector.
       * @return a vector types
       */
    public:
      inline const ::std::vector<ConstrainedType::Ptr>& types() const
      {
        return *_types;
      }

      /**
       * Filter the types in this set.
       * @param f a filter function
       * @return a vector types
       */
    public:
      template<class T>
      ::std::vector<::std::shared_ptr<const T>> filter(
          ::std::function<::std::shared_ptr<const T>(const ConstrainedType::Ptr&)> f) const
      {
        ::std::vector<::std::shared_ptr<const T>> res;
        for (auto t : *_types) {
          auto tmp = f(t);
          if (tmp) {
            res.push_back(tmp);
          }
        }
        return res;
      }

      /**
       * Filter the types by typename.
       */
      template<class T>
      ::std::vector<::std::shared_ptr<const T>> filter() const
      {
        ::std::vector<::std::shared_ptr<const T>> res;
        for (auto t : *_types) {
          auto tmp = ::std::dynamic_pointer_cast<const T>(t);
          if (tmp) {
            res.push_back(tmp);
          }
        }
        return res;
      }

      /** The list of types */
    private:
      ::std::shared_ptr<::std::vector<ConstrainedType::Ptr> > _types;
    };
  }
}
#endif
