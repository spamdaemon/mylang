#include <mylang/constraints/LiteralConstraint.h>

namespace mylang {
  namespace constraints {

    LiteralConstraint::LiteralConstraint(const mylang::expressions::Constant::Ptr &expr)
        : expression(expr)
    {
      if (!expression) {
        throw ::std::invalid_argument("Missing constant");
      }
    }

    LiteralConstraint::~LiteralConstraint()
    {
    }

    bool LiteralConstraint::isSameConstraint(const Constraint &other) const
    {
      const LiteralConstraint *that = dynamic_cast<const LiteralConstraint*>(&other);
      if (!that) {
        return false;
      }
      if (that->expression == this->expression) {
        return true;
      }
      return this->expression && that->expression
          && this->expression->isSameConstant(*that->expression);
    }

    ::std::shared_ptr<const LiteralConstraint> LiteralConstraint::get(
        const mylang::expressions::Constant::Ptr &xe)
    {
      struct Impl: public LiteralConstraint
      {
        Impl(const mylang::expressions::Constant::Ptr &e)
            : LiteralConstraint(e)
        {
        }

        ~Impl()
        {
        }
      };
      return ::std::make_shared<Impl>(xe);
    }
  }
}
