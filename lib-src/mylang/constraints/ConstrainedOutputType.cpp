#include <mylang/constraints/ConstrainedOutputType.h>
#include <mylang/constraints/ConstrainedTypeSet.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/defs.h>
#include <mylang/types/OutputType.h>
#include <stdexcept>
#include <string>

namespace mylang {
  namespace constraints {

    ConstrainedOutputType::ConstrainedOutputType(ConstrainedType::Ptr xelement)
        : element(xelement)
    {
      if (!element) {
        throw std::invalid_argument("Missing element");
      }
      if (!element->isAnyType() && !element->isConcrete()) {
        throw ::std::invalid_argument("Not a concrete element " + xelement->toString());
      }
    }

    ConstrainedOutputType::~ConstrainedOutputType()
    {
    }

    ::std::string ConstrainedOutputType::toString() const
    {
      return "<=" + element->toString();
    }

    void ConstrainedOutputType::addRecursive(ConstrainedTypeSet &ts) const
    {
      element->addRecursive(ts);
      ts.add(self());
    }

    ConstrainedType::Ptr ConstrainedOutputType::normalize(ConstrainedTypeSet &ts) const
    {
      return ts.add(get(element->normalize(ts))).first;
    }

    bool ConstrainedOutputType::isSameConstraint(const ConstrainedType &other) const
    {
      auto t = dynamic_cast<const ConstrainedOutputType*>(&other);
      if (!t) {
        return false;
      }
      return element == t->element || element->isSameConstraint(*t->element);
    }

    void ConstrainedOutputType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitOutputType(self<ConstrainedOutputType>());
    }

    ::std::shared_ptr<const ConstrainedOutputType> ConstrainedOutputType::get(
        ConstrainedType::Ptr xelement)
    {
      struct Impl: public ConstrainedOutputType
      {
        Impl(ConstrainedType::Ptr t)
            : ConstrainedOutputType(t), the_type(mylang::types::OutputType::get(t->type()))
        {
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

      private:
        const TypePtr the_type;

      };
      if (!xelement) {
        throw ::std::invalid_argument("Missing element type");
      }
      return ::std::make_shared<const Impl>(xelement);
    }

    ::std::shared_ptr<const ConstrainedOutputType> ConstrainedOutputType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::OutputType> &t)
    {
      return get(createConstraints(t->element));
    }

  }
}
