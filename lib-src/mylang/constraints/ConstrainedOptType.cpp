#include <mylang/constraints/ConstrainedOptType.h>
#include <mylang/constraints/ConstrainedTypeSet.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/defs.h>
#include <mylang/types/OptType.h>
#include <stdexcept>
#include <string>

namespace mylang {
  namespace constraints {

    ConstrainedOptType::ConstrainedOptType(ConstrainedType::Ptr xelement)
        : element(xelement)
    {
      if (!element) {
        throw std::invalid_argument("Missing element");
      }
      if (!element->isAnyType() && !element->isConcrete()) {
        throw ::std::invalid_argument("Not a concrete element " + xelement->toString());
      }
    }

    ConstrainedOptType::~ConstrainedOptType()
    {
    }

    ConstrainedType::Ptr ConstrainedOptType::flatten() const
    {
      auto opt = ::std::dynamic_pointer_cast<const ConstrainedOptType>(element);
      if (opt) {
        return opt->flatten();
      } else {
        return element;
      }
    }

    ConstrainedType::Ptr ConstrainedOptType::intersectWith(const Ptr &t) const
    {
      auto that = ::std::dynamic_pointer_cast<const ConstrainedOptType>(t);
      if (!that) {
        return nullptr;
      }

      auto e = element->intersectWith(that->element);
      if (!e) {
        return nullptr;
      }
      return get(e);
    }

    ConstrainedType::Ptr ConstrainedOptType::unionWith(const Ptr &t) const
    {
      ConstrainedType::Ptr res = ConstrainedType::unionWith(t);
      if (res) {
        return res;
      }
      auto that = ::std::dynamic_pointer_cast<const ConstrainedOptType>(t);
      if (!that) {
        return nullptr;
      }
      auto e = element->unionWith(that->element);
      if (!e) {
        return nullptr;
      }
      return get(e);
    }

    bool ConstrainedOptType::canSafeCastFrom(const ConstrainedType &from) const
    {
      auto other = dynamic_cast<const ConstrainedOptType*>(&from);
      if (!other) {
        return ConstrainedType::canSafeCastFrom(from);
      }
      return element->canSafeCastFrom(*other->element);
    }

    bool ConstrainedOptType::canCastFrom(const ConstrainedType &from) const
    {
      auto other = dynamic_cast<const ConstrainedOptType*>(&from);
      if (!other) {
        return ConstrainedType::canCastFrom(from);
      }
      return element->canCastFrom(*other->element);
    }

    ::std::string ConstrainedOptType::toString() const
    {
      return element->toString() + '?';
    }

    void ConstrainedOptType::addRecursive(ConstrainedTypeSet &ts) const
    {
      if (ts.add(self()).second) {
        ts.add(self());
        element->addRecursive(ts);
      }
    }

    ConstrainedType::Ptr ConstrainedOptType::normalize(ConstrainedTypeSet &ts) const
    {
      return ts.add(get(element->normalize(ts))).first;
    }

    bool ConstrainedOptType::isSameConstraint(const ConstrainedType &other) const
    {
      auto t = dynamic_cast<const ConstrainedOptType*>(&other);
      if (!t) {
        return false;
      }
      return element == t->element || element->isSameConstraint(*t->element);
    }

    bool ConstrainedOptType::isConstructible() const
    {
      return true;
    }

    void ConstrainedOptType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitOpt(::std::dynamic_pointer_cast<const ConstrainedOptType>(self()));
    }

    ::std::shared_ptr<const ConstrainedOptType> ConstrainedOptType::copy(
        const ConstrainedType::Ptr &newElement) const
    {
      return get(newElement);
    }

    ::std::shared_ptr<const ConstrainedOptType> ConstrainedOptType::get(
        ConstrainedType::Ptr xelement)
    {
      struct Impl: public ConstrainedOptType
      {
        Impl(ConstrainedType::Ptr t)
            : ConstrainedOptType(t), the_type(mylang::types::OptType::get(t->type()))
        {
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

      private:
        const TypePtr the_type;

      };
      if (!xelement) {
        throw ::std::invalid_argument("Missing element type");
      }
      return ::std::make_shared<const Impl>(xelement);
    }

    ::std::shared_ptr<const ConstrainedOptType> ConstrainedOptType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::OptType> &t)
    {
      return get(createConstraints(t->element));
    }

  }
}
