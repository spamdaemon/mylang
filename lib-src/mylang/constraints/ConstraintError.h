#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINTERROR_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINTERROR_H

#ifndef FILE_MYLANG_CONSTRAINTS_CONSTRAINTS_H
#include <mylang/constraints/Constraints.h>
#endif
#include <stdexcept>

namespace mylang {
  namespace constraints {

    /**
     * A constraint on a constraint.
     */
    class ConstraintError: public ::std::runtime_error
    {
      /**
       * Create a constraint with a name
       * @param name the name of the constraint
       * @param c the constraint
       * @param msg an error message
       */
    public:
      ConstraintError(const ::std::string &name, const Constraint::Ptr &c,
          const ::std::string &msg);

      /**
       * Create an exception
       * @param c the constraints the could not be applied
       * @param msg an error message
       */
    public:
      ConstraintError(const Constraints &c, const ::std::string &msg);

      /** Destructor */
    public:
      virtual ~ConstraintError();

      /** The name of the constraint */
    public:
      const Constraints constraints;

      /** The error */
    public:
      const ::std::string error;
    };
  }
}
#endif
