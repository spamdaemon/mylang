#include <mylang/constraints/ConstrainedFunctionType.h>
#include <mylang/constraints/ConstrainedRealType.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/types.h>

namespace mylang {
  namespace constraints {

    ConstrainedRealType::ConstrainedRealType()
    {
    }

    ConstrainedRealType::~ConstrainedRealType()
    {
    }

    void ConstrainedRealType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitReal(::std::dynamic_pointer_cast<const ConstrainedRealType>(self()));
    }

    bool ConstrainedRealType::canSafeCastFrom(const ConstrainedType &from) const
    {
      if (from.self<ConstrainedIntegerType>()) {
        return true;
      }
      return ConstrainedPrimitiveType::canSafeCastFrom(from);
    }

    bool ConstrainedRealType::canCastFrom(const ConstrainedType &from) const
    {
      return ConstrainedPrimitiveType::canCastFrom(from);
    }

    bool ConstrainedRealType::isSameConstraint(const ConstrainedType &other) const
    {
      return dynamic_cast<const ConstrainedRealType*>(&other) != nullptr;
    }

    ::std::string ConstrainedRealType::toString() const
    {
      return "real";
    }

    ::std::shared_ptr<const ConstrainedRealType> ConstrainedRealType::negate() const
    {
      return self<ConstrainedRealType>();
    }

    ::std::shared_ptr<const ConstrainedRealType> ConstrainedRealType::create()
    {
      struct Impl: public ConstrainedRealType
      {
        Impl()
            : the_type(mylang::types::RealType::create())
        {
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

      private:
        const TypePtr the_type;
      };

      return ::std::make_shared<Impl>();
    }

    ConstrainedTypePtr ConstrainedRealType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::RealType>&)
    {
      return create();
    }

  }
}
