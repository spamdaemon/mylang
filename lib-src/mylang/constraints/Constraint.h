#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINT_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINT_H

#ifndef CLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif
#include <string>

namespace mylang {
  namespace constraints {

    /**
     * A constraint on a constraint.
     */
    class Constraint: public ::std::enable_shared_from_this<Constraint>
    {

      /** A pointer to a constraint */
    public:
      typedef ::std::shared_ptr<const Constraint> Ptr;

      /**
       * Create a constraint with a name
       * @param name the name of the constraint
       */
    public:
      Constraint();

      /** Destructor */
    public:
      virtual ~Constraint();

      /**
       * Check if two constraints are the same.
       * @param c1 a constraint
       * @param c2 a constraint
       * @return true if c1 and c2 are the same constraint
       */
    public:
      static bool compare(const Ptr &c1, const Ptr &c2);

      /**
       * Determine if two constraints are the same
       * @param other a constraint
       * @return true if this constraint is the same as the other constraint
       */
    public:
      virtual bool isSameConstraint(const Constraint &other) const;
    };
  }
}
#endif
