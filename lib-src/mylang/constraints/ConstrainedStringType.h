#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDSTRINGTYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDSTRINGTYPE_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDPRIMITIVETYPE_H
#include <mylang/constraints/ConstrainedPrimitiveType.h>
#endif

#ifndef FILE_MYLANG_TYPES_H
#include <mylang/types.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDINTEGERTYPE_H
#include <mylang/constraints/ConstrainedIntegerType.h>
#endif

namespace mylang {
  namespace constraints {

    /** The baseclass for all types */
    class ConstrainedStringType: public ConstrainedPrimitiveType
    {

    private:
      ConstrainedStringType(const ::std::shared_ptr<const ConstrainedIntegerType> &idx,
          const ::std::shared_ptr<const ConstrainedIntegerType> &len);

    public:
      ~ConstrainedStringType();

      /**
       * Create a type constraint.
       * @param t a type
       * @return an "unconstrained" constraint
       */
    public:
      static Ptr createUnconstrained(const ::std::shared_ptr<const mylang::types::StringType> &t);

      /**
       * Get the bit constraint
       * @return a primitive constraint for a INTEGER
       */
    public:
      static ::std::shared_ptr<const ConstrainedStringType> create();

    private:
      static ::std::shared_ptr<const ConstrainedStringType> createString(
          const ::std::shared_ptr<const ConstrainedIntegerType> &idx,
          const ::std::shared_ptr<const ConstrainedIntegerType> &len);

    public:
      void accept(ConstrainedTypeVisitor &v) const override;

      bool isSameConstraint(const ConstrainedType &other) const final override;

      ::std::string toString() const final override;

      Ptr normalize(ConstrainedTypeSet &ts) const override;

      /** The index constraint; it may be null if not known */
    public:
      const ::std::shared_ptr<const ConstrainedIntegerType> index;

      /** The constraint on the length */
    public:
      const ::std::shared_ptr<const ConstrainedIntegerType> length;

    };
  }
}
#endif
