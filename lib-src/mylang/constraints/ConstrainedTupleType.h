#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTUPLETYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTUPLETYPE_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPE_H
#include <mylang/constraints/ConstrainedType.h>
#endif
#ifndef FILE_MYLANG_TYPES_H
#include <mylang/types.h>
#endif
#include <memory>
#include <vector>

namespace mylang {
  namespace constraints {

    /** A constraint representing an error */
    class ConstrainedTupleType: public ConstrainedType
    {

    public:
      struct Member
      {
        Member(const ::std::string &xname, const ConstrainedType::Ptr &xtype);

        /** The name of the parameter */
        const ::std::string name;

        /** The parameter constraint */
        const ConstrainedType::Ptr constraint;
      };

    private:
      ConstrainedTupleType(const ::std::vector<Ptr> &tuple);

    public:
      ~ConstrainedTupleType();

      /**
       * Create a type constraint.
       * @param t a type
       * @return an "unconstrained" constraint
       */
    public:
      static Ptr createUnconstrained(const ::std::shared_ptr<const mylang::types::TupleType> &t);

      /** Two primitives are the same if their constraint ids are the same  */
    public:
      Ptr normalize(ConstrainedTypeSet &ts) const override;

      void addRecursive(ConstrainedTypeSet &ts) const override;

      void accept(ConstrainedTypeVisitor &v) const override;

      bool isSameConstraint(const ConstrainedType &other) const final override;

      ::std::string toString() const final override;

      bool canSafeCastFrom(const ConstrainedType &t) const override;

      bool canCastFrom(const ConstrainedType &t) const override;

      bool isConstructible() const override;

      Ptr intersectWith(const Ptr &t) const override;

      Ptr unionWith(const Ptr &t) const override;

      /**
       * Get the head type.
       * @return the first type
       * @return the first type or nullptr if this tuple has no members
       */
    public:
      ConstrainedTypePtr getHead() const;

      /**
       * Get the tail tuple type.
       * @return a tuple consisting of all members but the first
       * @return the tail tuple or nullptr if this tuple has no members
       */
    public:
      ::std::shared_ptr<const ConstrainedTupleType> getTail() const;

      /**
       * Create a tuple that is the merge of the specified tuples
       * @param tuples a list of types
       * @return a tuple
       */
    public:
      static ::std::shared_ptr<const ConstrainedTupleType> merge(
          const ::std::vector<::std::shared_ptr<const ConstrainedTupleType>> &xtuples);

      /**
       * Get a Tupleure
       * @param tuple
       */
    public:
      static ::std::shared_ptr<const ConstrainedTupleType> get(const ::std::vector<Ptr> &xtuple);

      /** The types of the tuple */
    public:
      const ::std::vector<Ptr> types;

      /** The tuple (each constraint can be accessed using a unique name for convenience) */
    public:
      const ::std::vector<Member> tuple;
    };
  }
}
#endif
