#include <mylang/constraints/ConstrainedBooleanType.h>
#include <mylang/constraints/ConstrainedFunctionType.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/types.h>

namespace mylang {
  namespace constraints {

    ConstrainedBooleanType::ConstrainedBooleanType()
    {
    }

    ConstrainedBooleanType::~ConstrainedBooleanType()
    {
    }

    void ConstrainedBooleanType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitBoolean(::std::dynamic_pointer_cast<const ConstrainedBooleanType>(self()));
    }

    bool ConstrainedBooleanType::isSameConstraint(const ConstrainedType &other) const
    {
      return dynamic_cast<const ConstrainedBooleanType*>(&other) != nullptr;
    }

    ::std::string ConstrainedBooleanType::toString() const
    {
      return "boolean";
    }

    ::std::shared_ptr<const ConstrainedBooleanType> ConstrainedBooleanType::create()
    {
      struct Impl: public ConstrainedBooleanType
      {
        Impl()
            : the_type(mylang::types::BooleanType::create())
        {
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

      private:
        const TypePtr the_type;
      };

      return ::std::make_shared<Impl>();
    }

    ConstrainedTypePtr ConstrainedBooleanType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::BooleanType>&)
    {
      return create();
    }

  }
}
