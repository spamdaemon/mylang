#include <mylang/constraints/ConstrainedCharType.h>
#include <mylang/constraints/ConstrainedFunctionType.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/types.h>

namespace mylang {
  namespace constraints {

    ConstrainedCharType::ConstrainedCharType()
    {
    }

    ConstrainedCharType::~ConstrainedCharType()
    {
    }

    void ConstrainedCharType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitChar(::std::dynamic_pointer_cast<const ConstrainedCharType>(self()));
    }

    bool ConstrainedCharType::isSameConstraint(const ConstrainedType &other) const
    {
      return dynamic_cast<const ConstrainedCharType*>(&other) != nullptr;
    }

    ::std::string ConstrainedCharType::toString() const
    {
      return "char";
    }

    ::std::shared_ptr<const ConstrainedCharType> ConstrainedCharType::create()
    {
      struct Impl: public ConstrainedCharType
      {
        Impl()
            : the_type(mylang::types::CharType::create())
        {
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

      private:
        const TypePtr the_type;
      };

      return ::std::make_shared<Impl>();
    }

    ConstrainedTypePtr ConstrainedCharType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::CharType>&)
    {
      return create();
    }

  }
}
