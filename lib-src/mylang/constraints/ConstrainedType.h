#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPE_H
#include <memory>
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <mylang/defs.h>

#ifndef CLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINT_H
#include <mylang/constraints/Constraint.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINTS_H
#include <mylang/constraints/Constraints.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINTERROR_H
#include <mylang/constraints/ConstraintError.h>
#endif

namespace mylang {
  namespace functions {
    class Function;

    class FunctionArgument;
  }

  namespace constraints {
    class ConstrainedTypeSet;

    /** The constraint visitor */
    class ConstrainedTypeVisitor;

    /** The baseclass for all types */
    class ConstrainedType: public ::std::enable_shared_from_this<ConstrainedType>
    {

      /** A constraint pointer */
    public:
      typedef ::std::shared_ptr<const ConstrainedType> Ptr;

      /** A pointer to a constraint */
    public:
      typedef ::std::shared_ptr<const Constraint> ConstraintPtr;

      /** Create new constraint instance */
    protected:
      ConstrainedType();

      /** Create new constraint instance with a name.
       * @param xid an optional id */
    protected:
      ConstrainedType(const ::mylang::NamePtr &xid);

    public:
      virtual ~ConstrainedType() = 0;

      /**
       * Get the type of this constraint.
       * @return the type
       */
    public:
      virtual TypePtr type() const = 0;

      /**
       * Accept the specified constraint visitor
       * @param v a visitor
       */
    public:
      virtual void accept(ConstrainedTypeVisitor &v) const = 0;

      /**
       * Get this constraint.
       * @return this constraint instance
       */
    public:
      template<class T = ConstrainedType>
      inline ::std::shared_ptr<const T> self() const
      {
        return ::std::dynamic_pointer_cast<const T>(shared_from_this());
      }

      /**
       * Create a type constraint.
       * @param t a type
       * @return an "unconstrained" constraint
       */
    public:
      static Ptr createConstraints(const TypePtr &t);

      /**
       * Get the basetype of this type.
       * @return the basetype or null if it has no basetype
       */
    public:
      virtual Ptr basetype() const;

      /**
       * Follow the base types until null returned.
       * @return this type or the basetype of this type
       */
    public:
      Ptr roottype() const;

      /**
       * Determine if this type is concrete.  A type that references the AnyType, is
       * not not concrete.
       * @return true by if the corresponding type is concrete
       */
    public:
      bool isConcrete() const;

      /**
       * Is this the AnyType. This helper function to be make it easier to check for AnyType.
       * @return true if the corresponding type is an any type
       */
    public:
      bool isAnyType() const;

      /**
       * Is this constraint a base constraint of the given constraint.
       * @param derived a derived constraint
       * @return true if this constraint is a basetype of the given constraint
       */
    public:
      virtual bool isBaseTypeOf(const ConstrainedType &derived) const;

      /**
       * Determine if an implicit cast (safe or not safe) should be allowed.
       * @param from a type from thich to cast
       * @return true if an implicit cast should be allowed
       * @param from a constraint from which to cast
       * @return true if a cast between two constraint is safe, false otherwise
       */
    public:
      bool canImplicitlyCastFrom(const ConstrainedType &from) const;

      /**
       * Can the specified constraint object be safely cast to this constraint.
       * @param from a constraint from which to cast
       * @return true if a cast between two constraint is safe, false otherwise
       */
    public:
      virtual bool canSafeCastFrom(const ConstrainedType &from) const;

      /**
       * Can the specified constraint object be cast to this constraint. The cast
       * may be safe or unsafe, but there is a way to convert between the types.
       * @param from a constraint from which to cast
       * @return true if it possible to cast from to this constraint
       */
    public:
      virtual bool canCastFrom(const ConstrainedType &from) const;

      /**
       * Determine if this constraint is the same as the specified constraint
       * @param other another constraint
       * @return true if the two types are the same
       */
    public:
      virtual bool isSameConstraint(const ConstrainedType &other) const = 0;

      /**
       * Get a string representation of this constraint. The representation is undefined and is primarily for debugging purposes.
       * @return a string representation
       */
    public:
      virtual ::std::string toString() const = 0;

      /**
       * Determine if this constraint is constructible.
       * @return true if there is at least 1 constructor for this constraint.
       */
    public:
      virtual bool isConstructible() const;

      /**
       * Get the unique name for this constraint
       * @return a unique name for this constraint
       */
    public:
      ::mylang::NamePtr name() const;

      /**
       * Get the constraints for this constraint
       * @return the constraints on this constraint
       */
    public:
      virtual Constraints constraints() const;

      /**
       * Create a new constraint by constraining this constraint. If there are no
       * constraints, then it returns self.
       * @param constraints additional constraints
       * @return a new constraint
       * @throws a constraint  error if the constraints cannot be applied
       */
    public:
      virtual Ptr constrain(const Constraints &constraints) const;

      /**
       * Add a normalized copy of this constraint to the typeset.
       * @param ts a typeset
       * @return a instance of this constraint that is fully normalized
       */
    public:
      virtual Ptr normalize(ConstrainedTypeSet &ts) const = 0;

      /**
       * Collect all types recursively.
       * @param ts a typeset
       * @return a typeset
       */
    public:
      virtual void addRecursive(ConstrainedTypeSet &ts) const = 0;

      /**
       * Union this constraint with the specified constraint.
       * If t is the same constraint as this, then it returns self().
       * Otherwise the union of t must be such that both t and self()
       * can be safely casted to the union.
       * @param t a constraint
       * @return the constraint that is the union  or nullptr if a union is not possible
       */
    public:
      virtual Ptr unionWith(const Ptr &t) const;

      /**
       * Intersect this constraint with the specified constraint.
       * If t is the same constraint as this, then it returns self().
       * Otherwise, returns a type that can represents the values that are common to
       * the two involvded constraints
       * @param t a constraint
       * @return the constraint that is the union  or nullptr if an intersection is empty
       */
    public:
      virtual Ptr intersectWith(const Ptr &t) const;

      /**
       * Print the specified constraint.
       * @param out an output stream
       * @param constraint a constraint pointer
       * @return out
       */
    public:
      friend inline ::std::ostream& operator<<(::std::ostream &out, const Ptr &constraint)
      {
        if (constraint) {
          out << *constraint;
        }
        return out;
      }

      /**
       * Print the specified constraint.
       * @param out an output stream
       * @param constraint a constraint pointer
       * @return out
       */
    public:
      friend inline ::std::ostream& operator<<(::std::ostream &out,
          const ConstrainedType &constraint)
      {
        out << constraint.toString();
        return out;
      }

      /** A unique id for this constraint; the id is NOT used to determine if two types are the same */
    private:
      mutable ::mylang::NamePtr _id;
    };
  }
}
#endif
