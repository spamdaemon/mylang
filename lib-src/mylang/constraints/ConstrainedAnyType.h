#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDANYTYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDANYTYPE_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPE_H
#include <mylang/constraints/ConstrainedType.h>
#endif
#ifndef FILE_MYLANG_TYPES_H
#include <mylang/types.h>
#endif
#include <memory>

namespace mylang {
  namespace constraints {

    /** The baseclass for all types */
    class ConstrainedAnyType: public ConstrainedType
    {

    private:
      ConstrainedAnyType();

    public:
      ~ConstrainedAnyType();

      bool isConstructible() const override;

      Ptr normalize(ConstrainedTypeSet &ts) const override;

      void addRecursive(ConstrainedTypeSet &ts) const override;

      void accept(ConstrainedTypeVisitor &v) const override;

      bool isSameConstraint(const ConstrainedType &other) const final override;

      ::std::string toString() const final override;

      /**
       * Get the bit constraint
       * @return a primitive constraint for a bit
       */
    public:
      static ::std::shared_ptr<const ConstrainedAnyType> get();

      static Ptr createUnconstrained(const ::std::shared_ptr<const mylang::types::AnyType> &t);
    };
  }
}
#endif
