#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDUNIONTYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDUNIONTYPE_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDOPTTYPE_H
#include <mylang/constraints/ConstrainedOptType.h>
#endif

#ifndef CLASS_MYLANG_EXPRESSIONS_CONSTANT_H
#include <mylang/expressions/Constant.h>
#endif
#ifndef FILE_MYLANG_TYPES_H
#include <mylang/types.h>
#endif
#include <memory>
#include <map>
#include <vector>
#include <optional>

namespace mylang {
  namespace constraints {

    /** A constraint representing an error */
    class ConstrainedUnionType: public ConstrainedType
    {

    public:
      struct Discriminant
      {

        Discriminant(const ::std::string &xname, const ConstrainedType::Ptr &xtype);

        /** The name of the parameter */
        const ::std::string name;

        /** The parameter constraint */
        const ConstrainedType::Ptr constraint;
      };

    public:
      struct Member
      {

        Member(const mylang::expressions::Constant::Ptr &discriminant, const ::std::string &xname,
            const ConstrainedTypePtr &xtype);

        /** The value of the discriminant for this member */
        const mylang::expressions::Constant::Ptr discriminant;

        /** The name of the parameter */
        const ::std::string name;

        /** The parameter constraint */
        const ConstrainedTypePtr constraint;
      };

    private:
      ConstrainedUnionType(const Discriminant &xdiscriminant, const ::std::vector<Member> &members);

    public:
      ~ConstrainedUnionType();

      /**
       * Create a type constraint.
       * @param t a type
       * @return an "unconstrained" constraint
       */
    public:
      static Ptr createUnconstrained(const ::std::shared_ptr<const mylang::types::UnionType> &t);

      /** Two primitives are the same if their constraint ids are the same  */
    public:
      Ptr normalize(ConstrainedTypeSet &ts) const override;

      void addRecursive(ConstrainedTypeSet &ts) const override;

      void accept(ConstrainedTypeVisitor &v) const override;

      bool isSameConstraint(const ConstrainedType &other) const final override;

      ::std::string toString() const final override;

      bool isConstructible() const override;

      /**
       * Get a structure
       * @param members
       */
    public:
      static ::std::shared_ptr<const ConstrainedUnionType> get(const Discriminant &xdiscriminant,
          const ::std::vector<Member> &xmembers);

      /**
       * Find a member by name
       * @param name a name
       * @return an optional index if found
       */
    public:
      ::std::optional<size_t> getMemberIndex(const ::std::string &name) const;

      /**
       * Get the constraint of the subfield with the specified name.
       * @param name a subfield
       * @return the constraint of the subfield or nullptr if not found
       */
    public:
      ConstrainedTypePtr getMemberType(const ::std::string &field) const;

      ConstrainedTypePtr getMemberType(
          const mylang::expressions::Constant &discriminantValue) const;

      size_t getMemberIndex(const mylang::expressions::Constant &discriminantValue) const;

      /** The discriminant member */
    public:
      const Discriminant discriminant;

      /** The members */
    public:
      const ::std::vector<Member> members;

      /** The indexed members */
    private:
      ::std::map<::std::string, const Member> _indexedMembers;
    };
  }
}
#endif
