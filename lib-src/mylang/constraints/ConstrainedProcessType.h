#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDPROCESSTYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDPROCESSTYPE_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPE_H
#include <mylang/constraints/ConstrainedType.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDINPUTTYPE_H
#include <mylang/constraints/ConstrainedInputType.h>
#endif

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDOUTPUTTYPE_H
#include <mylang/constraints/ConstrainedOutputType.h>
#endif

#ifndef FILE_MYLANG_TYPES_H
#include <mylang/types.h>
#endif
#include <memory>
#include <map>

namespace mylang {
  namespace constraints {

    /** A constraint representing an error */
    class ConstrainedProcessType: public ConstrainedType
    {
    public:
      typedef ::std::map<::std::string, ::std::shared_ptr<const ConstrainedInputType>> Inputs;
      typedef ::std::map<::std::string, ::std::shared_ptr<const ConstrainedOutputType>> Outputs;

    private:
      ConstrainedProcessType(Inputs in, Outputs out);

    public:
      ~ConstrainedProcessType();

      /**
       * Create a type constraint.
       * @param t a type
       * @return an "unconstrained" constraint
       */
    public:
      static ::std::shared_ptr<const ConstrainedProcessType> createUnconstrained(
          const ::std::shared_ptr<const mylang::types::ProcessType> &t);

    public:
      static ::std::shared_ptr<const ConstrainedProcessType> get(Inputs in, Outputs out);

    public:
      Ptr normalize(ConstrainedTypeSet &ts) const override;

      void addRecursive(ConstrainedTypeSet &ts) const override;

      void accept(ConstrainedTypeVisitor &v) const override;

      ::std::string toString() const final override;

      bool isSameConstraint(const ConstrainedType &other) const override;

      bool isConstructible() const override;

    public:
      const Inputs inputs;
      const Outputs outputs;
    };
  }
}
#endif
