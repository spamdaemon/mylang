#include <mylang/constraints/ConstrainedNamedType.h>
#include <mylang/constraints/ConstrainedTypeSet.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/types/NamedType.h>
#include <cassert>

namespace mylang {
  namespace constraints {

    ConstrainedNamedType::ConstrainedNamedType(const mylang::NamePtr &xname)
        : ConstrainedType(xname)
    {
    }

    ConstrainedNamedType::~ConstrainedNamedType()
    {
    }

    TypePtr ConstrainedNamedType::type() const
    {
      auto c = self<ConstrainedNamedType>();
      auto resolver = [=]() {
        auto impl = c->resolve();
        return impl ? impl->type() : nullptr;
      };
      return mylang::types::NamedType::get(name(), resolver);
    }

    bool ConstrainedNamedType::canSafeCastFrom(const ConstrainedType &t) const
    {
#if 0
            if (basetype()->canSafeCastFrom(t)
                && dynamic_cast<const ConstrainedNamedType*>(&t) == nullptr) {
              //return true;
            }
#endif
      return ConstrainedType::canSafeCastFrom(t);
    }

    bool ConstrainedNamedType::canCastFrom(const ConstrainedType &t) const
    {
#if 0
            if (basetype()->canCastFrom(t) && dynamic_cast<const ConstrainedNamedType*>(&t) == nullptr) {
              //  return true;
            }
#endif
      return ConstrainedType::canCastFrom(t);
    }

    void ConstrainedNamedType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitNamedType(::std::dynamic_pointer_cast<const ConstrainedNamedType>(self()));
    }

    ConstrainedType::Ptr ConstrainedNamedType::basetype() const
    {
      return resolve();
    }

    bool ConstrainedNamedType::isConstructible() const
    {
      return resolve()->isConstructible();
    }

    ::std::string ConstrainedNamedType::toString() const
    {
      return name()->fullName();
    }

    bool ConstrainedNamedType::isSameConstraint(const ConstrainedType &other) const
    {
      auto t = dynamic_cast<const ConstrainedNamedType*>(&other);
      if (t && name() == t->name()) {
        // TODO: we probably should have a check to make sure we have no inconsistencies
        return true;
      }
      return false;
    }

    ::std::shared_ptr<const ConstrainedNamedType> ConstrainedNamedType::getRecursiveReference() const
    {
      struct Impl: public ConstrainedNamedType
      {
        Impl(const ::std::shared_ptr<const ConstrainedNamedType> &ptr)
            : ConstrainedNamedType(ptr->name()), weakRef(ptr)
        {
        }

        ~Impl()
        {
        }

        ::std::shared_ptr<const ConstrainedNamedType> getRecursiveReference()
        {
          return ::std::dynamic_pointer_cast<const ConstrainedNamedType>(self());
        }

        Ptr normalize(ConstrainedTypeSet &ts) const
        {
          if (isResolved()) {
            ts.add(resolve());
          }
          return self();
        }

        void addRecursive(ConstrainedTypeSet &ts) const
        {
          ts.add(self());
          if (isResolved()) {
            resolve()->addRecursive(ts);
          }
        }

        bool isResolved() const
        {
          return weakRef.lock()->isResolved();
        }

        /** Resolve this name to an actual constraint. Throws an exception if the name cannot be resolved. */
      public:
        ConstrainedType::Ptr resolve() const
        {
          return weakRef.lock()->resolve();
        }

        ::std::weak_ptr<const ConstrainedNamedType> weakRef;
      };
      return ::std::make_shared<Impl>(
          ::std::dynamic_pointer_cast<const ConstrainedNamedType>(self()));
    }

    ConstrainedTypePtr ConstrainedNamedType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::NamedType> &t)
    {
      throw ::std::runtime_error("Create unconstrained named types not yet supported");
    }

    ::std::shared_ptr<const ConstrainedNamedType> ConstrainedNamedType::get(
        const mylang::NamePtr &xname, Resolver xresolver)
    {
      struct Impl: public ConstrainedNamedType
      {
        Impl(const mylang::NamePtr &xname, Resolver xresolver)
            : ConstrainedNamedType(xname), resolver(xresolver)
        {
        }

        ~Impl()
        {
        }

        bool isResolved() const
        {
          try {
            resolve();
            return true;
          } catch (...) {
            return false;
          }
        }

        void addRecursive(ConstrainedTypeSet &ts) const
        {
          ts.add(self());
          if (isResolved()) {
            impl->addRecursive(ts);
          }
        }

        Ptr normalize(ConstrainedTypeSet &ts) const
        {
          ts.add(self());
          if (isResolved()) {
            ts.add(impl->normalize(ts));
          }
          return self();
        }

        /** Resolve this name to an actual constraint. Throws an exception if the name cannot be resolved. */
      public:
        ConstrainedType::Ptr resolve() const
        {
          if (impl) {
            return impl;
          }

          auto t = resolver();
          if (t) {
            resolver = Resolver();
            impl = t;
            return t;
          }
          throw ::std::runtime_error("Type not resolved : " + name()->fullName());
        }

        mutable Resolver resolver;
        mutable Ptr impl;
      };
      return ::std::make_shared<Impl>(xname, xresolver);
    }
  }
}
