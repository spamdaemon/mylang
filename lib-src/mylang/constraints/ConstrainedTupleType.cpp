#include <mylang/constraints/ConstrainedFunctionType.h>
#include <mylang/constraints/ConstrainedTupleType.h>
#include <mylang/constraints/ConstrainedTypeSet.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/types.h>

namespace mylang {
  namespace constraints {
    namespace {
      static ::std::vector<ConstrainedTupleType::Member> toMembers(
          const ::std::vector<ConstrainedType::Ptr> &xtuple)
      {
        ::std::vector<ConstrainedTupleType::Member> res;
        for (size_t i = 0; i < xtuple.size(); ++i) {
          auto t = xtuple[i];
          if (!t) {
            throw ::std::invalid_argument("null constraint");
          }
          res.emplace_back("_" + ::std::to_string(i), t);
        }
        return res;
      }
    }

    ConstrainedTupleType::Member::Member(const ::std::string &xname,
        const ConstrainedType::Ptr &xtype)
        : name(xname), constraint(xtype)
    {

    }

    ConstrainedTupleType::ConstrainedTupleType(const ::std::vector<Ptr> &xtuple)
        : types(xtuple), tuple(toMembers(xtuple))
    {
    }

    ConstrainedTupleType::~ConstrainedTupleType()
    {
    }

    ConstrainedType::Ptr ConstrainedTupleType::intersectWith(const Ptr &t) const
    {
      auto that = ::std::dynamic_pointer_cast<const ConstrainedTupleType>(t);
      if (!that) {
        return nullptr;
      }
      if (that->types.size() != types.size()) {
        return nullptr;
      }

      ::std::vector<Ptr> typeIntersection;
      for (size_t i = 0, sz = types.size(); i < sz; ++i) {
        auto tmp = that->types.at(i)->intersectWith(types.at(i));
        if (!tmp) {
          return nullptr;
        }
        typeIntersection.push_back(tmp);
      }

      return get(typeIntersection);
    }

    ConstrainedType::Ptr ConstrainedTupleType::unionWith(const Ptr &t) const
    {
      ConstrainedType::Ptr res = ConstrainedType::unionWith(t);
      if (res) {
        return res;
      }
      auto that = ::std::dynamic_pointer_cast<const ConstrainedTupleType>(t);
      if (!that) {
        return nullptr;
      }
      if (that->types.size() != types.size()) {
        return nullptr;
      }

      ::std::vector<Ptr> typeUnion;
      for (size_t i = 0, sz = types.size(); i < sz; ++i) {
        auto tmp = that->types.at(i)->unionWith(types.at(i));
        if (!tmp) {
          return nullptr;
        }
        typeUnion.push_back(tmp);
      }

      return get(typeUnion);
    }

    bool ConstrainedTupleType::canSafeCastFrom(const ConstrainedType &from) const
    {
      auto thatTuple = dynamic_cast<const ConstrainedTupleType*>(&from);
      if (!thatTuple) {
        return ConstrainedType::canSafeCastFrom(from);
      }
      if (types.size() != thatTuple->tuple.size()) {
        return false;
      }
      for (size_t i = 0; i < types.size(); ++i) {
        if (!types[i]->canSafeCastFrom(*thatTuple->types[i])) {
          return false;
        }
      }
      return true;
    }

    bool ConstrainedTupleType::canCastFrom(const ConstrainedType &from) const
    {
      auto thatTuple = dynamic_cast<const ConstrainedTupleType*>(&from);
      if (!thatTuple) {
        return ConstrainedType::canCastFrom(from);
      }
      if (types.size() != thatTuple->tuple.size()) {
        return false;
      }
      for (size_t i = 0; i < types.size(); ++i) {
        if (!types[i]->canCastFrom(*thatTuple->types[i])) {
          return false;
        }
      }
      return true;
    }

    bool ConstrainedTupleType::isConstructible() const
    {
      return true;
    }

    void ConstrainedTupleType::addRecursive(ConstrainedTypeSet &ts) const
    {
      if (ts.add(self()).second) {
        for (auto v : types) {
          v->addRecursive(ts);
        }
      }
    }

    ConstrainedType::Ptr ConstrainedTupleType::normalize(ConstrainedTypeSet &ts) const
    {
      ::std::vector<Ptr> tmp;
      for (auto v : types) {
        tmp.push_back(v->normalize(ts));
      }
      return ts.add(get(tmp)).first;
    }

    void ConstrainedTupleType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitTuple(::std::dynamic_pointer_cast<const ConstrainedTupleType>(self()));
    }

    bool ConstrainedTupleType::isSameConstraint(const ConstrainedType &other) const
    {
      auto t = dynamic_cast<const ConstrainedTupleType*>(&other);
      if (!t || t->tuple.size() != tuple.size()) {
        return false;
      }
      for (size_t i = 0; i < tuple.size(); ++i) {
        if (!tuple[i].constraint->isSameConstraint(*t->tuple[i].constraint)) {
          return false;
        }
      }

      return true;
    }

    ::std::string ConstrainedTupleType::toString() const
    {
      ::std::string str;
      str += "tuple {";
      const char *sep = "";
      for (auto t : tuple) {
        str += sep;
        str += t.constraint->toString();
        sep = ", ";
      }
      str += "}";
      return str;
    }

    ::std::shared_ptr<const ConstrainedTupleType> ConstrainedTupleType::get(
        const ::std::vector<Ptr> &tuple)
    {
      struct Impl: public ConstrainedTupleType
      {
        Impl(const ::std::vector<Ptr> &t)
            : ConstrainedTupleType(t)
        {
          ::std::vector<TypePtr> M;
          for (auto e : t) {
            M.push_back(e->type());
          }
          the_type = mylang::types::TupleType::get(M);
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

      private:
        TypePtr the_type;
      };
      return ::std::make_shared<Impl>(tuple);
    }

    ConstrainedTypePtr ConstrainedTupleType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::TupleType> &t)
    {
      ::std::vector<Ptr> members;
      for (auto m : t->types) {
        members.push_back(createConstraints(m));
      }
      return get(members);
    }

    ::std::shared_ptr<const ConstrainedTupleType> ConstrainedTupleType::merge(
        const ::std::vector<::std::shared_ptr<const ConstrainedTupleType>> &xtuples)
    {
      ::std::vector<Ptr> xtypes;
      for (auto t : xtuples) {
        xtypes.insert(xtypes.end(), t->types.begin(), t->types.end());
      }
      return get(xtypes);
    }

    ConstrainedTypePtr ConstrainedTupleType::getHead() const
    {
      if (types.empty()) {
        return nullptr;
      }
      return types.at(0);
    }

    ::std::shared_ptr<const ConstrainedTupleType> ConstrainedTupleType::getTail() const
    {
      if (types.empty()) {
        return nullptr;
      }
      ::std::vector<ConstrainedTypePtr> m;
      for (size_t i = 1; i < types.size(); ++i) {
        m.emplace_back(types.at(i));
      }
      return get(m);
    }

  }
}
