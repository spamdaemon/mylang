#ifndef CLASS_MYLANG_CONSTRAINTS_DYNAMICCONSTRAINT_H
#define CLASS_MYLANG_CONSTRAINTS_DYNAMICCONSTRAINT_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINT_H
#include <mylang/constraints/Constraint.h>
#endif

#ifndef CLASS_MYLANG_EXPRESSIONS_EXPRESSION_H
#include <mylang/expressions/Expression.h>
#endif

namespace mylang {
  namespace constraints {

    /**
     * A runtime constraint will be evaluated at runtime since the expression
     * is cannot be known at compile time.
     */
    class DynamicConstraint: public mylang::constraints::Constraint
    {
    private:
      DynamicConstraint(const mylang::expressions::Expression::Ptr &expr);

    public:
      ~DynamicConstraint();

      bool isSameConstraint(const Constraint &other) const;

      /**
       * Get a named constraint
       * @param name the name of the constraint
       * @param expr the expression
       * @return a named constraint
       */
    public:
      static ::std::shared_ptr<const DynamicConstraint> get(
          const mylang::expressions::Expression::Ptr &expr);

      /**
       * Expression
       */
    public:
      const mylang::expressions::Expression::Ptr expression;
    };
  }
}
#endif
