#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDOUTPUTTYPE_H
#define CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDOUTPUTTYPE_H

#ifndef CLASS_MYLANG_CONSTRAINTS_CONSTRAINEDTYPE_H
#include <mylang/constraints/ConstrainedType.h>
#endif
#ifndef FILE_MYLANG_TYPES_H
#include <mylang/types.h>
#endif
#include <memory>

namespace mylang {
  namespace constraints {

    /** A constraint representing an error */
    class ConstrainedOutputType: public ConstrainedType
    {

    private:
      ConstrainedOutputType(ConstrainedType::Ptr xelement);

    public:
      ~ConstrainedOutputType();

      /**
       * Create a type constraint.
       * @param t a type
       * @return an "unconstrained" constraint
       */
    public:
      static ::std::shared_ptr<const ConstrainedOutputType> createUnconstrained(
          const ::std::shared_ptr<const mylang::types::OutputType> &t);

    public:
      static ::std::shared_ptr<const ConstrainedOutputType> get(ConstrainedType::Ptr xelement);

    public:
      Ptr normalize(ConstrainedTypeSet &ts) const override;

      void addRecursive(ConstrainedTypeSet &ts) const override;

      void accept(ConstrainedTypeVisitor &v) const override;

      ::std::string toString() const final override;

      bool isSameConstraint(const ConstrainedType &other) const override;

      /** The element constraint */
    public:
      const ConstrainedType::Ptr element;
    };
  }
}
#endif
