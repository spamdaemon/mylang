#include <mylang/constraints/ConstrainedBitType.h>
#include <mylang/constraints/ConstrainedFunctionType.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/types.h>

namespace mylang {
  namespace constraints {

    ConstrainedBitType::ConstrainedBitType()
    {
    }

    ConstrainedBitType::~ConstrainedBitType()
    {
    }

    void ConstrainedBitType::accept(ConstrainedTypeVisitor &v) const
    {
      v.visitBit(::std::dynamic_pointer_cast<const ConstrainedBitType>(self()));
    }

    bool ConstrainedBitType::isSameConstraint(const ConstrainedType &other) const
    {
      return dynamic_cast<const ConstrainedBitType*>(&other) != nullptr;
    }

    ::std::string ConstrainedBitType::toString() const
    {
      return "bit";
    }

    ::std::shared_ptr<const ConstrainedBitType> ConstrainedBitType::create()
    {
      struct Impl: public ConstrainedBitType
      {
        Impl()
            : the_type(mylang::types::BitType::create())
        {
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type;
        }

      private:
        const TypePtr the_type;
      };

      return ::std::make_shared<Impl>();
    }

    ConstrainedTypePtr ConstrainedBitType::createUnconstrained(
        const ::std::shared_ptr<const mylang::types::BitType>&)
    {
      return create();
    }

  }
}
