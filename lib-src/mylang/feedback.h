#ifndef FILE_MYLANG_FEEDBACK_H
#define FILE_MYLANG_FEEDBACK_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_SOURCELOCATION_H
#include <mylang/SourceLocation.h>
#endif
#include <string>
#include <functional>
#include <memory>
#include <sstream>

namespace mylang {

  enum class FeedbackType
  {
    ERROR, WARNING, INFO, DEBUG, FIXME
  };

  typedef ::std::function<
      void(FeedbackType type, const SourceLocation &location, const ::std::string &message)> FeedbackHandler;

  /**
   * This utility class can be used to send message to a feed back handler.
   */
  class FeedbackBase
  {
    /** Constructor */
  protected:
    FeedbackBase();

    /** Constructor */
  protected:
    FeedbackBase(FeedbackHandler h);

    /** Constructor */
  protected:
    FeedbackBase(const FeedbackBase &h);

    /** Destructor */
  public:
    virtual ~FeedbackBase();

    /**
     * Emit a message to the feedback handler (if provided). The internal message stream
     * is cleared as part of this call.
     * @param node the node that provides context
     * @param message a stream that contains text that is sent to the handler
     */
  public:
    virtual void notifyInternalError(const SourceLocation &loc, ::std::ostream &message) const;

    /**
     * Emit a message to the feedback handler (if provided). The internal message stream
     * is cleared as part of this call.
     * @param node the node that provides context
     * @param message a stream that contains text that is sent to the handler
     */
  public:
    virtual void notifyError(const SourceLocation &loc, ::std::ostream &message) const;

    /**
     * Emit a message to the feedback handler (if provided). The internal message stream
     * is cleared as part of this call.
     * @param node the node that provides context
     * @param message a stream that contains text that is sent to the handler
     */
  public:
    virtual void notifyWarning(const SourceLocation &loc, ::std::ostream &message) const;

    /**
     * Emit a message to the feedback handler (if provided). The internal message stream
     * is cleared as part of this call.
     * @param node the node that provides context
     * @param message a stream that contains text that is sent to the handler
     */
  public:
    virtual void notifyInfo(const SourceLocation &loc, ::std::ostream &message) const;

    /**
     * Emit a message to the feedback handler (if provided). The internal message stream
     * is cleared as part of this call.
     * @param node the node that provides context
     * @param message a stream that contains text that is sent to the handler
     */
  public:
    virtual void notifyDebug(const SourceLocation &loc, ::std::ostream &message) const;

    /**
     * Emit a message to the feedback handler (if provided). The internal message stream
     * is cleared as part of this call.
     * @param node the node that provides context
     * @param message a stream that contains text that is sent to the handler
     */
  public:
    virtual void notifyFixme(const SourceLocation &loc, ::std::ostream &message) const;

    /** A string stream for use with the notify functions */
  public:
    mutable ::std::ostringstream stream;

    /** The function that provides feedback for errors, warnings, etc */
  private:
    FeedbackHandler _feedback;
  };

}
#endif
