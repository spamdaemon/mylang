#ifndef CLASS_MYLANG_SOURCELOCATION_H
#define CLASS_MYLANG_SOURCELOCATION_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif
#include <string>
#include <memory>
#include <sstream>

namespace mylang {

  /** This provides location information for error reporting */
  class SourceLocation
  {

  public:
    SourceLocation();

    SourceLocation(::std::string file);
    SourceLocation(const ::std::string &file, ::std::size_t line);

    SourceLocation(::idioma::ast::Node::Span xspan);

    template<class NodeTy>
    SourceLocation(const ::std::shared_ptr<NodeTy> &node)
        : SourceLocation(node ? node->span() : ::idioma::ast::Node::Span())
    {
    }

    friend ::std::ostream& operator<<(::std::ostream &out, const SourceLocation &loc)
    {
      loc.print(out);
      return out;
    }

    /** Get the span associated with the location */
  public:
    inline const ::idioma::ast::Node::Span& span() const
    {
      return _span;
    }

    /** Print the location */
  public:
    void print(::std::ostream &out) const;

    /** A location span */
  private:
    ::idioma::ast::Node::Span _span;

    /** The location as text */
  private:
    ::std::string _text;
  };

#define SOURCELOCATION (::mylang::SourceLocation(__FILE__,__LINE__))

}
#endif
