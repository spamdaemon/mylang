#ifndef FILE_MYLANG_ANNOTATIONS_H
#define FILE_MYLANG_ANNOTATIONS_H

#include <mylang/defs.h>
#include <idioma/ast.h>
#include <mylang/expressions.h>
#include <mylang/names.h>

#include <optional>
#include <string>

namespace mylang {

  struct AnnotationType
  {
    enum
    {
      ANNO_NAME, // just a name, but don't know what kind of name
      ANNO_CONSTANT,
      ANNO_CONSTRAINT,
      ANNO_TYPE,
      ANNO_PATTERN,
      ANNO_VARIABLE,
      ANNO_TAG,
      ANNO_NUM_ANNOTATIONS
    };
  };

  /**
   *  A constraint annotation is reference to a constraint.
   *  Using my_grammar::ANNO_TYPE
   */
  typedef ConstrainedTypePtr ConstraintAnnotation;
  typedef TypePtr TypeAnnotation;
  typedef PatternPtr PatternAnnotation;
  typedef ConstantPtr ConstAnnotation;
  typedef NamePtr NameAnnotation;
  typedef VariablePtr VariableAnnotation;

  void setTagAnnotation(idioma::ast::Node::Ptr node, const ::std::string &tag);
  ::std::optional<::std::string> getTagAnnotation(const idioma::ast::Node::CPtr &node);
  bool hasTagAnnotation(const idioma::ast::Node::CPtr &node, const ::std::string &tag);

  void setConstraintAnnotation(idioma::ast::Node::Ptr node, const ConstraintAnnotation &annotation);

  ConstraintAnnotation getConstraintAnnotation(const idioma::ast::Node::CPtr &node);

  void setTypeAnnotation(idioma::ast::Node::Ptr node, const TypeAnnotation &annotation);

  TypeAnnotation getTypeAnnotation(const idioma::ast::Node::CPtr &node);

  void setPatternAnnotation(idioma::ast::Node::Ptr node, const PatternAnnotation &annotation);

  PatternAnnotation getPatternAnnotation(const idioma::ast::Node::CPtr &node);

  void setConstAnnotation(idioma::ast::Node::Ptr node, const ConstAnnotation &annotation);

  ConstAnnotation getConstAnnotation(const idioma::ast::Node::CPtr &node);

  void setNameAnnotation(idioma::ast::Node::Ptr node, const NameAnnotation &annotation);

  NameAnnotation getNameAnnotation(const idioma::ast::Node::CPtr &node);

  void setVariableAnnotation(idioma::ast::Node::Ptr node, const VariableAnnotation &annotation);

  VariableAnnotation getVariableAnnotation(const idioma::ast::Node::CPtr &node);

  /**
   * Copy all annotations from one node to another
   * @param idioma::ast::Node::Ptr src
   * @param idioma::ast::Node::Ptr dest
   */
  void copyAnnotations(idioma::ast::Node::CPtr src, idioma::ast::Node::Ptr dest);
}
#endif
