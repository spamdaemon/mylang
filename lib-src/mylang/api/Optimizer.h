#ifndef CLASS_MYLANG_API_OPTIMIZER_H
#define CLASS_MYLANG_API_OPTIMIZER_H

#ifndef CLASS_MYLANG_ETHIR_IR_PROGRAM_H
#include <mylang/ethir/ir/Program.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#include <mylang/ethir/types/Type.h>
#endif

#include <filesystem>
#include <functional>
#include <memory>
#include <set>

namespace mylang::api {

  /**
   * The optimizer.
   */
  class Optimizer
  {
    /** A program pointer */
  public:
    using ProgramPtr = ::mylang::ethir::ir::Program::ProgramPtr;

    /**
     * A logger for debugging passes.
     * @param before the program before applying the pass (can be nullptr)
     * @param after the program after applying the pass (can be nullptr)
     * @param pass the name of the pass
     */
  public:
    typedef ::std::function<
        void(const ProgramPtr &before, const ProgramPtr &after, const ::std::string &pass)> PassLogger;

    /**
     * Converter from a type a native type
     * @param ty a type
     * @return a type that matches a builtin type
     */
  public:
    typedef ::std::function<
        mylang::ethir::types::Type::Ptr(const mylang::ethir::types::Type::Ptr &ty)> TypeConverter;

    /** The optimizer configuration */
  public:
    struct Options
    {
      enum Option
      {
        CSE,
        CONST_FOLDING,
        DERIVE_LITERALS,
        INLINE,
        LOOP_FUSION,
        PEEPHOLE,
        PRUNE_UNREACHABLE,
        PRUNE_UNUSED,
        REWRITE_BUILTINS,
        SHADOW_TYPING,
      };

      /**
       * Default constructor.
       * The default is to not enable any optimizations.
       */
      Options();

      /**
       * Options for the specified optimization level.
       * @param level the optimization level (0 is no optimization)
       */
      Options(size_t level);

      /**
       * Test if an option is set
       * @param opt an option
       * @return true if the option is set, false otherwise
       */
      inline bool operator[](Option opt) const
      {
        return options.count(opt) != 0;
      }

      /**
       * Set the specified option to true.
       * @param opt an opt
       */
      inline void enable(Option opt)
      {
        options.insert(opt);
      }

      /**
       * Set the specified option to true.
       * @param opt an opt
       */
      inline void disable(Option opt)
      {
        options.erase(opt);
      }

      /** The options to set */
      ::std::set<Option> options;

      /** A type converter which if specified will optimize code to use native types.
       * This function computes a type that may be wider than a given type.
       */
      TypeConverter nativeTypeConverter;

      /**
       * An IR logger. This function is invoked after an optimization function is run on the
       * IR.
       * This used for debugging the optimizer.
       */
      PassLogger passLogger;

      /** The maximum number of optimization loops. This is for debugging. */
      ::std::optional<size_t> maxOptimizationLoops;
    };

    /**
     * Create a new parser.
     */
  private:
    Optimizer(Options &&opts);

    /**
     * Create a new parser.
     */
  private:
    Optimizer();

    /**
     * Destructor
     */
  public:
    virtual ~Optimizer() = 0;

    /**
     * Create a parser.
     * @return parser
     */
  public:
    static ::std::unique_ptr<Optimizer> create(Options opts);

    /**
     * Optimize the specified program.
     * @param prg a program
     * @return an optimzed program
     */
  public:
    virtual ProgramPtr optimize(ProgramPtr prg) const;

    /**
     * Create a logger function
     * @param folder a folder where the logfile will be placed
     * @return a function that can be used as a passlogger
     */
  public:
    static PassLogger createPassLogger(const ::std::filesystem::path &path);

    /** The options */
  private:
    const Options options;
  };
}

#endif
