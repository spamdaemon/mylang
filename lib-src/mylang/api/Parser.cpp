#include <mylang/api/Parser.h>
#include <mylang/feedback.h>
#include <mylang/SourceLocation.h>
#include <mylang/grammar/PAst.h>
#include <mylang/validation/validation.h>
#include <mylang/vast/ValidationError.h>
#include <mylang/grammar/DefaultVisitor.h>
#include <mylang/vast/VAst.h>
#include <mylang/vast/VAstQuery.h>
#include <idioma/astutils/utils.h>

// the generated parser
#include <parser.h>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <set>
#include <vector>

namespace mylang::api {
  namespace {
    static void noFeedbackHandler(mylang::FeedbackType, const mylang::SourceLocation&,
        const ::std::string&)
    {

    }
    static void feedbackHandler(mylang::FeedbackType t, const mylang::SourceLocation &loc,
        const ::std::string &message)
    {
      ::std::ostream &out = ::std::cerr;
      loc.print(out);
      out << ':';

      switch (t) {
      case mylang::FeedbackType::ERROR:
        out << "error:";
        break;
      case mylang::FeedbackType::FIXME:
        out << "fixme:";
        break;
      case mylang::FeedbackType::WARNING:
        out << "warning:";
        break;
      case mylang::FeedbackType::INFO:
        out << "info:";
        break;
      case mylang::FeedbackType::DEBUG:
        out << "debug:";
        break;
      }
      out << message << ::std::endl;
    }

    static ::std::vector<::std::string> getName(const GroupNodeCPtr &node)
    {
      auto tokens = ::idioma::astutils::getTokens(node, mylang::grammar::PAst::REL_QN);
      if (tokens.empty()) {
        tokens = ::idioma::astutils::getTokens(node, mylang::grammar::PAst::TOK_IDENTIFIER);
      }
      ::std::vector<::std::string> res;
      res.reserve(tokens.size());
      for (const auto &tok : tokens) {
        res.push_back(tok->text);
      }
      return res;
    }

    static ::std::optional<Parser::Object> extractObject(GroupNodeCPtr root,
        const mylang::names::FQN &fqn, const Parser::Options &opts, const SourceLocation &loc)
    {
      struct ExtractVisitor: public mylang::grammar::DefaultVisitor
      {
        enum MatchType
        {
          NONE, FULL, PARTIAL
        };

        ExtractVisitor(const mylang::names::FQN &xfqn)
            : objectFQN(xfqn), error(false)
        {
          reverseFQN = objectFQN.segments();
          ::std::reverse(reverseFQN.begin(), reverseFQN.end());
        }

        ~ExtractVisitor()
        {
        }

        /**
         * Match the name components of the specified node.
         * @param node a node
         * @return true if the node's name was matched, false otherwise
         */
        MatchType matchNames(const GroupNodeCPtr &node)
        {
          if (error) {
            return NONE;
          }
          auto names = getName(node);
          if (names.empty()) {
            error = true;
            return NONE;
          }
          badName.insert(badName.end(), names.begin(), names.end());
          for (const auto &n : names) {
            if (reverseFQN.empty()) {
              return NONE;
            }
            if (n != reverseFQN.back().localName()) {
              return NONE;
            }
            reverseFQN.pop_back();
          }
          return reverseFQN.empty() ? FULL : PARTIAL;
        }

        bool isExported(const GroupNodeCPtr &node)
        {
          auto flag = ::idioma::astutils::getToken(node, mylang::grammar::PAst::REL_EXPORTED);
          return flag != nullptr;
        }

        GroupNodeCPtr visit_def(const GroupNodeCPtr &node, Parser::ObjectType ty)
        {
          if (error) {
            return node;
          }
          switch (matchNames(node)) {
          case FULL:
            objectType = ty;
            objectDefinition = node;
            return node;
          default:
            error = true;
            return nullptr;
          }
        }

        GroupNodeCPtr default_visit(const GroupNodeCPtr &node) override
        {
          return node;
        }

        GroupNodeCPtr visit_root(const GroupNodeCPtr &node) override
        {
          auto children = ::idioma::astutils::getGroups(node, mylang::grammar::PAst::REL_DECLS);
          if (children.size() == 1) {
            objectRoot = node;
            return visit(children.at(0));
          } else {
            error = true;
          }
          return node;
        }

        GroupNodeCPtr visit_def_namespace(const GroupNodeCPtr &node) override
        {
          switch (matchNames(node)) {
          case FULL:
            objectType = Parser::OBJECT_UNIT;
            objectDefinition = node;
            return node;
          case PARTIAL: {
            auto children = ::idioma::astutils::getGroups(node, mylang::grammar::PAst::REL_DECLS);
            if (children.size() == 1) {
              return visit(children.at(0));
            }
            error = true;
            return node;
          }
          default:
            error = true;
            return node;
          }
        }

        GroupNodeCPtr visit_def_function(const GroupNodeCPtr &node) override
        {
          if (!error) {
            visit_def(node, Parser::OBJECT_FUNCTION);
            error = !isExported(node);
          }
          return node;
        }

        GroupNodeCPtr visit_val_stmt(const GroupNodeCPtr &node) override
        {
          return visit_def(node, Parser::OBJECT_VALUE);
        }

        GroupNodeCPtr visit_def_type(const GroupNodeCPtr &node) override
        {
          return visit_def(node, Parser::OBJECT_TYPE);
        }

        GroupNodeCPtr visit_def_transform(const GroupNodeCPtr &node) override
        {
          return visit_def(node, Parser::OBJECT_TRANSFORM);
        }

        GroupNodeCPtr visit_def_process(const GroupNodeCPtr &node) override
        {
          return visit_def(node, Parser::OBJECT_PROCESS);
        }

        ::std::vector<mylang::names::FQN> reverseFQN;
        const mylang::names::FQN &objectFQN;
        GroupNodeCPtr objectRoot;
        GroupNodeCPtr objectDefinition;
        ::std::optional<Parser::ObjectType> objectType;
        // if there was a mismatch, this is the name we matched
        ::std::vector<::std::string> badName;
        bool error;
      };

      ExtractVisitor v(fqn);
      v.visit(root);
      if (v.objectType.has_value() && !v.error) {
        return { {v.objectRoot,v.objectDefinition,*v.objectType, v.objectFQN}};
      }
      if (opts.feedback) {
        if (v.badName.empty() == false) {
          opts.feedback(FeedbackType::ERROR, loc,
              "Expected " + fqn.fullName() + ", but found "
                  + mylang::names::FQN::create(v.badName).fullName());
        } else {
          opts.feedback(mylang::FeedbackType::ERROR, loc,
              ::std::string("Failed to find a valid object; expected " + fqn.fullName()));
        }
      }
      return ::std::nullopt;
    }

    static GroupNodeCPtr parseAst(::std::istream &in,
        const ::std::optional<::std::string> &identifier, Parser::Options opts)
    {
      if (!opts.feedback) {
        opts.feedback = noFeedbackHandler;
      }
      try {
        auto parser = mylang::grammar::parser::createParser();
        auto parsedAst = parser->parseStream(in, identifier.value_or(""));

        if (parsedAst == nullptr) {
          return nullptr;
        }

        // we need to remove ignorable, otherwise we have too many checks to do in the validation.
        auto parsed_ast = parsedAst->toTree()->removeIgnorables();

        auto ast = ::std::dynamic_pointer_cast<const idioma::ast::GroupNode>(parsed_ast);
        if (ast == nullptr) {
          opts.feedback(FeedbackType::ERROR, SourceLocation(), "Internal error: not a group node");
          return nullptr;
        }
        return ast;
      } catch (const ::mylang::vast::ValidationError &error) {
        opts.feedback(FeedbackType::ERROR, error.source, error.message);
      } catch (const ::std::exception &error) {
        opts.feedback(FeedbackType::ERROR, SourceLocation(),
            ::std::string("Internal error: uncaught exception: ") + error.what());
      }
      return nullptr;
    }

    static GroupNodeCPtr parseAst(const ::std::filesystem::path &path, Parser::Options opts)
    {
      ::std::ifstream in(path);
      if (!in.is_open()) {
        if (opts.feedback) {
          opts.feedback(FeedbackType::ERROR, SourceLocation(),
              "Failed to open file " + path.string());
        }
        return nullptr;
      }
      return parseAst(in, path.string(), ::std::move(opts));
    }

    static GroupNodeCPtr validateAst(GroupNodeCPtr ast, const ::std::optional<::std::string>&,
        Parser::Options opts)
    {
      if (!opts.feedback) {
        opts.feedback = noFeedbackHandler;
      }
      try {
        auto tree = mylang::validation::validateAST(ast, opts.feedback, opts.filesystem,
            opts.transforms);
        return tree;
      } catch (const ::mylang::vast::ValidationError &error) {
        opts.feedback(FeedbackType::ERROR, error.source, error.message);
      } catch (const ::std::exception &error) {
        opts.feedback(FeedbackType::ERROR, SourceLocation(),
            ::std::string("Internal error: uncaught exception: ") + error.what());
      }
      return nullptr;
    }
  }

  Parser::Options::Options()
      : feedback(feedbackHandler)
  {

  }

  Parser::Parser(const Options &opts)
      : options(opts)
  {
  }

  Parser::~Parser()
  {
  }

  bool Parser::lint(const ::std::filesystem::path &path)
  {
    auto opts = options;
    opts.transforms = nullptr;
    auto ast = parseAst(path, opts);
    if (ast == nullptr) {
      return false;
    }
    return validateAst(ast, path.string(), ::std::move(opts)) != nullptr;
  }

  bool Parser::lint(::std::istream &in, const ::std::optional<::std::string> &identifier)
  {
    auto opts = options;
    opts.transforms = nullptr;
    auto ast = parseAst(in, identifier, opts);
    if (ast == nullptr) {
      return false;
    }
    return validateAst(ast, identifier, ::std::move(opts)) != nullptr;
  }

  GroupNodeCPtr Parser::parse(const ::std::filesystem::path &path)
  {
    auto ast = parseAst(path, options);
    if (ast == nullptr) {
      return nullptr;
    }
    return validateAst(ast, path.string(), options);
  }

  GroupNodeCPtr Parser::parse(::std::istream &in, const ::std::optional<::std::string> &identifier)
  {
    auto ast = parseAst(in, identifier, options);
    if (ast == nullptr) {
      return nullptr;
    }
    return validateAst(ast, identifier, options);
  }

  GroupNodeCPtr Parser::parseOnly(const ::std::filesystem::path &path)
  {
    return parseAst(path, options);
  }

  GroupNodeCPtr Parser::parseOnly(::std::istream &in,
      const ::std::optional<::std::string> &identifier)
  {
    return parseAst(in, identifier, options);
  }

  ::std::optional<Parser::Object> Parser::parseObject(const ::std::filesystem::path &path,
      const mylang::names::FQN &fqn)
  {
    if (auto ast = parseOnly(path); ast) {
      if (auto obj = extractObject(ast, fqn, options, { path }); obj) {
        auto opts = options;
        opts.transforms = nullptr;
        auto validated = validateAst(obj->root, path.string(), ::std::move(opts));
        if (validated) {
          auto def = mylang::vast::VAst::findDefinition(validated, fqn);
          if (!def) {
            throw ::std::runtime_error("internal error: failed to find a definition");
          }

          // find the top-level object with the given fqn
          return { {validated, def, obj->type,obj->fqn}};
        }
      }
    }
    return ::std::nullopt;
  }

  ::std::optional<Parser::Object> Parser::parseObject(const mylang::names::FQN &fqn)
  {
    ::std::set<::mylang::filesystem::FileSystem::Ptr> fset;
    fset.insert(options.filesystem);
    if (options.transforms) {
      fset.insert(options.transforms->getFileSystem());
    }
    auto fs = ::mylang::filesystem::FileSystem::unionOf(fset);

    ::mylang::filesystem::FileSystem::Details details;
    auto stream = fs->open(fqn, &details);
    if (!stream) {
      return ::std::nullopt;
    }
    return parseObject(*stream, fqn, details.identifier);
  }

  ::std::optional<Parser::Object> Parser::parseObject(::std::istream &stream,
      const mylang::names::FQN &fqn, const ::std::optional<::std::string> &xidentifier)
  {
    auto identifier = xidentifier.value_or(fqn.fullName());
    if (auto ast = parseOnly(stream, identifier); ast) {
      if (auto obj = extractObject(ast, fqn, options, { identifier }); obj) {
        auto opts = options;
        opts.transforms = nullptr;
        auto validated = validateAst(obj->root, identifier, ::std::move(opts));
        if (validated) {
          auto def = mylang::vast::VAst::findDefinition(validated, fqn);
          if (!def) {
            throw ::std::runtime_error("internal error: failed to find a definition");
          }

          // find the top-level object with the given fqn
          return { {validated, def, obj->type,obj->fqn}};
        }
      } else {
        // failed to validate
      }
    }
    return ::std::nullopt;
  }

  ::std::unique_ptr<Parser> Parser::create(Options opts)
  {
    struct Impl: public Parser
    {
      Impl(const Options &opts)
          : Parser(opts)
      {
      }
      ~Impl()
      {
      }
    };

    return ::std::make_unique<Impl>(opts);
  }

}
