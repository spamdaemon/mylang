#include <mylang/api/Optimizer.h>
#include <mylang/ethir/analysis/TypeAnalysis.h>
#include <mylang/ethir/ssa/SSAProgram.h>
#include <mylang/ethir/ssa/SSAPass.h>
#include <mylang/ethir/ssa/PrunePass.h>
#include <mylang/ethir/ssa/PruneUnreachableCode.h>
#include <mylang/ethir/ssa/ConstFoldPass.h>
#include <mylang/ethir/ssa/ShadowTypingPass.h>
#include <mylang/ethir/ssa/MaintainSSAPass.h>
#include <mylang/ethir/ssa/CSEPass.h>
#include <mylang/ethir/ssa/SimplifyPass.h>
#include <mylang/ethir/ssa/DeriveLiterals.h>
#include <mylang/ethir/ssa/peepholes/peepholes.h>
#include <mylang/ethir/analysis/TypeAnalysis.h>
#include <mylang/ethir/transforms/NormalizeFunction.h>
#include <mylang/ethir/transforms/RewriteBuiltins.h>
#include <mylang/ethir/transforms/NativeTyping.h>
#include <mylang/ethir/transforms/ToLoops.h>
#include <mylang/ethir/transforms/PruneGlobals.h>
#include <mylang/ethir/transforms/PruneProcessMembers.h>
#include <mylang/ethir/transforms/LoopFusion.h>
#include <mylang/ethir/transforms/InlineFunctions.h>

// debugging support
#include <cassert>
#include <sstream>
#include <fstream>
#include <filesystem>
#include <mylang/ethir/prettyPrint.h>

namespace mylang::api {
  namespace {

    using namespace mylang::ethir;

    static ssa::SimplifyPass::Peepholes getPeepholes(const Optimizer::Options &opts)
    {
      if (opts[Optimizer::Options::PEEPHOLE] == false) {
        return {};
      }

      return ssa::SimplifyPass::Peepholes( { ssa::Peephole::createMapIdentity(),
          ssa::Peephole::createCallWrapper(), ssa::peepholes::BitLikeOps::getPeephole(),
          ssa::peepholes::Arrays::getReversePeephole(), ssa::peepholes::Arrays::getPeephole(),
          ssa::peepholes::Optionals::getPeephole(), ssa::peepholes::Structs::getPeephole(),
          ssa::peepholes::Unions::getPeephole(), ssa::peepholes::Tuples::getPeephole() });
    }

    static bool pruneWellKnownFunctions(ssa::SSAProgram &prg)
    {
      auto f = [](
          mylang::ethir::EGlobal g) {return g->kind == mylang::ethir::ir::GlobalValue::EXPORTED;};
      auto pass1 = ::std::make_unique<ssa::PrunePass>(f);
      auto pass2 = ::std::make_unique<ssa::PruneUnreachableCode>();

      bool retVal = false;
      bool continuePrune = true;
      while (continuePrune) {
        continuePrune = pass1->transformProgram(prg);
        continuePrune = continuePrune || pass2->transformProgram(prg);
        retVal = retVal || continuePrune;
      }
      return retVal;
    }

    ::std::filesystem::path makeTraceFile(size_t i, ::std::optional<::std::filesystem::path> folder,
        const ::std::string_view &prefix, const ::std::string &name)
    {
      ::std::ostringstream out;
      if (folder.has_value()) {
        out << folder.value().string() << '/';
      }
      out.width(4);
      out.fill('0');
      out << i << "--" << prefix << "--" << name;
      return out.str();
    }

    static void emitIR(const EProgram &prg, const ::std::string_view &prefix, size_t count,
        const ::std::string_view &pass, ::std::optional<size_t> suffix,
        const std::filesystem::path &folder)
    {
      if (!prg) {
        return;
      }
      ::std::string passname("-");
      passname += pass;
      if (suffix.has_value()) {
        passname += '-';
        passname += ::std::to_string(suffix.value());
      }
      passname += ".ir";
      auto path = makeTraceFile(count, folder, prefix, passname);
      auto dir = path.parent_path();
      ::std::filesystem::create_directories(dir);
      ::std::ofstream fout(path);
      mylang::ethir::prettyPrint(prg, fout);
    }

    struct Pass
    {
      Pass(const ::std::string_view &xname)
          : name(xname)
      {
        ::std::cerr << "PASS " << name << ' ';
      }

      ~Pass()
      {
        ::std::cerr << ::std::endl;
      }

      const ::std::string_view &name;
    };

    // TODO: the fact we have all these different onePass functions means we don't have
    // a consistent interface for transformations

    static EProgram onePass(const Optimizer::Options &opts, EProgram prg,
        const ::std::string_view &name, ::std::function<EProgram(EProgram)> f)
    {
      Pass pass(name);
      auto next = f(prg);
      auto newPrg = next ? next : prg;
      if (opts.passLogger) {
        opts.passLogger(prg, newPrg, ::std::string(name));
      }
      return newPrg;
    }

    static bool onePass(const Optimizer::Options &opts, ssa::SSAProgram &prg,
        const ::std::string_view &name, ::std::function<bool(ssa::SSAProgram&)> f)
    {
      Pass pass(name);
      bool res = false;
      if (opts.passLogger) {
        auto before = prg.getSSAForm();
        res = f(prg);
        auto after = prg.getSSAForm();
        opts.passLogger(before, after, ::std::string(name));
      } else {
        res = f(prg);
      }
      return res;
    }

    static EProgram onePass(const Optimizer::Options &opts, EProgram prg,
        const ::std::string_view &name, ::std::function<ENode(ENode)> f)
    {
      return onePass(opts, prg, name, [&](EProgram p) {
        EProgram res;
        auto next = f(p);
        if (next) {
          res = next->self<ir::Program>();
        }
        if (res == nullptr) {
          res = prg;
        }
        return res;
      });
    }

    static bool onePass(const Optimizer::Options &opts, ssa::SSAProgram &prg,
        const ::std::string_view &name, ssa::SSAPass &f)
    {
      return onePass(opts, prg, name, [&](ssa::SSAProgram &p) {
        return f.transformProgram(p);
      });
    }

    static EProgram onePass(const Optimizer::Options &opts, EProgram prg,
        const ::std::string_view &name, ::std::function<TransformResult<EProgram>(EProgram)> f)
    {
      return onePass(opts, prg, name, [&](EProgram p) {
        auto c = f(p);
        return c.actual;
      });
    }

    static EProgram onePass(const Optimizer::Options &opts, EProgram prg,
        const ::std::string_view &name, ::std::function<TransformResult<ENode>(ENode)> f)
    {
      return onePass(opts, prg, name, [&](ENode p) {
        auto c = f(p);
        return c.actual;
      });
    }

    static EProgram onePass(const Optimizer::Options &opts, EProgram prg,
        const ::std::string_view &name, Transform &f)
    {
      return onePass(opts, prg, name, [&](EProgram p) {
        EProgram res=p;
        auto c = f.apply(p);
        if (c) {
          res= c->self<ir::Program>();
        }
        return res;
      });
    }

    static EProgram withSSAForm(const Optimizer::Options &opts, EProgram prg,
        ::std::function<bool(ssa::SSAProgram&, const Optimizer::Options&)> f)
    {
      auto ssaForm = ssa::SSAProgram::createSSA(prg);

      auto after = prg;
      if (opts.passLogger) {
        opts.passLogger(prg, ssaForm->getSSAForm(), "to-SSA");
      }

      if (f(*ssaForm, opts)) {
        after = ssaForm->getNonSSAForm();
      }
      if (opts.passLogger) {
        opts.passLogger(ssaForm->getSSAForm(), after, "from-SSA");
      }
      return after;
    }
  }

  Optimizer::Options::Options(size_t level)
  {
    // enable additional options at the given level
    switch (level) {
    default:
      // fall-through
    case 3:
      enable(SHADOW_TYPING);
      // fall-through
    case 2:
      enable(INLINE);
      enable(LOOP_FUSION);
      // fall-through
    case 1:
      enable(CONST_FOLDING);
      enable(PRUNE_UNREACHABLE);
      enable(PRUNE_UNUSED);
      enable(REWRITE_BUILTINS);
      enable(CSE);
      // fall-through
    case 0:
      // no change to options
      break;
    }
  }

  Optimizer::Options::Options()
  {

  }

  Optimizer::Optimizer()
  {
  }

  Optimizer::Optimizer(Options &&opts)
      : options(::std::move(opts))
  {
  }

  Optimizer::~Optimizer()
  {
  }

  Optimizer::ProgramPtr Optimizer::optimize(ProgramPtr prg) const
  {
    transforms::RewriteBuiltins rewriteBuiltins;
    mylang::ethir::transforms::InlineFunctions inlineFunctions(
        [](const mylang::ethir::EVariable &v, const mylang::ethir::ELambda &l) {
          return mylang::ethir::transforms::InlineFunctions::isSimpleFunction(v,l,200);
        });
    mylang::ethir::transforms::ToLoops collectToArrayLoops;
    auto peepholePass = ::std::make_unique<ssa::SimplifyPass>(getPeepholes(options));

    // the main optimization loop
    EProgram cur = prg;
    for (size_t i = 0;; ++i) {
      if (options.maxOptimizationLoops.has_value()) {
        if (i >= options.maxOptimizationLoops.value()) {
          ::std::cerr << "WARNING: too many optimization loops" << ::std::endl;
          break;
        }
      }
      prg = cur;

      ::std::cerr << "Top of the loop " << prg << ::std::endl;

      if (options[Optimizer::Options::PRUNE_UNREACHABLE]) {
//        cur = onePass(cur, "prune-globals", mylang::ethir::transforms::PruneGlobals::transform);
        cur = onePass(options, cur, "prune-process-members",
            mylang::ethir::transforms::PruneProcessMembers::transform);
      }

      // perform some optimization in SSA form
      cur =
          withSSAForm(options, cur,
              [&](ssa::SSAProgram &p, const Options &opts) {
                bool modified=false;
//                    if (opts[Optimizer::Options::PRUNE_UNREACHABLE]) {
//                      modified |= onePass(opts,p,"ssa-prune-unreachable",*::std::make_unique<ssa::PruneUnreachableCode>());
//                    };
//                    if (opts[Optimizer::Options::PRUNE_UNUSED]) {
//                      modified |= onePass(opts,p,"ssa-prune-unused",*::std::make_unique<ssa::PrunePass>());
//                    }
//                    if (modified) {
//                      onePass(opts,p,"ssa-maintain-ssa",*::std::make_unique<ssa::MaintainSSAPass>());
//                    }

                  if (opts[Optimizer::Options::CONST_FOLDING]) {
                    modified |= onePass(opts,p,"ssa-const-folding",*::std::make_unique<ssa::ConstFoldPass>());
                  }
                  if (opts[Optimizer::Options::PRUNE_UNREACHABLE]) {
                    modified |= onePass(opts,p,"ssa-prune-unreachable",*::std::make_unique<ssa::PruneUnreachableCode>());
                  };
                  if (opts[Optimizer::Options::PRUNE_UNUSED]) {
                    if (onePass(opts,p,"ssa-prune-unused",*::std::make_unique<ssa::PrunePass>())) {
                      // TODO:maybe PrunePass should indicate that it needs cleanup of SSA form
                      // or we we add a PhiNode cleanup pass
                      onePass(opts,p,"ssa-maintain-ssa",*::std::make_unique<ssa::MaintainSSAPass>());
                      modified=true;
                    }
                  }
                  if (opts[Optimizer::Options::DERIVE_LITERALS]) {
                    modified |= onePass(opts,p,"ssa-derive-literals",*::std::make_unique<ssa::DeriveLiterals>());
                  }
                  if (opts[Optimizer::Options::PEEPHOLE]) {
                    modified |= onePass(opts,p,"ssa-peephole",*peepholePass);
                  }
                  // return early
                  if (modified) {return true;}

                  if (opts[Optimizer::Options::CSE]) {
                    modified |= onePass(opts,p,"ssa-cse",*::std::make_unique<ssa::CSEPass>());
                  }
                  if (opts[Optimizer::Options::SHADOW_TYPING]) {
                    modified |= onePass(opts,p,"ssa-shadow-typing",*::std::make_unique<ssa::ShadowTypingPass>());
                  }
                  return modified;
                });

      // we've done a whole bunch of optimizations already, so let's iterate
      if (cur != prg) {
        continue;
      }

      //
      if (options[Optimizer::Options::INLINE]) {

#if 0 // TODO: why is this off?
        cur = onePass(cur, "normalize-functions", [&](ENode p) {
          mylang::ethir::transforms::NormalizeFunction normalizeFunctions;
          return normalizeFunctions.normalizeFunctions(p);});
#endif

        cur = onePass(options, cur, "inline-functions",
            [&](EProgram p) {return inlineFunctions.apply(p).actual;});

        if (cur != prg) {
          // if we've inlined functions, we can do another round of simple optimizations
          continue;
        }
      }

      // rewrite the builtins and continue; this will very quickly
      // introduce new functions and so produce new opportunities for
      // optimizations
      cur = onePass(options, cur, "rewrite-builtins", rewriteBuiltins);
      if (cur != prg) {
        continue;
      }

      if (options[Optimizer::Options::LOOP_FUSION]) {
        cur = onePass(options, cur, "loop-fusion", transforms::LoopFusion::fuseLoops);
        // go back and start over
        if (cur != prg) {
          continue;
        }
      }

      cur = onePass(options, cur, "rewrite-builtins", rewriteBuiltins);
      if (cur != prg) {
        continue;
      }

      cur = onePass(options, cur, "collect-to-array", collectToArrayLoops);
      if (cur != prg) {
        continue;
      }

      if (options.nativeTypeConverter) {
        ::mylang::ethir::analysis::TypeAnalysis::get(cur);

        cur = onePass(options, cur, "native-typing", [&](EProgram p) {
          transforms::NativeTyping tx(options.nativeTypeConverter);
          return tx.transform(p);
        });
        // don't continue optimizing
        break;
      }

      // at this point, if the we've not made any changes, then we're done
      if (cur == prg) {
        break;
      }
    }

    // before we're finishing, we want to cleanup some extra code that may have been added in
    // this normally happens we use rewriteBuiltins
    cur = withSSAForm(options, cur, [&](ssa::SSAProgram p, const Options &opts) {
      return onePass(opts,p,"cleanup-extra-code",pruneWellKnownFunctions);
    });

    return cur;
  }

  ::std::unique_ptr<Optimizer> Optimizer::create(Options opts)
  {
    struct Impl: public Optimizer
    {
      Impl(Options opts)
          : Optimizer(::std::move(opts))
      {
      }
      ~Impl()
      {
      }
    };

    if (opts.options.empty()) {
      struct NoOptimizer: public Optimizer
      {
        NoOptimizer()
        {
        }
        ~NoOptimizer()
        {
        }
        ProgramPtr optimize(ProgramPtr prg) const
        {
          return prg;
        }
      };
      return ::std::make_unique<NoOptimizer>();
    }

    return ::std::make_unique<Impl>(::std::move(opts));
  }

  Optimizer::PassLogger Optimizer::createPassLogger(const ::std::filesystem::path &path)
  {
    struct XPassLogger
    {
      XPassLogger(::std::filesystem::path xpath)
          : path(::std::move(xpath)), count(0)
      {
      }
      ~XPassLogger() = default;

      void operator()(const ProgramPtr &before, const ProgramPtr &after, const ::std::string &pass)
      {
        if (before != after) {
          auto prefix = ++count;
          emitIR(before, "debug", prefix, pass, 0, path);
          emitIR(after, "debug", prefix, pass, 1, path);
        }
      }

      const ::std::filesystem::path path;
      size_t count;
    };

    return PassLogger(XPassLogger(path));
  }

}
