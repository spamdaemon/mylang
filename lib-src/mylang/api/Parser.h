#ifndef CLASS_MYLANG_API_PARSER_H
#define CLASS_MYLANG_API_PARSER_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_NAMES_FQN_H
#include <mylang/names/FQN.h>
#endif

#ifndef CLASS_MYLANG_FILESYSTEM_FILESYSTEM_H
#include <mylang/filesystem/FileSystem.h>
#endif

#ifndef CLASS_MYLANG_TRANSFORMS_TRANSFORMBASE_H
#include <mylang/transforms/TransformBase.h>
#endif

#ifndef FILE_MYLANG_FEEDBACK_H
#include <mylang/feedback.h>
#endif

#include <iosfwd>
#include <filesystem>
#include <memory>
#include <optional>
#include <string>

namespace mylang::api {

  /**
   * The parser.
   */
  class Parser
  {

    /** The parser configuration */
  public:
    struct Options
    {
      Options();

      /**
       * The repo location for resolving transforms.
       * Defaults to nullptr
       */
      mylang::transforms::TransformBase::Ptr transforms;

      /**
       * The search path for imports.
       * Defaults to nullptr
       */
      ::std::shared_ptr<mylang::filesystem::FileSystem> filesystem;

      /**
       * An optional error handler.
       * If this is not set, then the Parser
       * will use a default handler that prints to stderr.
       * Defaults to a nullptr
       */
      mylang::FeedbackHandler feedback;
    };

    /**
     * The type of an object.
     */
    enum ObjectType
    {
      OBJECT_TRANSFORM, OBJECT_FUNCTION, OBJECT_TYPE, OBJECT_PROCESS, OBJECT_UNIT, OBJECT_VALUE
    };

    /**
     * An object a file with what is effectively single top-level definition.
     */
    struct Object
    {
      /** The root of the AST */
      GroupNodeCPtr root;

      /** The definition node */
      GroupNodeCPtr definition;

      /** The type of object */
      ObjectType type;

      /** The FQN of the object */
      mylang::names::FQN fqn;
    };

    /**
     * Create a new parser.
     */
  private:
    Parser(const Options &opts);

    /**
     * Destructor
     */
  public:
    virtual ~Parser() = 0;

    /**
     * Create a parser.
     * @return parser
     */
  public:
    static ::std::unique_ptr<Parser> create(Options opts);

    /**
     * Lint a file.
     * @param path a file
     * @return true if the file was parsed and type-checked
     */
  public:
    bool lint(const ::std::filesystem::path &path);

    /**
     * Lint the specified stream.
     * An identifier can be provided to identify the stream in any feedback messages.
     *
     * @param stream a stream
     * @param identifier an identifier that will be used to identify the stream
     * @return true if the file was parsed and type-checked
     */
  public:
    bool lint(::std::istream &stream, const ::std::optional<::std::string> &identifier);

    /**
     * Parse a file.
     * @param path a file
     * @return a pointer to the AST, or nullptr on error
     */
  public:
    GroupNodeCPtr parse(const ::std::filesystem::path &path);

    /**
     * Parse the specified stream.
     * An identifier can be provided to identify the stream in any feedback messages.
     *
     * @param stream a stream
     * @param identifier an identifier that will be used to identify the stream
     * @return a pointer to the AST, or nullptr on error
     */
  public:
    GroupNodeCPtr parse(::std::istream &stream, const ::std::optional<::std::string> &identifier =
        ::std::nullopt);

    /**
     * Parse a file, but do not validate.
     * @param path a file
     * @return a pointer to the AST, or nullptr on error
     */
  public:
    GroupNodeCPtr parseOnly(const ::std::filesystem::path &path);

    /**
     * Parse the specified stream, but do not validate.
     * An identifier can be provided to identify the stream in any feedback messages.
     *
     * @param stream a stream
     * @param identifier an identifier that will be used to identify the stream
     * @return a pointer to the AST, or nullptr on error
     */
  public:
    GroupNodeCPtr parseOnly(::std::istream &stream,
        const ::std::optional<::std::string> &identifier = ::std::nullopt);

    /**
     * Parse an object with an expected FQN.
     * @param path a file path
     * @param fqn the expected FQN of the object
     * @return an object or empty on error
     */
  public:
    ::std::optional<Object> parseObject(const ::std::filesystem::path &path,
        const mylang::names::FQN &fqn);

    /**
     * Parse an object.
     * @param stream an input stream
     * @param fqn the expected FQN of the object
     * @return an object or empty on error
     */
  public:
    ::std::optional<Object> parseObject(::std::istream &stream, const mylang::names::FQN &fqn,
        const ::std::optional<::std::string> &identifier);

    /**
     * Parse an object with an expected FQN using the configured filesystems.
     * @param fqn the expected FQN of the object
     * @return an object or empty on error
     */
  public:
    ::std::optional<Object> parseObject(const mylang::names::FQN &fqn);

    /** The options */
  private:
    const Options options;
  };
}

#endif
