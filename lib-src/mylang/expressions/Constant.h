#ifndef CLASS_MYLANG_EXPRESSIONS_CONSTANT_H
#define CLASS_MYLANG_EXPRESSIONS_CONSTANT_H

#ifndef CLASS_MYLANG_EXPRESSIONS_EXPRESSION_H
#include <mylang/expressions/Expression.h>
#endif

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif
#include <string>

namespace mylang {
  namespace expressions {

    /** A constant value */
    class Constant: public Expression
    {
      /** A pointer to a constant */
    public:
      typedef ::std::shared_ptr<const Constant> Ptr;

      /* Constructor */
    protected:
      Constant();

      /** Destructor */
    public:
      virtual ~Constant() = 0;

      /**
       * Cast this constant to a different type.
       * @param toType the type to which to cast
       * @return a new constant or nullptr if cast cannot be performed
       */
    public:
      virtual Ptr cast(ConstrainedTypePtr toType) const;

      /**
       * Negate the value of this constant. If negation is not possible, then
       * nullptr is returned
       * @return a new constant after negating the value
       */
    public:
      virtual Ptr negate() const;

      /**
       * Get a simple constant
       * @param type the type of the constant
       * @param value the string value
       */
    public:
      static Ptr get(const TypePtr &type, const ::std::string &value);

      /**
       * Get a simple constant
       * @param type the type of the constant
       * @param value the string value
       */
    private:
      static Ptr get(const ConstrainedTypePtr &type, const ::std::string &value);

      /**
       * Get a simple integer constant
       * @param value the string value
       */
    public:
      static Ptr get(const BigInt &value);

      /**
       * Get a simple real constant
       * @param value the string value
       */
    public:
      static Ptr get(const BigReal &value);

      /**
       * Test if the constants have the same value
       * @param c a constant
       * @return true if c and this constant are the same
       */
    public:
      bool isSameConstant(const Constant &c) const;

      /**
       * Get the string value
       * @return the string value for this constant
       */
    public:
      virtual ::std::string value() const = 0;

      /**
       * Get a a double
       * @param res the result
       * @return true if the result contains a valid value
       */
    public:
      bool parse(BigReal &res) const;

      /**
       * Get an integer
       * @param res the result
       * @return true if the result contains a valid value
       */
    public:
      bool parse(BigUInt &res) const;

      /**
       * Get an integer
       * @param res the result
       * @return true if the result contains a valid value
       */
    public:
      bool parse(BigInt &res) const;

      /**
       * Get a a double.
       * FIXME: we need to return a range here, because that's what we'll get in most cases.
       * @param res the result
       * @return true if the result contains a valid value
       * @throws ::std::runtime_error the constant does not have real type
       */
    public:
      BigReal getReal() const;

      /**
       * Get an integer
       * @return true if the result contains a valid value
       * @throws ::std::runtime_error the constant does not have integer type
       */
    public:
      BigUInt getUnsignedInteger() const;

      /**
       * Get an integer
       * @return a signed integer
       * @throws ::std::runtime_error the constant does not have integer type
       */
    public:
      BigInt getSignedInteger() const;

      /**
       * Get an bit
       * @return true if value == 0b1, false otherwise
       * @throws ::std::runtime_error the constant does not have integer type
       */
    public:
      bool getBit() const;

      /**
       * Get an byte value
       * @return the unsiggned char value
       * @throws ::std::runtime_error the constant does not have integer type
       */
    public:
      unsigned char getByte() const;

    };
  }
}
#endif
