#ifndef CLASS_MYLANG_EXPRESSIONS_EXPRESSION_H
#define CLASS_MYLANG_EXPRESSIONS_EXPRESSION_H
#include <mylang/defs.h>
#include <memory>

namespace mylang {
  namespace expressions {

    /** An abstract class for an expression */
    class Expression
    {
      /** A pointer to a constant */
    public:
      typedef ::std::shared_ptr<const Expression> Ptr;

      /* Constructor */
    protected:
      Expression();

      /** Destructor */
    public:
      virtual ~Expression() = 0;

      /**
       * Get the type of this expression.
       * @return the type of this expression
       */
    public:
      virtual TypePtr type() const = 0;

      /**
       * Get the constraint type.
       * @return the constraint type for this constant
       */
    public:
      virtual ConstrainedTypePtr constraint() const;
    };
  }
}
#endif
