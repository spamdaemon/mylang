#include <mylang/expressions/Constant.h>
#include <mylang/types/IntegerType.h>
#include <mylang/types/RealType.h>
#include <mylang/constraints/ConstrainedIntegerType.h>
#include <mylang/constraints/ConstrainedRealType.h>
#include <cassert>
#include <cstring>
#include <cstddef>
#include <memory>
#include <stdexcept>

namespace mylang {
  namespace expressions {

    Constant::Constant()
    {
    }

    Constant::~Constant()
    {
    }

    Constant::Ptr Constant::cast(ConstrainedTypePtr toType) const
    {
      Ptr res;
      if (toType->canSafeCastFrom(*constraint())) {
        res = get(toType, value());
      }
      return res;
    }

    Constant::Ptr Constant::negate() const
    {
      return nullptr;
    }

    bool Constant::isSameConstant(const Constant &c) const
    {
      return type()->isSameType(*c.type()) && value() == c.value();
    }

    Constant::Ptr Constant::get(const BigReal &value)
    {
      return get(mylang::types::RealType::create(), value.toString());

    }

    Constant::Ptr Constant::get(const BigInt &value)
    {
      return get(mylang::types::IntegerType::create(), value.toString());
    }

    Constant::Ptr Constant::get(const TypePtr &xtype, const ::std::string &xvalue)
    {
      ConstrainedTypePtr ty;
      if (xtype->self<mylang::types::IntegerType>()) {
        ty = mylang::constraints::ConstrainedIntegerType::create(BigInt::parse(xvalue));
      } else if (xtype->self<mylang::types::RealType>()) {
        // every real number is already range, so we need to do this differently
        // FIXME: once reals support constraints
        //return mylang::constraints::ConstrainedRealType::create(getReal());
        ty = mylang::constraints::ConstrainedRealType::create();
      } else {
        ty = mylang::constraints::ConstrainedType::createConstraints(xtype);
      }
      return get(ty, xvalue);
    }

    Constant::Ptr Constant::get(const ConstrainedTypePtr &xtype, const ::std::string &xvalue)
    {
      struct Impl: public Constant
      {
        Impl(const ConstrainedTypePtr &xtype, const ::std::string &xvalue)
            : the_type(xtype), the_value(xvalue)
        {
        }

        ~Impl()
        {
        }

        TypePtr type() const
        {
          return the_type->type();
        }

        ConstrainedTypePtr constraint() const
        {
          return the_type;
        }

        ::std::string value() const
        {
          return the_value;
        }

        Constant::Ptr negate() const
        {
          auto ity = the_type->self<mylang::constraints::ConstrainedIntegerType>();
          auto rty = the_type->self<mylang::constraints::ConstrainedRealType>();

          if (ity) {
            return Constant::get(ity->negate(), (-getSignedInteger()).toString());
          } else if (rty) {
            // FIXME: this may involve some kind of rounding!!
            return Constant::get(rty->negate(), (-getReal()).toString());
          }
          return nullptr;
        }

        const ConstrainedTypePtr the_type;
        const ::std::string the_value;
      };
      return ::std::make_shared<Impl>(xtype, xvalue);
    }

    bool Constant::parse(BigReal &res) const
    {
      try {
        res = BigReal::parse(value());
        return true;
      } catch (...) {
      }
      return false;
    }

    bool Constant::parse(BigUInt &res) const
    {
      try {
        res = BigUInt::parse(value());
        return true;
      } catch (...) {
      }
      return false;
    }

    bool Constant::parse(BigInt &res) const
    {
      try {
        res = BigInt::parse(value());
        return true;
      } catch (...) {
      }
      return false;
    }

    BigReal Constant::getReal() const
    {
      return BigReal::parse(value());
    }

    BigUInt Constant::getUnsignedInteger() const
    {
      return BigUInt::parse(value());
    }

    BigInt Constant::getSignedInteger() const
    {
      return BigInt::parse(value());
    }

    bool Constant::getBit() const
    {
      auto v = value();
      return v == "0b1";
    }

    unsigned char Constant::getByte() const
    {
      auto v = value();
      if (v.length() != 5) {
        throw ::std::runtime_error("No valid byte literal " + v);
      }
      if (v[0] != '0' || v[1] != 'b' || v[2] != 'x') {
        throw ::std::runtime_error("No valid byte literal " + v);
      }
      int n;
      if (1 != ::std::sscanf(v.c_str() + 3, "%x", &n)) {
        throw ::std::runtime_error("No valid byte literal " + v);
      }
      return (unsigned char) n;
    }

  }
}
