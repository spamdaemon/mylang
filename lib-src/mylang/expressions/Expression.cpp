#include <mylang/expressions/Expression.h>
#include <mylang/constraints/ConstrainedType.h>

namespace mylang {
  namespace expressions {

    Expression::Expression()
    {
    }

    Expression::~Expression()
    {
    }

    ConstrainedTypePtr Expression::constraint() const
    {
      return mylang::constraints::ConstrainedType::createConstraints(type());
    }
  }
}
