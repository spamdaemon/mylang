#include <mylang/feedback.h>
#include <sstream>

namespace mylang {

  FeedbackBase::FeedbackBase()
  {
  }

  FeedbackBase::FeedbackBase(const FeedbackBase &f)
      : _feedback(f._feedback)
  {
  }

  FeedbackBase::FeedbackBase(FeedbackHandler f)
      : _feedback(f)
  {
  }

  FeedbackBase::~FeedbackBase()
  {
  }

  void FeedbackBase::notifyInternalError(const SourceLocation &loc, ::std::ostream &message) const
  {
    if (&message == &stream && _feedback) {
      _feedback(FeedbackType::ERROR, loc, stream.str());
    }
    stream.str(::std::string());
  }

  void FeedbackBase::notifyError(const SourceLocation &loc, ::std::ostream &message) const
  {
    if (&message == &stream && _feedback) {
      _feedback(FeedbackType::ERROR, loc, stream.str());
    }
    stream.str(::std::string());
  }

  void FeedbackBase::notifyWarning(const SourceLocation &loc, ::std::ostream &message) const
  {
    if (&message == &stream && _feedback) {
      _feedback(FeedbackType::WARNING, loc, stream.str());
    }
    stream.str(::std::string());
  }

  void FeedbackBase::notifyInfo(const SourceLocation &loc, ::std::ostream &message) const
  {
    if (&message == &stream && _feedback) {
      _feedback(FeedbackType::INFO, loc, stream.str());
    }
    stream.str(::std::string());
  }

  void FeedbackBase::notifyDebug(const SourceLocation &loc, ::std::ostream &message) const
  {
    if (&message == &stream && _feedback) {
      _feedback(FeedbackType::DEBUG, loc, stream.str());
    }
    stream.str(::std::string());
  }

  void FeedbackBase::notifyFixme(const SourceLocation &loc, ::std::ostream &message) const
  {
    if (&message == &stream && _feedback) {
      _feedback(FeedbackType::FIXME, loc, stream.str());
    }
    stream.str(::std::string());
  }

}
