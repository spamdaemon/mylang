#include <mylang/io/Directory.h>

#if __cplusplus >= 201703L
#define HAVE_CPP_FILESYSTEM 1
#else
#define HAVE_CPP_FILESYSTEM 0
#endif

#if HAVE_CPP_FILESYSTEM
#include <filesystem>

#else
#include <sys/stat.h>
#include <errno.h>
#endif
#include <fstream>

namespace mylang {
  namespace io {

    namespace {
      static ::std::string create_directory(const ::std::string &xpath)
      {
#if HAVE_CPP_FILESYSTEM == 1
        ::std::filesystem::path p(::std::filesystem::path::string_type(xpath),
            ::std::filesystem::path::auto_format);
        ::std::filesystem::create_directory(p);
#else
                auto ok = ::mkdir(xpath.c_str(),S_IRUSR|S_IWUSR|S_IXUSR|S_IRGRP|S_IXGRP|S_IROTH|S_IXOTH);
                if (ok!=0 && errno != EEXIST) {
                    ::perror(("failed to create directory "+xpath).c_str());
                    throw ::std::runtime_error("failed to create directory "+xpath);
                }
#endif
        return xpath;
      }

      static ::std::string create_file(const ::std::string &xparent, const ::std::string &xfile)
      {
#if HAVE_CPP_FILESYSTEM == 1
        ::std::filesystem::path p(::std::filesystem::path::string_type(xparent),
            ::std::filesystem::path::auto_format);
        p.append(xfile);
        return p.generic_string();
#else
                return xparent +'/'+xfile;
#endif
      }

      static ::std::string create_sub_directory(const ::std::string &xparent,
          const ::std::string &xpath)
      {
        return create_directory(create_file(xparent, xpath));
      }

      static void set_executable(const ::std::string &xpath)
      {
#if HAVE_CPP_FILESYSTEM == 1
        ::std::filesystem::path p(::std::filesystem::path::string_type(xpath),
            ::std::filesystem::path::auto_format);
        ::std::filesystem::permissions(p,
            ::std::filesystem::perms::owner_exec | ::std::filesystem::perms::group_exec
                | ::std::filesystem::perms::others_exec, ::std::filesystem::perm_options::add);
#else
                auto ok = ::chmod(xpath.c_str(),S_IRUSR|S_IWUSR|S_IXUSR|S_IRGRP|S_IXGRP|S_IROTH|S_IXOTH);
                if (ok!=0) {
                    ::perror(("failed to change filemode "+xpath).c_str());
                   throw ::std::runtime_error("failed to change filemode "+xpath);
                }
#endif
      }
    }

    Directory::Directory(const ::std::string &xpath)
        : path(xpath)
    {
    }

    Directory::~Directory()
    {
    }

    Directory Directory::create(const ::std::string &xpath)
    {
      Directory dir(create_directory(xpath));
      return dir;
    }

    Directory Directory::createDir(const ::std::string &xpath)
    {
      Directory dir(create_sub_directory(path, xpath));
      return dir;
    }

    void Directory::createFile(const ::std::string &file, const ::std::string &data)
    {
      ::std::ofstream stream(create_file(path, file));
      stream << data;
    }

    void Directory::createScriptFile(const ::std::string &file, const ::std::string &data)
    {
      auto xpath = create_file(path, file);

      ::std::ofstream stream(xpath);

      stream << data;
      set_executable(xpath);
    }

    void Directory::createFile(const mylang::names::Name &fqn, const ::std::string &data)
    {
      createFile(fqn, "", data);
    }

    void Directory::createFile(const mylang::names::Name &fqn, const ::std::string &extension,
        const ::std::string &data)
    {
      auto p = path;
      for (auto s : fqn.segments()) {
        create_directory(p);
        p = create_file(p, s->uniqueLocalName());
      }
      p += extension;

      ::std::ofstream stream(p);
      stream << data;
    }

    void Directory::createFile(const mylang::names::Name &fqn, const ::std::string &extension,
        const ::std::string &data, bool namesIsUnique)
    {
      auto p = path;
      for (auto s : fqn.segments()) {
        create_directory(p);
        if (namesIsUnique) {
          p = create_file(p, s->localName());
        } else {
          p = create_file(p, s->uniqueLocalName());
        }
      }
      p += extension;

      ::std::ofstream stream(p);
      stream << data;
    }

  }
}
