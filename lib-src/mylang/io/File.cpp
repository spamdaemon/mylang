#include <bits/types/FILE.h>
#include <mylang/io/File.h>
#include <cstdio>

#if __cplusplus >= 201703L
#define HAVE_CPP_FILESYSTEM 1
#else
#define HAVE_CPP_FILESYSTEM 0
#endif

#if HAVE_CPP_FILESYSTEM
#include <filesystem>

#else
#include <sys/stat.h>
#include <errno.h>
#endif
#include <fstream>
#include <cstdio>

namespace mylang {
  namespace io {

    File::File(const ::std::string &xpath)
        : path(xpath)
    {
    }

    File::~File()
    {
    }

    bool File::exists() const
    {
      FILE *f = std::fopen(path.c_str(), "r");
      if (f) {
        std::fclose(f);
        return true;
      }
      return false;
    }

  }
}
