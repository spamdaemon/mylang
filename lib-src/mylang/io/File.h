#ifndef CLASS_MYLANG_IO_FILE_H
#define CLASS_MYLANG_IO_FILE_H
#include <string>
#include <mylang/names/Name.h>

namespace mylang {
  namespace io {

    /**
     * This class represents a filesystem directory.
     */
    class File
    {

      /**
       * Constructor
       */
    public:
      File(const ::std::string &xpath);

      /**
       * Destructor
       */
    public:
      ~File();

      /**
       * Check if this file exists
       */
    public:
      bool exists() const;

      /** The full file path */
    public:
      const ::std::string path;
    };
  }
}
#endif
