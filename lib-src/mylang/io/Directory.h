#ifndef CLASS_MYLANG_IO_DIRECTORY_H
#define CLASS_MYLANG_IO_DIRECTORY_H
#include <string>
#include <mylang/names/Name.h>

namespace mylang {
  namespace io {

    /**
     * This class represents a filesystem directory.
     */
    class Directory
    {

      /**
       * Constructor
       */
    private:
      Directory(const ::std::string &xpath);

      /**
       * Destructor
       */
    public:
      ~Directory();

      /**
       * Create a subdirectory.
       * @param dir the name of a folder
       */
    public:
      Directory createDir(const ::std::string &dir);

      /**
       * Write the contents of the specified string to the given file
       * @param relPath a path to a file relative to this directory
       * @param data the data to write
       */
    public:
      void createFile(const ::std::string &file, const ::std::string &data);

      /**
       * Write the contents of the specified string to the given file and make the file executable.
       * @param relPath a path to a file relative to this directory
       * @param data the data to write
       */
    public:
      void createScriptFile(const ::std::string &file, const ::std::string &data);

      /**
       * Write the contents of the specified string to the given file
       * @param fqn a path to a file relative to this directory
       * @param data the data to write
       */
    public:
      void createFile(const ::mylang::names::Name &fqn, const ::std::string &data);

      /**
       * Write the contents of the specified string to the given file
       * @param fqn a path to a file relative to this directory
       * @param ext an extension to append to the filename
       * @param data the data to write
       * @parma nameIsUnique the name of the fqn is already unique if true
       */
    public:
      void createFile(const ::mylang::names::Name &fqn, const ::std::string &ext,
          const ::std::string &data);

      void createFile(const ::mylang::names::Name &fqn, const ::std::string &ext,
          const ::std::string &data, bool nameIsUnique);

      /**
       * Create a directory
       */
    public:
      static Directory create(const ::std::string &xpath);

      /** The path to the directory */
    public:
      const ::std::string path;
    };
  }
}
#endif
