#include <mylang/annotations.h>

namespace mylang {

  void copyAnnotations(idioma::ast::Node::CPtr src, idioma::ast::Node::Ptr dest)
  {
    for (int i = 0; i < AnnotationType::ANNO_NUM_ANNOTATIONS; ++i) {
      auto anno = src->annotation(i);
      if (anno) {
        dest->annotate(i, anno);
      }
    }
  }

  void setTagAnnotation(idioma::ast::Node::Ptr node, const ::std::string &annotation)
  {
    if (node && annotation.empty() == false) {
      node->annotate((int) AnnotationType::ANNO_TAG, annotation);
    }
  }

  ::std::optional<std::string> getTagAnnotation(const idioma::ast::Node::CPtr &node)
  {
    if (!node) {
      return {};
    }
    auto ann = node->annotation((int) AnnotationType::ANNO_TAG).cast<::std::string>();
    if (ann) {
      return ann->value();
    } else {
      return {};
    }
  }

  bool hasTagAnnotation(const idioma::ast::Node::CPtr &node, const ::std::string &tag)
  {
    auto a = getTagAnnotation(node);
    return a && a.value() == tag;
  }

  void setVariableAnnotation(idioma::ast::Node::Ptr node, const VariableAnnotation &annotation)
  {
    if (node && annotation) {
      node->annotate((int) AnnotationType::ANNO_VARIABLE, annotation);
    }
  }

  VariableAnnotation getVariableAnnotation(const idioma::ast::Node::CPtr &node)
  {
    if (!node) {
      return nullptr;
    }
    auto ann = node->annotation((int) AnnotationType::ANNO_VARIABLE).cast<VariableAnnotation>();
    if (ann) {
      return ann->value();
    } else {
      return nullptr;
    }
  }

  void setConstraintAnnotation(idioma::ast::Node::Ptr node, const ConstraintAnnotation &annotation)
  {
    if (node && annotation) {
      node->annotate((int) AnnotationType::ANNO_CONSTRAINT, annotation);
    }
  }

  ConstraintAnnotation getConstraintAnnotation(const idioma::ast::Node::CPtr &node)
  {
    if (!node) {
      return nullptr;
    }
    auto ann = node->annotation((int) AnnotationType::ANNO_CONSTRAINT).cast<ConstraintAnnotation>();
    if (ann) {
      return ann->value();
    } else {
      return nullptr;
    }
  }

  void setTypeAnnotation(idioma::ast::Node::Ptr node, const TypeAnnotation &annotation)
  {
    if (node && annotation) {
      node->annotate((int) AnnotationType::ANNO_TYPE, annotation);
    }
  }

  TypeAnnotation getTypeAnnotation(const idioma::ast::Node::CPtr &node)
  {
    if (!node) {
      return nullptr;
    }
    auto ann = node->annotation((int) AnnotationType::ANNO_TYPE).cast<TypeAnnotation>();
    if (ann) {
      return ann->value();
    } else {
      return nullptr;
    }
  }

  void setPatternAnnotation(idioma::ast::Node::Ptr node, const PatternAnnotation &annotation)
  {
    if (node && annotation) {
      node->annotate((int) AnnotationType::ANNO_PATTERN, annotation);
    }
  }

  PatternAnnotation getPatternAnnotation(const idioma::ast::Node::CPtr &node)
  {
    if (!node) {
      return nullptr;
    }
    auto ann = node->annotation((int) AnnotationType::ANNO_PATTERN).cast<PatternAnnotation>();
    if (ann) {
      return ann->value();
    } else {
      return nullptr;
    }
  }

  void setConstAnnotation(idioma::ast::Node::Ptr node, const ConstAnnotation &annotation)
  {
    if (node && annotation) {
      node->annotate((int) AnnotationType::ANNO_CONSTANT, annotation);
    }
  }

  ConstAnnotation getConstAnnotation(const idioma::ast::Node::CPtr &node)
  {
    if (!node) {
      return nullptr;
    }
    auto ann = node->annotation((int) AnnotationType::ANNO_CONSTANT).cast<ConstAnnotation>();
    if (ann) {
      return ann->value();
    } else {
      return nullptr;
    }
  }

  void setNameAnnotation(idioma::ast::Node::Ptr node, const NameAnnotation &annotation)
  {
    if (node && annotation) {
      node->annotate((int) AnnotationType::ANNO_NAME, annotation);
    }
  }

  NameAnnotation getNameAnnotation(const idioma::ast::Node::CPtr &node)
  {
    if (!node) {
      return nullptr;
    }
    auto ann = node->annotation((int) AnnotationType::ANNO_NAME).cast<NameAnnotation>();
    if (ann) {
      return ann->value();
    } else {
      return nullptr;
    }
  }

}
