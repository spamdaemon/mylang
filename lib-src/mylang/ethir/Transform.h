#ifndef CLASS_MYLANG_ETHIR_TRANSFORM_H
#define CLASS_MYLANG_ETHIR_TRANSFORM_H

#ifndef CLASS_MYLANG_ETHIR_VISITOR_H
#include <mylang/ethir/Visitor.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_LOOP_H
#include <mylang/ethir/ir/Loop.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TRANSFORMRESULT_H
#include <mylang/ethir/TransformResult.h>
#endif

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#include <set>
#include <vector>

namespace mylang {
  namespace ethir {

    // FIXME: we should not allow types to be transformed
    // we need a different transform for that
    class Transform: public Visitor<ENode>
    {
    public:
      template<class E>
      using Tx = TransformResult<E>;

    public:

      Transform();
      ~Transform();

      /**
       * Apply this transform to the specified node. This function applies this
       * transform until no more new nodes can be transformed.
       * @param node a node
       * @return a new node or node if no transform was done.
       */
      virtual ENode apply(const ENode &node);

      /**
       * Transform a type.
       * @param type a type reference
       * @return a transformed type. The default is to return no changge.
       */
      Tx<EType> transformType(const EType &type);
      Tx<ENode> transformNode(const ENode &expr);
      Tx<ELiteral> transformLiteral(const ELiteral &expr);
      Tx<EExpression> transformExpr(const EExpression &expr);
      Tx<EVariable> transformExpression(const EVariable &expr);
      Tx<EStatement> transformStatement(const EStatement &stmt);
      Tx<EDeclaration> transformDeclaration(const EDeclaration &decl);

      /**
       * Transform for a list of nodes of a given type.
       */
      template<class T>
      Tx<::std::vector<std::shared_ptr<const T>>> transformNodes(
          const ::std::vector<std::shared_ptr<const T>> &nodes)
      {

        ::std::vector<std::shared_ptr<const T>> vec;
        bool changed = false;
        for (auto n : nodes) {
          auto tx = transformNode(n);
          if (tx.isNew) {
            changed = true;
          }
          vec.push_back(tx.actual->template self<T>());
        }
        return Tx<::std::vector<std::shared_ptr<const T>>>(vec, changed);
      }

      Tx<::std::vector<EVariable>> transformExpressions(const ::std::vector<EVariable> &exprs);
      Tx<::std::vector<ELiteral>> transformLiterals(const ::std::vector<ELiteral> &exprs);
      Tx<::std::vector<ir::Parameter::ParameterPtr>> transformParameters(
          const ::std::vector<ir::Parameter::ParameterPtr> &params);
      Tx<ir::Loop::States> transformLoopStates(const ir::Loop::States &states);
      Tx<ir::Loop::Expressions> transformExpressions(const ir::Loop::Expressions &exprs);

      /**
       * A utility function to quickly create a NoStatement node. The NoStatement
       * can be used to effectively remove a statement.
       */
      static EStatement nop(EStatement next);

      /**
       * Visit a type
       * @param type a type
       * @return a new type or null if no changge
       */
      virtual EType visitType(const EType &type);

      ENode visit(const ir::ToArray::ToArrayPtr &node) override;
      ENode visit(const ir::Phi::PhiPtr &node) override;
      ENode visit(const ir::LetExpression::LetExpressionPtr &node) override;
      ENode visit(const ir::NewUnion::NewUnionPtr &node) override;
      ENode visit(const ir::NewProcess::NewProcessPtr &node) override;
      ENode visit(const ir::LiteralChar::LiteralCharPtr &node) override;
      ENode visit(const ir::InputPortDecl::InputPortDeclPtr &node) override;
      ENode visit(const ir::LiteralString::LiteralStringPtr &node) override;
      ENode visit(const ir::LiteralArray::LiteralArrayPtr &expr) override;
      ENode visit(const ir::LiteralNamedType::LiteralNamedTypePtr &expr) override;
      ENode visit(const ir::LiteralOptional::LiteralOptionalPtr &expr) override;
      ENode visit(const ir::LiteralStruct::LiteralStructPtr &expr) override;
      ENode visit(const ir::LiteralTuple::LiteralTuplePtr &expr) override;
      ENode visit(const ir::LiteralUnion::LiteralUnionPtr &expr) override;
      ENode visit(const ir::ProcessVariableUpdate::ProcessVariableUpdatePtr &node) override;
      ENode visit(const ir::VariableUpdate::VariableUpdatePtr &node) override;
      ENode visit(const ir::IfStatement::IfStatementPtr &node) override;
      ENode visit(const ir::TryCatch::TryCatchPtr &node) override;
      ENode visit(const ir::LiteralReal::LiteralRealPtr &node) override;
      ENode visit(const ir::OperatorExpression::OperatorExpressionPtr &node) override;
      ENode visit(const ir::NoValue::NoValuePtr &node) override;
      ENode visit(const ir::OpaqueExpression::OpaqueExpressionPtr &node) override;
      ENode visit(const ir::LiteralInteger::LiteralIntegerPtr &node) override;
      ENode visit(const ir::VariableDecl::VariableDeclPtr &node) override;
      ENode visit(const ir::ProcessVariable::ProcessVariablePtr &node) override;
      ENode visit(const ir::ProcessValue::ProcessValuePtr &node) override;
      ENode visit(const ir::ProcessDecl::ProcessDeclPtr &node) override;
      ENode visit(const ir::Parameter::ParameterPtr &param) override;
      ENode visit(const ir::LiteralBoolean::LiteralBooleanPtr &node) override;
      ENode visit(const ir::BreakStatement::BreakStatementPtr &node) override;
      ENode visit(const ir::ContinueStatement::ContinueStatementPtr &node) override;
      ENode visit(const ir::LambdaExpression::LambdaExpressionPtr &node) override;
      ENode visit(const ir::GetDiscriminant::GetDiscriminantPtr &node) override;
      ENode visit(const ir::GetMember::GetMemberPtr &node) override;
      ENode visit(const ir::LiteralByte::LiteralBytePtr &node) override;
      ENode visit(const ir::Variable::VariablePtr &node) override;
      ENode visit(const ir::OwnConstructorCall::OwnConstructorCallPtr &node) override;
      ENode visit(const ir::Program::ProgramPtr &node) override;
      ENode visit(const ir::LoopStatement::LoopStatementPtr &node) override;
      ENode visit(const ir::GlobalValue::GlobalValuePtr &node) override;
      ENode visit(const ir::ProcessBlock::ProcessBlockPtr &node) override;
      ENode visit(const ir::ProcessConstructor::ProcessConstructorPtr &node) override;
      ENode visit(const ir::LiteralBit::LiteralBitPtr &node) override;
      ENode visit(const ir::OutputPortDecl::OutputPortDeclPtr &node) override;
      ENode visit(const ir::ValueDecl::ValueDeclPtr &node) override;
      ENode visit(const ir::ForeachStatement::ForeachStatementPtr &node) override;
      ENode visit(const ir::StatementBlock::StatementBlockPtr &node) override;
      ENode visit(const ir::AbortStatement::AbortStatementPtr &node) override;
      ENode visit(const ir::ReturnStatement::ReturnStatementPtr &node) override;
      ENode visit(const ir::ThrowStatement::ThrowStatementPtr &node) override;
      ENode visit(const ir::CommentStatement::CommentStatementPtr &node) override;
      ENode visit(const ir::NoStatement::NoStatementPtr &node) override;
      ENode visit(const ir::Loop::LoopPtr &node) override;
      ENode visit(const ir::YieldStatement::YieldStatementPtr &node) override;
      ENode visit(const ir::SkipStatement::SkipStatementPtr &node) override;
      ENode visit(const ir::IterateArrayStep::IterateArrayStepPtr &node) override;
      ENode visit(const ir::SequenceStep::SequenceStepPtr &node) override;
      ENode visit(const ir::SingleValueStep::SingleValueStepPtr &node) override;

    };

  }
}
#endif
