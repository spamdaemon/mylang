#ifndef CLASS_MYLANG_ETHIR_VISITOR_H
#define CLASS_MYLANG_ETHIR_VISITOR_H

#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/ir/NodeVisitor.h>

namespace mylang {
  namespace ethir {

    template<class T>
    class Visitor
    {

    public:
      Visitor()
      {
      }

    public:
      virtual ~Visitor()
      {
      }

    public:
      virtual T visitNode(const ENode &node)
      {

        struct V: public ir::NodeVisitor
        {
          V(Visitor &tx)
              : t(tx)
          {
          }

          ~V()
          {
          }

          Visitor &t;
          T result;

          void visitNewUnion(ir::NewUnion::NewUnionPtr node) override final
          {
            result = t.visit(node);
          }

          void visitNewProcess(ir::NewProcess::NewProcessPtr node) override final
          {
            result = t.visit(node);
          }

          void visitLiteralArray(ir::LiteralArray::LiteralArrayPtr node) override final
          {
            result = t.visit(node);
          }
          void visitLiteralNamedType(ir::LiteralNamedType::LiteralNamedTypePtr node)
          override final
          {
            result = t.visit(node);
          }
          void visitLiteralOptional(ir::LiteralOptional::LiteralOptionalPtr node) override final
          {
            result = t.visit(node);
          }
          void visitLiteralStruct(ir::LiteralStruct::LiteralStructPtr node) override final
          {
            result = t.visit(node);
          }
          void visitLiteralTuple(ir::LiteralTuple::LiteralTuplePtr node) override final
          {
            result = t.visit(node);
          }
          void visitLiteralUnion(ir::LiteralUnion::LiteralUnionPtr node) override final
          {
            result = t.visit(node);
          }

          void visitLiteralChar(ir::LiteralChar::LiteralCharPtr node) override final
          {
            result = t.visit(node);
          }

          void visitInputPortDecl(ir::InputPortDecl::InputPortDeclPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitLiteralString(ir::LiteralString::LiteralStringPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitLetExpression(ir::LetExpression::LetExpressionPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitPhi(ir::Phi::PhiPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitToArray(ir::ToArray::ToArrayPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitProcessVariableUpdate(ir::ProcessVariableUpdate::ProcessVariableUpdatePtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitVariableUpdate(ir::VariableUpdate::VariableUpdatePtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitIfStatement(ir::IfStatement::IfStatementPtr node) override final
          {
            result = t.visit(node);
          }

          void visitReturnStatement(ir::ReturnStatement::ReturnStatementPtr node) override final
          {
            result = t.visit(node);
          }

          void visitYieldStatement(ir::YieldStatement::YieldStatementPtr node) override final
          {
            result = t.visit(node);
          }

          void visitSkipStatement(ir::SkipStatement::SkipStatementPtr node) override final
          {
            result = t.visit(node);
          }

          void visitIterateArrayStep(ir::IterateArrayStep::IterateArrayStepPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitSequenceStep(ir::SequenceStep::SequenceStepPtr node) override final
          {
            result = t.visit(node);
          }

          void visitSingleValueStep(ir::SingleValueStep::SingleValueStepPtr node) override final
          {
            result = t.visit(node);
          }

          void visitLoop(ir::Loop::LoopPtr node) override final
          {
            result = t.visit(node);
          }

          void visitAbortStatement(ir::AbortStatement::AbortStatementPtr node) override final
          {
            result = t.visit(node);
          }

          void visitThrowStatement(ir::ThrowStatement::ThrowStatementPtr node) override final
          {
            result = t.visit(node);
          }

          void visitCommentStatement(ir::CommentStatement::CommentStatementPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitTryCatch(ir::TryCatch::TryCatchPtr node) override final
          {
            result = t.visit(node);
          }

          void visitLiteralReal(ir::LiteralReal::LiteralRealPtr node) override final
          {
            result = t.visit(node);
          }

          void visitOperatorExpression(ir::OperatorExpression::OperatorExpressionPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitNoValue(ir::NoValue::NoValuePtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitOpaqueExpression(ir::OpaqueExpression::OpaqueExpressionPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitLiteralInteger(ir::LiteralInteger::LiteralIntegerPtr node) override final
          {
            result = t.visit(node);
          }

          void visitVariableDecl(ir::VariableDecl::VariableDeclPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitProcessVariable(ir::ProcessVariable::ProcessVariablePtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitProcessValue(ir::ProcessValue::ProcessValuePtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitProcessDecl(ir::ProcessDecl::ProcessDeclPtr node) override final
          {
            result = t.visit(node);
          }

          void visitParameter(ir::Parameter::ParameterPtr node) override final
          {
            result = t.visit(node);
          }

          void visitLiteralBoolean(ir::LiteralBoolean::LiteralBooleanPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitBreakStatement(ir::BreakStatement::BreakStatementPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitContinueStatement(ir::ContinueStatement::ContinueStatementPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitLambdaExpression(ir::LambdaExpression::LambdaExpressionPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitGetDiscriminant(ir::GetDiscriminant::GetDiscriminantPtr node) override final
          {
            result = t.visit(node);
          }

          void visitGetMember(ir::GetMember::GetMemberPtr node) override final
          {
            result = t.visit(node);
          }

          void visitLiteralByte(ir::LiteralByte::LiteralBytePtr node) override final
          {
            result = t.visit(node);
          }

          void visitVariable(ir::Variable::VariablePtr node) override final
          {
            result = t.visit(node);
          }

          void visitOwnConstructorCall(ir::OwnConstructorCall::OwnConstructorCallPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitProgram(ir::Program::ProgramPtr node) override final
          {
            result = t.visit(node);
          }

          void visitLoopStatement(ir::LoopStatement::LoopStatementPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitGlobalValue(ir::GlobalValue::GlobalValuePtr node) override final
          {
            result = t.visit(node);
          }

          void visitProcessBlock(ir::ProcessBlock::ProcessBlockPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitProcessConstructor(ir::ProcessConstructor::ProcessConstructorPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitLiteralBit(ir::LiteralBit::LiteralBitPtr node) override final
          {
            result = t.visit(node);
          }

          void visitOutputPortDecl(ir::OutputPortDecl::OutputPortDeclPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitValueDecl(ir::ValueDecl::ValueDeclPtr node) override final
          {
            result = t.visit(node);
          }

          void visitForeachStatement(ir::ForeachStatement::ForeachStatementPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitStatementBlock(ir::StatementBlock::StatementBlockPtr node)
          override final
          {
            result = t.visit(node);
          }

          void visitNoStatement(ir::NoStatement::NoStatementPtr node)
          override final
          {
            result = t.visit(node);
          }

        };

        if (node) {
          V v(*this);
          node->accept(v);
          return ::std::move(v.result);
        } else {
          return nullptr;
        }
      }

      virtual T visit(const ir::GetDiscriminant::GetDiscriminantPtr &node) = 0;
      virtual T visit(const ir::GetMember::GetMemberPtr &node) = 0;
      virtual T visit(const ir::LambdaExpression::LambdaExpressionPtr &node) = 0;
      virtual T visit(const ir::LetExpression::LetExpressionPtr &node) = 0;
      virtual T visit(const ir::LiteralArray::LiteralArrayPtr &expr) = 0;
      virtual T visit(const ir::LiteralBit::LiteralBitPtr &node) = 0;
      virtual T visit(const ir::LiteralBoolean::LiteralBooleanPtr &node) = 0;
      virtual T visit(const ir::LiteralByte::LiteralBytePtr &node) = 0;
      virtual T visit(const ir::LiteralChar::LiteralCharPtr &node) = 0;
      virtual T visit(const ir::LiteralInteger::LiteralIntegerPtr &node) = 0;
      virtual T visit(const ir::LiteralNamedType::LiteralNamedTypePtr &expr)= 0;
      virtual T visit(const ir::LiteralOptional::LiteralOptionalPtr &expr)= 0;
      virtual T visit(const ir::LiteralReal::LiteralRealPtr &node) = 0;
      virtual T visit(const ir::LiteralString::LiteralStringPtr &node) = 0;
      virtual T visit(const ir::LiteralStruct::LiteralStructPtr &expr) = 0;
      virtual T visit(const ir::LiteralTuple::LiteralTuplePtr &expr)= 0;
      virtual T visit(const ir::LiteralUnion::LiteralUnionPtr &expr) = 0;
      virtual T visit(const ir::NewUnion::NewUnionPtr &node) = 0;
      virtual T visit(const ir::NewProcess::NewProcessPtr &node) = 0;
      virtual T visit(const ir::OperatorExpression::OperatorExpressionPtr &node) = 0;
      virtual T visit(const ir::OpaqueExpression::OpaqueExpressionPtr &node) = 0;
      virtual T visit(const ir::NoValue::NoValuePtr &node) = 0;
      virtual T visit(const ir::Phi::PhiPtr &node) = 0;
      virtual T visit(const ir::ToArray::ToArrayPtr &node) = 0;
      virtual T visit(const ir::Variable::VariablePtr &node) = 0;

      virtual T visit(const ir::NoStatement::NoStatementPtr &node) = 0;
      virtual T visit(const ir::CommentStatement::CommentStatementPtr &node) = 0;
      virtual T visit(const ir::AbortStatement::AbortStatementPtr &node) = 0;
      virtual T visit(const ir::BreakStatement::BreakStatementPtr &node) = 0;
      virtual T visit(const ir::ContinueStatement::ContinueStatementPtr &node) = 0;
      virtual T visit(const ir::Parameter::ParameterPtr &param) = 0;
      virtual T visit(const ir::VariableDecl::VariableDeclPtr &node) = 0;
      virtual T visit(const ir::ProcessVariable::ProcessVariablePtr &node) = 0;
      virtual T visit(const ir::ProcessValue::ProcessValuePtr &node) = 0;
      virtual T visit(const ir::ProcessDecl::ProcessDeclPtr &node) = 0;
      virtual T visit(const ir::InputPortDecl::InputPortDeclPtr &node) = 0;
      virtual T visit(const ir::ProcessVariableUpdate::ProcessVariableUpdatePtr &node) = 0;
      virtual T visit(const ir::VariableUpdate::VariableUpdatePtr &node) = 0;
      virtual T visit(const ir::IfStatement::IfStatementPtr &node) = 0;
      virtual T visit(const ir::TryCatch::TryCatchPtr &node) = 0;
      virtual T visit(const ir::OwnConstructorCall::OwnConstructorCallPtr &node) = 0;
      virtual T visit(const ir::Program::ProgramPtr &node) = 0;
      virtual T visit(const ir::LoopStatement::LoopStatementPtr &node) = 0;
      virtual T visit(const ir::GlobalValue::GlobalValuePtr &node) = 0;
      virtual T visit(const ir::ProcessBlock::ProcessBlockPtr &node) = 0;
      virtual T visit(const ir::ProcessConstructor::ProcessConstructorPtr &node) = 0;
      virtual T visit(const ir::OutputPortDecl::OutputPortDeclPtr &node) = 0;
      virtual T visit(const ir::ValueDecl::ValueDeclPtr &node) = 0;
      virtual T visit(const ir::ForeachStatement::ForeachStatementPtr &node) = 0;
      virtual T visit(const ir::StatementBlock::StatementBlockPtr &node) = 0;
      virtual T visit(const ir::ReturnStatement::ReturnStatementPtr &node) = 0;
      virtual T visit(const ir::ThrowStatement::ThrowStatementPtr &node) = 0;
      virtual T visit(const ir::YieldStatement::YieldStatementPtr &node) = 0;
      virtual T visit(const ir::SkipStatement::SkipStatementPtr &node) = 0;
      virtual T visit(const ir::IterateArrayStep::IterateArrayStepPtr &node) = 0;
      virtual T visit(const ir::SequenceStep::SequenceStepPtr &node) = 0;
      virtual T visit(const ir::SingleValueStep::SingleValueStepPtr &node) = 0;
      virtual T visit(const ir::Loop::LoopPtr &node) = 0;

    };

  }
}
#endif
