#ifndef FILE_MYLANG_ETHIR_HASHCODE_H
#define FILE_MYLANG_ETHIR_HASHCODE_H

#include <bits/stdint-uintn.h>

#include "Name.h"
#include "Tag.h"

#ifndef CLASS_MYLANG_BIGINT_H
#include <mylang/BigInt.h>
#endif

#ifndef CLASS_MYLANG_BIGUINT_H
#include <mylang/BigUInt.h>
#endif

#ifndef CLASS_MYLANG_BIGREAL_H
#include <mylang/BigReal.h>
#endif

#ifndef CLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#include <optional>
#include <cstdint>
#include <string>
#include <typeinfo>

namespace mylang {
  namespace ethir {
    class Name;
    class Tag;

    /**
     * Hashcode.
     */
    class HashCode
    {
      typedef ::std::uint64_t HashType;

      /** Create the 0 hashcode */
    public:
      inline HashCode()
          : hashcode(0)
      {
      }

    public:
      inline HashType value() const
      {
        return hashcode;
      }

      /**
       * Integrate another hashcode.
       */
    public:
      HashCode& mix(const HashCode &value);

      /**
       * Integrate an integer value into the hashcode
       */
    public:
      HashCode& mix(::std::uint64_t value);

      /**
       * Integrate an integer value into the hashcode
       */
    public:
      inline HashCode& mix(::std::int64_t value)
      {
        return mix(static_cast<::std::uint64_t>(value));
      }

      /**
       * Integrate an integer value into the hashcode
       */
    public:
      HashCode& mix(const void *value);

      /**
       * Integrate an integer value into the hashcode
       */
    public:
      HashCode& mix(const EType &value);

      /**
       * Integrate an integer value into the hashcode
       */
    public:
      HashCode& mix(const ENode &value);

      /**
       * Integrate an integer value into the hashcode
       */
    public:
      template<class T>
      inline HashCode& mix(const ::std::optional<T> opt)
      {
        if (opt.has_value()) {
          return mix(true).mix(opt.value());
        } else {
          return mix(false);
        }
      }

      /**
       * Integrate an integer value into the hashcode
       */
    public:
      template<class T>
      inline HashCode& mix(T *value)
      {
        return mix(static_cast<const void*>(value));
      }

      /**
       * Integrate a collection into this hashcode.
       */
    public:
      template<class T, class U>
      inline HashCode& mix(T start, U end)
      {
        while (start != end) {
          mix(*start);
          ++start;
        }
        return *this;
      }

      /**
       * Integrate an boolean value into the hashcode
       */
    public:
      HashCode& mix(bool value);

      /**
       * Integrate an integer value into the hashcode
       */
    public:
      HashCode& mix(const ::std::string &str);

      /**
       * Mix a null-terminated string
       */
      HashCode& mixString(const char *text);

      /**
       * Integrate a name into the code.
       */
    public:
      HashCode& mix(const Tag &tag);

      /**
       * Integrate a name into the code.
       */
    public:
      HashCode& mix(const Name &name);

      /**
       * Integrate a name into the code.
       */
    public:
      HashCode& mix(const mylang::names::Name::Ptr &name);

      /**
       * Integrate a value into the code.
       */
    public:
      HashCode& mix(const BigInt &value);

      /**
       * Integrate a value into the code.
       */
    public:
      HashCode& mix(const BigUInt &value);

      /**
       * Integrate a value into the code.
       */
    public:
      HashCode& mix(const BigReal &value);

      /**
       * Integrate a name into the code.
       */
    public:
      HashCode& mix(const ::std::type_info &id);

      /**
       * Check if we have the same hashcode
       */
    public:
      friend bool operator==(const HashCode &h1, const HashCode &h2)
      {
        return h1.hashcode == h2.hashcode;
      }

      /**
       * Check if we have the different hashcodes
       */
    public:
      friend bool operator!=(const HashCode &h1, const HashCode &h2)
      {
        return h1.hashcode != h2.hashcode;
      }

      /** The hash type */
    private:
      HashType hashcode;
    };

  }
}
#endif
