#ifndef CLASS_MYLANG_ETHIR_TYPECASTERROR_H
#define CLASS_MYLANG_ETHIR_TYPECASTERROR_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#include <stdexcept>

namespace mylang {
  namespace ethir {

    /**
     * A type mismatch.
     */
    class TypeCastError: public ::std::runtime_error
    {
      /**
       * Create a type mismatch error.
       * @param from the type from which to cast
       * @param to the type to which to cast
       */
    public:
      TypeCastError(const EType &from, const EType &to);

      /** Destructor */
    public:
      virtual ~TypeCastError();

      /**
       * Perform a check for a safe-cast between two types.
       * @param from the type from which to cast
       * @param to the type to which to cast
       */
    private:
      static void checkSafeCast(const EType &from, const EType &to);

      /**
       * Perform a check for a castability between two types.
       * @param from the type from which to cast
       * @param to the type to which to cast
       */
    public:
      static void checkCast(const EType &from, const EType &to);

      /** The type that was expected */
    public:
      const EType from;

      /** The actual type */
    public:
      const EType to;
    };
  }
}
#endif
