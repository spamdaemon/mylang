#ifndef CLASS_MYLANG_ETHIR_TYPES_MUTABLETYPE_H
#define CLASS_MYLANG_ETHIR_TYPES_MUTABLETYPE_H

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#include <mylang/ethir/types/Type.h>
#endif
#include <memory>

namespace mylang {
  namespace ethir {
    namespace types {

      /** A type representing an error */
      class MutableType: public Type
      {

      private:
        MutableType(::std::shared_ptr<const Type> xelement);

      public:
        ~MutableType();

        /**
         * Follow the element type until an element is found that is not a optional
         * @return an element type (maybe nullptr!)
         */
      public:
        Type::Ptr flatten() const;

      public:
        static ::std::shared_ptr<const MutableType> get(::std::shared_ptr<const Type> xelement);

      public:
        Ptr normalize(TypeSet &ts) const override;

        void accept(TypeVisitor &v) const override;

        bool canSafeCastFrom(const Type &t) const override;

        bool canCastFrom(const Type &t) const override;

        ::std::string toString() const final;

        bool isSameType(const Type &other) const override;

        /**
         * Create a new instance of of this mutable, but change the return type
         * @param element the new element type
         */
      public:
        ::std::shared_ptr<const MutableType> copy(
            const ::std::shared_ptr<const Type> &newElement) const;

        /** The element type */
      public:
        const ::std::shared_ptr<const Type> element;
      };
    }
  }
}
#endif
