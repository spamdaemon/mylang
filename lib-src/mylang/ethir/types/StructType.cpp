#include <mylang/ethir/types/StructType.h>
#include <mylang/ethir/types/TypeVisitor.h>
#include <mylang/ethir/types/TypeSet.h>

namespace mylang {
  namespace ethir {
    namespace types {
      StructType::Member::Member(const ::std::string &xname, const Type::Ptr &xtype)
          : name(xname), type(xtype)
      {

      }

      StructType::StructType(const ::std::vector<Member> &xmembers)
          : members(xmembers)
      {
        for (auto sf : members) {
          if (sf.name.empty()) {
            throw ::std::invalid_argument("Missing struct member name");
          }
          if (!sf.type) {
            throw ::std::invalid_argument("Missing struct member type");
          }
          if (_indexedMembers.count(sf.name) != 0) {
            throw ::std::invalid_argument("Duplicate struct member " + sf.name);
          }
          _indexedMembers.insert(::std::make_pair(sf.name, sf));
        }
      }

      StructType::~StructType()
      {
      }

      Type::Ptr StructType::normalize(TypeSet &ts) const
      {
        ::std::vector<Member> tmp;
        for (auto m : members) {
          tmp.push_back(Member(m.name, m.type->normalize(ts)));
        }
        return ts.add(get(tmp));
      }

      void StructType::accept(TypeVisitor &v) const
      {
        v.visit(::std::dynamic_pointer_cast<const StructType>(self()));
      }

      bool StructType::isSameType(const Type &other) const
      {
        auto t = dynamic_cast<const StructType*>(&other);
        if (!t || t->members.size() != members.size()) {
          return false;
        }
        for (size_t i = 0; i < members.size(); ++i) {
          if (members[i].name != t->members[i].name) {
            return false;
          }
          if (!members[i].type->isSameType(*t->members[i].type)) {
            return false;
          }
        }

        return true;
      }

      bool StructType::canSafeCastFrom(const Type &from) const
      {
        auto thatStruct = dynamic_cast<const StructType*>(&from);
        if (!thatStruct) {
          return Type::canSafeCastFrom(from);
        }
        if (members.size() != thatStruct->members.size()) {
          return false;
        }
        for (size_t i = 0; i < members.size(); ++i) {
          if (members[i].name != thatStruct->members[i].name) {
            return false;
          }
          if (!members[i].type->canSafeCastFrom(*thatStruct->members[i].type)) {
            return false;
          }
        }
        return true;
      }

      bool StructType::canCastFrom(const Type &from) const
      {
        auto thatStruct = dynamic_cast<const StructType*>(&from);
        if (!thatStruct) {
          return Type::canSafeCastFrom(from);
        }
        if (members.size() != thatStruct->members.size()) {
          return false;
        }
        for (size_t i = 0; i < members.size(); ++i) {
          if (members[i].name != thatStruct->members[i].name) {
            return false;
          }
          if (!members[i].type->canCastFrom(*thatStruct->members[i].type)) {
            return false;
          }
        }
        return true;
      }

      ::std::string StructType::toString() const
      {
        ::std::string str;
        str += "struct {";
        if (!members.empty()) {
          str += '\n';
        }
        for (auto sf : members) {
          str += sf.name;
          str += ':';
          str += sf.type->toString();
          str += ";\n";
        }
        str += "}";
        return str;
      }

      ::std::shared_ptr<const StructType> StructType::get(const ::std::vector<Member> &members)
      {
        struct Impl: public StructType
        {
          Impl(const ::std::vector<Member> &t)
              : StructType(t)
          {
          }

          ~Impl()
          {
          }
        };
        return ::std::make_shared<Impl>(members);
      }

      ::std::vector<::std::shared_ptr<const Type>> StructType::getMemberTypes() const
      {
        ::std::vector<::std::shared_ptr<const Type>> res;
        res.reserve(members.size());
        for (auto m : members) {
          res.push_back(m.type);
        }
        return res;
      }

      ::std::shared_ptr<const Type> StructType::getMemberType(const ::std::string &field) const
      {
        auto i = _indexedMembers.find(field);
        if (i == _indexedMembers.end()) {
          return nullptr;
        }
        return i->second.type;
      }

      size_t StructType::indexOfMember(const ::std::string &member) const
      {
        for (size_t i = 0; i < members.size(); ++i) {
          if (members[i].name == member) {
            return i;
          }
        }
        throw ::std::runtime_error("No such member : " + member);
      }

    }
  }
}
