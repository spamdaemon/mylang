#include <mylang/ethir/types/RealType.h>
#include <mylang/ethir/types/TypeVisitor.h>

namespace mylang {
  namespace ethir {
    namespace types {

      RealType::RealType()
      {
      }

      RealType::~RealType()
      {
      }

      void RealType::accept(TypeVisitor &v) const
      {
        v.visit(::std::dynamic_pointer_cast<const RealType>(self()));
      }

      bool RealType::canCastFrom(const Type &from) const
      {
        if (dynamic_cast<const IntegerType*>(&from)) {
          return true;
        }
        return PrimitiveType::canCastFrom(from);
      }

      bool RealType::isSameType(const Type &other) const
      {
        return dynamic_cast<const RealType*>(&other) != nullptr;
      }

      ::std::string RealType::toString() const
      {
        return "real";
      }

      ::std::shared_ptr<const RealType> RealType::create()
      {
        struct Impl: public RealType
        {
          ~Impl()
          {
          }
        };

        return ::std::make_shared<Impl>();
      }

    }
  }
}
