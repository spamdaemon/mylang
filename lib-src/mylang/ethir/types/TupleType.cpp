#include <mylang/ethir/types/TupleType.h>
#include <mylang/ethir/types/TypeVisitor.h>
#include <mylang/ethir/types/TypeSet.h>

namespace mylang {
  namespace ethir {
    namespace types {
      namespace {
        static ::std::vector<TupleType::Member> toMembers(const ::std::vector<Type::Ptr> &xtuple)
        {
          ::std::vector<TupleType::Member> res;
          for (size_t i = 0; i < xtuple.size(); ++i) {
            auto t = xtuple[i];
            if (!t) {
              throw ::std::invalid_argument("null type");
            }
            res.emplace_back("_" + ::std::to_string(i), t);
          }
          return res;
        }
      }

      TupleType::Member::Member(const ::std::string &xname, const Type::Ptr &xtype)
          : name(xname), type(xtype)
      {

      }

      TupleType::TupleType(const ::std::vector<Ptr> &xtuple)
          : types(xtuple), tuple(toMembers(xtuple))
      {
      }

      TupleType::~TupleType()
      {
      }

      ::std::shared_ptr<const Type> TupleType::getMemberType(const ::std::string &field) const
      {
        for (auto &m : tuple) {
          if (m.name == field) {
            return m.type;
          }
        }
        return nullptr;
      }

      size_t TupleType::indexOfMember(const ::std::string &member) const
      {
        for (size_t i = 0; i < tuple.size(); ++i) {
          if (tuple[i].name == member) {
            return i;
          }
        }
        throw ::std::runtime_error("No such member : " + member);
      }

      bool TupleType::canSafeCastFrom(const Type &from) const
      {
        auto thatTuple = dynamic_cast<const TupleType*>(&from);
        if (!thatTuple) {
          return Type::canSafeCastFrom(from);
        }
        if (types.size() != thatTuple->tuple.size()) {
          return false;
        }
        for (size_t i = 0; i < types.size(); ++i) {
          if (!types[i]->canSafeCastFrom(*thatTuple->types[i])) {
            return false;
          }
        }
        return true;
      }

      bool TupleType::canCastFrom(const Type &from) const
      {
        auto thatTuple = dynamic_cast<const TupleType*>(&from);
        if (!thatTuple) {
          return Type::canCastFrom(from);
        }
        if (types.size() != thatTuple->tuple.size()) {
          return false;
        }
        for (size_t i = 0; i < types.size(); ++i) {
          if (!types[i]->canCastFrom(*thatTuple->types[i])) {
            return false;
          }
        }
        return true;
      }

      Type::Ptr TupleType::normalize(TypeSet &ts) const
      {
        ::std::vector<Ptr> tmp;
        for (auto v : types) {
          tmp.push_back(v->normalize(ts));
        }
        return ts.add(get(tmp));
      }

      void TupleType::accept(TypeVisitor &v) const
      {
        v.visit(::std::dynamic_pointer_cast<const TupleType>(self()));
      }

      bool TupleType::isSameType(const Type &other) const
      {
        auto t = dynamic_cast<const TupleType*>(&other);
        if (!t || t->tuple.size() != tuple.size()) {
          return false;
        }
        for (size_t i = 0; i < tuple.size(); ++i) {
          if (!tuple[i].type->isSameType(*t->tuple[i].type)) {
            return false;
          }
        }

        return true;
      }

      ::std::string TupleType::toString() const
      {
        ::std::string str;
        str += "tuple {";
        const char *sep = "";
        for (auto t : tuple) {
          str += sep;
          str += t.type->toString();
          sep = ", ";
        }
        str += "}";
        return str;
      }

      ::std::shared_ptr<const TupleType> TupleType::get(const ::std::vector<Ptr> &tuple)
      {
        struct Impl: public TupleType
        {
          Impl(const ::std::vector<Ptr> &t)
              : TupleType(t)
          {
          }

          ~Impl()
          {
          }
        };
        return ::std::make_shared<Impl>(tuple);
      }

      ::std::shared_ptr<const TupleType> TupleType::merge(
          const ::std::vector<::std::shared_ptr<const TupleType>> &xtuples)
      {
        ::std::vector<Ptr> xtypes;
        for (auto t : xtuples) {
          xtypes.insert(xtypes.end(), t->types.begin(), t->types.end());
        }
        return get(xtypes);
      }

    }
  }
}
