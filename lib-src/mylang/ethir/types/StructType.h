#ifndef CLASS_MYLANG_ETHIR_TYPES_STRUCTTYPE_H
#define CLASS_MYLANG_ETHIR_TYPES_STRUCTTYPE_H

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#include <mylang/ethir/types/Type.h>
#endif
#include <memory>
#include <map>
#include <vector>

namespace mylang {
  namespace ethir {
    namespace types {

      /** A type representing an error */
      class StructType: public Type
      {

      public:
        struct Member
        {

          Member(const ::std::string &xname, const ::std::shared_ptr<const Type> &xtype);

          /** The name of the parameter */
          const ::std::string name;

          /** The parameter type */
          const ::std::shared_ptr<const Type> type;
        };

      private:
        StructType(const ::std::vector<Member> &members);

      public:
        ~StructType();

        /** Two primitives are the same if their type ids are the same  */
      public:
        Ptr normalize(TypeSet &ts) const;

        void accept(TypeVisitor &v) const;

        bool isSameType(const Type &other) const final;

        ::std::string toString() const final;

        bool canSafeCastFrom(const Type &t) const override;

        bool canCastFrom(const Type &t) const override;

        /**
         * Get a structure
         * @param members
         */
      public:
        static ::std::shared_ptr<const StructType> get(const ::std::vector<Member> &xmembers);

        /**
         * Get the type of the subfield with the specified name.
         * @param name a subfield
         * @return the type of the subfield or nullptr if not found
         */
      public:
        ::std::shared_ptr<const Type> getMemberType(const ::std::string &field) const;

        /**
         * Get the types for each member.
         * @return the types for each mmember by its index
         */
      public:
        ::std::vector<::std::shared_ptr<const Type>> getMemberTypes() const;

        /**
         * Get the index of a member
         * @param name member name
         * @return index of member
         */
      public:
        size_t indexOfMember(const ::std::string &member) const;

        /** The members */
      public:
        const ::std::vector<Member> members;

        /** The indexed members */
      private:
        ::std::map<::std::string, const Member> _indexedMembers;
      };
    }
  }
}
#endif
