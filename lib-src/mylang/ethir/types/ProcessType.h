#ifndef CLASS_MYLANG_ETHIR_TYPES_PROCESSTYPE_H
#define CLASS_MYLANG_ETHIR_TYPES_PROCESSTYPE_H

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#include <mylang/ethir/types/Type.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_INPUTOUTPUTTYPE_H
#include <mylang/ethir/types/InputOutputType.h>
#endif
#include <map>
#include <memory>

namespace mylang {
  namespace ethir {
    namespace types {

      /** A type representing an error */
      class ProcessType: public Type
      {
      public:
        typedef ::std::map<::std::string, ::std::shared_ptr<const InputOutputType>> Ports;

      private:
        ProcessType(Ports ports);

      public:
        ~ProcessType();

      public:
        static ::std::shared_ptr<const ProcessType> get(Ports ports);

      public:
        Ptr normalize(TypeSet &ts) const override;

        void accept(TypeVisitor &v) const override;

        ::std::string toString() const final;

        bool isSameType(const Type &other) const override;

      public:
        const Ports ports;
      };
    }
  }
}
#endif
