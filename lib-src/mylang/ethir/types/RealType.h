#ifndef CLASS_MYLANG_ETHIR_TYPES_REALTYPE_H
#define CLASS_MYLANG_ETHIR_TYPES_REALTYPE_H

#ifndef CLASS_MYLANG_ETHIR_TYPES_NUMERICTYPE_H
#include <mylang/ethir/types/NumericType.h>
#endif

namespace mylang {
  namespace ethir {
    namespace types {

      /** The baseclass for all types */
      class RealType: public NumericType
      {

      private:
        RealType();

      public:
        ~RealType();

        /**
         * Get the bit type
         * @return a primitive type for a INTEGER
         */
      public:
        static ::std::shared_ptr<const RealType> create();

      public:
        bool canCastFrom(const Type &t) const;

        void accept(TypeVisitor &v) const;

        bool isSameType(const Type &other) const final;

        ::std::string toString() const final;
      };
    }
  }
}
#endif
