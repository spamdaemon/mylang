#ifndef CLASS_MYLANG_ETHIR_TYPES_INTEGERTYPE_H
#define CLASS_MYLANG_ETHIR_TYPES_INTEGERTYPE_H

#ifndef CLASS_MYLANG_ETHIR_TYPES_NUMERICTYPE_H
#include <mylang/ethir/types/NumericType.h>
#endif

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_INTERVAL_H
#include <mylang/Interval.h>
#endif

namespace mylang {
  namespace ethir {
    namespace types {

      /** Integer type */
      class IntegerType: public NumericType
      {
      public:
        typedef ::std::shared_ptr<const IntegerType> IntegerTypePtr;

      public:
        using Range = Interval;

      protected:
        IntegerType(Range range);

      public:
        ~IntegerType();

        /**
         * Get the bit type
         * @return a primitive type for a INTEGER
         */
      public:
        static IntegerTypePtr create();

        /**
         * Get the bit type
         * @return a primitive type for a INTEGER
         */
      public:
        static IntegerTypePtr create(Integer min, Integer max);

        /**
         * Get the bit type
         * @return a primitive type for a INTEGER
         */
      public:
        static IntegerTypePtr create(const Range &r);

        /**
         * Union two integer types
         * @param a a type
         * @param b a type
         * @return the union of the two types
         */
      public:
        static IntegerTypePtr unionOf(IntegerTypePtr a, IntegerTypePtr b);
        static IntegerTypePtr intersectionOf(IntegerTypePtr a, IntegerTypePtr b);
        static IntegerTypePtr maxOf(IntegerTypePtr a, IntegerTypePtr b);
        static IntegerTypePtr minOf(IntegerTypePtr a, IntegerTypePtr b);

        /**
         * Check if this is an unsigned integer type.
         * The value is unsigned if the min of the range is at least 0.
         */
      public:
        bool isUnsigned() const;

      public:
        void accept(TypeVisitor &v) const override final;

        bool isSameType(const Type &other) const final override;
        bool canSafeCastFrom(const Type &t) const final override;
        Ptr joinWith(const Ptr &t) const final override;
        Ptr unionWith(const Ptr &t) const final override;
        Ptr intersectWith(const Ptr &t) const final override;

        bool canCastFrom(const Type &t) const final override;
        void inferConstraints(RelOp op, const Ptr &right, Ptr &leftIfTrue, Ptr &leftIfFalse,
            Ptr &rightIfTrue, Ptr &rightIfFalse) const final override;

        ::std::string toString() const final override;

        /** The range of values for this type */
      public:
        const Range range;
      };
    }
  }
}
#endif
