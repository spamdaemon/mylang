#include <mylang/ethir/types/AbstractTypeVisitor.h>
#include <mylang/ethir/types/ArrayType.h>
#include <mylang/ethir/types/BitType.h>
#include <mylang/ethir/types/BooleanType.h>
#include <mylang/ethir/types/BuilderType.h>
#include <mylang/ethir/types/ByteType.h>
#include <mylang/ethir/types/CharType.h>
#include <mylang/ethir/types/FunctionType.h>
#include <mylang/ethir/types/GenericType.h>
#include <mylang/ethir/types/InputType.h>
#include <mylang/ethir/types/IntegerType.h>
#include <mylang/ethir/types/MutableType.h>
#include <mylang/ethir/types/NamedType.h>
#include <mylang/ethir/types/OptType.h>
#include <mylang/ethir/types/OutputType.h>
#include <mylang/ethir/types/ProcessType.h>
#include <mylang/ethir/types/RealType.h>
#include <mylang/ethir/types/StringType.h>
#include <mylang/ethir/types/types.h>
#include <memory>

namespace mylang {
  namespace ethir {
    namespace types {

      AbstractTypeVisitor::AbstractTypeVisitor(bool xrecurse)
          : recurse(xrecurse ? ::std::make_unique<::std::set<TypeName>>() : nullptr)
      {
      }

      AbstractTypeVisitor::~AbstractTypeVisitor()
      {
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const BuilderType> &type)
      {
        if (recurse) {
          type->product->accept(*this);
        }
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const BitType>&)
      {
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const BooleanType>&)
      {
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const ByteType>&)
      {
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const CharType>&)
      {
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const RealType>&)
      {
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const IntegerType>&)
      {
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const StringType>&)
      {
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const VoidType>&)
      {
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const GenericType>&)
      {
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const ArrayType> &type)
      {
        if (recurse) {
          type->element->accept(*this);
        }
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const UnionType> &type)
      {
        if (recurse) {
          type->discriminant.type->accept(*this);
          for (const auto &m : type->members) {
            m.type->accept(*this);
          }
        }
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const StructType> &type)
      {
        if (recurse) {
          for (const auto &m : type->members) {
            m.type->accept(*this);
          }
        }
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const TupleType> &type)
      {
        if (recurse) {
          for (const auto &t : type->types) {
            t->accept(*this);
          }
        }
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const FunctionType> &type)
      {
        if (recurse) {
          type->returnType->accept(*this);
          for (const auto &t : type->parameters) {
            t->accept(*this);
          }
        }
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const MutableType> &type)
      {
        if (recurse) {
          type->element->accept(*this);
        }
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const OptType> &type)
      {
        if (recurse) {
          type->element->accept(*this);
        }
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const NamedType> &type)
      {
        if (recurse && recurse->insert(type->name()).second) {
          type->resolve()->accept(*this);
        }
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const InputType> &type)
      {
        if (recurse) {
          type->element->accept(*this);
        }
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const OutputType> &type)
      {
        if (recurse) {
          type->element->accept(*this);
        }
      }

      void AbstractTypeVisitor::visit(const ::std::shared_ptr<const ProcessType> &type)
      {
        if (recurse) {
          for (const auto &p : type->ports) {
            p.second->accept(*this);
          }
        }
      }

    }
  }
}
