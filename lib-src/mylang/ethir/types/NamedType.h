#ifndef CLASS_MYLANG_ETHIR_TYPES_NAMEDTYPE_H
#define CLASS_MYLANG_ETHIR_TYPES_NAMEDTYPE_H

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#include <mylang/ethir/types/Type.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPENAME_H
#include <mylang/ethir/TypeName.h>
#endif

#include <memory>
#include <functional>

namespace mylang {
  namespace ethir {
    namespace types {

      /** A type representing an error */
      class NamedType: public Type
      {
        /** A pointer to a named type */
      public:
        typedef ::std::shared_ptr<const mylang::ethir::types::NamedType> NamedTypePtr;

        /** A resolver function; returns null if the name cannot be resolved */
      public:
        typedef ::std::function<Type::Ptr()> Resolver;

      private:
        NamedType(const TypeName &name);

      public:
        ~NamedType();

      public:
        static ::std::shared_ptr<const NamedType> get(const TypeName &name, Resolver resolver);

      public:
        void accept(TypeVisitor &v) const;

        bool isSameType(const Type &other) const final;

        ::std::string toString() const;

        bool isConstructible() const;

        /**
         * Create a type that can weakly reference
         */
      public:
        virtual ::std::shared_ptr<const NamedType> getRecursiveReference() const;

        /**
         * Determine if this name is resolved. If true, then resolve() will not throw an exception *
         */
      public:
        virtual bool isResolved() const = 0;

        /** Resolve this name to an actual type. Throws an exception if the name cannot be resolved. */
      public:
        virtual ::std::shared_ptr<const Type> resolve() const = 0;
      };
    }
  }
}
#endif
