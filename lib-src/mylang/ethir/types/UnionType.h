#ifndef CLASS_MYLANG_ETHIR_TYPES_UNIONTYPE_H
#define CLASS_MYLANG_ETHIR_TYPES_UNIONTYPE_H

#ifndef CLASS_MYLANG_ETHIR_TYPES_OPTTYPE_H
#include <mylang/ethir/types/OptType.h>
#endif
#include <memory>
#include <map>
#include <vector>

namespace mylang {
  namespace ethir {
    namespace ir {
      class Literal;
    }
    namespace types {

      /** A type representing an error */
      class UnionType: public Type
      {
        /** A literal pointer */
      public:
        typedef ::std::shared_ptr<const mylang::ethir::ir::Literal> LiteralPtr;

      public:
        struct Discriminant
        {

          Discriminant(const ::std::string &xname, const Type::Ptr &xtype);

          /** The name of the parameter */
          const ::std::string name;

          /** The parameter type */
          const Type::Ptr type;
        };

      public:
        struct Member
        {

          Member(const UnionType::LiteralPtr &discriminant, const ::std::string &xname,
              const Type::Ptr &xtype);

          /** The value of the discriminant for this member */
          const UnionType::LiteralPtr discriminant;

          /** The name of the parameter */
          const ::std::string name;

          /** The parameter type */
          const Type::Ptr type;
        };

      private:
        UnionType(const Discriminant &xdiscriminant, const ::std::vector<Member> &members);

      public:
        ~UnionType();

        /** Two primitives are the same if their type ids are the same  */
      public:
        Ptr normalize(TypeSet &ts) const;

        void accept(TypeVisitor &v) const;

        bool isSameType(const Type &other) const final;

        ::std::string toString() const final;

        /**
         * Get a structure
         * @param members
         */
      public:
        static ::std::shared_ptr<const UnionType> get(const Discriminant &xdiscriminant,
            const ::std::vector<Member> &xmembers);

        /**
         * Get the type of the subfield with the specified name.
         * @param name a subfield
         * @return the type of the subfield or nullptr if not found
         */
      public:
        Type::Ptr getMemberType(const ::std::string &field) const;

        Type::Ptr getMemberType(const LiteralPtr &discriminantValue) const;

        ::std::string getMember(const LiteralPtr &discriminantValue) const;

        /**
         * Get the index of a member
         * @param name member name
         * @return index of member
         */
      public:
        size_t indexOfMember(const ::std::string &member) const;

        /**
         * Get the discriminant associated with the specified member.
         * @param field a member
         * @return the literal the represents the disciminant
         */
      public:
        LiteralPtr getMemberDiscriminant(const ::std::string &field) const;

        /** The discriminant member */
      public:
        const Discriminant discriminant;

        /** The members */
      public:
        const ::std::vector<Member> members;

        /** The indexed members */
      private:
        ::std::map<::std::string, const Member> _indexedMembers;
      };
    }
  }
}
#endif
