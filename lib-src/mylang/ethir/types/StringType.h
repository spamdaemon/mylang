#ifndef CLASS_MYLANG_ETHIR_TYPES_STRINGTYPE_H
#define CLASS_MYLANG_ETHIR_TYPES_STRINGTYPE_H

#ifndef CLASS_MYLANG_ETHIR_TYPES_PRIMITIVETYPE_H
#include <mylang/ethir/types/PrimitiveType.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_INTEGERTYPE_H
#include <mylang/ethir/types/IntegerType.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_CHARTYPE_H
#include <mylang/ethir/types/CharType.h>
#endif

namespace mylang {
  namespace ethir {
    namespace types {

      /** The baseclass for all types */
      class StringType: public PrimitiveType
      {

      private:
        StringType();

      public:
        ~StringType();

        /**
         * Get the bit type
         * @return a primitive type for a INTEGER
         */
      public:
        static ::std::shared_ptr<const StringType> create();

      public:
        void accept(TypeVisitor &v) const;

        bool isSameType(const Type &other) const final;

        ::std::string toString() const final;

        /** The element type */
      public:
        const ::std::shared_ptr<const CharType> element;

        /** The length type */
      public:
        const ::std::shared_ptr<const IntegerType> lengthType;

        /** The index type */
      public:
        const ::std::shared_ptr<const IntegerType> indexType;

        /** The counter type can be used to index up the array length */
      public:
        const ::std::shared_ptr<const IntegerType> counterType;
      };
    }
  }
}
#endif
