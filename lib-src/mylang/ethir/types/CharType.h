#ifndef CLASS_MYLANG_ETHIR_TYPES_CHARTYPE_H
#define CLASS_MYLANG_ETHIR_TYPES_CHARTYPE_H

#ifndef CLASS_MYLANG_ETHIR_TYPES_PRIMITIVETYPE_H
#include <mylang/ethir/types/PrimitiveType.h>
#endif

namespace mylang {
  namespace ethir {
    namespace types {

      /** The baseclass for all types */
      class CharType: public PrimitiveType
      {

      private:
        CharType();

      public:
        ~CharType();

        /**
         * Get the bit type
         * @return a primitive type for a INTEGER
         */
      public:
        static ::std::shared_ptr<const CharType> create();

      public:
        void accept(TypeVisitor &v) const;

        bool isSameType(const Type &other) const final;

        ::std::string toString() const final;
      };
    }
  }
}
#endif
