#include <mylang/ethir/types/Type.h>

namespace mylang {
  namespace ethir {
    namespace types {
      Type::Type()
          : _id(TypeName::create())
      {
      }

      Type::Type(const TypeName &xid)
          : _id(xid)
      {
      }

      Type::~Type()
      {
      }

      bool Type::isVoid() const
      {
        return false;
      }

      const HashCode& Type::hashcode() const
      {
        if (!hc.has_value()) {
          HashCode h;
          computeHashCode(h);
          hc = h;
        }
        return hc.value();
      }

      void Type::inferConstraints(RelOp, const Ptr&, Ptr&, Ptr&, Ptr&, Ptr&) const
      {
      }

      void Type::computeHashCode(HashCode &h) const
      {
        h.mix(_id.source());
      }

      bool Type::canSafeCastFrom(const Type &from) const
      {
        return isSameType(from);
      }

      bool Type::canCastFrom(const Type &from) const
      {
        return canSafeCastFrom(from);
      }

      bool Type::canCastBetween(const Type &from) const
      {
        return canCastFrom(from) && from.canCastFrom(*this);
      }

      const TypeName& Type::name() const
      {
        return _id;
      }

      Type::Ptr Type::joinWith(const Ptr &t) const
      {
        if (intersectWith(t)) {
          return unionWith(t);
        } else {
          return nullptr;
        }
      }

      Type::Ptr Type::unionWith(const Ptr &t) const
      {
        if (canSafeCastFrom(*t)) {
          return self();
        }
        if (t->canSafeCastFrom(*this)) {
          return t;
        }
        return nullptr;
      }

      Type::Ptr Type::intersectWith(const Ptr &t) const
      {
        if (isSameType(*t)) {
          return self();
        } else {
          return nullptr;
        }
      }

    }
  }
}
