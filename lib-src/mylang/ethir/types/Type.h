#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#define CLASS_MYLANG_ETHIR_TYPES_TYPE_H

#include <optional>
#include <memory>
#include <string>
#include <map>
#include <iostream>

#ifndef FILE_MYLANG_ETHIR_HASHCODE_H
#include <mylang/ethir/HashCode.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPENAME_H
#include <mylang/ethir/TypeName.h>
#endif

namespace mylang {
  namespace functions {
    class Function;

    class FunctionArgument;
  }

  namespace ethir {
    namespace types {
      class TypeSet;

      /** The type visitor */
      class TypeVisitor;

      /** The baseclass for all types */
      class Type: public ::std::enable_shared_from_this<Type>
      {
      public:
        enum RelOp
        {
          OP_EQ, OP_NEQ, OP_LT, OP_LTE
        };

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const Type> Ptr;

        /** Create new type instance */
      protected:
        Type();

        /** Create new type instance with a name.
         * @param xid an optional id */
      protected:
        Type(const TypeName &xid);

      public:
        virtual ~Type() = 0;

        /**
         * Accept the specified type visitor
         * @param v a visitor
         */
      public:
        virtual void accept(TypeVisitor &v) const = 0;

        /**
         * Get this type.
         * @return this type instance
         */
      public:
        template<class T = Type>
        inline ::std::shared_ptr<const T> self() const
        {
          return ::std::dynamic_pointer_cast<const T>(shared_from_this());
        }

        /**
         * Test if this is the void type.
         * @return true if this is the void type.
         */
      public:
        virtual bool isVoid() const;

        /**
         * Can the specified type object be safely cast to this type.
         * @param from a type from which to cast
         * @return true if a cast between two type is safe, false otherwise
         */
      public:
        virtual bool canSafeCastFrom(const Type &from) const;

        /**
         * Can the specified type object be cast to this type. The cast
         * may be safe or unsafe, but there is a way to convert between the types.
         * @param from a type from which to cast
         * @return true if it possible to cast from to this type
         */
      public:
        virtual bool canCastFrom(const Type &from) const;

        /**
         * Is it possible to cast back and forth between the two types without losing
         * information.
         * @param from a type from which to cast
         * @return true if it possible to cast from the type and to the type
         */
      public:
        bool canCastBetween(const Type &from) const;

        /**
         * Determine if this type is the same as the specified type
         * @param other another type
         * @return true if the two types are the same
         */
      public:
        virtual bool isSameType(const Type &other) const = 0;

        /**
         * Get a string representation of this type. The representation is undefined and is primarily for debugging purposes.
         * @return a string representation
         */
      public:
        virtual ::std::string toString() const = 0;

        /**
         * Get the unique name for this type
         * @return a unique name for this type
         */
      public:
        const TypeName& name() const;

        /**
         * Add a normalized copy of this type to the typeset.
         * @param ts a typeset
         * @return a instance of this type that is fully normalized
         */
      public:
        virtual Ptr normalize(TypeSet &ts) const = 0;

        /**
         * Join two types such that combined type contains exactly the same
         * information as the two individual types.
         * @param t a type
         * @return a instance of this type that is fully normalized
         */
      public:
        virtual Ptr joinWith(const Ptr &t) const;

        /**
         * Determine the union of the two types. The union of the
         * two types may represent more values than the individual types.
         * @param t a type
         * @return a instance of this type that is fully normalized
         */
      public:
        virtual Ptr unionWith(const Ptr &t) const;

        /**
         * Intersection of this type with the specified type.
         * @param t a type
         * @return the intersection of this and this
         */
      public:
        virtual Ptr intersectWith(const Ptr &t) const;

        /**
         * Print the specified type.
         * @param out an output stream
         * @param type a type pointer
         * @return out
         */
      public:
        friend inline ::std::ostream& operator<<(::std::ostream &out, const Ptr &type)
        {
          if (type) {
            out << type->toString();
          }
          return out;
        }

        /**
         * Get the hashcode for this node.
         * @return the hashcode for this node
         */
      public:
        const HashCode& hashcode() const;

        /** The types resulting from comparison functions. Assuming a comparison
         * is true, then we can make some statements on the bounds of both input types,
         * and likewise, we can make similar statements if we assume the result is true.
         * The output types are unchanged if no constraints could be derived.
         * @param op a relation operator
         * @param right the type of the right operator
         * @param leftIfTrue the type that left must have been if (left op right) == true
         * @param leftIfFalse the type that left must have been if (left op right) == false
         * @param rightIfTrue the type that left must have been if (left op right) == true
         * @param rightIfFalse the type that left must have been if (left op right) == false
         */
      public:
        virtual void inferConstraints(RelOp op, const Ptr &right, Ptr &leftIfTrue, Ptr &leftIfFalse,
            Ptr &rightIfTrue, Ptr &rightIfFalse) const;

        /**
         * Compute the hashcode for this node. The default hashcode hashes the address of this
         * object.
         * @return the hashcode for this node
         */
      protected:
        virtual void computeHashCode(HashCode &hc) const;

        /** A unique id for this type; the id is NOT used to determine if two types are the same */
      private:
        const TypeName _id;

        /** The hashcode */
      private:
        mutable ::std::optional<HashCode> hc;
      };
    }
  }
}
#endif
