#include <mylang/ethir/types/ProcessType.h>
#include <mylang/ethir/types/TypeVisitor.h>
#include <mylang/ethir/types/TypeSet.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace types {

      ProcessType::ProcessType(Ports xports)
          : ports(::std::move(xports))
      {
        for (auto io : ports) {
          if (io.second == nullptr) {
            throw ::std::invalid_argument("null value for input " + io.first);
          }
        }
      }

      ProcessType::~ProcessType()
      {
      }

      ::std::string ProcessType::toString() const
      {
        ::std::string decl;
        for (auto p : ports) {
          decl += p.first;
          decl += ':';
          decl += p.second->toString();
        }
        return "process{" + decl + "}";
      }

      Type::Ptr ProcessType::normalize(TypeSet &ts) const
      {
        Ports p;

        for (auto port : ports) {
          p[port.first] = port.second->normalize(ts)->self<InputOutputType>();
        }
        return ts.add(get(p));
      }

      bool ProcessType::isSameType(const Type &other) const
      {
        auto that = dynamic_cast<const ProcessType*>(&other);
        if (!that || that->ports.size() != ports.size()) {
          return false;
        }
        for (auto i : that->ports) {
          auto tmp = ports.find(i.first);
          if (tmp == ports.end()) {
            return false;
          }
          if (!tmp->second->isSameType(*i.second)) {
            return false;
          }
        }
        return true;
      }

      void ProcessType::accept(TypeVisitor &v) const
      {
        v.visit(::std::dynamic_pointer_cast<const ProcessType>(self()));
      }

      ::std::shared_ptr<const ProcessType> ProcessType::get(Ports xports)
      {
        struct Impl: public ProcessType
        {
          Impl(Ports xports)
              : ProcessType(xports)
          {
          }

          ~Impl()
          {
          }

        };
        return ::std::make_shared<const Impl>(xports);
      }
    }
  }
}
