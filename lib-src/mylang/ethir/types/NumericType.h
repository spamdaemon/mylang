#ifndef CLASS_MYLANG_ETHIR_TYPES_NUMERICTYPE_H
#define CLASS_MYLANG_ETHIR_TYPES_NUMERICTYPE_H

#ifndef CLASS_MYLANG_ETHIR_TYPES_PRIMITIVETYPE_H
#include <mylang/ethir/types/PrimitiveType.h>
#endif

namespace mylang {
  namespace ethir {
    namespace types {

      class NumericType: public PrimitiveType
      {

      protected:
        NumericType();

      public:
        ~NumericType();
      };
    }
  }
}
#endif
