#ifndef CLASS_MYLANG_ETHIR_TYPES_BUILDERTYPE_H
#define CLASS_MYLANG_ETHIR_TYPES_BUILDERTYPE_H

#ifndef CLASS_MYLANG_ETHIR_TYPES_ARRAYTYPE_H
#include <mylang/ethir/types/ArrayType.h>
#endif

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#include <memory>
#include <optional>

namespace mylang {
  namespace ethir {
    namespace types {

      /** A type representing an array */
      class BuilderType: public Type
      {
        /** The output type */
      public:
        typedef ArrayType ProductType;

        /** The output type */
      public:
        typedef ::std::shared_ptr<const ProductType> ProductTypePtr;

      private:
        BuilderType(ProductTypePtr ptr);

      public:
        ~BuilderType();

        /**
         * Create a new array with the specified element type. The element
         * type may an any element, otherwise be concrete.
         * @param xelement the element
         */
      public:
        static ::std::shared_ptr<const BuilderType> get(ProductTypePtr ptr);

        void accept(TypeVisitor &v) const override final;
        bool isSameType(const Type &other) const override final;
        ::std::string toString() const override final;
        Ptr normalize(TypeSet &ts) const override final;

        /** The target for the build */
      public:
        const ProductTypePtr product;

      };
    }
  }
}
#endif
