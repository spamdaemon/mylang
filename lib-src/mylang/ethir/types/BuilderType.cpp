#include <mylang/ethir/types/BuilderType.h>
#include <mylang/ethir/types/TypeVisitor.h>
#include <mylang/ethir/types/TypeSet.h>
#include <cstddef>
#include <string>
#include <utility>

namespace mylang {
  namespace ethir {
    namespace types {

      BuilderType::BuilderType(ProductTypePtr ty)
          : product(ty)
      {
      }

      BuilderType::~BuilderType()
      {
      }

      Type::Ptr BuilderType::normalize(TypeSet &ts) const
      {
        return BuilderType::get(product->normalize(ts)->self<ProductType>());
      }

      void BuilderType::accept(TypeVisitor &v) const
      {
        v.visit(::std::dynamic_pointer_cast<const BuilderType>(self()));
      }

      ::std::string BuilderType::toString() const
      {
        ::std::string res;
        res += "builder{";
        res += product->toString();
        res += '}';
        return res;
      }

      bool BuilderType::isSameType(const Type &other) const
      {
        auto that = dynamic_cast<const BuilderType*>(&other);
        if (!that) {
          return false;
        }
        return product->isSameType(*that->product);
      }

      ::std::shared_ptr<const BuilderType> BuilderType::get(ProductTypePtr xp)
      {
        struct Impl: public BuilderType
        {
          Impl(ProductTypePtr xp)
              : BuilderType(xp)
          {
          }

          ~Impl()
          {
          }
        };
        return ::std::make_shared<const Impl>(xp);
      }
    }
  }
}
