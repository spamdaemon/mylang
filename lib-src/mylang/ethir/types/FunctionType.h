#ifndef CLASS_MYLANG_ETHIR_TYPES_FUNCTIONTYPE_H
#define CLASS_MYLANG_ETHIR_TYPES_FUNCTIONTYPE_H

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#include <mylang/ethir/types/Type.h>
#endif
#include <memory>
#include <vector>

namespace mylang {
  namespace ethir {
    namespace types {

      /** A type representing an error */
      class FunctionType: public Type
      {
      public:
        typedef ::std::vector<Type::Ptr> Parameters;
        typedef ::std::shared_ptr<const FunctionType> FunctionTypePtr;

      private:
        FunctionType(const Type::Ptr &ret, const Parameters &xparameters);

      public:
        ~FunctionType();

        /** Two primitives are the same if their type ids are the same  */
      public:
        Ptr normalize(TypeSet &ts) const;

        void accept(TypeVisitor &v) const;

        bool isSameType(const Type &other) const final;

        ::std::string toString() const final;

        bool canSafeCastFrom(const Type &t) const override;

        bool canCastFrom(const Type &t) const override;

        /**
         * Test if the return type is a void.
         */
      public:
        inline bool isVoid() const
        {
          return returnType->isVoid();
        }

        /**
         * Check if the specified parameters match this signature.
         * @param params a parameters object
         */
      public:
        bool matchParameters(const Parameters &params) const;

        /**
         * Create a function type.
         * @param res the return type
         * @param parameters the positional parameters
         */
      public:
        static FunctionTypePtr get(const Type::Ptr &ret, const Parameters &xparameters);

        /** The return type */
      public:
        const Type::Ptr returnType;

        /** The return type */
      public:
        const Parameters parameters;
      };
    }
  }
}
#endif
