#ifndef CLASS_MYLANG_ETHIR_TYPES_ABSTRACTTYPEVISITOR_H
#define CLASS_MYLANG_ETHIR_TYPES_ABSTRACTTYPEVISITOR_H

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPEVISITOR_H
#include <mylang/ethir/types/TypeVisitor.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPENAME_H
#include <mylang/ethir/TypeName.h>
#endif

#include <set>

namespace mylang {
  namespace ethir {
    namespace types {

      /** A type visitor */
      class AbstractTypeVisitor: public TypeVisitor
      {

        /**
         * The constructor
         * @param recurse true if the visitor should recurse
         */
      protected:
        AbstractTypeVisitor(bool recurse);

        /** Destructor */
      public:
        ~AbstractTypeVisitor();

      public:
        void visit(const ::std::shared_ptr<const BuilderType> &type) override;

        void visit(const ::std::shared_ptr<const BitType> &type) override;

        void visit(const ::std::shared_ptr<const BooleanType> &type) override;

        void visit(const ::std::shared_ptr<const ByteType> &type) override;

        void visit(const ::std::shared_ptr<const CharType> &type) override;

        void visit(const ::std::shared_ptr<const RealType> &type) override;

        void visit(const ::std::shared_ptr<const IntegerType> &type) override;

        void visit(const ::std::shared_ptr<const StringType> &type) override;

        void visit(const ::std::shared_ptr<const VoidType> &type) override;

        void visit(const ::std::shared_ptr<const GenericType> &type) override;

        void visit(const ::std::shared_ptr<const ArrayType> &type) override;

        void visit(const ::std::shared_ptr<const UnionType> &type) override;

        void visit(const ::std::shared_ptr<const StructType> &type) override;

        void visit(const ::std::shared_ptr<const TupleType> &type) override;

        void visit(const ::std::shared_ptr<const FunctionType> &type) override;

        void visit(const ::std::shared_ptr<const MutableType> &type) override;

        void visit(const ::std::shared_ptr<const OptType> &type) override;

        void visit(const ::std::shared_ptr<const NamedType> &type) override;

        void visit(const ::std::shared_ptr<const InputType> &type) override;

        void visit(const ::std::shared_ptr<const OutputType> &type) override;

        void visit(const ::std::shared_ptr<const ProcessType> &type) override;

        /** The already seen type names if we're recursing */
      private:
        ::std::unique_ptr<::std::set<TypeName>> recurse;
      };
    }
  }

}
#endif 
