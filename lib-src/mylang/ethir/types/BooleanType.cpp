#include <mylang/ethir/types/BooleanType.h>
#include <mylang/ethir/types/TypeVisitor.h>

namespace mylang {
  namespace ethir {
    namespace types {

      BooleanType::BooleanType()
      {
      }

      BooleanType::~BooleanType()
      {
      }

      void BooleanType::accept(TypeVisitor &v) const
      {
        v.visit(::std::dynamic_pointer_cast<const BooleanType>(self()));
      }

      bool BooleanType::isSameType(const Type &other) const
      {
        return dynamic_cast<const BooleanType*>(&other) != nullptr;
      }

      ::std::string BooleanType::toString() const
      {
        return "boolean";
      }

      ::std::shared_ptr<const BooleanType> BooleanType::create()
      {
        struct Impl: public BooleanType
        {
          ~Impl()
          {
          }
        };

        return ::std::make_shared<Impl>();
      }

    }
  }
}
