#include <mylang/ethir/types/InputType.h>
#include <mylang/ethir/types/TypeVisitor.h>
#include <mylang/ethir/types/TypeSet.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace types {

      InputType::InputType(::std::shared_ptr<const Type> xelement)
          : InputOutputType(xelement)
      {
      }

      InputType::~InputType()
      {
      }

      ::std::string InputType::toString() const
      {
        return "=>" + element->toString();
      }

      Type::Ptr InputType::normalize(TypeSet &ts) const
      {
        return ts.add(get(element->normalize(ts)));
      }

      bool InputType::isSameType(const Type &other) const
      {
        auto that = dynamic_cast<const InputType*>(&other);
        if (!that) {
          return false;
        }
        return element->isSameType(*that->element);
      }

      void InputType::accept(TypeVisitor &v) const
      {
        v.visit(self<InputType>());
      }

      ::std::shared_ptr<const InputType> InputType::get(::std::shared_ptr<const Type> xelement)
      {
        struct Impl: public InputType
        {
          Impl(::std::shared_ptr<const Type> t)
              : InputType(t)
          {
          }

          ~Impl()
          {
          }

        };
        return ::std::make_shared<const Impl>(xelement);
      }
    }
  }
}
