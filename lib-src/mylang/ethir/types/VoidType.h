#ifndef CLASS_MYLANG_ETHIR_TYPES_VOIDTYPE_H
#define CLASS_MYLANG_ETHIR_TYPES_VOIDTYPE_H

#ifndef CLASS_MYLANG_ETHIR_TYPES_PRIMITIVETYPE_H
#include <mylang/ethir/types/PrimitiveType.h>
#endif
#include <memory>

namespace mylang {
  namespace ethir {
    namespace types {

      /** A type representing an error */
      class VoidType: public PrimitiveType
      {

      private:
        VoidType();

      public:
        ~VoidType();

        /** Two primitives are the same if their type ids are the same  */
      public:
        void accept(TypeVisitor &v) const override final;
        bool isVoid() const override final;

        bool isSameType(const Type &other) const override final;

        ::std::string toString() const override final;

      public:
        static ::std::shared_ptr<const VoidType> create();
      };
    }
  }
}
#endif
