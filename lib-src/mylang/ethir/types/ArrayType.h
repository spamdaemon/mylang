#ifndef CLASS_MYLANG_ETHIR_TYPES_ARRAYTYPE_H
#define CLASS_MYLANG_ETHIR_TYPES_ARRAYTYPE_H

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#include <mylang/ethir/types/Type.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_INTEGERTYPE_H
#include <mylang/ethir/types/IntegerType.h>
#endif

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_INTERVAL_H
#include <mylang/Interval.h>
#endif

#include <memory>
#include <optional>

namespace mylang {
  namespace ethir {
    namespace types {

      /** A type representing an array */
      class ArrayType: public Type
      {
      public:
        typedef ::std::shared_ptr<const IntegerType> IntType;

      public:
        using Bounds = Interval;

      private:
        ArrayType(const Ptr &xelement, IntType xlengthType, ::std::optional<IntType> xindexType,
            IntType xcounterType);

      public:
        ~ArrayType();

        /**
         * Follow the element type until an element is found that is not a optional
         * @return an element type (maybe nullptr!)
         */
      public:
        Type::Ptr flatten() const;

        /**
         * Create a new array with the specified element type. The element
         * type may an any element, otherwise be concrete.
         * @param xelement the element
         */
      public:
        static ::std::shared_ptr<const ArrayType> get(::std::shared_ptr<const Type> xelement,
            Integer minSize, Integer maxSize);
        static ::std::shared_ptr<const ArrayType> get(::std::shared_ptr<const Type> xelement,
            const Bounds &b);

        /**
         * Create an array whose min and max length is defined by the given length type.
         * @param element the element type
         * @param lengthType the type that defines the length of the array
         * @param indexType the type that defines the valid indices (may be empty)
         * @param counterType the type that defines the counter
         * @throws invalid_argument if the indexType or counterType are not consistent with the lengthType
         */
        static ::std::shared_ptr<const ArrayType> get(::std::shared_ptr<const Type> xelement,
            IntType lengthType, ::std::optional<IntType> indexType, IntType counterType);

        bool canSafeCastFrom(const Type &t) const override;

        bool canCastFrom(const Type &t) const override;

        Ptr normalize(TypeSet &ts) const override;
        Ptr unionWith(const Ptr &t) const override;
        Ptr intersectWith(const Ptr &t) const override;

        void accept(TypeVisitor &v) const override;

        ::std::string toString() const override;

        bool isSameType(const Type &other) const override;

        /**
         * Determine the dimension of this array. If the element is also an array
         * then the dimension is increased by 1.
         */
      public:
        size_t dimensionality() const;

        /**
         * Get the leaf element.
         */
      public:
        Ptr leafElement() const;

        /**
         * Create a new instanceof of this array, but change the return type
         * @param element the new element type
         */
      public:
        virtual ::std::shared_ptr<const ArrayType> copy(const Type::Ptr &newElement) const;

        /** The element type (never null) */
      public:
        const Type::Ptr element;

        /** The minimum number of elements */
      public:
        const Integer minSize;

        /** The maximum number of elements allowed */
      public:
        const Integer maxSize;

        /** The range */
      public:
        const Bounds bounds;

        /** The length type */
      public:
        const ::std::shared_ptr<const IntegerType> lengthType;

        /** The index type */
      public:
        const ::std::optional<::std::shared_ptr<const IntegerType>> indexType;

        /** The counter type can be used to index up the array length */
      public:
        const ::std::shared_ptr<const IntegerType> counterType;
      };
    }
  }
}
#endif
