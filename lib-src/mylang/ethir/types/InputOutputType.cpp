#include <mylang/ethir/types/InputOutputType.h>

namespace mylang {
  namespace ethir {
    namespace types {

      InputOutputType::InputOutputType(::std::shared_ptr<const Type> xelement)
          : element(xelement)
      {
        if (!xelement) {
          throw ::std::invalid_argument("Null element");
        }
      }

      InputOutputType::~InputOutputType()
      {
      }
    }
  }
}
