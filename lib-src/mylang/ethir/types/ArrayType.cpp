#include <mylang/ethir/types/ArrayType.h>
#include <mylang/ethir/types/TypeVisitor.h>
#include <mylang/ethir/types/TypeSet.h>
#include <cstddef>
#include <string>
#include <vector>
#include <cassert>
#include <utility>

namespace mylang {
  namespace ethir {
    namespace types {

      ArrayType::ArrayType(const Ptr &xelement, IntType xlengthType,
          ::std::optional<IntType> xindexType, IntType xcounterType)
          : element(xelement), minSize(xlengthType->range.min()), maxSize(xlengthType->range.max()),
              bounds(xlengthType->range.min(), xlengthType->range.max()), lengthType(xlengthType),
              indexType(xindexType), counterType(xcounterType)
      {
        if (!xelement) {
          throw ::std::invalid_argument("Null element");
        }
        if (maxSize < 0) {
          // TODO: this needs an explanation
          throw ::std::invalid_argument("Invalid max array size");
        }

        if (maxSize < minSize) {
          throw ::std::invalid_argument("Invalid array bounds");
        }
      }

      ArrayType::~ArrayType()
      {
      }

      size_t ArrayType::dimensionality() const
      {
        size_t dim = 0;
        auto c = self<ArrayType>();
        while (c) {
          ++dim;
          c = c->element->self<ArrayType>();
        }
        return dim;
      }

      Type::Ptr ArrayType::leafElement() const
      {
        Ptr res;
        auto c = self<ArrayType>();
        while (c) {
          res = c->element;
          c = res->self<ArrayType>();
        }
        assert(res);
        return res;
      }

      Type::Ptr ArrayType::normalize(TypeSet &ts) const
      {
        auto e = element->normalize(ts);
        return ArrayType::get(e, minSize, maxSize);
      }

      ArrayType::Ptr ArrayType::unionWith(const Ptr &t) const
      {
        auto that = t->self<ArrayType>();
        if (that) {
          auto elemTy = that->element->unionWith(element);
          if (elemTy) {
            auto iBounds = bounds.unionWith(that->bounds);
            if (bounds == iBounds && elemTy->isSameType(*element)) {
              return self<ArrayType>();
            } else if (that->bounds == iBounds && elemTy->isSameType(*that->element)) {
              return that;
            } else {
              return get(elemTy, bounds);
            }
          }
        }
        return nullptr;
      }

      ArrayType::Ptr ArrayType::intersectWith(const Ptr &t) const
      {
        auto that = t->self<ArrayType>();
        if (that) {
          auto elemTy = that->element->intersectWith(element);
          if (elemTy) {
            auto iBounds = bounds.intersectWith(that->bounds);
            if (bounds == iBounds && elemTy->isSameType(*element)) {
              return self<ArrayType>();
            } else if (that->bounds == iBounds && elemTy->isSameType(*that->element)) {
              return that;
            } else {
              return get(elemTy, bounds);
            }
          }
        }
        return nullptr;
      }

      void ArrayType::accept(TypeVisitor &v) const
      {
        v.visit(::std::dynamic_pointer_cast<const ArrayType>(self()));
      }

      Type::Ptr ArrayType::flatten() const
      {
        auto arr = ::std::dynamic_pointer_cast<const ArrayType>(element);
        if (arr) {
          return arr->flatten();
        } else {
          return element;
        }
      }

      ::std::string ArrayType::toString() const
      {
        ::std::string res;
        res += element->toString();
        res += '[';
        res += bounds.toString();
        res += ']';
        return res;
      }

      bool ArrayType::isSameType(const Type &other) const
      {
        if (&other == this) {
          return true;
        }
        auto that = dynamic_cast<const ArrayType*>(&other);
        if (!that) {
          return false;
        }
        return bounds == that->bounds && element->isSameType(*that->element);
      }

      bool ArrayType::canSafeCastFrom(const Type &from) const
      {
        auto other = dynamic_cast<const ArrayType*>(&from);
        if (!other) {
          return Type::canSafeCastFrom(from);
        }

        if (!bounds.contains(other->bounds)) {
          return false;
        }
        return element->canSafeCastFrom(*other->element);
      }

      bool ArrayType::canCastFrom(const Type &from) const
      {
        auto other = dynamic_cast<const ArrayType*>(&from);
        if (!other) {
          return Type::canCastFrom(from);
        }
        if (!bounds.intersectWith(other->bounds).has_value()) {
          return false;
        }
        return element->canCastFrom(*other->element);
      }

      ::std::shared_ptr<const ArrayType> ArrayType::copy(
          const ::std::shared_ptr<const Type> &newElement) const
      {
        return get(newElement, minSize, maxSize);
      }

      ::std::shared_ptr<const ArrayType> ArrayType::get(::std::shared_ptr<const Type> xelement,
          IntType xlengthType, ::std::optional<IntType> xindexType, IntType xcounterType)
      {
        struct Impl: public ArrayType
        {
          Impl(::std::shared_ptr<const Type> xelement, IntType xlengthType,
              ::std::optional<IntType> xindexType, IntType xcounterType)
              : ArrayType(xelement, xlengthType, xindexType, xcounterType)
          {
          }

          ~Impl()
          {
          }
        };

        // NOTE: min of index and counters could be <0, e.g. if choose a signed int to represent them!
        // make sure index and counter types are consistent with the length type
        if (xindexType.has_value() && !xindexType.value()->range.contains(0)) {
          throw ::std::invalid_argument("Invalid index type");
        }
        if (!xcounterType->range.contains(0)) {
          throw ::std::invalid_argument("Invalid counter type");
        }
        if (xlengthType->range.max().isFinite() && xcounterType->range.max() < 0) {
          throw ::std::invalid_argument("Invalid length type");
        }

        if (xlengthType->range.max().isFinite()) {
          if (xindexType.has_value() && !xindexType.value()->range.max().isFinite()) {
            throw ::std::invalid_argument("Invalid index type");
          }
          if (!xcounterType->range.max().isFinite()) {
            throw ::std::invalid_argument("Invalid counter type");
          }

          if (xindexType.has_value()
              && xindexType.value()->range.max() > xlengthType->range.max()) {
            throw ::std::invalid_argument("Invalid index type");
          }
          if (xcounterType->range.max() != xlengthType->range.max()) {
            throw ::std::invalid_argument("Invalid counter type");
          }
        } else {
          if (xindexType.value()->range.max().isFinite()) {
            throw ::std::invalid_argument("Invalid index type");
          }
          if (xcounterType->range.max().isFinite()) {
            throw ::std::invalid_argument("Invalid counter type");
          }
        }

        return ::std::make_shared<const Impl>(xelement, xlengthType, xindexType, xcounterType);
      }

      ::std::shared_ptr<const ArrayType> ArrayType::get(::std::shared_ptr<const Type> xelement,
          const Bounds &b)
      {
        return get(xelement, b.min(), b.max());
      }

      ::std::shared_ptr<const ArrayType> ArrayType::get(::std::shared_ptr<const Type> xelement,
          Integer xminSize, Integer xmaxSize)
      {
        struct Impl: public ArrayType
        {
          Impl(::std::shared_ptr<const Type> xelement, IntType xlengthType,
              ::std::optional<IntType> xindexType, IntType xcounterType)
              : ArrayType(xelement, xlengthType, xindexType, xcounterType)
          {
          }

          ~Impl()
          {
          }
        };

        // since the types are derived by construction we don't need to validate them
        // and so we'll just use a new constructor
        auto xlengthType = IntegerType::create(xminSize, xmaxSize);
        ::std::optional<IntType> xindexType;
        if (xmaxSize != 0) {
          xindexType = IntegerType::create(Integer::ZERO(), xmaxSize.towards0());
        }
        auto xcounterType = IntegerType::create(0, xmaxSize);

        return ::std::make_shared<const Impl>(xelement, xlengthType, xindexType, xcounterType);
      }
    }
  }
}
