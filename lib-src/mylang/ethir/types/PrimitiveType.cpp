#include <mylang/ethir/types/PrimitiveType.h>
#include <mylang/ethir/types/TypeVisitor.h>
#include <mylang/ethir/types/BitType.h>
#include <mylang/ethir/types/BooleanType.h>
#include <mylang/ethir/types/ByteType.h>
#include <mylang/ethir/types/CharType.h>
#include <mylang/ethir/types/RealType.h>
#include <mylang/ethir/types/IntegerType.h>
#include <mylang/ethir/types/StringType.h>
#include <mylang/ethir/types/TypeSet.h>

namespace mylang {
  namespace ethir {
    namespace types {

      PrimitiveType::PrimitiveType()
      {
      }

      PrimitiveType::~PrimitiveType()
      {
      }

      Type::Ptr PrimitiveType::normalize(TypeSet &ts) const
      {
        return ts.add(self());
      }

      ::std::shared_ptr<const PrimitiveType> PrimitiveType::getBit()
      {
        return BitType::create();
      }

      ::std::shared_ptr<const PrimitiveType> PrimitiveType::getBoolean()
      {
        return BooleanType::create();
      }

      ::std::shared_ptr<const PrimitiveType> PrimitiveType::getByte()
      {
        return ByteType::create();
      }

      ::std::shared_ptr<const PrimitiveType> PrimitiveType::getChar()
      {
        return CharType::create();
      }

      ::std::shared_ptr<const PrimitiveType> PrimitiveType::getReal()
      {
        return RealType::create();
      }

      ::std::shared_ptr<const PrimitiveType> PrimitiveType::getInteger(const IntegerType::Range &r)
      {
        return IntegerType::create(r);
      }

      ::std::shared_ptr<const PrimitiveType> PrimitiveType::getString()
      {
        return StringType::create();
      }

      ::std::shared_ptr<const PrimitiveType> PrimitiveType::getVoid()
      {
        return VoidType::create();
      }

    }
  }
}
