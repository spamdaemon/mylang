#include <mylang/ethir/types/OptType.h>
#include <mylang/ethir/types/TypeVisitor.h>
#include <mylang/ethir/types/TypeSet.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace types {

      OptType::OptType(::std::shared_ptr<const Type> xelement)
          : element(xelement)
      {
        if (!xelement) {
          throw ::std::invalid_argument("Null element");
        }
      }

      OptType::~OptType()
      {
      }

      Type::Ptr OptType::flatten() const
      {
        auto opt = ::std::dynamic_pointer_cast<const OptType>(element);
        if (opt) {
          return opt->flatten();
        } else {
          return element;
        }
      }

      bool OptType::canSafeCastFrom(const Type &from) const
      {
        auto other = dynamic_cast<const OptType*>(&from);
        if (!other) {
          return Type::canSafeCastFrom(from);
        }
        return element->canSafeCastFrom(*other->element);
      }

      bool OptType::canCastFrom(const Type &from) const
      {
        auto other = dynamic_cast<const OptType*>(&from);
        if (!other) {
          return Type::canCastFrom(from);
        }
        return element->canCastFrom(*other->element);
      }

      ::std::string OptType::toString() const
      {
        return element->toString() + '?';
      }

      Type::Ptr OptType::normalize(TypeSet &ts) const
      {
        return ts.add(get(element->normalize(ts)));
      }

      bool OptType::isSameType(const Type &other) const
      {
        auto that = dynamic_cast<const OptType*>(&other);
        if (!that) {
          return false;
        }
        return element->isSameType(*that->element);
      }

      ::std::shared_ptr<const OptType> OptType::copy(
          const ::std::shared_ptr<const Type> &newElement) const
      {
        return get(newElement);
      }

      void OptType::accept(TypeVisitor &v) const
      {
        v.visit(::std::dynamic_pointer_cast<const OptType>(self()));
      }

      ::std::shared_ptr<const OptType> OptType::get(::std::shared_ptr<const Type> xelement)
      {
        struct Impl: public OptType
        {
          Impl(::std::shared_ptr<const Type> t)
              : OptType(t)
          {
          }

          ~Impl()
          {
          }

        };
        return ::std::make_shared<const Impl>(xelement);
      }

    }
  }
}
