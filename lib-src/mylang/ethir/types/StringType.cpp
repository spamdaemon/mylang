#include <mylang/ethir/types/StringType.h>
#include <mylang/ethir/types/TypeVisitor.h>

namespace mylang {
  namespace ethir {
    namespace types {

      StringType::StringType()
          : element(CharType::create()), lengthType(IntegerType::create(Interval::NON_NEGATIVE())),
              indexType(IntegerType::create(Interval::NON_NEGATIVE())),
              counterType(IntegerType::unionOf(lengthType, indexType))

      {
      }

      StringType::~StringType()
      {
      }

      void StringType::accept(TypeVisitor &v) const
      {
        v.visit(::std::dynamic_pointer_cast<const StringType>(self()));
      }

      bool StringType::isSameType(const Type &other) const
      {
        return dynamic_cast<const StringType*>(&other) != nullptr;
      }

      ::std::string StringType::toString() const
      {
        return "string";
      }

      ::std::shared_ptr<const StringType> StringType::create()
      {
        struct Impl: public StringType
        {
          ~Impl()
          {
          }
        };

        return ::std::make_shared<Impl>();
      }

    }
  }
}
