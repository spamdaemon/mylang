#include <mylang/BigInt.h>
#include <mylang/ethir/types/IntegerType.h>
#include <mylang/ethir/types/Type.h>
#include <mylang/ethir/types/TypeVisitor.h>
#include <algorithm>
#include <memory>
#include <optional>
#include <stdexcept>
#include <string>

namespace mylang {
  namespace ethir {
    namespace types {
      static IntegerType::Ptr getEqBound(const IntegerType::Range &left,
          const IntegerType::Range &right)
      {
        auto t = left.intersectWith(right);
        if (t.has_value()) {
          return IntegerType::create(t.value());
        }
        return nullptr;
      }
      static IntegerType::Ptr getNeqBound(const IntegerType::Range &left,
          const IntegerType::Range &right)
      {
        auto t = left.intersectWith(right);
        if (t.has_value()) {
          return IntegerType::create(left);
        }
        return nullptr;
      }

      static IntegerType::Ptr getLtBound(const IntegerType::Range &left,
          const IntegerType::Range &right)
      {
        auto t = left.intersectWith(right);
        if (t.has_value()) {
          auto from = left.min();
          auto to = t->max().min(right.max().decrement());
          if (from <= to) {
            return IntegerType::create(from, to);
          }
        }
        return nullptr;
      }

      static IntegerType::Ptr getLteBound(const IntegerType::Range &left,
          const IntegerType::Range &right)
      {
        auto t = left.intersectWith(right);
        if (t.has_value()) {
          return IntegerType::create(left.min(), t->max());
        }
        return nullptr;
      }

      static IntegerType::Ptr getGtBound(const IntegerType::Range &left,
          const IntegerType::Range &right)
      {
        auto t = left.intersectWith(right);
        if (t.has_value()) {
          auto from = t->min().max(right.min().increment());
          auto to = left.max();
          if (from <= to) {
            return IntegerType::create(from, to);
          }
        }
        return nullptr;
      }

      static IntegerType::Ptr getGteBound(const IntegerType::Range &left,
          const IntegerType::Range &right)
      {
        auto t = left.intersectWith(right);
        if (t.has_value()) {
          return IntegerType::create(t->min(), left.max());
        }
        return nullptr;
      }

      IntegerType::IntegerType(Range xrange)
          : range(xrange)
      {
      }

      IntegerType::~IntegerType()
      {
      }

      void IntegerType::inferConstraints(RelOp op, const Ptr &right, Ptr &leftIfTrue,
          Ptr &leftIfFalse, Ptr &rightIfTrue, Ptr &rightIfFalse) const
      {
        auto r = right->self<IntegerType>();
        if (!r) {
          return;
        }

        switch (op) {
        case OP_EQ:
          leftIfTrue = getEqBound(range, r->range);
          rightIfTrue = getEqBound(r->range, range);
          leftIfFalse = getNeqBound(range, r->range);
          rightIfFalse = getNeqBound(r->range, range);
          break;
        case OP_NEQ:
          leftIfTrue = getNeqBound(range, r->range);
          rightIfTrue = getNeqBound(r->range, range);
          leftIfFalse = getEqBound(range, r->range);
          rightIfFalse = getEqBound(r->range, range);
          break;
        case OP_LT:
          leftIfTrue = getLtBound(range, r->range);
          rightIfTrue = getGtBound(r->range, range);
          leftIfFalse = getGteBound(range, r->range);
          rightIfFalse = getLteBound(r->range, range);
          break;
        case OP_LTE:
          leftIfTrue = getLteBound(range, r->range);
          rightIfTrue = getGteBound(r->range, range);
          leftIfFalse = getGtBound(range, r->range);
          rightIfFalse = getLtBound(r->range, range);
          break;
        default:
          throw ::std::runtime_error("Unexpected ");
        }

#if 0
        ::std::cerr << "**Infer constraints : " << ::std::endl << "left : " << this->toString()
            << ::std::endl << "right : " << right->toString() << ::std::endl << "leftIfTrue : "
            << leftIfTrue->toString() << ::std::endl << "rightIfTrue : " << rightIfTrue->toString()
            << ::std::endl << "leftIfFalse : " << leftIfFalse->toString() << ::std::endl
            << "rightIfFalse : " << rightIfFalse->toString() << ::std::endl << ::std::endl;
#endif
      }

      void IntegerType::accept(TypeVisitor &v) const
      {
        v.visit(::std::dynamic_pointer_cast<const IntegerType>(self()));
      }

      IntegerType::Ptr IntegerType::joinWith(const Ptr &t) const
      {
        auto that = t->self<IntegerType>();
        if (that) {
          auto thisTy = self<IntegerType>();
          auto rng = range.joinWith(that->range);
          if (rng) {
            return create(rng.value());
          }
        }
        return nullptr;
      }

      IntegerType::Ptr IntegerType::unionWith(const Ptr &t) const
      {
        auto that = t->self<IntegerType>();
        if (that) {
          auto thisTy = self<IntegerType>();
          return unionOf(thisTy, that);
        }
        return Type::unionWith(t);
      }

      IntegerType::Ptr IntegerType::intersectWith(const Ptr &t) const
      {
        auto that = t->self<IntegerType>();
        if (that) {
          auto thisTy = self<IntegerType>();
          return intersectionOf(thisTy, that);
        }
        return nullptr;
      }

      bool IntegerType::isSameType(const Type &other) const
      {
        if (this == &other) {
          return true;
        }
        auto that = dynamic_cast<const IntegerType*>(&other);
        if (!that) {
          return false;
        }
        return that->range == range;
      }

      bool IntegerType::canSafeCastFrom(const Type &from) const
      {
        auto other = dynamic_cast<const IntegerType*>(&from);
        if (!other) {
          return Type::canSafeCastFrom(from);
        }

        return range.contains(other->range);
      }

      bool IntegerType::canCastFrom(const Type &from) const
      {
        auto other = dynamic_cast<const IntegerType*>(&from);
        if (!other) {
          return Type::canCastFrom(from);
        }
        return range.intersectWith(other->range).has_value();
      }

      ::std::string IntegerType::toString() const
      {
        ::std::string res;
        res += "integer";
        if (range.min().isFinite() || range.max().isFinite()) {
          res += '{';
          const char *sep = "";
          if (range.min().isFinite()) {
            res += sep;
            res += "min:";
            res += range.min()->toString();
            sep = ";";
          }
          if (range.max().isFinite()) {
            res += sep;
            res += "max:";
            res += range.max()->toString();
          }
          res += '}';
        }
        return res;
      }

      IntegerType::IntegerTypePtr IntegerType::create(const Range &r)
      {
        struct Impl: public IntegerType
        {
          Impl(const Range &r)
              : IntegerType(r)
          {
          }

          ~Impl()
          {
          }
        };

        return ::std::make_shared<Impl>(r);
      }

      IntegerType::IntegerTypePtr IntegerType::create(Integer min, Integer max)
      {
        struct Impl: public IntegerType
        {
          Impl(Integer min, Integer max)
              : IntegerType(Range(min, max))
          {
          }

          ~Impl()
          {
          }
        };

        return ::std::make_shared<Impl>(min, max);
      }

      IntegerType::IntegerTypePtr IntegerType::create()
      {
        return create(Interval::INFINITE());
      }

      IntegerType::IntegerTypePtr IntegerType::unionOf(IntegerTypePtr a, IntegerTypePtr b)
      {
        auto r = a->range.unionWith(b->range);
        if (r == a->range) {
          return a;
        }
        if (r == b->range) {
          return b;
        }
        return create(r);
      }

      IntegerType::IntegerTypePtr IntegerType::intersectionOf(IntegerTypePtr a, IntegerTypePtr b)
      {
        auto r = a->range.intersectWith(b->range);
        if (r.has_value() == false) {
          return nullptr;
        }
        if (r.value() == a->range) {
          return a;
        }
        if (r.value() == b->range) {
          return b;
        }
        return create(r.value());
      }

      IntegerType::IntegerTypePtr IntegerType::maxOf(IntegerTypePtr a, IntegerTypePtr b)
      {
        auto r = a->range.pairwiseMax(b->range);
        return IntegerType::create(r);
      }

      IntegerType::IntegerTypePtr IntegerType::minOf(IntegerTypePtr a, IntegerTypePtr b)
      {
        auto r = a->range.pairwiseMin(b->range);
        return IntegerType::create(r);
      }

      bool IntegerType::isUnsigned() const
      {
        return range.min().sign() >= 0;
      }

    }
  }
}
