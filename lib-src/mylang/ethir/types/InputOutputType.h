#ifndef CLASS_MYLANG_ETHIR_TYPES_INPUTOUTPUTTYPE_H
#define CLASS_MYLANG_ETHIR_TYPES_INPUTOUTPUTTYPE_H

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#include <mylang/ethir/types/Type.h>
#endif
#include <memory>

namespace mylang {
  namespace ethir {
    namespace types {

      /** A type representing an error */
      class InputOutputType: public Type
      {
      protected:
        InputOutputType(::std::shared_ptr<const Type> xelement);

      public:
        ~InputOutputType();

        /** The element type */
      public:
        const ::std::shared_ptr<const Type> element;
      };
    }
  }
}
#endif
