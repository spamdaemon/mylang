#include <mylang/ethir/queries/FindTypeShadows.h>
#include <mylang/ethir/ir/DefaultNodeVisitor.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/prettyPrint.h>
#include <set>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace queries {
      namespace {
        using namespace mylang::ethir::ir;

        /**
         * If the specified is a cast expression then
         * get the variable that is casted
         */
        static EVariable getCastSource(ENode node)
        {
          if (node) {
            auto op = node->self<ir::OperatorExpression>();
            if (op) {
              switch (op->name) {
              case OperatorExpression::OP_CHECKED_CAST:
              case OperatorExpression::OP_UNCHECKED_CAST:
              case OperatorExpression::OP_BASETYPE_CAST:
              case OperatorExpression::OP_ROOTTYPE_CAST:
                return op->arguments.at(0);
              default:
                break;
              }
            }
          }
          return nullptr;
        }

        struct Visitor: public DefaultNodeVisitor
        {
          ~Visitor()
          {
          }

          void updateShadow(EVariable var, EExpression value)
          {
            // if we have a cast, then the variable being casted provides the shadow type
            EVariable srcVar = getCastSource(value);
            if (srcVar == nullptr) {
              srcVar = value->self<ir::Variable>();
            }
            auto &shadow = shadows[var->name];
            if (srcVar) {
              assert((srcVar->name != var->name) && "srcVar and var are the same");

              // means've we got a cast function
              auto s = shadows.find(srcVar->name);
              if (s == shadows.end()) {
                shadow.initial = srcVar;
                shadow.intersection = shadow.initial->type;
              } else {
                shadow.initial = s->second.initial;
                shadow.intersection = s->second.intersection->intersectWith(var->type);
                if (shadow.intersection == nullptr) {
                  // this happens, e.g. if we've got a cast to a named type
                  shadow.intersection = shadow.initial->type;
                }
              }
              if (shadow.intersection->isSameType(*var->type)) {
                ignoredShadows.insert(var->name);
              } else {
                ignoredShadows.erase(var->name);
              }
            } else {
              auto literal = value->self<ir::Literal>();
              if (literal) {
                //TODO:
                // we want to the the raw type of the literal
                // e.g. the type of 17 might be int{0;255} but should be int{17;17}
                shadow.initial = literal;
                shadow.intersection = literal->getSourceType();
              } else if (value->self<ir::Variable>()) {
                shadow.initial = value;
                shadow.intersection = shadow.initial->type;
              } else {
                shadow.initial = var;
                shadow.intersection = shadow.initial->type;
              }
              ignoredShadows.insert(var->name);
            }
          }

          void visitValueDecl(ir::ValueDecl::ValueDeclPtr node) override
          {
            if (node->value) {
              visitNode(node->value);
              updateShadow(node->variable, node->value);
            }
            visitNode(node->next);
          }

          void visitGlobalValue(ir::GlobalValue::GlobalValuePtr node) override
          {
            visitNode(node->value);
            updateShadow(node->variable, node->value);
          }

          void visitProcessValue(ir::ProcessValue::ProcessValuePtr node) override
          {
            if (node->value) {
              visitNode(node->value);
              updateShadow(node->variable, node->value);
            }
          }

          /** The shadows */
          ::std::map<Name, FindTypeShadows::Shadow> shadows;

          /** Shadows of themselves */
          ::std::set<Name> ignoredShadows;
        };

      }

      ::std::map<Name, FindTypeShadows::Shadow> FindTypeShadows::find(ENode src)
      {
        Visitor v;
        // need to make 2 passes because the global and process values are not ordered in any way
        src->accept(v);
        src->accept(v);

        for (auto i = v.ignoredShadows.begin(); i != v.ignoredShadows.end(); ++i) {
          v.shadows.erase(v.shadows.find(*i));
        }

        return v.shadows;
      }

    }

  }
}

