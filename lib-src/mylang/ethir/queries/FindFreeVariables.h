#ifndef CLASS_MYLANG_ETHIR_QUERIES_FINDFREEVARIABLES_H
#define CLASS_MYLANG_ETHIR_QUERIES_FINDFREEVARIABLES_H

#ifndef CLASS_MYLANG_ETHIR_NAME_H
#include <mylang/ethir/Name.h>
#endif

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#include <map>

namespace mylang {
  namespace ethir {
    namespace queries {

      /**
       * Find all variables that are used within the scope of a node, but not
       * defined within the same scope.
       */
      class FindFreeVariables
      {
        /** A mapping for each variable name to the statement block in which it must be declared.  */
      public:
        typedef ::std::map<Name, EVariable> Names;

      private:
        ~FindFreeVariables() = delete;

      public:
        static Names find(const ENode &node);
      };
    }
  }
}
#endif
