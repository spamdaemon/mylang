#include <mylang/ethir/Visitor.h>
#include <mylang/ethir/ir/OperatorVisitor.h>
#include <mylang/ethir/queries/GetLimits.h>
#include <mylang/ethir/limits/limits.h>
#include <mylang/ethir/types/types.h>

namespace mylang {
  namespace ethir {
    using namespace ir;
    using namespace types;
    using namespace limits;

    namespace queries {

      namespace {

        struct OpVisitor: public OperatorVisitor
        {
          typedef Limits::LimitsPtr LimitsPtr;

          OpVisitor(const GetLimits::VarBounds &varbounds)
              : bounds(varbounds)
          {
          }

          ~OpVisitor()
          {
          }

          Limits::LimitsPtr getLimits(const OperatorExpression::OperatorExpressionPtr &e)
          {
            result = nullptr;
            visit(e);
            Limits::LimitsPtr res = result;
            result = nullptr;
            return res;
          }

          Limits::LimitsPtr getBound(const Variable::VariablePtr &v) const
          {
            auto i = bounds.find(v->name);
            if (i == bounds.end()) {
              return nullptr;
            }
            return i->second;
          }

          Limits::LimitsPtr getBound(const Variable::VariablePtr &v, const EType &ty) const
          {
            auto b = getBound(v);
            if (b) {
              return b;
            }
            return Limits::of(ty);
          }

          ::std::vector<Limits::LimitsPtr> getBounds(
              const ::std::vector<Variable::VariablePtr> &vars, const EType &ty) const
          {
            ::std::vector<Limits::LimitsPtr> res;
            res.reserve(vars.size());
            for (auto v : vars) {
              res.push_back(getBound(v, ty));
            }
            return res;
          }

          ::std::vector<Limits::LimitsPtr> getBounds(
              const ::std::vector<Variable::VariablePtr> &vars) const
          {
            ::std::vector<Limits::LimitsPtr> res;
            res.reserve(vars.size());
            for (auto v : vars) {
              res.push_back(getBound(v));
            }
            return res;
          }

          ::std::vector<Limits::LimitsPtr> getBounds(
              const ::std::vector<Variable::VariablePtr> &vars,
              const ::std::vector<EType> &types) const
          {
            ::std::vector<Limits::LimitsPtr> res;
            res.reserve(vars.size());
            for (size_t i = 0; i < vars.size(); ++i) {
              res.push_back(getBound(vars.at(i), types.at(i)));
            }
            return res;
          }

          LimitsPtr unaryResult(const OperatorExpression::OperatorExpressionPtr &e,
              ::std::function<LimitsPtr(LimitsPtr)> f) const
          {
            auto arg0 = getBound(e->arguments.at(0));
            if (arg0) {
              try {
                return f(arg0);
              } catch (...) {
                // ignore exceptions
              }
            }
            return nullptr;
          }

          LimitsPtr binaryResult(const OperatorExpression::OperatorExpressionPtr &e,
              ::std::function<LimitsPtr(LimitsPtr, LimitsPtr)> f) const
          {
            auto arg0 = getBound(e->arguments.at(0));
            if (arg0) {
              auto arg1 = getBound(e->arguments.at(1));
              if (arg1) {
                try {
                  return f(arg0, arg1);
                } catch (...) {
                  // ignore exceptions
                }
              }
            }
            return nullptr;
          }

          void visit_NEW(OperatorExpression::OperatorExpressionPtr e) override final
          {
            if (e->type->self<OptType>()) {
              auto ty = e->type->self<OptType>();
              if (e->arguments.empty()) {
                result = OptLimits::getNil();
              } else {
                result = OptLimits::get(BooleanLimits::getTrue(nullptr),
                    getBound(e->arguments.at(0), ty->element));
              }
              return;
            }
            if (e->type->self<ArrayType>()) {
              auto ty = e->type->self<ArrayType>();
              auto E = getBounds(e->arguments, ty->element);
              result = ArrayLimits::get(E);
              return;
            }
            if (e->type->self<StructType>()) {
              auto ty = e->type->self<StructType>();
              auto E = getBounds(e->arguments, ty->getMemberTypes());
              result = StructLimits::get(E);
              return;
            }
            if (e->type->self<TupleType>()) {
              auto ty = e->type->self<TupleType>();
              auto E = getBounds(e->arguments, ty->types);
              result = TupleLimits::get(E);
              return;
            }
            if (e->type->self<GenericType>()) {
              result = GenericLimits::get(getBound(e->arguments.at(0), e->arguments.at(0)->type));
              return;
            }

          }
          void visit_CALL(OperatorExpression::OperatorExpressionPtr) override final
          {
          }

          void visit_CHECKED_CAST(OperatorExpression::OperatorExpressionPtr e) override final
          {
            auto argTy = e->arguments.at(0)->type;
            auto toTy = e->type;

            if (toTy->self<IntegerType>() && argTy->self<IntegerType>()) {
              result = getBound(e->arguments.at(0), toTy);
              return;
            }

          }
          void visit_UNCHECKED_CAST(OperatorExpression::OperatorExpressionPtr e) override final
          {
            auto argTy = e->arguments.at(0)->type;
            auto toTy = e->type;

            if (toTy->self<IntegerType>() && argTy->self<IntegerType>()) {
              result = getBound(e->arguments.at(0), toTy);
              return;
            }
          }

          void visit_NEG(OperatorExpression::OperatorExpressionPtr e) override final
          {
            if (e->type->self<BooleanType>()) {
              result = unaryResult(e, [&](LimitsPtr a) {
                auto l = a->self<IntegerLimits>();
                if (l->isUnbounded()) {
                  return l;
                }
                return IntegerLimits::get(l->bounds.negate());
              });
            }
          }
          void visit_ADD(OperatorExpression::OperatorExpressionPtr e) override final
          {
            if (e->type->self<IntegerType>()) {
              result = binaryResult(e, [&](LimitsPtr a, LimitsPtr b) {
                auto l = a->self<IntegerLimits>();
                if (l->isUnbounded()) {return l;}
                auto r = b->self<IntegerLimits>();
                if (r->isUnbounded()) {return r;}
                return IntegerLimits::get(l->bounds+r->bounds);
              });
            }
          }
          void visit_SUB(OperatorExpression::OperatorExpressionPtr e) override final
          {
            if (e->type->self<IntegerType>()) {
              result = binaryResult(e, [&](LimitsPtr a, LimitsPtr b) {
                auto l = a->self<IntegerLimits>();
                if (l->isUnbounded()) {return l;}
                auto r = b->self<IntegerLimits>();
                if (r->isUnbounded()) {return r;}
                return IntegerLimits::get(l->bounds - r->bounds);
              });
            }
          }
          void visit_MUL(OperatorExpression::OperatorExpressionPtr e) override final
          {
            if (e->type->self<IntegerType>()) {
              result = binaryResult(e, [&](LimitsPtr a, LimitsPtr b) {
                auto l = a->self<IntegerLimits>();
                if (l->isUnbounded()) {return l;}
                auto r = b->self<IntegerLimits>();
                if (r->isUnbounded()) {return r;}
                return IntegerLimits::get(l->bounds * r->bounds);
              });
            }
          }
          void visit_DIV(OperatorExpression::OperatorExpressionPtr e) override final
          {
            if (e->type->self<IntegerType>()) {
              result = binaryResult(e, [&](LimitsPtr a, LimitsPtr b) {
                auto l = a->self<IntegerLimits>();
                if (l->isUnbounded()) {return l;}
                auto r = b->self<IntegerLimits>();
                auto op =l->bounds/r->bounds;
                // exceptions are ignored
                  return IntegerLimits::get(*op);
                });
            }
          }
          void visit_MOD(OperatorExpression::OperatorExpressionPtr e) override final
          {
            if (e->type->self<IntegerType>()) {
              result = binaryResult(e, [&](LimitsPtr a, LimitsPtr b) {
                auto l = a->self<IntegerLimits>();
                auto r = b->self<IntegerLimits>();
                auto op =l->bounds.mod(r->bounds);
                // exceptions are ignored
                  return IntegerLimits::get(*op);
                });
            }
          }
          void visit_REM(OperatorExpression::OperatorExpressionPtr e) override final
          {
            if (e->type->self<IntegerType>()) {
              result = binaryResult(e, [&](LimitsPtr a, LimitsPtr b) {
                auto l = a->self<IntegerLimits>();
                auto r = b->self<IntegerLimits>();
                auto op =l->bounds.remainder(r->bounds);
                // exceptions are ignored
                  return IntegerLimits::get(*op);
                });
            }
          }

          void visit_INDEX(OperatorExpression::OperatorExpressionPtr) override final
          {

          }
          void visit_SUBRANGE(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_EQ(OperatorExpression::OperatorExpressionPtr e) override final
          {
            result = binaryResult(e, [&](LimitsPtr a, LimitsPtr b) {
              return a->compareEQ(b,e);
            });
          }

          void visit_NEQ(OperatorExpression::OperatorExpressionPtr e) override final
          {
            result = binaryResult(e, [&](LimitsPtr a, LimitsPtr b) {
              return a->compareEQ(b,e)->NOT();
            });
          }
          void visit_LT(OperatorExpression::OperatorExpressionPtr e) override final
          {
            result = binaryResult(e, [&](LimitsPtr a, LimitsPtr b) {
              return a->compareLT(b,e);
            });
          }
          void visit_LTE(OperatorExpression::OperatorExpressionPtr e) override final
          {
            result = binaryResult(e, [&](LimitsPtr a, LimitsPtr b) {
              return b->compareLT(a,e)->NOT();
            });
          }

          void visit_AND(OperatorExpression::OperatorExpressionPtr e) override final
          {
            if (e->type->self<BooleanType>()) {
              result = binaryResult(e, [&](LimitsPtr a, LimitsPtr b) {
                auto l = a->self<BooleanLimits>();
                auto r = b->self<BooleanLimits>();
                return BooleanLimits::AND(l,r);
              });
            }
          }
          void visit_OR(OperatorExpression::OperatorExpressionPtr e) override final
          {
            if (e->type->self<BooleanType>()) {
              result = binaryResult(e, [&](LimitsPtr a, LimitsPtr b) {
                auto l = a->self<BooleanLimits>();
                auto r = b->self<BooleanLimits>();
                if (l->containsOnly(true) || r->containsOnly(true)) {
                  return BooleanLimits::getTrue(e);
                }
                if (l->containsOnly(false) && r->containsOnly(false)) {
                  return BooleanLimits::getFalse(e);
                }
                return BooleanLimits::getTrueOrFalse(e);
              });
            }
          }
          void visit_XOR(OperatorExpression::OperatorExpressionPtr e) override final
          {
            if (e->type->self<BooleanType>()) {
              result = binaryResult(e, [&](LimitsPtr a, LimitsPtr b) {
                auto l = a->self<BooleanLimits>();
                auto r = b->self<BooleanLimits>();
                if (l->containsOnly(true) && r->containsOnly(false)) {
                  return BooleanLimits::getTrue(e);
                }
                if (l->containsOnly(false) && r->containsOnly(true)) {
                  return BooleanLimits::getTrue(e);
                }
                if (l->containsOnly(false) && r->containsOnly(false)) {
                  return BooleanLimits::getFalse(e);
                }
                if (l->containsOnly(true) && r->containsOnly(true)) {
                  return BooleanLimits::getFalse(e);
                }
                return BooleanLimits::getTrueOrFalse(e);
              });
            }
          }
          void visit_NOT(OperatorExpression::OperatorExpressionPtr e) override final
          {
            if (e->type->self<BooleanType>()) {
              result = unaryResult(e, [&](LimitsPtr a) {
                return a->self<BooleanLimits>()->NOT();
              });
            }
          }
          void visit_APPEND(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_BUILD(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_GET(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_IS_LOGGABLE(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_IS_PRESENT(OperatorExpression::OperatorExpressionPtr e) override final
          {
            result = unaryResult(e, [&](LimitsPtr a) {
              return a->self<OptLimits>()->present;
            });
          }
          void visit_IS_INPUT_NEW(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_IS_INPUT_CLOSED(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_IS_OUTPUT_CLOSED(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_IS_PORT_READABLE(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_IS_PORT_WRITABLE(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_LENGTH(OperatorExpression::OperatorExpressionPtr e) override final
          {
            result = unaryResult(e, [&](LimitsPtr a) {
              LimitsPtr res;
              auto arr = a->self<ArrayLimits>();
              if (arr) {
                res= arr->length;
              }
              return res;
            });
          }

          void visit_CONCATENATE(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_MERGE_TUPLES(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_ZIP(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_BITS_TO_BYTES(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_BYTES_TO_BITS(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_FROM_BITS(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_TO_BITS(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_FROM_BYTES(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_TO_BYTES(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_CLAMP(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_WRAP(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_BYTE_TO_UINT8(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_BYTE_TO_SINT8(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_STRING_TO_CHARS(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_INTERPOLATE_TEXT(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_TO_STRING(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_FOLD(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_PAD(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_TRIM(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_DROP(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_TAKE(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_MAP_ARRAY(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_MAP_OPTIONAL(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_FILTER_ARRAY(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_FILTER_OPTIONAL(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_FIND(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_REVERSE(OperatorExpression::OperatorExpressionPtr e) override final
          {
            result = unaryResult(e, [&](LimitsPtr x) {
              LimitsPtr res;
              auto arr = x->self<ArrayLimits>();
              if (arr) {
                res= ArrayLimits::get(arr->length,arr->elements);
              }
              return res;
            });
          }

          void visit_FLATTEN(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_PARTITION(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_HEAD(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_TAIL(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_STRUCT_TO_TUPLE(OperatorExpression::OperatorExpressionPtr e) override final
          {
            result = unaryResult(e,
                [&](LimitsPtr x) {return TupleLimits::get(x->self<StructLimits>()->members);});
          }
          void visit_TUPLE_TO_STRUCT(OperatorExpression::OperatorExpressionPtr e) override final
          {
            result = unaryResult(e,
                [&](LimitsPtr x) {return StructLimits::get(x->self<TupleLimits>()->elements);});
          }
          void visit_BASETYPE_CAST(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_ROOTTYPE_CAST(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_READ_PORT(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_BLOCK_READ(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_BLOCK_WRITE(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_CLEAR_PORT(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_CLOSE_PORT(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_WAIT_PORT(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_WRITE_PORT(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_LOG(OperatorExpression::OperatorExpressionPtr) override final
          {
          }
          void visit_SET(OperatorExpression::OperatorExpressionPtr) override final
          {
          }

          Limits::LimitsPtr result;

          const GetLimits::VarBounds &bounds;
        };

        struct IRVisitor: public Visitor<Limits::LimitsPtr>
        {
          IRVisitor(const GetLimits::VarBounds &varbounds)
              : opVisitor(varbounds)
          {
          }

          ~IRVisitor()
          {
          }

          Limits::LimitsPtr visit(const ir::Parameter::ParameterPtr &param) override final
          {
            return visit(param->variable);
          }

          Limits::LimitsPtr visit(const ir::ToArray::ToArrayPtr&) override final
          {
            return nullptr;
          }

          Limits::LimitsPtr visit(const ir::Phi::PhiPtr &node) override final
          {
            Limits::LimitsPtr lim;
            for (size_t i = 0; i < node->arguments.size(); ++i) {
              Limits::LimitsPtr t = visit(node->arguments.at(i));
              if (!t) {
                return nullptr;
              }
              if (lim) {
                lim = Limits::merge(lim, t);
              } else {
                lim = t;
              }
              if (!lim) {
                // could not determine a limit
                return nullptr;
              }
            }

            return lim;
          }

          Limits::LimitsPtr visit(const ir::LetExpression::LetExpressionPtr&) override final
          {
            // not supported, because we would have to modify the bounds
            return nullptr;
          }

          Limits::LimitsPtr visit(const ir::OpaqueExpression::OpaqueExpressionPtr&) override final
          {
            // not supported, because we would have to modify the bounds
            return nullptr;
          }

          Limits::LimitsPtr visit(const ir::NoValue::NoValuePtr&) override final
          {
            return nullptr;
          }

          Limits::LimitsPtr visit(const ir::NewUnion::NewUnionPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::NewProcess::NewProcessPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::LiteralChar::LiteralCharPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::LiteralString::LiteralStringPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::LiteralReal::LiteralRealPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::OperatorExpression::OperatorExpressionPtr &node)
          override final
          {
            return opVisitor.getLimits(node);
          }
          Limits::LimitsPtr visit(const ir::LiteralInteger::LiteralIntegerPtr &node)
          override final
          {
            return IntegerLimits::get(node->value);
          }
          Limits::LimitsPtr visit(const ir::LiteralBoolean::LiteralBooleanPtr &node)
          override final
          {
            return BooleanLimits::get(node->value);
          }
          Limits::LimitsPtr visit(const ir::LambdaExpression::LambdaExpressionPtr&)
          override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::GetDiscriminant::GetDiscriminantPtr&)
          override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::GetMember::GetMemberPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::LiteralByte::LiteralBytePtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::LiteralArray::LiteralArrayPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::LiteralNamedType::LiteralNamedTypePtr&)
          override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::LiteralOptional::LiteralOptionalPtr&)
          override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::LiteralStruct::LiteralStructPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::LiteralTuple::LiteralTuplePtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::LiteralUnion::LiteralUnionPtr&) override final
          {
            return nullptr;
          }

          Limits::LimitsPtr visit(const ir::Variable::VariablePtr &node) override final
          {
            auto i = opVisitor.bounds.find(node->name);
            if (i == opVisitor.bounds.end()) {
              return nullptr;
            }
            return i->second;
          }
          Limits::LimitsPtr visit(const ir::LiteralBit::LiteralBitPtr&) override final
          {
            return nullptr;
          }

          // we ignore all statements!

          Limits::LimitsPtr visit(const ir::InputPortDecl::InputPortDeclPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::ProcessVariableUpdate::ProcessVariableUpdatePtr&)
          override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::VariableUpdate::VariableUpdatePtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::IfStatement::IfStatementPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::TryCatch::TryCatchPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::VariableDecl::VariableDeclPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::ProcessVariable::ProcessVariablePtr&)
          override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::ProcessValue::ProcessValuePtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::ProcessDecl::ProcessDeclPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::NoStatement::NoStatementPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::CommentStatement::CommentStatementPtr&)
          override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::AbortStatement::AbortStatementPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::BreakStatement::BreakStatementPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::ContinueStatement::ContinueStatementPtr&)
          override final
          {
            return nullptr;
          }

          Limits::LimitsPtr visit(const ir::OwnConstructorCall::OwnConstructorCallPtr&)
          override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::Program::ProgramPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::LoopStatement::LoopStatementPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::GlobalValue::GlobalValuePtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::ProcessBlock::ProcessBlockPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::ProcessConstructor::ProcessConstructorPtr&)
          override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::OutputPortDecl::OutputPortDeclPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::ValueDecl::ValueDeclPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::ForeachStatement::ForeachStatementPtr&)
          override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::StatementBlock::StatementBlockPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::ReturnStatement::ReturnStatementPtr&)
          override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::ThrowStatement::ThrowStatementPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::YieldStatement::YieldStatementPtr &yield)
          override final
          {
            return visit(yield->value);
          }
          Limits::LimitsPtr visit(const ir::SkipStatement::SkipStatementPtr&) override final
          {
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::IterateArrayStep::IterateArrayStepPtr&) override final
          {
            // FIXME: we should try to do a get limits here!
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::SequenceStep::SequenceStepPtr&) override final
          {
            // FIXME: we should try to do a get limits here!
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::SingleValueStep::SingleValueStepPtr&) override final
          {
            // FIXME: we should try to do a get limits here!
            return nullptr;
          }
          Limits::LimitsPtr visit(const ir::Loop::LoopPtr&) override final
          {
            // TODO: do we need to do anything for loops?
            return nullptr;
          }

          OpVisitor opVisitor;
        };

      }

      Limits::LimitsPtr GetLimits::get(const EExpression &node, const VarBounds &varbounds)
      {
        IRVisitor v(varbounds);
        return v.visitNode(node);
      }
    }
  }
}
