#include <mylang/ethir/queries/CountStatements.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/ir/DefaultNodeVisitor.h>

namespace mylang {
  namespace ethir {
    using namespace ir;
    namespace queries {

      size_t CountStatements::countStatements(const ENode &node)
      {
        struct V: public DefaultNodeVisitor
        {
          V()
              : count(0)
          {
          }
          ~V()
          {
          }

          void visitIfStatement(IfStatement::IfStatementPtr s) override final
          {
            V ift, iff;
            ift.visitNode(s->iftrue);
            iff.visitNode(s->iftrue);
            count += std::max(ift.count, iff.count);
          }

          void visitStatementBlock(StatementBlock::StatementBlockPtr s) override final
          {
            --count; // we already account for the block, so decrement the count
            DefaultNodeVisitor::visitStatementBlock(s);
          }

          virtual void visitNode(const ENode &node) override final
          {
            if (node && node->self<Statement>()) {
              ++count;
            }
            DefaultNodeVisitor::visitNode(node);
          }

          size_t count;
        };

        V v;
        node->accept(v);
        return v.count;
      }

    }
  }
}
