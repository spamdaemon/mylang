#include <mylang/ethir/queries/FindDefinedVariables.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/ir/DefaultNodeVisitor.h>

namespace mylang {
  namespace ethir {
    using namespace ir;
    namespace queries {

      FindDefinedVariables::Names FindDefinedVariables::find(const ENode &node)
      {
        struct V: public DefaultNodeVisitor
        {
          V()
          {
          }
          ~V()
          {
          }

          void define(EVariable e)
          {
            if (e) {
              names[e->name] = e;
            }
          }

          void visitGlobalValue(GlobalValue::GlobalValuePtr node) override
          {
            define(node->variable);
            DefaultNodeVisitor::visitGlobalValue(node);
          }
          void visitLetExpression(LetExpression::LetExpressionPtr expr) override
          {
            define(expr->variable);
            DefaultNodeVisitor::visitLetExpression(expr);
          }
          void visitInputPortDecl(InputPortDecl::InputPortDeclPtr stmt) override
          {
            define(stmt->variable);
            DefaultNodeVisitor::visitInputPortDecl(stmt);
          }
          void visitOutputPortDecl(OutputPortDecl::OutputPortDeclPtr stmt) override
          {
            define(stmt->variable);
            DefaultNodeVisitor::visitOutputPortDecl(stmt);
          }

          void visitProcessVariable(ProcessVariable::ProcessVariablePtr stmt) override
          {
            define(stmt->variable);
            DefaultNodeVisitor::visitProcessVariable(stmt);
          }

          void visitProcessValue(ProcessValue::ProcessValuePtr stmt) override
          {
            define(stmt->variable);
            DefaultNodeVisitor::visitProcessValue(stmt);
          }
          void visitValueDecl(ValueDecl::ValueDeclPtr stmt) override
          {
            define(stmt->variable);
            DefaultNodeVisitor::visitValueDecl(stmt);
          }

          void visitVariableDecl(VariableDecl::VariableDeclPtr stmt) override
          {
            define(stmt->variable);
            DefaultNodeVisitor::visitVariableDecl(stmt);
          }
          void visitParameter(Parameter::ParameterPtr param) override
          {
            define(param->variable);
            DefaultNodeVisitor::visitParameter(param);
          }

          void visitIterateArrayStep(IterateArrayStep::IterateArrayStepPtr node) override
          {
            for (auto i : node->initializers) {
              define(i.first);
            }
            define(node->counter);
            define(node->value);
            DefaultNodeVisitor::visitIterateArrayStep(node);
          }
          void visitSequenceStep(SequenceStep::SequenceStepPtr node) override
          {
            for (auto i : node->initializers) {
              define(i.first);
            }
            define(node->counter);
            DefaultNodeVisitor::visitSequenceStep(node);
          }

          FindDefinedVariables::Names names;
        };

        V v;
        node->accept(v);
        return ::std::move(v.names);
      }

    }
  }
}
