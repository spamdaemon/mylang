#include <mylang/ethir/queries/SideEffects.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/types/types.h>
#include <mylang/ethir/ir/AbstractNodeVisitor.h>

namespace mylang {
  namespace ethir {
    using namespace ir;
    namespace queries {

      SideEffects::Effect SideEffects::test(const Environment &env, const EExpression &expr)
      {
        struct V: public AbstractNodeVisitor
        {
          V(const Environment &xenv)
              : env(xenv), sideEffect(EFFECT_NONE)
          {
          }
          ~V()
          {
          }

          void updateSideEffect(Effect e)
          {
            if (e > sideEffect) {
              sideEffect = e;
            }
          }

          void visitLetExpression(LetExpression::LetExpressionPtr expr) override
          {
            expr->value->accept(*this);
            expr->result->accept(*this);
          }

          void visitOpaqueExpression(OpaqueExpression::OpaqueExpressionPtr expr) override
          {
            // opaque expression have side-effects if the underlying expression has side-effects
            expr->expression->accept(*this);
          }

          void visitOperatorExpression(OperatorExpression::OperatorExpressionPtr expr) override
          {
            switch (expr->name) {
            // these have side-effects for sure
            case OperatorExpression::OP_BLOCK_READ:
            case OperatorExpression::OP_BLOCK_WRITE:
            case OperatorExpression::OP_CLEAR_PORT:
            case OperatorExpression::OP_CLOSE_PORT:
            case OperatorExpression::OP_READ_PORT:
            case OperatorExpression::OP_SET:
            case OperatorExpression::OP_WAIT_PORT:
            case OperatorExpression::OP_WRITE_PORT:
            case OperatorExpression::OP_APPEND:
            case OperatorExpression::OP_BUILD:
              updateSideEffect(EFFECT_STATE);
              return;
            case OperatorExpression::OP_IS_INPUT_CLOSED:
            case OperatorExpression::OP_IS_OUTPUT_CLOSED:
            case OperatorExpression::OP_IS_PORT_READABLE:
            case OperatorExpression::OP_IS_PORT_WRITABLE:
              updateSideEffect(EFFECT_READ_VOLATILE);
              return;
            case OperatorExpression::OP_GET:
              if (expr->arguments.at(0)->type->self<types::MutableType>()) {
                updateSideEffect(EFFECT_READ_VOLATILE);
              } else {
                updateSideEffect(EFFECT_NONE);
              }
              return;

            case OperatorExpression::OP_CALL:
              // these may have side-effects, but we treat them as having side-effects for the time
              // being
              // TODO: actually use the environment to determine if these expression have side-effects
              updateSideEffect(EFFECT_STATE);
              return;
            case OperatorExpression::OP_LOG:
              // logging may not always be a side-effect
              // if a function has no side-effects other than log statements
              // we should be free to eliminate it
              updateSideEffect(EFFECT_LOGGING);
              return;

              // these really must never have side-effects; we should have checked that before
            case OperatorExpression::OP_FILTER_ARRAY:
            case OperatorExpression::OP_FILTER_OPTIONAL:
            case OperatorExpression::OP_FIND:
            case OperatorExpression::OP_MAP_ARRAY:
            case OperatorExpression::OP_MAP_OPTIONAL:
            case OperatorExpression::OP_FOLD:
              updateSideEffect(EFFECT_STATE);
              return;
              // operator new can have side-effect, in the sense
              // that create different objects cannot be cached
            case OperatorExpression::OP_NEW:
              if (expr->type->self<types::BuilderType>()) {
                updateSideEffect(EFFECT_READ_VOLATILE);
              } else if (expr->type->self<types::MutableType>()) {
                updateSideEffect(EFFECT_READ_VOLATILE);
              } else if (expr->type->self<types::ProcessType>()) {
                updateSideEffect(EFFECT_READ_VOLATILE);
              } else if (expr->type->self<types::InputOutputType>()) {
                updateSideEffect(EFFECT_READ_VOLATILE);
              } else {
                updateSideEffect(EFFECT_NONE);
              }
              return;
            default:
              return;
            }
          }

          void visitNewProcess(NewProcess::NewProcessPtr) override
          {
            updateSideEffect(EFFECT_STATE);
          }

          const Environment &env;
          Effect sideEffect;
        };

        V v(env);
        expr->accept(v);
        return v.sideEffect;

      }

    }
  }
}
