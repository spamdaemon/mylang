#ifndef CLASS_MYLANG_ETHIR_QUERIES_FINDDEFINEDVARIABLES_H
#define CLASS_MYLANG_ETHIR_QUERIES_FINDDEFINEDVARIABLES_H

#ifndef CLASS_MYLANG_ETHIR_NAME_H
#include <mylang/ethir/Name.h>
#endif

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#include <map>

namespace mylang {
  namespace ethir {
    namespace queries {

      /**
       * Find the names of all variables that are defined somewhere.
       */
      class FindDefinedVariables
      {
        /** A mapping for each variable name to the statement block in which it must be declared.  */
      public:
        typedef ::std::map<Name, EVariable> Names;

      private:
        ~FindDefinedVariables() = delete;

        /**
         * Find all variables declared or defined somewhere in the specified node.
         * @param node the node to use as a starting point
         * @return the defined variables indexed by their names
         */
      public:
        static Names find(const ENode &node);
      };
    }
  }
}
#endif
