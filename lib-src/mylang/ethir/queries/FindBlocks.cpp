#include <mylang/ethir/queries/FindBlocks.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/ir/DefaultNodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    using namespace ir;
    namespace queries {

      namespace {

        struct XBlock
        {
          XBlock() = delete;
          XBlock(XBlock *enc)
              : enclosing(enc), info(::std::make_shared<FindBlocks::Block>())
          {
          }

          XBlock *enclosing;
          FindBlocks::Block::Ptr info;
        };

      }

      FindBlocks::Blocks FindBlocks::find(const ENode &node)
      {
        struct V: public DefaultNodeVisitor
        {
          V()
              : current(nullptr)
          {
          }

          ~V()
          {
          }

          void visitLoop(Loop::LoopPtr) override
          {
            // do not enter the loop
          }

          void visitLambdaExpression(LambdaExpression::LambdaExpressionPtr expr) override
          {
            XBlock *restore = current;
            current = nullptr;
            enterBlock(expr->body);
            current = restore;
          }

          void visitProcessBlock(ProcessBlock::ProcessBlockPtr stmt) override
          {
            XBlock *restore = current;
            current = nullptr;
            enterBlock(stmt->body);
            current = restore;
          }

          void visitOwnConstructorCall(OwnConstructorCall::OwnConstructorCallPtr stmt) override
          {
            XBlock *restore = current;
            current = nullptr;
            enterBlock(stmt->pre);
            current = restore;
          }

          void visitProcessConstructor(ProcessConstructor::ProcessConstructorPtr stmt) override
          {
            XBlock *restore = current;
            current = nullptr;
            enterBlock(stmt->callConstructor);
            enterBlock(stmt->body);
            current = restore;
          }

          void visitStatementBlock(StatementBlock::StatementBlockPtr stmt) override
          {
            enterBlock(stmt->statements);
            visitNode(stmt->next);
          }

          void visitForeachStatement(ForeachStatement::ForeachStatementPtr stmt) override
          {
            auto body = enterBlock(stmt->body);
            body->label = stmt->name;
            visitNode(stmt->next);
          }

          void visitLoopStatement(LoopStatement::LoopStatementPtr stmt) override
          {
            auto body = enterBlock(stmt->body);
            body->label = stmt->name;
            visitNode(stmt->next);
          }

          void visitIfStatement(IfStatement::IfStatementPtr stmt) override
          {
            enterBlock(stmt->iftrue);
            enterBlock(stmt->iffalse);
            visitNode(stmt->next);
          }

          void visitTryCatch(TryCatch::TryCatchPtr stmt) override
          {
            enterBlock(stmt->tryBody);
            enterBlock(stmt->catchBody);
            visitNode(stmt->next);
          }
          void visitValueDecl(ValueDecl::ValueDeclPtr stmt) override
          {
            assert(current && "No current block in scope");
            if (!stmt->value) {
              current->info->blockVariables.insert(stmt->name);
            }
            visitNode(stmt->value);
            visitNode(stmt->next);
          }

          void visitVariableDecl(VariableDecl::VariableDeclPtr stmt) override
          {
            assert(current && "No current block in scope");
            current->info->blockVariables.insert(stmt->name);
            if (stmt->value) {
              current->info->localUpdates.insert(stmt->name);
            }
            visitNode(stmt->value);
            visitNode(stmt->next);
          }

          void visitVariableUpdate(VariableUpdate::VariableUpdatePtr stmt) override
          {
            const Name &name = stmt->target->name;
            assert(current && "No current block in scope");

            XBlock *block = current;
            if (block->info->blockVariables.count(name) != 0) {
              block->info->localUpdates.insert(name);
              visitNode(stmt->next);
              return;
            }

            do {
              block->info->updates.insert(name);
              if (block->info->blockVariables.count(name) != 0) {
                // once we hit a defining block, we terminate the loop
                break;
              }
              block = block->enclosing;
              if (block == nullptr) {
                ::std::cerr << "Could not find block for variable " << name << ::std::endl;
                assert(false);
              }
            } while (block);

            visitNode(stmt->next);
          }

          Block::Ptr enterBlock(const EStatement &s)
          {
            if (s == nullptr) {
              return nullptr;
            }
            Block::Ptr block;

            XBlock b(current);
            current = &b;
            current->info->statement = s;
            block = current->info;
            blocks[s] = block;
            visitNode(s);
            current = b.enclosing;
            return block;
          }

          // the blocks that were found
          ::std::map<EStatement, Block::Ptr> blocks;

          // the current block
          XBlock *current;
        };

        V v;
        node->accept(v);
        return v.blocks;
      }

    }
  }
}
