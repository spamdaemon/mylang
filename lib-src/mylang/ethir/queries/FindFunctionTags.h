#ifndef CLASS_MYLANG_ETHIR_QUERIES_FINDFUNCTIONTAGS_H
#define CLASS_MYLANG_ETHIR_QUERIES_FINDFUNCTIONTAGS_H

#ifndef CLASS_MYLANG_ETHIR_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TAG_H
#include <mylang/ethir/Tag.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_ABSTRACTVALUEDECLARATION_H
#include <mylang/ethir/ir/AbstractValueDeclaration.h>
#endif

#include <memory>
#include <vector>
#include <map>

namespace mylang {
  namespace ethir {
    namespace queries {

      /**
       * For each function in a program, locate all callsites
       */
      class FindFunctionTags
      {
      public:
        struct TaggedFunctions
        {
          /** Find a function with the specified signature and return the functions' name */
          EVariable findBySignature(const EType &type) const;

          ::std::vector<mylang::ethir::ir::AbstractValueDeclaration::AbstractValueDeclarationPtr> functions;

        };

        /** The callsites for each function indexed by the function name */
      public:
        typedef ::std::map<Tag, TaggedFunctions> FunctionsByTag;

      private:
        ~FindFunctionTags() = delete;

        /**
         * Find all function calls in the program.
         * @param node the node to use as a starting point
         * @return the blocks where a variable should be defined
         */
      public:
        static FunctionsByTag find(const ENode &node);
      };
    }
  }
}
#endif
