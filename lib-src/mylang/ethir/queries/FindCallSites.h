#ifndef CLASS_MYLANG_ETHIR_QUERIES_FINDCALLSITES_H
#define CLASS_MYLANG_ETHIR_QUERIES_FINDCALLSITES_H

#ifndef CLASS_MYLANG_ETHIR_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_ABSTRACTVALUEDECLARATION_H
#include <mylang/ethir/ir/AbstractValueDeclaration.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_OPERATOREXPRESSION_H
#include <mylang/ethir/ir/OperatorExpression.h>
#endif

#ifndef CLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif

#include <memory>
#include <vector>
#include <map>

namespace mylang {
  namespace ethir {
    namespace queries {

      /**
       * For each function in a program, locate all callsites
       */
      class FindCallSites
      {
      public:
        /** The callsites for each function */
        struct CallSites
        {
          /** The declaration that defines the called function */
          mylang::ethir::ir::AbstractValueDeclaration::AbstractValueDeclarationPtr decl;

          /** The callsites for the function.  */
          ::std::vector<ir::OperatorExpression::OperatorExpressionPtr> sites;
        };

        /** The callsites for each function indexed by the function name */
      public:
        typedef ::std::map<Name, CallSites> FunctionCallSites;

      private:
        ~FindCallSites() = delete;

        /**
         * Find all function calls in the program. If a function is called from anything
         * other than an OperatorExpression, it is removed from the result.
         * @param node the node to use as a starting point
         * @return the blocks where a variable should be defined
         */
      public:
        static FunctionCallSites find(const ENode &node);
      };
    }
  }
}
#endif
