#ifndef CLASS_MYLANG_ETHIR_QUERIES_FINDVARIABLESCOPE_H
#define CLASS_MYLANG_ETHIR_QUERIES_FINDVARIABLESCOPE_H

#ifndef CLASS_MYLANG_ETHIR_NAME_H
#include <mylang/ethir/Name.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENTBLOCK_H
#include <mylang/ethir/ir/StatementBlock.h>
#endif

#include <memory>
#include <map>

namespace mylang {
  namespace ethir {
    namespace queries {

      /**
       * For each variable in statement block find the scope in which it must be defined. The reason this
       * is not so simple is because SSA form does not respect block boundaries and so all local variables
       * are in the same scope. The most common case is an if-statement that used to intializer a variable,
       * e,g,:
       * <code>
       *  val x:integer;
       *  if (cond) { x = 1; } else { x = 2; }
       * </code>
       * In SSA form this looks like:
       * <code>
       *  if (cond) {
       *    val x0:integer=1;
       *  } else {
       *    val x1:integer=2;
       *  }
       *  val x2 = phi(x1,x2);
       * </code>
       */
      class FindVariableScope
      {
        /** A mapping for each variable name to the statement block in which it must be declared */
      public:
        typedef ::std::map<mylang::names::Name::Ptr, ir::StatementBlock::StatementBlockPtr> DeclarationScopes;

      private:
        ~FindVariableScope() = delete;

        /**
         * Determine if the specified expression has side-effects.
         * @param node the node to use as a starting point
         * @return the blocks where a variable should be defined
         */
      public:
        static ::std::unique_ptr<DeclarationScopes> find(
            const ir::StatementBlock::StatementBlockPtr &node);
      };
    }
  }
}
#endif
