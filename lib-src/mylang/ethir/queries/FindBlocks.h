#ifndef CLASS_MYLANG_ETHIR_QUERIES_FINDVARIABLESCOPE_H
#define CLASS_MYLANG_ETHIR_QUERIES_FINDVARIABLESCOPE_H

#ifndef CLASS_MYLANG_ETHIR_NAME_H
#include <mylang/ethir/Name.h>
#endif

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#include <memory>
#include <optional>
#include <map>
#include <set>

namespace mylang {
  namespace ethir {
    namespace queries {

      /**
       * Find blocks of straight line code. Unlike basic blocks, the returned blocks may be enclosed
       * in other blocks. The primary purpose of this function is to determine which variables are local
       * to a block and which variables are updated in the block (or its enclosing blocks).
       */
      class FindBlocks
      {
        /**
         * A block.
         */
      public:
        class Block
        {
        public:
          typedef ::std::shared_ptr<Block> Ptr;

          /** The statement that starts this block */
        public:
          EStatement statement;

          /** The label for this block (optional) */
        public:
          ::std::optional<Name> label;

          /** The variables defined in this block */
        public:
          ::std::set<Name> blockVariables;

          /** The names of variables updated in this block or a child block */
        public:
          ::std::set<Name> updates;

          /** Locally updates variables */
        public:
          ::std::set<Name> localUpdates;
        };

        /** The block list is mapping of statement to block */
      public:
        typedef ::std::map<EStatement, Block::Ptr> Blocks;

      private:
        ~FindBlocks() = delete;

        /**
         * Find all blocks
         * @param node the node to use as a starting point
         * @return an blocks found
         */
      public:
        static Blocks find(const ENode &node);
      };
    }
  }
}
#endif
