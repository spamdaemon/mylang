#ifndef CLASS_MYLANG_ETHIR_QUERIES_GETLIMITS_H
#define CLASS_MYLANG_ETHIR_QUERIES_GETLIMITS_H

#ifndef CLASS_MYLANG_ETHIR_LIMITS_LIMITS_H
#include <mylang/ethir/limits/Limits.h>
#endif

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_NAME_H
#include <mylang/ethir/Name.h>
#endif

#include <map>

namespace mylang {
  namespace ethir {
    namespace queries {

      /**
       * Determine the limits of an expression.
       */
      class GetLimits
      {

        /** A mapping for each variable name to the statement block in which it must be declared */
      public:
        typedef ::std::map<Name, mylang::ethir::limits::Limits::LimitsPtr> VarBounds;

      private:
        ~GetLimits() = delete;

        /**
         * Determine the limits for the result of an expression. Any variables will
         * be resolved with the provided limits.
         * @param node the node to use as a starting point
         * @param varbounds the bounds of variables
         * @return the limits of the expression result
         */
      public:
        static mylang::ethir::limits::Limits::LimitsPtr get(const EExpression &node,
            const VarBounds &varbounds);

      };
    }
  }
}
#endif
