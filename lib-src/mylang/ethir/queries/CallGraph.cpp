#include <mylang/ethir/queries/CallGraph.h>
#include <mylang/ethir/ir/DefaultNodeVisitor.h>
#include <set>

namespace mylang {
  namespace ethir {
    namespace queries {
      using namespace mylang::ethir::ir;

      struct Node
      {
        Node(EVariable xname, ELambda xlambda)
            : name(xname), lambda(xlambda)
        {
        }

        /** The name of variable that holds a lambda */
        EVariable name;

        /** The lambda function */
        ELambda lambda;

        /** A set of functions that it calls */
        ::std::set<Name> calls;
      };

      /** Find all lambda expressions and calls in a program */
      static void findFunctionsAndCalls(const EProgram &prg, ::std::map<Name, Node> &nodes)
      {
        struct V: public DefaultNodeVisitor
        {
          V(::std::map<Name, Node> &xnodes)
              : nodes(xnodes), currentNode(nullptr)
          {
          }
          ~V()
          {
          }

          bool recordFunction(AbstractValueDeclaration::AbstractValueDeclarationPtr stmt)
          {
            if (stmt->value) {
              auto lambda = stmt->value->self<LambdaExpression>();
              if (lambda) {
                auto insert_result = nodes.insert(
                    ::std::make_pair(stmt->name, Node(stmt->variable, lambda)));
                Node *bak = currentNode;
                currentNode = &insert_result.first->second;
                visitNode(lambda->body);
                currentNode = bak;
                visitNode(stmt->next);
                return true;
              }
            }
            return false;
          }

          void visitOperatorExpression(OperatorExpression::OperatorExpressionPtr expr) override
          {
            if (currentNode && expr->name == OperatorExpression::OP_CALL) {
              currentNode->calls.insert(expr->arguments.at(0)->name);
            }
          }

          void visitProcessValue(ProcessValue::ProcessValuePtr stmt) override
          {
            if (recordFunction(stmt)) {
              return;
            }
            DefaultNodeVisitor::visitProcessValue(stmt);
          }

          void visitValueDecl(ValueDecl::ValueDeclPtr stmt) override
          {
            if (recordFunction(stmt)) {
              return;
            }
            DefaultNodeVisitor::visitValueDecl(stmt);
          }

          void visitGlobalValue(GlobalValue::GlobalValuePtr node)
          {
            if (recordFunction(node)) {
              return;
            }
            DefaultNodeVisitor::visitGlobalValue(node);
          }
          ::std::map<Name, Node> &nodes;
          Node *currentNode;
        };

        V v(nodes);
        prg->accept(v);

        // need to do a bit of post processing on the nodes to remove
        // calls that are not actually pointing directly to a function
        for (auto &entry : nodes) {
          auto &node = entry.second;
          for (auto i = node.calls.begin(); i != node.calls.end();) {
            if (nodes.count(*i) == 0) {
              i = node.calls.erase(i);
            } else {
              ++i;
            }
          }
        }
      }

      CallGraph::CallGraph()
      {
      }
      CallGraph::~CallGraph()
      {
      }

      ::std::unique_ptr<CallGraph> CallGraph::create(EProgram prg)
      {
        struct Impl: public CallGraph
        {
          ~Impl()
          {
          }

          ELambda findLambda(const Name &name) const override final
          {
            auto i = nodes.find(name);
            if (i == nodes.end()) {
              return nullptr;
            } else {
              return i->second.lambda;
            }
          }

          void printDebug(::std::ostream &out) const override
          {
            for (auto &e : nodes) {
              out << e.first << " :";
              char sep = ' ';
              for (auto &call : e.second.calls) {
                out << sep << call;
                sep = ',';
              }
              ::std::cerr << ::std::endl;
            }
          }

          bool isRecursive(const Name &name, ::std::set<Name> &visited) const
          {
            if (visited.insert(name).second == false) {
              // we've already seen the function
              return true;
            }
            auto i = nodes.find(name);
            for (auto call : i->second.calls) {
              if (isRecursive(call, visited)) {
                return true;
              }
            }
            return false;
          }

          bool isRecursive(const Name &name) const override final
          {
            bool res;
            auto i = nodes.find(name);
            if (i == nodes.end()) {
              res = false;
            } else {
              ::std::set<Name> visited;
              res = isRecursive(name, visited);
            }
            return res;
          }

          /** the functions in the callgraph */
          ::std::map<Name, Node> nodes;
        };
        auto res = ::std::make_unique<Impl>();

        findFunctionsAndCalls(prg, res->nodes);

        return res;
      }
    }
  }
}
