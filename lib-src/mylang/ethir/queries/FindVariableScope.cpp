#include <mylang/ethir/queries/FindVariableScope.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/ir/DefaultNodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    using namespace ir;
    namespace queries {

      namespace {

        struct Block
        {
          typedef ::std::shared_ptr<Block> Ptr;
          Block()
              : level(0)
          {
          }

          Block::Ptr enclosing;
          StatementBlock::StatementBlockPtr block;
          size_t level;
        };

        static Block::Ptr findEnclosingBlock(::std::shared_ptr<Block> a, Block::Ptr b)
        {
          while (a->level > b->level) {
            a = a->enclosing;
          }
          while (a->level < b->level) {
            b = b->enclosing;
          }
          while (a != b) {
            assert(a->level == b->level);
            a = a->enclosing;
            b = b->enclosing;
          }
          if (a->block == nullptr) {
            // we've reach the root block
            throw ::std::runtime_error("Failed to find an enclosing block");
          }
          return a;
        }

      }

      ::std::unique_ptr<FindVariableScope::DeclarationScopes> FindVariableScope::find(
          const StatementBlock::StatementBlockPtr &node)
      {
        struct V: public DefaultNodeVisitor
        {
          V()
              : block(::std::make_shared<Block>())
          {
          }

          ~V()
          {
          }

          void record(Variable::VariablePtr var)
          {
            if (var->scope == Variable::Scope::FUNCTION_SCOPE) {
              mylang::names::Name::Ptr name = var->name.source();
              auto s = scopes.find(name);
              if (s == scopes.end()) {
                scopes.insert(::std::make_pair(name, block));
              } else {
                s->second = findEnclosingBlock(s->second, block);
              }
            }
          }

          void visitVariable(Variable::VariablePtr expr) override
          {
            record(expr);
            DefaultNodeVisitor::visitVariable(expr);
          }

          void visitStatementBlock(StatementBlock::StatementBlockPtr stmt) override
          {
            visitNode(stmt->next);
            Block::Ptr restore = block;

            block = ::std::make_shared<Block>();
            block->enclosing = restore;
            block->block = stmt;
            block->level = restore->level + 1;
            visitNode(stmt->statements);
            block = restore;
          }

          ::std::map<mylang::names::Name::Ptr, Block::Ptr> scopes;

          //the currently active statement block
          Block::Ptr block;
        };

        V v;
        node->accept(v);
        auto scopes = std::make_unique<DeclarationScopes>();
        for (const auto &e : v.scopes) {
          scopes->insert(::std::make_pair(e.first, e.second->block));
        }
        return scopes;

      }

    }
  }
}
