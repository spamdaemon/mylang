#ifndef CLASS_MYLANG_ETHIR_QUERIES_FINDTYPESHADOWS_H
#define CLASS_MYLANG_ETHIR_QUERIES_FINDTYPESHADOWS_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#include <map>

namespace mylang {
  namespace ethir {
    namespace queries {

      /**
       * The purpose of this query is to find the source value for any given declaration, even if a
       * series of casts is between them. For example, if
       *   let x:int8 = ...;
       *   let y:integer = cast<integer>(x);
       *   let z:int16 = cast<int16>(y);
       * then y and z are shadows of x and z can be rewritten as
       *   let z:int16 = cast<int16>(x);
       * Also, "cast<int16>(y)" requires a bounds check, but that can also be eliminted by based on the shadow information.
       *
       */
      class FindTypeShadows
      {
      public:
        struct Shadow
        {

          /** A variable that references the initial value or a literal */
          EExpression initial;

          /** The intersection of all the types */
          EType intersection;
        };

        /** Destructor */
      private:
        ~FindTypeShadows() = delete;

        /** Find the type shadows for each expression. */
      public:
        static ::std::map<Name, Shadow> find(ENode node);
      };
    }

  }
}
#endif
