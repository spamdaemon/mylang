#ifndef CLASS_MYLANG_ETHIR_QUERIES_SIDEEFFECTS_H
#define CLASS_MYLANG_ETHIR_QUERIES_SIDEEFFECTS_H

#ifndef CLASS_MYLANG_ETHIR_ENVIRONMENT_H
#include <mylang/ethir/Environment.h>
#endif

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#include <memory>

namespace mylang {
  namespace ethir {
    namespace queries {

      /**
       * Test an expression for side-effects that would change the
       * state of the system. In other words, if an expression with a side-effect were
       * to be eliminated, then the state of the system would not be as expected.
       */
      class SideEffects
      {
        /** The type of side-effects that can occur.  */
      public:
        enum Effect
        {
          /** No side effect */
          EFFECT_NONE = 0,
          /** Optimization may produce incorrect log message or none at all */
          EFFECT_LOGGING = 1,
          /** A volatile read is performed; repeated reads may produce different results */
          EFFECT_READ_VOLATILE = 2,
          /** An expression or statement modifies state in a way that is not observable by the compiler */
          EFFECT_STATE = 4
        };

      private:
        ~SideEffects() = delete;

        /**
         * Determine if the specified expression has side-effects.
         * @param env a bind environment for variables
         * @param expr an expression
         * @returns true if the expression has side-effects, false otherwise.
         */
      public:
        static Effect test(const Environment &env, const EExpression &expr);
      };
    }
  }
}
#endif
