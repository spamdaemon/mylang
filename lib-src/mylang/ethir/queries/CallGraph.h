#ifndef CLASS_MYLANG_ETHIR_QUERIES_CALLGRAPH_H
#define CLASS_MYLANG_ETHIR_QUERIES_CALLGRAPH_H

#ifndef CLASS_MYLANG_ETHIR_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#include <memory>
#include <iosfwd>

namespace mylang {
  namespace ethir {
    namespace queries {

      /**
       * A simplified callgraph. The callgraph only traces calls between functions
       * if a direct call is made.
       */
      class CallGraph
      {
      public:
        virtual ~CallGraph() = 0;

      private:
        CallGraph();

        /**
         * Create the callgraph of a program.
         * @param prg a prgoram
         * @return the callgraph for the program
         */
      public:
        static ::std::unique_ptr<CallGraph> create(EProgram program);

        /**
         * Print  the graph for debugging purposes
         * @param out an output stream
         */
      public:
        virtual void printDebug(::std::ostream &out) const =0;

        /**
         * Get the lambda expression with the specified name.
         * @param name a name
         * @return the lambda function
         */
      public:
        virtual ELambda findLambda(const Name &name) const = 0;

        /**
         * Determine if the specified function is recursive.
         * @param name a name
         * @return true if the function is recursive, false otherwise
         */
      public:
        virtual bool isRecursive(const Name &name) const = 0;

      };
    }
  }
}
#endif
