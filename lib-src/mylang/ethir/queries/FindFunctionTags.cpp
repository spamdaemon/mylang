#include <mylang/ethir/queries/FindFunctionTags.h>
#include <mylang/ethir/ir/DefaultNodeVisitor.h>

namespace mylang {
  namespace ethir {
    namespace queries {
      using namespace mylang::ethir::ir;

      EVariable FindFunctionTags::TaggedFunctions::findBySignature(const EType &type) const
      {
        for (auto decl : functions) {
          if (decl->type->isSameType(*type)) {
            return decl->variable;
          }
        }
        return nullptr;
      }

      FindFunctionTags::FunctionsByTag FindFunctionTags::find(const ENode &node)
      {

        struct V: public DefaultNodeVisitor
        {
          V()
          {
          }
          ~V()
          {
          }

          void recordFunction(AbstractValueDeclaration::AbstractValueDeclarationPtr decl)
          {
            if (!decl->value) {
              return;
            }
            auto lambda = decl->value->self<LambdaExpression>();
            if (!lambda) {
              return;
            }

            TaggedFunctions &fn = tags[lambda->tag];
            fn.functions.push_back(decl);
          }

          void visitProcessValue(ProcessValue::ProcessValuePtr stmt) override
          {
            recordFunction(stmt);
            DefaultNodeVisitor::visitProcessValue(stmt);
          }

          void visitValueDecl(ValueDecl::ValueDeclPtr stmt) override
          {
            recordFunction(stmt);
            DefaultNodeVisitor::visitValueDecl(stmt);
          }

          void visitGlobalValue(GlobalValue::GlobalValuePtr node)
          {
            recordFunction(node);
            DefaultNodeVisitor::visitGlobalValue(node);
          }

          FunctionsByTag tags;
        };

        V v;
        node->accept(v);
        return v.tags;
      }
    }
  }
}
