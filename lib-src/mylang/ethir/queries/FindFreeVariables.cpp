#include <mylang/ethir/queries/FindFreeVariables.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/ir/DefaultNodeVisitor.h>

namespace mylang {
  namespace ethir {
    using namespace ir;
    namespace queries {

      FindFreeVariables::Names FindFreeVariables::find(const ENode &node)
      {
        struct V: public DefaultNodeVisitor
        {
          V()
          {
          }
          ~V()
          {
          }

          void define(const EVariable &e)
          {
            if (e) {
              defined[e->name] = e;
            }
          }

          void visitGlobalValue(GlobalValue::GlobalValuePtr node) override
          {
            define(node->variable);
            DefaultNodeVisitor::visitGlobalValue(node);
          }
          void visitLetExpression(LetExpression::LetExpressionPtr expr) override
          {
            define(expr->variable);
            DefaultNodeVisitor::visitLetExpression(expr);
          }
          void visitInputPortDecl(InputPortDecl::InputPortDeclPtr stmt) override
          {
            define(stmt->variable);
            DefaultNodeVisitor::visitInputPortDecl(stmt);
          }
          void visitOutputPortDecl(OutputPortDecl::OutputPortDeclPtr stmt) override
          {
            define(stmt->variable);
            DefaultNodeVisitor::visitOutputPortDecl(stmt);
          }

          void visitProcessVariable(ProcessVariable::ProcessVariablePtr stmt) override
          {
            define(stmt->variable);
            DefaultNodeVisitor::visitProcessVariable(stmt);
          }

          void visitProcessValue(ProcessValue::ProcessValuePtr stmt) override
          {
            define(stmt->variable);
            DefaultNodeVisitor::visitProcessValue(stmt);
          }
          void visitValueDecl(ValueDecl::ValueDeclPtr stmt) override
          {
            define(stmt->variable);
            DefaultNodeVisitor::visitValueDecl(stmt);
          }

          void visitVariableDecl(VariableDecl::VariableDeclPtr stmt) override
          {
            define(stmt->variable);
            DefaultNodeVisitor::visitVariableDecl(stmt);
          }
          void visitParameter(Parameter::ParameterPtr param) override
          {
            define(param->variable);
            DefaultNodeVisitor::visitParameter(param);
          }
          void visitIterateArrayStep(IterateArrayStep::IterateArrayStepPtr node) override
          {
            define(node->counter);
            define(node->value);
            for (auto n : node->initializers) {
              define(n.first);
            }
            DefaultNodeVisitor::visitIterateArrayStep(node);
          }

          void visitSequenceStep(SequenceStep::SequenceStepPtr node) override
          {
            define(node->counter);
            for (auto n : node->initializers) {
              define(n.first);
            }
            DefaultNodeVisitor::visitSequenceStep(node);
          }

          void visitVariable(EVariable v) override
          {
            if (v->name.version() == 0) {
              used[v->name] = v;
            }
          }

          FindFreeVariables::Names used;
          FindFreeVariables::Names defined;
        };

        V v;
        node->accept(v);

        // remove the defined variables from the used ones
        for (auto &e : v.defined) {
          v.used.erase(e.first);
        }

        return ::std::move(v.used);
      }

    }
  }
}
