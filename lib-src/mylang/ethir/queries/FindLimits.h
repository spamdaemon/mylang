#ifndef CLASS_MYLANG_ETHIR_QUERIES_FINDLIMITS_H
#define CLASS_MYLANG_ETHIR_QUERIES_FINDLIMITS_H

#ifndef CLASS_MYLANG_ETHIR_LIMITS_LIMITS_H
#include <mylang/ethir/limits/Limits.h>
#endif

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_NAME_H
#include <mylang/ethir/Name.h>
#endif

#include <memory>
#include <map>
#include <optional>

namespace mylang {
  namespace ethir {
    namespace queries {

      /**
       * Determine the limits for values of a given expression.
       */
      class FindLimits
      {

        /** A mapping for each variable name to the statement block in which it must be declared */
      public:
        typedef ::std::map<Name, mylang::ethir::limits::Limits::LimitsPtr> VarBounds;

      private:
        ~FindLimits() = delete;

        /**
         * Determine the bounds of all known variables. 
         * If the result does not contain a variable, then
         * know bounds information could be derived.
         * @param node the node to use as a starting point
         * @return the bounds for each variable.
         */
      public:
        static VarBounds find(const ENode &node);

      };
    }
  }
}
#endif
