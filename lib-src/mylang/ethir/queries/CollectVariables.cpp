#include <mylang/ethir/queries/CollectVariables.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/ir/DefaultNodeVisitor.h>

namespace mylang {
  namespace ethir {
    using namespace ir;
    namespace queries {

      Variable::Set CollectVariables::collect(const ENode &node)
      {
        struct V: public DefaultNodeVisitor
        {
          V()
          {
          }
          ~V()
          {
          }

          void visitVariable(Variable::VariablePtr expr) override
          {
            vars.insert(expr);
          }

          Variable::Set vars;
        };

        V v;
        node->accept(v);
        return ::std::move(v.vars);
      }

    }
  }
}
