#ifndef CLASS_MYLANG_ETHIR_QUERIES_FINDREACHABLEVARIABLES_H
#define CLASS_MYLANG_ETHIR_QUERIES_FINDREACHABLEVARIABLES_H

#ifndef CLASS_MYLANG_ETHIR_NAME_H
#include <mylang/ethir/Name.h>
#endif

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#include <set>
#include <functional>

namespace mylang {
  namespace ethir {
    namespace queries {

      /**
       * Find all variables that are reachable from an exported global variable.
       * This function requires that all variables have distinct names.
       */
      class FindReachableVariables
      {
        /** A mapping for each variable name to the statement block in which it must be declared.  */
      public:
        typedef ::std::set<Name> Names;

        /** A filter */
      public:
        typedef ::std::function<bool(EGlobal)> Filter;

      private:
        ~FindReachableVariables() = delete;

        /**
         * Find the names of global variables matching the specified filter.
         * @param node the node to use as a starting point
         * @param f a filter
         * @return the names of global values that match the filter
         */
      public:
        static Names findGlobals(const EProgram &node, const Filter &f);

        /**
         * Find the names of all global values that are reachable
         * from global variables that match the specified filter.
         *
         * @param node the node to use as a starting point
         * @param f a filter
         * @return the names of global values that either pass the filter
         * or a reachable from such a node.
         */
      public:
        static Names find(const EProgram &node, const Filter &f);
      };
    }
  }
}
#endif
