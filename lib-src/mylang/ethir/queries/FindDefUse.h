#ifndef CLASS_MYLANG_ETHIR_QUERIES_FINDDEFUSE_H
#define CLASS_MYLANG_ETHIR_QUERIES_FINDDEFUSE_H

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_NAME_H
#include <mylang/ethir/Name.h>
#endif

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#include <map>

namespace mylang {
  namespace ethir {
    namespace queries {

      /**
       * Find the names of all variables that are defined somewhere.
       */
      class FindDefUse
      {
        /** A use */
      public:
        struct Use
        {
          /** The kind of variable; the kind of a variable may be unknown
           * if we only ever encounter uses of the variable */
          enum Kind
          {
            VALUE, VARIABLE, PORT, UNKNOWN
          };

          /** The variable about which this is a used */
          EVariable variable;

          /** The initial value for the variable, if known */
          EExpression initialValue;

          /**
           * True if the variable was defined.
           * Depending on the starting node for finding
           * the DefUse, a node may be used, but it's
           * definition is elsewhere (e.g. a global value).
           */
          bool isDefined;

          /** The kind of variable we have */
          Kind kind;

          /** The updates made to the variable (only for variables) */
          ::std::vector<EExpression> updates;

          /** The number of times a variable was used.
           * a definition does not count as a use
           */
          size_t useCount;
        };

        /** A mapping for each variable name to the statement block in which it must be declared.  */
      public:
        typedef ir::Variable::Map<Use> DefUse;

      private:
        ~FindDefUse() = delete;

        /**
         * Find all variables declared or defined somewhere in the specified node.
         * @param node the node to use as a starting point
         * @return the defined variables indexed by their names
         */
      public:
        static DefUse find(const ENode &node);
      };
    }
  }
}
#endif
