#include <mylang/ethir/queries/FindCallSites.h>
#include <mylang/ethir/ir/DefaultNodeVisitor.h>
#include <set>

namespace mylang {
  namespace ethir {
    namespace queries {
      using namespace mylang::ethir::ir;

      FindCallSites::FunctionCallSites FindCallSites::find(const ENode &node)
      {

        struct V: public DefaultNodeVisitor
        {
          V()
          {
          }
          ~V()
          {
          }

          void recordFunction(AbstractValueDeclaration::AbstractValueDeclarationPtr stmt)
          {
            if (stmt->value && stmt->value->self<LambdaExpression>()) {
              functions[stmt->name] = stmt;
            }
          }

          void visitOpaqueExpression(OpaqueExpression::OpaqueExpressionPtr expr) override
          {
            if (opaque == nullptr) {
              opaque = expr;
            }

            visitNode(opaque->expression);

            if (opaque == expr) {
              opaque = nullptr;
            }
          }

          void visitOperatorExpression(OperatorExpression::OperatorExpressionPtr expr) override
          {
            if (expr->name != OperatorExpression::OP_CALL) {
              return;
            }
            EVariable fn = expr->arguments.at(0);
            CallSites &cs = sites[fn->name];
            cs.sites.push_back(expr);
            if (opaque) {
              // if we're in an opaque expression context,
              // the remember that we've called that function from such a context
              opaqueSites.insert(fn->name);
            }
          }

          void visitProcessValue(ProcessValue::ProcessValuePtr stmt) override
          {
            recordFunction(stmt);
            DefaultNodeVisitor::visitProcessValue(stmt);
          }

          void visitValueDecl(ValueDecl::ValueDeclPtr stmt) override
          {
            recordFunction(stmt);
            DefaultNodeVisitor::visitValueDecl(stmt);
          }

          void visitGlobalValue(GlobalValue::GlobalValuePtr node)
          {
            recordFunction(node);
            DefaultNodeVisitor::visitGlobalValue(node);
          }

          ::std::map<Name, AbstractValueDeclaration::AbstractValueDeclarationPtr> functions;
          FunctionCallSites sites;

          OpaqueExpression::OpaqueExpressionPtr opaque;

          // the set of functions that have been called from within an opaque
          // expression
          ::std::set<Name> opaqueSites;
        };

        V v;
        node->accept(v);
        for (auto i = v.sites.begin(); i != v.sites.end();) {
          auto f = v.functions.find(i->first);
          // remove any sites to functions we've not see, or functions
          // that are called from within an opaque expression
          if (f == v.functions.end() || v.opaqueSites.count(i->first) != 0) {
            i = v.sites.erase(i);
          } else {
            i->second.decl = f->second;
            ++i;
          }
        }
        return v.sites;
      }
    }
  }
}
