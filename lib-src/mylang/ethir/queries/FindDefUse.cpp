#include <mylang/ethir/queries/FindDefUse.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/ir/DefaultNodeVisitor.h>

namespace mylang {
  namespace ethir {
    using namespace ir;
    namespace queries {

      FindDefUse::DefUse FindDefUse::find(const ENode &node)
      {
        struct V: public DefaultNodeVisitor
        {
          V()
          {
          }
          ~V()
          {
          }

          Use& getUse(EVariable e)
          {
            auto i = uses.find(e);
            if (i == uses.end()) {
              Use defUse { e, nullptr, false, Use::UNKNOWN, { }, 0 };
              if (e->type->self<types::InputOutputType>()) {
                defUse.kind = Use::PORT;
              }
              i = uses.insert( { e, defUse }).first;
            }
            return i->second;
          }

          void defValue(EVariable e, EExpression val)
          {
            auto &use = getUse(e);
            use.kind = Use::VALUE;
            use.isDefined = true;
            if (val) {
              use.initialValue = val;
            }
          }

          void defVariable(EVariable e, EExpression val)
          {
            if (e) {
              auto &use = getUse(e);
              use.kind = Use::VARIABLE;
              use.isDefined = true;
              if (val) {
                // in some instances we may define a variable twice
                // just because of the way we visit tree; things should
                // still be consistent, though!
                use.initialValue = val;
              }
            }
          }

          void defPort(EVariable e)
          {
            auto &use = getUse(e);
            if (!use.isDefined) {
              use.kind = Use::PORT;
              use.isDefined = true;
            }
          }

          void use(EVariable e)
          {
            ++getUse(e).useCount;
          }

          void update(EVariable e, EExpression val)
          {
            auto &use = getUse(e);
            use.updates.push_back(val);
            if (use.kind == Use::UNKNOWN) {
              use.kind = Use::VARIABLE;
            }
          }

          void visitVariable(EVariable node) override
          {
            use(node);
          }
          void visitVariableUpdate(VariableUpdate::VariableUpdatePtr node) override
          {
            update(node->target, node->value);
            visitNode(node->value);
            visitNode(node->next);
          }
          void visitProcessVariableUpdate(ProcessVariableUpdate::ProcessVariableUpdatePtr node)
          override
          {
            update(node->target, node->value);
            visitNode(node->value);
            visitNode(node->next);
          }
          void visitIterateArrayStep(IterateArrayStep::IterateArrayStepPtr node) override
          {
            for (auto i : node->initializers) {
              defVariable(i.first, i.second);
              visitNode(i.second);
            }
            use(node->array);
            defVariable(node->counter, nullptr);
            defValue(node->value, nullptr);
            visitNode(node->nextState);
            visitNode(node->body);
          }
          void visitSequenceStep(SequenceStep::SequenceStepPtr node) override
          {
            for (auto i : node->initializers) {
              defVariable(i.first, i.second);
              visitNode(i.second);
            }
            use(node->iterationCount);
            defVariable(node->counter, nullptr);
            visitNode(node->body);
            visitNode(node->nextState);
          }
          void visitSingleValueStep(SingleValueStep::SingleValueStepPtr node) override
          {
            use(node->value);
            visitNode(node->body);
          }
          void visitSkipStatement(SkipStatement::SkipStatementPtr stmt) override
          {
            for (auto u : stmt->updates) {
              update(u.first, u.second);
              visitNode(u.second);
            }
            visitNode(stmt->next);
          }

          void visitGlobalValue(GlobalValue::GlobalValuePtr node) override
          {
            defValue(node->variable, node->value);
            visitNode(node->value);
            visitNode(node->next);
          }
          void visitLetExpression(LetExpression::LetExpressionPtr expr) override
          {
            defValue(expr->variable, expr->value);
            visitNode(expr->value);
            visitNode(expr->result);
          }
          void visitInputPortDecl(InputPortDecl::InputPortDeclPtr stmt) override
          {
            defPort(stmt->variable);
            visitNode(stmt->next);
          }
          void visitOutputPortDecl(OutputPortDecl::OutputPortDeclPtr stmt) override
          {
            defPort(stmt->variable);
            visitNode(stmt->next);
          }

          void visitProcessVariable(ProcessVariable::ProcessVariablePtr stmt) override
          {
            defVariable(stmt->variable, stmt->value);
            visitNode(stmt->value);
            visitNode(stmt->next);
          }

          void visitProcessValue(ProcessValue::ProcessValuePtr stmt) override
          {
            defValue(stmt->variable, stmt->value);
            visitNode(stmt->value);
            visitNode(stmt->next);
          }
          void visitValueDecl(ValueDecl::ValueDeclPtr stmt) override
          {
            defValue(stmt->variable, stmt->value);
            visitNode(stmt->value);
            visitNode(stmt->next);
          }

          void visitVariableDecl(VariableDecl::VariableDeclPtr stmt) override
          {
            defVariable(stmt->variable, stmt->value);
            visitNode(stmt->value);
            visitNode(stmt->next);
          }
          void visitParameter(Parameter::ParameterPtr param) override
          {
            defValue(param->variable, nullptr);
            DefaultNodeVisitor::visitParameter(param);
          }

          FindDefUse::DefUse uses;
        };

        V v;
        node->accept(v);
        return ::std::move(v.uses);
      }

    }
  }
}
