#ifndef CLASS_MYLANG_ETHIR_QUERIES_COUNTSTATEMENTS_H
#define CLASS_MYLANG_ETHIR_QUERIES_COUNTSTATEMENTS_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#include <set>

namespace mylang {
  namespace ethir {
    namespace queries {

      /**
       * Count the number of statements below a node.
       */
      class CountStatements
      {

      private:
        ~CountStatements() = delete;

        /**
         * Count the number of statements.
         * @param node the node to use as a starting point
         * @return the number of statements that may be executed.
         */
      public:
        static size_t countStatements(const ENode &node);
      };
    }
  }
}
#endif
