#include <mylang/ethir/ir/DefaultNodeVisitor.h>
#include <mylang/ethir/ir/GlobalValue.h>
#include <mylang/ethir/ir/ProcessValue.h>
#include <mylang/ethir/ir/ValueDecl.h>
#include <mylang/ethir/ir/Variable.h>
#include <mylang/ethir/ir/VariableDecl.h>
#include <mylang/ethir/ir/VariableUpdate.h>
#include <mylang/ethir/queries/FindLimits.h>
#include <mylang/ethir/queries/GetLimits.h>
#include <mylang/ethir/limits/ArrayLimits.h>
#include <mylang/ethir/limits/BooleanLimits.h>
#include <stddef.h>

namespace mylang {
  namespace ethir {
    using namespace ir;
    namespace queries {

      FindLimits::VarBounds FindLimits::find(const ENode &node)
      {
        struct V: public DefaultNodeVisitor
        {
          size_t loopNesting;

          V()
              : loopNesting(0)
          {
          }

          ~V()
          {
          }

          limits::Limits::LimitsPtr getLimits(const EExpression &expr)
          {
            limits::Limits::LimitsPtr lim = GetLimits::get(expr, bounds);
            return lim;
          }

          limits::Limits::LimitsPtr updateLimits(const Variable::VariablePtr &v,
              const EExpression &expr)
          {
            if (!expr) {
              return nullptr;
            }

            limits::Limits::LimitsPtr lim = getLimits(expr);
            if (!lim) {
              lim = limits::Limits::of(v->type);
            }

            if (lim) {
              auto i = bounds.find(v->name);
              if (i == bounds.end() || loopNesting == 0) {
                //::std::cerr << "Found a limit for " << v->name << ::std::endl;
                bounds[v->name] = lim;
              } else {
                //::std::cerr << "Updating limit for " << v->name << ::std::endl;
                i->second = limits::Limits::merge(i->second, lim);
              }
            }
            return lim;
          }

          void visitGlobalValue(GlobalValue::GlobalValuePtr node) override
          {
            updateLimits(node->variable, node->value);
            DefaultNodeVisitor::visitGlobalValue(node);
          }

          void visitProcessValue(ProcessValue::ProcessValuePtr stmt) override
          {
            updateLimits(stmt->variable, stmt->value);
            DefaultNodeVisitor::visitProcessValue(stmt);
          }
          void visitValueDecl(ValueDecl::ValueDeclPtr stmt) override
          {
            updateLimits(stmt->variable, stmt->value);
            DefaultNodeVisitor::visitValueDecl(stmt);
          }

          void visitVariableDecl(VariableDecl::VariableDeclPtr stmt) override
          {
            updateLimits(stmt->variable, stmt->value);
            DefaultNodeVisitor::visitVariableDecl(stmt);
          }

          void visitVariableUpdate(VariableUpdate::VariableUpdatePtr stmt) override
          {
            updateLimits(stmt->target, stmt->value);
            DefaultNodeVisitor::visitVariableUpdate(stmt);
          }

          void visitForeachStatement(ForeachStatement::ForeachStatementPtr stmt) override
          {
            auto src = getLimits(stmt->data);
            if (src) {
              // we need to get the limits of the elements in the array
              if (src->self<limits::ArrayLimits>()) {
                bounds[stmt->variable->name] = src->self<limits::ArrayLimits>()->elements;
              }
            }
            ++loopNesting;
            stmt->body->accept(*this);
            // need to visit the loop twice to deal with loop variables whose values aren't known
            // until the loop continues
            //stmt->body->accept(*this);
            --loopNesting;
            visitNode(stmt->next);
          }

          void visitLoopStatement(LoopStatement::LoopStatementPtr stmt) override
          {
            ++loopNesting;
            stmt->body->accept(*this);
            // need to visit the loop twice to deal with loop variables whose values aren't known
            // until the loop continues
            //stmt->body->accept(*this);
            --loopNesting;
            visitNode(stmt->next);
          }

          void visitIfStatement(IfStatement::IfStatementPtr stmt) override
          {
            limits::BooleanLimits::Ptr cond;
            auto condLimits = getLimits(stmt->condition);
            if (condLimits) {
              cond = condLimits->self<limits::BooleanLimits>();
            }

            if (!cond || cond->contains(true)) {
              stmt->iftrue->accept(*this);
            }
            if (!cond || cond->contains(false)) {
              stmt->iffalse->accept(*this);
            }
            visitNode(stmt->next);
          }

          VarBounds bounds;

        };

        V v;
        node->accept(v);
        return v.bounds;

      }
    }
  }
}
