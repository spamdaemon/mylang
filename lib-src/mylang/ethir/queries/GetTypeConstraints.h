#ifndef CLASS_MYLANG_ETHIR_QUERIES_GETTYPECONSTRAINTS_H
#define CLASS_MYLANG_ETHIR_QUERIES_GETTYPECONSTRAINTS_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#include <map>
#include <functional>

namespace mylang {
  namespace ethir {
    namespace queries {

      /**
       * Determine the limits of an expression.
       */
      class GetTypeConstraints
      {

        /** A mapping for each variable name to the statement block in which it must be declared */
      public:
        typedef ::std::map<Name, EType> VarTypes;

        /** Update the variable with specified new type */
      public:
        typedef ::std::function<void(EVariable, EType)> UpdateFN;

      private:
        ~GetTypeConstraints() = delete;

        /**
         * Constraint the types of variables in an expression
         * @param node the node to use as a starting point
         * @param varbounds the bounds of variables
         * @parma update this function is invoked with a more restricted variable type
         */
      public:
        static void get(const EExpression &node, const VarTypes &varbounds, UpdateFN update);

      };
    }
  }
}
#endif
