#include <mylang/ethir/queries/GetTypeConstraints.h>
#include <mylang/ethir/ir/AbstractOperatorVisitor.h>
#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/OperatorExpression.h>
#include <mylang/ethir/ir/Variable.h>

namespace mylang {
  namespace ethir {
    namespace queries {

      namespace {

        struct OpVisitor: public ir::AbstractOperatorVisitor
        {

          OpVisitor(const GetTypeConstraints::VarTypes &xvartypes,
              GetTypeConstraints::UpdateFN &xupdate)
              : types(xvartypes), update(xupdate)
          {
          }

          ~OpVisitor()
          {
          }

          EType getBound(const EVariable &v) const
          {
            auto i = types.find(v->name);
            if (i == types.end()) {
              return nullptr;
            }
            return i->second;
          }

          void tryUpdate(const EVariable &v, const EType &ty) const
          {
            if (ty && !ty->isSameType(*v->type)) {
              update(v, ty);
            }
          }

          void visit_EQ(ir::OperatorExpression::OperatorExpressionPtr e) override final
          {
            auto l = getBound(e->arguments.at(0));
            auto r = getBound(e->arguments.at(1));
            if (l && r) {
              auto ty = l->intersectWith(r);
              tryUpdate(e->arguments.at(0), ty);
              tryUpdate(e->arguments.at(1), ty);
            }
          }

          void visit_NEQ(ir::OperatorExpression::OperatorExpressionPtr e) override final
          {
            // TODO: at the moment, there is nothing we can do it seems.
          }
          void visit_LT(ir::OperatorExpression::OperatorExpressionPtr e) override final
          {
            auto l = getBound(e->arguments.at(0));
            auto r = getBound(e->arguments.at(1));
            if (l && r) {
              // TODO: this is actually LTE, not LT; we should reduce the type of r by 1
              tryUpdate(e->arguments.at(0), l->intersectWith(r));
            }
          }

          void visit_LTE(ir::OperatorExpression::OperatorExpressionPtr e) override final
          {
            auto l = getBound(e->arguments.at(0));
            auto r = getBound(e->arguments.at(1));
            if (l && r) {
              tryUpdate(e->arguments.at(0), l->intersectWith(r));
            }
          }

          const GetTypeConstraints::VarTypes &types;
          GetTypeConstraints::UpdateFN &update;
        };

      }

      void GetTypeConstraints::get(const EExpression &node, const VarTypes &vartypes,
          UpdateFN updateFN)
      {
        auto op = node->self<ir::OperatorExpression>();
        if (op) {
          OpVisitor v(vartypes, updateFN);
          v.visit(op);
        }

      }
    }
  }
}
