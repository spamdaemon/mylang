#include <mylang/ethir/queries/FindReachableVariables.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/ir/DefaultNodeVisitor.h>

namespace mylang {
  namespace ethir {
    using namespace ir;
    namespace queries {

      FindReachableVariables::Names FindReachableVariables::findGlobals(const EProgram &node,
          const Filter &f)
      {
        FindReachableVariables::Names names;
        for (auto g : node->globals) {
          if (f(g)) {
            names.insert(g->name);
          }
        }
        return names;
      }

      FindReachableVariables::Names FindReachableVariables::find(const EProgram &node,
          const Filter &f)
      {
        struct V: public DefaultNodeVisitor
        {
          V(const Filter &f)
              : filter(f)
          {
          }
          ~V()
          {
          }

          void visitVariable(Variable::VariablePtr expr) override
          {
            names.insert(expr->name);
            DefaultNodeVisitor::visitVariable(expr);
          }

          void visitProgram(Program::ProgramPtr prg) override
          {
            for (auto proc : prg->processes) {
              proc->accept(*this);
            }

            // pass 1: visit all exported functions
            Program::Globals reachable;
            Program::Globals candidates(prg->globals);
            for (auto i = candidates.begin(); i != candidates.end();) {
              const EGlobal &g = *i;

              if (filter(g)) {
                names.insert(g->name);
                if (g->value->self<LambdaExpression>()) {
                  g->accept(*this);
                  reachable.push_back(g);
                  i = candidates.erase(i);
                } else {
                  ++i;
                }
              } else {
                ++i;
              }
            }

            // pass 2: repeatedly visit the remaining globals if they are in the names set
            for (auto i = candidates.begin(); i != candidates.end();) {
              auto &g = *i;
              if (names.count(g->name) > 0) {
                g->accept(*this);
                reachable.push_back(g);
                candidates.erase(i); // no longer a candidate to consider
                i = candidates.begin();
              } else {
                ++i;
              }
            }
          }

          const Filter &filter;
          FindReachableVariables::Names names;
        };

        V v(f);
        node->accept(v);
        return ::std::move(v.names);
      }

    }
  }
}
