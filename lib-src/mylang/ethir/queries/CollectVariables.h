#ifndef CLASS_MYLANG_ETHIR_QUERIES_COLLECTVARIABLES_H
#define CLASS_MYLANG_ETHIR_QUERIES_COLLECTVARIABLES_H

#ifndef CLASS_MYLANG_ETHIR_NAME_H
#include <mylang/ethir/Name.h>
#endif

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#include <set>

namespace mylang {
  namespace ethir {
    namespace queries {

      /**
       * What??
       */
      class CollectVariables
      {

      private:
        ~CollectVariables() = delete;

        /**
         * Determine if the specified expression has side-effects.
         * @param node the node to use as a starting point
         * @return the blocks where a variable should be defined
         */
      public:
        static ir::Variable::Set collect(const ENode &node);
      };
    }
  }
}
#endif
