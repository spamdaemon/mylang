#include <idioma/ast/GroupNode.h>
#include <idioma/ast/Node.h>
#include <idioma/ast/TokenNode.h>
#include <mylang/annotations.h>
#include <mylang/BigInt.h>
#include <mylang/BigReal.h>
#include <mylang/BigUInt.h>
#include <mylang/constraints/ConstrainedArrayType.h>
#include <mylang/constraints/ConstrainedFunctionType.h>
#include <mylang/constraints/ConstrainedInputType.h>
#include <mylang/constraints/ConstrainedMutableType.h>
#include <mylang/constraints/ConstrainedNamedType.h>
#include <mylang/constraints/ConstrainedOptType.h>
#include <mylang/constraints/ConstrainedOutputType.h>
#include <mylang/constraints/ConstrainedProcessType.h>
#include <mylang/constraints/ConstrainedStructType.h>
#include <mylang/constraints/ConstrainedTupleType.h>
#include <mylang/constraints/ConstrainedTypeVisitor.h>
#include <mylang/constraints/ConstrainedUnionType.h>
#include <mylang/constraints/ConstrainedTypeSet.h>
#include <mylang/defs.h>
#include <mylang/ethir/ethir.h>
#include <mylang/ethir/ir/StatementBuilder.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/prettyPrint.h>
#include <mylang/ethir/types/types.h>
#include <mylang/ethir/types/TypeSet.h>
#include <mylang/ethir/types/TypeVisitor.h>
#include <mylang/expressions/Constant.h>
#include <mylang/names/Name.h>
#include <mylang/variables/Variable.h>
#include <mylang/vast/queries/VariableFinder.h>
#include <mylang/vast/VAst.h>
#include <mylang/vast/VAstVisitor.h>
#include <algorithm>
#include <cassert>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <optional>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

namespace mylang {
  namespace ethir {
    namespace {
      using mylang::vast::VAst;
      using namespace mylang::constraints;

      struct ProcessMembers
      {
        ir::ProcessDecl::Ports ports;
        ir::ProcessDecl::Declarations variables;
        ir::ProcessDecl::Constructors constructors;
        ir::ProcessDecl::Blocks blocks;
      };

      struct TypeConverter: public mylang::constraints::ConstrainedTypeVisitor
      {
        TypeConverter(mylang::vast::VAstVisitor<ENode> &xnodeConverter)
            : nodeConverter(xnodeConverter), wknBit(types::BitType::create()),
                wknBoolean(types::BooleanType::create()), wknByte(types::ByteType::create()),
                wknChar(types::CharType::create())
        {
        }

        void visitAny(const ::std::shared_ptr<const ConstrainedAnyType>&)
        {
          // any type is NOT support
          throw ::std::runtime_error("ConstrainedAnyType is not supported");
        }

        void visitBit(const ::std::shared_ptr<const ConstrainedBitType>&)
        {
          type = wknBit;
        }

        void visitBoolean(const ::std::shared_ptr<const ConstrainedBooleanType>&)
        {
          type = wknBoolean;
        }

        void visitByte(const ::std::shared_ptr<const ConstrainedByteType>&)
        {
          type = wknByte;
        }

        void visitChar(const ::std::shared_ptr<const ConstrainedCharType>&)
        {
          type = wknChar;
        }

        void visitReal(const ::std::shared_ptr<const ConstrainedRealType>&)
        {
          type = types::RealType::create();
        }

        void visitInteger(const ::std::shared_ptr<const ConstrainedIntegerType> &constraint)
        {
          type = types::IntegerType::create(constraint->range);
        }

        void visitString(const ::std::shared_ptr<const ConstrainedStringType>&)
        {
          type = types::StringType::create();
        }

        void visitVoid(const ::std::shared_ptr<const ConstrainedVoidType>&)
        {
          type = types::VoidType::create();
        }

        void visitGeneric(const ::std::shared_ptr<const ConstrainedGenericType>&)
        {
          type = types::GenericType::create();
        }

        void visitArray(const ::std::shared_ptr<const ConstrainedArrayType> &constraint)
        {
          auto elemTy = toType(constraint->element);
          type = types::ArrayType::get(elemTy, constraint->bounds);
        }

        void visitUnion(const ::std::shared_ptr<const ConstrainedUnionType> &constraint)
        {
          mylang::vast::VAst ast;

          types::UnionType::Discriminant discriminant(constraint->discriminant.name,
              toType(constraint->discriminant.constraint));

          ::std::vector<types::UnionType::Member> members;
          for (auto m : constraint->members) {
            auto ty = toType(m.constraint);
            auto lit = ast.createLiteral(m.discriminant, idioma::ast::Node::Span());
            auto node = nodeConverter.visit(lit);
            if (!node) {
              throw ::std::runtime_error("Cannot convert literal " + m.discriminant->value());
            }
            auto literal = node->self<ir::Literal>();
            literal = literal->castTo(discriminant.type);
            members.push_back(types::UnionType::Member(literal, m.name, ty));
          }
          type = types::UnionType::get(discriminant, members);
        }

        void visitStruct(const ::std::shared_ptr<const ConstrainedStructType> &constraint)
        {
          ::std::vector<types::StructType::Member> members;
          for (auto m : constraint->members) {
            members.push_back(types::StructType::Member(m.name, toType(m.constraint)));
          }
          type = types::StructType::get(members);
        }

        void visitTuple(const ::std::shared_ptr<const ConstrainedTupleType> &constraint)
        {
          ::std::vector<EType> elements;
          for (auto e : constraint->types) {
            elements.push_back(toType(e));
          }
          type = types::TupleType::get(elements);

        }

        void visitFunction(const ::std::shared_ptr<const ConstrainedFunctionType> &constraint)
        {
          auto retTy = toType(constraint->returnType);
          ::std::vector<EType> parameters;
          for (auto p : constraint->parameters) {
            parameters.push_back(toType(p));
          }
          type = types::FunctionType::get(retTy, parameters);
        }

        void visitOpt(const ::std::shared_ptr<const ConstrainedOptType> &constraint)
        {
          auto elemTy = toType(constraint->element);
          type = types::OptType::get(elemTy);
        }

        void visitMutable(const ::std::shared_ptr<const ConstrainedMutableType> &constraint)
        {
          auto elemTy = toType(constraint->element);
          type = types::MutableType::get(elemTy);
        }

        void visitNamedType(const ::std::shared_ptr<const ConstrainedNamedType> &constraint)
        {
          struct Holder
          {
            EType type;
          };
          struct Resolver
          {
            Resolver()
                : holder(::std::make_shared<Holder>())
            {
            }

            EType operator()() const
            {
              return holder->type;
            }
            ::std::shared_ptr<Holder> holder;
          };

          Resolver resolver;
          types::NamedType::Resolver R(resolver);
          auto cname = TypeName::create(constraint->name());
          auto result = types::NamedType::get(cname, R);

          mapped[constraint] = result;

          resolver.holder->type = toType(constraint->resolve());

          type = result;
        }

        void visitInputType(const ::std::shared_ptr<const ConstrainedInputType> &constraint)
        {
          auto elemTy = toType(constraint->element);
          type = types::InputType::get(elemTy);
        }

        void visitOutputType(const ::std::shared_ptr<const ConstrainedOutputType> &constraint)
        {
          auto elemTy = toType(constraint->element);
          type = types::OutputType::get(elemTy);
        }

        void visitProcessType(const ::std::shared_ptr<const ConstrainedProcessType> &constraint)
        {
          types::ProcessType::Ports ports;

          for (auto in : constraint->inputs) {
            ports[in.first] = toType(in.second)->self<types::InputOutputType>();
          }
          for (auto out : constraint->outputs) {
            ports[out.first] = toType(out.second)->self<types::InputOutputType>();
          }

          type = types::ProcessType::get(ports);
        }

        EType toType(ConstrainedTypePtr ctype)
        {
          type = nullptr;

          {
            auto i = mapped.find(ctype);
            if (i != mapped.end()) {
              return i->second;
            }

            // the constrained type instances are not necessarily unique
            // get a unique instance first
            auto uniquetype = uniqueConstraints.add(ctype).first;
            i = mapped.find(uniquetype);
            if (i != mapped.end()) {
              // associate the mapped type with original entry, for speed
              mapped[ctype] = i->second;
              return i->second;
            }
          }

          ctype->accept(*this);
          if (!type) {
            throw ::std::runtime_error("Failed to map type : " + ctype->toString());
          }
          auto uniqueType = uniqueTypes.add(type);

#if 0
          ::std::cerr << "Mapping type " << ctype->toString() << " with id " << ctype->name()
              << " to " << type->name() << ::std::endl;
#endif

          mapped[ctype] = uniqueType;

          return type;
        }

      public:
        types::Type::Ptr getUniqueType(types::Type::Ptr ty)
        {
          return uniqueTypes.add(ty);
        }

      private:
        mylang::vast::VAstVisitor<ENode> &nodeConverter;

        /** The well-known types */
      public:
        const ::std::shared_ptr<const types::BitType> wknBit;

        /** The well-known types */
      public:
        const ::std::shared_ptr<const types::BooleanType> wknBoolean;

        /** The well-known types */
      public:
        const ::std::shared_ptr<const types::ByteType> wknByte;

        /** The well-known types */
      public:
        const ::std::shared_ptr<const types::CharType> wknChar;

        /** The current result */
      private:
        EType type;

        /** Types we've already mapped */
      private:
        ::std::map<ConstrainedTypePtr, EType> mapped;

        /** The type set to ensure we have no duplicates */
      private:
        mylang::constraints::ConstrainedTypeSet uniqueConstraints;

        /** The type set to ensure we have no duplicates */
      private:
        types::TypeSet uniqueTypes;
      };

      struct Visitor: public mylang::vast::VAstVisitor<ENode>
      {

        /** Values */
      public:
        typedef ::std::vector<ir::Declaration::DeclarationPtr> Decls;

        Visitor()
            : converter(*this), scope(ir::Declaration::FUNCTION_SCOPE), currentProcess(nullptr)
        {
        }

        ~Visitor()
        {
        }

        ENode unexpected(GroupNodeCPtr) const
        {
          throw ::std::runtime_error("Unexpected call");
        }

        ENode visit_type(GroupNodeCPtr node) final override
        {
          return unexpected(node);
        }

        ENode visit_literal_byte(GroupNodeCPtr node) final override
        {
          auto anno = getConstAnnotation(node);
          auto value = anno->getByte();
          auto type = toType(node)->self<types::ByteType>();
          return ir::LiteralByte::create(type, value);
        }

        ENode visit_literal_bit(GroupNodeCPtr node) final override
        {
          auto anno = getConstAnnotation(node);
          auto type = toType(node)->self<types::BitType>();
          return ir::LiteralBit::create(type, anno->getBit());
        }

        ENode visit_literal_integer(GroupNodeCPtr node) final override
        {
          auto anno = getConstAnnotation(node);
          auto type = toType(node)->self<types::IntegerType>();
          return ir::LiteralInteger::create(type, anno->getSignedInteger());
        }

        ENode visit_literal_real(GroupNodeCPtr node) final override
        {
          auto anno = getConstAnnotation(node);
          auto type = toType(node)->self<types::RealType>();
          return ir::LiteralReal::create(type, anno->getReal());
        }

        ENode visit_literal_boolean(GroupNodeCPtr node) final override
        {
          auto anno = getConstAnnotation(node);
          auto type = toType(node)->self<types::BooleanType>();
          return ir::LiteralBoolean::create(type, anno->value() == "true");
        }

        ENode visit_literal_string(GroupNodeCPtr node) final override
        {
          auto anno = getConstAnnotation(node);
          auto type = toType(node)->self<types::StringType>();
          return ir::LiteralString::create(type, anno->value());
        }

        ENode visit_literal_char(GroupNodeCPtr node) final override
        {
          auto anno = getConstAnnotation(node);
          auto type = toType(node)->self<types::CharType>();
          return ir::LiteralChar::create(type, anno->value());
        }

        ENode visit_literal_array(GroupNodeCPtr node) final override
        {
          auto arrTy = toType(node)->self<mylang::ethir::types::ArrayType>();
          auto exprs = toExpressions(node, VAst::Expr::EXPRESSIONS);
          return ir::OperatorExpression::create(arrTy, ir::OperatorExpression::OP_NEW, exprs);
        }

        ENode visit_literal_tuple(GroupNodeCPtr node) final override
        {
          auto tupleTy = toType(node)->self<mylang::ethir::types::TupleType>();
          ::std::vector<EVariable> exprs;
          exprs.reserve(tupleTy->types.size());
          for (auto member : VAst::getRelations(node, VAst::Relation::MEMBERS)) {
            exprs.push_back(toExpression(member, VAst::Expr::EXPRESSION));
          }
          return ir::OperatorExpression::create(tupleTy, ir::OperatorExpression::OP_NEW, exprs);
        }

        ENode visit_literal_tuple_member(GroupNodeCPtr node) final override
        {
          return toExpression(node, VAst::Expr::EXPRESSION);
        }

        ENode visit_literal_struct(GroupNodeCPtr node) final override
        {
          auto structTy = toType(node)->self<mylang::ethir::types::StructType>();
          ::std::vector<EVariable> exprs;
          exprs.resize(structTy->members.size());
          for (auto member : VAst::getRelations(node, VAst::Relation::MEMBERS)) {
            size_t i = structTy->indexOfMember(
                VAst::getToken(member, VAst::Token::IDENTIFIER)->text);
            exprs[i] = toExpression(member, VAst::Expr::EXPRESSION);
          }

          return ir::OperatorExpression::create(structTy, ir::OperatorExpression::OP_NEW, exprs);
        }

        ENode visit_literal_struct_member(GroupNodeCPtr node) final override
        {
          return unexpected(node);
        }

        ENode visit_literal_nil(GroupNodeCPtr) final override
        {
          if (nullptr == expectedTy) {
            throw ::std::runtime_error("conversion of nil literal requires an expected type");
          }
          ::std::vector<EVariable> empty;
          auto optTy = expectedTy->self<mylang::ethir::types::OptType>();
          return ir::OperatorExpression::create(optTy, ir::OperatorExpression::OP_NEW, empty);
        }

        ENode visit_literal_empty_array(GroupNodeCPtr) final override
        {
          if (nullptr == expectedTy) {
            throw ::std::runtime_error(
                "conversion of empty array literal requires an expected type");
          }
          ::std::vector<EVariable> empty;
          auto arrTy = expectedTy->self<mylang::ethir::types::ArrayType>();
          return ir::OperatorExpression::create(arrTy, ir::OperatorExpression::OP_NEW, empty);
        }

        ENode visit_void_expression(GroupNodeCPtr) final override
        {
          // void expressions are only used in the false branch of a conditional
          return nullptr;
        }

        ENode visit_named_expression(GroupNodeCPtr node) final override
        {
          return unexpected(node);
        }

        ENode visit_guard_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);

          builder().comment("Guard expression ");
          auto cond = toExpression(node, VAst::Expr::CONDITION);
          auto not_cond = mkTmp(
              ir::OperatorExpression::create(cond->type, ir::OperatorExpression::OP_NOT, cond));
          push();
          auto fail = toExpression(node, VAst::Expr::MESSAGE);
          builder().appendAbort(fail);
          auto failed = pop();
          builder().appendIf(not_cond, failed, ir::NoStatement::create(nullptr));
          return toExpression(node, VAst::Expr::EXPRESSION);
        }

        ENode visit_get_bit_from_bits_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_FROM_BITS, e);
        }

        ENode visit_get_boolean_from_bits_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_FROM_BITS, e);
        }

        ENode visit_get_byte_from_bits_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_FROM_BITS, e);
        }

        ENode visit_get_char_from_bits_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_FROM_BITS, e);
        }

        ENode visit_get_integer_from_bits_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_FROM_BITS, e);
        }

        ENode visit_get_real_from_bits_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_FROM_BITS, e);
        }

        ENode visit_get_bits_from_bit_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_TO_BITS, e);
        }

        ENode visit_get_bits_from_boolean_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_TO_BITS, e);
        }

        ENode visit_get_bits_from_byte_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_TO_BITS, e);
        }

        ENode visit_get_bits_from_char_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_TO_BITS, e);
        }

        ENode visit_get_bits_from_integer_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_TO_BITS, e);
        }

        ENode visit_get_bits_from_real_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_TO_BITS, e);
        }

        ENode visit_get_input_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto port = toExpression(node, VAst::Expr::PORT);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_GET, port);
        }

        ENode visit_get_output_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto port = toExpression(node, VAst::Expr::PORT);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_GET, port);
        }

        ENode visit_variable_reference_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto name = getNodeName(node);
          auto v = vast_variables.find(name.source());
          if (v == vast_variables.end()) {
            throw ::std::runtime_error(
                "Internal error: variable not found " + name.source()->uniqueFullName("."));
          }

          ir::Declaration::Scope vscope = ir::Declaration::FUNCTION_SCOPE;
          switch (v->second->scope) {
          case mylang::variables::Variable::SCOPE_GLOBAL:
            vscope = ir::Declaration::GLOBAL_SCOPE;
            break;
          case mylang::variables::Variable::SCOPE_PROCESS:
            vscope = ir::Declaration::PROCESS_SCOPE;
            break;
          case mylang::variables::Variable::SCOPE_LOCAL:
            vscope = ir::Declaration::FUNCTION_SCOPE;
            break;
          }

          return ir::Variable::create(vscope, t, name);
        }

        ENode visit_new_process_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto args = toExpressions(node, VAst::Expr::ARGUMENTS);
          auto pname = getNodeName(node);
          // FIXME: we don't have a ctor name yet
          auto ctorname = pname;

          return ir::NewProcess::create(t, pname, ctorname, args);
        }

        ENode visit_new_bit_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto args = toExpressions(node, VAst::Expr::ARGUMENTS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_NEW, args);
        }

        ENode visit_new_boolean_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto args = toExpressions(node, VAst::Expr::ARGUMENTS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_NEW, args);
        }

        ENode visit_new_byte_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto args = toExpressions(node, VAst::Expr::ARGUMENTS);
          if (args.size() == 1) {
            auto bits = types::ArrayType::get(types::BitType::getBit(), 8, 8);
            if (bits->isSameType(*args.at(0)->type)) {
              return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_FROM_BITS, args);
            }
          }
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_NEW, args);
        }

        ENode visit_new_char_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto args = toExpressions(node, VAst::Expr::ARGUMENTS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_NEW, args);
        }

        ENode visit_new_integer_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto args = toExpressions(node, VAst::Expr::ARGUMENTS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_NEW, args);
        }

        ENode visit_new_real_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto args = toExpressions(node, VAst::Expr::ARGUMENTS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_NEW, args);
        }

        ENode visit_new_string_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto args = toExpressions(node, VAst::Expr::ARGUMENTS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_NEW, args);
        }

        ENode visit_new_function_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto args = toExpressions(node, VAst::Expr::ARGUMENTS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_NEW, args);
        }

        ENode visit_new_array_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto args = toExpressions(node, VAst::Expr::ARGUMENTS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_NEW, args);
        }

        ENode visit_new_mutable_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto args = toExpressions(node, VAst::Expr::ARGUMENTS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_NEW, args);
        }

        ENode visit_new_optional_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto args = toExpressions(node, VAst::Expr::ARGUMENTS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_NEW, args);
        }

        ENode visit_new_struct_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto args = toExpressions(node, VAst::Expr::ARGUMENTS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_NEW, args);
        }

        ENode visit_new_tuple_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto args = toExpressions(node, VAst::Expr::ARGUMENTS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_NEW, args);
        }

        ENode visit_new_union_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node)->self<types::UnionType>();

          auto discriminant = visit(VAst::getRelation(node, VAst::Relation::DISCRIMINANT));
          auto literal = discriminant->self<ir::Literal>();
          assert(literal && "discriminant is not a literal");

          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::NewUnion::create(t, t->getMember(literal), e);
        }

        ENode visit_new_namedtype_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto args = toExpressions(node, VAst::Expr::ARGUMENTS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_NEW, args);
        }

        ENode visit_integer_wrap_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_WRAP, e);
        }

        ENode visit_integer_clamp_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_CLAMP, e);
        }

        ENode visit_integer_negate_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_NEG, e);
        }

        ENode visit_integer_add_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_ADD, lhs, rhs);
        }

        ENode visit_integer_subtract_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_SUB, lhs, rhs);
        }

        ENode visit_integer_multiply_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_MUL, lhs, rhs);
        }

        ENode visit_integer_divide_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_DIV, lhs, rhs);
        }

        ENode visit_integer_modulus_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_MOD, lhs, rhs);
        }

        ENode visit_integer_remainder_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_REM, lhs, rhs);
        }

        ENode visit_real_negate_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_NEG, e);
        }

        ENode visit_real_add_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_ADD, lhs, rhs);
        }

        ENode visit_real_subtract_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_SUB, lhs, rhs);
        }

        ENode visit_real_multiply_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_MUL, lhs, rhs);
        }

        ENode visit_real_divide_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_DIV, lhs, rhs);
        }

        ENode visit_real_remainder_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_REM, lhs, rhs);
        }

        ENode visit_loop_expression(GroupNodeCPtr node) final override
        {
          builder().comment("Loop expression ");
          auto loopName = Name::create("loop");
          auto decl = VAst::getRelation(node, VAst::Relation::DECL);
          auto name = getNodeName(decl);
          auto init = toExpression(decl, VAst::Expr::INITIALIZER);

          auto var = ir::Variable::create(ir::Declaration::FUNCTION_SCOPE, init->type, name);
          builder().declareVariable(var, init);

          /* build the loop body */
          push();
          auto cond = toExpression(node, VAst::Expr::CONDITION);

          push();
          auto loopExpr = toExpression(node, VAst::Expr::EXPRESSION);
          builder().appendUpdate(var, loopExpr);
          auto loopBody = pop();
          auto breakout = ir::BreakStatement::create(loopName);
          builder().appendIf(cond, loopBody, breakout);
          auto body = pop();

          builder().appendLoop(loopName, body);
          return var;
        }

        ENode visit_with_expression(GroupNodeCPtr node) final override
        {
          for (auto decl : VAst::getRelations(node, VAst::Relation::DECLS)) {
            builder().comment("WITH " + getNameAnnotation(decl)->uniqueLocalName());
            appendStatement(decl);
          }
          return toExpression(node, VAst::Expr::EXPRESSION);
        }

        ENode visit_and_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_AND, lhs, rhs);
        }

        ENode visit_or_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_OR, lhs, rhs);
        }

        ENode visit_xor_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_XOR, lhs, rhs);
        }

        ENode visit_conditional_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);

          auto res = declareTmpVal(t);
          auto cond = toExpression(node, VAst::Expr::CONDITION);
          builder().comment("Conditional expression ");

          push();
          auto lhs = toExpression(node, VAst::Expr::IFTRUE);
          if (lhs) {
            builder().appendUpdate(res, lhs);
          }
          auto left = pop();

          push();
          auto rhs = toExpression(node, VAst::Expr::IFFALSE);
          if (rhs) {
            builder().appendUpdate(res, rhs);
          }
          auto right = pop();

          builder().appendIf(cond, left, right);
          return res;
        }

        ENode visit_short_circuit_and_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);

          builder().comment("short-circuit AND");
          auto res = declareTmpVal(t);
          auto lhs = toExpression(node, VAst::Expr::LHS);

          push();
          builder().appendUpdate(res, lhs);
          auto left = pop();

          push();
          auto rhs = toExpression(node, VAst::Expr::RHS);
          builder().appendUpdate(res, rhs);
          auto right = pop();

          builder().appendIf(lhs, right, left);
          return res;
        }

        ENode visit_short_circuit_or_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          builder().comment("short-circuit OR");

          auto res = declareTmpVal(t);
          auto lhs = toExpression(node, VAst::Expr::LHS);

          push();
          builder().appendUpdate(res, lhs);
          auto left = pop();

          push();
          auto rhs = toExpression(node, VAst::Expr::RHS);
          builder().appendUpdate(res, rhs);
          auto right = pop();

          builder().appendIf(lhs, left, right);
          return res;
        }

        ENode visit_not_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_NOT, e);
        }

        ENode visit_comparison_eq_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_EQ, lhs, rhs);
        }

        ENode visit_comparison_neq_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_NEQ, lhs, rhs);
        }

        ENode visit_comparison_lt_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_LT, lhs, rhs);
        }

        ENode visit_comparison_lte_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_LTE, lhs, rhs);
        }

        ENode visit_comparison_gt_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_LT, rhs, lhs);
        }

        ENode visit_comparison_gte_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_LTE, rhs, lhs);
        }

        void define_global_value(ir::Variable::VariablePtr var, GroupNodeCPtr node)
        {
          assert(scope == ir::Declaration::GLOBAL_SCOPE);

          if (node == nullptr) {
            return;
          }

          auto oldScope = scope;
          scope = ir::Declaration::FUNCTION_SCOPE;
          push();
          auto tmp = visit(node);
          if (tmp == nullptr) {
            visit(node);
          }
          assert(tmp && "Failed to create an expression");

          auto expr = tmp->self<ir::Expression>();

          // if we have no statements, then the expr is a simple expression
          // which we can just return
          if (!hasStatements()) {
            pop(); // need to pop
            scope = oldScope;
            globals.push_back(ir::GlobalValue::create(var, expr, ir::GlobalValue::DEFAULT));
          } else {
            auto tmpExpr = mkTmp(expr);
            builder().appendReturn(tmpExpr);
            auto block = pop();
            scope = oldScope;

            auto fnTy = types::FunctionType::get(var->type, { });
            fnTy = converter.getUniqueType(fnTy)->self<types::FunctionType>();

            auto lambda = ir::LambdaExpression::create(fnTy, { }, block);
            auto lambdaVar = ir::Variable::create(ir::Declaration::GLOBAL_SCOPE, lambda->type,
                Name::create("global"));
            globals.push_back(ir::GlobalValue::create(lambdaVar, lambda, ir::GlobalValue::DEFAULT));
            expr = ir::OperatorExpression::create(var->type, ir::OperatorExpression::OP_CALL,
                lambdaVar);
            globals.push_back(ir::GlobalValue::create(var, expr, ir::GlobalValue::DEFAULT));
          }
        }

        void define_process_member(ir::Variable::VariablePtr var, bool isValue, GroupNodeCPtr node)
        {
          assert(scope == ir::Declaration::PROCESS_SCOPE);

          if (node == nullptr) {
            if (isValue) {
              currentProcess->variables.push_back(ir::ProcessValue::create(var, nullptr));
            } else {
              currentProcess->variables.push_back(ir::ProcessVariable::create(var, nullptr));
            }
            return;
          }

          auto oldScope = scope;
          scope = ir::Declaration::FUNCTION_SCOPE;
          push();
          auto tmp = visit(node);
          if (tmp == nullptr) {
            visit(node);
          }
          assert(tmp && "Failed to create an expression");

          auto expr = tmp->self<ir::Expression>();

          // if we have no statements, then the expr is a simple expression
          // which we can just return
          if (!hasStatements()) {
            pop(); // need to pop
            scope = oldScope;

            if (isValue) {
              currentProcess->variables.push_back(ir::ProcessValue::create(var, expr));
            } else {
              auto tmpExpr = mkTmp(expr);
              currentProcess->variables.push_back(ir::ProcessVariable::create(var, tmpExpr));
            }
          } else {
            auto tmpExpr = mkTmp(expr);
            builder().appendReturn(tmpExpr);
            auto block = pop();
            scope = oldScope;

            auto fnTy = types::FunctionType::get(var->type, { });
            fnTy = converter.getUniqueType(fnTy)->self<types::FunctionType>();

            auto lambda = ir::LambdaExpression::create(fnTy, { }, block);
            auto lambdaVar = ir::Variable::create(ir::Declaration::PROCESS_SCOPE, lambda->type,
                Name::create("process"));
            auto e = ir::OperatorExpression::create(var->type, ir::OperatorExpression::OP_CALL,
                lambdaVar);

            if (isValue) {
              currentProcess->variables.push_back(ir::ProcessValue::create(var, e));
              currentProcess->variables.push_back(ir::ProcessValue::create(lambdaVar, lambda));
            } else {
              currentProcess->variables.push_back(ir::ProcessValue::create(lambdaVar, lambda));
              auto tmpVar = mkTmp(e);
              currentProcess->variables.push_back(ir::ProcessVariable::create(var, tmpVar));
            }
          }
        }

        ENode visit_def_variable(GroupNodeCPtr node) final override
        {
          auto name = getNodeName(node);
          auto info = getVariableAnnotation(node);
          auto t = toType(node);

          auto var = ir::Variable::create(scope, t, name);
          if (info->kind == mylang::variables::Variable::KIND_VALUE) {
            builder().comment("begin declare value " + name.source()->fullName());
            auto initExpr = VAst::getExpr(node, VAst::Expr::INITIALIZER);
            // for a global variable, we need to create a yield
            if (scope == ir::Declaration::GLOBAL_SCOPE) {
              define_global_value(var, initExpr);
              return nullptr;
            } else if (scope == ir::Declaration::PROCESS_SCOPE) {
              define_process_member(var, true, initExpr);
              return nullptr;
            } else {
              auto init = initExpr ? visit(initExpr)->self<ir::Expression>() : nullptr;
              assert(!init || init->type->isSameType(*var->type));
              builder().declareValue(var, init);
              return nullptr;
            }
          }
          if (info->kind == mylang::variables::Variable::KIND_VARIABLE) {
            builder().comment("begin declare variable " + name.source()->fullName());
            auto initExpr = VAst::getExpr(node, VAst::Expr::INITIALIZER);
            if (scope == ir::Declaration::PROCESS_SCOPE) {
              define_process_member(var, false, initExpr);
              return nullptr;
            } else {
              auto init = toExpression(initExpr);
              builder().declareVariable(var, init);
              return nullptr;
            }
          }
          if (info->kind == mylang::variables::Variable::KIND_PARAMETER) {
            return ir::Parameter::create(name, t);
          }
          if (info->kind == mylang::variables::Variable::KIND_INPUT) {
            auto publicName = VAst::getToken(node, VAst::Token::IDENTIFIER)->text;
            currentProcess->ports.push_back(ir::InputPortDecl::create(var, publicName));
            return nullptr;
          }
          if (info->kind == mylang::variables::Variable::KIND_OUTPUT) {
            auto publicName = VAst::getToken(node, VAst::Token::IDENTIFIER)->text;
            currentProcess->ports.push_back(ir::OutputPortDecl::create(var, publicName));
            return nullptr;
          }
          throw ::std::runtime_error("Invalid port kind");
        }

        ENode visit_get_byte_as_unsigned_integer_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_BYTE_TO_UINT8, e);
        }

        ENode visit_get_byte_as_signed_integer_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_BYTE_TO_SINT8, e);
        }

        ENode visit_encode_bit_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_TO_BYTES, e);
        }

        ENode visit_encode_boolean_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_TO_BYTES, e);
        }

        ENode visit_encode_byte_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_TO_BYTES, e);
        }

        ENode visit_encode_char_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_TO_BYTES, e);
        }

        ENode visit_encode_integer_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_TO_BYTES, e);
        }

        ENode visit_encode_real_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_TO_BYTES, e);
        }

        ENode visit_encode_string_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_TO_BYTES, e);
        }

        ENode visit_decode_bit_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_FROM_BYTES, e);
        }

        ENode visit_decode_boolean_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_FROM_BYTES, e);
        }

        ENode visit_decode_byte_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_FROM_BYTES, e);
        }

        ENode visit_decode_char_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_FROM_BYTES, e);
        }

        ENode visit_decode_integer_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_FROM_BYTES, e);
        }

        ENode visit_decode_real_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_FROM_BYTES, e);
        }

        ENode visit_decode_string_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_FROM_BYTES, e);
        }

        ENode visit_process_member_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::EXPRESSION);
          auto name = VAst::getToken(node, VAst::Token::IDENTIFIER)->text;
          return ir::GetMember::create(t, expr, name);
        }

        ENode visit_union_member_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::EXPRESSION);
          auto unionTy = expr->type->self<types::UnionType>();
          auto name = VAst::getToken(node, VAst::Token::IDENTIFIER)->text;
          if (name == unionTy->discriminant.name) {
            return ir::GetDiscriminant::create(t, expr);
          }

          auto optTy = t->self<types::OptType>();
          auto member = unionTy->indexOfMember(name);

          /* we need to wrap the get member function with a check for the discriminant */
          auto discriminant = mkTmp(ir::GetDiscriminant::create(unionTy->discriminant.type, expr));
          auto literal = mkTmp(unionTy->members[member].discriminant);
          auto eq = mkTmp(
              ir::OperatorExpression::create(ir::OperatorExpression::OP_EQ, discriminant, literal));
          auto result = declareTmpVar(optTy);
          builder().appendIf(eq,
              [&](
                  ir::StatementBuilder &b) {
                    auto v = b.declareLocalValue(ir::GetMember::create(optTy->element, expr, name));
                    auto res = b.declareLocalValue(ir::OperatorExpression::create(optTy,ir::OperatorExpression::OP_NEW,v));
                    b.appendUpdate(result, res);
                  },
              [&](
                  ir::StatementBuilder &b) {
                    auto res = b.declareLocalValue(ir::OperatorExpression::create(optTy,ir::OperatorExpression::OP_NEW));
                    b.appendUpdate(result, res);
                  });
          return result;
        }

        ENode visit_struct_member_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::EXPRESSION);
          auto name = VAst::getToken(node, VAst::Token::IDENTIFIER)->text;
          return ir::GetMember::create(t, expr, name);
        }

        ENode visit_tuple_member_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::LHS);
          auto index =
              getConstAnnotation(VAst::getExpr(node, VAst::Expr::RHS))->getUnsignedInteger();

          return ir::GetMember::create(t, expr, index.unsignedValue());
        }

        ENode visit_optify_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_NEW, expr);
        }

        ENode visit_get_optional_value_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_GET, expr);
        }

        ENode visit_get_mutable_value_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_GET, expr);
        }

        ENode visit_index_string_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_INDEX, lhs, rhs);
        }

        ENode visit_index_array_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_INDEX, lhs, rhs);
        }

        ENode visit_subrange_string_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          auto begin = toExpression(node, VAst::Expr::BEGIN);
          auto end = toExpression(node, VAst::Expr::END);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_SUBRANGE, e, begin,
              end);
        }

        ENode visit_subrange_array_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          auto begin = toExpression(node, VAst::Expr::BEGIN);
          auto end = toExpression(node, VAst::Expr::END);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_SUBRANGE, e, begin,
              end);
        }

        ENode visit_call_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);

          auto fn = toExpression(node, VAst::Expr::FUNCTION);
          auto args = toExpressions(node, VAst::Expr::ARGUMENTS);

          ::std::vector<ir::Variable::VariablePtr> params;
          params.push_back(fn);
          params.insert(params.end(), args.begin(), args.end());

          auto fnTy = fn->type->self<types::FunctionType>();
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_CALL, params);
        }

        ENode visit_lambda_expression(GroupNodeCPtr node) final override
        {
          auto oldScope = scope;
          scope = ir::Declaration::FUNCTION_SCOPE;
          auto fnTy = toType(node)->self<mylang::ethir::types::FunctionType>();
          auto body = toStatement(VAst::getRelation(node, VAst::Relation::BODY));

          ir::LambdaExpression::Parameters params;
          for (auto p : VAst::getRelations(node, VAst::Relation::PARAMETERS)) {
            auto param = toNode(p)->self<ir::Parameter>();
            params.push_back(param);
          }
          scope = oldScope;
          return ir::LambdaExpression::create(fnTy, params, body);
        }

        ENode visit_concatenate_arrays_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_CONCATENATE, lhs, rhs);
        }

        ENode visit_concatenate_strings_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_CONCATENATE, lhs, rhs);
        }

        ENode visit_merge_tuples_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_MERGE_TUPLES, lhs,
              rhs);
        }

        ENode visit_zip_arrays_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto lhs = toExpression(node, VAst::Expr::LHS);
          auto rhs = toExpression(node, VAst::Expr::RHS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_ZIP, lhs, rhs);
        }

        ENode visit_transform_expression(GroupNodeCPtr) final override
        {
          throw ::std::runtime_error("Transform expression is not expected");
        }

        ENode visit_is_present_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_IS_PRESENT, expr);
        }

        ENode visit_has_new_input_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::PORT);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_IS_INPUT_NEW, expr);
        }

        ENode visit_is_input_closed_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::PORT);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_IS_INPUT_CLOSED, expr);
        }

        ENode visit_is_output_closed_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::PORT);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_IS_OUTPUT_CLOSED,
              expr);
        }

        ENode visit_is_port_readable_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::PORT);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_IS_PORT_READABLE,
              expr);
        }

        ENode visit_is_port_writable_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::PORT);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_IS_PORT_WRITABLE,
              expr);
        }

        ENode visit_array_length_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_SIZE, expr);
        }

        ENode visit_string_length_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_SIZE, expr);
        }

        ENode visit_string_to_chars_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_STRING_TO_CHARS, expr);
        }

        ENode visit_char_to_string_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_TO_STRING, expr);
        }

        ENode visit_interpolate_text_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto exprs = toExpressions(node, VAst::Expr::EXPRESSIONS);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_INTERPOLATE_TEXT,
              exprs);
        }

        ENode visit_stringify_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_TO_STRING, expr);
        }

        ENode visit_fold_array_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto arr = toExpression(node, VAst::Expr::EXPRESSION);
          auto fn = toExpression(node, VAst::Expr::FUNCTION);
          auto init = toExpression(node, VAst::Expr::INITIALIZER);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_FOLD, arr, fn, init);
        }

        ENode visit_map_array_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto arr = toExpression(node, VAst::Expr::EXPRESSION);
          auto fn = toExpression(node, VAst::Expr::FUNCTION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_MAP_ARRAY, arr, fn);
        }

        ENode visit_pad_array_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto arr = toExpression(node, VAst::Expr::EXPRESSION);
          auto sz = toExpression(node, VAst::Expr::SIZE);
          auto init = toExpression(node, VAst::Expr::INITIALIZER);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_PAD, arr, sz, init);
        }

        ENode visit_trim_array_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto arr = toExpression(node, VAst::Expr::EXPRESSION);
          auto sz = toExpression(node, VAst::Expr::SIZE);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_TRIM, arr, sz);
        }

        ENode visit_drop_array_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto arr = toExpression(node, VAst::Expr::EXPRESSION);
          auto sz = toExpression(node, VAst::Expr::SIZE);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_DROP, arr, sz);
        }

        ENode visit_take_array_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto arr = toExpression(node, VAst::Expr::EXPRESSION);
          auto sz = toExpression(node, VAst::Expr::SIZE);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_TAKE, arr, sz);
        }

        ENode visit_filter_array_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto arr = toExpression(node, VAst::Expr::EXPRESSION);
          auto fn = toExpression(node, VAst::Expr::FUNCTION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_FILTER_ARRAY, arr, fn);
        }

        ENode visit_find_array_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto arr = toExpression(node, VAst::Expr::EXPRESSION);
          auto fn = toExpression(node, VAst::Expr::FUNCTION);
          auto index = toExpression(node, VAst::Expr::INDEX);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_FIND, arr, fn, index);
        }

        ENode visit_map_optional_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto opt = toExpression(node, VAst::Expr::EXPRESSION);
          auto fn = toExpression(node, VAst::Expr::FUNCTION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_MAP_OPTIONAL, opt, fn);
        }

        ENode visit_reverse_array_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto arr = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_REVERSE, arr);
        }

        ENode visit_flatten_array_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto arr = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_FLATTEN, arr);
        }

        ENode visit_partition_array_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto arr = toExpression(node, VAst::Expr::EXPRESSION);
          auto sz = toExpression(node, VAst::Expr::SIZE);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_PARTITION, arr, sz);
        }

        ENode visit_flatten_optional_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto opt = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_FLATTEN, opt);
        }

        ENode visit_get_head_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto arr = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_HEAD, arr);
        }

        ENode visit_get_tail_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto arr = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_TAIL, arr);
        }

        ENode visit_convert_to_tuple_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_STRUCT_TO_TUPLE, expr);
        }

        ENode visit_struct_from_tuple_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto expr = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_TUPLE_TO_STRUCT, expr);
        }

        ENode visit_checked_cast_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION, t);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_CHECKED_CAST, e);
        }

        ENode visit_safe_cast_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION, t);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_CHECKED_CAST, e);
        }

        ENode visit_implicit_cast_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION, t);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_CHECKED_CAST, e);
        }

        ENode visit_implicit_basetypecast_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_BASETYPE_CAST, e);
        }

        ENode visit_unsafe_cast_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION, t);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_UNCHECKED_CAST, e);
        }

        ENode visit_get_as_roottype_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_ROOTTYPE_CAST, e);
        }

        ENode visit_get_as_basetype_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(t, ir::OperatorExpression::OP_BASETYPE_CAST, e);
        }

        ENode visit_orelse_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);
          auto res = declareTmpVal(t);
          builder().comment("or-else expression");

          // the optional object
          auto opt = toExpression(node, VAst::Expr::LHS);

          auto cond = mkTmp(
              ir::OperatorExpression::create(converter.wknBoolean,
                  ir::OperatorExpression::OP_IS_PRESENT, opt));

          push();
          auto lhs = mkTmp(ir::OperatorExpression::create(t, ir::OperatorExpression::OP_GET, opt));
          builder().appendUpdate(res, lhs);
          auto left = pop();

          push();
          auto rhs = toExpression(node, VAst::Expr::RHS);
          builder().appendUpdate(res, rhs);
          auto right = pop();

          builder().appendIf(cond, left, right);
          return res;
        }

        ENode visit_try_expression(GroupNodeCPtr node) final override
        {
          auto t = toType(node);

          builder().comment("try-expression");
          auto tmp = declareTmpVar(t);

          push();
          auto lhs = toExpression(node, VAst::Expr::LHS);
          builder().appendUpdate(tmp, lhs);
          auto left = pop();

          push();
          auto rhs = toExpression(node, VAst::Expr::RHS);
          builder().appendUpdate(tmp, rhs);
          auto right = pop();

          builder().appendTryCatch(left, right);
          auto res = mkTmp(tmp, true); // need to force it
          return res;
        }

        ENode visit_def_type(GroupNodeCPtr node) final override
        {
          auto t = toType(node)->self<types::NamedType>();
          assert(t && "def_type must be a named type");
          types.push_back(t);
          return nullptr;
        }

        ENode visit_root(GroupNodeCPtr node) final override
        {
          auto oldScope = scope;
          scope = ir::Declaration::GLOBAL_SCOPE;
          auto s = toStatement(VAst::getRelations(node, VAst::Relation::DECLS));
          scope = oldScope;

          builder().append(
              [=](
                  ir::Statement::StatementPtr) {return ir::Program::create(::std::move(globals), ::std::move(processes), ::std::move(types));});
          return nullptr;
        }

        ENode visit_def_namespace(GroupNodeCPtr node) final override
        {
          assert(scope == ir::Declaration::GLOBAL_SCOPE);
          for (auto s : VAst::getRelations(node, VAst::Relation::DECLS)) {
            appendStatement(s);
          }
          return nullptr;
        }

        ir::Declaration::DeclarationPtr def_function(GroupNodeCPtr node)
        {
          auto oldScope = scope;
          scope = ir::Declaration::FUNCTION_SCOPE;

          auto name = getNodeName(node);
          auto fnTy = toType(node)->self<mylang::ethir::types::FunctionType>();
          auto body = toStatement(VAst::getRelation(node, VAst::Relation::BODY));

          ir::LambdaExpression::Parameters params;
          for (auto p : VAst::getRelations(node, VAst::Relation::PARAMETERS)) {
            auto param = toNode(p)->self<ir::Parameter>();
            params.push_back(param);
          }

          auto lambda = ir::LambdaExpression::create(fnTy, params, body);
          scope = oldScope;
          auto var = ir::Variable::create(scope, lambda->type, name);

          if (scope == ir::Declaration::GLOBAL_SCOPE) {
            return ir::GlobalValue::create(var, lambda, ir::GlobalValue::DEFAULT);
          } else if (scope == ir::Declaration::PROCESS_SCOPE) {
            assert(lambda->type->isSameType(*var->type));
            return ir::ProcessValue::create(var, lambda);
          } else {
            assert(lambda->type->isSameType(*var->type));
            return ir::ValueDecl::create(var, lambda, nullptr);
          }
        }

        ENode visit_def_exported_function(GroupNodeCPtr node) final override
        {
          auto decl = def_function(node)->self<ir::GlobalValue>();
          globals.push_back(
              ir::GlobalValue::create(decl->variable, decl->value, ir::GlobalValue::EXPORTED));
          assert(decl && "exported functions must be global values");
          return nullptr;
        }

        ENode visit_def_function(GroupNodeCPtr node) final override
        {
          auto decl = def_function(node);
          auto global = decl->self<ir::GlobalValue>();
          if (global) {
            globals.push_back(global);
            return nullptr;
          } else if (decl->scope == ir::Declaration::Scope::PROCESS_SCOPE) {
            currentProcess->variables.push_back(decl->self<ir::ProcessValue>());
            return nullptr;
          } else {
            auto val = decl->self<ir::ValueDecl>();
            builder().declareValue(val->variable, val->value);
            return nullptr;
          }
        }

        ENode visit_def_process(GroupNodeCPtr node) final override
        {
          auto oldScope = scope;
          scope = ir::Declaration::PROCESS_SCOPE;

          ProcessMembers members;
          ProcessMembers *restore = currentProcess;
          currentProcess = &members;

          auto name = getNodeName(node);
          auto ty = toType(node)->self<types::ProcessType>();
          push();
          for (auto n : VAst::getRelations(node, VAst::Relation::STATEMENTS)) {
            appendStatement(n);
          }
          auto statements = pop();

          scope = oldScope;

          processes.push_back(
              ir::ProcessDecl::create(scope, name, ty, currentProcess->ports,
                  currentProcess->variables, currentProcess->constructors, currentProcess->blocks));
          currentProcess = restore;
          return nullptr;
        }

        ENode visit_process_block(GroupNodeCPtr node) final override
        {
          auto name = VAst::getToken(node, VAst::Token::IDENTIFIER);
          ::std::optional<::std::string> optName;
          if (name) {
            optName = name->text;
          }

          auto oldScope = scope;
          scope = ir::Declaration::FUNCTION_SCOPE;

          push();
          auto cond = toExpression(node, VAst::Expr::CONDITION);
          if (cond) {
            auto body = toStatement(VAst::getRelation(node, VAst::Relation::BODY));
            builder().appendIf(cond, body, nullptr);
          } else {
            appendStatement(VAst::getRelation(node, VAst::Relation::BODY));
          }
          auto block = pop();

          scope = oldScope;
          currentProcess->blocks.push_back(ir::ProcessBlock::create(optName, block));
          return nullptr;
        }

        ENode visit_process_constructor(GroupNodeCPtr node) final override
        {
          ir::ProcessConstructor::Parameters params;
          for (auto p : VAst::getRelations(node, VAst::Relation::PARAMETERS)) {
            auto param = toNode(p)->self<ir::Parameter>();
            params.push_back(param);
          }

          auto oldScope = scope;
          scope = ir::Declaration::FUNCTION_SCOPE;

          // there should just be 1
          auto init = VAst::getRelation(node, VAst::Relation::INIT);
          ir::OwnConstructorCall::OwnConstructorCallPtr callOwnCtorStmt;
          if (init) {
            callOwnCtorStmt = toStatement(init)->self<ir::OwnConstructorCall>();
            assert(callOwnCtorStmt && "Not an own constructor call");
          }
          auto body = toStatement(VAst::getRelation(node, VAst::Relation::BODY));
          scope = oldScope;
          currentProcess->constructors.push_back(
              ir::ProcessConstructor::create(params, callOwnCtorStmt, body));
          return nullptr;
        }

        ENode visit_function_body(GroupNodeCPtr node) final override
        {
          for (auto s : VAst::getRelations(node, VAst::Relation::STATEMENTS)) {
            appendStatement(s);
          }
          return nullptr;
        }

        ENode visit_read_port_expression(GroupNodeCPtr node) final override
        {
          auto ty = toType(node);
          auto port = toExpression(node, VAst::Expr::PORT);
          return ir::OperatorExpression::create(ty, ir::OperatorExpression::OP_READ_PORT, port);
        }

        ENode visit_write_port_expression(GroupNodeCPtr node) final override
        {
          auto port = toExpression(node, VAst::Expr::PORT);
          auto value = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(ir::OperatorExpression::OP_WRITE_PORT, port, value);
        }

        ENode visit_clear_port_expression(GroupNodeCPtr node) final override
        {
          auto port = toExpression(node, VAst::Expr::PORT);
          return ir::OperatorExpression::create(ir::OperatorExpression::OP_CLEAR_PORT, port);
        }

        ENode visit_close_port_expression(GroupNodeCPtr node) final override
        {
          auto port = toExpression(node, VAst::Expr::PORT);
          return ir::OperatorExpression::create(ir::OperatorExpression::OP_CLOSE_PORT, port);
        }

        ENode visit_wait_port_expression(GroupNodeCPtr node) final override
        {
          auto port = toExpression(node, VAst::Expr::PORT);
          return ir::OperatorExpression::create(ir::OperatorExpression::OP_WAIT_PORT, port);
        }

        ENode visit_wait_statement(GroupNodeCPtr node) final override
        {
          builder().comment("wait");
          auto ports = toExpressions(node, VAst::Expr::PORT);
          builder().declareLocalValue(
              ir::OperatorExpression::create(ir::OperatorExpression::OP_WAIT_PORT, ports));
          return nullptr;
        }

        ENode visit_block_read_events_expression(GroupNodeCPtr node) final override
        {
          auto port = toExpression(node, VAst::Expr::PORT);
          auto value = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(ir::OperatorExpression::OP_BLOCK_READ, port, value);
        }

        ENode visit_block_write_events_expression(GroupNodeCPtr node) final override
        {
          auto port = toExpression(node, VAst::Expr::PORT);
          auto value = toExpression(node, VAst::Expr::EXPRESSION);
          return ir::OperatorExpression::create(ir::OperatorExpression::OP_BLOCK_WRITE, port, value);
        }

        ENode visit_statement_list(GroupNodeCPtr node) final override
        {
          for (auto n : VAst::getRelations(node, VAst::Relation::STATEMENTS)) {
            appendStatement(n);
          }
          return nullptr;
        }

        ENode visit_statement_block(GroupNodeCPtr node) final override
        {
          for (auto n : VAst::getRelations(node, VAst::Relation::STATEMENTS)) {
            appendStatement(n);
          }
          return nullptr;
        }

        ENode visit_assert_statement(GroupNodeCPtr node) final override
        {
          builder().comment("Assertion");
          auto cond = toExpression(node, VAst::Expr::CONDITION);
          push();
          auto message = toExpression(node, VAst::Expr::MESSAGE);
          builder().appendAbort(message);
          auto assertionFailed = pop();
          builder().append([=](ir::Statement::StatementPtr n) {
            return ir::IfStatement::create(cond,n,assertionFailed,nullptr);
          });
          return nullptr;
        }

        ENode visit_call_constructor_statement(GroupNodeCPtr node) final override
        {
          push();
          auto args = toExpressions(node, VAst::Expr::ARGUMENTS);
          auto pre = pop();
          builder().appendCallOwnConstrutor(pre, args);
          return nullptr;
        }

        ENode visit_expression_statement(GroupNodeCPtr node) final override
        {
          auto e = toExpression(node, VAst::Expr::EXPRESSION);
          builder().declareLocalVariable(e);
          return nullptr;
        }

        ENode visit_foreach_statement(GroupNodeCPtr node) final override
        {
          builder().comment("for-each");
          auto decl = VAst::getRelation(node, VAst::Relation::DECL);
          auto loopVarType = toType(decl);
          auto loopVarName = getNodeName(decl);

          auto label = Name::create("loop");
          auto data = toExpression(node, VAst::Expr::EXPRESSION);
          auto var = ir::Variable::create(ir::Declaration::FUNCTION_SCOPE, loopVarType,
              loopVarName);
          loops.push_back(label);
          auto body = toStatement(node, VAst::Stmt::LOOP_BODY);
          loops.pop_back();

          builder().appendForEach(label, var, data, body);
          return nullptr;
        }

        ENode visit_if_statement(GroupNodeCPtr node) final override
        {
          builder().comment("if-statement");
          auto cond = toExpression(node, VAst::Expr::CONDITION);
          auto iftrue = toStatement(VAst::getStmt(node, VAst::Stmt::IFTRUE));
          auto iffalse = toStatement(VAst::getStmt(node, VAst::Stmt::IFFALSE));
          builder().appendIf(cond, iftrue, iffalse);
          return nullptr;
        }

        ENode visit_log_statement(GroupNodeCPtr node) final override
        {
          builder().comment("log statement");
          auto level = VAst::getToken(node, VAst::Token::LOG_LEVEL)->text;
          auto stype = toType(mylang::constraints::ConstrainedStringType::getString())->self<
              types::StringType>();
          auto btype = toType(mylang::constraints::ConstrainedBooleanType::getBoolean())->self<
              types::BooleanType>();

          auto levelExpr = mkTmp(ir::LiteralString::create(stype, level));
          auto cond = mkTmp(
              ir::OperatorExpression::create(btype, ir::OperatorExpression::OP_IS_LOGGABLE,
                  levelExpr));

          push();
          ir::OperatorExpression::Arguments exprs;
          exprs.push_back(levelExpr);
          exprs.push_back(toExpression(node, VAst::Expr::MESSAGE));
          auto args = toExpressions(node, VAst::Expr::ARGUMENTS);
          exprs.insert(exprs.end(), args.begin(), args.end());
          builder().declareLocalValue(
              ir::OperatorExpression::create(ir::OperatorExpression::OP_LOG, exprs));
          auto isLoggable = pop();

          // TODO: the guard should not be added here, but in VAST instead
          builder().appendIf(cond, isLoggable, nullptr);
          return nullptr;
        }

        ENode visit_return_statement(GroupNodeCPtr node) final override
        {
          builder().comment("return statement");
          auto expr = toExpression(node, VAst::Expr::EXPRESSION);
          builder().appendReturn(expr);
          return nullptr;
        }

        ENode visit_set_statement(GroupNodeCPtr node) final override
        {
          builder().comment("set statement");
          auto target = toExpression(node, VAst::Expr::LHS);
          auto value = toExpression(node, VAst::Expr::RHS);
          builder().declareLocalValue(
              ir::OperatorExpression::create(ir::OperatorExpression::OP_SET, target, value));
          return nullptr;
        }

        ENode visit_throw_statement(GroupNodeCPtr) final override
        {
          builder().comment("throw statement");
          builder().appendThrow(nullptr);
          return nullptr;
        }

        ENode visit_break_statement(GroupNodeCPtr) final override
        {
          builder().comment("break statement");
          builder().appendBreak(loops.back());
          return nullptr;
        }

        ENode visit_continue_statement(GroupNodeCPtr) final override
        {
          builder().comment("continue statement");
          builder().appendContinue(loops.back());
          return nullptr;
        }

        ENode visit_try_statement(GroupNodeCPtr node) final override
        {
          builder().comment("try-catch statement");
          auto tryBody = toStatement(VAst::getStmt(node, VAst::Stmt::TRY));
          auto catchBody = toStatement(VAst::getStmt(node, VAst::Stmt::CATCH));
          catchBody = ir::CommentStatement::create("catch-block", catchBody);
          builder().appendTryCatch(tryBody, catchBody);
          return nullptr;
        }

        ENode visit_update_statement(GroupNodeCPtr node) final override
        {
          auto target = toExpression(node, VAst::Expr::LHS);
          auto expr = toExpression(node, VAst::Expr::RHS);
          builder().comment("update variable " + target->name.toString());
          builder().appendUpdate(target, expr);
          return nullptr;
        }

        ENode visit_while_statement(GroupNodeCPtr node) final override
        {
          builder().comment("while loop");
          auto expr = VAst::getExpr(node, VAst::Expr::CONDITION);
          auto name = Name::create("loop");

          push();
          auto cond = toExpression(expr);
          auto breakout = ir::BreakStatement::create(name);
          loops.push_back(name);
          auto loopBody = toStatement(node, VAst::Stmt::LOOP_BODY);
          loops.pop_back();
          builder().appendIf(cond, loopBody, breakout);
          auto body = pop();
          builder().appendLoop(name, body);
          return nullptr;
        }

        ENode visit_def_generic_function(GroupNodeCPtr node) final override
        {
          return unexpected(node);
        }

        ENode visit_instantiate_generic_function(GroupNodeCPtr node) final override
        {
          return unexpected(node);
        }

        ENode visit_inline_group(GroupNodeCPtr node) final override
        {
          return unexpected(node);
        }

        EType toType(ConstrainedTypePtr ctype)
        {
          return converter.toType(ctype);
        }

        EType toType(GroupNodeCPtr node)
        {
          return toType(getConstraintAnnotation(node));
        }

        EVariable toExpression(GroupNodeCPtr node, EType expectedType = nullptr)
        {
          EVariable var;
          EType oldExpectedTy = expectedTy;
          expectedTy = expectedType;

          ENode n = toNode(node);
          if (n) {
            EExpression e = n->self<ir::Expression>();
            if (!e) {
              throw ::std::runtime_error("Could not convert node to expression");
            }
            var = mkTmp(e);
          }
          expectedTy = oldExpectedTy;
          return var;
        }

        EVariable toExpression(GroupNodeCPtr group, VAst::Expr expr, EType expectedType = nullptr)
        {
          return toExpression(VAst::getExpr(group, expr), expectedType);
        }

        ::std::vector<EVariable> toExpressions(GroupNodeCPtr group, VAst::Expr expr)
        {
          ::std::vector<EVariable> res;
          for (GroupNodeCPtr node : VAst::getExprs(group, expr)) {
            auto e = toExpression(node);
            if (e) {
              res.push_back(e);
            }
          }
          return res;
        }

        ENode toNode(mylang::GroupNodeCPtr node)
        {
          if (node) {
            return visit(node);
          } else {
            return nullptr;
          }
        }

        /**
         * Push a new context onto the stack.
         */
        void push()
        {
          stack.push_back(ir::StatementBuilder());
        }

        ir::StatementBuilder& builder()
        {
          return stack.back();
        }

        void appendStatement(const GroupNodeCPtr &node)
        {
          EStatement e;
          if (node) {
            ENode n = toNode(node);
            if (n != nullptr) {
              toNode(node);
              // conversion of a statement always returns nullptr
              throw ::std::runtime_error("Invalid statement");
            }
          }
        }

        void appendStatement(const GroupNodeCPtr &group, VAst::Stmt stmt)
        {
          auto s = VAst::getStmt(group, stmt);
          appendStatement(s);
        }

        /**
         * Get the number of statements on the stack.
         */
        bool hasStatements() const
        {
          return !stack.back().isEmpty();
        }

        /**
         * Pop the current context off the stack and return its statements
         * in a statement list.
         */
        ir::Statement::StatementPtr pop()
        {
          ir::Statement::StatementPtr res = stack.back().build();
          stack.pop_back();
          if (res == nullptr) {
            res = ir::StatementBlock::singleton(nullptr);
          }
          return res;
        }

        /**
         * Convert a node into a statement list.
         * @param node a node
         * @return a statement list
         */
        ir::Statement::StatementPtr toStatement(const GroupNodeCPtr &node)
        {
          push();
          appendStatement(node);
          return pop();
        }

        /**
         * Convert a node into a statement list.
         * @param node a node
         * @return a statement list
         */
        ir::Statement::StatementPtr toStatement(const ::std::vector<GroupNodeCPtr> &nodes)
        {
          push();
          for (auto node : nodes) {
            appendStatement(node);
          }
          return pop();
        }

        /**
         * Convert a node into a statement list.
         * @param node a node
         * @return a statement list
         */
        ir::Statement::StatementPtr toStatement(const GroupNodeCPtr &node, VAst::Stmt stmt)
        {
          push();
          appendStatement(node, stmt);
          return pop();
        }

        /**
         * Declare a temporary variable.
         * @param ty an type
         * @return the variable for the temporary
         */
        ir::Variable::VariablePtr declareTmpVal(types::Type::Ptr ty)
        {
          assert(scope == ir::Declaration::Scope::FUNCTION_SCOPE);
          auto name = Name::create("tmp");
          auto var = ir::Variable::create(scope, ty, name);
          builder().declareValue(var);
          return var;
        }

        /**
         * Declare a temporary variable.
         * @param ty an type
         * @return the variable for the temporary
         */
        ir::Variable::VariablePtr declareTmpVar(types::Type::Ptr ty)
        {
          assert(scope == ir::Declaration::Scope::FUNCTION_SCOPE);
          auto name = Name::create("tmp");
          auto var = ir::Variable::create(scope, ty, name);
          builder().declareVariable(var);
          return var;
        }

        /**
         * Create a temporary.
         * @param expr an expression
         * @return a variable to reference the temporary
         */
        ir::Variable::VariablePtr mkTmp(ir::Expression::ExpressionPtr expr, bool force = false)
        {
          auto var = expr->self<ir::Variable>();
          if (var == nullptr || force) {
            auto name = Name::create("tmpvar");
            var = ir::Variable::create(scope, expr->type, name);
            if (scope == ir::Declaration::GLOBAL_SCOPE) {
              globals.push_back(ir::GlobalValue::create(var, expr, ir::GlobalValue::DEFAULT));
            } else if (scope == ir::Declaration::PROCESS_SCOPE) {
              currentProcess->variables.push_back(ir::ProcessValue::create(var, expr));
            } else {
              assert(expr->type->isSameType(*var->type));
              builder().declareValue(var, expr);
            }
          }
          return var;
        }

        /**
         * Get or create a new name.
         * @param ptr a name ptr
         * @return a name
         */
        Name createName(mylang::names::Name::Ptr n)
        {
          auto i = name_map.find(n);
          if (i == name_map.end()) {
            Name res = Name::create(n);
            name_map.insert(::std::make_pair(n, res));
            return res;
          } else {
            return i->second;
          }
        }

        Name getNodeName(const GroupNodeCPtr &node)
        {
          return createName(getNameAnnotation(node));
        }

        /** The stack statement lists and blocks */
        ::std::vector<ir::StatementBuilder> stack;

        ir::Program::Globals globals;
        ir::Program::Processes processes;
        ir::Program::NamedTypes types;

        TypeConverter converter;

        ir::Declaration::Scope scope;

        /** The stack of loops */
        ::std::vector<Name> loops;

        /** The current process being defined */
        ProcessMembers *currentProcess;

        /** A mapping of names to Name objects */
        ::std::map<mylang::names::Name::Ptr, Name> name_map;

        /*
         * this variable is used when converting the empty array literal or the nil optional literal.
         * This is only set in the cast calls.
         */
        EType expectedTy;

        ::std::map<mylang::names::Name::Ptr, VariablePtr> vast_variables;
      };

      static GroupNodeCPtr simplify(const GroupNodeCPtr &tree)
      {
        // apply the transforms
        GroupNodeCPtr node = tree;
        size_t nPasses = 0;
        while (true) {
          auto tmp = node;
          // these transforms are necessary for the code generator to work correctly
          if (tmp == node) {
            break;
          } else {
            node = tmp;
          }
          if (++nPasses % 100 == 0) {
            ::std::cerr << "Number of passes #" << nPasses << ::std::endl;
          }
        }
        return node;
      }
    }

    EProgram vast2ethir(GroupNodeCPtr tree)
    {
      Visitor v;
      // before we do anything, we need to ensure the node is simplified
      GroupNodeCPtr node = simplify(tree);

      v.vast_variables = mylang::vast::queries::VariableFinder::find(node);

      auto s = v.toStatement(node);
      if (s == nullptr) {
        return nullptr;
      }
      auto prog = s->self<ir::Program>();
      return prog;
    }

  }
}
