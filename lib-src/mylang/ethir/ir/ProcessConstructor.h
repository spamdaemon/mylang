#ifndef CLASS_MYLANG_ETHIR_IR_PROCESSCONSTRUCTOR_H
#define CLASS_MYLANG_ETHIR_IR_PROCESSCONSTRUCTOR_H

#ifndef CLASS_MYLANG_ETHIR_IR_PARAMETER_H
#include <mylang/ethir/ir/Parameter.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENT_H
#include <mylang/ethir/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENTBLOCK_H
#include <mylang/ethir/ir/StatementBlock.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_OWNCONSTRUCTORCALL_H
#include <mylang/ethir/ir/OwnConstructorCall.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * A variable declaration. Variables are mutable!
       */
      class ProcessConstructor: public Statement
      {
        ProcessConstructor(const ProcessConstructor &e) = delete;

        ProcessConstructor& operator=(const ProcessConstructor &e) = delete;

        /** The parameters */
      public:
        typedef ::std::vector<Parameter::ParameterPtr> Parameters;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const ProcessConstructor> ProcessConstructorPtr;

        /**
         * Create a bind statement.
         * @param name a name
         * @param value the value to be bound
         */
      public:
        ProcessConstructor(Parameters parameters, OwnConstructorCall::OwnConstructorCallPtr fwdCtor,
            StatementBlock::StatementBlockPtr body);

        /** Destructor */
      public:
        ~ProcessConstructor();

        /**
         * Declare a mutable variable with the specified initializer
         * @param name a name
         * @param value the value to be bound
         */
      public:
        static ProcessConstructorPtr create(Parameters parameters,
            OwnConstructorCall::OwnConstructorCallPtr fwdCtor, Statement::StatementPtr body);

      public:
        void accept(NodeVisitor &visitor) const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The parameters */
      public:
        const Parameters parameters;

        /** A call to another constructor (optional) before processing the body */
      public:
        const OwnConstructorCall::OwnConstructorCallPtr callConstructor;

        /**
         * The body
         */
      public:
        const StatementBlock::StatementBlockPtr body;
      };
    }

  }
}
#endif
