#ifndef CLASS_MYLANG_ETHIR_IR_CommentSTATEMENT_H
#define CLASS_MYLANG_ETHIR_IR_CommentSTATEMENT_H

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENT_H
#include <mylang/ethir/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * Assign a value to a variable that is in scope.
       */
      class CommentStatement: public Statement
      {
        CommentStatement(const CommentStatement &e) = delete;

        CommentStatement& operator=(const CommentStatement &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const CommentStatement> CommentStatementPtr;

        /**
         * Crea
         * @param name a name
         * @param value the value to be bound
         */
      public:
        CommentStatement(::std::string comment, StatementPtr next);

        /** Destructor */
      public:
        ~CommentStatement();

        /**
         * Updateare a mutable variable with the specified initializer
         * @param message a message to printed
         */
      public:
        static CommentStatementPtr create(::std::string comment, StatementPtr next);

      public:
        void accept(NodeVisitor &visitor) const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        StatementPtr skipComments() const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /**
         * The bound value (may be null!)
         */
      public:
        const ::std::string comment;
      };
    }
  }
}
#endif
