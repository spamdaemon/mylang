#include <mylang/ethir/ir/BreakStatement.h>
#include <mylang/ethir/ir/NodeVisitor.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      BreakStatement::BreakStatement(Name xscope)
          : ControlFlowStatement(nullptr), scope(xscope)
      {
      }

      BreakStatement::~BreakStatement()
      {
      }

      BreakStatement::BreakStatementPtr BreakStatement::create(Name xscope)
      {
        return ::std::make_shared<BreakStatement>(xscope);
      }

      void BreakStatement::accept(NodeVisitor &visitor) const
      {
        visitor.visitBreakStatement(self<BreakStatement>());
      }
      Statement::StatementPtr BreakStatement::replaceNext(StatementPtr) const
      {
        return self<Statement>();
      }
      bool BreakStatement::isEqual(const Node &node) const
      {
        const BreakStatement &that = dynamic_cast<const BreakStatement&>(node);
        return scope == that.scope && equals(next, that.next);
      }

      void BreakStatement::computeHashCode(HashCode &h) const
      {
        h.mix(typeid(*this));
        h.mix(scope);
        h.mix(next);
      }

    }
  }
}
