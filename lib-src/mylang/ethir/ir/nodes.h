#ifndef FILE_MYLANG_ETHIR_IR_NODES_H
#define FILE_MYLANG_ETHIR_IR_NODES_H

#include <mylang/ethir/ir/Program.h>
#include <mylang/ethir/ir/GlobalValue.h>

// process related
#include <mylang/ethir/ir/ProcessVariable.h>
#include <mylang/ethir/ir/ProcessValue.h>
#include <mylang/ethir/ir/ProcessBlock.h>
#include <mylang/ethir/ir/ProcessConstructor.h>
#include <mylang/ethir/ir/ProcessDecl.h>

// expressions
#include <mylang/ethir/ir/Phi.h>
#include <mylang/ethir/ir/NoValue.h>
#include <mylang/ethir/ir/OpaqueExpression.h>
#include <mylang/ethir/ir/OperatorExpression.h>
#include <mylang/ethir/ir/GetDiscriminant.h>
#include <mylang/ethir/ir/GetMember.h>
#include <mylang/ethir/ir/LambdaExpression.h>
#include <mylang/ethir/ir/LetExpression.h>
#include <mylang/ethir/ir/LiteralBit.h>
#include <mylang/ethir/ir/LiteralBoolean.h>
#include <mylang/ethir/ir/LiteralByte.h>
#include <mylang/ethir/ir/LiteralChar.h>
#include <mylang/ethir/ir/LiteralInteger.h>
#include <mylang/ethir/ir/LiteralReal.h>
#include <mylang/ethir/ir/LiteralString.h>
#include <mylang/ethir/ir/LiteralArray.h>
#include <mylang/ethir/ir/LiteralNamedType.h>
#include <mylang/ethir/ir/LiteralOptional.h>
#include <mylang/ethir/ir/LiteralStruct.h>
#include <mylang/ethir/ir/LiteralTuple.h>
#include <mylang/ethir/ir/LiteralUnion.h>
#include <mylang/ethir/ir/NewProcess.h>
#include <mylang/ethir/ir/NewUnion.h>
#include <mylang/ethir/ir/Variable.h>

// statements
#include <mylang/ethir/ir/AbortStatement.h>
#include <mylang/ethir/ir/BreakStatement.h>
#include <mylang/ethir/ir/ContinueStatement.h>
#include <mylang/ethir/ir/CommentStatement.h>
#include <mylang/ethir/ir/ForeachStatement.h>
#include <mylang/ethir/ir/IfStatement.h>
#include <mylang/ethir/ir/InputPortDecl.h>
#include <mylang/ethir/ir/LoopStatement.h>
#include <mylang/ethir/ir/NoStatement.h>
#include <mylang/ethir/ir/OutputPortDecl.h>
#include <mylang/ethir/ir/OwnConstructorCall.h>
#include <mylang/ethir/ir/ProcessVariableUpdate.h>
#include <mylang/ethir/ir/ReturnStatement.h>
#include <mylang/ethir/ir/StatementBlock.h>
#include <mylang/ethir/ir/ThrowStatement.h>
#include <mylang/ethir/ir/TryCatch.h>
#include <mylang/ethir/ir/ValueDecl.h>
#include <mylang/ethir/ir/VariableDecl.h>
#include <mylang/ethir/ir/VariableUpdate.h>

// fusable loops
#include <mylang/ethir/ir/Loop.h>
#include <mylang/ethir/ir/YieldStatement.h>
#include <mylang/ethir/ir/SkipStatement.h>
#include <mylang/ethir/ir/IterateArrayStep.h>
#include <mylang/ethir/ir/SequenceStep.h>
#include <mylang/ethir/ir/SingleValueStep.h>
#include <mylang/ethir/ir/ToArray.h>

// other
#include <mylang/ethir/ir/Parameter.h>
#endif
