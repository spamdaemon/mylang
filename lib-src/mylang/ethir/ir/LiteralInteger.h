#ifndef CLASS_MYLANG_ETHIR_IR_LITERALINT_H
#define CLASS_MYLANG_ETHIR_IR_LITERALINT_H

#ifndef CLASS_MYLANG_ETHIR_IR_LITERALPRIMITIVE_H
#include <mylang/ethir/ir/LiteralPrimitive.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_INTEGERTYPE_H
#include <mylang/ethir/types/IntegerType.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * This class represent the constructor for a literal.
       */
      class LiteralInteger: public LiteralPrimitive
      {
        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const LiteralInteger> LiteralIntegerPtr;

        /** The value type */
      public:
        typedef BigInt ValueType;

        /**
         * Create an expression that returns a literal value.
         * @param type the type
         * @aparam value the string value for the literal
         */
      public:
        LiteralInteger(::std::shared_ptr<const types::IntegerType> type, ValueType value);

        /** Destructor */
      public:
        ~LiteralInteger();

        /**
         * Create an bit literal
         * @return a literal
         */
      public:
        static LiteralIntegerPtr create(ValueType value);

        /**
         * Create an bit literal
         * @param value true or false
         * @return a literal
         */
      public:
        static LiteralIntegerPtr create(::std::shared_ptr<const types::IntegerType> type,
            ValueType value);

      public:
        void accept(NodeVisitor &visitor) const override final;
        ::std::optional<int> compare(const Literal &that) const override final;
        LiteralPtr castTo(const EType &newType) const override;
        TypePtr getSourceType() const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The Int64 value */
      public:
        const ValueType value;
      };
    }
  }
}
#endif
