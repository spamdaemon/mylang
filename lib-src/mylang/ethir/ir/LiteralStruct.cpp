#include <mylang/ethir/ir/LiteralStruct.h>
#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/TypeMismatchError.h>
#include <mylang/ethir/TypeCastError.h>
#include <stddef.h>
#include <cassert>
#include <memory>
#include <optional>
#include <vector>

namespace mylang {
  namespace ethir {
    namespace ir {

      LiteralStruct::LiteralStruct(::std::shared_ptr<const types::StructType> xtype,
          const Members &xmembers)
          : Literal(xtype), members(xmembers)
      {
        assert(members.size() == xtype->members.size() && "Invalid literal struct");
        for (size_t i = 0; i < members.size(); ++i) {
          TypeMismatchError::check(xtype->members.at(i).type, members.at(i)->type);
        }
      }

      LiteralStruct::~LiteralStruct()
      {
      }

      LiteralStruct::LiteralStructPtr LiteralStruct::create(
          ::std::shared_ptr<const types::StructType> xtype, const Members &xmembers)
      {
        return ::std::make_shared<LiteralStruct>(xtype, xmembers);
      }

      void LiteralStruct::accept(NodeVisitor &visitor) const
      {
        visitor.visitLiteralStruct(self<LiteralStruct>());
      }
      ::std::optional<int> LiteralStruct::compare(const Literal &other) const
      {
        const LiteralStruct *that = dynamic_cast<const LiteralStruct*>(&other);
        if (that == this) {
          return true;
        }
        if (that && type->isSameType(*that->type)) {
          ::std::optional<int> res = ::std::make_optional(0);
          for (size_t i = 0; i < members.size() && res.value() == 0; ++i) {
            res = members.at(i)->compare(*that->members.at(i));
          }
          return res;
        }
        return std::nullopt;
      }
      Literal::LiteralPtr LiteralStruct::castTo(const EType &newType) const
      {
        TypeCastError::checkCast(type, newType);
        auto ty = newType->self<types::StructType>();
        Members elems;
        for (size_t i = 0; i < ty->members.size(); ++i) {
          elems.push_back(members.at(i)->castTo(ty->members.at(i).type));
        }
        return LiteralStruct::create(ty, elems);
      }
      bool LiteralStruct::isEqual(const Node &node) const
      {
        const LiteralStruct &that = dynamic_cast<const LiteralStruct&>(node);
        return equals(type, that.type)
            && equals(members.begin(), members.end(), that.members.begin(), that.members.end());
      }

      void LiteralStruct::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(members.begin(), members.end());
      }

    }
  }
}
