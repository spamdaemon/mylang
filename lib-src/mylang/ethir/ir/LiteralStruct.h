#ifndef CLASS_MYLANG_ETHIR_IR_LITERALSTRUCT_H
#define CLASS_MYLANG_ETHIR_IR_LITERALSTRUCT_H

#ifndef CLASS_MYLANG_ETHIR_IR_LITERAL_H
#include <mylang/ethir/ir/Literal.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_STRUCTTYPE_H
#include <mylang/ethir/types/StructType.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * This class represent the constructor for a literal.
       */
      class LiteralStruct: public Literal
      {
        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const LiteralStruct> LiteralStructPtr;

        /** The elements */
      public:
        typedef ::std::vector<Literal::LiteralPtr> Members;

        /**
         * Create an expression that returns a literal value.
         * @param type the type
         * @aparam value the Struct value for the literal
         */
      public:
        LiteralStruct(::std::shared_ptr<const types::StructType> type, const Members &members);

        /** Destructor */
      public:
        ~LiteralStruct();

        /**
         * Create an bit literal
         * @param value true or false
         * @return a literal
         */
      public:
        static LiteralStructPtr create(::std::shared_ptr<const types::StructType> type,
            const Members &members);

      public:
        void accept(NodeVisitor &visitor) const override final;
        ::std::optional<int> compare(const Literal &that) const override;
        LiteralPtr castTo(const EType &newType) const override;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The struct memebers */
      public:
        const Members members;
      };
    }
  }
}

#endif
