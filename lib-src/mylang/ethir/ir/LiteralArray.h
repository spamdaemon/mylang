#ifndef CLASS_MYLANG_ETHIR_IR_LITERALARRAY_H
#define CLASS_MYLANG_ETHIR_IR_LITERALARRAY_H

#ifndef CLASS_MYLANG_ETHIR_IR_LITERAL_H
#include <mylang/ethir/ir/Literal.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_ARRAYTYPE_H
#include <mylang/ethir/types/ArrayType.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * This class represent the constructor for a literal.
       */
      class LiteralArray: public Literal
      {
        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const LiteralArray> LiteralArrayPtr;

        /** The elements */
      public:
        typedef ::std::vector<Literal::LiteralPtr> Elements;

        /**
         * Create an expression that returns a literal value.
         * @param type the type
         * @aparam elements the array elements
         */
      public:
        LiteralArray(::std::shared_ptr<const types::ArrayType> type, const Elements &elements);

        /** Destructor */
      public:
        ~LiteralArray();

        /**
         * Create an bit literal
         * @aparam elements the array elements
         * @return a literal
         */
      public:
        static LiteralArrayPtr create(::std::shared_ptr<const types::ArrayType> type,
            const Elements &elements);

      public:
        void accept(NodeVisitor &visitor) const override final;
        ::std::optional<int> compare(const Literal &that) const override;
        LiteralPtr castTo(const EType &newType) const override;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The elements */
      public:
        const Elements elements;
      };
    }
  }
}

#endif
