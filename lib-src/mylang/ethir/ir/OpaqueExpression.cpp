#include <mylang/ethir/ir/OpaqueExpression.h>
#include <mylang/ethir/ir/LetExpression.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/queries/FindFreeVariables.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      namespace {
        static OpaqueExpression::Variables findFreeVariables(const EExpression &e)
        {
          OpaqueExpression::Variables res;
          auto vars = mylang::ethir::queries::FindFreeVariables::find(e);
          res.reserve(vars.size());
          for (auto &v : vars) {
            res.push_back(v.second);
          }
          return res;
        }
      }

      OpaqueExpression::OpaqueExpression(EExpression xexpr)
          : Expression(xexpr->type), arguments(findFreeVariables(xexpr)), freeVariables(arguments),
              expression(xexpr)
      {
      }

      OpaqueExpression::OpaqueExpression(const OpaqueExpression &src, Variables args)
          : Expression(src.type), arguments(::std::move(args)), freeVariables(src.freeVariables),
              expression(src.expression)
      {
        if (freeVariables.size() != arguments.size()) {
          throw ::std::runtime_error("Arguments and free variables do not match");
        }
        for (size_t i = 0; i < arguments.size(); ++i) {
          if (!equals(arguments.at(i)->type, freeVariables.at(i)->type)) {
            throw ::std::runtime_error("Types of argument and free variable do not match");
          }
        }
      }

      OpaqueExpression::~OpaqueExpression()
      {
      }

      OpaqueExpression::OpaqueExpressionPtr OpaqueExpression::create(Expression::ExpressionPtr xpr)
      {
        return ::std::make_shared<OpaqueExpression>(xpr);
      }

      OpaqueExpression::OpaqueExpressionPtr OpaqueExpression::changeArguments(Variables args) const
      {
        return ::std::make_shared<OpaqueExpression>(*this, ::std::move(args));
      }

      EExpression OpaqueExpression::restore() const
      {
        EExpression res = expression;
        for (size_t i = 0; i < arguments.size(); ++i) {
          auto lhs = freeVariables.at(i);
          auto rhs = arguments.at(i);
          if (!equals(lhs, rhs)) {
            res = LetExpression::create(freeVariables[i], arguments[i], res);
          }
        }
        return res;
      }

      void OpaqueExpression::accept(NodeVisitor &visitor) const
      {
        visitor.visitOpaqueExpression(self<OpaqueExpression>());
      }
      bool OpaqueExpression::isEqual(const Node &node) const
      {
        const OpaqueExpression &that = dynamic_cast<const OpaqueExpression&>(node);
        return equals(arguments.begin(), arguments.end(), that.arguments.begin(),
            that.arguments.end()) && equals(expression, that.expression);

      }
      void OpaqueExpression::computeHashCode(HashCode &h) const
      {
        h.mix(expression);
        h.mix(arguments.begin(), arguments.end());
      }

    }

  }
}
