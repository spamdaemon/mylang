#include <mylang/ethir/ir/DefaultNodeVisitor.h>

namespace mylang {
  namespace ethir {
    namespace ir {
      static bool DEBUG_IT = false;
      DefaultNodeVisitor::~DefaultNodeVisitor()
      {
      }

      void DefaultNodeVisitor::visitNode(const ENode &node)
      {
        if (node) {
          if (DEBUG_IT)
            ::std::cerr << "Visit node " << typeid(*node).name() << ::std::endl;
          node->accept(*this);
        }
      }

      void DefaultNodeVisitor::visitProgram(Program::ProgramPtr node)
      {
        for (auto g : node->globals) {
          visitNode(g);
        }
        for (auto p : node->processes) {
          visitNode(p);
        }
        visitNode(node->next);
      }
      void DefaultNodeVisitor::visitGlobalValue(GlobalValue::GlobalValuePtr node)
      {
        visitNode(node->value);
      }

      // expression
      void DefaultNodeVisitor::visitPhi(Phi::PhiPtr expr)
      {
        for (auto phi : expr->arguments) {
          visitNode(phi);
        }
      }

      void DefaultNodeVisitor::visitOpaqueExpression(OpaqueExpression::OpaqueExpressionPtr expr)
      {
        for (auto arg : expr->arguments) {
          visitNode(arg);
        }
      }
      void DefaultNodeVisitor::visitNoValue(NoValue::NoValuePtr)
      {

      }

      void DefaultNodeVisitor::visitToArray(ToArray::ToArrayPtr expr)
      {
        visitNode(expr->loop);
      }

      void DefaultNodeVisitor::visitOperatorExpression(
          OperatorExpression::OperatorExpressionPtr expr)
      {
        for (auto arg : expr->arguments) {
          visitNode(arg);
        }
      }

      void DefaultNodeVisitor::visitGetDiscriminant(GetDiscriminant::GetDiscriminantPtr expr)
      {
        visitNode(expr->object);
      }

      void DefaultNodeVisitor::visitGetMember(GetMember::GetMemberPtr expr)
      {
        visitNode(expr->object);
      }

      void DefaultNodeVisitor::visitLambdaExpression(LambdaExpression::LambdaExpressionPtr expr)
      {
        for (auto param : expr->parameters) {
          visitNode(param);
        }
        visitNode(expr->body);
      }

      void DefaultNodeVisitor::visitLetExpression(LetExpression::LetExpressionPtr expr)
      {
        visitNode(expr->variable);
        visitNode(expr->value);
        visitNode(expr->result);
      }

      void DefaultNodeVisitor::visitLiteralBit(LiteralBit::LiteralBitPtr)
      {
      }

      void DefaultNodeVisitor::visitLiteralBoolean(LiteralBoolean::LiteralBooleanPtr)
      {
      }

      void DefaultNodeVisitor::visitLiteralByte(LiteralByte::LiteralBytePtr)
      {
      }

      void DefaultNodeVisitor::visitLiteralChar(LiteralChar::LiteralCharPtr)
      {
      }

      void DefaultNodeVisitor::visitLiteralInteger(LiteralInteger::LiteralIntegerPtr)
      {
      }

      void DefaultNodeVisitor::visitLiteralReal(LiteralReal::LiteralRealPtr)
      {
      }

      void DefaultNodeVisitor::visitLiteralString(LiteralString::LiteralStringPtr)
      {
      }
      void DefaultNodeVisitor::visitLiteralArray(LiteralArray::LiteralArrayPtr expr)
      {
        for (auto e : expr->elements) {
          visitNode(e);
        }
      }

      void DefaultNodeVisitor::visitLiteralNamedType(LiteralNamedType::LiteralNamedTypePtr expr)
      {
        visitNode(expr->value);
      }
      void DefaultNodeVisitor::visitLiteralOptional(LiteralOptional::LiteralOptionalPtr expr)
      {
        if (expr->value) {
          visitNode(expr->value);
        }
      }
      void DefaultNodeVisitor::visitLiteralStruct(LiteralStruct::LiteralStructPtr expr)
      {
        for (auto e : expr->members) {
          visitNode(e);
        }
      }
      void DefaultNodeVisitor::visitLiteralTuple(LiteralTuple::LiteralTuplePtr expr)
      {
        for (auto e : expr->elements) {
          visitNode(e);
        }
      }
      void DefaultNodeVisitor::visitLiteralUnion(LiteralUnion::LiteralUnionPtr expr)
      {
        visitNode(expr->value);
      }

      void DefaultNodeVisitor::visitNewProcess(NewProcess::NewProcessPtr expr)
      {
        for (auto arg : expr->arguments) {
          visitNode(arg);
        }
      }

      void DefaultNodeVisitor::visitNewUnion(NewUnion::NewUnionPtr expr)
      {
        visitNode(expr->value);
      }

      void DefaultNodeVisitor::visitVariable(Variable::VariablePtr)
      {
      }

      void DefaultNodeVisitor::visitAbortStatement(AbortStatement::AbortStatementPtr stmt)
      {
        visitNode(stmt->value);
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitBreakStatement(BreakStatement::BreakStatementPtr stmt)
      {
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitContinueStatement(ContinueStatement::ContinueStatementPtr stmt)
      {
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitCommentStatement(CommentStatement::CommentStatementPtr stmt)
      {
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitForeachStatement(ForeachStatement::ForeachStatementPtr stmt)
      {
        visitNode(stmt->data);
        visitNode(stmt->variable);
        visitNode(stmt->body);
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitIfStatement(IfStatement::IfStatementPtr stmt)
      {
        visitNode(stmt->condition);
        visitNode(stmt->iftrue);
        visitNode(stmt->iffalse);
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitInputPortDecl(InputPortDecl::InputPortDeclPtr stmt)
      {
        visitNode(stmt->variable);
      }

      void DefaultNodeVisitor::visitLoopStatement(LoopStatement::LoopStatementPtr stmt)
      {
        visitNode(stmt->body);
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitNoStatement(NoStatement::NoStatementPtr stmt)
      {
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitOutputPortDecl(OutputPortDecl::OutputPortDeclPtr stmt)
      {
        visitNode(stmt->variable);
      }

      void DefaultNodeVisitor::visitOwnConstructorCall(
          OwnConstructorCall::OwnConstructorCallPtr stmt)
      {
        visitNode(stmt->pre);
        for (auto arg : stmt->arguments) {
          visitNode(arg);
        }
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitProcessBlock(ProcessBlock::ProcessBlockPtr stmt)
      {
        visitNode(stmt->body);
      }

      void DefaultNodeVisitor::visitProcessConstructor(
          ProcessConstructor::ProcessConstructorPtr stmt)
      {
        for (auto param : stmt->parameters) {
          visitNode(param);
        }
        visitNode(stmt->body);
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitProcessDecl(ProcessDecl::ProcessDeclPtr stmt)
      {
        for (auto v : stmt->publicPorts) {
          visitNode(v);
        }
        for (auto v : stmt->variables) {
          visitNode(v);
        }
        for (auto c : stmt->constructors) {
          visitNode(c);
        }
        for (auto b : stmt->blocks) {
          visitNode(b);
        }
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitProcessVariableUpdate(
          ProcessVariableUpdate::ProcessVariableUpdatePtr stmt)
      {
        visitNode(stmt->target);
        visitNode(stmt->value);
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitReturnStatement(ReturnStatement::ReturnStatementPtr stmt)
      {
        visitNode(stmt->value);
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitYieldStatement(YieldStatement::YieldStatementPtr stmt)
      {
        visitNode(stmt->value);
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitSkipStatement(SkipStatement::SkipStatementPtr stmt)
      {
        for (const auto &c : stmt->updates) {
          visitNode(c.second);
        }
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitIterateArrayStep(IterateArrayStep::IterateArrayStepPtr stmt)
      {
        for (auto &init : stmt->initializers) {
          visitNode(init.second);
        }
        visitNode(stmt->nextState);
        visitNode(stmt->array);
        visitNode(stmt->counter);
        visitNode(stmt->value);
        visitNode(stmt->body);
      }

      void DefaultNodeVisitor::visitSequenceStep(SequenceStep::SequenceStepPtr stmt)
      {
        for (auto &init : stmt->initializers) {
          visitNode(init.second);
        }
        visitNode(stmt->nextState);
        visitNode(stmt->iterationCount);
        visitNode(stmt->counter);
        visitNode(stmt->body);
      }

      void DefaultNodeVisitor::visitSingleValueStep(SingleValueStep::SingleValueStepPtr stmt)
      {
        visitNode(stmt->value);
        visitNode(stmt->body);
      }

      void DefaultNodeVisitor::visitStatementBlock(StatementBlock::StatementBlockPtr stmt)
      {
        visitNode(stmt->statements);
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitTryCatch(TryCatch::TryCatchPtr stmt)
      {
        visitNode(stmt->tryBody);
        visitNode(stmt->catchBody);
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitThrowStatement(ThrowStatement::ThrowStatementPtr stmt)
      {
        visitNode(stmt->value);
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitProcessValue(ProcessValue::ProcessValuePtr stmt)
      {
        visitNode(stmt->variable);
        visitNode(stmt->value);
      }

      void DefaultNodeVisitor::visitProcessVariable(ProcessVariable::ProcessVariablePtr stmt)
      {
        visitNode(stmt->variable);
        visitNode(stmt->value);
      }

      void DefaultNodeVisitor::visitValueDecl(ValueDecl::ValueDeclPtr stmt)
      {
        visitNode(stmt->variable);
        visitNode(stmt->value);
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitVariableDecl(VariableDecl::VariableDeclPtr stmt)
      {
        visitNode(stmt->variable);
        visitNode(stmt->value);
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitVariableUpdate(VariableUpdate::VariableUpdatePtr stmt)
      {
        visitNode(stmt->target);
        visitNode(stmt->value);
        visitNode(stmt->next);
      }

      void DefaultNodeVisitor::visitLoop(Loop::LoopPtr loop)
      {
        for (const auto &e : loop->states) {
          visitNode(e.second);
        }
      }

      void DefaultNodeVisitor::visitParameter(Parameter::ParameterPtr)
      {
      }

    }
  }
}
