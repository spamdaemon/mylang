#ifndef CLASS_MYLANG_ETHIR_IR_NODEVISITOR_H
#define CLASS_MYLANG_ETHIR_IR_NODEVISITOR_H

#ifndef FILE_MYLANG_ETHIR_IR_NODES_H
#include <mylang/ethir/ir/nodes.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {
      class NodeVisitor
      {
      public:
        virtual ~NodeVisitor() = 0;

      public:
        // global
        virtual void visitProgram(Program::ProgramPtr node) = 0;
        virtual void visitGlobalValue(GlobalValue::GlobalValuePtr node) = 0;

        // processes
        virtual void visitProcessValue(ProcessValue::ProcessValuePtr node) = 0;
        virtual void visitProcessVariable(ProcessVariable::ProcessVariablePtr node) = 0;
        virtual void visitProcessBlock(ProcessBlock::ProcessBlockPtr stmt) = 0;
        virtual void visitProcessConstructor(ProcessConstructor::ProcessConstructorPtr stmt) = 0;
        virtual void visitProcessDecl(ProcessDecl::ProcessDeclPtr stmt) = 0;

        // expression

        virtual void visitPhi(Phi::PhiPtr expr) = 0;

        virtual void visitOpaqueExpression(OpaqueExpression::OpaqueExpressionPtr expr) = 0;

        virtual void visitNoValue(NoValue::NoValuePtr expr) = 0;

        virtual void visitOperatorExpression(OperatorExpression::OperatorExpressionPtr expr) = 0;

        virtual void visitGetDiscriminant(GetDiscriminant::GetDiscriminantPtr expr) = 0;

        virtual void visitGetMember(GetMember::GetMemberPtr expr) = 0;

        virtual void visitLambdaExpression(LambdaExpression::LambdaExpressionPtr expr) = 0;

        virtual void visitLetExpression(LetExpression::LetExpressionPtr expr) = 0;

        virtual void visitLiteralBit(LiteralBit::LiteralBitPtr expr) = 0;

        virtual void visitLiteralBoolean(LiteralBoolean::LiteralBooleanPtr expr) = 0;

        virtual void visitLiteralByte(LiteralByte::LiteralBytePtr expr) = 0;

        virtual void visitLiteralChar(LiteralChar::LiteralCharPtr expr) = 0;

        virtual void visitLiteralInteger(LiteralInteger::LiteralIntegerPtr expr) = 0;

        virtual void visitLiteralReal(LiteralReal::LiteralRealPtr expr) = 0;

        virtual void visitLiteralString(LiteralString::LiteralStringPtr expr) = 0;

        virtual void visitLiteralArray(LiteralArray::LiteralArrayPtr expr) = 0;
        virtual void visitLiteralNamedType(LiteralNamedType::LiteralNamedTypePtr expr) = 0;
        virtual void visitLiteralOptional(LiteralOptional::LiteralOptionalPtr expr) = 0;
        virtual void visitLiteralStruct(LiteralStruct::LiteralStructPtr expr) = 0;
        virtual void visitLiteralTuple(LiteralTuple::LiteralTuplePtr expr) = 0;
        virtual void visitLiteralUnion(LiteralUnion::LiteralUnionPtr expr) = 0;

        virtual void visitNewProcess(NewProcess::NewProcessPtr expr) = 0;

        virtual void visitNewUnion(NewUnion::NewUnionPtr expr) = 0;

        virtual void visitVariable(Variable::VariablePtr expr) = 0;

        // statements

        virtual void visitAbortStatement(AbortStatement::AbortStatementPtr stmt) = 0;

        virtual void visitBreakStatement(BreakStatement::BreakStatementPtr stmt) = 0;

        virtual void visitContinueStatement(ContinueStatement::ContinueStatementPtr stmt) = 0;

        virtual void visitCommentStatement(CommentStatement::CommentStatementPtr stmt) = 0;

        virtual void visitForeachStatement(ForeachStatement::ForeachStatementPtr stmt) = 0;

        virtual void visitIfStatement(IfStatement::IfStatementPtr stmt) = 0;

        virtual void visitInputPortDecl(InputPortDecl::InputPortDeclPtr stmt) = 0;

        virtual void visitLoopStatement(LoopStatement::LoopStatementPtr stmt) = 0;

        virtual void visitNoStatement(NoStatement::NoStatementPtr stmt) = 0;

        virtual void visitOutputPortDecl(OutputPortDecl::OutputPortDeclPtr stmt) = 0;

        virtual void visitOwnConstructorCall(OwnConstructorCall::OwnConstructorCallPtr stmt) = 0;

        virtual void visitProcessVariableUpdate(
            ProcessVariableUpdate::ProcessVariableUpdatePtr stmt) = 0;

        virtual void visitReturnStatement(ReturnStatement::ReturnStatementPtr stmt) = 0;

        virtual void visitStatementBlock(StatementBlock::StatementBlockPtr stmt) = 0;

        virtual void visitTryCatch(TryCatch::TryCatchPtr stmt) = 0;

        virtual void visitThrowStatement(ThrowStatement::ThrowStatementPtr stmt) = 0;

        virtual void visitValueDecl(ValueDecl::ValueDeclPtr stmt) = 0;

        virtual void visitVariableDecl(VariableDecl::VariableDeclPtr stmt) = 0;

        virtual void visitVariableUpdate(VariableUpdate::VariableUpdatePtr stmt) = 0;

        // loop fusion
        virtual void visitYieldStatement(YieldStatement::YieldStatementPtr stmt) = 0;
        virtual void visitSkipStatement(SkipStatement::SkipStatementPtr stmt) = 0;
        virtual void visitIterateArrayStep(IterateArrayStep::IterateArrayStepPtr stmt) = 0;
        virtual void visitSequenceStep(SequenceStep::SequenceStepPtr stmt) = 0;
        virtual void visitSingleValueStep(SingleValueStep::SingleValueStepPtr stmt) = 0;
        virtual void visitLoop(Loop::LoopPtr param) = 0;
        virtual void visitToArray(ToArray::ToArrayPtr expr) = 0;

        // others
        virtual void visitParameter(Parameter::ParameterPtr param) = 0;

      };
    }
  }

}
#endif
