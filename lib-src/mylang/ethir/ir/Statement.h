#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENT_H
#define CLASS_MYLANG_ETHIR_IR_STATEMENT_H

#ifndef CLASS_MYLANG_ETHIR_IR_NODE_H
#include <mylang/ethir/ir/Node.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      class Statement: public Node
      {
        Statement(const Statement &e) = delete;

        Statement& operator=(const Statement &e) = delete;

        /** An statement pointer */
      public:
        typedef ::std::shared_ptr<const Statement> StatementPtr;

        /** A statement */
      protected:
        Statement(StatementPtr next);

        /** Destructor */
      public:
        virtual ~Statement() = 0;

        /**
         * Determine if this statement is effectively a NOP statement
         * @return true if this statement is a no-op
         */
      public:
        virtual bool isNOP() const;

        /**
         * Skip any comment statement
         * @return a non-comment statement
         */
      public:
        virtual StatementPtr skipComments() const;

        /**
         * replaceNext the specified statement to this statement.
         * @param stmt the statement to replaceNext
         * @return a new statement or the same statement
         */
      public:
        StatementPtr append(StatementPtr stmt) const;

        /**
         * Create a duplicate of this statement and replace the next statement!
         * @param stmt the next statement
         * @return a new statement
         */
      public:
        virtual StatementPtr replaceNext(StatementPtr stmt) const = 0;

        /**
         * Follow the next pointer to the last statement. If this statement is the last
         * statement, then this statement is returned.
         * @return a statement (never nullptr)
         */
      public:
        virtual StatementPtr lastStatement() const;

        /**
         * Get the first statement. Normally, this is the same as this
         * statement, but maybe different for block like structures.
         * @return a statement (never nullptr)
         */
      public:
        virtual StatementPtr firstStatement() const;

        /** The next statement */
      public:
        const StatementPtr next;
      };
    }
  }
}
#endif
