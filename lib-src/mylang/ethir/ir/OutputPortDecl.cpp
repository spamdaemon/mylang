#include <mylang/ethir/ir/OutputPortDecl.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      OutputPortDecl::OutputPortDecl(Variable::VariablePtr xval, ::std::string xpublicName)
          : PortDecl(xval, xpublicName, nullptr)
      {
        assert(xval->scope == Declaration::Scope::PROCESS_SCOPE);
      }

      OutputPortDecl::~OutputPortDecl()
      {
      }

      OutputPortDecl::OutputPortDeclPtr OutputPortDecl::create(Variable::VariablePtr xval,
          ::std::string xpublicName)
      {
        return ::std::make_shared<OutputPortDecl>(xval, xpublicName);
      }

      void OutputPortDecl::accept(NodeVisitor &visitor) const
      {
        visitor.visitOutputPortDecl(self<OutputPortDecl>());
      }
      Statement::StatementPtr OutputPortDecl::replaceNext(StatementPtr) const
      {
        return self<Statement>();
      }
      bool OutputPortDecl::isEqual(const Node &node) const
      {
        const OutputPortDecl &that = dynamic_cast<const OutputPortDecl&>(node);
        return equals(variable, that.variable) && equals(next, that.next);
      }
      void OutputPortDecl::computeHashCode(HashCode &h) const
      {
        h.mix(typeid(*this));
        h.mix(variable);
        h.mix(next);
      }
    }

  }
}
