#include <mylang/ethir/ir/ThrowStatement.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      ThrowStatement::ThrowStatement(Variable::VariablePtr xexpr)
          : ControlFlowStatement(nullptr), value(xexpr)
      {
      }

      ThrowStatement::~ThrowStatement()
      {
      }

      ThrowStatement::ThrowStatementPtr ThrowStatement::create()
      {
        return ::std::make_shared<ThrowStatement>(nullptr);
      }

      ThrowStatement::ThrowStatementPtr ThrowStatement::create(Variable::VariablePtr xvalue)
      {
        return ::std::make_shared<ThrowStatement>(xvalue);
      }

      void ThrowStatement::accept(NodeVisitor &visitor) const
      {
        visitor.visitThrowStatement(self<ThrowStatement>());
      }
      Statement::StatementPtr ThrowStatement::replaceNext(StatementPtr) const
      {
        return self<Statement>();
      }
      bool ThrowStatement::isEqual(const Node &node) const
      {
        const ThrowStatement &that = dynamic_cast<const ThrowStatement&>(node);
        return equals(value, that.value) && equals(next, that.next);
      }
      void ThrowStatement::computeHashCode(HashCode &h) const
      {
        h.mix(typeid(*this));
        h.mix(value);
        h.mix(next);
      }

    }

  }
}
