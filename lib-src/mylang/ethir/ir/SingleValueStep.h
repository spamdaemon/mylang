#ifndef CLASS_MYLANG_ETHIR_IR_SINGLEVALUESTEP_H
#define CLASS_MYLANG_ETHIR_IR_SINGLEVALUESTEP_H

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENT_H
#include <mylang/ethir/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENTBUILDER_H
#include <mylang/ethir/ir/StatementBuilder.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_SKIPSTATEMENT_H
#include <mylang/ethir/ir/SkipStatement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_LOOP_H
#include <mylang/ethir/ir/Loop.h>
#endif

#include <functional>

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * A loop step that yields a single value the skips to the next state.
       */
      class SingleValueStep: public Loop::Step
      {
        SingleValueStep(const SingleValueStep &e) = delete;

        SingleValueStep& operator=(const SingleValueStep &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const SingleValueStep> SingleValueStepPtr;

      public:
        SingleValueStep(EVariable value, EStatement body);

        /** Destructor */
      public:
        ~SingleValueStep();

        /**
         * Create a value that yields the specified value in the body of the step.
         * @param value the value to yield in the body
         * @param body the body of the iteration
         */
      public:
        static SingleValueStepPtr create(EVariable value, EStatement body);

        /**
         * Yield a single value and then skip to the next state.
         * @param value the value to be yielded
         * @param nextState the next state
         */
      public:
        static SingleValueStepPtr create(EVariable value, Loop::State nextState = Loop::STOP_STATE);

      public:
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &that) const override final;
        void accept(NodeVisitor &visitor) const override final;

        Loop::Expressions getInitializers() const override final;
        EStatement toStatement() const override final;

        /** The value to be yielded. */
      public:
        const EVariable value;

        /** The body of this stop */
      public:
        const EStatement body;
      };
    }
  }
}
#endif
