#include <mylang/ethir/ir/LiteralTuple.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/TypeMismatchError.h>
#include <mylang/ethir/TypeCastError.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      LiteralTuple::LiteralTuple(::std::shared_ptr<const types::TupleType> xtype,
          const Elements &xelements)
          : Literal(xtype), elements(xelements)
      {
        assert(elements.size() == xtype->tuple.size() && "Invalid literal tuple");
        for (size_t i = 0; i < elements.size(); ++i) {
          TypeMismatchError::check(xtype->types.at(i), elements.at(i)->type);
        }
      }

      LiteralTuple::~LiteralTuple()
      {
      }

      LiteralTuple::LiteralTuplePtr LiteralTuple::create(
          ::std::shared_ptr<const types::TupleType> xtype, const Elements &xelements)
      {
        return ::std::make_shared<LiteralTuple>(xtype, xelements);
      }

      void LiteralTuple::accept(NodeVisitor &visitor) const
      {
        visitor.visitLiteralTuple(self<LiteralTuple>());
      }
      ::std::optional<int> LiteralTuple::compare(const Literal &other) const
      {
        const LiteralTuple *that = dynamic_cast<const LiteralTuple*>(&other);
        if (that == this) {
          return true;
        }
        if (that && type->isSameType(*that->type)) {
          ::std::optional<int> res = ::std::make_optional(0);
          for (size_t i = 0; i < elements.size() && res.value() == 0; ++i) {
            res = elements.at(i)->compare(*that->elements.at(i));
          }
          return res;
        }
        return std::nullopt;
      }
      Literal::LiteralPtr LiteralTuple::castTo(const EType &newType) const
      {
        auto ty = newType->self<types::TupleType>();
        Elements elems;
        for (size_t i = 0; i < ty->types.size(); ++i) {
          elems.push_back(elements.at(i)->castTo(ty->types.at(i)));
        }
        return LiteralTuple::create(ty, elems);
      }
      bool LiteralTuple::isEqual(const Node &node) const
      {
        const LiteralTuple &that = dynamic_cast<const LiteralTuple&>(node);
        return equals(type, that.type)
            && equals(elements.begin(), elements.end(), that.elements.begin(), that.elements.end());
      }
      void LiteralTuple::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(elements.begin(), elements.end());
      }

    }
  }

}
