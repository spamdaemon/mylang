#ifndef CLASS_MYLANG_ETHIR_IR_GETMEMBER_H
#define CLASS_MYLANG_ETHIR_IR_GETMEMBER_H

#ifndef MYLANGCLASS_MYLANG_ETHIR_NAME_H
#include <mylang/ethir/Name.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#include <mylang/ethir/types/Type.h>
#endif
#include <vector>

namespace mylang {
  namespace ethir {
    namespace ir {
      class GetMember: public Expression
      {

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const GetMember> GetMemberPtr;

        /**
         * Create an expression gets the specified member variable.
         * @param type the return type
         * @param obj object on which to invoke the method name
         * @param name the name of the member to retrieve
         */
      public:
        GetMember(TypePtr t, Variable::VariablePtr obj, ::std::string name);

        /**destructor */
      public:
        ~GetMember();

        /**
         * Create a function call.
         * @param type the return type
         * @param name the name of the function
         * @param args the arguments
         * @return a function call ptr/
         */
      public:
        static GetMemberPtr create(TypePtr t, Variable::VariablePtr obj, ::std::string name);

        /**
         * Create a function call.
         * @param type the return type
         * @param name the name of the function
         * @param args the arguments
         * @return a function call ptr/
         */
      public:
        static GetMemberPtr create(TypePtr t, Variable::VariablePtr obj, size_t i);

        void accept(NodeVisitor &visitor) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The object on which to invoke the method */
      public:
        const Variable::VariablePtr object;

        /** The function name */
      public:
        const ::std::string name;
      };

    }
  }
}
#endif
