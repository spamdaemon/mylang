#include <mylang/ethir/ir/AbstractValueDeclaration.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      AbstractValueDeclaration::AbstractValueDeclaration(Variable::VariablePtr xval,
          Expression::ExpressionPtr xexpr, StatementPtr xnext)
          : Declaration(xval->scope, xval->name, xval->type, xnext), variable(xval), value(xexpr)
      {
        if (value) {
          if (!value->type->isSameType(*variable->type)) {
            ::std::cerr << "ValueType " << value->type->toString() << ::std::endl << "VariableType "
                << variable->type->toString() << ::std::endl;
            assert(value->type->isSameType(*variable->type));
          }
        } else {
          assert(xval->scope != Variable::Scope::GLOBAL_SCOPE);
        }
      }

      AbstractValueDeclaration::~AbstractValueDeclaration()
      {
      }

      bool AbstractValueDeclaration::isEqual(const Node &node) const
      {
        const AbstractValueDeclaration &that = dynamic_cast<const AbstractValueDeclaration&>(node);
        return equals(variable, that.variable) && equals(value, that.value)
            && equals(next, that.next);
      }
      void AbstractValueDeclaration::computeHashCode(HashCode &h) const
      {
        h.mix(variable);
        h.mix(value);
        h.mix(next);
      }
    }

  }

}
