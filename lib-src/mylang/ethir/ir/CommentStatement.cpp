#include <mylang/ethir/ir/CommentStatement.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      CommentStatement::CommentStatement(::std::string xcomment, StatementPtr xnext)
          : Statement(xnext), comment(xcomment)
      {
      }

      CommentStatement::~CommentStatement()
      {
      }

      CommentStatement::CommentStatementPtr CommentStatement::create(::std::string xcomment,
          StatementPtr xnext)
      {
        return ::std::make_shared<CommentStatement>(xcomment, xnext);
      }

      void CommentStatement::accept(NodeVisitor &visitor) const
      {
        visitor.visitCommentStatement(self<CommentStatement>());
      }
      Statement::StatementPtr CommentStatement::replaceNext(StatementPtr tail) const
      {
        return create(comment, tail);
      }

      Statement::StatementPtr CommentStatement::skipComments() const
      {
        if (next) {
          return next->skipComments();
        } else {
          return next;
        }
      }
      bool CommentStatement::isEqual(const Node &node) const
      {
        const CommentStatement &that = dynamic_cast<const CommentStatement&>(node);
        return comment == that.comment && equals(next, that.next);
      }

      void CommentStatement::computeHashCode(HashCode &h) const
      {
        h.mix(typeid(*this));
        h.mix(comment);
        h.mix(next);
      }
    }

  }
}
