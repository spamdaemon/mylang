#include <mylang/ethir/ir/Phi.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/types/types.h>
#include <cassert>
#include <vector>
#include <algorithm>

namespace mylang {
  namespace ethir {
    namespace ir {

      namespace {
        static Phi::Arguments& sort(Phi::Arguments &args)
        {
          ::std::sort(args.begin(), args.end(),
              [&](const Phi::VariablePtr &a, const Phi::VariablePtr &b) {
                return a->name< b->name;
              });
          return args;
        }
      }

      Phi::Phi(Arguments args)
          : Expression((*args.begin())->type), arguments(::std::move(sort(args)))
      {
      }

      Phi::~Phi()
      {
      }

      EExpression Phi::create(Arguments args)
      {
        if (args.size() == 1) {
          return args.at(0);
        }
        if (args.empty()) {
          throw ::std::invalid_argument("Missing phi function arguments");
        }

        Arguments uniqueArgs;
        for (auto arg : args) {
          bool found = false;
          for (auto uarg : uniqueArgs) {
            if (arg->name == uarg->name) {
              found = true;
              break;
            }
          }
          if (!found) {
            uniqueArgs.push_back(arg);
            if (!arg->type->isSameType(*uniqueArgs.at(0)->type)) {
              throw ::std::invalid_argument("Arguments for phi function must have the same type");
            }
            if (arg->name.source() != uniqueArgs.at(0)->name.source()) {
              throw ::std::invalid_argument(
                  "Arguments for phi function must have the same base name");
            }
          }
        }
        return ::std::make_shared<Phi>(::std::move(args));
      }

      void Phi::accept(NodeVisitor &visitor) const
      {
        visitor.visitPhi(self<Phi>());
      }

      ::mylang::names::Name::Ptr Phi::rootName() const
      {
        auto var = *arguments.begin();
        return var->name.source();
      }

      bool Phi::isEqual(const Node &node) const
      {
        const Phi &that = dynamic_cast<const Phi&>(node);
        return equals(type, that.type)
            && equals(arguments.begin(), arguments.end(), that.arguments.begin(),
                that.arguments.end());
      }
      void Phi::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(arguments.begin(), arguments.end());
      }
    }
  }
}
