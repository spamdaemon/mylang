#include <mylang/ethir/ir/ContinueStatement.h>
#include <mylang/ethir/ir/NodeVisitor.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      ContinueStatement::ContinueStatement(Name xscope)
          : ControlFlowStatement(nullptr), scope(xscope)
      {
      }

      ContinueStatement::~ContinueStatement()
      {
      }

      ContinueStatement::ContinueStatementPtr ContinueStatement::create(Name xscope)
      {
        return ::std::make_shared<ContinueStatement>(xscope);
      }

      void ContinueStatement::accept(NodeVisitor &visitor) const
      {
        visitor.visitContinueStatement(self<ContinueStatement>());
      }
      Statement::StatementPtr ContinueStatement::replaceNext(StatementPtr) const
      {
        return self<Statement>();
      }
      bool ContinueStatement::isEqual(const Node &node) const
      {
        const ContinueStatement &that = dynamic_cast<const ContinueStatement&>(node);
        return scope == that.scope && equals(next, that.next);
      }

      void ContinueStatement::computeHashCode(HashCode &h) const
      {
        h.mix(typeid(*this));
        h.mix(scope);
        h.mix(next);
      }

    }
  }
}
