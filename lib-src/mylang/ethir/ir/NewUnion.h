#ifndef CLASS_MYLANG_ETHIR_IR_NEWUNION_H
#define CLASS_MYLANG_ETHIR_IR_NEWUNION_H

#ifndef MYLANGCLASS_MYLANG_ETHIR_NAME_H
#include <mylang/ethir/Name.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_LITERAL_H
#include <mylang/ethir/ir/Literal.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#include <mylang/ethir/types/Type.h>
#endif
#include <vector>

namespace mylang {
  namespace ethir {
    namespace ir {
      class NewUnion: public Expression
      {

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const NewUnion> NewUnionPtr;

        /**
         * Create an expression gets the specified member variable.
         * @param type the return type
         * @param member the member to be initialized
         * @param name the name of the member to retrieve
         */
      public:
        NewUnion(TypePtr t, ::std::string member, Variable::VariablePtr value);

        /**destructor */
      public:
        ~NewUnion();

        /**
         * Create a function call.
         * @param type the return type
         * @param member the member to be initialized
         * @param args the arguments
         * @return afunction call ptr/
         */
      public:
        static NewUnionPtr create(TypePtr t, ::std::string member, Variable::VariablePtr value);

        void accept(NodeVisitor &visitor) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The object on which to invoke the method */
      public:
        const ::std::string member;

        /** The object on which to invoke the method */
      public:
        const Variable::VariablePtr value;
      };

    }
  }
}
#endif
