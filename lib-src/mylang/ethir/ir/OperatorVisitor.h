#ifndef CLASS_MYLANG_ETHIR_IR_OPERATORVISITOR_H
#define CLASS_MYLANG_ETHIR_IR_OPERATORVISITOR_H

#ifndef CLASS_MYLANG_ETHIR_IR_OPERATOREXPRESSION_H
#include <mylang/ethir/ir/OperatorExpression.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {
      class OperatorVisitor
      {
      public:
        virtual ~OperatorVisitor() = 0;

      public:
        virtual void visit(OperatorExpression::OperatorExpressionPtr e);

      protected:
        virtual void visit_NEW(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_CALL(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_CHECKED_CAST(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_UNCHECKED_CAST(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_NEG(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_ADD(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_SUB(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_MUL(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_DIV(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_MOD(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_REM(OperatorExpression::OperatorExpressionPtr e) = 0;

        virtual void visit_INDEX(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_SUBRANGE(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_EQ(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_NEQ(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_LT(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_LTE(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_AND(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_OR(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_XOR(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_NOT(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_GET(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_IS_LOGGABLE(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_IS_PRESENT(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_IS_INPUT_NEW(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_IS_INPUT_CLOSED(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_IS_OUTPUT_CLOSED(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_IS_PORT_READABLE(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_IS_PORT_WRITABLE(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_LENGTH(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_CONCATENATE(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_MERGE_TUPLES(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_ZIP(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_BITS_TO_BYTES(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_BYTES_TO_BITS(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_FROM_BITS(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_TO_BITS(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_FROM_BYTES(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_TO_BYTES(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_CLAMP(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_WRAP(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_BYTE_TO_UINT8(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_BYTE_TO_SINT8(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_STRING_TO_CHARS(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_INTERPOLATE_TEXT(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_TO_STRING(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_FOLD(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_MAP_ARRAY(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_PAD(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_TRIM(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_TAKE(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_DROP(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_MAP_OPTIONAL(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_FILTER_ARRAY(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_FILTER_OPTIONAL(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_FIND(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_REVERSE(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_FLATTEN(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_PARTITION(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_HEAD(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_TAIL(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_STRUCT_TO_TUPLE(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_TUPLE_TO_STRUCT(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_BASETYPE_CAST(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_ROOTTYPE_CAST(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_READ_PORT(OperatorExpression::OperatorExpressionPtr e) = 0;
        virtual void visit_BLOCK_READ(OperatorExpression::OperatorExpressionPtr) = 0;
        virtual void visit_BLOCK_WRITE(OperatorExpression::OperatorExpressionPtr) = 0;
        virtual void visit_CLEAR_PORT(OperatorExpression::OperatorExpressionPtr) = 0;
        virtual void visit_CLOSE_PORT(OperatorExpression::OperatorExpressionPtr) = 0;
        virtual void visit_WAIT_PORT(OperatorExpression::OperatorExpressionPtr) = 0;
        virtual void visit_WRITE_PORT(OperatorExpression::OperatorExpressionPtr) = 0;
        virtual void visit_LOG(OperatorExpression::OperatorExpressionPtr) = 0;
        virtual void visit_SET(OperatorExpression::OperatorExpressionPtr) = 0;
        virtual void visit_APPEND(OperatorExpression::OperatorExpressionPtr) = 0;
        virtual void visit_BUILD(OperatorExpression::OperatorExpressionPtr) = 0;
      };
    }
  }

}
#endif
