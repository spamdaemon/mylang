#include <mylang/ethir/ir/IterateArrayStep.h>
#include <mylang/ethir/ir/Loop.h>
#include <mylang/ethir/ir/OperatorExpression.h>
#include <mylang/ethir/ir/ContinueStatement.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/transforms/TransformSkip.h>
#include <mylang/ethir/types/ArrayType.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {
      namespace {

        static bool isEmptyArray(const EVariable &array)
        {
          auto arrTy = array->type->self<types::ArrayType>();
          return !arrTy->indexType.has_value();
        }

        static bool isSingletonArray(const EVariable &array)
        {
          auto arrTy = array->type->self<types::ArrayType>();
          if (!arrTy->indexType.has_value()) {
            return false;
          }
          auto indexTy = arrTy->indexType.value();
          // todo; should we consider size here??
          return indexTy->range == Integer::PLUS_ONE();
        }

        static Loop::Expressions initializerExpressions(bool reverse, EVariable array,
            EVariable counter)
        {
          Loop::Expressions counterInit;
          EExpression initE;
          auto arrTy = array->type->self<types::ArrayType>();
          if (arrTy->indexType.has_value()) {
            if (reverse) {
              initE = OperatorExpression::checked_cast(counter->type,
                  OperatorExpression::create(OperatorExpression::OP_SIZE, array));
            } else {
              initE = LiteralInteger::create(arrTy->counterType, 0);
            }
            counterInit.emplace(counter, initE);
          }
          return counterInit;
        }

        static EExpression indexExpression(bool reverse, EVariable array, EVariable counter)
        {
          auto arrTy = array->type->self<types::ArrayType>();
          if (!arrTy->indexType.has_value()) {
            return nullptr;
          }
          return LetExpression::create(LiteralInteger::create(reverse ? 1 : 0),
              [&](
                  EVariable zeroOrOne) {
                    return LetExpression::create(OperatorExpression::create(OperatorExpression::OP_SUB,counter,zeroOrOne),
                        [&](EVariable index) {
                          return OperatorExpression::create(*arrTy->indexType,OperatorExpression::OP_CHECKED_CAST, index);
                        });
                  });
        }

        static EExpression valueExpression(EVariable array, EVariable index)
        {
          return LetExpression::create(index, [&](EVariable i) {
            return OperatorExpression::create(OperatorExpression::OP_INDEX,array,i);});
        }

        static EExpression terminateExpression(bool reverse, EVariable array, EVariable counter)
        {
          auto arrTy = array->type->self<types::ArrayType>();
          if (!arrTy->indexType.has_value()) {
            return nullptr;
          }

          if (reverse) {
            return LetExpression::create(LiteralInteger::create(0), [&](EVariable zero) {
              return OperatorExpression::create(OperatorExpression::OP_EQ,counter,zero);
            });
          } else {
            return LetExpression::create(
                OperatorExpression::create(OperatorExpression::OP_SIZE, array),
                [&](EVariable size) {
                  return OperatorExpression::create(OperatorExpression::OP_EQ,counter,size);
                });
          }
        }

        static Loop::Expressions updateExpressions(bool reverse, EVariable counter)
        {
          Loop::Expressions counterUpdate;
          OperatorExpression::Operator op =
              reverse ? OperatorExpression::OP_SUB : OperatorExpression::OP_ADD;

          auto updateE =
              LetExpression::create(LiteralInteger::create(1),
                  [&](
                      EVariable one) {
                        return LetExpression::create(OperatorExpression::create(op,counter,one),
                            [&](EVariable c) {
                              return OperatorExpression::create(counter->type,OperatorExpression::OP_CHECKED_CAST, c);
                            });
                      });
          counterUpdate.emplace(counter, updateE);
          return counterUpdate;
        }

      }
      IterateArrayStep::IterateArrayStep(bool xreverse, SkipStatement::SkipStatementPtr xnextState,
          EVariable xarray, EVariable xcounter, EVariable xvalue, Loop::Expressions xinitializers,
          EStatement xbody)
          : reverse(xreverse && !isSingletonArray(xarray)), nextState(xnextState), array(xarray),
              counter(xcounter), value(xvalue), initializers(::std::move(xinitializers)),
              body(xbody)
      {
        assert(nextState && array && counter && value && body);
      }

      IterateArrayStep::~IterateArrayStep()
      {
      }

      IterateArrayStep::IterateArrayStepPtr IterateArrayStep::create(bool xreverse,
          SkipStatement::SkipStatementPtr xnextState, EVariable xarray, EVariable xcounter,
          EVariable xvalue, Loop::Expressions xinitializers, EStatement xbody)
      {
        return ::std::make_shared<IterateArrayStep>(xreverse, xnextState, xarray, xcounter, xvalue,
            ::std::move(xinitializers), ::std::move(xbody));
      }

      IterateArrayStep::IterateArrayStepPtr IterateArrayStep::create(bool xreverse,
          EVariable xarray, Loop::State xnextState, Builder stepBuilder)
      {
        auto arrTy = xarray->type->self<types::ArrayType>();
        auto counter = Variable::createLocal(arrTy->counterType);
        auto value = Variable::createLocal(arrTy->element);

        if (isEmptyArray(xarray)) {
          // if we have an array with no elements, then we just go
          // to the next state
          auto skip = StatementBlock::empty();
          return create(xreverse, SkipStatement::create(xnextState), xarray, counter, value, { },
              skip);
        } else {
          StatementBuilder b;
          Loop::Expressions inits = stepBuilder(value, b);
          return create(xreverse, SkipStatement::create(xnextState), xarray, counter, value, inits,
              b.build());
        }
      }

      IterateArrayStep::IterateArrayStepPtr IterateArrayStep::create(bool xreverse,
          EVariable xarray, Builder stepBuilder)
      {
        return create(xreverse, xarray, Loop::STOP_STATE, ::std::move(stepBuilder));
      }
      IterateArrayStep::IterateArrayStepPtr IterateArrayStep::createDefault(bool xreverse,
          EVariable xarray, Loop::State xnextState)
      {
        return create(xreverse, xarray, xnextState, [&](EVariable v, StatementBuilder &b) {
          auto next = isSingletonArray(xarray) ? xnextState : Loop::CURRENT_STATE;
          auto skip = SkipStatement::create( {},next);
          b.appendYield(YieldStatement::create(v,skip));
          return Loop::Expressions {};
        });
      }

      bool IterateArrayStep::isEqual(const Node &node) const
      {
        const IterateArrayStep &that = dynamic_cast<const IterateArrayStep&>(node);
        return Node::equals(array, that.array) && Node::equals(counter, that.counter)
            && Node::equals(value, that.value) && Node::equals(nextState, that.nextState)
            && Loop::equals(initializers, that.initializers) && Node::equals(body, that.body);
      }

      void IterateArrayStep::computeHashCode(HashCode &h) const
      {
        Loop::computeHashCode(initializers, h);
        h.mix(body);
        h.mix(array);
        h.mix(counter);
        h.mix(value);
        h.mix(nextState);
      }

      void IterateArrayStep::accept(NodeVisitor &visitor) const
      {
        visitor.visitIterateArrayStep(self<IterateArrayStep>());
      }

      Loop::Expressions IterateArrayStep::getInitializers() const
      {
        Loop::Expressions res = initializerExpressions(reverse, array, counter);
        res.insert(initializers.begin(), initializers.end());
        return res;
      }

      EStatement IterateArrayStep::toStatement() const
      {
        if (isEmptyArray(array)) {
          return nextState;
        }
        StatementBuilder b;

        transforms::TransformSkip::ReplaceSkipFN replaceSkipFN(
            [&](SkipStatement::SkipStatementPtr skip) {
              SkipStatement::SkipStatementPtr res;
              if (skip->nextState!=Loop::STOP_STATE) {
                auto updates = updateExpressions(reverse, counter);
                updates.insert(skip->updates.begin(),skip->updates.end());
                res= SkipStatement::create(updates,skip->nextState);
              }
              return res;
            });

        auto done = b.declareLocalValue(terminateExpression(reverse, array, counter));
        b.appendIf(done, [&](StatementBuilder &sb) {
          sb.appendSkip(nextState);
        }, [&](StatementBuilder &sb) {
          auto index = sb.declareLocalValue(indexExpression(reverse, array, counter));
          sb.declareValue(value,valueExpression(array,index));
          auto addSkipUpdates = transforms::TransformSkip::replaceSkip(body,replaceSkipFN);
          sb.appendBlock(addSkipUpdates.actual->self<Statement>());
        });

        return b.build();
      }

    }

  }
}
