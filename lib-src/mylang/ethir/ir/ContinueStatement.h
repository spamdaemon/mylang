#ifndef CLASS_MYLANG_ETHIR_IR_CONTINUESTATEMENT_H
#define CLASS_MYLANG_ETHIR_IR_CONTINUESTATEMENT_H

#ifndef CLASS_MYLANG_ETHIR_NAME_H
#include <mylang/ethir/Name.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_CONTROLFLOWSTATEMENT_H
#include <mylang/ethir/ir/ControlFlowStatement.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * A statement to continue loop.
       */
      class ContinueStatement: public ControlFlowStatement
      {
        ContinueStatement(const ContinueStatement &e) = delete;

        ContinueStatement& operator=(const ContinueStatement &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const ContinueStatement> ContinueStatementPtr;

        /**
         * Create an continue statement.
         * @param scope the name scope.
         */
      public:
        ContinueStatement(Name scope);

        /** Destructor */
      public:
        ~ContinueStatement();

        /**
         * Create a new continue statement
         * @param scope the name of the scope to exit (e.g. a loop name)
         */
      public:
        static ContinueStatementPtr create(Name scope);

      public:
        void accept(NodeVisitor &visitor) const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The name of the scope to exit */
      public:
        const Name scope;
      };
    }
  }
}
#endif
