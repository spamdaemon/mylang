#include <mylang/ethir/ir/LiteralChar.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/TypeMismatchError.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      LiteralChar::LiteralChar(::std::shared_ptr<const types::CharType> xtype, ValueType xvalue)
          : LiteralPrimitive(xtype, xvalue), value(xvalue)
      {
      }

      LiteralChar::~LiteralChar()
      {
      }

      LiteralChar::LiteralCharPtr LiteralChar::create(
          ::std::shared_ptr<const types::CharType> xtype, ValueType xvalue)
      {
        return ::std::make_shared<LiteralChar>(xtype, xvalue);
      }

      LiteralChar::LiteralCharPtr LiteralChar::create(
          ::std::shared_ptr<const types::CharType> xtype, char xvalue)
      {
        ::std::string str;
        str += xvalue;
        return ::std::make_shared<LiteralChar>(xtype, str);
      }

      LiteralChar::LiteralCharPtr LiteralChar::create(char xvalue)
      {
        ::std::string str;
        str += xvalue;
        return ::std::make_shared<LiteralChar>(types::CharType::create(), str);
      }

      void LiteralChar::accept(NodeVisitor &visitor) const
      {
        visitor.visitLiteralChar(self<LiteralChar>());
      }
      ::std::optional<int> LiteralChar::compare(const Literal &obj) const
      {
        auto that = dynamic_cast<const LiteralChar*>(&obj);
        if (that == nullptr) {
          return ::std::nullopt;
        }
        if (value < that->value) {
          return -1;
        } else if (value > that->value) {
          return 1;
        } else {
          return 0;
        }
      }
      Literal::LiteralPtr LiteralChar::castTo(const EType &newType) const
      {
        TypeMismatchError::check(type, newType);
        return self<Literal>();
      }
      bool LiteralChar::isEqual(const Node &node) const
      {
        const LiteralChar &that = dynamic_cast<const LiteralChar&>(node);
        return value == that.value && equals(type, that.type);
      }
      void LiteralChar::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(value);
      }

    }
  }
}
