#include <mylang/ethir/ir/GlobalValue.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/TypeMismatchError.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      GlobalValue::GlobalValue(Variable::VariablePtr xval, Kind xKind,
          Expression::ExpressionPtr xexpr)
          : AbstractValueDeclaration(xval, xexpr, nullptr), kind(xKind)
      {
        assert(xval->scope == GLOBAL_SCOPE);

        if (value) {
          TypeMismatchError::check(variable->type, value->type);
        }
      }

      GlobalValue::~GlobalValue()
      {
      }

      GlobalValue::GlobalValuePtr GlobalValue::create(Variable::VariablePtr var,
          Expression::ExpressionPtr xvalue, Kind xKind)
      {
        return ::std::make_shared<GlobalValue>(var, xKind, xvalue);
      }

      GlobalValue::GlobalValuePtr GlobalValue::create(const Name &xname,
          Expression::ExpressionPtr xvalue, Kind xKind)
      {
        Variable::VariablePtr var = Variable::create(GLOBAL_SCOPE, xvalue->type, xname);
        return ::std::make_shared<GlobalValue>(var, xKind, xvalue);
      }

      void GlobalValue::accept(NodeVisitor &visitor) const
      {
        visitor.visitGlobalValue(self<GlobalValue>());
      }
      Statement::StatementPtr GlobalValue::replaceNext(StatementPtr) const
      {
        return self<Statement>();
      }
      bool GlobalValue::isEqual(const Node &node) const
      {
        const GlobalValue &that = dynamic_cast<const GlobalValue&>(node);
        return kind == that.kind && AbstractValueDeclaration::isEqual(node);
      }
      void GlobalValue::computeHashCode(HashCode &h) const
      {
        AbstractValueDeclaration::computeHashCode(h);
        h.mix(static_cast<::std::uint64_t>(kind));
      }

    }
  }
}
