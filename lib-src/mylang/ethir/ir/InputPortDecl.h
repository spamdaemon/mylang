#ifndef CLASS_MYLANG_ETHIR_IR_INPUTPORTDECL_H
#define CLASS_MYLANG_ETHIR_IR_INPUTPORTDECL_H

#ifndef CLASS_MYLANG_ETHIR_IR_PORTDECL_H
#include <mylang/ethir/ir/PortDecl.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {
      class InputPortDecl: public PortDecl
      {
        InputPortDecl(const InputPortDecl &e) = delete;

        InputPortDecl& operator=(const InputPortDecl &e) = delete;

        /** An expression pointer */
      public:
        typedef ::std::shared_ptr<const InputPortDecl> InputPortDeclPtr;

        /**
         * Create a declaration of that associates a type with a name.
         * @param name the name of the declaration
         * @param type the type
         */
      public:
        InputPortDecl(Variable::VariablePtr val, ::std::string publicName);

        /** Destructor */
      public:
        ~InputPortDecl();

        /**
         * Create a input port declaration.
         * @param name the name of the parameter
         * @param type the parameter type
         */
      public:
        static InputPortDeclPtr create(Variable::VariablePtr val, ::std::string publicName);

      public:
        void accept(NodeVisitor &visitor) const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

      };
    }
  }
}
#endif
