#ifndef CLASS_MYLANG_ETHIR_IR_ABSTRACTNODEVISITOR_H
#define CLASS_MYLANG_ETHIR_IR_ABSTRACTNODEVISITOR_H

#ifndef CLASS_MYLANG_ETHIR_IR_NODEVISITOR_H
#include <mylang/ethir/ir/NodeVisitor.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {
      class AbstractNodeVisitor: public NodeVisitor
      {
      public:
        virtual ~AbstractNodeVisitor() =0;

      public:
        // global
        void visitProgram(Program::ProgramPtr node) override;
        void visitGlobalValue(GlobalValue::GlobalValuePtr node) override;

        // expression
        void visitToArray(ToArray::ToArrayPtr expr) override;

        void visitPhi(Phi::PhiPtr expr) override;

        void visitOpaqueExpression(OpaqueExpression::OpaqueExpressionPtr expr) override;
        void visitNoValue(NoValue::NoValuePtr expr) override;

        void visitOperatorExpression(OperatorExpression::OperatorExpressionPtr expr) override;

        void visitGetDiscriminant(GetDiscriminant::GetDiscriminantPtr expr) override;

        void visitGetMember(GetMember::GetMemberPtr expr) override;

        void visitLambdaExpression(LambdaExpression::LambdaExpressionPtr expr) override;

        void visitLetExpression(LetExpression::LetExpressionPtr expr) override;

        void visitLiteralBit(LiteralBit::LiteralBitPtr expr) override;

        void visitLiteralBoolean(LiteralBoolean::LiteralBooleanPtr expr) override;

        void visitLiteralByte(LiteralByte::LiteralBytePtr expr) override;

        void visitLiteralChar(LiteralChar::LiteralCharPtr expr) override;

        void visitLiteralInteger(LiteralInteger::LiteralIntegerPtr expr) override;

        void visitLiteralReal(LiteralReal::LiteralRealPtr expr) override;

        void visitLiteralString(LiteralString::LiteralStringPtr expr) override;
        void visitLiteralArray(LiteralArray::LiteralArrayPtr expr) override;
        void visitLiteralNamedType(LiteralNamedType::LiteralNamedTypePtr expr) override;
        void visitLiteralOptional(LiteralOptional::LiteralOptionalPtr expr) override;
        void visitLiteralStruct(LiteralStruct::LiteralStructPtr expr) override;
        void visitLiteralTuple(LiteralTuple::LiteralTuplePtr expr) override;
        void visitLiteralUnion(LiteralUnion::LiteralUnionPtr expr) override;

        void visitNewProcess(NewProcess::NewProcessPtr expr) override;

        void visitNewUnion(NewUnion::NewUnionPtr expr) override;

        void visitVariable(Variable::VariablePtr expr) override;

        // statements

        void visitAbortStatement(AbortStatement::AbortStatementPtr stmt) override;

        void visitBreakStatement(BreakStatement::BreakStatementPtr stmt) override;

        void visitContinueStatement(ContinueStatement::ContinueStatementPtr stmt) override;

        void visitCommentStatement(CommentStatement::CommentStatementPtr stmt) override;

        void visitForeachStatement(ForeachStatement::ForeachStatementPtr stmt) override;

        void visitIfStatement(IfStatement::IfStatementPtr stmt) override;

        void visitInputPortDecl(InputPortDecl::InputPortDeclPtr stmt) override;

        void visitLoopStatement(LoopStatement::LoopStatementPtr stmt) override;

        void visitNoStatement(NoStatement::NoStatementPtr stmt) override;

        void visitOutputPortDecl(OutputPortDecl::OutputPortDeclPtr stmt) override;

        void visitOwnConstructorCall(OwnConstructorCall::OwnConstructorCallPtr stmt) override;

        void visitProcessBlock(ProcessBlock::ProcessBlockPtr stmt) override;

        void visitProcessConstructor(ProcessConstructor::ProcessConstructorPtr stmt) override;

        void visitProcessDecl(ProcessDecl::ProcessDeclPtr stmt) override;

        void visitProcessVariable(ProcessVariable::ProcessVariablePtr stmt) override;

        void visitProcessValue(ProcessValue::ProcessValuePtr stmt) override;

        void visitProcessVariableUpdate(ProcessVariableUpdate::ProcessVariableUpdatePtr stmt)
            override;

        void visitReturnStatement(ReturnStatement::ReturnStatementPtr stmt) override;

        void visitStatementBlock(StatementBlock::StatementBlockPtr stmt) override;

        void visitTryCatch(TryCatch::TryCatchPtr stmt) override;

        void visitThrowStatement(ThrowStatement::ThrowStatementPtr stmt) override;

        void visitValueDecl(ValueDecl::ValueDeclPtr stmt) override;

        void visitVariableDecl(VariableDecl::VariableDeclPtr stmt) override;

        void visitVariableUpdate(VariableUpdate::VariableUpdatePtr stmt) override;
        void visitYieldStatement(YieldStatement::YieldStatementPtr stmt) override;
        void visitSkipStatement(SkipStatement::SkipStatementPtr stmt) override;
        void visitIterateArrayStep(IterateArrayStep::IterateArrayStepPtr stmt) override;
        void visitSequenceStep(SequenceStep::SequenceStepPtr stmt) override;
        void visitSingleValueStep(SingleValueStep::SingleValueStepPtr stmt) override;

        // others
        void visitLoop(Loop::LoopPtr param) override;
        void visitParameter(Parameter::ParameterPtr param) override;

      };
    }
  }

}
#endif
