#include <mylang/ethir/ir/ForeachStatement.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/types/ArrayType.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      ForeachStatement::ForeachStatement(Name loopName, Variable::VariablePtr loopvar,
          Variable::VariablePtr loopdata, StatementBlock::StatementBlockPtr xbody,
          StatementPtr xnext)
          : ControlFlowStatement(xnext), name(loopName), variable(loopvar), data(loopdata),
              body(xbody)
      {
      }

      ForeachStatement::~ForeachStatement()
      {
      }

      ForeachStatement::ForeachStatementPtr ForeachStatement::create(Name loopName,
          Variable::VariablePtr loopvar, Variable::VariablePtr loopdata,
          Statement::StatementPtr xbody, StatementPtr xnext)
      {
        return ::std::make_shared<ForeachStatement>(loopName, loopvar, loopdata,
            StatementBlock::singleton(xbody), xnext);
      }

      void ForeachStatement::accept(NodeVisitor &visitor) const
      {
        visitor.visitForeachStatement(self<ForeachStatement>());
      }

      bool ForeachStatement::isNOP() const
      {
        return body->isNOP();
      }
      Statement::StatementPtr ForeachStatement::replaceNext(StatementPtr tail) const
      {
        return create(name, variable, data, body, tail);
      }
      bool ForeachStatement::isEqual(const Node &node) const
      {
        const ForeachStatement &that = dynamic_cast<const ForeachStatement&>(node);

        return name == that.name && equals(variable, that.variable) && equals(data, that.data)
            && equals(body, that.body) && equals(next, that.next);
      }

      void ForeachStatement::computeHashCode(HashCode &h) const
      {
        h.mix(name);
        h.mix(variable);
        h.mix(data);
        h.mix(body);
        h.mix(next);
      }

    }
  }
}
