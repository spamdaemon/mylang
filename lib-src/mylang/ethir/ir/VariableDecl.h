#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLEDECL_H
#define CLASS_MYLANG_ETHIR_IR_VARIABLEDECL_H

#ifndef CLASS_MYLANG_ETHIR_IR_DECLARATION_H
#include <mylang/ethir/ir/Declaration.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_EXPRESSION_H
#include <mylang/ethir/ir/Expression.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * A variable declaration. Variables are mutable!
       */
      class VariableDecl: public Declaration
      {
        VariableDecl(const VariableDecl&) = delete;

        VariableDecl& operator=(const VariableDecl&) = delete;

        /** A variable pointer */
      public:
        typedef ::std::shared_ptr<const VariableDecl> VariableDeclPtr;

        /**
         * Create a bind statement.
         * @param name a name
         * @param value the value to be bound
         */
      public:
        VariableDecl(Variable::VariablePtr var, Variable::VariablePtr value, StatementPtr next);

        /** Destructor */
      public:
        ~VariableDecl();

        /**
         * Declare a mutable variable with the specified initializer
         * @param var the variable  to hold the value
         * @param value the value to be bound
         */
      public:
        static VariableDeclPtr create(Variable::VariablePtr var, Variable::VariablePtr value,
            StatementPtr next);

        /**
         * Declare a mutable variable that is not initialized
         * @param var the variable value
         */
      public:
        static VariableDeclPtr create(Variable::VariablePtr var, StatementPtr next);

      public:
        void accept(NodeVisitor &visitor) const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /**
         * The name and type of the local variable that was introduced.
         */
      public:
        const Variable::VariablePtr variable;

        /**
         * The bound value (may be null!)
         */
      public:
        const Variable::VariablePtr value;
      };
    }

  }
}
#endif
