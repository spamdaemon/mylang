#ifndef CLASS_MYLANG_ETHIR_IR_LITERALBYTE_H
#define CLASS_MYLANG_ETHIR_IR_LITERALBYTE_H

#ifndef CLASS_MYLANG_ETHIR_IR_LITERALPRIMITIVE_H
#include <mylang/ethir/ir/LiteralPrimitive.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_BYTETYPE_H
#include <mylang/ethir/types/ByteType.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * This class represent the constructor for a literal.
       */
      class LiteralByte: public LiteralPrimitive
      {
        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const LiteralByte> LiteralBytePtr;

        /** The value type */
      public:
        typedef unsigned char ValueType;

        /**
         * Create an expression that returns a literal value.
         * @param type the type
         * @aparam value the string value for the literal
         */
      public:
        LiteralByte(::std::shared_ptr<const types::ByteType> type, ValueType value);

        /** Destructor */
      public:
        ~LiteralByte();

        /**
         * Create an bit literal
         * @param value true or false
         * @return a literal
         */
      public:
        static LiteralBytePtr create(ValueType value);

        /**
         * Create an bit literal
         * @param value true or false
         * @return a literal
         */
      public:
        static LiteralBytePtr create(::std::shared_ptr<const types::ByteType> type,
            ValueType value);

      public:
        void accept(NodeVisitor &visitor) const override final;
        ::std::optional<int> compare(const Literal &that) const override final;
        LiteralPtr castTo(const EType &newType) const override;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The bit value */
      public:
        const ValueType value;
      };
    }
  }
}
#endif
