#include <mylang/ethir/ir/LoopStatement.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      LoopStatement::LoopStatement(Name xname, StatementBlock::StatementBlockPtr xbody,
          StatementPtr xnext)
          : ControlFlowStatement(xnext), name(xname), body(xbody)
      {
      }

      LoopStatement::~LoopStatement()
      {
      }

      LoopStatement::LoopStatementPtr LoopStatement::create(Name xname,
          Statement::StatementPtr xbody, StatementPtr xnext)
      {
        return ::std::make_shared<LoopStatement>(xname, StatementBlock::singleton(xbody), xnext);
      }

      void LoopStatement::accept(NodeVisitor &visitor) const
      {
        visitor.visitLoopStatement(self<LoopStatement>());
      }
      bool LoopStatement::isNOP() const
      {
        return body->isNOP();
      }
      Statement::StatementPtr LoopStatement::replaceNext(StatementPtr tail) const
      {
        return create(name, body, tail);
      }
      bool LoopStatement::isEqual(const Node &node) const
      {
        const LoopStatement &that = dynamic_cast<const LoopStatement&>(node);
        return name == that.name && equals(body, that.body) && equals(next, that.next);
      }

      void LoopStatement::computeHashCode(HashCode &h) const
      {
        h.mix(name);
        h.mix(body);
        h.mix(next);
      }
    }

  }
}
