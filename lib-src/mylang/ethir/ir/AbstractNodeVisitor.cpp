#include <mylang/ethir/ir/AbstractNodeVisitor.h>

namespace mylang {
  namespace ethir {
    namespace ir {
      AbstractNodeVisitor::~AbstractNodeVisitor()
      {
      }

      void AbstractNodeVisitor::visitProgram(Program::ProgramPtr)
      {
      }
      void AbstractNodeVisitor::visitGlobalValue(GlobalValue::GlobalValuePtr)
      {
      }

      // expression
      void AbstractNodeVisitor::visitToArray(ToArray::ToArrayPtr)
      {
      }

      void AbstractNodeVisitor::visitPhi(Phi::PhiPtr)
      {
      }

      void AbstractNodeVisitor::visitOpaqueExpression(OpaqueExpression::OpaqueExpressionPtr)
      {

      }

      void AbstractNodeVisitor::visitNoValue(NoValue::NoValuePtr)
      {

      }

      void AbstractNodeVisitor::visitOperatorExpression(OperatorExpression::OperatorExpressionPtr)
      {
      }

      void AbstractNodeVisitor::visitGetDiscriminant(GetDiscriminant::GetDiscriminantPtr)
      {
      }

      void AbstractNodeVisitor::visitGetMember(GetMember::GetMemberPtr)
      {
      }

      void AbstractNodeVisitor::visitLambdaExpression(LambdaExpression::LambdaExpressionPtr)
      {
      }

      void AbstractNodeVisitor::visitLetExpression(LetExpression::LetExpressionPtr)
      {
      }

      void AbstractNodeVisitor::visitLiteralBit(LiteralBit::LiteralBitPtr)
      {
      }

      void AbstractNodeVisitor::visitLiteralBoolean(LiteralBoolean::LiteralBooleanPtr)
      {
      }

      void AbstractNodeVisitor::visitLiteralByte(LiteralByte::LiteralBytePtr)
      {
      }

      void AbstractNodeVisitor::visitLiteralChar(LiteralChar::LiteralCharPtr)
      {
      }

      void AbstractNodeVisitor::visitLiteralInteger(LiteralInteger::LiteralIntegerPtr)
      {
      }

      void AbstractNodeVisitor::visitLiteralReal(LiteralReal::LiteralRealPtr)
      {
      }

      void AbstractNodeVisitor::visitLiteralString(LiteralString::LiteralStringPtr)
      {
      }
      void AbstractNodeVisitor::visitLiteralArray(LiteralArray::LiteralArrayPtr)
      {
      }

      void AbstractNodeVisitor::visitLiteralNamedType(LiteralNamedType::LiteralNamedTypePtr)
      {
      }
      void AbstractNodeVisitor::visitLiteralOptional(LiteralOptional::LiteralOptionalPtr)
      {
      }
      void AbstractNodeVisitor::visitLiteralStruct(LiteralStruct::LiteralStructPtr)
      {
      }
      void AbstractNodeVisitor::visitLiteralTuple(LiteralTuple::LiteralTuplePtr)
      {
      }
      void AbstractNodeVisitor::visitLiteralUnion(LiteralUnion::LiteralUnionPtr)
      {
      }

      void AbstractNodeVisitor::visitNewProcess(NewProcess::NewProcessPtr)
      {
      }

      void AbstractNodeVisitor::visitNewUnion(NewUnion::NewUnionPtr)
      {
      }

      void AbstractNodeVisitor::visitVariable(Variable::VariablePtr)
      {
      }

      // statements

      void AbstractNodeVisitor::visitAbortStatement(AbortStatement::AbortStatementPtr)
      {
      }

      void AbstractNodeVisitor::visitBreakStatement(BreakStatement::BreakStatementPtr)
      {
      }

      void AbstractNodeVisitor::visitContinueStatement(ContinueStatement::ContinueStatementPtr)
      {
      }

      void AbstractNodeVisitor::visitCommentStatement(CommentStatement::CommentStatementPtr)
      {
      }

      void AbstractNodeVisitor::visitForeachStatement(ForeachStatement::ForeachStatementPtr)
      {
      }

      void AbstractNodeVisitor::visitIfStatement(IfStatement::IfStatementPtr)
      {
      }

      void AbstractNodeVisitor::visitInputPortDecl(InputPortDecl::InputPortDeclPtr)
      {
      }

      void AbstractNodeVisitor::visitLoopStatement(LoopStatement::LoopStatementPtr)
      {
      }

      void AbstractNodeVisitor::visitNoStatement(NoStatement::NoStatementPtr)
      {
      }

      void AbstractNodeVisitor::visitOutputPortDecl(OutputPortDecl::OutputPortDeclPtr)
      {
      }

      void AbstractNodeVisitor::visitOwnConstructorCall(OwnConstructorCall::OwnConstructorCallPtr)
      {
      }

      void AbstractNodeVisitor::visitProcessBlock(ProcessBlock::ProcessBlockPtr)
      {
      }

      void AbstractNodeVisitor::visitProcessConstructor(ProcessConstructor::ProcessConstructorPtr)
      {
      }

      void AbstractNodeVisitor::visitProcessDecl(ProcessDecl::ProcessDeclPtr)
      {
      }

      void AbstractNodeVisitor::visitProcessVariableUpdate(
          ProcessVariableUpdate::ProcessVariableUpdatePtr)
      {
      }

      void AbstractNodeVisitor::visitReturnStatement(ReturnStatement::ReturnStatementPtr)
      {
      }

      void AbstractNodeVisitor::visitYieldStatement(YieldStatement::YieldStatementPtr)
      {
      }

      void AbstractNodeVisitor::visitSkipStatement(SkipStatement::SkipStatementPtr)
      {
      }

      void AbstractNodeVisitor::visitIterateArrayStep(IterateArrayStep::IterateArrayStepPtr)
      {
      }

      void AbstractNodeVisitor::visitSequenceStep(SequenceStep::SequenceStepPtr)
      {
      }

      void AbstractNodeVisitor::visitSingleValueStep(SingleValueStep::SingleValueStepPtr)
      {
      }

      void AbstractNodeVisitor::visitStatementBlock(StatementBlock::StatementBlockPtr)
      {
      }

      void AbstractNodeVisitor::visitTryCatch(TryCatch::TryCatchPtr)
      {
      }

      void AbstractNodeVisitor::visitThrowStatement(ThrowStatement::ThrowStatementPtr)
      {
      }

      void AbstractNodeVisitor::visitProcessValue(ProcessValue::ProcessValuePtr)
      {
      }

      void AbstractNodeVisitor::visitProcessVariable(ProcessVariable::ProcessVariablePtr)
      {
      }

      void AbstractNodeVisitor::visitValueDecl(ValueDecl::ValueDeclPtr)
      {
      }

      void AbstractNodeVisitor::visitVariableDecl(VariableDecl::VariableDeclPtr)
      {
      }

      void AbstractNodeVisitor::visitVariableUpdate(VariableUpdate::VariableUpdatePtr)
      {
      }

      // others
      void AbstractNodeVisitor::visitLoop(Loop::LoopPtr)
      {
      }
      void AbstractNodeVisitor::visitParameter(Parameter::ParameterPtr)
      {
      }

    }
  }
}
