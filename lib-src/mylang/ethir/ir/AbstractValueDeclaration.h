#ifndef CLASS_MYLANG_ETHIR_IR_ABSTRACTVALUEDECLARATION_H
#define CLASS_MYLANG_ETHIR_IR_ABSTRACTVALUEDECLARATION_H

#ifndef CLASS_MYLANG_ETHIR_IR_DECLARATION_H
#include <mylang/ethir/ir/Declaration.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_EXPRESSION_H
#include <mylang/ethir/ir/Expression.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * A variable declaration that cannot be modified.
       */
      class AbstractValueDeclaration: public Declaration
      {
        AbstractValueDeclaration(const AbstractValueDeclaration&) = delete;

        AbstractValueDeclaration& operator=(const AbstractValueDeclaration&) = delete;

        /** A value pointer */
      public:
        typedef ::std::shared_ptr<const AbstractValueDeclaration> AbstractValueDeclarationPtr;

        /**
         * Create a bind statement.
         * @param name a name
         * @param value the value to be bound
         */
      public:
        AbstractValueDeclaration(Variable::VariablePtr val, Expression::ExpressionPtr value,
            StatementPtr next);

        /** Destructor */
      public:
        ~AbstractValueDeclaration();

        void computeHashCode(HashCode &h) const override;
        bool isEqual(const Node &node) const override;

        /**
         * The name and type of the local variable that was introduced.
         */
      public:
        const Variable::VariablePtr variable;

        /**
         * The bound value may be null (but not if it's a global)
         */
      public:
        const Expression::ExpressionPtr value;
      };
    }

  }
}
#endif
