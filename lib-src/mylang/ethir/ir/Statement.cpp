#include <mylang/ethir/ir/Statement.h>
#include <mylang/ethir/ir/NoStatement.h>
#include <mylang/ethir/ir/SkipStatement.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      namespace {
        static Statement::StatementPtr filterNop(Statement::StatementPtr next)
        {
          if (next) {
            auto nop = next->self<NoStatement>();
            if (nop) {
              return nop->next;
            }
          }
          return next;
        }
      }

      Statement::Statement(StatementPtr xnext)
          : next(filterNop(xnext))
      {
      }

      Statement::~Statement()
      {
      }

      Statement::StatementPtr Statement::skipComments() const
      {
        return self<Statement>();
      }

      bool Statement::isNOP() const
      {
        return false;
      }

      Statement::StatementPtr Statement::append(StatementPtr stmt) const
      {
        if (stmt == nullptr) {
          return self<Statement>();
        }

        Statement::StatementPtr tail;
        if (next) {
          tail = next->append(stmt);
          // if statement sequence ends with, e.g. break, then append
          // becomes a NOP
          if (tail == next) {
            return self<Statement>();
          }
        } else {
          tail = stmt;
        }
        return replaceNext(tail);
      }

      Statement::StatementPtr Statement::lastStatement() const
      {
        // it would be faster to do this in a simple while loop, but
        // then we'd have issues with blocks.
        // TODO: refactor code so that we can do this as a single while loop
        if (next) {
          return next->lastStatement();
        } else {
          return self<Statement>();
        }
      }

      Statement::StatementPtr Statement::firstStatement() const
      {
        return self<Statement>();
      }

    }
  }
}
