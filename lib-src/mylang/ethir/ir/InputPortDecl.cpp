#include <mylang/ethir/ir/InputPortDecl.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      InputPortDecl::InputPortDecl(Variable::VariablePtr xval, ::std::string xpublicName)
          : PortDecl(xval, xpublicName, nullptr)
      {
        assert(xval->scope == Declaration::Scope::PROCESS_SCOPE);
      }

      InputPortDecl::~InputPortDecl()
      {
      }

      InputPortDecl::InputPortDeclPtr InputPortDecl::create(Variable::VariablePtr xval,
          ::std::string xpublicName)
      {
        return ::std::make_shared<InputPortDecl>(xval, xpublicName);
      }

      void InputPortDecl::accept(NodeVisitor &visitor) const
      {
        visitor.visitInputPortDecl(self<InputPortDecl>());
      }
      Statement::StatementPtr InputPortDecl::replaceNext(StatementPtr) const
      {
        return self<Statement>();
      }
      bool InputPortDecl::isEqual(const Node &node) const
      {
        const InputPortDecl &that = dynamic_cast<const InputPortDecl&>(node);
        return equals(variable, that.variable) && publicName == that.publicName
            && equals(next, that.next);
      }

      void InputPortDecl::computeHashCode(HashCode &h) const
      {
        h.mix(typeid(*this));
        h.mix(variable);
        h.mix(publicName);
        h.mix(next);
      }
    }

  }
}
