#ifndef CLASS_MYLANG_ETHIR_IR_ITERATEARRAYSTEP_H
#define CLASS_MYLANG_ETHIR_IR_ITERATEARRAYSTEP_H

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENT_H
#include <mylang/ethir/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENTBUILDER_H
#include <mylang/ethir/ir/StatementBuilder.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_SKIPSTATEMENT_H
#include <mylang/ethir/ir/SkipStatement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_LOOP_H
#include <mylang/ethir/ir/Loop.h>
#endif

#include <functional>

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * A loop step that loops over the elements in an array in reverse or forward
       * order and yields each element.
       */
      class IterateArrayStep: public Loop::Step
      {
        IterateArrayStep(const IterateArrayStep &e) = delete;

        IterateArrayStep& operator=(const IterateArrayStep &e) = delete;

        /** A body builder
         * @param value the value
         * @param updates the updates to be applied when the step is done
         * @param b a builder */
      public:
        typedef ::std::function<Loop::Expressions(EVariable, StatementBuilder&)> Builder;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const IterateArrayStep> IterateArrayStepPtr;

        /**
         * A new skip statement
         * @param any updates to apply to the loop variables
         */
      public:
        IterateArrayStep(bool reverse, SkipStatement::SkipStatementPtr nextState, EVariable array,
            EVariable counter, EVariable value, Loop::Expressions initializers, EStatement body);

        /** Destructor */
      public:
        ~IterateArrayStep();

        /**
         * Iterate over an array and yield each element.
         * @param reverse true if the array will be traversed in the reverse order
         * @param array the array to iterate over
         * @param counter the variable that holds the index
         * @param value the value at each iteration
         * @param body the body of the iteration
         */
      public:
        static IterateArrayStepPtr create(bool reverse, SkipStatement::SkipStatementPtr nextState,
            EVariable array, EVariable counter, EVariable value, Loop::Expressions initializers,
            EStatement body);

        /**
         * Iterate over an array and yield each element.
         * @param reverse true if the array will be traversed in the reverse order
         * @param array the array to iterate over
         * @param nextState the next state of the enclosing loop
         */
      public:
        static IterateArrayStepPtr create(bool reverse, EVariable array, Loop::State nextState,
            Builder b);

        /**
         * Iterate over an array and yield each element.
         * @param reverse true if the array will be traversed in the reverse order
         * @param array the array to iterate over
         * @param nextState the next state of the enclosing loop
         */
      public:
        static IterateArrayStepPtr create(bool reverse, EVariable array, Builder b);

        /**
         * Iterate over an array and yield each element.
         * @param reverse true if the array will be traversed in the reverse order
         * @param array the array to iterate over
         * @param nextState the next state of the enclosing loop
         */
      public:
        static IterateArrayStepPtr createDefault(bool reverse, EVariable array,
            Loop::State nextState = Loop::STOP_STATE);

      public:
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &that) const override final;
        void accept(NodeVisitor &visitor) const override final;

        Loop::Expressions getInitializers() const override final;
        EStatement toStatement() const override final;

        /** True if we're running in reverse */
      public:
        const bool reverse;

        /** The skip statement to get to the next state. */
      public:
        const SkipStatement::SkipStatementPtr nextState;

        /** The array that is being iterated over */
      public:
        const EVariable array;

        /** The counter variable to count from 0 to length of the array */
      public:
        const EVariable counter;

        /** The value that is processed by the body */
      public:
        const EVariable value;

        /** Local loop initializers. Typically, the only initializer
         * is the counter variable, but other variables may appear as well. */
      public:
        const Loop::Expressions initializers;

        /** The body of this stop */
      public:
        const EStatement body;
      };
    }
  }
}
#endif
