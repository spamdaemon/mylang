#ifndef CLASS_MYLANG_ETHIR_IR_PROCESSBLOCK_H
#define CLASS_MYLANG_ETHIR_IR_PROCESSBLOCK_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENT_H
#include <mylang/ethir/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENTBLOCK_H
#include <mylang/ethir/ir/StatementBlock.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif
#include <string>
#include <optional>

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * Read from a port into a variable.
       */
      class ProcessBlock: public Statement
      {
        ProcessBlock(const ProcessBlock &e) = delete;

        ProcessBlock& operator=(const ProcessBlock &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const ProcessBlock> ProcessBlockPtr;

        /**
         * Create a process block
         * @param name name of the block
         * @param condition under which the block is executed
         * @param body the body
         */
      public:
        ProcessBlock(::std::optional<::std::string> name, StatementBlock::StatementBlockPtr body);

        /** Destructor */
      public:
        ~ProcessBlock();

        /**
         * Create a read statement to read into the specified variable.
         * @param body the function body
         * @return a foreach statement
         */
      public:
        static ProcessBlockPtr create(::std::optional<::std::string> name,
            Statement::StatementPtr body);

      public:
        void accept(NodeVisitor &visitor) const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /**
         * The name and type of the local variable that was introduced.
         */
      public:
        const ::std::optional<::std::string> name;

        /** The body */
      public:
        const StatementBlock::StatementBlockPtr body;
      };

    }
  }
}
#endif
