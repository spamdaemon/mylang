#include <mylang/ethir/ir/GetDiscriminant.h>
#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <algorithm>
#include <memory>
#include <string>

namespace mylang {
  namespace ethir {
    namespace ir {

      GetDiscriminant::GetDiscriminant(TypePtr t, Variable::VariablePtr obj)
          : Expression(::std::move(t)), object(::std::move(obj))
      {
      }

      GetDiscriminant::~GetDiscriminant()
      {
      }

      GetDiscriminant::GetDiscriminantPtr GetDiscriminant::create(TypePtr t,
          Variable::VariablePtr obj)
      {
        return ::std::make_shared<GetDiscriminant>(t, obj);
      }

      void GetDiscriminant::accept(NodeVisitor &visitor) const
      {
        visitor.visitGetDiscriminant(self<GetDiscriminant>());
      }
      bool GetDiscriminant::isEqual(const Node &node) const
      {
        const GetDiscriminant &that = dynamic_cast<const GetDiscriminant&>(node);
        return equals(type, that.type) && equals(object, that.object);
      }
      void GetDiscriminant::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(object);
      }

    }
  }
}
