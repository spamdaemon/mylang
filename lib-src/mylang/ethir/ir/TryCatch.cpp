#include <mylang/ethir/ir/TryCatch.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      TryCatch::TryCatch(StatementBlock::StatementBlockPtr xTryBody,
          StatementBlock::StatementBlockPtr xCatchBody, StatementPtr xnext)
          : ControlFlowStatement(xnext), tryBody(xTryBody), catchBody(xCatchBody)
      {
      }

      TryCatch::~TryCatch()
      {
      }

      TryCatch::StatementPtr TryCatch::create(Statement::StatementPtr xTryBody,
          Statement::StatementPtr xCatchBody, StatementPtr xnext)
      {
        if (xTryBody->isNOP()) {
          return xnext;
        }
        return ::std::make_shared<TryCatch>(StatementBlock::singleton(xTryBody),
            StatementBlock::singleton(xCatchBody), xnext);
      }

      void TryCatch::accept(NodeVisitor &visitor) const
      {
        visitor.visitTryCatch(self<TryCatch>());
      }

      bool TryCatch::isNOP() const
      {
        return tryBody->isNOP();
      }
      Statement::StatementPtr TryCatch::replaceNext(StatementPtr tail) const
      {
        return create(tryBody, catchBody, tail);
      }
      bool TryCatch::isEqual(const Node &node) const
      {
        const TryCatch &that = dynamic_cast<const TryCatch&>(node);
        return equals(tryBody, that.tryBody) && equals(catchBody, that.catchBody)
            && equals(next, that.next);
      }
      void TryCatch::computeHashCode(HashCode &h) const
      {
        h.mix(tryBody);
        h.mix(catchBody);
        h.mix(next);
      }
    }
  }
}
