#ifndef CLASS_MYLANG_ETHIR_IR_PROCESSVALUE_H
#define CLASS_MYLANG_ETHIR_IR_PROCESSVALUE_H

#ifndef CLASS_MYLANG_ETHIR_IR_AHSTRACTVALUEDECLARATION_H
#include <mylang/ethir/ir/AbstractValueDeclaration.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_EXPRESSION_H
#include <mylang/ethir/ir/Expression.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * A variable declaration that cannot be modified.
       */
      class ProcessValue: public AbstractValueDeclaration
      {
        ProcessValue(const ProcessValue&) = delete;

        ProcessValue& operator=(const ProcessValue&) = delete;

        /** A value pointer */
      public:
        typedef ::std::shared_ptr<const ProcessValue> ProcessValuePtr;

        /**
         * Create a bind statement.
         * @param name a name
         * @param value the value to be bound
         */
      public:
        ProcessValue(Variable::VariablePtr val, Expression::ExpressionPtr value);

        /** Destructor */
      public:
        ~ProcessValue();

        /**
         * Declare a mutable variable with the specified initializer
         * @param name a name
         * @param value the value to be bound
         */
      public:
        static ProcessValuePtr create(Variable::VariablePtr val, Expression::ExpressionPtr value);

        /**
         * Declare a mutable variable with the specified initializer
         * @param name a name
         * @param value the value to be bound
         */
      public:
        static ProcessValuePtr create(const Name &val, Expression::ExpressionPtr value);

        /**
         * Declare a mutable variable that is not initialized
         * @param name a name
         * @param type of the value to be bound
         */
      public:
        static ProcessValuePtr create(Variable::VariablePtr val);

      public:
        void accept(NodeVisitor &visitor) const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

      };
    }

  }
}
#endif
