#ifndef CLASS_MYLANG_ETHIR_IR_LOOPSTATEMENT_H
#define CLASS_MYLANG_ETHIR_IR_LOOPSTATEMENT_H

#ifndef CLASS_MYLANG_ETHIR_IR_CONTROLFLOWSTATEMENT_H
#include <mylang/ethir/ir/ControlFlowStatement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENTBLOCK_H
#include <mylang/ethir/ir/StatementBlock.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * A variable declaration. Variables are mutable!
       */
      class LoopStatement: public ControlFlowStatement
      {
        LoopStatement(const LoopStatement &e) = delete;

        LoopStatement& operator=(const LoopStatement &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const LoopStatement> LoopStatementPtr;

        /**
         * Create an if-statment
         * @param a label for the loop
         * @param body body of the statement
         */
      public:
        LoopStatement(Name name, StatementBlock::StatementBlockPtr body, StatementPtr next);

        /** Destructor */
      public:
        ~LoopStatement();

        /**
         * Declare a mutable variable with the specified initializer
         * @param cond the condition
         * @param body body of the statement
         */
      public:
        static LoopStatementPtr create(Name name, Statement::StatementPtr body, StatementPtr next);

      public:
        void accept(NodeVisitor &visitor) const override final;
        bool isNOP() const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /**
         * The bound value (may be null!)
         */
      public:
        const Name name;

        /**
         * The statement to be executed while condition is true
         */
      public:
        const StatementBlock::StatementBlockPtr body;
      };
    }

  }
}
#endif
