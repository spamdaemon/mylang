#ifndef CLASS_MYLANG_ETHIR_IR_LAMBDAEXPRESSION_H
#define CLASS_MYLANG_ETHIR_IR_LAMBDAEXPRESSION_H

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TAG_H
#include <mylang/ethir/Tag.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_PARAMETER_H
#include <mylang/ethir/ir/Parameter.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENT_H
#include <mylang/ethir/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENTBLOCK_H
#include <mylang/ethir/ir/StatementBlock.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_FUNCTIONTYPE_H
#include <mylang/ethir/types/FunctionType.h>
#endif

#include <functional>

namespace mylang {
  namespace ethir {
    namespace ir {
      class StatementBuilder;

      /**
       * A variable declaration. Variables are mutable!
       */
      class LambdaExpression: public Expression
      {
        LambdaExpression(const LambdaExpression &e) = delete;

        LambdaExpression& operator=(const LambdaExpression &e) = delete;

        /** The parameters */
      public:
        typedef ::std::vector<Parameter::ParameterPtr> Parameters;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const LambdaExpression> LambdaExpressionPtr;

        /** Arguments */
      public:
        typedef ::std::vector<EVariable> Arguments;

        /** A simple lambda builder.         */
      public:
        typedef ::std::function<EType(Arguments, StatementBuilder&)> Builder;

        /** The signature type */
      public:
        typedef ::std::shared_ptr<const types::FunctionType> Signature;

        /**
         * Create a bind statement.
         * @param name a name
         * @param value the value to be bound
         */
      public:
        LambdaExpression(const Tag &xtag, Signature signature, Parameters parameters,
            StatementBlock::StatementBlockPtr body);

        /** Destructor */
      public:
        ~LambdaExpression();

        /**
         * Declare a mutable variable with the specified initializer
         * @param name a name
         * @param value the value to be bound
         */
      public:
        static LambdaExpressionPtr create(const Tag &xtag, Signature signature,
            Parameters parameters, Statement::StatementPtr body);

        /**
         * Declare a mutable variable with the specified initializer
         * @param name a name
         * @param value the value to be bound
         */
      public:
        static LambdaExpressionPtr create(Signature signature, Parameters parameters,
            Statement::StatementPtr body);

        /**
         * Declare a mutable variable with the specified initializer
         * @param name a name
         * @param value the value to be bound
         */
      public:
        static LambdaExpressionPtr create(Parameters parameters, types::Type::Ptr retTy,
            Statement::StatementPtr body);

        /**
         * Declare a mutable variable with the specified initializer
         * @param name a name
         * @param value the value to be bound
         */
      public:
        static LambdaExpressionPtr create(const Tag &xtag, Parameters parameters,
            types::Type::Ptr retTy, Statement::StatementPtr body);

        /**
         * Create a lambda expression.
         * @param params the parameter types
         * @param builder a function that builds the body
         * @return a lambda expression
         */
      public:
        static LambdaExpressionPtr create(const ::std::vector<EType> &params, Builder b);

        /**
         * Declare a mutable variable with the specified initializer
         * @param name a name
         * @param value the value to be bound
         */
      public:
        static LambdaExpressionPtr createIdentity(types::Type::Ptr retTy);

        /**
         * Test if the body of the lambda is the identity, ie. (x) -> { return x; }
         * @return true if this expression is identity
         */
      public:
        bool isIdentity() const;

      public:
        void accept(NodeVisitor &visitor) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /**
         * Get the function signature
         */
      public:
        inline Signature signature() const
        {
          return type->self<types::FunctionType>();
        }

        /**
         * Determine if this lambda's return type is void.
         * This function is equivalent to signature()->isVoid()
         */
      public:
        inline bool isVoid() const
        {
          return signature()->isVoid();
        }

        /**
         * Determine if this lambda is normalized.
         * A normalized lambda is on that has at most 1 return statement
         * and that return statement is the last statement in the body of the lambda.
         */
      public:
        bool isNormalized() const;

        /**
         * A globally unique name for this expression. If the function is duplicated, the name
         * can be used to tie the copy and the original one to each other.
         */
      public:
        const Tag tag;

        /** The parameters */
      public:
        const Parameters parameters;

        /**
         * The bound value (may be null!)
         */
      public:
        const StatementBlock::StatementBlockPtr body;
      };
    }

  }
}
#endif
