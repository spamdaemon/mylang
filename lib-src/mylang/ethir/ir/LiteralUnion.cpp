#include <mylang/ethir/ir/LiteralUnion.h>
#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/TypeMismatchError.h>
#include <mylang/ethir/TypeCastError.h>
#include <memory>
#include <string>

namespace mylang {
  namespace ethir {
    namespace ir {

      LiteralUnion::LiteralUnion(::std::shared_ptr<const types::UnionType> xtype,
          const ::std::string &xmember, LiteralPtr xvalue)
          : Literal(xtype), member(xmember), value(xvalue)
      {
        TypeMismatchError::check(xtype->getMemberType(xmember), value->type);
      }

      LiteralUnion::~LiteralUnion()
      {
      }

      LiteralUnion::LiteralUnionPtr LiteralUnion::create(
          ::std::shared_ptr<const types::UnionType> xtype, const ::std::string &xmember,
          LiteralPtr xvalue)
      {
        return ::std::make_shared<LiteralUnion>(xtype, xmember, xvalue);
      }

      void LiteralUnion::accept(NodeVisitor &visitor) const
      {
        visitor.visitLiteralUnion(self<LiteralUnion>());
      }
      ::std::optional<int> LiteralUnion::compare(const Literal &other) const
      {
        const LiteralUnion *that = dynamic_cast<const LiteralUnion*>(&other);
        if (that && type->isSameType(*that->type)) {
          auto ty = type->self<types::UnionType>();
          auto i = ty->indexOfMember(member);
          auto j = ty->indexOfMember(that->member);
          if (i != j) {
            return i < j ? -1 : 1;
          }
          return value->compare(*that->value);
        }
        return ::std::nullopt;
      }
      Literal::LiteralPtr LiteralUnion::castTo(const EType &newType) const
      {
        // cannot cast between different kinds of unions
        TypeMismatchError::check(type, newType);
        return self<Literal>();
      }
      bool LiteralUnion::isEqual(const Node &node) const
      {
        const LiteralUnion &that = dynamic_cast<const LiteralUnion&>(node);
        return member == that.member && equals(type, that.type) && equals(value, that.value);
      }
      void LiteralUnion::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(member);
        h.mix(value);
      }

    }

  }
}
