#include <mylang/ethir/ir/ControlFlowStatement.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      ControlFlowStatement::ControlFlowStatement(StatementPtr xnext)
          : Statement(xnext)
      {
      }

      ControlFlowStatement::~ControlFlowStatement()
      {
      }
    }

  }
}
