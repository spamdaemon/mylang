#ifndef CLASS_MYLANG_ETHIR_IR_LITERALREAL_H
#define CLASS_MYLANG_ETHIR_IR_LITERALREAL_H

#ifndef CLASS_MYLANG_ETHIR_IR_LITERALPRIMITIVE_H
#include <mylang/ethir/ir/LiteralPrimitive.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_REALYPE_H
#include <mylang/ethir/types/RealType.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * This class represent the constructor for a literal.
       */
      class LiteralReal: public LiteralPrimitive
      {
        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const LiteralReal> LiteralRealPtr;

        /** The value type */
      public:
        typedef BigReal ValueType;

        /**
         * Create an expression that returns a literal value.
         * @param type the type
         * @aparam value the string value for the literal
         */
      public:
        LiteralReal(::std::shared_ptr<const types::RealType> type, ValueType value);

        /** Destructor */
      public:
        ~LiteralReal();

        /**
         * Create an bit literal
         * @param value true or false
         * @return a literal
         */
      public:
        static LiteralRealPtr create(::std::shared_ptr<const types::RealType> type,
            ValueType value);

        /**
         * Create an bit literal
         * @param value true or false
         * @return a literal
         */
      public:
        static LiteralRealPtr create(ValueType value);

      public:
        void accept(NodeVisitor &visitor) const override final;
        ::std::optional<int> compare(const Literal &that) const override final;
        LiteralPtr castTo(const EType &newType) const override;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The Int64 value */
      public:
        const ValueType value;
      };
    }
  }
}
#endif
