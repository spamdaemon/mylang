#ifndef CLASS_MYLANG_ETHIR_IR_OPERATOREXPRESSION_H
#define CLASS_MYLANG_ETHIR_IR_OPERATOREXPRESSION_H

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_FUNCTIONTYPE_H
#include <mylang/ethir/types/FunctionType.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#include <mylang/ethir/types/Type.h>
#endif

#include <vector>

namespace mylang {
  namespace ethir {
    namespace ir {

      class OperatorExpression: public Expression
      {
      public:
        enum Operator
        {
          OP_NEW, // the operator to allocate a new object
          OP_CALL, // a function call
          OP_NEG, // the negation operator on numbers
          OP_ADD, // the plus operator on numbers
          OP_SUB, // the minus operator on numbers
          OP_MUL, // the multiplication operator on numbers
          OP_DIV, // the division operator on numbers
          OP_MOD, // the modulus operator on numbers
          OP_REM, // the remainder operator on numbers

          OP_CHECKED_CAST, // a cast that whose preconditions have been checked and cannot fail
          OP_UNCHECKED_CAST, // a cast that depends on the value and may fail at runtime
          OP_BASETYPE_CAST,
          OP_ROOTTYPE_CAST,

          OP_INDEX, // indexing into an array or string
          OP_SUBRANGE, // subrange into an array or string
          OP_EQ, // comparing two values for equality
          OP_NEQ, // comparing two values for inequality
          OP_LT, // comparing two values for ordering LessThan
          OP_LTE, // comparing two values for ordering LessThanOrEqual
          OP_AND, // the bitwise/logical AND operator &
          OP_OR,  // the bitwise/logical OR operator |
          OP_XOR,  // the bitwise/logical XOR operator ^
          OP_NOT,  // the bitwise NOT operator ~
          OP_GET, // get value of an optional
          OP_IS_LOGGABLE, // check if a log level is enabled
          OP_IS_PRESENT, // true if an optional is present
          OP_IS_INPUT_NEW, // true if input is new
          OP_IS_INPUT_CLOSED, // true if input port is closed
          OP_IS_OUTPUT_CLOSED, // ture if output port is closed
          OP_IS_PORT_READABLE, // true if a port can be read without block
          OP_IS_PORT_WRITABLE, // true if a port can be written without blocking
          OP_SIZE, // length of an array
          OP_CONCATENATE,
          OP_MERGE_TUPLES,
          OP_ZIP,
          OP_FROM_BITS,
          OP_TO_BITS,
          OP_FROM_BYTES,
          OP_TO_BYTES,
          OP_BITS_TO_BYTES,
          OP_BYTES_TO_BITS,
          OP_CLAMP,
          OP_WRAP,
          OP_BYTE_TO_UINT8,
          OP_BYTE_TO_SINT8,
          OP_STRING_TO_CHARS,
          OP_INTERPOLATE_TEXT,
          OP_TO_STRING,
          OP_FOLD,
          OP_MAP_ARRAY,
          OP_MAP_OPTIONAL,
          OP_FILTER_ARRAY,
          OP_FILTER_OPTIONAL,
          OP_FIND,
          OP_PAD,
          OP_TRIM,
          OP_TAKE,
          OP_DROP,
          OP_REVERSE,
          OP_FLATTEN,
          OP_PARTITION,
          OP_HEAD,
          OP_TAIL,
          OP_STRUCT_TO_TUPLE,
          OP_TUPLE_TO_STRUCT,
          /***************************************\
           * these expressions have side-effects *
           \***************************************/
          OP_READ_PORT,
          OP_BLOCK_READ,
          OP_BLOCK_WRITE,
          OP_CLEAR_PORT,
          OP_CLOSE_PORT,
          OP_WAIT_PORT,
          OP_WRITE_PORT,
          OP_LOG,
          OP_SET,
          OP_APPEND,
          OP_BUILD,
        };

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const OperatorExpression> OperatorExpressionPtr;

        /** A type pointer */
      public:
        typedef Variable::VariablePtr VariablePtr;

      public:
        typedef ::std::vector<VariablePtr> Arguments;

      public:
        typedef ::std::vector<TypePtr> Types;

        /**
         * Create an expression with the specified type.
         * @param type the type
         * @param op the builtin function
         * @param args the arguments
         */
      public:
        OperatorExpression(TypePtr t, Operator op, Arguments args);

        /** destructor */
      public:
        ~OperatorExpression();

        /**
         * Create an expression that takes no arguments
         * @param type the type
         * @param op the builtin function
         */
      public:
        static OperatorExpressionPtr create(TypePtr t, Operator op);

        /**
         * Create a unary operator expression.
         * @param type the type
         * @param op the builtin function
         * @param arg an argument
         */
      public:
        static OperatorExpressionPtr create(TypePtr t, Operator op, Arguments args);

        /**
         * Create a unary operator expression.
         * @param type the type
         * @param op the builtin function
         * @param arg an argument
         */
      public:
        static OperatorExpressionPtr create(TypePtr t, Operator op, VariablePtr arg);

        /**
         * Create an expression with the specified type.
         * @param type the type
         * @param op the builtin function
         * @param left the left argument
         * @param right the right argument
         * @return a builtin call
         */
      public:
        static OperatorExpressionPtr create(TypePtr t, Operator op, VariablePtr left,
            VariablePtr right);

        /**
         * Create an expression with the specified type.
         * @param type the type
         * @param op the builtin function
         * @param arg1 the first argument
         * @param arg2 the second argument
         * @param arg3 the third argument
         * @return a builtin call
         */
      public:
        static OperatorExpressionPtr create(TypePtr t, Operator op, VariablePtr arg1,
            VariablePtr arg2, VariablePtr arg3);

        /**
         * Create an expression that takes no arguments
         * @param type the type
         * @param op the builtin function
         */
      public:
        static OperatorExpressionPtr create(Operator op);

        /**
         * Create a unary operator expression.
         * @param type the type
         * @param op the builtin function
         * @param arg an argument
         */
      public:
        static OperatorExpressionPtr create(Operator op, Arguments args);

        /**
         * Create a unary operator expression.
         * @param type the type
         * @param op the builtin function
         * @param arg an argument
         */
      public:
        static OperatorExpressionPtr create(Operator op, VariablePtr arg);

        /**
         * Create an expression with the specified type.
         * @param type the type
         * @param op the builtin function
         * @param left the left argument
         * @param right the right argument
         * @return a builtin call
         */
      public:
        static OperatorExpressionPtr create(Operator op, VariablePtr left, VariablePtr right);

        /**
         * Create an expression with the specified type.
         * @param type the type
         * @param op the builtin function
         * @param arg1 the first argument
         * @param arg2 the second argument
         * @param arg3 the third argument
         * @return a builtin call
         */
      public:
        static OperatorExpressionPtr create(Operator op, VariablePtr arg1, VariablePtr arg2,
            VariablePtr arg3);

        /**
         * Create a checked cast expression for an expression.
         * @param ty the destination type
         * @param expr an expression
         * @return an expression (not necessarily an operator expression)
         */
      public:
        static ExpressionPtr checked_cast(TypePtr t, ExpressionPtr expr);

        /**
         * Get the type for the specified operator and arguments.
         * @param op the builtin function
         * @param arg an argument
         * @reeturn a type or nullptr if a type could not be derived
         */
      public:
        static TypePtr typeFor(Operator op, Arguments args);

        /**
         * Get the type for a specified operator. This function computes the output
         * type for the specified operation, given the types for its input arguments.
         * The result type is optional and only used in expression where an output type
         * is required.
         * @param op an operator
         * @param args the arguments
         * @param resTy the expected output type
         * @return the output type
         */
        static TypePtr typeFor(Operator op, const Types &args, const TypePtr &resTy = nullptr);

        void accept(NodeVisitor &visitor) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /**
         * Get the function signature for this expression.
         * This signature can be used to wrap the expression in a lambda expression.
         * @return the function type for the equivalent function, or nullptr if a function cannot exist
         */
      public:
        ::std::shared_ptr<const types::FunctionType> getSignature() const;

        /**
         * Get a string for the operator.
         * @return a name for the operator
         */
      public:
        static const char* operatorName(Operator op);

        /** The builtin name */
      public:
        const Operator name;

        /** The function arguments */
      public:
        const Arguments arguments;
      };
    }
  }
}
#endif
