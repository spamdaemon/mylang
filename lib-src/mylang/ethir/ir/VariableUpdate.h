#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLEUPDATE_H
#define CLASS_MYLANG_ETHIR_IR_VARIABLEUPDATE_H

#ifndef MYLANGCLASS_MYLANG_ETHIR_NAME_H
#include <mylang/ethir/Name.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENT_H
#include <mylang/ethir/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * Assign a value to a variable that is in a function scope.
       */
      class VariableUpdate: public Statement
      {
        VariableUpdate(const VariableUpdate &e) = delete;

        VariableUpdate& operator=(const VariableUpdate &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const VariableUpdate> VariableUpdatePtr;

        /**
         * Crea
         * @param name a name
         * @param value the value to be bound
         */
      public:
        VariableUpdate(Variable::VariablePtr target, Variable::VariablePtr value,
            StatementPtr next);

        /** Destructor */
      public:
        ~VariableUpdate();

        /**
         * Updateare a mutable variable with the specified initializer
         * @param name a name
         * @param value the value to be bound
         */
      public:
        static VariableUpdatePtr create(Variable::VariablePtr target, Variable::VariablePtr value,
            StatementPtr next);

      public:
        void accept(NodeVisitor &visitor) const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /**
         * The name and type of the local variable that was introduced.
         */
      public:
        const Variable::VariablePtr target;

        /**
         * The bound value (may be null!)
         */
      public:
        const Variable::VariablePtr value;
      };
    }
  }
}
#endif
