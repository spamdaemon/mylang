#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/ir/StatementBlock.h>
#include <mylang/ethir/prettyPrint.h>
#include <memory>
#include <cassert>

namespace mylang::ethir::ir {
  namespace {

    // most of the time, this will be a no-op
    static Statement::StatementPtr concatenate(const Statement::StatementPtr &s,
        const Statement::StatementPtr &n)
    {
      if (s == nullptr) {
        return n;
      }
      auto next = concatenate(s->next, n);
      if (next == s->next) {
        return s;
      } else {
        return s->replaceNext(next);
      }
    }

    // most of the time, this will be a no-op, since most lists of statements won't
    // contain blocks
    static Statement::StatementPtr flattenBlocks(const Statement::StatementPtr &s,
        const Statement::StatementPtr &n)
    {
      if (s == nullptr) {
        if (n) {
          return flattenBlocks(n, nullptr);
        } else {
          return nullptr;
        }
      }

      auto next = flattenBlocks(s->next, n);
      if (auto b = s->self<StatementBlock>(); b) {
        // next is already free of blocks and so is are
        // the statements of the block by induction
        return concatenate(b->statements, next);
      } else if (next != s->next) {
        return s->replaceNext(next);
      } else {
        return s;
      }
    }
  }

  StatementBlock::StatementBlock(Statement::StatementPtr xstatements)
      : Statement(nullptr), statements(xstatements)
  {
    auto s = statements;
    while (s != nullptr) {
      assert(s->self<StatementBlock>() == nullptr);
      s = s->next;
    }
  }

  StatementBlock::~StatementBlock()
  {
  }

  StatementBlock::StatementBlockPtr StatementBlock::singleton(Statement::StatementPtr stmts)
  {
    if (stmts == nullptr) {
      return ::std::make_shared<StatementBlock>(nullptr);
    }
    if (auto b = stmts->self<StatementBlock>(); b) {
      return b;
    }
    return ::std::make_shared<StatementBlock>(flattenBlocks(stmts, nullptr));
  }

  StatementBlock::StatementBlockPtr StatementBlock::create(Statement::StatementPtr xstatements,
      Statement::StatementPtr xnext)
  {
    if (xnext == nullptr) {
      return singleton(xstatements);
    }
    return ::std::make_shared<StatementBlock>(flattenBlocks(xstatements, xnext));
  }

  void StatementBlock::accept(NodeVisitor &visitor) const
  {
    visitor.visitStatementBlock(self<StatementBlock>());
  }

  bool StatementBlock::isNOP() const
  {
    auto s = statements;
    while (s) {
      if (!s->isNOP()) {
        return false;
      }
      s = s->next;
    }
    return true;
  }
  Statement::StatementPtr StatementBlock::replaceNext(StatementPtr tail) const
  {
    auto n = flattenBlocks(tail, nullptr);
    auto s = concatenate(statements, n);

    return ::std::make_shared<StatementBlock>(s);
  }
  bool StatementBlock::isEqual(const Node &node) const
  {
    const StatementBlock &that = dynamic_cast<const StatementBlock&>(node);
    return equals(statements, that.statements);
  }

  void StatementBlock::computeHashCode(HashCode &h) const
  {
    h.mix(typeid(*this));
    h.mix(statements);
  }

  StatementBlock::StatementPtr StatementBlock::firstStatement() const
  {
    if (!statements) {
      return nullptr;
    }
    if (auto b = statements->self<StatementBlock>(); b) {
      return b->firstStatement();
    }
    return statements;
  }

  StatementBlock::StatementPtr StatementBlock::lastStatement() const
  {
    if (statements) {
      return statements->lastStatement();
    } else {
      return self<Statement>();
    }
  }

}
