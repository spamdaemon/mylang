#include <mylang/ethir/ir/SkipStatement.h>
#include <mylang/ethir/ir/Loop.h>
#include <mylang/ethir/ir/ContinueStatement.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      SkipStatement::SkipStatement(Loop::Expressions xupdates, Loop::State xnextState,
          bool xresetNextState)
          : ControlFlowStatement(nullptr), updates(::std::move(xupdates)), nextState(xnextState),
              resetNextState(xresetNextState)
      {
      }

      SkipStatement::~SkipStatement()
      {
      }

      SkipStatement::SkipStatementPtr SkipStatement::create(Loop::Expressions xupdates,
          Loop::State xnextState, bool xresetNextState)
      {
        assert(xresetNextState == false || xnextState != Loop::STOP_STATE);
        return ::std::make_shared<SkipStatement>(::std::move(xupdates), xnextState, xresetNextState);
      }

      SkipStatement::SkipStatementPtr SkipStatement::create(Loop::State xnextState,
          bool xresetNextState)
      {
        return create( { }, xnextState, xresetNextState);
      }

      SkipStatement::SkipStatementPtr SkipStatement::createStop()
      {
        return create(Loop::STOP_STATE);
      }

      void SkipStatement::accept(NodeVisitor &visitor) const
      {
        visitor.visitSkipStatement(self<SkipStatement>());
      }

      Statement::StatementPtr SkipStatement::replaceNext(StatementPtr) const
      {
        return self<Statement>();
      }
      bool SkipStatement::isEqual(const Node &node) const
      {
        const SkipStatement &that = dynamic_cast<const SkipStatement&>(node);
        return resetNextState == that.resetNextState && nextState == that.nextState
            && Loop::equals(updates, that.updates);
      }

      void SkipStatement::computeHashCode(HashCode &h) const
      {
        h.mix(typeid(*this));
        h.mix(resetNextState);
        h.mix(next);
      }

    }

  }
}
