#ifndef CLASS_MYLANG_ETHIR_IR_PARAMETER_H
#define CLASS_MYLANG_ETHIR_IR_PARAMETER_H

#ifndef CLASS_MYLANG_ETHIR_IR_NODE_H
#include <mylang/ethir/ir/Node.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#include <mylang/ethir/types/Type.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_NAME_H
#include <mylang/ethir/Name.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {
      class Parameter: public Node
      {
        Parameter(const Parameter &e) = delete;

        Parameter& operator=(const Parameter &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const mylang::ethir::types::Type> TypePtr;

        /** An expression pointer */
      public:
        typedef ::std::shared_ptr<const Parameter> ParameterPtr;

        /**
         * Create a declaration of that associates a type with a name.
         * @param name the name of the declaration
         * @param type the type
         */
      public:
        Parameter(Name name, TypePtr t);

        /** Destructor */
      public:
        virtual ~Parameter();

        /**
         * Create a parameter.
         * @param name the name of the parameter
         * @param type the parameter type
         */
      public:
        static ParameterPtr create(Name name, TypePtr type);

      public:
        void accept(NodeVisitor &visitor) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /**
         * The variable that references the parameter.
         */
      public:
        const EVariable variable;
      };
    }
  }
}
#endif
