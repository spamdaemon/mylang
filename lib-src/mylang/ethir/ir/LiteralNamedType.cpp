#include <mylang/ethir/ir/LiteralNamedType.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/TypeMismatchError.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      LiteralNamedType::LiteralNamedType(::std::shared_ptr<const types::NamedType> xtype,
          LiteralPtr xvalue)
          : Literal(xtype), value(xvalue)
      {
        if (!value) {
          throw ::std::invalid_argument("Missing value");
        }
        TypeMismatchError::check(xtype->resolve(), value->type);
      }

      LiteralNamedType::~LiteralNamedType()
      {
      }

      LiteralNamedType::LiteralNamedTypePtr LiteralNamedType::create(
          ::std::shared_ptr<const types::NamedType> xtype, LiteralPtr xvalue)
      {
        return ::std::make_shared<LiteralNamedType>(xtype, xvalue);
      }

      void LiteralNamedType::accept(NodeVisitor &visitor) const
      {
        visitor.visitLiteralNamedType(self<LiteralNamedType>());
      }
      ::std::optional<int> LiteralNamedType::compare(const Literal &other) const
      {
        const LiteralNamedType *that = dynamic_cast<const LiteralNamedType*>(&other);
        if (that && type->isSameType(*that->type)) {
          return value->compare(*that->value);
        }
        return ::std::nullopt;
      }
      Literal::LiteralPtr LiteralNamedType::castTo(const EType &newType) const
      {
        TypeMismatchError::check(type, newType);
        return self<Literal>();
      }
      bool LiteralNamedType::isEqual(const Node &node) const
      {
        const LiteralNamedType &that = dynamic_cast<const LiteralNamedType&>(node);
        return equals(type, that.type) && equals(value, that.value);
      }
      void LiteralNamedType::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(value);
      }

    }
  }
}
