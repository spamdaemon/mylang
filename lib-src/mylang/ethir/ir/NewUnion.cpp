#include <mylang/ethir/ir/NewUnion.h>
#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      NewUnion::NewUnion(TypePtr t, ::std::string xmember, Variable::VariablePtr xvalue)
          : Expression(::std::move(t)), member(xmember), value(xvalue)
      {
        assert(value && "Missing value");
      }

      NewUnion::~NewUnion()
      {
      }

      NewUnion::NewUnionPtr NewUnion::create(TypePtr t, ::std::string xmember,
          Variable::VariablePtr xvalue)
      {
        return ::std::make_shared<NewUnion>(t, xmember, xvalue);
      }

      void NewUnion::accept(NodeVisitor &visitor) const
      {
        visitor.visitNewUnion(self<NewUnion>());
      }

      bool NewUnion::isEqual(const Node &node) const
      {
        const NewUnion &that = dynamic_cast<const NewUnion&>(node);
        return member == that.member && equals(type, that.type) && equals(value, that.value);
      }

      void NewUnion::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(member);
        h.mix(value);
      }
    }

  }
}
