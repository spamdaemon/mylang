#include <mylang/ethir/ir/AbstractOperatorVisitor.h>

namespace mylang {
  namespace ethir {
    namespace ir {
      AbstractOperatorVisitor::~AbstractOperatorVisitor()
      {
      }

      void AbstractOperatorVisitor::visit_NEW(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_CALL(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_CHECKED_CAST(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_UNCHECKED_CAST(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_NEG(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_ADD(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_SUB(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_MUL(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_DIV(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_MOD(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_REM(OperatorExpression::OperatorExpressionPtr)
      {
      }

      void AbstractOperatorVisitor::visit_INDEX(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_SUBRANGE(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_EQ(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_NEQ(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_LT(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_LTE(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_AND(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_OR(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_XOR(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_NOT(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_GET(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_IS_LOGGABLE(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_IS_PRESENT(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_IS_INPUT_NEW(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_IS_INPUT_CLOSED(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_IS_OUTPUT_CLOSED(
          OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_IS_PORT_READABLE(
          OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_IS_PORT_WRITABLE(
          OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_LENGTH(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_CONCATENATE(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_MERGE_TUPLES(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_ZIP(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_BITS_TO_BYTES(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_BYTES_TO_BITS(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_FROM_BITS(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_TO_BITS(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_FROM_BYTES(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_TO_BYTES(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_CLAMP(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_WRAP(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_BYTE_TO_UINT8(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_BYTE_TO_SINT8(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_STRING_TO_CHARS(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_INTERPOLATE_TEXT(
          OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_TO_STRING(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_FOLD(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_PAD(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_TRIM(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_TAKE(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_DROP(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_MAP_ARRAY(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_MAP_OPTIONAL(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_FILTER_ARRAY(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_FILTER_OPTIONAL(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_FIND(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_REVERSE(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_FLATTEN(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_PARTITION(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_HEAD(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_TAIL(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_STRUCT_TO_TUPLE(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_TUPLE_TO_STRUCT(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_BASETYPE_CAST(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_ROOTTYPE_CAST(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_READ_PORT(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_BLOCK_READ(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_BLOCK_WRITE(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_CLEAR_PORT(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_CLOSE_PORT(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_WAIT_PORT(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_WRITE_PORT(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_LOG(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_SET(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_APPEND(OperatorExpression::OperatorExpressionPtr)
      {
      }
      void AbstractOperatorVisitor::visit_BUILD(OperatorExpression::OperatorExpressionPtr)
      {
      }

    }
  }
}
