#include <mylang/ethir/ir/LiteralReal.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/TypeCastError.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      LiteralReal::LiteralReal(::std::shared_ptr<const types::RealType> xtype, ValueType xvalue)
          : LiteralPrimitive(xtype, xvalue.toString()), value(xvalue)
      {
      }

      LiteralReal::~LiteralReal()
      {
      }

      LiteralReal::LiteralRealPtr LiteralReal::create(
          ::std::shared_ptr<const types::RealType> xtype, ValueType xvalue)
      {
        return ::std::make_shared<LiteralReal>(xtype, xvalue);
      }

      LiteralReal::LiteralRealPtr LiteralReal::create(ValueType xvalue)
      {
        return ::std::make_shared<LiteralReal>(types::RealType::create(), xvalue);
      }

      void LiteralReal::accept(NodeVisitor &visitor) const
      {
        visitor.visitLiteralReal(self<LiteralReal>());
      }
      ::std::optional<int> LiteralReal::compare(const Literal &obj) const
      {
        auto that = dynamic_cast<const LiteralReal*>(&obj);
        if (that == nullptr) {
          return ::std::nullopt;
        }
        if (value < that->value) {
          return -1;
        } else if (value > that->value) {
          return 1;
        } else {
          return 0;
        }
      }
      Literal::LiteralPtr LiteralReal::castTo(const EType &newType) const
      {
        TypeCastError::checkCast(type, newType);
        return self<Literal>();
      }
      bool LiteralReal::isEqual(const Node &node) const
      {
        const LiteralReal &that = dynamic_cast<const LiteralReal&>(node);
        return value == that.value && equals(type, that.type);
      }
      void LiteralReal::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(value);
      }

    }
  }
}
