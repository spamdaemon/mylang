#ifndef CLASS_MYLANG_ETHIR_IR_PORTDECL_H
#define CLASS_MYLANG_ETHIR_IR_PORTDECL_H

#ifndef CLASS_MYLANG_ETHIR_IR_DECLARATION_H
#include <mylang/ethir/ir/Declaration.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#include <mylang/ethir/types/Type.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_NAME_H
#include <mylang/ethir/Name.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {
      class PortDecl: public Declaration
      {
        PortDecl(const PortDecl &e) = delete;

        PortDecl& operator=(const PortDecl &e) = delete;

        /** An expression pointer */
      public:
        typedef ::std::shared_ptr<const PortDecl> PortDeclPtr;

        /**
         * Create a declaration of that associates a type with a name.
         * @param name the name of the declaration
         * @param type the type
         */
      public:
        PortDecl(Variable::VariablePtr val, ::std::string publicName, StatementPtr next);

        /** Destructor */
      public:
        virtual ~PortDecl() = 0;

        /**
         * The name and type of the local variable that was introduced.
         */
      public:
        const Variable::VariablePtr variable;

        /** The public name of this port. */
      public:
        const ::std::string publicName;
      };
    }
  }
}
#endif
