#include <mylang/ethir/ir/LiteralByte.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/TypeMismatchError.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      LiteralByte::LiteralByte(::std::shared_ptr<const types::ByteType> xtype, ValueType xvalue)
          : LiteralPrimitive(xtype, ::std::to_string(static_cast<uint16_t>(xvalue))), value(xvalue)
      {
      }

      LiteralByte::~LiteralByte()
      {
      }

      LiteralByte::LiteralBytePtr LiteralByte::create(
          ::std::shared_ptr<const types::ByteType> xtype, ValueType xvalue)
      {
        return ::std::make_shared<LiteralByte>(xtype, xvalue);
      }

      LiteralByte::LiteralBytePtr LiteralByte::create(ValueType xvalue)
      {
        return create(types::ByteType::create(), xvalue);
      }

      void LiteralByte::accept(NodeVisitor &visitor) const
      {
        visitor.visitLiteralByte(self<LiteralByte>());
      }
      ::std::optional<int> LiteralByte::compare(const Literal &obj) const
      {
        auto that = dynamic_cast<const LiteralByte*>(&obj);
        if (that == nullptr) {
          return ::std::nullopt;
        }
        if (value < that->value) {
          return -1;
        } else if (value > that->value) {
          return 1;
        } else {
          return 0;
        }
      }
      Literal::LiteralPtr LiteralByte::castTo(const EType &newType) const
      {
        TypeMismatchError::check(type, newType);
        return self<Literal>();
      }
      bool LiteralByte::isEqual(const Node &node) const
      {
        const LiteralByte &that = dynamic_cast<const LiteralByte&>(node);
        return value == that.value && equals(type, that.type);
      }
      void LiteralByte::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix((::std::uint64_t) value);
      }

    }
  }
}
