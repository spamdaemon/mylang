#include <mylang/ethir/ir/VariableUpdate.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      VariableUpdate::VariableUpdate(Variable::VariablePtr xtarget, Variable::VariablePtr xexpr,
          StatementPtr xnext)
          : Statement(xnext), target(xtarget), value(xexpr)
      {
        if (target->scope != Declaration::Scope::FUNCTION_SCOPE) {
          throw ::std::invalid_argument("Variable is not in function scope");
        }
        assert(target);
        assert(value);
        assert(target->type->isSameType(*value->type) && "Variable update with incompatible types");
      }

      VariableUpdate::~VariableUpdate()
      {
      }

      VariableUpdate::VariableUpdatePtr VariableUpdate::create(Variable::VariablePtr xtarget,
          Variable::VariablePtr xvalue, StatementPtr xnext)
      {
        return ::std::make_shared<VariableUpdate>(xtarget, xvalue, xnext);
      }

      void VariableUpdate::accept(NodeVisitor &visitor) const
      {
        visitor.visitVariableUpdate(self<VariableUpdate>());
      }

      Statement::StatementPtr VariableUpdate::replaceNext(StatementPtr tail) const
      {
        return create(target, value, tail);
      }
      bool VariableUpdate::isEqual(const Node &node) const
      {
        const VariableUpdate &that = dynamic_cast<const VariableUpdate&>(node);
        return equals(target, that.target) && equals(value, that.value) && equals(next, that.next);
      }

      void VariableUpdate::computeHashCode(HashCode &h) const
      {
        h.mix(typeid(*this));
        h.mix(target);
        h.mix(value);
        h.mix(next);
      }
    }
  }
}
