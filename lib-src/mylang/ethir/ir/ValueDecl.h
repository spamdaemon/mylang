#ifndef CLASS_MYLANG_ETHIR_IR_VALUEDECL_H
#define CLASS_MYLANG_ETHIR_IR_VALUEDECL_H

#ifndef CLASS_MYLANG_ETHIR_IR_AHSTRACTVALUEDECLARATION_H
#include <mylang/ethir/ir/AbstractValueDeclaration.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_EXPRESSION_H
#include <mylang/ethir/ir/Expression.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * A variable declaration that cannot be modified.
       */
      class ValueDecl: public AbstractValueDeclaration
      {
        ValueDecl(const ValueDecl&) = delete;

        ValueDecl& operator=(const ValueDecl&) = delete;

        /** A value pointer */
      public:
        typedef ::std::shared_ptr<const ValueDecl> ValueDeclPtr;

        /**
         * Create a bind statement.
         * @param name a name
         * @param value the value to be bound
         */
      public:
        ValueDecl(Variable::VariablePtr val, Expression::ExpressionPtr value, StatementPtr next);

        /** Destructor */
      public:
        ~ValueDecl();

        /**
         * Declare a mutable variable with the specified initializer
         * @param name a name
         * @param value the value to be bound
         */
      public:
        static ValueDeclPtr create(Variable::VariablePtr val, Expression::ExpressionPtr value,
            StatementPtr next);

        /**
         * Declare a mutable variable with the specified initializer
         * @param name a name
         * @param value the value to be bound
         */
      public:
        static ValueDeclPtr create(const Name &val, Expression::ExpressionPtr value,
            StatementPtr next);

        /**
         * Declare a mutable variable that is not initialized
         * @param name a name
         * @param type of the value to be bound
         */
      public:
        static ValueDeclPtr create(Variable::VariablePtr val, StatementPtr next);

        /**
         * Create a simplfified value declaration, such that the value
         * will not be a let expression
         * @param val the name of the value
         * @param value the value
         * @param next the next statement
         * @return a value decl without let expressions
         */
      public:
        static ValueDeclPtr createSimplified(Variable::VariablePtr val,
            Expression::ExpressionPtr value, StatementPtr next);

      public:
        void accept(NodeVisitor &visitor) const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

      };
    }

  }
}
#endif
