#include <mylang/ethir/ir/LiteralString.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/TypeMismatchError.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      LiteralString::LiteralString(::std::shared_ptr<const types::StringType> xtype,
          ValueType xvalue)
          : LiteralPrimitive(xtype, xvalue), value(xvalue)
      {
      }

      LiteralString::~LiteralString()
      {
      }

      LiteralString::LiteralStringPtr LiteralString::create(
          ::std::shared_ptr<const types::StringType> xtype, ValueType xvalue)
      {
        return ::std::make_shared<LiteralString>(xtype, xvalue);
      }

      LiteralString::LiteralStringPtr LiteralString::create(ValueType xvalue)
      {
        return create(types::StringType::create(), xvalue);
      }

      LiteralString::LiteralStringPtr LiteralString::create(
          ::std::shared_ptr<const types::StringType> xtype, char xvalue)
      {
        return ::std::make_shared<LiteralString>(xtype, ::std::to_string(xvalue));
      }

      LiteralString::LiteralStringPtr LiteralString::create(char xvalue)
      {
        return create(types::StringType::create(), ::std::to_string(xvalue));
      }

      LiteralString::LiteralStringPtr LiteralString::createEmpty()
      {
        return create(types::StringType::create(), "");
      }

      ::std::optional<int> LiteralString::compare(const Literal &obj) const
      {
        auto that = dynamic_cast<const LiteralString*>(&obj);
        if (that == nullptr) {
          return ::std::nullopt;
        }
        if (value < that->value) {
          return -1;
        } else if (value > that->value) {
          return 1;
        } else {
          return 0;
        }
      }

      void LiteralString::accept(NodeVisitor &visitor) const
      {
        visitor.visitLiteralString(self<LiteralString>());
      }
      Literal::LiteralPtr LiteralString::castTo(const EType &newType) const
      {
        TypeMismatchError::check(type, newType);
        return self<Literal>();
      }
      bool LiteralString::isEqual(const Node &node) const
      {
        const LiteralString &that = dynamic_cast<const LiteralString&>(node);
        return equals(type, that.type) && value == that.value;
      }
      void LiteralString::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(value);
      }

    }
  }
}
