#include <mylang/ethir/ir/ToArray.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/types/types.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      ToArray::ToArray(ArrayPtr t, Loop::LoopPtr xloop)
          : Expression(t), loop(xloop)
      {
      }

      ToArray::~ToArray()
      {
      }

      ToArray::ToArrayPtr ToArray::create(ArrayPtr t, Loop::LoopPtr xloop)
      {
        return ::std::make_shared<ToArray>(t, xloop);
      }

      void ToArray::accept(NodeVisitor &visitor) const
      {
        visitor.visitToArray(self<ToArray>());
      }

      bool ToArray::isEqual(const Node &node) const
      {
        const ToArray &that = dynamic_cast<const ToArray&>(node);
        return equals(type, that.type) && equals(loop, that.loop);
      }

      void ToArray::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(loop);
      }
    }
  }
}
