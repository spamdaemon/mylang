#ifndef CLASS_MYLANG_ETHIR_IR_NEWPROCESS_H
#define CLASS_MYLANG_ETHIR_IR_NEWPROCESS_H

#ifndef MYLANGCLASS_MYLANG_ETHIR_NAME_H
#include <mylang/ethir/Name.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_LITERAL_H
#include <mylang/ethir/ir/Literal.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#include <mylang/ethir/types/Type.h>
#endif
#include <vector>

namespace mylang {
  namespace ethir {
    namespace ir {
      class NewProcess: public Expression
      {

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const NewProcess> NewProcessPtr;

        /**
         * Create an expression gets the specified member variable.
         * @param type the return type
         * @param name the name of the process
         * @param ctor the name of the process constructor
         * @param arguments to the constructor
         */
      public:
        NewProcess(TypePtr t, const Name &pname, const Name &ctorName,
            ::std::vector<Variable::VariablePtr> args);

        /**destructor */
      public:
        ~NewProcess();

        /**
         * Create a function call.
         * @param type the return type
         * @param member the member to be initialized
         * @param args the arguments
         * @return afunction call ptr/
         */
      public:
        static NewProcessPtr create(TypePtr t, const Name &pname, const Name &ctorName,
            ::std::vector<Variable::VariablePtr> value);

        void accept(NodeVisitor &visitor) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The name of the process */
      public:
        const Name processName;

        /** The constructor name */
      public:
        const Name constructorName;

        /** The object on which to invoke the method */
      public:
        const ::std::vector<Variable::VariablePtr> arguments;
      };

    }
  }
}
#endif
