#ifndef CLASS_MYLANG_ETHIR_IR_LOOP_H
#define CLASS_MYLANG_ETHIR_IR_LOOP_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_NODE_H
#include <mylang/ethir/ir/Node.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_EXPRESSION_H
#include <mylang/ethir/ir/Expression.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENTBUILDER_H
#include <mylang/ethir/ir/StatementBuilder.h>
#endif

#include <functional>
#include <map>

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * A loop that implements certain kinds of builtin expressions, e.g.
       * map, filter, etc.
       * A loop always has a body, the body of the loop may be split into
       * multiple states. A loop must contain a YieldStatement somewhere
       * to produce the output of the loop.
       */
      class Loop: public Node
      {
        Loop(const Loop &e) = delete;

        Loop& operator=(const Loop &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const Loop> LoopPtr;

        /** A yield handler returns a list of statements that do something with the yielded value. */
      public:
        typedef ::std::function<void(EVariable, StatementBuilder&)> YieldHandler;

        /**
         * The a state is represented by a pointer. A null pointer
         * represents the special state DONE.
         */
      public:
        class State
        {
          friend class Loop;

          /** Default constructor */
        public:
          State() = delete;

          /** Constructor with a known value */
        private:
          inline State(size_t xid)
              : _id(xid)
          {
          }

          /** Compare two states */
        public:
          inline bool operator<(const State &s) const
          {
            return _id < s._id;
          }

        public:
          inline bool operator==(const State &s) const
          {
            return _id == s._id;
          }

        public:
          inline bool operator!=(const State &s) const
          {
            return _id != s._id;
          }

        public:
          friend ::std::ostream& operator<<(::std::ostream &out, const State &s)
          {
            return out << s._id;
          }

        private:
          int64_t _id;
        };

        /** The the special state to indicate that the loop is done */
      public:
        static State STOP_STATE;

        /** The special current state can be used with a skip to remain in the current state */
      public:
        static State CURRENT_STATE;

        /** A map of variables and expressions used to initialize them. */
      public:
        typedef Variable::Map<EExpression> Expressions;

        /** A loop step. A step is executed by the loop with each iteration. Each step
         * is associated with a single loop state.
         */
      public:
        class Step: public Node
        {
          /** A step pointer */
        public:
          typedef ::std::shared_ptr<const Step> Ptr;

          /* Constructor */
        public:
          Step();

          /** Destructor */
        public:
          virtual ~Step();

          /**
           * Turn this step into a statement
           * @return get the statement that implements this step.
           */
        public:
          virtual EStatement toStatement() const = 0;

          /**
           * Get the variables that are managed by this step.
           * These variables are defined before the associated loop
           * is entered.
           */
        public:
          virtual Expressions getInitializers() const = 0;
        };

        /** A map of variables and expressions */
      public:
        typedef ::std::map<State, Step::Ptr> States;

        /**
         * Create a loop.
         * @param init expression to initialize the loop variables
         * @param body body of the loop (one body for each value of the state variable)
         */
      public:
        Loop(State start, States body);

        /** Destructor */
      public:
        ~Loop();

        /**
         * Create a new loop state.
         * @return a new loop state
         */
      public:
        static State newState();

        /**
         * Create a loop with a single state. The two states START and STOP
         * are not allowed to be provided in the body or as the start state.
         * @param initializers initializer expressions
         * @param start the start state
         * @param body body of the loop
         */
      public:
        static LoopPtr create(Step::Ptr body);

        /**
         * Create a loop with a single state.
         * @param initializers initializer expressions
         * @param start the start state
         * @param body body of the loop
         * @param phi expressions evaluated before each dispatch
         * @param phi expressions evaluated after each dispatch
         */
      public:
        static LoopPtr create(State start, States body);

      public:
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &that) const override final;
        void accept(NodeVisitor &visitor) const override final;

      public:
        static bool equals(const Expressions &a, const Expressions &b);
        static void computeHashCode(const Expressions &e, HashCode &hc);

        /**
         * Get the inital step.
         * @return the state that corresponds to the start state
         */
      public:
        Step::Ptr initialStep() const;

        /**
         * Get the initializer expressions
         */
      public:
        Expressions initializers() const;

        /**
         * Convert this loop into statements. Each yield is transformed into a
         * statement using the specified handler.
         * THis method method not be called in if the loop is in SSA form.
         * @param handler a yield handler
         * @param b a statement builder
         */
      public:
        void toStatements(YieldHandler handler, StatementBuilder &b) const;

        /** The initial state */
      public:
        const State start;

        /**
         * The statement to be executed while condition is true
         */
      public:
        const States states;
      };
    }

  }
}
#endif
