#ifndef CLASS_MYLANG_ETHIR_IR_ABSTRACTOPERATORVISITOR_H
#define CLASS_MYLANG_ETHIR_IR_ABSTRACTOPERATORVISITOR_H

#ifndef CLASS_MYLANG_ETHIR_IR_OPERATORVISITOR_H
#include <mylang/ethir/ir/OperatorVisitor.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {
      class AbstractOperatorVisitor: public OperatorVisitor
      {
      public:
        virtual ~AbstractOperatorVisitor() = 0;

      protected:
        void visit_NEW(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_CALL(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_CHECKED_CAST(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_UNCHECKED_CAST(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_NEG(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_ADD(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_SUB(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_MUL(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_DIV(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_MOD(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_REM(OperatorExpression::OperatorExpressionPtr e) override;

        void visit_INDEX(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_SUBRANGE(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_EQ(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_NEQ(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_LT(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_LTE(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_AND(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_OR(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_XOR(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_NOT(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_GET(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_IS_LOGGABLE(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_IS_PRESENT(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_IS_INPUT_NEW(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_IS_INPUT_CLOSED(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_IS_OUTPUT_CLOSED(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_IS_PORT_READABLE(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_IS_PORT_WRITABLE(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_LENGTH(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_CONCATENATE(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_MERGE_TUPLES(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_ZIP(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_BITS_TO_BYTES(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_BYTES_TO_BITS(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_FROM_BITS(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_TO_BITS(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_FROM_BYTES(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_TO_BYTES(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_CLAMP(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_WRAP(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_BYTE_TO_UINT8(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_BYTE_TO_SINT8(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_STRING_TO_CHARS(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_INTERPOLATE_TEXT(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_TO_STRING(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_FOLD(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_PAD(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_TRIM(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_TAKE(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_DROP(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_MAP_ARRAY(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_MAP_OPTIONAL(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_FILTER_ARRAY(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_FILTER_OPTIONAL(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_FIND(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_REVERSE(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_FLATTEN(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_PARTITION(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_HEAD(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_TAIL(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_STRUCT_TO_TUPLE(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_TUPLE_TO_STRUCT(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_BASETYPE_CAST(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_ROOTTYPE_CAST(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_READ_PORT(OperatorExpression::OperatorExpressionPtr e) override;
        void visit_BLOCK_READ(OperatorExpression::OperatorExpressionPtr) override;
        void visit_BLOCK_WRITE(OperatorExpression::OperatorExpressionPtr) override;
        void visit_CLEAR_PORT(OperatorExpression::OperatorExpressionPtr) override;
        void visit_CLOSE_PORT(OperatorExpression::OperatorExpressionPtr) override;
        void visit_WAIT_PORT(OperatorExpression::OperatorExpressionPtr) override;
        void visit_WRITE_PORT(OperatorExpression::OperatorExpressionPtr) override;
        void visit_LOG(OperatorExpression::OperatorExpressionPtr) override;
        void visit_SET(OperatorExpression::OperatorExpressionPtr) override;
        void visit_APPEND(OperatorExpression::OperatorExpressionPtr) override;
        void visit_BUILD(OperatorExpression::OperatorExpressionPtr) override;
      };
    }
  }

}
#endif
