#ifndef CLASS_MYLANG_ETHIR_IR_DECLARATION_H
#define CLASS_MYLANG_ETHIR_IR_DECLARATION_H

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENT_H
#include <mylang/ethir/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_NODE_H
#include <mylang/ethir/ir/Node.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#include <mylang/ethir/types/Type.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_NAME_H
#include <mylang/ethir/Name.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {
      class Declaration: public Statement
      {
        /** The scope of  the declaration */
      public:
        enum Scope
        {
          FUNCTION_SCOPE, PROCESS_SCOPE, GLOBAL_SCOPE
        };

        Declaration(const Declaration &e) = delete;

        Declaration& operator=(const Declaration &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const mylang::ethir::types::Type> TypePtr;

        /** An expression pointer */
      public:
        typedef ::std::shared_ptr<const Declaration> DeclarationPtr;

        /**
         * Create a declaration of that associates a type with a name.
         * @param scope the declaration scope
         * @param name the name of the declaration
         * @param type the type
         */
      protected:
        Declaration(Scope scope, Name name, TypePtr t, StatementPtr next);

        /** Destructor */
      public:
        virtual ~Declaration() = 0;

        /** The scope */
      public:
        const Scope scope;

        /**
         * Get the type of this expression.
         * @return a type
         */
      public:
        const Name name;

        /**
         * Get the type of this expression.
         * @return a type
         */
      public:
        const TypePtr type;
      };
    }
  }
}
#endif
