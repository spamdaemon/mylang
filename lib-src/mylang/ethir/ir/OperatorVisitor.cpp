#include <mylang/ethir/ir/OperatorVisitor.h>

namespace mylang {
  namespace ethir {
    namespace ir {
      OperatorVisitor::~OperatorVisitor()
      {
      }

      void OperatorVisitor::visit(OperatorExpression::OperatorExpressionPtr e)
      {
        switch (e->name) {
        case OperatorExpression::OP_NEW:
          visit_NEW(e);
          break;
        case OperatorExpression::OP_CALL:
          visit_CALL(e);
          break;
        case OperatorExpression::OP_CHECKED_CAST:
          visit_CHECKED_CAST(e);
          break;
        case OperatorExpression::OP_UNCHECKED_CAST:
          visit_UNCHECKED_CAST(e);
          break;
        case OperatorExpression::OP_NEG:
          visit_NEG(e);
          break;
        case OperatorExpression::OP_ADD:
          visit_ADD(e);
          break;
        case OperatorExpression::OP_SUB:
          visit_SUB(e);
          break;
        case OperatorExpression::OP_MUL:
          visit_MUL(e);
          break;
        case OperatorExpression::OP_DIV:
          visit_DIV(e);
          break;
        case OperatorExpression::OP_MOD:
          visit_MOD(e);
          break;
        case OperatorExpression::OP_REM:
          visit_REM(e);
          break;
        case OperatorExpression::OP_INDEX:
          visit_INDEX(e);
          break;
        case OperatorExpression::OP_SUBRANGE:
          visit_SUBRANGE(e);
          break;
        case OperatorExpression::OP_EQ:
          visit_EQ(e);
          break;
        case OperatorExpression::OP_NEQ:
          visit_NEQ(e);
          break;
        case OperatorExpression::OP_LT:
          visit_LT(e);
          break;
        case OperatorExpression::OP_LTE:
          visit_LTE(e);
          break;
        case OperatorExpression::OP_AND:
          visit_AND(e);
          break;
        case OperatorExpression::OP_OR:
          visit_OR(e);
          break;
        case OperatorExpression::OP_XOR:
          visit_XOR(e);
          break;
        case OperatorExpression::OP_NOT:
          visit_NOT(e);
          break;
        case OperatorExpression::OP_GET:
          visit_GET(e);
          break;
        case OperatorExpression::OP_IS_LOGGABLE:
          visit_IS_LOGGABLE(e);
          break;
        case OperatorExpression::OP_IS_PRESENT:
          visit_IS_PRESENT(e);
          break;
        case OperatorExpression::OP_IS_INPUT_NEW:
          visit_IS_INPUT_NEW(e);
          break;
        case OperatorExpression::OP_IS_INPUT_CLOSED:
          visit_IS_INPUT_CLOSED(e);
          break;
        case OperatorExpression::OP_IS_OUTPUT_CLOSED:
          visit_IS_OUTPUT_CLOSED(e);
          break;
        case OperatorExpression::OP_IS_PORT_READABLE:
          visit_IS_PORT_READABLE(e);
          break;
        case OperatorExpression::OP_IS_PORT_WRITABLE:
          visit_IS_PORT_WRITABLE(e);
          break;
        case OperatorExpression::OP_SIZE:
          visit_LENGTH(e);
          break;
        case OperatorExpression::OP_CONCATENATE:
          visit_CONCATENATE(e);
          break;
        case OperatorExpression::OP_MERGE_TUPLES:
          visit_MERGE_TUPLES(e);
          break;
        case OperatorExpression::OP_ZIP:
          visit_ZIP(e);
          break;
        case OperatorExpression::OP_BITS_TO_BYTES:
          visit_BITS_TO_BYTES(e);
          break;
        case OperatorExpression::OP_BYTES_TO_BITS:
          visit_BYTES_TO_BITS(e);
          break;
        case OperatorExpression::OP_FROM_BITS:
          visit_FROM_BITS(e);
          break;
        case OperatorExpression::OP_TO_BITS:
          visit_TO_BITS(e);
          break;
        case OperatorExpression::OP_FROM_BYTES:
          visit_FROM_BYTES(e);
          break;
        case OperatorExpression::OP_TO_BYTES:
          visit_TO_BYTES(e);
          break;
        case OperatorExpression::OP_CLAMP:
          visit_CLAMP(e);
          break;
        case OperatorExpression::OP_WRAP:
          visit_WRAP(e);
          break;
        case OperatorExpression::OP_BYTE_TO_UINT8:
          visit_BYTE_TO_UINT8(e);
          break;
        case OperatorExpression::OP_BYTE_TO_SINT8:
          visit_BYTE_TO_SINT8(e);
          break;
        case OperatorExpression::OP_STRING_TO_CHARS:
          visit_STRING_TO_CHARS(e);
          break;
        case OperatorExpression::OP_INTERPOLATE_TEXT:
          visit_INTERPOLATE_TEXT(e);
          break;
        case OperatorExpression::OP_TO_STRING:
          visit_TO_STRING(e);
          break;
        case OperatorExpression::OP_FOLD:
          visit_FOLD(e);
          break;
        case OperatorExpression::OP_PAD:
          visit_PAD(e);
          break;
        case OperatorExpression::OP_TRIM:
          visit_TRIM(e);
          break;
        case OperatorExpression::OP_DROP:
          visit_DROP(e);
          break;
        case OperatorExpression::OP_TAKE:
          visit_TAKE(e);
          break;
        case OperatorExpression::OP_MAP_ARRAY:
          visit_MAP_ARRAY(e);
          break;
        case OperatorExpression::OP_MAP_OPTIONAL:
          visit_MAP_OPTIONAL(e);
          break;
        case OperatorExpression::OP_FILTER_ARRAY:
          visit_FILTER_ARRAY(e);
          break;
        case OperatorExpression::OP_FILTER_OPTIONAL:
          visit_FILTER_OPTIONAL(e);
          break;
        case OperatorExpression::OP_FIND:
          visit_FIND(e);
          break;
        case OperatorExpression::OP_REVERSE:
          visit_REVERSE(e);
          break;
        case OperatorExpression::OP_FLATTEN:
          visit_FLATTEN(e);
          break;
        case OperatorExpression::OP_PARTITION:
          visit_PARTITION(e);
          break;
        case OperatorExpression::OP_HEAD:
          visit_HEAD(e);
          break;
        case OperatorExpression::OP_TAIL:
          visit_TAIL(e);
          break;
        case OperatorExpression::OP_STRUCT_TO_TUPLE:
          visit_STRUCT_TO_TUPLE(e);
          break;
        case OperatorExpression::OP_TUPLE_TO_STRUCT:
          visit_TUPLE_TO_STRUCT(e);
          break;
        case OperatorExpression::OP_BASETYPE_CAST:
          visit_BASETYPE_CAST(e);
          break;
        case OperatorExpression::OP_ROOTTYPE_CAST:
          visit_ROOTTYPE_CAST(e);
          break;
        case OperatorExpression::OP_READ_PORT:
          visit_READ_PORT(e);
          break;
        case OperatorExpression::OP_BLOCK_READ:
          visit_BLOCK_READ(e);
          break;
        case OperatorExpression::OP_BLOCK_WRITE:
          visit_BLOCK_WRITE(e);
          break;
        case OperatorExpression::OP_CLEAR_PORT:
          visit_CLEAR_PORT(e);
          break;
        case OperatorExpression::OP_CLOSE_PORT:
          visit_CLOSE_PORT(e);
          break;
        case OperatorExpression::OP_WAIT_PORT:
          visit_WAIT_PORT(e);
          break;
        case OperatorExpression::OP_WRITE_PORT:
          visit_WRITE_PORT(e);
          break;
        case OperatorExpression::OP_LOG:
          visit_LOG(e);
          break;
        case OperatorExpression::OP_SET:
          visit_SET(e);
          break;
        case OperatorExpression::OP_APPEND:
          visit_APPEND(e);
          break;
        case OperatorExpression::OP_BUILD:
          visit_BUILD(e);
          break;
        }
      }

    }
  }
}
