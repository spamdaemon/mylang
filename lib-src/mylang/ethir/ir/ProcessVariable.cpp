#include <mylang/ethir/ir/ProcessVariable.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      ProcessVariable::ProcessVariable(Variable::VariablePtr xval, Variable::VariablePtr xexpr)

          : Declaration(xval->scope, xval->name, xval->type, nullptr), variable(xval), value(xexpr)
      {
        assert(!value || value->type->isSameType(*variable->type));
        assert(xval->scope == PROCESS_SCOPE);
      }

      ProcessVariable::~ProcessVariable()
      {
      }

      ProcessVariable::ProcessVariablePtr ProcessVariable::create(Variable::VariablePtr xval,
          Variable::VariablePtr xvalue)
      {
        return ::std::make_shared<ProcessVariable>(xval, xvalue);
      }

      ProcessVariable::ProcessVariablePtr ProcessVariable::create(Variable::VariablePtr xval)
      {
        return ::std::make_shared<ProcessVariable>(xval, nullptr);
      }

      void ProcessVariable::accept(NodeVisitor &visitor) const
      {
        visitor.visitProcessVariable(self<ProcessVariable>());
      }
      Statement::StatementPtr ProcessVariable::replaceNext(StatementPtr) const
      {
        return self<Statement>();
      }
      bool ProcessVariable::isEqual(const Node &node) const
      {
        const ProcessVariable &that = dynamic_cast<const ProcessVariable&>(node);
        return equals(variable, that.variable) && equals(value, that.value)
            && equals(next, that.next);
      }

      void ProcessVariable::computeHashCode(HashCode &h) const
      {
        h.mix(typeid(*this));
        h.mix(variable);
        h.mix(value);
        h.mix(next);
      }
    }

  }

}
