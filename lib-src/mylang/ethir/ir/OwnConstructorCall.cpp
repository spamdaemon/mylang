#include <mylang/ethir/ir/OwnConstructorCall.h>
#include <mylang/ethir/ir/NodeVisitor.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      OwnConstructorCall::OwnConstructorCall(StatementPtr xpre,
          ::std::vector<Variable::VariablePtr> args)
          : Statement(nullptr), pre(xpre), arguments(::std::move(args))
      {
      }

      OwnConstructorCall::~OwnConstructorCall()
      {
      }

      OwnConstructorCall::OwnConstructorCallPtr OwnConstructorCall::create(StatementPtr xpre,
          ::std::vector<Variable::VariablePtr> args)
      {
        return ::std::make_shared<OwnConstructorCall>(xpre, args);
      }

      void OwnConstructorCall::accept(NodeVisitor &visitor) const
      {
        visitor.visitOwnConstructorCall(self<OwnConstructorCall>());
      }
      Statement::StatementPtr OwnConstructorCall::replaceNext(StatementPtr) const
      {
        return self<Statement>();
      }
      bool OwnConstructorCall::isEqual(const Node &node) const
      {
        const OwnConstructorCall &that = dynamic_cast<const OwnConstructorCall&>(node);
        return equals(pre, that.pre)
            && equals(arguments.begin(), arguments.end(), that.arguments.begin(),
                that.arguments.end()) && equals(next, that.next);

      }
      void OwnConstructorCall::computeHashCode(HashCode &h) const
      {
        h.mix(pre);
        h.mix(arguments.begin(), arguments.end());
        h.mix(next);
      }
    }

  }
}
