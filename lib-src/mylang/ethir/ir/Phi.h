#ifndef CLASS_MYLANG_ETHIR_IR_PHI_H
#define CLASS_MYLANG_ETHIR_IR_PHI_H

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#include <vector>

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * The Phi expression magically picks the variables with the
       * correct value from among a set of variables.
       * It's purpose is only to support SSA forms.
       */
      class Phi: public Expression
      {

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const Phi> PhiPtr;

        /** A type pointer */
      public:
        typedef Variable::VariablePtr VariablePtr;

      public:
        typedef ::std::vector<VariablePtr> Arguments;

        /**
         * Create an expression with the specified type.
         * @param type the type
         * @param op the builtin function
         * @param args the arguments
         */
      public:
        Phi(Arguments args);

        /** destructor */
      public:
        ~Phi();

        /**
         * Create a new phi function expression.
         * If the arguments consist only of a single variable,
         * then that variable is returned.
         * @param arg the arguments
         * @return a new phi function or a variable expression.
         * @throws invalid_argument if there are no arguments
         */
      public:
        static ExpressionPtr create(Arguments args);

        /**
         * Create phi function expression with a single value.
         * The purpose of this function is document in the code
         * that a phi expression is created, even if a Phi is
         * not constructed.
         * @param arg the arguments
         * @return the argument
         */
      public:
        static inline ExpressionPtr create(VariablePtr arg)
        {
          return arg;
        }

        /**
         * Get the root name.
         */
      public:
        ::mylang::names::Name::Ptr rootName() const;

        /**
         * Accept a node visitor.
         */
      public:
        void accept(mylang::ethir::ir::NodeVisitor &visitor) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The function arguments */
      public:
        const Arguments arguments;
      };
    }
  }
}
#endif
