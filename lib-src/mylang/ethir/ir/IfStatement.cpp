#include <mylang/ethir/ir/IfStatement.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      IfStatement::IfStatement(Variable::VariablePtr xcond,
          StatementBlock::StatementBlockPtr xiftrue, StatementBlock::StatementBlockPtr xiffalse,
          StatementPtr xnext)
          : ControlFlowStatement(xnext), condition(xcond), iftrue(xiftrue), iffalse(xiffalse)
      {
      }

      IfStatement::~IfStatement()
      {
      }

      IfStatement::IfStatementPtr IfStatement::create(Variable::VariablePtr xcond,
          Statement::StatementPtr xiftrue, Statement::StatementPtr xiffalse, StatementPtr xnext)
      {
        return ::std::make_shared<IfStatement>(xcond, StatementBlock::singleton(xiftrue),
            StatementBlock::singleton(xiffalse), xnext);
      }

      void IfStatement::accept(NodeVisitor &visitor) const
      {
        visitor.visitIfStatement(self<IfStatement>());
      }

      bool IfStatement::isNOP() const
      {
        return iftrue->isNOP() && iffalse->isNOP();
      }
      Statement::StatementPtr IfStatement::replaceNext(StatementPtr tail) const
      {
        return create(condition, iftrue, iffalse, tail);
      }
      bool IfStatement::isEqual(const Node &node) const
      {
        const IfStatement &that = dynamic_cast<const IfStatement&>(node);
        return equals(condition, that.condition) && equals(iftrue, that.iftrue)
            && equals(iffalse, that.iffalse) && equals(next, that.next);
      }
      void IfStatement::computeHashCode(HashCode &h) const
      {
        h.mix(condition);
        h.mix(iftrue);
        h.mix(iffalse);
        h.mix(next);
      }
    }
  }
}
