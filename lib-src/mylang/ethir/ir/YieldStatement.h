#ifndef CLASS_MYLANG_ETHIR_IR_YIELDSTATEMENT_H
#define CLASS_MYLANG_ETHIR_IR_YIELDSTATEMENT_H

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENT_H
#include <mylang/ethir/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_SKIPSTATEMENT_H
#include <mylang/ethir/ir/SkipStatement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_LOOP_H
#include <mylang/ethir/ir/Loop.h>
#endif

#include <optional>

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * Yield a value to a loop consumer. YieldStatements are only allowed within the body of a
       * Loop.
       */
      class YieldStatement: public Statement
      {
        YieldStatement(const YieldStatement &e) = delete;

        YieldStatement& operator=(const YieldStatement &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const YieldStatement> YieldStatementPtr;

        /**
         * A new statement
         * @param value the value to yield
         * @param skip the skip statement to continue the loop
         */
      public:
        YieldStatement(Variable::VariablePtr value, SkipStatement::SkipStatementPtr skip);

        /** Destructor */
      public:
        ~YieldStatement();

        /**
         * Yield a value for one iteration of a loop.
         * @param value the value to produce
         * @param skip the skip statement to continue the loop
         */
      public:
        static YieldStatementPtr create(Variable::VariablePtr value,
            SkipStatement::SkipStatementPtr skip);

      public:
        void accept(NodeVisitor &visitor) const override final;
        EStatement replaceNext(EStatement stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /**
         * The yielded value.
         */
      public:
        const Variable::VariablePtr value;

        /** The skip statement to execute */
      public:
        const SkipStatement::SkipStatementPtr skip;
      };
    }
  }
}
#endif
