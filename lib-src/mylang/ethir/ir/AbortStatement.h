#ifndef CLASS_MYLANG_ETHIR_IR_ABORTSTATEMENT_H
#define CLASS_MYLANG_ETHIR_IR_ABORTSTATEMENT_H

#ifndef CLASS_MYLANG_ETHIR_IR_CONTROLFLOWSTATEMENT_H
#include <mylang/ethir/ir/ControlFlowStatement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * Assign a value to a variable that is in scope.
       */
      class AbortStatement: public ControlFlowStatement
      {
        AbortStatement(const AbortStatement &e) = delete;

        AbortStatement& operator=(const AbortStatement &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const AbortStatement> AbortStatementPtr;

        /**
         * Crea
         * @param name a name
         * @param value the value to be bound
         */
      public:
        AbortStatement(Variable::VariablePtr value);

        /** Destructor */
      public:
        ~AbortStatement();

        /**
         * Updateare a mutable variable with the specified initializer
         * @param message a message to printed
         */
      public:
        static AbortStatementPtr create(Variable::VariablePtr value);

        /**
         * Updateare a mutable variable with the specified initializer
         */
      public:
        static AbortStatementPtr create();

      public:
        void accept(NodeVisitor &visitor) const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /**
         * The bound value (may be null!)
         */
      public:
        const Variable::VariablePtr value;
      };
    }
  }
}
#endif
