#include <mylang/ethir/ir/SequenceStep.h>
#include <mylang/ethir/ir/Loop.h>
#include <mylang/ethir/ir/OperatorExpression.h>
#include <mylang/ethir/ir/ContinueStatement.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/transforms/TransformSkip.h>
#include <mylang/ethir/types/IntegerType.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {
      using IntegerType = types::IntegerType;
      using IntegerTypePtr = types::IntegerType::IntegerTypePtr;
      namespace {

        static IntegerTypePtr ensureCounterType(EVariable iterationCount)
        {
          auto ity = iterationCount->type->self<IntegerType>();
          if (!ity->isUnsigned()) {
            throw new std::invalid_argument("Sequence count must be unsigned");
          }
          return ity;
        }

        static Loop::Expressions initializerExpressions(EVariable counter)
        {
          Loop::Expressions counterInit;
          auto ty = counter->type->self<IntegerType>();
          counterInit[counter] = LiteralInteger::create(ty, 0);
          return counterInit;
        }

        static EExpression terminateExpression(EVariable iterationCount, EVariable counter)
        {
          if (counter) {
            return OperatorExpression::create(OperatorExpression::OP_LTE, iterationCount, counter);
          } else {
            return nullptr;
          }
        }

        static Loop::Expressions updateExpressions(EVariable counter)
        {
          Loop::Expressions counterUpdate;

          auto updateE =
              LetExpression::create(LiteralInteger::create(1),
                  [&](
                      EVariable one) {
                        return LetExpression::create(OperatorExpression::create(OperatorExpression::OP_ADD,counter,one),
                            [&](EVariable c) {
                              return OperatorExpression::create(counter->type,OperatorExpression::OP_CHECKED_CAST, c);
                            });
                      });
          counterUpdate.emplace(counter, updateE);
          return counterUpdate;
        }

      }
      SequenceStep::SequenceStep(SkipStatement::SkipStatementPtr xnextState,
          EVariable xiterationCount, EVariable xcounter, Loop::Expressions xinitializers,
          EStatement xbody)
          : nextState(xnextState), iterationCount(xiterationCount), counter(xcounter),
              initializers(::std::move(xinitializers)), body(xbody)
      {
        assert(nextState && iterationCount && counter && body);
        auto ty = ensureCounterType(iterationCount);
        auto cty = counter->type->self<IntegerType>();
        assert(cty->range.max() == ty->range.max());
      }

      SequenceStep::~SequenceStep()
      {
      }

      SequenceStep::SequenceStepPtr SequenceStep::create(SkipStatement::SkipStatementPtr xnextState,
          EVariable xiterationCount, EVariable xcounter, Loop::Expressions xinitializers,
          EStatement xbody)
      {
        return ::std::make_shared<SequenceStep>(xnextState, xiterationCount, xcounter,
            ::std::move(xinitializers), ::std::move(xbody));
      }

      SequenceStep::SequenceStepPtr SequenceStep::create(EVariable xiterationCount,
          Loop::State xnextState, Builder stepBuilder)
      {
        auto intTy = ensureCounterType(xiterationCount);
        auto counterTy = IntegerType::create(Integer::ZERO(), intTy->range.max());
        auto counter = Variable::createLocal(counterTy);
        StatementBuilder b;
        Loop::Expressions inits = stepBuilder(counter, b);
        return create(SkipStatement::create(xnextState), xiterationCount, counter, inits, b.build());
      }

      SequenceStep::SequenceStepPtr SequenceStep::create(EVariable xiterationCount,
          Builder stepBuilder)
      {
        return create(xiterationCount, Loop::STOP_STATE, ::std::move(stepBuilder));
      }

      bool SequenceStep::isEqual(const Node &node) const
      {
        const SequenceStep &that = dynamic_cast<const SequenceStep&>(node);
        return Loop::equals(initializers, that.initializers)
            && Node::equals(iterationCount, that.iterationCount)
            && Node::equals(counter, that.counter) && Node::equals(body, that.body);
      }

      void SequenceStep::computeHashCode(HashCode &h) const
      {
        Loop::computeHashCode(initializers, h);
        h.mix(iterationCount);
        h.mix(counter);
        h.mix(body);
      }

      void SequenceStep::accept(NodeVisitor &visitor) const
      {
        visitor.visitSequenceStep(self<SequenceStep>());
      }

      Loop::Expressions SequenceStep::getInitializers() const
      {
        Loop::Expressions res = initializerExpressions(counter);
        res.insert(initializers.begin(), initializers.end());
        return res;
      }

      EStatement SequenceStep::toStatement() const
      {
        auto counterTy = counter->type->self<IntegerType>();
        if (counterTy->range.max().is0()) {
          return nextState;
        }

        transforms::TransformSkip::ReplaceSkipFN replaceSkipFN(
            [&](SkipStatement::SkipStatementPtr skip) {
              SkipStatement::SkipStatementPtr res;
              if (skip->nextState!=Loop::STOP_STATE) {
                auto updates = updateExpressions(counter);
                updates.insert(skip->updates.begin(),skip->updates.end());
                res= SkipStatement::create(updates,skip->nextState);
              }
              return res;
            });

        StatementBuilder b;
        auto done = b.declareLocalValue(terminateExpression(iterationCount, counter));
        b.appendIf(done, [&](StatementBuilder &sb) {
          sb.appendSkip(nextState);
        }, [&](StatementBuilder &sb) {
          auto addSkipUpdates = transforms::TransformSkip::replaceSkip(body,replaceSkipFN);
          sb.appendBlock(addSkipUpdates.actual->self<Statement>());
        });

        return b.build();
      }

    }

  }
}
