#include <mylang/ethir/HashCode.h>
#include <mylang/ethir/Transform.h>
#include <mylang/ethir/ir/Loop.h>
#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/ir/SkipStatement.h>
#include <mylang/ethir/ir/Statement.h>
#include <mylang/ethir/ir/StatementBuilder.h>
#include <mylang/ethir/ir/LiteralInteger.h>
#include <mylang/ethir/transforms/TransformSkip.h>
#include <mylang/ethir/prettyPrint.h>
#include <algorithm>
#include <cassert>
#include <map>
#include <memory>
#include <set>
#include <stdexcept>
#include <utility>

namespace mylang {
  namespace ethir {
    namespace ir {

      Loop::Step::Step()
      {
      }

      Loop::Step::~Step()
      {
      }

      Loop::State Loop::STOP_STATE(0);
      Loop::State Loop::CURRENT_STATE(-1);

      Loop::Loop(State xstart, States xbody)
          : start(xstart), states(::std::move(xbody))
      {
      }

      Loop::~Loop()
      {
      }

      Loop::State Loop::newState()
      {
        static int64_t nextID = 0;
        return State(++nextID);
      }

      Loop::LoopPtr Loop::create(State xstart, States xbody)
      {
        Expressions exprs;
        size_t n = 0;
        for (const auto &s : xbody) {
          if (s.first == CURRENT_STATE || s.first == STOP_STATE) {
            throw ::std::runtime_error(
                "CURRENT_STATE and STOP_STATE cannot be used as loop states");
          }
          const auto &e = s.second->getInitializers();
          n += e.size();
          exprs.insert(e.begin(), e.end());
        }
        if (n != exprs.size()) {
          throw ::std::runtime_error("Duplicate initializers in loop steps");
        }

        if (xstart == STOP_STATE || xstart == CURRENT_STATE) {
          throw ::std::runtime_error("Stop or current state cannot be the start state");
        }
        if (xbody.count(STOP_STATE) != 0 || xbody.count(CURRENT_STATE) != 0) {
          throw ::std::runtime_error("Stop or current state cannot have a body");
        }
        if (xbody.count(xstart) == 0) {
          throw ::std::runtime_error("Start state not found");
        }
        // TODO: check that the start state is in the list of states and that we can reach
        // the stop state from the start state
        auto loop = ::std::make_shared<Loop>(xstart, ::std::move(xbody));
        for (auto s : loop->states) {
          transforms::TransformSkip::replaceSkip(s.second,
              [&](
                  SkipStatement::SkipStatementPtr skip) {
                    if (skip->nextState != STOP_STATE && skip->nextState!=CURRENT_STATE) {
                      auto ns = loop->states.find(skip->nextState);
                      if (ns == loop->states.end()) {
                        ::std::cerr << "Failed to locate next state " << skip->nextState << "in " << s.first << ::std::endl;
                        assert(false && "Failed to locate state ");
                      }
                    }
                    return nullptr;
                  });
        }
        return loop;
      }

      Loop::LoopPtr Loop::create(Step::Ptr xbody)
      {
        States b;
        State start = newState();
        b[start] = ::std::move(xbody);
        return create(start, b);
      }

      bool Loop::isEqual(const Node &node) const
      {
        const Loop &that = dynamic_cast<const Loop&>(node);
        if (start != that.start) {
          return false;
        }

        if (states.size() != that.states.size()) {
          return false;
        }

        for (const auto &e : states) {
          auto i = that.states.find(e.first);
          if (i == that.states.end()) {
            return false;
          }
          if (!Node::equals(e.second, i->second)) {
            return false;
          }
        }
        return true;
      }

      void Loop::accept(NodeVisitor &visitor) const
      {
        visitor.visitLoop(self<Loop>());
      }

      void Loop::computeHashCode(HashCode &h) const
      {
        h.mix(states.size());
        for (const auto &e : states) {
          h.mix(e.second);
        }
      }

      bool Loop::equals(const Expressions &a, const Expressions &b)
      {
        if (a.size() != b.size()) {
          return false;
        }
        for (const auto &e : a) {
          auto i = b.find(e.first);
          if (i == b.end()) {
            return false;
          }
          if (!Node::equals(e.second, i->second)) {
            return false;
          }
        }
        return true;
      }
      void Loop::computeHashCode(const Expressions &e, HashCode &h)
      {
        h.mix(e.size());
        for (const auto &i : e) {
          h.mix(i.first);
          h.mix(i.second);
        }
      }

      Loop::Step::Ptr Loop::initialStep() const
      {
        auto i = states.find(start);
        assert(i != states.end());
        return i->second;
      }

      Loop::Expressions Loop::initializers() const
      {
        Expressions exprs;
        for (const auto &s : states) {
          const auto &e = s.second->getInitializers();
          exprs.insert(e.begin(), e.end());
        }
        return exprs;
      }

      void Loop::toStatements(YieldHandler handler, StatementBuilder &b) const
      {
        struct Replace: public Transform
        {
          Replace(Name n, const ::std::map<State, EVariable> &sn, const States &allStates, State s,
              EVariable svar, YieldHandler &h)
              : loop(n), stateNames(sn), states(allStates), current(s), stateVariable(svar),
                  yieldHandler(h)
          {
          }
          ~Replace()
          {
          }

          ENode visit(const ir::Loop::LoopPtr&)
          {
            return nullptr;
          }

          ENode visit(const ir::YieldStatement::YieldStatementPtr &node)
          {
            auto next = transformStatement(node->next);
            StatementBuilder b;
            yieldHandler(node->value, b);
            return b.build(next.actual);
          }

          ENode visit(const ir::SkipStatement::SkipStatementPtr &node)
          {
            auto nextState = node->nextState == CURRENT_STATE ? current : node->nextState;
            StatementBuilder b;
            for (const auto &e : node->updates) {
              auto tmp = b.declareLocalValue(e.second);
              b.appendUpdate(e.first, tmp);
            }
            if (nextState != STOP_STATE) {
              if (node->resetNextState) {
                auto inits = states.find(nextState)->second->getInitializers();
                for (const auto &e : inits) {
                  auto tmp = b.declareLocalValue(e.second);
                  b.appendUpdate(e.first, tmp);
                }
              }
              if (current != nextState) {
                auto ns = stateNames.find(nextState);
                if (ns == stateNames.end()) {
                  ::std::cerr << "Failed to locate next state " << nextState << "("
                      << node->nextState << "), (current state : " << current << ")" << ::std::endl;
                  assert(ns != stateNames.end() && "Failed to locate state ");
                }
                b.appendUpdate(stateVariable, ns->second);
              }
              b.appendContinue(loop);
            } else {
              b.appendBreak(loop);
            }
            return b.build();
          }

          const Name loop;
          const ::std::map<State, EVariable> &stateNames;
          const States states;
          const State current;
          const EVariable stateVariable;
          YieldHandler &yieldHandler;
        };

        Name loopName = Name::create("loopify");

        // we need a var decl for each loop variable
        for (const auto &e : initializers()) {
          auto tmp = b.declareLocalValue(e.second);
          b.declareVariable(e.first, tmp);
        }
        ::std::map<State, EVariable> stateNames;
        auto sTy = types::IntegerType::create(0, states.size() - 1);
        for (const auto &s : states) {
          auto n = LiteralInteger::create(sTy, stateNames.size());
          stateNames[s.first] = b.declareLocalValue(n);
        }
        auto svar = b.declareLocalVariable(stateNames[start]);

        if (states.size() == 1) {
          // replace all yields with whatever the yield handler generates
          // and each transition to STOP with a break
          Replace repl(loopName, stateNames, states, start, svar, handler);
          auto body = repl.transformStatement(states.begin()->second->toStatement());
          b.appendLoop(loopName, body.actual);
        } else {
          b.appendLoop(loopName, [&](StatementBuilder &bb) {
            // TODO: eventually, use a switch statement here!
              for (const auto &s : states) {
                auto inState = bb.declareLocalValue(OperatorExpression::OP_EQ, {svar,
                      stateNames[s.first]});
                Replace repl(loopName, stateNames,states, s.first, svar, handler);
                auto body = repl.transformStatement(s.second->toStatement());
                auto ifTrue = StatementBlock::create(body.actual,
                    ir::ContinueStatement::create(loopName));
                bb.appendIf(inState, ifTrue, nullptr);
              }});
        }

        return;
      }

    }
  }
}
