#ifndef CLASS_MYLANG_ETHIR_IR_OWNCONSTRUCTORCALL_H
#define CLASS_MYLANG_ETHIR_IR_OWNCONSTRUCTORCALL_H

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENT_H
#include <mylang/ethir/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#include <mylang/ethir/types/Type.h>
#endif
#include <vector>

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * This class represents a call to another constructor of the same being constructed.
       * It is not a normal statement and must effectively be the first statement in a constructor,
       * e.g. process constructor.
       */
      class OwnConstructorCall: public Statement
      {
        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const OwnConstructorCall> OwnConstructorCallPtr;

        /**
         * Create a call to a constructor.
         * @param statements to be executed before calling the constructor.
         * @param args the variables to pass to the other constructor
         */
      public:
        OwnConstructorCall(StatementPtr pre, ::std::vector<Variable::VariablePtr> args);

        /**destructor */
      public:
        ~OwnConstructorCall();

        /**
         * Create a function call.
         * @param type the return type
         * @param name the name of the function
         * @param args the arguments
         * @return afunction call ptr/
         */
      public:
        static OwnConstructorCallPtr create(StatementPtr pre,
            ::std::vector<Variable::VariablePtr> args);

        void accept(NodeVisitor &visitor) const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** A statement block */
      public:
        const StatementPtr pre;

        /** The function arguments */
      public:
        const ::std::vector<Variable::VariablePtr> arguments;
      };
    }
  }
}
#endif
