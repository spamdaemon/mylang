#ifndef CLASS_MYLANG_ETHIR_IR_NOVALUE_H
#define CLASS_MYLANG_ETHIR_IR_NOVALUE_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_EXPRESSION_H
#include <mylang/ethir/ir/Expression.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#include <vector>

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * An expression that represent the undefined constant.
       */
      class NoValue: public Expression
      {
        NoValue(const NoValue&) = delete;

        NoValue& operator=(const NoValue&) = delete;

        /** A value pointer */
      public:
        typedef ::std::shared_ptr<const NoValue> NoValuePtr;

        /**
         * Create a NoValue expression of the given type.
         */
      public:
        NoValue(EType t);

        /** Destructor */
      public:
        ~NoValue();

        /**
         * Create an opaque expression.
         * @param e an expression
         * @return an opaque expression
         */
      public:
        static NoValuePtr create(EType t);

      public:
        void accept(NodeVisitor &visitor) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;
      };
    }

  }
}
#endif
