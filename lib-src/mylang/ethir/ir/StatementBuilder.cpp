#include <mylang/ethir/ir/StatementBuilder.h>
#include <mylang/ethir/ir/nodes.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {
      namespace {
        static Statement::StatementPtr makeStatement(StatementBuilder::Builder &b)
        {
          StatementBuilder sb;
          if (b) {
            b(sb);
          }
          return sb.build();
        }
      }

      StatementBuilder::StatementBuilder()
      {
      }

      StatementBuilder::~StatementBuilder()
      {
      }

      void StatementBuilder::append(Producer p)
      {
        if (p) {
          producers.push_back(p);
        }
      }

      void StatementBuilder::comment(::std::string message)
      {
        auto p = [=](ir::Statement::StatementPtr next) {
          return CommentStatement::create(message,next);
        };
        append(p);
      }

      void StatementBuilder::appendBreak(Name scope)
      {
        auto p = [=](ir::Statement::StatementPtr next) {
          if (next) {
            ::std::cerr << "Unreachable statement after break" << ::std::endl;
          }
          return BreakStatement::create(scope);
        };
        append(p);
      }

      void StatementBuilder::appendContinue(Name scope)
      {
        auto p = [=](ir::Statement::StatementPtr next) {
          if (next) {
            ::std::cerr << "Unreachable statement after continue" << ::std::endl;
          }
          return ContinueStatement::create(scope);
        };
        append(p);
      }

      void StatementBuilder::appendSkip(SkipStatement::SkipStatementPtr skip)
      {
        auto p = [=](ir::Statement::StatementPtr next) {
          if (next) {
            ::std::cerr << "Unreachable statement after skip" << ::std::endl;
          }
          return skip;
        };
        append(p);
      }

      void StatementBuilder::appendStop()
      {
        appendSkip(SkipStatement::createStop());
      }

      void StatementBuilder::appendYield(YieldStatement::YieldStatementPtr yield)
      {
        auto p = [=](ir::Statement::StatementPtr next) {
          if (next) {
            ::std::cerr << "Unreachable statement after yield" << ::std::endl;
          }
          return yield;
        };
        append(p);
      }

      void StatementBuilder::appendForEach(Name loopname, Variable::VariablePtr loopvar,
          Variable::VariablePtr loopdata, Statement::StatementPtr body)
      {
        auto p = [=](ir::Statement::StatementPtr next) {
          return ForeachStatement::create(loopname,loopvar,loopdata,body,next);
        };
        append(p);
      }
      void StatementBuilder::appendForEach(Name loopname, Variable::VariablePtr loopvar,
          Variable::VariablePtr loopdata, Builder bodyBuilder)
      {
        auto body = makeStatement(bodyBuilder);
        return appendForEach(loopname, loopvar, loopdata, body);
      }

      void StatementBuilder::appendIf(Variable::VariablePtr cond, Statement::StatementPtr iftrue,
          Statement::StatementPtr iffalse)
      {
        auto p = [=](ir::Statement::StatementPtr next) {
          return IfStatement::create(cond,iftrue,iffalse,next);
        };
        append(p);
      }

      void StatementBuilder::appendIf(Variable::VariablePtr cond, Builder buildIftrue,
          Builder buildIffalse)
      {
        auto iftrue = makeStatement(buildIftrue);
        auto iffalse = makeStatement(buildIffalse);
        return appendIf(cond, iftrue, iffalse);
      }

      void StatementBuilder::appendBlock(Statement::StatementPtr statements)
      {
        auto p = [=](ir::Statement::StatementPtr next) {
          return StatementBlock::create(statements,next);
        };
        append(p);
      }

      void StatementBuilder::appendBlock(Builder builder)
      {
        auto b = makeStatement(builder);
        appendBlock(b);
      }

      void StatementBuilder::appendLoop(Name loopname, Statement::StatementPtr body)
      {
        auto p = [=](ir::Statement::StatementPtr next) {
          return LoopStatement::create(loopname, body,next);
        };
        append(p);
      }

      void StatementBuilder::appendLoop(Name loopname, Builder bodyBuilder)
      {
        auto body = makeStatement(bodyBuilder);
        return appendLoop(loopname, body);
      }

      void StatementBuilder::appendReturn(Variable::VariablePtr value)
      {
        auto p = [=](ir::Statement::StatementPtr next) {
          if (next) {
            ::std::cerr << "Unreachable statement after return" << ::std::endl;
          }
          return ReturnStatement::create(value);
        };
        append(p);
      }

      void StatementBuilder::appendAbort(const ::std::string &value)
      {
        auto message = declareLocalValue(ir::LiteralString::create(value));
        auto p = [=](ir::Statement::StatementPtr next) {
          if (next) {
            ::std::cerr << "Unreachable statement after abort" << ::std::endl;
          }
          return AbortStatement::create(message);
        };
        append(p);
      }

      void StatementBuilder::appendAbort(Variable::VariablePtr value)
      {
        auto p = [=](ir::Statement::StatementPtr next) {
          if (next) {
            ::std::cerr << "Unreachable statement after abort" << ::std::endl;
          }
          return AbortStatement::create(value);
        };
        append(p);
      }

      void StatementBuilder::appendThrow(Variable::VariablePtr value)
      {
        auto p = [=](ir::Statement::StatementPtr next) {
          if (next) {
            ::std::cerr << "Unreachable statement after throw" << ::std::endl;
          }
          return ThrowStatement::create(value);
        };
        append(p);
      }

      void StatementBuilder::appendTryCatch(Statement::StatementPtr tryBody,
          Statement::StatementPtr catchBody)
      {
        auto p = [=](ir::Statement::StatementPtr next) {
          return TryCatch::create(tryBody, catchBody, next);
        };
        append(p);
      }

      void StatementBuilder::appendTryCatch(Builder tryBodyBuilder, Builder catchBodyBuilder)
      {
        auto tryBody = makeStatement(tryBodyBuilder);
        auto tryCatch = makeStatement(catchBodyBuilder);
        return appendTryCatch(tryBody, tryCatch);
      }

      void StatementBuilder::appendCallOwnConstrutor(Statement::StatementPtr setup,
          ::std::vector<Variable::VariablePtr> args)
      {
        auto p = [=](ir::Statement::StatementPtr next) {
          if (next) {
            ::std::cerr << "Unreachable statement after own-ctor call" << ::std::endl;
          }
          return OwnConstructorCall::create(setup, args);
        };
        append(p);
      }

      void StatementBuilder::appendUpdate(Variable::VariablePtr target, Variable::VariablePtr value)
      {
        if (target->type->isSameType(*value->type) == false) {
          ::std::cerr << "Incompatible types : " << target->type->toString() << ", "
              << value->type->toString() << ::std::endl;
          assert(
              target->type->isSameType(*value->type) && "Incompatible types for variable update");
        }

        if (target->scope == Declaration::Scope::PROCESS_SCOPE) {
          auto p = [=](ir::Statement::StatementPtr next) {
            return ProcessVariableUpdate::create(target,value,next);
          };
          append(p);
        } else {
          auto p = [=](ir::Statement::StatementPtr next) {
            return VariableUpdate::create(target,value,next);
          };
          append(p);
        }
      }

      Variable::VariablePtr StatementBuilder::declareValue(Variable::VariablePtr val,
          Expression::ExpressionPtr value)
      {

        if (value && val->type->isSameType(*value->type) == false) {
          ::std::cerr << "Incompatible types : " << val->type->toString() << ", "
              << value->type->toString() << ::std::endl;
          assert((!value || val->type->isSameType(*value->type)) && "Incompatible types");
        }
        auto p = [=](ir::Statement::StatementPtr next) {
          return ValueDecl::createSimplified(val,value,next);
        };
        append(p);
        return val;
      }

      Variable::VariablePtr StatementBuilder::declareVariable(Variable::VariablePtr val,
          Variable::VariablePtr value)
      {
        if (value && val->type->isSameType(*value->type) == false) {
          ::std::cerr << "Incompatible types : " << val->type->toString() << ", "
              << value->type->toString() << ::std::endl;
          assert((!value || val->type->isSameType(*value->type)) && "Incompatible types");
        }
        auto p = [=](ir::Statement::StatementPtr next) {
          return VariableDecl::create(val,value,next);
        };
        append(p);
        return val;
      }

      Variable::VariablePtr StatementBuilder::declareProcessValue(Expression::ExpressionPtr value)
      {
        auto name = Name::create("process");
        auto var = Variable::create(Variable::Scope::PROCESS_SCOPE, value->type, name);
        return declareValue(var, value);
      }

      Variable::VariablePtr StatementBuilder::declareProcessValue(
          mylang::ethir::types::Type::Ptr type)
      {
        auto name = Name::create("process");
        auto var = Variable::create(Variable::Scope::PROCESS_SCOPE, type, name);
        return declareValue(var, nullptr);
      }

      Variable::VariablePtr StatementBuilder::declareProcessVariable(Variable::VariablePtr value)
      {
        auto name = Name::create("process");
        auto var = Variable::create(Variable::Scope::PROCESS_SCOPE, value->type, name);
        return declareVariable(var, value);
      }

      Variable::VariablePtr StatementBuilder::declareLocalValue(Expression::ExpressionPtr value)
      {
        auto name = Name::create("local");
        auto var = Variable::create(Variable::Scope::FUNCTION_SCOPE, value->type, name);
        return declareValue(var, value);
      }

      Variable::VariablePtr StatementBuilder::declareLocalValue(const EType &retTy,
          OperatorExpression::Operator op, const OperatorExpression::Arguments &args)
      {
        return declareLocalValue(OperatorExpression::create(retTy, op, args));
      }

      Variable::VariablePtr StatementBuilder::declareLocalValue(OperatorExpression::Operator op,
          const OperatorExpression::Arguments &args)
      {
        return declareLocalValue(OperatorExpression::create(op, args));
      }

      Variable::VariablePtr StatementBuilder::declareLocalValue(
          mylang::ethir::types::Type::Ptr type)
      {
        auto name = Name::create("local");
        auto var = Variable::create(Variable::Scope::FUNCTION_SCOPE, type, name);
        return declareValue(var, nullptr);
      }

      Variable::VariablePtr StatementBuilder::declareLocalValue(const Name &name,
          Expression::ExpressionPtr value)
      {
        auto v = Variable::create(Variable::Scope::FUNCTION_SCOPE, value->type, name);
        return declareValue(v, value);
      }

      Variable::VariablePtr StatementBuilder::declareLocalVariable(const Name &name,
          Variable::VariablePtr value)
      {
        auto var = Variable::create(Variable::Scope::FUNCTION_SCOPE, value->type, name);
        return declareVariable(var, value);
      }

      Variable::VariablePtr StatementBuilder::declareLocalVariable(Variable::VariablePtr value)
      {
        return declareLocalVariable(Name::create("local"), value);
      }

      Variable::VariablePtr StatementBuilder::declareLocalVariable(const Name &name,
          mylang::ethir::types::Type::Ptr type)
      {
        auto var = Variable::create(Variable::Scope::FUNCTION_SCOPE, type, name);
        declareVariable(var, nullptr);
        return var;
      }

      Variable::VariablePtr StatementBuilder::declareLocalVariable(
          mylang::ethir::types::Type::Ptr type)
      {
        return declareLocalVariable(Name::create("local"), type);
      }

      Variable::VariablePtr StatementBuilder::declareLocalLambda(
          ::std::vector<mylang::ethir::types::Type::Ptr> paramTypes, LambdaProducer lambdaFN)
      {
        LambdaParameters params;
        for (size_t i = 0; i < paramTypes.size(); ++i) {
          auto arg = Name::create("arg" + std::to_string(i));
          params.push_back(Parameter::create(arg, paramTypes.at(i)));
        }
        auto lambda = lambdaFN(::std::move(params));
        return declareLocalValue(lambda);
      }

      Variable::VariablePtr StatementBuilder::declareLocalLambda(
          ::std::vector<mylang::ethir::types::Type::Ptr> paramTypes, LambdaBuilder lambda)
      {
        auto producer = [&](LambdaParameters params) {
          StatementBuilder b;
          ::std::vector<EVariable> args;
          for (auto p : params) {
            args.push_back(p->variable);
          }
          auto retTy = lambda(b,::std::move(args));
          return LambdaExpression::create(params,retTy,b.build());
        };

        return declareLocalLambda(paramTypes, producer);
      }

      Statement::StatementPtr StatementBuilder::build(Statement::StatementPtr next)
      {
        Statement::StatementPtr res = next;
        for (size_t i = producers.size(); i-- > 0;) {
          const Producer &p = producers.at(i);
          res = p(res);
        }
        producers.clear();
        return res;
      }

    }
  }
}
