#include <mylang/ethir/ir/LiteralBit.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/TypeMismatchError.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      LiteralBit::LiteralBit(::std::shared_ptr<const types::BitType> xtype, ValueType xvalue)
          : LiteralPrimitive(xtype, xvalue ? "1" : "0"), value(xvalue)
      {
      }

      LiteralBit::~LiteralBit()
      {
      }

      LiteralBit::LiteralBitPtr LiteralBit::create(::std::shared_ptr<const types::BitType> xtype,
          ValueType xvalue)
      {
        return ::std::make_shared<LiteralBit>(xtype, xvalue);
      }

      LiteralBit::LiteralBitPtr LiteralBit::create(ValueType xvalue)
      {
        return create(types::BitType::create(), xvalue);
      }

      void LiteralBit::accept(NodeVisitor &visitor) const
      {
        visitor.visitLiteralBit(self<LiteralBit>());
      }
      ::std::optional<int> LiteralBit::compare(const Literal &obj) const
      {
        auto that = dynamic_cast<const LiteralBit*>(&obj);
        if (that == nullptr) {
          return ::std::nullopt;
        }
        if (value == that->value) {
          return 0;
        } else {
          return value ? 1 : -1;
        }
      }
      Literal::LiteralPtr LiteralBit::castTo(const EType &newType) const
      {
        TypeMismatchError::check(type, newType);
        return self<Literal>();
      }
      bool LiteralBit::isEqual(const Node &node) const
      {
        const LiteralBit &that = dynamic_cast<const LiteralBit&>(node);
        return equals(type, that.type) && value == that.value;
      }
      void LiteralBit::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(value);
      }

    }
  }
}
