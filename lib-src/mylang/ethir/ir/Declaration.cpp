#include <mylang/ethir/ir/Declaration.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      Declaration::Declaration(Scope xscope, Name xname, TypePtr xtype, StatementPtr xnext)
          : Statement(xnext), scope(xscope), name(xname), type(xtype)
      {
      }

      Declaration::~Declaration()
      {
      }
    }
  }
}
