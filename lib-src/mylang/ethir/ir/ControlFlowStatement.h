#ifndef CLASS_MYLANG_ETHIR_IR_CONTROLFLOWSTATEMENT_H
#define CLASS_MYLANG_ETHIR_IR_CONTROLFLOWSTATEMENT_H

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENT_H
#include <mylang/ethir/ir/Statement.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * Assign a value to a variable that is in scope.
       */
      class ControlFlowStatement: public Statement
      {
        ControlFlowStatement(const ControlFlowStatement &e) = delete;

        ControlFlowStatement& operator=(const ControlFlowStatement &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const ControlFlowStatement> ControlFlowStatementPtr;

        /**
         * Constructor.
         * @param next the statement following the control flow statement.
         */
      protected:
        ControlFlowStatement(StatementPtr next);

        /** Destructor */
      public:
        ~ControlFlowStatement();
      };
    }
  }
}
#endif
