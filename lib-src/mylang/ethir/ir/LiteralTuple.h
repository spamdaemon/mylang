#ifndef CLASS_MYLANG_ETHIR_IR_LITERALTUPLE_H
#define CLASS_MYLANG_ETHIR_IR_LITERALTUPLE_H

#ifndef CLASS_MYLANG_ETHIR_IR_LITERAL_H
#include <mylang/ethir/ir/Literal.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_TUPLETYPE_H
#include <mylang/ethir/types/TupleType.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * This class represent the constructor for a literal.
       */
      class LiteralTuple: public Literal
      {
        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const LiteralTuple> LiteralTuplePtr;

        /** The elements */
      public:
        typedef ::std::vector<Literal::LiteralPtr> Elements;

        /**
         * Create an expression that returns a literal value.
         * @param type the type
         * @aparam value the Tuple value for the literal
         */
      public:
        LiteralTuple(::std::shared_ptr<const types::TupleType> type, const Elements &elements);

        /** Destructor */
      public:
        ~LiteralTuple();

        /**
         * Create an bit literal
         * @param value true or false
         * @return a literal
         */
      public:
        static LiteralTuplePtr create(::std::shared_ptr<const types::TupleType> type,
            const Elements &elements);

      public:
        void accept(NodeVisitor &visitor) const override final;
        ::std::optional<int> compare(const Literal &that) const override;
        LiteralPtr castTo(const EType &newType) const override;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The Int64 value */
      public:
        const Elements elements;
      };
    }

  }
}
#endif
