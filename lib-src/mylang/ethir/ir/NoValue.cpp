#include <mylang/ethir/ir/NoValue.h>
#include <mylang/ethir/ir/NodeVisitor.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      NoValue::NoValue(EType t)
          : Expression(t)
      {
      }

      NoValue::~NoValue()
      {
      }

      NoValue::NoValuePtr NoValue::create(EType t)
      {
        return ::std::make_shared<NoValue>(t);
      }

      void NoValue::accept(NodeVisitor &visitor) const
      {
        visitor.visitNoValue(self<NoValue>());
      }
      bool NoValue::isEqual(const Node &node) const
      {
        const NoValue &that = dynamic_cast<const NoValue&>(node);
        return type->isSameType(*that.type);
      }

      void NoValue::computeHashCode(HashCode &h) const
      {
        h.mix(type);
      }

    }

  }
}
