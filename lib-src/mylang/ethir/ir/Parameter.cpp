#include <mylang/ethir/ir/Parameter.h>
#include <mylang/ethir/ir/NodeVisitor.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      Parameter::Parameter(Name xname, TypePtr xtype)
          : variable(Variable::create(ir::Declaration::FUNCTION_SCOPE, xtype, xname))
      {
      }

      Parameter::~Parameter()
      {
      }

      Parameter::ParameterPtr Parameter::create(Name xname, TypePtr xtype)
      {
        return ::std::make_shared<Parameter>(xname, xtype);
      }

      void Parameter::accept(NodeVisitor &visitor) const
      {
        visitor.visitParameter(self<Parameter>());
      }
      bool Parameter::isEqual(const Node &node) const
      {
        const Parameter &that = dynamic_cast<const Parameter&>(node);
        return equals(variable, that.variable);
      }
      void Parameter::computeHashCode(HashCode &h) const
      {
        h.mix(typeid(*this));
        h.mix(variable);
      }

    }
  }
}
