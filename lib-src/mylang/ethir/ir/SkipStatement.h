#ifndef CLASS_MYLANG_ETHIR_IR_SKIPSTATEMENT_H
#define CLASS_MYLANG_ETHIR_IR_SKIPSTATEMENT_H

#ifndef CLASS_MYLANG_ETHIR_IR_CONTROLFLOWSTATEMENT_H
#include <mylang/ethir/ir/ControlFlowStatement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_LOOP_H
#include <mylang/ethir/ir/Loop.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * A skip statement is applies updates to the state of a loop
       * and continues the loop.
       */
      class SkipStatement: public ControlFlowStatement
      {
        SkipStatement(const SkipStatement &e) = delete;

        SkipStatement& operator=(const SkipStatement &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const SkipStatement> SkipStatementPtr;

        /**
         * A new skip statement
         * @param any updates to apply to the loop variables
         * @param nextState the next state or the special state Loop::CURRENT_STATE
         * @param resetNext reset the next state before going to the next step
         */
      public:
        SkipStatement(Loop::Expressions updates, Loop::State nextState, bool resetNextState);

        /** Destructor */
      public:
        ~SkipStatement();

        /**
         * Create a skip statement that applies the specified updates before
         * into a new state or the same state.
         * @param updates any updates to apply before entering into the new state
         * @param nextState the next state of the enclosing loop the next state
         * @param resetNextState if true reset the variables associated with the next state
         */
      public:
        static SkipStatementPtr create(Loop::Expressions updates, Loop::State nextState,
            bool resetNextState = false);

        /**
         * Create a skip statement that applies the specified updates before
         * into a new state or the same state.
         * @param nextState the next state of the enclosing loop the next state
         * @param resetNextState if true reset the variables associated with the next state
         */
      public:
        static SkipStatementPtr create(Loop::State nextState, bool resetNextState = false);

        /**
         * Create the state skip to the done state of a loop.
         */
      public:
        static SkipStatementPtr createStop();

      public:
        void accept(NodeVisitor &visitor) const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The update expressions */
      public:
        const Loop::Expressions updates;

        /** The next state */
      public:
        const Loop::State nextState;

        /** Reset the next state before jumping to it */
      public:
        const bool resetNextState;
      };
    }
  }
}
#endif
