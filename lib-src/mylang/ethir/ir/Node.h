#ifndef CLASS_MYLANG_ETHIR_IR_NODE_H
#define CLASS_MYLANG_ETHIR_IR_NODE_H

#ifndef FILE_MYLANG_ETHIR_HASHCODE_H
#include <mylang/ethir/HashCode.h>
#endif

#include <memory>
#include <optional>

namespace mylang {
  namespace ethir {
    namespace ir {

      class NodeVisitor;

      class Node: public ::std::enable_shared_from_this<Node>
      {
        Node(const Node &e) = delete;

        Node& operator=(const Node &e) = delete;

        /** A pointer to a node */
      public:
        typedef ::std::shared_ptr<const Node> NodePtr;

        /** Constructor */
      public:
        Node();

        /** Destructor */
      public:
        virtual ~Node() = 0;

        /**
         * Get the hashcode for this node.
         * @return the hashcode for this node
         */
      public:
        const HashCode& hashcode() const;

        /**
         * Check if this node equals the specified node
         * @param other
         * @return true if this node compares as the node by value
         */
      public:
        bool equals(const NodePtr &ptr) const;

        /**
         * Compare two nodes for equality.
         */
        static bool equals(const NodePtr &a, const NodePtr &b);

        /**
         * Compare two nodes for equality.
         */
        static bool equals(const EType &a, const EType &b);

        template<class T, class U>
        inline static bool equals(T i, U iend, T j, U jend)
        {
          while (i != iend && j != jend) {
            if (!equals(*i, *j)) {
              return false;
            }
            ++i;
            ++j;
          }
          return i == iend && j == jend;
        }

        /**
         * Accept the specified node visitor
         * @param v a visitor
         * @return a node pointer, or nullptr if the node has not changed
         */
      public:
        virtual void accept(NodeVisitor &visitor) const = 0;

        /**
         * Compute the hashcode for this node. The default hashcode hashes the address of this
         * object.
         * @param hc a hashcode to mix the node into
         */
      protected:
        virtual void computeHashCode(HashCode &h) const;

        /**
         * Test if this node compares as equal to the specified node. Before invoking
         * this method, the hashcode has already been compared as equal.
         * @param that a node that is instance of the same type as this node
         * @return true if this node and other should be considered equal.
         */
      protected:
        virtual bool isEqual(const Node &that) const;

        /**
         * Get this node casted to a derived node.
         * @return a pointer to this node or nullptr if not of the requested type
         */
      public:
        template<class T = Node>
        inline ::std::shared_ptr<const T> self() const
        {
          return ::std::dynamic_pointer_cast<const T>(shared_from_this());
        }

        /** The hashcode */
      private:
        mutable ::std::optional<HashCode> hc;

      };
    }
  }
}
#endif
