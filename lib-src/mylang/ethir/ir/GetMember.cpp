#include <mylang/ethir/ir/GetMember.h>
#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <algorithm>
#include <memory>
#include <string>

namespace mylang {
  namespace ethir {
    namespace ir {

      GetMember::GetMember(TypePtr t, Variable::VariablePtr obj, ::std::string xname)
          : Expression(::std::move(t)), object(::std::move(obj)), name(::std::move(xname))
      {
      }

      GetMember::~GetMember()
      {
      }

      GetMember::GetMemberPtr GetMember::create(TypePtr t, Variable::VariablePtr obj,
          ::std::string xname)
      {
        return ::std::make_shared<GetMember>(t, obj, xname);
      }

      GetMember::GetMemberPtr GetMember::create(TypePtr t, Variable::VariablePtr obj, size_t i)
      {
        if (obj->type->self<types::StructType>()) {
          auto ty = obj->type->self<types::StructType>();
          return create(t, obj, ty->members.at(i).name);
        }
        if (obj->type->self<types::TupleType>()) {
          auto ty = obj->type->self<types::TupleType>();
          return create(t, obj, ty->tuple.at(i).name);
        }
        if (obj->type->self<types::UnionType>()) {
          auto ty = obj->type->self<types::UnionType>();
          return create(t, obj, ty->members.at(i).name);
        }
        throw ::std::runtime_error("Unexpected object type");
      }

      void GetMember::accept(NodeVisitor &visitor) const
      {
        visitor.visitGetMember(self<GetMember>());
      }
      bool GetMember::isEqual(const Node &node) const
      {
        const GetMember &that = dynamic_cast<const GetMember&>(node);
        return equals(type, that.type) && equals(object, that.object) && name == name;
      }

      void GetMember::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(object);
        h.mix(name);
      }

    }
  }
}
