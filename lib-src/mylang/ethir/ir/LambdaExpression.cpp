#include <mylang/ethir/ethir.h>
#include <mylang/ethir/HashCode.h>
#include <mylang/ethir/ir/LambdaExpression.h>
#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/ir/Parameter.h>
#include <mylang/ethir/ir/ReturnStatement.h>
#include <mylang/ethir/ir/StatementBlock.h>
#include <mylang/ethir/ir/Variable.h>
#include <mylang/ethir/ir/DefaultNodeVisitor.h>
#include <mylang/ethir/Name.h>
#include <mylang/ethir/types/FunctionType.h>
#include <mylang/ethir/types/Type.h>
#include <mylang/ethir/types/VoidType.h>
#include <mylang/ethir/Tag.h>
#include <mylang/ethir/TypeMismatchError.h>
#include <stddef.h>
#include <cassert>
#include <memory>
#include <stdexcept>
#include <vector>

namespace mylang {
  namespace ethir {
    namespace ir {

      LambdaExpression::LambdaExpression(const Tag &xname,
          ::std::shared_ptr<const types::FunctionType> signature, Parameters xparameters,
          StatementBlock::StatementBlockPtr xbody)
          : Expression(signature), tag(xname), parameters(xparameters), body(xbody)
      {
        assert(parameters.size() == signature->parameters.size());
        for (size_t i = 0; i < parameters.size(); ++i) {
          TypeMismatchError::check(signature->parameters.at(i), parameters.at(i)->variable->type);
        }
        if (xbody->next) {
          throw ::std::invalid_argument("Lambda body has a next statement");
        }
      }

      LambdaExpression::~LambdaExpression()
      {
      }

      LambdaExpression::LambdaExpressionPtr LambdaExpression::create(const Tag &xname,
          ::std::shared_ptr<const types::FunctionType> signature, Parameters xparameters,
          Statement::StatementPtr xbody)
      {
        return ::std::make_shared<LambdaExpression>(xname, signature, xparameters,
            StatementBlock::singleton(xbody));
      }

      LambdaExpression::LambdaExpressionPtr LambdaExpression::create(
          ::std::shared_ptr<const types::FunctionType> signature, Parameters xparameters,
          Statement::StatementPtr xbody)
      {
        Tag xname = Tag::create("f");
        return create(xname, signature, xparameters, xbody);
      }

      LambdaExpression::LambdaExpressionPtr LambdaExpression::create(const Tag &xname,
          Parameters xparameters, types::Type::Ptr retTy, Statement::StatementPtr xbody)
      {

        types::FunctionType::Parameters params;
        for (auto p : xparameters) {
          params.push_back(p->variable->type);
        }
        auto fnTy = types::FunctionType::get(retTy, params);
        return create(xname, fnTy, xparameters, xbody);
      }

      LambdaExpression::LambdaExpressionPtr LambdaExpression::create(Parameters xparameters,
          types::Type::Ptr retTy, Statement::StatementPtr xbody)
      {
        return create(Tag::create("lambda"), xparameters, retTy, xbody);
      }

      LambdaExpression::LambdaExpressionPtr LambdaExpression::create(
          const ::std::vector<EType> &params, Builder b)
      {
        Parameters xparameters;
        Arguments args;
        for (auto t : params) {
          auto p = Parameter::create(Name::create("lambdaParam"), t);
          xparameters.push_back(p);
          args.push_back(p->variable);
        }
        StatementBuilder sb;
        auto retTy = b(::std::move(args), sb);
        return create(xparameters, retTy, sb.build());
      }

      LambdaExpression::LambdaExpressionPtr LambdaExpression::createIdentity(types::Type::Ptr retTy)
      {
        auto argName = Name::create("arg");
        auto arg = Parameter::create(argName, retTy);

        types::FunctionType::Parameters params;
        params.push_back(arg->variable->type);
        auto fnTy = types::FunctionType::get(arg->variable->type, params);
        auto body = ir::ReturnStatement::create(arg->variable);
        auto xname = Tag::create("identity");
        return create(xname, fnTy, { arg }, body);
      }

      bool LambdaExpression::isIdentity() const
      {
        // only one argument allowed
        if (parameters.size() != 1) {
          return false;
        }
        // there needs to be a single statement
        if (body->statements == nullptr) {
          return false;
        }

        // make sure return type matches the argument type
        auto fnTy = type->self<types::FunctionType>();
        if (!fnTy->parameters.at(0)->isSameType(*fnTy->returnType)) {
          return false;
        }
        // statement must be a return statement
        auto tmp = body->statements->skipComments();
        if (!tmp) {
          return false;
        }
        auto stmt = tmp->self<ir::ReturnStatement>();
        if (stmt == nullptr) {
          return false;
        }
        // the return value must be the parameter
        auto arg = stmt->value;
        auto p0 = parameters.at(0);
        if (arg->name != p0->variable->name) {
          return false;
        }

        return true;
      }

      void LambdaExpression::accept(NodeVisitor &visitor) const
      {
        visitor.visitLambdaExpression(self<LambdaExpression>());
      }
      bool LambdaExpression::isEqual(const Node &node) const
      {
        const LambdaExpression &that = dynamic_cast<const LambdaExpression&>(node);
        return tag == that.tag && equals(type, that.type)
            && equals(parameters.begin(), parameters.end(), that.parameters.begin(),
                that.parameters.end()) && equals(body, that.body);
      }
      void LambdaExpression::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(tag);
        h.mix(parameters.begin(), parameters.end());
        h.mix(body);
      }

      bool LambdaExpression::isNormalized() const
      {
        // a visitor to count the number of returns in an lambda
        struct CountReturns: public DefaultNodeVisitor
        {
          CountReturns()
              : nReturns(0)
          {
          }
          virtual ~CountReturns() = default;

          void visitLambdaExpression(LambdaExpression::LambdaExpressionPtr) override final
          {
            // do not recurse into a lambda
          }

          void visitReturnStatement(ReturnStatement::ReturnStatementPtr) override final
          {
            ++nReturns;
          }

          size_t nReturns;
        };

        // quick check to see if the lambda's body ends with a return statement
        auto returnStmt = body->lastStatement()->self<ReturnStatement>();
        if (!returnStmt) {
          return false;
        }

        CountReturns v;
        v.visitNode(body);

        return returnStmt && v.nReturns == 1;
      }

    }

  }
}
