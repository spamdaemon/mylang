#include <mylang/ethir/ir/ProcessVariableUpdate.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      ProcessVariableUpdate::ProcessVariableUpdate(Variable::VariablePtr xtarget,
          Variable::VariablePtr xexpr, StatementPtr xnext)
          : Statement(xnext), target(xtarget), value(xexpr)
      {
        if (target->scope != Declaration::Scope::PROCESS_SCOPE) {
          throw ::std::invalid_argument("Variable is not in process scope");
        }
        assert(target);
        assert(value);
      }

      ProcessVariableUpdate::~ProcessVariableUpdate()
      {
      }

      ProcessVariableUpdate::ProcessVariableUpdatePtr ProcessVariableUpdate::create(
          Variable::VariablePtr xtarget, Variable::VariablePtr xvalue, StatementPtr xnext)
      {
        return ::std::make_shared<ProcessVariableUpdate>(xtarget, xvalue, xnext);
      }

      void ProcessVariableUpdate::accept(NodeVisitor &visitor) const
      {
        visitor.visitProcessVariableUpdate(self<ProcessVariableUpdate>());
      }
      Statement::StatementPtr ProcessVariableUpdate::replaceNext(StatementPtr tail) const
      {
        return create(target, value, tail);
      }
      bool ProcessVariableUpdate::isEqual(const Node &node) const
      {
        const ProcessVariableUpdate &that = dynamic_cast<const ProcessVariableUpdate&>(node);
        return equals(target, that.target) && equals(value, that.value) && equals(next, that.next);
      }
      void ProcessVariableUpdate::computeHashCode(HashCode &h) const
      {
        h.mix(target);
        h.mix(value);
        h.mix(next);
      }

    }
  }
}
