#include <mylang/ethir/ir/ProcessValue.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      ProcessValue::ProcessValue(Variable::VariablePtr xval, Expression::ExpressionPtr xexpr)
          : AbstractValueDeclaration(xval, xexpr, nullptr)
      {
        if (value) {
          assert(value->type->isSameType(*variable->type));
        }
        assert(xval->scope == PROCESS_SCOPE);
      }

      ProcessValue::~ProcessValue()
      {
      }

      ProcessValue::ProcessValuePtr ProcessValue::create(Variable::VariablePtr xval,
          Expression::ExpressionPtr xvalue)
      {
        return ::std::make_shared<ProcessValue>(xval, xvalue);
      }

      ProcessValue::ProcessValuePtr ProcessValue::create(const Name &xval,
          Expression::ExpressionPtr xvalue)
      {
        auto var = Variable::create(PROCESS_SCOPE, xvalue->type, xval);
        return ::std::make_shared<ProcessValue>(var, xvalue);
      }

      ProcessValue::ProcessValuePtr ProcessValue::create(Variable::VariablePtr xval)
      {
        return create(xval, nullptr);
      }

      void ProcessValue::accept(NodeVisitor &visitor) const
      {
        visitor.visitProcessValue(self<ProcessValue>());
      }
      Statement::StatementPtr ProcessValue::replaceNext(StatementPtr) const
      {
        return self<Statement>();
      }
      bool ProcessValue::isEqual(const Node &node) const
      {
        return AbstractValueDeclaration::isEqual(node);
      }

      void ProcessValue::computeHashCode(HashCode &h) const
      {
        AbstractValueDeclaration::computeHashCode(h);
      }
    }

  }

}
