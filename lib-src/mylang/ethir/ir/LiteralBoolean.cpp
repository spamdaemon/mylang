#include <mylang/ethir/ir/LiteralBoolean.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/TypeMismatchError.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      LiteralBoolean::LiteralBoolean(::std::shared_ptr<const types::BooleanType> xtype,
          ValueType xvalue)
          : LiteralPrimitive(xtype, xvalue ? "true" : "false"), value(xvalue)
      {
      }

      LiteralBoolean::~LiteralBoolean()
      {
      }

      LiteralBoolean::LiteralBooleanPtr LiteralBoolean::create(
          ::std::shared_ptr<const types::BooleanType> xtype, ValueType xvalue)
      {
        return ::std::make_shared<LiteralBoolean>(xtype, xvalue);
      }

      LiteralBoolean::LiteralBooleanPtr LiteralBoolean::create(ValueType xvalue)
      {
        return create(types::BooleanType::create(), xvalue);
      }

      void LiteralBoolean::accept(NodeVisitor &visitor) const
      {
        visitor.visitLiteralBoolean(self<LiteralBoolean>());
      }
      ::std::optional<int> LiteralBoolean::compare(const Literal &obj) const
      {
        auto that = dynamic_cast<const LiteralBoolean*>(&obj);
        if (that == nullptr) {
          return ::std::nullopt;
        }
        if (value == that->value) {
          return 0;
        } else {
          return value ? 1 : -1;
        }
      }
      Literal::LiteralPtr LiteralBoolean::castTo(const EType &newType) const
      {
        TypeMismatchError::check(type, newType);
        return self<Literal>();
      }
      bool LiteralBoolean::isEqual(const Node &node) const
      {
        const LiteralBoolean &that = dynamic_cast<const LiteralBoolean&>(node);
        return equals(type, that.type) && value == that.value;
      }
      void LiteralBoolean::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(value);
      }

    }
  }
}
