#include <mylang/ethir/ir/Expression.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      Expression::Expression(TypePtr xtype)
          : type(::std::move(xtype))
      {
        if (!type) {
          throw ::std::runtime_error("Expression has no type");
        }
      }

      Expression::~Expression()
      {
      }
    }
  }
}
