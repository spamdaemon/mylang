#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/ir/ValueDecl.h>
#include <mylang/ethir/types/Type.h>
#include <cassert>
#include <memory>

namespace mylang {
  namespace ethir {
    namespace ir {

      ValueDecl::ValueDecl(Variable::VariablePtr xval, Expression::ExpressionPtr xexpr,
          StatementPtr xnext)
          : AbstractValueDeclaration(xval, xexpr, xnext)
      {
        assert(xval->scope == FUNCTION_SCOPE);

        if (xexpr) {
          auto v = xexpr->self<Variable>();
          if (v && v->name == xval->name) {
            ::std::cerr << "Invalid value decl: " << xval->name << " = " << v->name << ::std::endl;
            assert(0 == "Invalid value declaration");
          }
        }
      }

      ValueDecl::~ValueDecl()
      {
      }

      ValueDecl::ValueDeclPtr ValueDecl::create(Variable::VariablePtr xval,
          Expression::ExpressionPtr xvalue, StatementPtr xnext)
      {
        return ::std::make_shared<ValueDecl>(xval, xvalue, xnext);
      }

      ValueDecl::ValueDeclPtr ValueDecl::create(const Name &xval, Expression::ExpressionPtr xvalue,
          StatementPtr xnext)
      {
        auto var = Variable::create(FUNCTION_SCOPE, xvalue->type, xval);
        return ::std::make_shared<ValueDecl>(var, xvalue, xnext);
      }

      ValueDecl::ValueDeclPtr ValueDecl::create(Variable::VariablePtr xval, StatementPtr xnext)
      {
        return create(xval, nullptr, xnext);
      }

      void ValueDecl::accept(NodeVisitor &visitor) const
      {
        visitor.visitValueDecl(self<ValueDecl>());
      }

      ValueDecl::ValueDeclPtr ValueDecl::createSimplified(Variable::VariablePtr xval,
          Expression::ExpressionPtr xvalue, StatementPtr xnext)
      {
        if (xvalue == nullptr) {
          return create(xval, xvalue, xnext);
        }
        LetExpression::LetExpressionPtr ptr = xvalue->self<LetExpression>();
        if (ptr == nullptr) {
          return create(xval, xvalue, xnext);
        }

        ValueDeclPtr res = ValueDecl::createSimplified(xval, ptr->result, xnext);
        res = ValueDecl::createSimplified(ptr->variable, ptr->value, res);
        return res;
      }

      Statement::StatementPtr ValueDecl::replaceNext(StatementPtr tail) const
      {
        return create(variable, value, tail);
      }
      bool ValueDecl::isEqual(const Node &node) const
      {
        return AbstractValueDeclaration::isEqual(node);
      }
      void ValueDecl::computeHashCode(HashCode &h) const
      {
        AbstractValueDeclaration::computeHashCode(h);
      }
    }
  }
}
