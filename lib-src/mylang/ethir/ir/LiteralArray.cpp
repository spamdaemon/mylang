#include <mylang/ethir/ir/LiteralArray.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/TypeMismatchError.h>
#include <mylang/ethir/TypeCastError.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      LiteralArray::LiteralArray(::std::shared_ptr<const types::ArrayType> xtype,
          const Elements &xelements)
          : Literal(xtype), elements(xelements)
      {
        if (!xtype->bounds.contains(xelements.size())) {
          throw ::std::invalid_argument("Number of elements does not match the array type");
        }
        for (size_t i = 0; i < elements.size(); ++i) {
          TypeMismatchError::check(xtype->element, elements.at(i)->type);
        }
      }

      LiteralArray::~LiteralArray()
      {
      }

      LiteralArray::LiteralArrayPtr LiteralArray::create(
          ::std::shared_ptr<const types::ArrayType> xtype, const Elements &xelements)
      {
        return ::std::make_shared<LiteralArray>(xtype, xelements);
      }

      void LiteralArray::accept(NodeVisitor &visitor) const
      {
        visitor.visitLiteralArray(self<LiteralArray>());
      }

      ::std::optional<int> LiteralArray::compare(const Literal &other) const
      {
        const LiteralArray *that = dynamic_cast<const LiteralArray*>(&other);
        if (that == this) {
          return true;
        }
        if (that && type->isSameType(*that->type)) {
          if (that->elements.size() != elements.size()) {
            return elements.size() < that->elements.size() ? -1 : 1;
          }
          ::std::optional<int> res = ::std::make_optional(0);

          for (size_t i = 0; i < elements.size() && res.value() == 0; ++i) {
            res = elements.at(i)->compare(*that->elements.at(i));
          }
          return res;
        }
        return std::nullopt;
      }

      Literal::LiteralPtr LiteralArray::castTo(const EType &newType) const
      {
        auto ty = newType->self<types::ArrayType>();
        Elements elems;
        for (auto e : elements) {
          elems.push_back(e->castTo(ty->element));
        }

        return LiteralArray::create(ty, elems);
      }
      bool LiteralArray::isEqual(const Node &node) const
      {
        const LiteralArray &that = dynamic_cast<const LiteralArray&>(node);
        return equals(type, that.type)
            && equals(elements.begin(), elements.end(), that.elements.begin(), that.elements.end());
      }
      void LiteralArray::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(elements.begin(), elements.end());
      }
    }
  }
}
