#include <mylang/ethir/ir/PortDecl.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      PortDecl::PortDecl(Variable::VariablePtr xval, ::std::string xpublicName, StatementPtr xnext)
          : Declaration(PROCESS_SCOPE, xval->name, xval->type, xnext), variable(xval),
              publicName(xpublicName)
      {
        assert(xval->scope == PROCESS_SCOPE);
      }

      PortDecl::~PortDecl()
      {
      }

    }
  }
}
