#include <mylang/ethir/ir/NoStatement.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      NoStatement::NoStatement()
          : Statement(nullptr)
      {
      }

      NoStatement::~NoStatement()
      {
      }

      Statement::StatementPtr NoStatement::create(StatementPtr xnext)
      {
        if (xnext) {
          return xnext;
        }
        return ::std::make_shared<NoStatement>();
      }

      void NoStatement::accept(NodeVisitor &visitor) const
      {
        visitor.visitNoStatement(self<NoStatement>());
      }
      bool NoStatement::isNOP() const
      {
        return true;
      }
      Statement::StatementPtr NoStatement::replaceNext(StatementPtr tail) const
      {
        if (tail) {
          return tail;
        } else {
          return self<Statement>();
        }
      }
      bool NoStatement::isEqual(const Node &node) const
      {
        const NoStatement &that = dynamic_cast<const NoStatement&>(node);
        return equals(next, that.next);
      }

      void NoStatement::computeHashCode(HashCode &h) const
      {
        h.mix(typeid(*this));
        h.mix(next);
      }
    }
  }
}
