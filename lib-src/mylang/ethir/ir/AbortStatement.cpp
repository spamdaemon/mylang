#include <mylang/ethir/ir/AbortStatement.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      AbortStatement::AbortStatement(Variable::VariablePtr xexpr)
          : ControlFlowStatement(nullptr), value(xexpr)
      {
      }

      AbortStatement::~AbortStatement()
      {
      }

      AbortStatement::AbortStatementPtr AbortStatement::create()
      {
        return ::std::make_shared<AbortStatement>(nullptr);
      }

      AbortStatement::AbortStatementPtr AbortStatement::create(Variable::VariablePtr xvalue)
      {
        return ::std::make_shared<AbortStatement>(xvalue);
      }

      void AbortStatement::accept(NodeVisitor &visitor) const
      {
        visitor.visitAbortStatement(self<AbortStatement>());
      }
      Statement::StatementPtr AbortStatement::replaceNext(StatementPtr) const
      {
        return self<Statement>();
      }
      bool AbortStatement::isEqual(const Node &node) const
      {
        const AbortStatement &that = dynamic_cast<const AbortStatement&>(node);
        return equals(value, that.value) && equals(next, that.next);
      }
      void AbortStatement::computeHashCode(HashCode &h) const
      {
        h.mix(typeid(*this));
        h.mix(value);
        h.mix(next);
      }
    }

  }
}
