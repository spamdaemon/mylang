#include <mylang/ethir/ir/ProcessBlock.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/types/ArrayType.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      ProcessBlock::ProcessBlock(::std::optional<::std::string> xname,
          StatementBlock::StatementBlockPtr xbody)
          : Statement(nullptr), name(xname), body(xbody)
      {
      }

      ProcessBlock::~ProcessBlock()
      {
      }

      ProcessBlock::ProcessBlockPtr ProcessBlock::create(::std::optional<::std::string> xname,
          Statement::StatementPtr xbody)
      {
        return ::std::make_shared<ProcessBlock>(xname, StatementBlock::singleton(xbody));
      }

      void ProcessBlock::accept(NodeVisitor &visitor) const
      {
        visitor.visitProcessBlock(self<ProcessBlock>());
      }
      Statement::StatementPtr ProcessBlock::replaceNext(StatementPtr) const
      {
        return self<Statement>();
      }
      bool ProcessBlock::isEqual(const Node &node) const
      {
        const ProcessBlock &that = dynamic_cast<const ProcessBlock&>(node);
        return name == that.name && equals(body, that.body) && equals(next, that.next);
      }
      void ProcessBlock::computeHashCode(HashCode &h) const
      {
        h.mix(name);
        h.mix(body);
        h.mix(next);
      }
    }
  }
}
