#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/types/Type.h>
#include <typeindex>

namespace mylang {
  namespace ethir {
    namespace ir {

      Node::Node()
      {
      }

      Node::~Node()
      {
      }

      const HashCode& Node::hashcode() const
      {
        if (!hc.has_value()) {
          HashCode c;
          computeHashCode(c);
          hc = c;
        }
        return hc.value();
      }

      bool Node::equals(const NodePtr &other) const
      {
        if (other.get() == this) {
          return true;
        }
        if (!other) {
          return false;
        }

        const ::std::type_index THIS(typeid(*this));
        const ::std::type_index THAT(typeid(*other));
        if (THIS != THAT) {
          return false;
        }

        if (hashcode() != other->hashcode()) {
          // hashcodes must be the same
          return false;
        }

        return isEqual(*other);
      }

      bool Node::equals(const NodePtr &a, const NodePtr &b)
      {
        if (a == b) {
          return true;
        }
        if (!a || !b) {
          return false;
        }
        return a->equals(b);
      }

      bool Node::equals(const EType &a, const EType &b)
      {
        if (a == b) {
          return true;
        }
        if (!a || !b) {
          return false;
        }
        return a->isSameType(*b);
      }

      void Node::computeHashCode(HashCode &h) const
      {
        h.mix(this);
      }
      bool Node::isEqual(const Node &node) const
      {
        return &node == this;
      }

    }
  }
}
