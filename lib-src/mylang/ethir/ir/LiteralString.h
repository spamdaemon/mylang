#ifndef CLASS_MYLANG_ETHIR_IR_LITERALSTRING_H
#define CLASS_MYLANG_ETHIR_IR_LITERALSTRING_H

#ifndef CLASS_MYLANG_ETHIR_IR_LITERALPRIMITIVE_H
#include <mylang/ethir/ir/LiteralPrimitive.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_STRINGYPE_H
#include <mylang/ethir/types/StringType.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * This class represent the constructor for a literal.
       */
      class LiteralString: public LiteralPrimitive
      {
        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const LiteralString> LiteralStringPtr;

        /** The value type */
      public:
        typedef ::std::string ValueType;

        /**
         * Create an expression that returns a literal value.
         * @param type the type
         * @aparam value the string value for the literal
         */
      public:
        LiteralString(::std::shared_ptr<const types::StringType> type, ValueType value);

        /** Destructor */
      public:
        ~LiteralString();

        /**
         * Create an bit literal
         * @param value true or false
         * @return a literal
         */
      public:
        static LiteralStringPtr create(::std::shared_ptr<const types::StringType> type,
            ValueType value);

        /**
         * Create an bit literal
         * @param value true or false
         * @return a literal
         */
      public:
        static LiteralStringPtr create(ValueType value);

        /**
         * Create an bit literal
         * @param value true or false
         * @return a literal
         */
      public:
        static LiteralStringPtr create(::std::shared_ptr<const types::StringType> type, char value);

        /**
         * Create an bit literal
         * @param value true or false
         * @return a literal
         */
      public:
        static LiteralStringPtr create(char value);

        /**
         * Create an bit literal
         * @param value true or false
         * @return a literal
         */
      public:
        static LiteralStringPtr createEmpty();

      public:
        void accept(NodeVisitor &visitor) const override final;
        ::std::optional<int> compare(const Literal &that) const override final;
        LiteralPtr castTo(const EType &newType) const override;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The string value */
      public:
        const ValueType value;
      };
    }
  }
}
#endif
