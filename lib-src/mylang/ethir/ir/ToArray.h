#ifndef CLASS_MYLANG_ETHIR_IR_TOARRAY_H
#define CLASS_MYLANG_ETHIR_IR_TOARRAY_H

#ifndef CLASS_MYLANG_ETHIR_IR_EXPRESSION_H
#include <mylang/ethir/ir/Expression.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_LOOP_H
#include <mylang/ethir/ir/Loop.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_ARRAYTYPE_H
#include <mylang/ethir/types/ArrayType.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      class ToArray: public Expression
      {

        /** The pointer to the expression */
      public:
        typedef ::std::shared_ptr<const ToArray> ToArrayPtr;

        /** The array type */
      public:
        typedef ::std::shared_ptr<const types::ArrayType> ArrayPtr;

        /**
         * Create an expression that generates an array from a loop.
         * @param type the array type
         * @param loop the loop that yields the array elements
         */
      public:
        ToArray(ArrayPtr type, Loop::LoopPtr loop);

        /** destructor */
      public:
        ~ToArray();

        /**
         * Create a new phi function expression.
         * @param arg the arguments
         * @return a new phi function
         */
      public:
        static ToArrayPtr create(ArrayPtr type, Loop::LoopPtr loop);

        /**
         * Accept a node visitor.
         */
      public:
        void accept(mylang::ethir::ir::NodeVisitor &visitor) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The loop */
      public:
        const Loop::LoopPtr loop;
      };
    }
  }
}
#endif
