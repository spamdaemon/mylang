#ifndef CLASS_MYLANG_ETHIR_IR_FOREACHSTATEMENT_H
#define CLASS_MYLANG_ETHIR_IR_FOREACHSTATEMENT_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_CONTROLFLOWSTATEMENT_H
#include <mylang/ethir/ir/ControlFlowStatement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENTBLOCK_H
#include <mylang/ethir/ir/StatementBlock.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * Read from a port into a variable.
       */
      class ForeachStatement: public ControlFlowStatement
      {
        ForeachStatement(const ForeachStatement &e) = delete;

        ForeachStatement& operator=(const ForeachStatement &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const ForeachStatement> ForeachStatementPtr;

        /**
         * Create a foreach statement
         * @param loopvar the loop variable the variable to be defined
         * @param loopdata the data over which to loop
         * @param body the function body
         */
      public:
        ForeachStatement(Name loopname, Variable::VariablePtr loopvar,
            Variable::VariablePtr loopdata, StatementBlock::StatementBlockPtr body,
            StatementPtr next);

        /** Destructor */
      public:
        ~ForeachStatement();

        /**
         * Create a read statement to read into the specified variable.
         * @param loopvar the loop variable the variable to be defined
         * @param loopdata the data over which to loop
         * @param body the function body
         * @return a foreach statement
         */
      public:
        static ForeachStatementPtr create(Name loopname, Variable::VariablePtr loopvar,
            Variable::VariablePtr loopdata, Statement::StatementPtr body, StatementPtr next);

      public:
        void accept(NodeVisitor &visitor) const override final;
        bool isNOP() const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /**
         * The name of the loop statement, which is used in conjunection with a break statement
         */
      public:
        const Name name;

        /**
         * The name and type of the local variable that was introduced.
         */
      public:
        const Variable::VariablePtr variable;

        /** The loop data */
      public:
        const Variable::VariablePtr data;

        /** The loop body */
      public:
        const StatementBlock::StatementBlockPtr body;
      };
    }
  }
}
#endif
