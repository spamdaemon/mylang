#ifndef CLASS_MYLANG_ETHIR_IR_LITERAL_H
#define CLASS_MYLANG_ETHIR_IR_LITERAL_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#include <mylang/ethir/types/Type.h>
#endif

#ifndef CLASS_MYLANG_BIGINT_H
#include <mylang/BigInt.h>
#endif

#ifndef CLASS_MYLANG_BIGUINT_H
#include <mylang/BigUInt.h>
#endif

#ifndef CLASS_MYLANG_BIGREAL_H
#include <mylang/BigReal.h>
#endif

#include <optional>

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * This class represent the constructor for a literal.
       */
      class Literal: public Expression
      {
        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const Literal> LiteralPtr;

        /**
         * Create an expression that returns a literal value.
         * @param type the type
         * @aparam value the string value for the literal
         */
      public:
        Literal(TypePtr t);

        /** Destructor */
      public:
        virtual ~Literal() = 0;

        /**
         * Get the raw type for this literal.
         * @return the raw type for this literal
         */
      public:
        virtual TypePtr getSourceType() const;

        /**
         * Cast this literal to the specified type.
         * @param newType the new type
         * @return a literal
         * @throws std::exception if this literal cannot be cast
         */
      public:
        virtual LiteralPtr castTo(const EType &newType) const = 0;

        /**
         * Determine if two literals are the same
         * @param a a literal
         * @param b a literal
         * @return true if the literals are the same
         */
      public:
        inline friend bool operator==(const Literal &a, const Literal &b)
        {
          return a.isSameLiteral(b);
        }

        /**
         * Determine if two literals are the same. If this method return true, then compare must return 0.
         * If this method return false, then compare must not return 0.
         * @param that another literal
         */
      public:
        bool isSameLiteral(const Literal &that) const;

        /**
         * Compare two literals that must have the same type.
         * @param that another literal
         * @return -1 if this literal is less, 0 if equal, and 1 if this is greater, nullopt if not comparable
         */
      public:
        virtual ::std::optional<int> compare(const Literal &that) const = 0;
      };
    }
  }
}
#endif
