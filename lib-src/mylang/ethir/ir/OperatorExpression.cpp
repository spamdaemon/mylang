#include <mylang/ethir/ir/OperatorExpression.h>
#include <mylang/ethir/ir/LetExpression.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/types/types.h>
#include <cassert>
#include <vector>

namespace mylang {
  namespace ethir {
    namespace ir {
      namespace {
        ::std::nullptr_t typeError(const ::std::string &message)
        {
          ::std::cerr << "type error " << message << ::std::endl;
          return nullptr;
        }

      }
      OperatorExpression::OperatorExpression(TypePtr t, Operator xname, Arguments args)
          : Expression(::std::move(t)), name(xname), arguments(::std::move(args))
      {
        for (auto arg : arguments) {
          assert(arg && "Argument is nullptr");
        }
      }

      OperatorExpression::~OperatorExpression()
      {
      }

      OperatorExpression::TypePtr OperatorExpression::typeFor(Operator op, Arguments args)
      {
        Types argTypes;
        for (auto e : args) {
          argTypes.push_back(e->type);
        }
        return typeFor(op, argTypes, nullptr);
      }

      OperatorExpression::TypePtr OperatorExpression::typeFor(Operator op, const Types &args,
          const TypePtr &resTy)
      {

        TypePtr arg0 = args.size() > 0 ? args.at(0) : nullptr;
        TypePtr arg1 = args.size() > 1 ? args.at(1) : nullptr;
        TypePtr arg2 = args.size() > 2 ? args.at(2) : nullptr;

        switch (op) {
        case Operator::OP_EQ:
        case Operator::OP_NEQ:
        case Operator::OP_LT:
        case Operator::OP_LTE:
        case Operator::OP_IS_LOGGABLE:
        case Operator::OP_IS_PRESENT:
        case Operator::OP_IS_INPUT_NEW:
        case Operator::OP_IS_INPUT_CLOSED:
        case Operator::OP_IS_OUTPUT_CLOSED:
        case Operator::OP_IS_PORT_READABLE:
        case Operator::OP_IS_PORT_WRITABLE:
          return types::PrimitiveType::getBoolean();

        case Operator::OP_CALL:
          return arg0->self<types::FunctionType>()->returnType;

        case Operator::OP_NEG:
          if (arg0->self<types::RealType>()) {
            return arg0;
          } else if (arg0->self<types::IntegerType>()) {
            auto ity = arg0->self<types::IntegerType>();
            auto neg = ity->range.negate();
            return types::IntegerType::create(neg);
          } else {
            return nullptr;
          }
        case Operator::OP_ADD:
          if (arg0->self<types::RealType>()) {
            return arg0;
          } else if (arg0->self<types::IntegerType>()) {
            auto lhs = arg0->self<types::IntegerType>();
            auto rhs = arg1->self<types::IntegerType>();
            auto add = lhs->range + rhs->range;
            return types::IntegerType::create(add);
          } else {
            return nullptr;
          }
        case Operator::OP_SUB:
          if (arg0->self<types::RealType>()) {
            return arg0;
          } else if (arg0->self<types::IntegerType>()) {
            auto lhs = arg0->self<types::IntegerType>();
            auto rhs = arg1->self<types::IntegerType>();
            auto sub = lhs->range - rhs->range;
            return types::IntegerType::create(sub);
          } else {
            return nullptr;
          }
        case Operator::OP_MUL:
          if (arg0->self<types::RealType>()) {
            return arg0;
          } else if (arg0->self<types::IntegerType>()) {
            auto lhs = arg0->self<types::IntegerType>();
            auto rhs = arg1->self<types::IntegerType>();
            auto mul = lhs->range * rhs->range;
            return types::IntegerType::create(mul);
          } else {
            return nullptr;
          }
        case Operator::OP_DIV:
          if (arg0->self<types::RealType>()) {
            return arg0;
          } else if (arg0->self<types::IntegerType>()) {
            auto lhs = arg0->self<types::IntegerType>();
            auto rhs = arg1->self<types::IntegerType>();
            auto d = lhs->range / rhs->range;
            if (d.has_value()) {
              return types::IntegerType::create(*d);
            } else {
              return typeError("Cannot determine type for division");
            }
          } else {
            return nullptr;
          }
        case Operator::OP_MOD:
          if (arg0->self<types::IntegerType>()) {
            auto lhs = arg0->self<types::IntegerType>();
            auto rhs = arg1->self<types::IntegerType>();
            auto m = lhs->range.mod(rhs->range);
            if (m.has_value()) {
              return types::IntegerType::create(*m);
            } else {
              return typeError("Cannot determine type for modulus");
            }
          } else {
            return nullptr;
          }
        case Operator::OP_REM:
          if (arg0->self<types::RealType>()) {
            return arg0;
          } else if (arg0->self<types::IntegerType>()) {
            auto lhs = arg0->self<types::IntegerType>();
            auto rhs = arg1->self<types::IntegerType>();
            auto r = lhs->range.remainder(rhs->range);
            if (r.has_value()) {
              return types::IntegerType::create(*r);
            } else {
              return typeError("Cannot determine type for remainder");
            }
          } else {
            return nullptr;
          }
        case Operator::OP_INDEX:
          if (arg0->self<types::ArrayType>()) {
            return arg0->self<types::ArrayType>()->element;
          } else if (arg0->self<types::StringType>()) {
            return arg0->self<types::StringType>()->element;
          } else {
            return nullptr;
          }
        case Operator::OP_SIZE:
          if (arg0->self<types::ArrayType>()) {
            return arg0->self<types::ArrayType>()->lengthType;
          } else if (arg0->self<types::StringType>()) {
            return arg0->self<types::StringType>()->lengthType;
          } else {
            return nullptr;
          }
        case Operator::OP_TO_STRING:
          return types::StringType::create();
        case Operator::OP_SUBRANGE: {
          if (auto arrTy = arg0->self<types::ArrayType>()) {
            return types::ArrayType::get(arrTy->element, 0, arrTy->maxSize);
          } else if (auto strTy = arg0->self<types::StringType>()) {
            return strTy;
          } else {
            return nullptr;
          }
        }
        case Operator::OP_AND:
        case Operator::OP_OR:
        case Operator::OP_XOR:
        case Operator::OP_NOT:
          return arg0;
        case Operator::OP_GET: {
          if (auto optTy = arg0->self<types::OptType>()) {
            return optTy->element;
          }
          if (auto mutTy = arg0->self<types::MutableType>()) {
            return mutTy->element;
          }
          if (auto portTy = arg0->self<types::InputOutputType>()) {
            return types::OptType::get(portTy->element);
          }
          throw ::std::runtime_error("Unexpected operand to OP_GET : " + arg0->toString());
        }
        case Operator::OP_MERGE_TUPLES: {
          auto t0 = arg0->self<types::TupleType>();
          auto t1 = arg1->self<types::TupleType>();
          ::std::vector<types::Type::Ptr> ttypes(t0->types);
          ttypes.insert(ttypes.end(), t1->types.begin(), t1->types.end());
          return types::TupleType::get(ttypes);
        }
        case Operator::OP_STRUCT_TO_TUPLE: {
          auto t0 = arg0->self<types::StructType>();
          ::std::vector<types::Type::Ptr> ttypes;
          for (auto m : t0->members) {
            ttypes.push_back(m.type);
          }
          return types::TupleType::get(ttypes);
        }
        case Operator::OP_ZIP: {
          auto t0 = arg0->self<types::ArrayType>();
          auto t1 = arg0->self<types::ArrayType>();
          auto tup = types::TupleType::get( { t0->element, t1->element });
          auto bnds = t0->bounds.intersectWith(t1->bounds);
          if (bnds.has_value()) {
            return types::ArrayType::get(tup, *bnds);
          } else {
            return typeError("Failed to determine bounds for zip");
          }
        }
        case Operator::OP_CONCATENATE: {
          if (arg0->self<types::ArrayType>()) {
            auto t0 = arg0->self<types::ArrayType>();
            auto t1 = arg0->self<types::ArrayType>();
            auto bnds = t0->bounds + t1->bounds;
            return types::ArrayType::get(t0->element, bnds);
          } else if (arg0->self<types::StringType>()) {
            return arg0;
          } else {
            return nullptr;
          }
        }
        case Operator::OP_FROM_BITS:
        case Operator::OP_FROM_BYTES:
          return nullptr;
        case Operator::OP_TO_BITS:
          if (arg0->self<types::BitType>()) {
            return types::ArrayType::get(types::BitType::create(), 1, 1);
          } else if (arg0->self<types::BooleanType>()) {
            return types::ArrayType::get(types::BitType::create(), 1, 1);
          } else if (arg0->self<types::ByteType>()) {
            return types::ArrayType::get(types::BitType::create(), 8, 8);
          } else if (arg0->self<types::CharType>()) {
            return types::ArrayType::get(types::BitType::create(), 8, 8 * 6);
          } else if (arg0->self<types::RealType>()) {
            return types::ArrayType::get(types::BitType::create(), 8, Integer::PLUS_INFINITY());
          } else {
            return nullptr;
          }
        case Operator::OP_TO_BYTES:
          if (arg0->self<types::BitType>()) {
            return types::ArrayType::get(types::ByteType::create(), 1, 1);
          } else if (arg0->self<types::BooleanType>()) {
            return types::ArrayType::get(types::ByteType::create(), 1, 1);
          } else if (arg0->self<types::ByteType>()) {
            return types::ArrayType::get(types::ByteType::create(), 1, 1);
          } else if (arg0->self<types::CharType>()) {
            return types::ArrayType::get(types::ByteType::create(), 1, 6);
          } else if (arg0->self<types::RealType>()) {
            return types::ArrayType::get(types::ByteType::create(), 1, Integer::PLUS_INFINITY());
          } else {
            return nullptr;
          }
        case Operator::OP_BYTE_TO_UINT8:
          return types::IntegerType::create(0, 255);
        case Operator::OP_BYTE_TO_SINT8:
          return types::IntegerType::create(-128, 127);
        case Operator::OP_STRING_TO_CHARS: {
          auto strTy = arg0->self<types::StringType>();
          return types::ArrayType::get(strTy->element, Interval::NON_NEGATIVE());
        }
        case Operator::OP_INTERPOLATE_TEXT:
          return types::StringType::create();
        case Operator::OP_FOLD:
          return arg1->self<types::FunctionType>()->returnType;
        case Operator::OP_PAD: {
          auto arrTy = arg0->self<types::ArrayType>();
          auto intTy = arg1->self<types::IntegerType>();
          auto bounds = arrTy->bounds.pairwiseMax(intTy->range);
          return types::ArrayType::get(arrTy->element, bounds);
        }
        case Operator::OP_TRIM: {
          auto arrTy = arg0->self<types::ArrayType>();
          auto intTy = arg1->self<types::IntegerType>();
          auto bounds = arrTy->bounds.pairwiseMin(intTy->range);
          return types::ArrayType::get(arrTy->element, bounds);
        }
        case Operator::OP_TAKE: {
          auto arrTy = arg0->self<types::ArrayType>();
          auto intTy = arg1->self<types::IntegerType>();
          auto bounds = arrTy->bounds.pairwiseMin(intTy->range.abs());
          return types::ArrayType::get(arrTy->element, bounds);
        }
        case Operator::OP_DROP: {
          auto arrTy = arg0->self<types::ArrayType>();
          auto intTy = arg1->self<types::IntegerType>();
          auto bounds = (arrTy->bounds - intTy->range.abs()).pairwiseMax(0);
          return types::ArrayType::get(arrTy->element, bounds);
        }
        case Operator::OP_MAP_ARRAY:
          return arg0->self<types::ArrayType>()->copy(arg1->self<types::FunctionType>()->returnType);
        case Operator::OP_MAP_OPTIONAL:
          return arg0->self<types::OptType>()->copy(arg1->self<types::FunctionType>()->returnType);
        case Operator::OP_FILTER_ARRAY: {
          auto arrTy = arg0->self<types::ArrayType>();
          return types::ArrayType::get(arrTy->element, 0, arrTy->maxSize);
        }
        case Operator::OP_FILTER_OPTIONAL:
          return arg0;
        case Operator::OP_FIND: {
          auto arrTy = arg0->self<types::ArrayType>();
          if (arrTy->indexType.has_value()) {
            return types::OptType::get(arrTy->indexType.value());
          } else {
            throw ::std::runtime_error("Cannot have an empty array as input to find");
          }
        }
        case Operator::OP_REVERSE:
          return arg0;
        case Operator::OP_BITS_TO_BYTES: {
          auto arrTy = arg0->self<types::ArrayType>();
          auto bnds = *arrTy->bounds.add(7).divide(8);
          return types::ArrayType::get(types::ByteType::create(), bnds);
        }
        case Operator::OP_BYTES_TO_BITS: {
          auto arrTy = arg0->self<types::ArrayType>();
          auto bnds = arrTy->bounds.multiply(8);
          return types::ArrayType::get(types::BitType::create(), bnds);
        }
        case Operator::OP_FLATTEN: {
          if (arg0->self<types::ArrayType>()) {
            auto arrTy = arg0->self<types::ArrayType>();
            auto elemTy = arrTy->element->self<types::ArrayType>();
            if (!elemTy) {
              throw ::std::runtime_error("expected a 2-dimensional array");
            }
            auto bounds = arrTy->bounds * elemTy->bounds;
            auto ty = types::ArrayType::get(elemTy->element, bounds);
            return ty;
          } else if (arg0->self<types::OptType>()) {
            auto optTy = arg0->self<types::OptType>();
            while (optTy->element->self<types::OptType>()) {
              optTy = optTy->element->self<types::OptType>();
            }
            return optTy;
          } else {
            break;
          }

        }
        case Operator::OP_PARTITION: {
          auto arrTy = arg0->self<types::ArrayType>();
          auto sizeTy = arg1->self<types::IntegerType>();
          auto optBounds = arrTy->bounds.remainder(sizeTy->range);
          if (!optBounds.has_value()) {
            return typeError("cannot determine bounds for partition operation");
          }
          // the actual bounds cannot be negative, reset the bounds
          const Interval bounds(Integer::ZERO().max(optBounds->min()), optBounds->max());

          // we can conveniently use the partitionSize's bounds for the bounds of inner array
          auto elemTy = types::ArrayType::get(arrTy->element, sizeTy->range);

          auto ty = types::ArrayType::get(elemTy, bounds);
          return ty;
        }
        case Operator::OP_HEAD:
          return arg0->self<types::ArrayType>()->element;
        case Operator::OP_TAIL: {
          auto arrTy = arg0->self<types::ArrayType>();
          auto min = arrTy->minSize.towards0();
          auto max = arrTy->maxSize.towards0();
          return types::ArrayType::get(arrTy->element, min, max);
        }
        case Operator::OP_BASETYPE_CAST: {
          auto ty = arg0->self<types::NamedType>();
          if (ty) {
            return ty->resolve();
          } else {
            return arg0;
          }
        }
        case Operator::OP_ROOTTYPE_CAST: {
          auto ty = arg0;
          while (ty->self<types::NamedType>()) {
            ty = ty->self<types::NamedType>()->resolve();
          }
          return ty;
        }
        case Operator::OP_READ_PORT: {
          auto portTy = arg0->self<types::InputType>();
          if (portTy) {
            return types::OptType::get(portTy->element);
          }
          return nullptr;
        }
        case Operator::OP_BLOCK_READ:
        case Operator::OP_BLOCK_WRITE:
        case Operator::OP_CLEAR_PORT:
        case Operator::OP_CLOSE_PORT:
        case Operator::OP_WAIT_PORT:
        case Operator::OP_WRITE_PORT:
        case Operator::OP_LOG:
        case Operator::OP_SET:
        case Operator::OP_APPEND:
          return types::VoidType::getVoid();
        case Operator::OP_BUILD: {
          auto ty = arg0->self<types::BuilderType>();
          return ty->product;
        }
          // these ones are not supported!
        case Operator::OP_TUPLE_TO_STRUCT:
        case Operator::OP_CHECKED_CAST:
        case Operator::OP_UNCHECKED_CAST:
        case Operator::OP_CLAMP:
        case Operator::OP_WRAP:
        case Operator::OP_NEW:
          break;
        }
        return nullptr;
      }

      OperatorExpression::OperatorExpressionPtr OperatorExpression::create(TypePtr t, Operator op,
          Arguments args)
      {
        auto ety = t ? t : typeFor(op, args);
        if (!ety) {
          throw ::std::runtime_error(
              "Failed to determine the return type for operator expression "
                  + ::std::string(operatorName(op)));
        }
        return ::std::make_shared<OperatorExpression>(ety, op, args);
      }

      OperatorExpression::OperatorExpressionPtr OperatorExpression::create(TypePtr t, Operator op)
      {
        Arguments args;
        return create(t, op, args);
      }

      OperatorExpression::OperatorExpressionPtr OperatorExpression::create(TypePtr t, Operator op,
          VariablePtr arg)
      {
        Arguments args( { arg });
        return create(t, op, args);
      }

      OperatorExpression::OperatorExpressionPtr OperatorExpression::create(TypePtr t, Operator op,
          VariablePtr left, VariablePtr right)
      {
        return create(t, op, { left, right });
      }

      OperatorExpression::OperatorExpressionPtr OperatorExpression::create(TypePtr t, Operator op,
          VariablePtr arg1, VariablePtr arg2, VariablePtr arg3)
      {
        return create(t, op, { arg1, arg2, arg3 });
      }

      OperatorExpression::OperatorExpressionPtr OperatorExpression::create(Operator op,
          Arguments args)
      {
        auto ty = typeFor(op, args);
        if (ty) {
          return create(ty, op, args);
        }
        throw ::std::runtime_error(
            "Failed to determine the type of operator expression "
                + ::std::string(operatorName(op)));
      }

      OperatorExpression::OperatorExpressionPtr OperatorExpression::create(Operator op)
      {
        Arguments args;
        return create(op, args);
      }

      OperatorExpression::OperatorExpressionPtr OperatorExpression::create(Operator op,
          VariablePtr arg)
      {
        Arguments args( { arg });
        return create(op, args);
      }

      OperatorExpression::OperatorExpressionPtr OperatorExpression::create(Operator op,
          VariablePtr left, VariablePtr right)
      {
        return create(op, { left, right });
      }

      OperatorExpression::OperatorExpressionPtr OperatorExpression::create(Operator op,
          VariablePtr arg1, VariablePtr arg2, VariablePtr arg3)
      {
        return create(op, { arg1, arg2, arg3 });
      }

      Expression::ExpressionPtr OperatorExpression::checked_cast(TypePtr t, ExpressionPtr expr)
      {
        if (t->isSameType(*expr->type)) {
          return expr;
        }

        auto v = expr->self<Variable>();
        if (v) {
          return create(t, Operator::OP_CHECKED_CAST, v);
        } else {
          return LetExpression::create(expr, [&](EVariable vv) {
            return create(t,Operator::OP_CHECKED_CAST,vv);
          });
        }
      }

      void OperatorExpression::accept(NodeVisitor &visitor) const
      {
        visitor.visitOperatorExpression(self<OperatorExpression>());
      }

      ::std::shared_ptr<const types::FunctionType> OperatorExpression::getSignature() const
      {
        types::FunctionType::Parameters params;
        for (auto arg : arguments) {
          params.push_back(arg->type);
        }
        return types::FunctionType::get(type, params);
      }

      const char* OperatorExpression::operatorName(Operator op)
      {
#define UNIQUE_OPERATOR_IMPL(X) #X
#define MAKE_FUNC_NAME(OP) UNIQUE_OPERATOR_IMPL(OP)
        switch (op) {
        case Operator::OP_NEW:
          return MAKE_FUNC_NAME(OP_NEW);
        case Operator::OP_CALL:
          return MAKE_FUNC_NAME(OP_CALL);
        case Operator::OP_CHECKED_CAST:
          return MAKE_FUNC_NAME(OP_CHECKED_CAST);
        case Operator::OP_UNCHECKED_CAST:
          return MAKE_FUNC_NAME(OP_UNCHECKED_CAST);
        case Operator::OP_NEG:
          return MAKE_FUNC_NAME(OP_NEG);
        case Operator::OP_ADD:
          return MAKE_FUNC_NAME(OP_ADD);
        case Operator::OP_SUB:
          return MAKE_FUNC_NAME(OP_SUB);
        case Operator::OP_MUL:
          return MAKE_FUNC_NAME(OP_MUL);
        case Operator::OP_DIV:
          return MAKE_FUNC_NAME(OP_DIV);
        case Operator::OP_MOD:
          return MAKE_FUNC_NAME(OP_MOD);
        case Operator::OP_REM:
          return MAKE_FUNC_NAME(OP_REM);

        case Operator::OP_INDEX:
          return MAKE_FUNC_NAME(OP_INDEX);
        case Operator::OP_SUBRANGE:
          return MAKE_FUNC_NAME(OP_SUBRANGE);
        case Operator::OP_EQ:
          return MAKE_FUNC_NAME(OP_EQ);
        case Operator::OP_NEQ:
          return MAKE_FUNC_NAME(OP_NEQ);
        case Operator::OP_LT:
          return MAKE_FUNC_NAME(OP_LT);
        case Operator::OP_LTE:
          return MAKE_FUNC_NAME(OP_LTE);
        case Operator::OP_AND:
          return MAKE_FUNC_NAME(OP_AND);
        case Operator::OP_OR:
          return MAKE_FUNC_NAME(OP_OR);
        case Operator::OP_XOR:
          return MAKE_FUNC_NAME(OP_XOR);
        case Operator::OP_NOT:
          return MAKE_FUNC_NAME(OP_NOT);
        case Operator::OP_GET:
          return MAKE_FUNC_NAME(OP_GET);
        case Operator::OP_IS_LOGGABLE:
          return MAKE_FUNC_NAME(OP_IS_LOGGABLE);
        case Operator::OP_IS_PRESENT:
          return MAKE_FUNC_NAME(OP_IS_PRESENT);
        case Operator::OP_IS_INPUT_NEW:
          return MAKE_FUNC_NAME(OP_IS_INPUT_NEW);
        case Operator::OP_IS_INPUT_CLOSED:
          return MAKE_FUNC_NAME(OP_IS_INPUT_CLOSED);
        case Operator::OP_IS_OUTPUT_CLOSED:
          return MAKE_FUNC_NAME(OP_IS_OUTPUT_CLOSED);
        case Operator::OP_IS_PORT_READABLE:
          return MAKE_FUNC_NAME(OP_IS_PORT_READABLE);
        case Operator::OP_IS_PORT_WRITABLE:
          return MAKE_FUNC_NAME(OP_IS_PORT_WRITABLE);
        case Operator::OP_SIZE:
          return MAKE_FUNC_NAME(OP_SIZE);
        case Operator::OP_CONCATENATE:
          return MAKE_FUNC_NAME(OP_CONCATENATE);
        case Operator::OP_MERGE_TUPLES:
          return MAKE_FUNC_NAME(OP_MERGE_TUPLES);
        case Operator::OP_ZIP:
          return MAKE_FUNC_NAME(OP_ZIP);
        case Operator::OP_FROM_BITS:
          return MAKE_FUNC_NAME(OP_FROM_BITS);
        case Operator::OP_TO_BITS:
          return MAKE_FUNC_NAME(OP_TO_BITS);
        case Operator::OP_FROM_BYTES:
          return MAKE_FUNC_NAME(OP_FROM_BYTES);
        case Operator::OP_TO_BYTES:
          return MAKE_FUNC_NAME(OP_TO_BYTES);
        case Operator::OP_BITS_TO_BYTES:
          return MAKE_FUNC_NAME(OP_BITS_TO_BYTES);
        case Operator::OP_BYTES_TO_BITS:
          return MAKE_FUNC_NAME(OP_BYTES_TO_BITS);
        case Operator::OP_CLAMP:
          return MAKE_FUNC_NAME(OP_CLAMP);
        case Operator::OP_WRAP:
          return MAKE_FUNC_NAME(OP_WRAP);
        case Operator::OP_BYTE_TO_UINT8:
          return MAKE_FUNC_NAME(OP_BYTE_TO_UINT8);
        case Operator::OP_BYTE_TO_SINT8:
          return MAKE_FUNC_NAME(OP_BYTE_TO_SINT8);
        case Operator::OP_STRING_TO_CHARS:
          return MAKE_FUNC_NAME(OP_STRING_TO_CHARS);
        case Operator::OP_INTERPOLATE_TEXT:
          return MAKE_FUNC_NAME(OP_INTERPOLATE_TEXT);
        case Operator::OP_TO_STRING:
          return MAKE_FUNC_NAME(OP_TO_STRING);
        case Operator::OP_FOLD:
          return MAKE_FUNC_NAME(OP_FOLD);
        case Operator::OP_MAP_ARRAY:
          return MAKE_FUNC_NAME(OP_MAP_ARRAY);
        case Operator::OP_MAP_OPTIONAL:
          return MAKE_FUNC_NAME(OP_MAP_OPTIONAL);
        case Operator::OP_FILTER_ARRAY:
          return MAKE_FUNC_NAME(OP_FILTER_ARRAY);
        case Operator::OP_FILTER_OPTIONAL:
          return MAKE_FUNC_NAME(OP_FILTER_OPTIONAL);
        case Operator::OP_FIND:
          return MAKE_FUNC_NAME(OP_FIND);
        case Operator::OP_PAD:
          return MAKE_FUNC_NAME(OP_PAD);
        case Operator::OP_TRIM:
          return MAKE_FUNC_NAME(OP_TRIM);
        case Operator::OP_DROP:
          return MAKE_FUNC_NAME(OP_DROP);
        case Operator::OP_TAKE:
          return MAKE_FUNC_NAME(OP_TAKE);
        case Operator::OP_REVERSE:
          return MAKE_FUNC_NAME(OP_REVERSE);
        case Operator::OP_FLATTEN:
          return MAKE_FUNC_NAME(OP_FLATTEN);
        case Operator::OP_PARTITION:
          return MAKE_FUNC_NAME(OP_PARTITION);
        case Operator::OP_HEAD:
          return MAKE_FUNC_NAME(OP_HEAD);
        case Operator::OP_TAIL:
          return MAKE_FUNC_NAME(OP_TAIL);
        case Operator::OP_STRUCT_TO_TUPLE:
          return MAKE_FUNC_NAME(OP_STRUCT_TO_TUPLE);
        case Operator::OP_TUPLE_TO_STRUCT:
          return MAKE_FUNC_NAME(OP_TUPLE_TO_STRUCT);
        case Operator::OP_BASETYPE_CAST:
          return MAKE_FUNC_NAME(OP_BASETYPE_CAST);
        case Operator::OP_ROOTTYPE_CAST:
          return MAKE_FUNC_NAME(OP_ROOTTYPE_CAST);
        case Operator::OP_READ_PORT:
          return MAKE_FUNC_NAME(OP_READ_PORT);
        case Operator::OP_BLOCK_READ:
          return MAKE_FUNC_NAME(OP_BLOCK_READ);
        case Operator::OP_BLOCK_WRITE:
          return MAKE_FUNC_NAME(OP_BLOCK_WRITE);
        case Operator::OP_CLEAR_PORT:
          return MAKE_FUNC_NAME(OP_CLEAR_PORT);
        case Operator::OP_CLOSE_PORT:
          return MAKE_FUNC_NAME(OP_CLOSE_PORT);
        case Operator::OP_WAIT_PORT:
          return MAKE_FUNC_NAME(OP_WAIT_PORT);
        case Operator::OP_WRITE_PORT:
          return MAKE_FUNC_NAME(OP_WRITE_PORT);
        case Operator::OP_LOG:
          return MAKE_FUNC_NAME(OP_LOG);
        case Operator::OP_SET:
          return MAKE_FUNC_NAME(OP_SET);
        case Operator::OP_APPEND:
          return MAKE_FUNC_NAME(OP_APPEND);
        case Operator::OP_BUILD:
          return MAKE_FUNC_NAME(OP_BUILD);
        }
        throw ::std::runtime_error("Unknown operator");
      }
      bool OperatorExpression::isEqual(const Node &node) const
      {
        const OperatorExpression &that = dynamic_cast<const OperatorExpression&>(node);
        return name == that.name && equals(type, that.type)
            && equals(arguments.begin(), arguments.end(), that.arguments.begin(),
                that.arguments.end());
      }
      void OperatorExpression::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix((std::uint64_t) name);
        h.mix(arguments.begin(), arguments.end());
      }

    }
  }
}
