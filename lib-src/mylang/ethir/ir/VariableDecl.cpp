#include <mylang/ethir/ir/VariableDecl.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      VariableDecl::VariableDecl(Variable::VariablePtr xval, Variable::VariablePtr xexpr,
          StatementPtr xnext)
          : Declaration(xval->scope, xval->name, xval->type, xnext), variable(xval), value(xexpr)
      {
        assert(!value || value->type->isSameType(*variable->type));
        assert(xval->scope == FUNCTION_SCOPE);
      }

      VariableDecl::~VariableDecl()
      {
      }

      VariableDecl::VariableDeclPtr VariableDecl::create(Variable::VariablePtr xval,
          Variable::VariablePtr xvalue, StatementPtr xnext)
      {
        return ::std::make_shared<VariableDecl>(xval, xvalue, xnext);
      }

      VariableDecl::VariableDeclPtr VariableDecl::create(Variable::VariablePtr xval,
          StatementPtr xnext)
      {
        return ::std::make_shared<VariableDecl>(xval, nullptr, xnext);
      }

      void VariableDecl::accept(NodeVisitor &visitor) const
      {
        visitor.visitVariableDecl(self<VariableDecl>());
      }

      Statement::StatementPtr VariableDecl::replaceNext(StatementPtr tail) const
      {
        return create(variable, value, tail);
      }
      bool VariableDecl::isEqual(const Node &node) const
      {
        const VariableDecl &that = dynamic_cast<const VariableDecl&>(node);
        return equals(variable, that.variable) && equals(value, that.value)
            && equals(next, that.next);
      }
      void VariableDecl::computeHashCode(HashCode &h) const
      {
        h.mix(typeid(*this));
        h.mix(variable);
        h.mix(value);
        h.mix(next);
      }
    }
  }
}
