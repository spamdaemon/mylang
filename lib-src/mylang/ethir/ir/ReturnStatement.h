#ifndef CLASS_MYLANG_ETHIR_IR_RETURNSTATEMENT_H
#define CLASS_MYLANG_ETHIR_IR_RETURNSTATEMENT_H

#ifndef CLASS_MYLANG_ETHIR_IR_CONTROLFLOWSTATEMENT_H
#include <mylang/ethir/ir/ControlFlowStatement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * Assign a value to a variable that is in scope.
       */
      class ReturnStatement: public ControlFlowStatement
      {
        ReturnStatement(const ReturnStatement &e) = delete;

        ReturnStatement& operator=(const ReturnStatement &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const ReturnStatement> ReturnStatementPtr;

        /**
         * Crea
         * @param name a name
         * @param value the value to be bound
         */
      public:
        ReturnStatement(Variable::VariablePtr value);

        /** Destructor */
      public:
        ~ReturnStatement();

        /**
         * Updateare a mutable variable with the specified initializer
         * @param value the value to be bound
         */
      public:
        static ReturnStatementPtr create(Variable::VariablePtr value);

        /**
         * Return.
         */
      public:
        static ReturnStatementPtr create();

      public:
        void accept(NodeVisitor &visitor) const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /**
         * The bound value (may be null!)
         */
      public:
        const Variable::VariablePtr value;
      };
    }
  }
}
#endif
