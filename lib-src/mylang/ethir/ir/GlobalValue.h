#ifndef CLASS_MYLANG_ETHIR_IR_GLOBALVALUE_H
#define CLASS_MYLANG_ETHIR_IR_GLOBALVALUE_H

#ifndef CLASS_MYLANG_ETHIR_IR_AHSTRACTVALUEDECLARATION_H
#include <mylang/ethir/ir/AbstractValueDeclaration.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_EXPRESSION_H
#include <mylang/ethir/ir/Expression.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * A variable declaration that cannot be modified.
       */
      class GlobalValue: public AbstractValueDeclaration
      {
        GlobalValue(const GlobalValue&) = delete;

        GlobalValue& operator=(const GlobalValue&) = delete;

      public:
        enum Kind
        {
          /** A normal global value */
          DEFAULT = 0,

          /** A global value whose name is well and needs to remain fixed. */
          WELL_KNOWN = 1,

          /** A global value that is exported. */
          EXPORTED = 3
        };

        /** A value pointer */
      public:
        typedef ::std::shared_ptr<const GlobalValue> GlobalValuePtr;

        /**
         * Create a bind statement.
         * @param name a name
         * @param value the value to be bound
         */
      public:
        GlobalValue(Variable::VariablePtr var, Kind kind, Expression::ExpressionPtr value);

        /** Destructor */
      public:
        ~GlobalValue();

        /**
         * Declare a global value with the specified initializer.
         * @param name a name
         * @param value the value to be bound
         * @parma kind the kind of global value
         */
      public:
        static GlobalValuePtr create(Variable::VariablePtr var, Expression::ExpressionPtr value,
            Kind kind);

        /**
         * Declare a mutable variable with the specified initializer
         * @param name a name
         * @param value the value to be bound
         * @param kind the kind of global value
         */
      public:
        static GlobalValuePtr create(const Name &name, Expression::ExpressionPtr value, Kind kind);

      public:
        void accept(NodeVisitor &visitor) const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The kind of global value */
      public:
        const Kind kind;
      };
    }

  }
}
#endif
