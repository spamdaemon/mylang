#ifndef CLASS_MYLANG_ETHIR_IR_LITERALOPTIONAL_H
#define CLASS_MYLANG_ETHIR_IR_LITERALOPTIONAL_H

#ifndef CLASS_MYLANG_ETHIR_IR_LITERAL_H
#include <mylang/ethir/ir/Literal.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_OPTTYPE_H
#include <mylang/ethir/types/OptType.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * This class represent the constructor for a literal.
       */
      class LiteralOptional: public Literal
      {
        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const LiteralOptional> LiteralOptionalPtr;

        /**
         * Create an expression that returns a literal value.
         * @param type the type
         * @aparam value the value of the optional or null if nil
         */
      public:
        LiteralOptional(::std::shared_ptr<const types::OptType> type, LiteralPtr value);

        /** Destructor */
      public:
        ~LiteralOptional();

        /**
         * Create an optional
         * @param value the value that is optional or nullptr if nil
         * @return a literal
         */
      public:
        static LiteralOptionalPtr create(::std::shared_ptr<const types::OptType> type,
            LiteralPtr value);

        /**
         * Create an bit literal
         * @return a literal
         */
      public:
        static LiteralOptionalPtr create(::std::shared_ptr<const types::OptType> type);

      public:
        void accept(NodeVisitor &visitor) const override final;
        ::std::optional<int> compare(const Literal &that) const override;
        LiteralPtr castTo(const EType &newType) const override;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The optional value, or nullptr if this is a nil optional */
      public:
        const LiteralPtr value;
      };
    }

  }
}
#endif
