#include <mylang/ethir/ethir.h>
#include <mylang/ethir/HashCode.h>
#include <mylang/ethir/ir/Declaration.h>
#include <mylang/ethir/ir/LetExpression.h>
#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/ir/Variable.h>
#include <mylang/ethir/Name.h>
#include <mylang/ethir/TypeMismatchError.h>
#include <cassert>
#include <functional>
#include <memory>

namespace mylang {
  namespace ethir {
    namespace ir {

      LetExpression::LetExpression(EVariable xval, EExpression xexpr, EExpression xresult)
          : Expression(xresult->type), variable(xval), value(xexpr), result(xresult)
      {
        TypeMismatchError::check(variable->type, value->type);
      }

      LetExpression::~LetExpression()
      {
      }

      LetExpression::LetExpressionPtr LetExpression::create(EVariable xval, EExpression xvalue,
          EExpression xresult)
      {
        return ::std::make_shared<LetExpression>(xval, xvalue, xresult);
      }

      LetExpression::LetExpressionPtr LetExpression::create(const Name &xval, EExpression xvalue,
          EExpression xresult)
      {
        EVariable var = Variable::create(Variable::Scope::FUNCTION_SCOPE, xvalue->type, xval);
        return ::std::make_shared<LetExpression>(var, xvalue, xresult);
      }

      LetExpression::LetExpressionPtr LetExpression::create(EExpression xvalue,
          ::std::function<EExpression(EVariable)> in)
      {
        EVariable var = Variable::create(Variable::Scope::FUNCTION_SCOPE, xvalue->type,
            Name::create("let"));
        return create(var, xvalue, in(var));
      }

      EExpression LetExpression::create(const ::std::vector<EExpression> xvalues,
          ::std::function<EExpression(::std::vector<EVariable>)> in)
      {
        ::std::vector<EVariable> vars;
        for (auto expr : xvalues) {
          vars.push_back(
              Variable::create(Variable::Scope::FUNCTION_SCOPE, expr->type, Name::create("let")));
        }
        EExpression res = in(vars);
        for (size_t i = vars.size(); i-- > 0;) {
          res = create(vars.at(i), xvalues.at(i), res);
        }
        return res;
      }

      void LetExpression::accept(NodeVisitor &visitor) const
      {
        visitor.visitLetExpression(self<LetExpression>());
      }
      bool LetExpression::isEqual(const Node &node) const
      {
        const LetExpression &that = dynamic_cast<const LetExpression&>(node);
        return equals(type, that.type) && equals(variable, that.variable)
            && equals(value, that.value) && equals(result, that.result);

      }
      void LetExpression::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(variable);
        h.mix(value);
        h.mix(result);
      }

    }

  }
}
