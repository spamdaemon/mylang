#include <mylang/ethir/ir/ReturnStatement.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      ReturnStatement::ReturnStatement(Variable::VariablePtr xexpr)
          : ControlFlowStatement(nullptr), value(xexpr)
      {
      }

      ReturnStatement::~ReturnStatement()
      {
      }

      ReturnStatement::ReturnStatementPtr ReturnStatement::create()
      {
        return ::std::make_shared<ReturnStatement>(nullptr);
      }

      ReturnStatement::ReturnStatementPtr ReturnStatement::create(Variable::VariablePtr xvalue)
      {
        return ::std::make_shared<ReturnStatement>(xvalue);
      }

      void ReturnStatement::accept(NodeVisitor &visitor) const
      {
        visitor.visitReturnStatement(self<ReturnStatement>());
      }
      Statement::StatementPtr ReturnStatement::replaceNext(StatementPtr) const
      {
        return self<Statement>();
      }
      bool ReturnStatement::isEqual(const Node &node) const
      {
        const ReturnStatement &that = dynamic_cast<const ReturnStatement&>(node);
        return equals(value, that.value) && equals(next, that.next);
      }

      void ReturnStatement::computeHashCode(HashCode &h) const
      {
        h.mix(typeid(*this));
        h.mix(value);
        h.mix(next);
      }

    }

  }
}
