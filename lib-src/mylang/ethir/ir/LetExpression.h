#ifndef CLASS_MYLANG_ETHIR_IR_LETEXPRESSION_H
#define CLASS_MYLANG_ETHIR_IR_LETEXPRESSION_H

#include <memory>

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_EXPRESSION_H
#include <mylang/ethir/ir/Expression.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#include <functional>

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * A variable declaration that cannot be modified.
       */
      class LetExpression: public Expression
      {
        LetExpression(const LetExpression&) = delete;

        LetExpression& operator=(const LetExpression&) = delete;

        /** A value pointer */
      public:
        typedef ::std::shared_ptr<const LetExpression> LetExpressionPtr;

        /**
         * Create a bind statement.
         * @param name a name
         * @param value the value to be bound
         */
      public:
        LetExpression(EVariable val, EExpression value, EExpression result);

        /** Destructor */
      public:
        ~LetExpression();

        /**
         * Declare a mutable variable with the specified initializer
         * @param name a name
         * @param value the value to be bound
         */
      public:
        static LetExpressionPtr create(EVariable val, EExpression value, EExpression result);

        /**
         * Declare a mutable variable with the specified initializer
         * @param name a name
         * @param value the value to be bound
         */
      public:
        static LetExpressionPtr create(const Name &name, EExpression value, EExpression result);

        /**
         * Bind an expression to a variable name and create a new expression based on that.
         * @param value an expression value
         * @param a function that produces an expression based on the bound value
         */
      public:
        static LetExpressionPtr create(EExpression value,
            ::std::function<EExpression(EVariable)> in);

        /**
         * Bind an expression to a variable name and create a new expression based on that.
         * @param value an expression value
         * @param a function that produces an expression based on the bound value
         */
      public:
        static EExpression create(const ::std::vector<EExpression> values,
            ::std::function<EExpression(::std::vector<EVariable>)> in);

      public:
        void accept(NodeVisitor &visitor) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /**
         * The name and type of the local variable that was introduced.
         */
      public:
        const EVariable variable;

        /**
         * The bound value (never null)
         */
      public:
        const EExpression value;

        /** The expression to be evaluate with the let-binding */
      public:
        const EExpression result;
      };
    }

  }
}
#endif
