#include <mylang/ethir/ir/ProcessConstructor.h>
#include <mylang/ethir/ir/NodeVisitor.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      ProcessConstructor::ProcessConstructor(Parameters xparameters,
          OwnConstructorCall::OwnConstructorCallPtr fwdCtor,
          StatementBlock::StatementBlockPtr xbody)
          : Statement(nullptr), parameters(xparameters), callConstructor(fwdCtor), body(xbody)
      {
      }

      ProcessConstructor::~ProcessConstructor()
      {
      }

      ProcessConstructor::ProcessConstructorPtr ProcessConstructor::create(Parameters xparameters,
          OwnConstructorCall::OwnConstructorCallPtr fwdCtor, Statement::StatementPtr xbody)
      {
        return ::std::make_shared<ProcessConstructor>(xparameters, fwdCtor,
            StatementBlock::singleton(xbody));
      }

      void ProcessConstructor::accept(NodeVisitor &visitor) const
      {
        visitor.visitProcessConstructor(self<ProcessConstructor>());
      }
      Statement::StatementPtr ProcessConstructor::replaceNext(StatementPtr) const
      {
        return self<Statement>();
      }
      bool ProcessConstructor::isEqual(const Node &node) const
      {
        const ProcessConstructor &that = dynamic_cast<const ProcessConstructor&>(node);
        return equals(parameters.begin(), parameters.end(), that.parameters.begin(),
            that.parameters.end()) && equals(callConstructor, that.callConstructor)
            && equals(body, that.body) && equals(next, that.next);
      }
      void ProcessConstructor::computeHashCode(HashCode &h) const
      {
        h.mix(parameters.begin(), parameters.end());
        h.mix(callConstructor);
        h.mix(body);
        h.mix(next);
      }
    }
  }
}
