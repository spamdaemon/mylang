#ifndef CLASS_MYLANG_ETHIR_IR_GETDISCRIMINANT_H
#define CLASS_MYLANG_ETHIR_IR_GETDISCRIMINANT_H

#ifndef MYLANGCLASS_MYLANG_ETHIR_NAME_H
#include <mylang/ethir/Name.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#include <mylang/ethir/types/Type.h>
#endif
#include <vector>

namespace mylang {
  namespace ethir {
    namespace ir {
      class GetDiscriminant: public Expression
      {

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const GetDiscriminant> GetDiscriminantPtr;

        /**
         * Create an expression gets the specified member variable.
         * @param type the return type
         * @param obj object on which to invoke the method name
         * @param name the name of the member to retrieve
         */
      public:
        GetDiscriminant(TypePtr t, Variable::VariablePtr obj);

        /**destructor */
      public:
        ~GetDiscriminant();

        /**
         * Create a function call.
         * @param type the return type
         * @param name the name of the function
         * @param args the arguments
         * @return a function call ptr/
         */
      public:
        static GetDiscriminantPtr create(TypePtr t, Variable::VariablePtr obj);

        void accept(NodeVisitor &visitor) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The object on which to invoke the method */
      public:
        const Variable::VariablePtr object;
      };

    }
  }
}
#endif
