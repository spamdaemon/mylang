#include "LiteralPrimitive.h"

#include <algorithm>
#include <memory>
#include <string>

#include "../types/Type.h"

namespace mylang {
  namespace ethir {
    namespace ir {

      LiteralPrimitive::LiteralPrimitive(TypePtr xtype, ::std::string xtext)
          : Literal(::std::move(xtype)), text(xtext)
      {
      }

      LiteralPrimitive::~LiteralPrimitive()
      {
      }

      ::std::optional<int> LiteralPrimitive::compare(const Literal &other) const
      {
        const LiteralPrimitive *that = dynamic_cast<const LiteralPrimitive*>(&other);
        return that && text == that->text && type->isSameType(*that->type);
      }

    }
  }
}
