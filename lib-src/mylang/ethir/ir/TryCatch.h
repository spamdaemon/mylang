#ifndef CLASS_MYLANG_ETHIR_IR_TRYCATCH_H
#define CLASS_MYLANG_ETHIR_IR_TRYCATCH_H

#ifndef CLASS_MYLANG_ETHIR_IR_CONTROLFLOWSTATEMENT_H
#include <mylang/ethir/ir/ControlFlowStatement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENTBLOCK_H
#include <mylang/ethir/ir/StatementBlock.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * A variable declaration. Variables are mutable!
       */
      class TryCatch: public ControlFlowStatement
      {
        TryCatch(const TryCatch &e) = delete;

        TryCatch& operator=(const TryCatch &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const TryCatch> TryCatchPtr;

        /**
         * Create a try catch statment
         * @param tryBody the statements in the try body
         * @param catchBody the statements in the catch body
         */
      public:
        TryCatch(StatementBlock::StatementBlockPtr tryBody,
            StatementBlock::StatementBlockPtr catchBody, StatementPtr next);

        /** Destructor */
      public:
        ~TryCatch();

        /**
         * Declare a mutable variable with the specified initializer
         * @param cond the condition
         * @param body body of the statement
         */
      public:
        static StatementPtr create(Statement::StatementPtr tryBody,
            Statement::StatementPtr catchBody, StatementPtr next);

      public:
        void accept(NodeVisitor &visitor) const override final;
        bool isNOP() const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /**
         * The statement to be executed if an exception is thrown
         */
      public:
        const StatementBlock::StatementBlockPtr tryBody;

        /**
         * The statement to be executed if an exception is thrown
         */
      public:
        const StatementBlock::StatementBlockPtr catchBody;
      };
    }
  }
}
#endif
