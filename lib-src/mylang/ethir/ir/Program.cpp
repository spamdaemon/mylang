#include <mylang/ethir/ir/Program.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/HashCode.h>
#include <mylang/ethir/ir/GlobalValue.h>
#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/ProcessDecl.h>
#include <mylang/ethir/Name.h>
#include <mylang/ethir/types/types.h>
#include <mylang/ethir/types/AbstractTypeVisitor.h>
#include <algorithm>
#include <memory>
#include <vector>

namespace mylang {
  namespace ethir {
    namespace ir {

      namespace {
        static Program::PublicTypes findPublicTypes(const Program::Globals &globals)
        {
          struct V: public types::AbstractTypeVisitor
          {
            V()
                : types::AbstractTypeVisitor(true)
            {
            }
            ~V()
            {
            }

            void visit(const ::std::shared_ptr<const types::NamedType> &type) override
            {
              result.insert(type->name());
              types::AbstractTypeVisitor::visit(type);
            }

            Program::PublicTypes result;
          };

          V v;
          for (const auto &g : globals) {
            if (g->kind == GlobalValue::EXPORTED) {
              g->type->accept(v);
            }
          }

          return ::std::move(v.result);
        }
      }

      Program::Program(Globals xglobals, Processes xprocesses, NamedTypes xtypes,
          PublicTypes xpublicTypes)
          : Statement(nullptr), globals(::std::move(xglobals)), processes(::std::move(xprocesses)),
              types(::std::move(xtypes)), publicTypes(::std::move(xpublicTypes))
      {
      }

      Program::~Program()
      {
      }

      Program::ProgramPtr Program::create(Globals xglobals, Processes xprocesses, NamedTypes xtypes)
      {
        auto publicTypes = findPublicTypes(xglobals);
        return ::std::make_shared<Program>(::std::move(xglobals), ::std::move(xprocesses),
            ::std::move(xtypes), ::std::move(publicTypes));
      }

      void Program::accept(NodeVisitor &visitor) const
      {
        visitor.visitProgram(self<Program>());
      }

      Statement::StatementPtr Program::replaceNext(StatementPtr) const
      {
        return self<Statement>();
      }

      GlobalValue::GlobalValuePtr Program::findGlobal(const Name &name) const
      {
        for (auto g : globals) {
          if (g->name == name) {
            return g;
          }
        }
        return nullptr;
      }

      ProcessDecl::ProcessDeclPtr Program::findProcess(const Name &name) const
      {
        for (auto g : processes) {
          if (g->name == name) {
            return g;
          }
        }
        return nullptr;
      }
      bool Program::isEqual(const Node &node) const
      {
        const Program &that = dynamic_cast<const Program&>(node);
        return equals(globals.begin(), globals.end(), that.globals.begin(), that.globals.end())
            && equals(processes.begin(), processes.end(), that.processes.begin(),
                that.processes.end())
            && equals(types.begin(), types.end(), that.types.begin(), that.types.end())
            && equals(next, that.next);
      }
      void Program::computeHashCode(HashCode &h) const
      {
        h.mix(globals.begin(), globals.end());
        h.mix(processes.begin(), processes.end());
        h.mix(types.begin(), types.end());
        h.mix(next);
      }

    }
  }
}
