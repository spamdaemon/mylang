#ifndef CLASS_MYLANG_ETHIR_IR_LITERALPRIMITIVE_H
#define CLASS_MYLANG_ETHIR_IR_LITERALPRIMITIVE_H

#ifndef CLASS_MYLANG_ETHIR_IR_LITERAL_H
#include <mylang/ethir/ir/Literal.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * This class represent the constructor for a literal.
       */
      class LiteralPrimitive: public Literal
      {
        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const LiteralPrimitive> LiteralPrimitivePtr;

        /**
         * Create an expression that returns a literal value.
         * @param type the type
         * @aparam value the string value for the literal
         */
      public:
        LiteralPrimitive(TypePtr t, ::std::string text);

        /** Destructor */
      public:
        virtual ~LiteralPrimitive() = 0;

      public:
        ::std::optional<int> compare(const Literal &that) const override;

        /** The text representation for the value */
      public:
        const ::std::string text;
      };
    }
  }
}

#endif
