#ifndef CLASS_MYLANG_ETHIR_IR_OPAQUEEXPRESSION_H
#define CLASS_MYLANG_ETHIR_IR_OPAQUEEXPRESSION_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_EXPRESSION_H
#include <mylang/ethir/ir/Expression.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#include <vector>

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * A variable declaration that cannot be modified.
       */
      class OpaqueExpression: public Expression
      {
        OpaqueExpression(const OpaqueExpression&) = delete;

        OpaqueExpression& operator=(const OpaqueExpression&) = delete;

        /** A value pointer */
      public:
        typedef ::std::shared_ptr<const OpaqueExpression> OpaqueExpressionPtr;

        /** A list of variables */
      public:
        typedef ::std::vector<EVariable> Variables;

        /**
         * Create an opaque expression that wraps the specified expression
         * @param expression an expression
         */
      public:
        OpaqueExpression(EExpression expr);

        /**
         * Create an opaque expression that wraps the specified expression
         * @param src  an expression
         * @apram args the new arguments
         */
      public:
        OpaqueExpression(const OpaqueExpression &src, Variables args);

        /** Destructor */
      public:
        ~OpaqueExpression();

        /**
         * Create an opaque expression.
         * @param e an expression
         * @return an opaque expression
         */
      public:
        static OpaqueExpressionPtr create(EExpression e);

        /**
         * Create a new opaque expression by changing the arguments.
         * @param args the new arguments
         * @return a new opaque expression
         */
      public:
        OpaqueExpressionPtr changeArguments(Variables args) const;

        /**
         * Turn this opaque expression back into the real expression.
         * @return an expression
         */
      public:
        EExpression restore() const;

      public:
        void accept(NodeVisitor &visitor) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /**
         * The arguments to this opaque expression
         */
      public:
        const Variables arguments;

        /** The free variables of the expression */
      public:
        const Variables freeVariables;

        /** The original expression */
      public:
        const EExpression expression;
      };
    }

  }
}
#endif
