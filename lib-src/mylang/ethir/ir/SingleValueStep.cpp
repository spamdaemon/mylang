#include <mylang/ethir/ir/SingleValueStep.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/ir/YieldStatement.h>
#include <mylang/ethir/ir/SkipStatement.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      SingleValueStep::SingleValueStep(EVariable xvalue, EStatement xbody)
          : value(xvalue), body(xbody)
      {
        assert(value && body);
      }

      SingleValueStep::~SingleValueStep()
      {
      }

      SingleValueStep::SingleValueStepPtr SingleValueStep::create(EVariable xvalue,
          EStatement xbody)
      {
        return ::std::make_shared<SingleValueStep>(::std::move(xvalue), ::std::move(xbody));
      }

      SingleValueStep::SingleValueStepPtr SingleValueStep::create(EVariable xvalue,
          Loop::State nextState)
      {
        auto skip = SkipStatement::create( { }, nextState);

        return create(xvalue, YieldStatement::create(xvalue, skip));
      }

      bool SingleValueStep::isEqual(const Node &node) const
      {
        const SingleValueStep &that = dynamic_cast<const SingleValueStep&>(node);
        return Node::equals(value, that.value) && Node::equals(body, that.body);
      }

      void SingleValueStep::computeHashCode(HashCode &h) const
      {
        h.mix(value);
        h.mix(body);
      }

      void SingleValueStep::accept(NodeVisitor &visitor) const
      {
        visitor.visitSingleValueStep(self<SingleValueStep>());
      }

      Loop::Expressions SingleValueStep::getInitializers() const
      {
        return {};
      }

      EStatement SingleValueStep::toStatement() const
      {
        return body;
      }

    }
  }
}
