#include <mylang/ethir/ir/NewProcess.h>
#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      NewProcess::NewProcess(TypePtr t, const Name &pname, const Name &ctorName,
          ::std::vector<Variable::VariablePtr> value)
          : Expression(::std::move(t)), processName(pname), constructorName(ctorName),
              arguments(value)
      {
      }

      NewProcess::~NewProcess()
      {
      }

      NewProcess::NewProcessPtr NewProcess::create(TypePtr t, const Name &pname,
          const Name &ctorName, ::std::vector<Variable::VariablePtr> value)
      {
        return ::std::make_shared<NewProcess>(t, pname, ctorName, value);
      }

      void NewProcess::accept(NodeVisitor &visitor) const
      {
        visitor.visitNewProcess(self<NewProcess>());
      }
      bool NewProcess::isEqual(const Node &node) const
      {
        const NewProcess &that = dynamic_cast<const NewProcess&>(node);
        return processName == that.processName && constructorName == that.constructorName
            && equals(type, that.type)
            && equals(arguments.begin(), arguments.end(), that.arguments.begin(),
                that.arguments.end());
      }
      void NewProcess::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(processName);
        h.mix(constructorName);
        h.mix(arguments.begin(), arguments.end());
      }

    }

  }
}
