#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENTBLOCK_H
#define CLASS_MYLANG_ETHIR_IR_STATEMENTBLOCK_H

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENT_H
#include <mylang/ethir/ir/Statement.h>
#endif

#include <vector>

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * A statement block contains a sequence of statements, none of
       * which will have be a statement block.
       * A block will not have a next statement.
       */
      class StatementBlock: public Statement
      {
        StatementBlock(const StatementBlock &e) = delete;

        StatementBlock& operator=(const StatementBlock &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const StatementBlock> StatementBlockPtr;

        /**
         * Create a statement block
         * @param statements the statements in the block
         */
      public:
        StatementBlock(Statement::StatementPtr statements);

        /** Destructor */
      public:
        ~StatementBlock();

        /**
         * Create an empty statement block.
         * @return a statement block
         */
      public:
        static StatementBlockPtr empty()
        {
          return singleton(nullptr);
        }

        /**
         * Create a statement block containing the specified statements. If
         * there is only a single statement, and it is a StatementBlock, then that statement block
         * is returned and new one is not created!
         * @param statements the statements in the block
         * @return a statement block
         */
      public:
        static StatementBlockPtr create(Statement::StatementPtr statements,
            Statement::StatementPtr next);

        /**
         * Create a statement block that that no next.
         * @param statements the statements in the block
         * @return a statement block
         */
      public:
        static StatementBlockPtr singleton(Statement::StatementPtr statements);

      public:
        void accept(NodeVisitor &visitor) const override final;
        bool isNOP() const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /**
         * Get the first statement that is not also block
         */
      public:
        StatementPtr firstStatement() const override final;

        /**
         * Get the last statement of this block.
         */
      public:
        StatementPtr lastStatement() const override final;

        /**
         * The name and type of the local variable that was introduced.
         */
      public:
        const Statement::StatementPtr statements;
      };

    }
  }
}
#endif
