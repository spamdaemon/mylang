#include <mylang/ethir/ir/YieldStatement.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      YieldStatement::YieldStatement(Variable::VariablePtr xvalue,
          SkipStatement::SkipStatementPtr xSkip)
          : Statement(xSkip), value(xvalue), skip(xSkip)
      {
      }

      YieldStatement::~YieldStatement()
      {
      }

      YieldStatement::YieldStatementPtr YieldStatement::create(Variable::VariablePtr xvalue,
          SkipStatement::SkipStatementPtr xSkip)
      {
        return ::std::make_shared<YieldStatement>(xvalue, ::std::move(xSkip));
      }

      void YieldStatement::accept(NodeVisitor &visitor) const
      {
        visitor.visitYieldStatement(self<YieldStatement>());
      }
      Statement::StatementPtr YieldStatement::replaceNext(StatementPtr) const
      {
        return self<Statement>();
      }
      bool YieldStatement::isEqual(const Node &node) const
      {
        const YieldStatement &that = dynamic_cast<const YieldStatement&>(node);
        return equals(value, that.value) && equals(skip, that.skip);
      }

      void YieldStatement::computeHashCode(HashCode &h) const
      {
        h.mix(typeid(*this));
        h.mix(value);
        h.mix(skip);
      }

    }

  }
}
