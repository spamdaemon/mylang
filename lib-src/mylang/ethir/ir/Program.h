#ifndef CLASS_MYLANG_ETHIR_IR_PROGRAM_H
#define CLASS_MYLANG_ETHIR_IR_PROGRAM_H

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENT_H
#include <mylang/ethir/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_PROCESSDECL_H
#include <mylang/ethir/ir/ProcessDecl.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_GLOBALVALUE_H
#include <mylang/ethir/ir/GlobalValue.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPENAME_H
#include <mylang/ethir/TypeName.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_NAMEDTYPE_H
#include <mylang/ethir/types/NamedType.h>
#endif

#include <set>

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * A variable declaration. Variables are mutable!
       */
      class Program: public Statement
      {
        Program(const Program &e) = delete;
        Program& operator=(const Program &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const Program> ProgramPtr;

        /** Values */
      public:
        typedef ::std::vector<GlobalValue::GlobalValuePtr> Globals;

        /** The processes */
      public:
        typedef ::std::vector<ProcessDecl::ProcessDeclPtr> Processes;

        /** The named types */
      public:
        typedef ::std::vector<mylang::ethir::types::NamedType::NamedTypePtr> NamedTypes;

        /** The named types */
      public:
        typedef ::std::set<TypeName> PublicTypes;

        /**
         * Create a program
         * @param functions the global functions
         * @param values the global values
         * @param processes the defined processes
         * @param publicTypes types used in public facing functions
         */
      public:
        Program(Globals globals, Processes processes, NamedTypes types, PublicTypes publicTypes);

        /** Destructor */
      public:
        ~Program();

        /**
         * Create a program
         * @param functions the global functions
         * @param values the global values
         * @param processes the defined processes
         */
      public:
        static ProgramPtr create(Globals globals, Processes processes, NamedTypes types);

        /**
         * Find the global with the specified name.
         * @param name the name of a global
         * @return the global or nullptr if not found
         */
      public:
        GlobalValue::GlobalValuePtr findGlobal(const Name &name) const;

        /**
         * Find the process with the specified name.
         * @param name the name of a process
         * @return the process or nullptr if not found
         */
      public:
        ProcessDecl::ProcessDeclPtr findProcess(const Name &name) const;

      public:
        void accept(NodeVisitor &visitor) const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** Global values, functions, and procedures  */
      public:
        const Globals globals;

        /** The process declarations  */
      public:
        const Processes processes;

        /** The named types */
      public:
        const NamedTypes types;

        /** Types of type names that are used in externally facing functions */
      public:
        PublicTypes publicTypes;
      };
    }

  }
}
#endif
