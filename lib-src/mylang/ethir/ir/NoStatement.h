#ifndef CLASS_MYLANG_ETHIR_IR_NOSTATEMENT_H
#define CLASS_MYLANG_ETHIR_IR_NOSTATEMENT_H

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENT_H
#include <mylang/ethir/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * Assign a value to a variable that is in scope.
       */
      class NoStatement: public Statement
      {
        NoStatement(const NoStatement &e) = delete;

        NoStatement& operator=(const NoStatement &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const NoStatement> NoStatementPtr;

        /**
         * Crea
         * @param name a name
         * @param value the value to be bound
         */
      public:
        NoStatement();

        /** Destructor */
      public:
        ~NoStatement();

        /**
         * Create a NoStatement node.
         * This function create a NoStatement node, unless the next pointer
         * is not nullptr. If the next pointer is not a nullptr, then next
         * is returned.
         *
         * @param next the following statement
         * @return a NoStatement node, or next if next != null
         */
      public:
        static StatementPtr create(StatementPtr next);

      public:
        void accept(NodeVisitor &visitor) const override final;
        bool isNOP() const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

      };
    }
  }
}
#endif
