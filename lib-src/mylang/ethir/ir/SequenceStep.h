#ifndef CLASS_MYLANG_ETHIR_IR_SEQUENCESTEP_H
#define CLASS_MYLANG_ETHIR_IR_SEQUENCESTEP_H

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENT_H
#include <mylang/ethir/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENTBUILDER_H
#include <mylang/ethir/ir/StatementBuilder.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_SKIPSTATEMENT_H
#include <mylang/ethir/ir/SkipStatement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_LOOP_H
#include <mylang/ethir/ir/Loop.h>
#endif

#include <functional>

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * A loop step that loops over the elements in an array in reverse or forward
       * order and yields each element.
       */
      class SequenceStep: public Loop::Step
      {
        SequenceStep(const SequenceStep &e) = delete;

        SequenceStep& operator=(const SequenceStep &e) = delete;

        /**
         * A body builder
         * @param counter the current value of the counter
         * @param updates the updates to be applied when the step is done
         * @param b a builder */
      public:
        typedef ::std::function<Loop::Expressions(EVariable, StatementBuilder&)> Builder;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const SequenceStep> SequenceStepPtr;

        /**
         * A new skip statement
         * @param any updates to apply to the loop variables
         */
      public:
        SequenceStep(SkipStatement::SkipStatementPtr nextState, EVariable iterationCount,
            EVariable counter, Loop::Expressions initializers, EStatement body);

        /** Destructor */
      public:
        ~SequenceStep();

        /**
         * Loop for a maximum number of iterations.
         * @param iterationCount the variable that holds the maximum number of values to iterate
         * @param counter the current iteration count
         * @param body the body of the iteration
         */
      public:
        static SequenceStepPtr create(SkipStatement::SkipStatementPtr nextState,
            EVariable iterationCount, EVariable counter, Loop::Expressions initializers,
            EStatement body);

        /**
         * Loop for a maximum number of iterations.
         * @param iterationCount the variable that holds the maximum number of values to iterate
         * @param nextState the next state of the enclosing loop
         * @param b a builder that produces the body
         */
      public:
        static SequenceStepPtr create(EVariable iterationCount, Loop::State nextState, Builder b);

        /**
         * Loop for a maximum number of iterations.
         * @param iterationCount the variable that holds the maximum number of values to iterate
         * @param b a builder that produces the body
         */
      public:
        static SequenceStepPtr create(EVariable iterationCount, Builder b);

      public:
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &that) const override final;
        void accept(NodeVisitor &visitor) const override final;

        Loop::Expressions getInitializers() const override final;
        EStatement toStatement() const override final;

        /** The skip statement to get to the next state. */
      public:
        const SkipStatement::SkipStatementPtr nextState;

        /** The variable that defines how may iterations to run. The type
         * of the variable must be unsigned */
      public:
        const EVariable iterationCount;

        /** The counter holds all values from 0 to iteration count. If the max of the associated
         * integer type is 0, then no iterations will be done. */
      public:
        const EVariable counter;

        /** Local loop initializers. Typically, the only initializer
         * is the counter variable, but other variables may appear as well. */
      public:
        const Loop::Expressions initializers;

        /** The body of this stop */
      public:
        const EStatement body;
      };
    }
  }
}
#endif
