#include <mylang/ethir/ir/Literal.h>
#include <algorithm>

namespace mylang {
  namespace ethir {
    namespace ir {

      Literal::Literal(TypePtr xtype)
          : Expression(::std::move(xtype))
      {
      }

      Literal::~Literal()
      {
      }

      bool Literal::isSameLiteral(const Literal &that) const
      {
        auto cmp = compare(that);
        return cmp.has_value() && cmp.value() == 0;
      }

      Literal::TypePtr Literal::getSourceType() const
      {
        return type;
      }

    }
  }
}
