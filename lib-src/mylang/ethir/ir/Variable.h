#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#define CLASS_MYLANG_ETHIR_IR_VARIABLE_H

#ifndef MYLANGCLASS_MYLANG_ETHIR_NAME_H
#include <mylang/ethir/Name.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_EXPRESSION_H
#include <mylang/ethir/ir/Expression.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_DECLARATION_H
#include <mylang/ethir/ir/Declaration.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_TYPE_H
#include <mylang/ethir/types/Type.h>
#endif

#include <map>
#include <set>

namespace mylang {
  namespace ethir {
    namespace ir {
      class Variable: public Expression
      {
        /** The scope of the variable's declaration */
      public:
        typedef Declaration::Scope Scope;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const Variable> VariablePtr;

        /** The less function defined on variables */
      public:
        struct less
        {
          bool operator()(const VariablePtr &a, const VariablePtr &b) const
          {
            if (a && b) {
              return a->name < b->name;
            }
            return a < b;
          }
        };

        /** An alias for a map indexed by variables */
      public:
        template<class T>
        using Map = ::std::map<VariablePtr,T, less>;

        /** An alias for a set of variables */
      public:
        using Set = ::std::set<VariablePtr,less>;

        /**
         * Create an expression gets the specified member variable.
         * @param type the return type
         * @param obj object on which to invoke the method name
         * @param name the name of the member to retrieve
         */
      public:
        Variable(Scope scope, TypePtr t, Name name);

        /**destructor */
      public:
        ~Variable();

        /**
         * Create a variable
         * @param scope the scope of the variable
         * @param t the type of the variable
         * @param name the name of the variable
         * @return a variable
         */
      public:
        static VariablePtr create(Scope scope, TypePtr t, Name name);

        /**
         * Create a local variable with an auto-generated name.
         * @param type the return type
         * @return a variable
         */
      public:
        static VariablePtr createLocal(TypePtr t);

        /**
         * Rename this variable.
         * @param newName a new name
         * @return a new variable with the same scope and type, but a new name
         */
      public:
        EVariable rename(Name name) const;

        void accept(NodeVisitor &visitor) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The scope in which the variable is defined */
      public:
        const Scope scope;

        /** The function name */
      public:
        const Name name;
      };
    }

  }
}
#endif
