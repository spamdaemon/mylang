#ifndef CLASS_MYLANG_ETHIR_IR_LITERALUNION_H
#define CLASS_MYLANG_ETHIR_IR_LITERALUNION_H

#ifndef CLASS_MYLANG_ETHIR_IR_LITERAL_H
#include <mylang/ethir/ir/Literal.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_UNIONTYPE_H
#include <mylang/ethir/types/UnionType.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * This class represent the constructor for a literal.
       */
      class LiteralUnion: public Literal
      {
        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const LiteralUnion> LiteralUnionPtr;

        /**
         * Create an expression that returns a literal value.
         * @param type the type
         * @aparam value the Union value for the literal
         */
      public:
        LiteralUnion(::std::shared_ptr<const types::UnionType> type, const ::std::string &member,
            LiteralPtr value);

        /** Destructor */
      public:
        ~LiteralUnion();

        /**
         * Create an bit literal
         * @param value true or false
         * @return a literal
         */
      public:
        static LiteralUnionPtr create(::std::shared_ptr<const types::UnionType> type,
            const ::std::string &member, LiteralPtr value);

      public:
        void accept(NodeVisitor &visitor) const override final;
        ::std::optional<int> compare(const Literal &that) const override;
        LiteralPtr castTo(const EType &newType) const override;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The member to be set (equivalent to the discriminant) */
      public:
        const ::std::string member;

        /** The value */
      public:
        const LiteralPtr value;

      };
    }
  }

}
#endif
