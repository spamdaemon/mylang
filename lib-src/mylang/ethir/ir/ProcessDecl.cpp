#include <mylang/ethir/ethir.h>
#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/ir/ProcessDecl.h>
#include <mylang/ethir/types/InputOutputType.h>
#include <mylang/ethir/types/ProcessType.h>
#include <memory>

namespace mylang {
  namespace ethir {
    namespace ir {

      namespace {
        static EType make_process_type(const ProcessDecl::Ports ports)
        {
          mylang::ethir::types::ProcessType::Ports P;

          for (auto p : ports) {
            auto ty = p->type->self<mylang::ethir::types::InputOutputType>();
            P[p->name.source()->localName()] = ty;
          }

          return mylang::ethir::types::ProcessType::get(P);
        }
      }

      ProcessDecl::ProcessDecl(Scope xscope, Name xname,
          ::std::shared_ptr<const types::ProcessType> xtype, Ports xpublicPorts,
          Declarations xvariables, Constructors xconstructors, Blocks xblocks)
          : Declaration(xscope, xname, xtype, nullptr), publicPorts(xpublicPorts),
              variables(xvariables), constructors(xconstructors), blocks(xblocks)
      {
        auto pty = make_process_type(xpublicPorts);
        if (!xtype->isSameType(*pty)) {
          throw ::std::runtime_error(
              "Public ports and process type mismatch; expected " + xtype->toString()
                  + ", but found " + pty->toString());
        }
      }

      ProcessDecl::~ProcessDecl()
      {
      }

      ProcessDecl::ProcessDeclPtr ProcessDecl::create(Scope xscope, Name xname,
          ::std::shared_ptr<const types::ProcessType> xtype, Ports xpublicPorts,
          Declarations xvariables, Constructors xconstructors, Blocks xblocks)
      {
        return ::std::make_shared<ProcessDecl>(xscope, xname, xtype, xpublicPorts, xvariables,
            xconstructors, xblocks);
      }

      void ProcessDecl::accept(NodeVisitor &visitor) const
      {
        visitor.visitProcessDecl(self<ProcessDecl>());
      }
      Statement::StatementPtr ProcessDecl::replaceNext(StatementPtr) const
      {
        return self<Statement>();
      }
      bool ProcessDecl::isEqual(const Node &node) const
      {
        const ProcessDecl &that = dynamic_cast<const ProcessDecl&>(node);
        return equals(publicPorts.begin(), publicPorts.end(), that.publicPorts.begin(),
            that.publicPorts.end())
            && equals(variables.begin(), variables.end(), that.variables.begin(),
                that.variables.end())
            && equals(constructors.begin(), constructors.end(), that.constructors.begin(),
                that.constructors.end())
            && equals(blocks.begin(), blocks.end(), that.blocks.begin(), that.blocks.end())
            && equals(next, that.next);
      }

      void ProcessDecl::computeHashCode(HashCode &h) const
      {
        h.mix(publicPorts.begin(), publicPorts.end());
        h.mix(variables.begin(), variables.end());
        h.mix(constructors.begin(), constructors.end());
        h.mix(blocks.begin(), blocks.end());
        h.mix(next);
      }
    }
  }
}
