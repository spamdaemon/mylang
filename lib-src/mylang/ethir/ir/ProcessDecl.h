#ifndef CLASS_MYLANG_ETHIR_IR_PROCESSDECL_H
#define CLASS_MYLANG_ETHIR_IR_PROCESSDECL_H

#ifndef CLASS_MYLANG_ETHIR_IR_DECLARATION_H
#include <mylang/ethir/ir/Declaration.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_PORTDECL_H
#include <mylang/ethir/ir/PortDecl.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_PROCESSBLOCK_H
#include <mylang/ethir/ir/ProcessBlock.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_PROCESSCONSTRUCTOR_H
#include <mylang/ethir/ir/ProcessConstructor.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_PROCESSTYPE_H
#include <mylang/ethir/types/ProcessType.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_NAMEDTYPE_H
#include <mylang/ethir/types/NamedType.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ir {

      /**
       * A variable declaration. Variables are mutable!
       */
      class ProcessDecl: public Declaration
      {
        ProcessDecl(const ProcessDecl &e) = delete;

        ProcessDecl& operator=(const ProcessDecl &e) = delete;

        /** A type pointer */
      public:
        typedef ::std::shared_ptr<const ProcessDecl> ProcessDeclPtr;

        /** The public I/O ports */
      public:
        typedef ::std::vector<PortDecl::PortDeclPtr> Ports;

        /** A type pointer */
      public:
        typedef ::std::vector<Declaration::DeclarationPtr> Declarations;

        /** A type pointer */
      public:
        typedef ::std::vector<ProcessConstructor::ProcessConstructorPtr> Constructors;

        /** A type pointer */
      public:
        typedef ::std::vector<ProcessBlock::ProcessBlockPtr> Blocks;

        /**
         * Create a process statement.
         * @param name a name
         * @param publicPorts the public ports
         * @parma decls process private declarations
         * @param blocks the processing block
         */
      public:
        ProcessDecl(Scope scope, Name name, ::std::shared_ptr<const types::ProcessType> xtype,
            Ports publicPorts, Declarations variables, Constructors constructors, Blocks blocks);

        /** Destructor */
      public:
        ~ProcessDecl();

        /**
         * Declare a mutable variable with the specified initializer
         * @param name a name
         * @param publicPorts the public ports
         * @parma decls process private declarations
         * @param blocks the processing block
         */
      public:
        static ProcessDeclPtr create(Scope scope, Name name,
            ::std::shared_ptr<const types::ProcessType> xtype, Ports publicPorts,
            Declarations variables, Constructors constructors, Blocks blocks);

      public:
        void accept(NodeVisitor &visitor) const override final;
        StatementPtr replaceNext(StatementPtr stmt) const override final;
        void computeHashCode(HashCode &hc) const override final;
        bool isEqual(const Node &node) const override final;

        /** The public ports */
      public:
        const Ports publicPorts;

        /** Locally defined variables and values */
      public:
        const Declarations variables;

        /** The constructors for this process */
      public:
        const Constructors constructors;

        /** The processing blocks */
      public:
        const Blocks blocks;
      };
    }

  }
}
#endif
