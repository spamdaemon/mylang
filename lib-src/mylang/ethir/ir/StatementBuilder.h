#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENTBUILDER_H
#define CLASS_MYLANG_ETHIR_IR_STATEMENTBUILDER_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_STATEMENT_H
#include <mylang/ethir/ir/Statement.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_EXPRESSION_H
#include <mylang/ethir/ir/Expression.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_LAMBDAEXPRESSION_H
#include <mylang/ethir/ir/LambdaExpression.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_OPERATOREXPRESSION_H
#include <mylang/ethir/ir/OperatorExpression.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#include <functional>
#include <vector>

namespace mylang {
  namespace ethir {
    namespace ir {
      class SkipStatement;
      class YieldStatement;

      /**
       * A builder for a sequence of statements.
       */
      class StatementBuilder
      {
        /** A function that creates a statement given the next statement. */
      public:
        typedef ::std::function<ir::Statement::StatementPtr(ir::Statement::StatementPtr)> Producer;

        typedef ir::LambdaExpression::LambdaExpressionPtr LambdaExpressionPtr;
        typedef ir::LambdaExpression::Parameters LambdaParameters;
        typedef ::std::function<LambdaExpressionPtr(LambdaParameters params)> LambdaProducer;

        /**
         * A simple builder than the producer.
         * The arguments to the builder a passed as variables, and the provided StatementBuilder provides the body.
         * The body of the lambda must contain the correct return statements.
         * The lambda's result type must be returned by the builder.
         */
        typedef ::std::function<EType(StatementBuilder&, ::std::vector<EVariable>)> LambdaBuilder;

        /** A simple builder interface */
      public:
        typedef ::std::function<void(StatementBuilder&)> Builder;

      private:
        typedef ::std::vector<Producer> Producers;

        /** Default constructor */
      public:
        StatementBuilder();

        /** Destructor */
      public:
        virtual ~StatementBuilder();

        /**
         * Append a function that can build a statement. This is the primary function
         * for constructing the eventual statment.
         * @param fn a function that produces a statement
         */
      public:
        void append(Producer fn);

        /**
         * Append a break statement
         */
      public:
        void comment(::std::string message);

        /**
         * Append a abort statement
         */
      public:
        void appendAbort(Variable::VariablePtr message = nullptr);
        void appendAbort(const ::std::string &message);

        /**
         * Append a break statement
         */
      public:
        void appendThrow(Variable::VariablePtr value = nullptr);

        /**
         * Append a break statement
         * @param scope the scope to break out of
         */
      public:
        void appendBreak(Name scope);

        /**
         * Append a continue statement
         * @param scope the scope to break out of
         */
      public:
        void appendContinue(Name scope);

        /**
         * Append a skip statement
         * @param skip  a skip statement
         */
      public:
        void appendSkip(::std::shared_ptr<const SkipStatement> skip);

        /**
         * Append a skip statement that represents a stop.
         */
      public:
        void appendStop();

        /**
         * Append a yield statement
         * @param yield a yield statement
         */
      public:
        void appendYield(::std::shared_ptr<const YieldStatement> yield);

        /**
         * Append a for each statement
         */
      public:
        void appendForEach(Name loopname, Variable::VariablePtr loopvar,
            Variable::VariablePtr loopdata, Statement::StatementPtr body);
        void appendForEach(Name loopname, Variable::VariablePtr loopvar,
            Variable::VariablePtr loopdata, Builder bodyBuilder);

        /**
         * Append a if-statement
         */
      public:
        void appendIf(Variable::VariablePtr cond, Statement::StatementPtr iftrue,
            Statement::StatementPtr iffalse);

        /**
         * Append a if-statement
         */
      public:
        void appendIf(Variable::VariablePtr cond, Builder iftrueBuilder, Builder iffalseBuilder =
            Builder());

        /**
         * Append a statement block
         */
      public:
        void appendBlock(Statement::StatementPtr statements);
        void appendBlock(Builder builder);
        /**
         * Append a loop statment
         */
      public:
        void appendLoop(Name loopname, Statement::StatementPtr body);
        void appendLoop(Name loopname, Builder bodyBuilder);

        /**
         * Append a if-statement
         */
      public:
        void appendReturn(Variable::VariablePtr value = nullptr);

        /**
         * Append a if-statement
         */
      public:
        void appendTryCatch(Statement::StatementPtr tryBody, Statement::StatementPtr catchBody);
        void appendTryCatch(Builder tryBodyBuilder, Builder catchBodyBuilder);

        /**
         * Append a if-statement
         */
      public:
        void appendCallOwnConstrutor(Statement::StatementPtr setup,
            ::std::vector<Variable::VariablePtr> args);

        /**
         * Append an update statement. If the taret is a process variable,
         * then it a ProcessVariableUpdate is created.
         */
      public:
        void appendUpdate(Variable::VariablePtr target, Variable::VariablePtr value);

        /**
         * Declare a value
         */
      public:
        Variable::VariablePtr declareValue(Variable::VariablePtr val,
            Expression::ExpressionPtr value = nullptr);

        /**
         * Declare a value
         */
      public:
        Variable::VariablePtr declareVariable(Variable::VariablePtr val,
            Variable::VariablePtr value = nullptr);

        /**
         * Declare a value.
         * @param name the name of the variable
         * @param value the value to initialize the variable with.
         */
      public:
        Variable::VariablePtr declareLocalValue(const Name &name, Expression::ExpressionPtr value);

        /**
         * Declare a local or temporary value
         * @param value the value to be associated with temporary
         * @return the name of the anonymous variable
         */
      public:
        Variable::VariablePtr declareLocalValue(Expression::ExpressionPtr value);

        /**
         * Declare a local or temporary value
         * @param type the type of the variable
         * @return the name of the anonymous variable
         */
      public:
        Variable::VariablePtr declareLocalValue(mylang::ethir::types::Type::Ptr type);

        /** Declare a local value as the result of an operator expression
         * @param retTy the return type
         * @param op the operator
         * @param args the arguments
         * @return the name of the variable that holds the result
         */
      public:
        Variable::VariablePtr declareLocalValue(const EType &retTy, OperatorExpression::Operator op,
            const OperatorExpression::Arguments &args);

        /** Declare a local value as the result of an operator expression. The type of the expression is automatically determined, if possible.
         * @param op the operator
         * @param args the arguments
         * @return the name of the variable that holds the result
         */
      public:
        Variable::VariablePtr declareLocalValue(OperatorExpression::Operator op,
            const OperatorExpression::Arguments &args);

        /**
         * Declare a local or temporary variable.
         * @param name the name for the variable
         * @param value the value to be associated with the variable.
         * @return the name of the variable
         */
      public:
        Variable::VariablePtr declareLocalVariable(const Name &name, Variable::VariablePtr value);

        /**
         * Declare a local or temporary variable.
         * @param value the value to be associated with the variable.
         * @return the name of the variable
         */
      public:
        Variable::VariablePtr declareLocalVariable(Variable::VariablePtr value);

        /**
         * Declare a process value
         * @param value the value to be associated with temporary
         * @return the name of the anonymous process value
         */
      public:
        Variable::VariablePtr declareProcessValue(Expression::ExpressionPtr value);

        /**
         * Declare a process value
         * @param type the type of the variable
         * @return the name of the anonymous process value
         */
      public:
        Variable::VariablePtr declareProcessValue(mylang::ethir::types::Type::Ptr type);

        /**
         * Declare a process variable.
         * @param value the value to be associated with the variable.
         * @return the name of the anonymous process variable
         */
      public:
        Variable::VariablePtr declareProcessVariable(Variable::VariablePtr value);

        /**
         * Declare a local or temporary variable.
         * @param name the variable name
         * @param type the type of the variable
         * @return the name of the variable
         */
      public:
        Variable::VariablePtr declareLocalVariable(const Name &name,
            mylang::ethir::types::Type::Ptr type);

        /**
         * Declare a local or temporary variable.
         * @param type the type of the variable
         * @return the name of the variable
         */
      public:
        Variable::VariablePtr declareLocalVariable(mylang::ethir::types::Type::Ptr type);

        /**
         * Declare a lambda function
         */
      public:
        Variable::VariablePtr declareLocalLambda(
            ::std::vector<mylang::ethir::types::Type::Ptr> paramTypes, LambdaProducer lambda);

        /**
         * Declare a lambda function using a lambda builder.
         */
      public:
        Variable::VariablePtr declareLocalLambda(
            ::std::vector<mylang::ethir::types::Type::Ptr> paramTypes, LambdaBuilder lambda);

        /**
         * Build the statement. Once built, the builder is reset and can be reused.
         * @return a statement or nullptr if none was built.
         */
      public:
        virtual Statement::StatementPtr build(Statement::StatementPtr next = nullptr);

        /** Determine if there are any statements to be build */
      public:
        inline bool isEmpty() const
        {
          return producers.empty();
        }

        /** The producers */
      private:
        Producers producers;
      };

    }
  }
}
#endif
