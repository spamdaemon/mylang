#include <mylang/ethir/ir/LiteralInteger.h>
#include <mylang/ethir/ir/LiteralReal.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/TypeCastError.h>

namespace mylang {
  namespace ethir {
    namespace ir {

      LiteralInteger::LiteralInteger(::std::shared_ptr<const types::IntegerType> xtype,
          ValueType xvalue)
          : LiteralPrimitive(xtype, xvalue.toString()), value(xvalue)
      {
      }

      LiteralInteger::~LiteralInteger()
      {
      }

      LiteralInteger::LiteralIntegerPtr LiteralInteger::create(
          ::std::shared_ptr<const types::IntegerType> xtype, ValueType xvalue)
      {
        return ::std::make_shared<LiteralInteger>(xtype, xvalue);
      }

      LiteralInteger::LiteralIntegerPtr LiteralInteger::create(ValueType xvalue)
      {
        auto ty = types::IntegerType::create(xvalue, xvalue);
        return create(ty, xvalue);
      }
      Literal::TypePtr LiteralInteger::getSourceType() const
      {
        return types::IntegerType::create(value, value);
      }

      void LiteralInteger::accept(NodeVisitor &visitor) const
      {
        visitor.visitLiteralInteger(self<LiteralInteger>());
      }
      ::std::optional<int> LiteralInteger::compare(const Literal &obj) const
      {
        auto that = dynamic_cast<const LiteralInteger*>(&obj);
        if (that == nullptr) {
          return ::std::nullopt;
        }
        if (value < that->value) {
          return -1;
        } else if (value > that->value) {
          return 1;
        } else {
          return 0;
        }
      }
      Literal::LiteralPtr LiteralInteger::castTo(const EType &newType) const
      {
        if (newType->isSameType(*type)) {
          return self<Literal>();
        }
        // we use type associated with the value and try to convert that
        auto ty = types::IntegerType::create(value, value);
        TypeCastError::checkCast(ty, newType);
        if (newType->self<types::IntegerType>()) {
          return LiteralInteger::create(newType->self<types::IntegerType>(), value);
        }
        if (newType->self<types::RealType>()) {
          return LiteralReal::create(newType->self<types::RealType>(), value);
        }
        throw TypeCastError(type, newType);
      }
      bool LiteralInteger::isEqual(const Node &node) const
      {
        const LiteralInteger &that = dynamic_cast<const LiteralInteger&>(node);
        return equals(type, that.type) && value == value;
      }
      void LiteralInteger::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(value);
      }

    }
  }
}
