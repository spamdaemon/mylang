#include <mylang/ethir/ir/Variable.h>
#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <algorithm>
#include <memory>
#include <string>

namespace mylang {
  namespace ethir {
    namespace ir {

      Variable::Variable(Scope xscope, TypePtr t, Name xname)
          : Expression(::std::move(t)), scope(xscope), name(::std::move(xname))
      {
      }

      Variable::~Variable()
      {
      }

      Variable::VariablePtr Variable::create(Scope xscope, TypePtr t, Name xname)
      {
        return ::std::make_shared<Variable>(xscope, t, xname);
      }
      Variable::VariablePtr Variable::createLocal(TypePtr t)
      {
        return create(Scope::FUNCTION_SCOPE, t, Name::create("lv"));
      }

      Variable::VariablePtr Variable::rename(Name xname) const
      {
        if (xname == name) {
          return self<Variable>();
        }
        return create(scope, type, ::std::move(xname));
      }

      void Variable::accept(NodeVisitor &visitor) const
      {
        visitor.visitVariable(self<Variable>());
      }
      bool Variable::isEqual(const Node &node) const
      {
        const Variable &that = dynamic_cast<const Variable&>(node);
        return scope == that.scope && name == that.name && equals(type, that.type);
      }

      void Variable::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix((::std::uint64_t) scope);
        h.mix(name);
      }

    }
  }
}
