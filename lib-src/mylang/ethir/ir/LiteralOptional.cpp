#include <mylang/ethir/ir/LiteralOptional.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/TypeMismatchError.h>
#include <mylang/ethir/TypeCastError.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace ir {

      LiteralOptional::LiteralOptional(::std::shared_ptr<const types::OptType> xtype,
          LiteralPtr xvalue)
          : Literal(xtype), value(xvalue)
      {
        if (value) {
          TypeMismatchError::check(xtype->element, value->type);
        }
      }

      LiteralOptional::~LiteralOptional()
      {
      }

      LiteralOptional::LiteralOptionalPtr LiteralOptional::create(
          ::std::shared_ptr<const types::OptType> xtype, LiteralPtr xvalue)
      {
        return ::std::make_shared<LiteralOptional>(xtype, xvalue);
      }

      LiteralOptional::LiteralOptionalPtr LiteralOptional::create(
          ::std::shared_ptr<const types::OptType> xtype)
      {
        return create(xtype, nullptr);
      }

      void LiteralOptional::accept(NodeVisitor &visitor) const
      {
        visitor.visitLiteralOptional(self<LiteralOptional>());
      }

      ::std::optional<int> LiteralOptional::compare(const Literal &other) const
      {
        const LiteralOptional *that = dynamic_cast<const LiteralOptional*>(&other);
        if (that && type->isSameType(*that->type)) {
          if (value == that->value) {
            return 0;
          }
          if (value && that->value) {
            return value->compare(*that->value);
          }
          return value ? 1 : -1;
        }
        return ::std::nullopt;
      }
      Literal::LiteralPtr LiteralOptional::castTo(const EType &newType) const
      {
        TypeCastError::checkCast(type, newType);

        auto optTy = newType->self<types::OptType>();

        if (value) {
          auto v = value->castTo(optTy->element);
          return LiteralOptional::create(optTy, v);
        } else {
          return LiteralOptional::create(optTy);
        }
      }
      bool LiteralOptional::isEqual(const Node &node) const
      {
        const LiteralOptional &that = dynamic_cast<const LiteralOptional&>(node);
        return equals(type, that.type) && equals(value, that.value);
      }
      void LiteralOptional::computeHashCode(HashCode &h) const
      {
        h.mix(type);
        h.mix(value);
      }

    }
  }
}
