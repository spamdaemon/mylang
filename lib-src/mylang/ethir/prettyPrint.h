#ifndef FILE_MYLANG_ETHIR_PRETTYPRINT_H
#define FILE_MYLANG_ETHIR_PRETTYPRINT_H

#include <mylang/ethir/ethir.h>
#include <iosfwd>

namespace mylang {
  namespace ethir {

    /**
     * Pretty print the specified node to the provided stream
     * @param node a node to print
     * @param out an output stream
     */
    void prettyPrint(const ENode &node, ::std::ostream &out);

    /**
     * Pretty print the specified node to the provided stream
     * @param node a node to print
     * @return a string
     */
    ::std::string prettyPrint(const ENode &node);
  }
}
#endif
