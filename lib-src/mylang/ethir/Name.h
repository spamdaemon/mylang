#ifndef CLASS_MYLANG_ETHIR_NAME_H
#define CLASS_MYLANG_ETHIR_NAME_H

#ifndef CLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif

namespace mylang {
  namespace ethir {

    class Name
    {
      /** A counter */
      struct Counter
      {
        Counter(size_t i)
            : count(i)
        {
        }
        size_t next()
        {
          return count++;
        }
        size_t count;
      };

      /** Create a name for a global name */
    private:
      Name(mylang::names::Name::Ptr name);

      /** Create a name for a global name */
    private:
      Name(mylang::names::Name::Ptr name, ::std::shared_ptr<Counter> counter);

    public:
      Name(const Name &name);
      Name& operator=(const Name &name);
      ~Name();

      /**
       * Create a name with a new version
       * @return a name with a new version
       */
    public:
      Name newVersion() const;

      /**
       * Reset the version from this name.
       * @return a name with version 0
       */
    public:
      Name resetVersion() const;

      /**
       * Create a new name from a global name.
       * @param src the source name
       */
    public:
      static Name create(mylang::names::Name::Ptr src);

      /**
       * Create a temporary name.
       * @param prefix an optional prefix
       */
    public:
      static Name create(const ::std::string &prefix);

      /**
       * Change the basename of this name, but keep the counter
       * @param src the new src name
       * @return a new name with the specified source
       */
    public:
      Name rename(mylang::names::Name::Ptr src) const;

      /**
       * Compare for equality.
       * @param name a name
       * @return true if equal, false otherwise
       */
    public:
      bool equals(const Name &name) const;

      /**
       * Compare for less than.
       * @param name a name
       * @return true if this name is less than the specified name.
       */
    public:
      bool less(const Name &name) const;

      /**
       * Compare this name to the specified name.
       * @param name a name
       * @return -1 if this is  < , 0 if equal, 1 if greater
       */
    public:
      int compare(const Name &name) const;

      friend inline bool operator<(const Name &a, const Name &b)
      {
        return a.less(b);
      }

      friend inline bool operator==(const Name &a, const Name &b)
      {
        return a.equals(b);
      }

      friend inline bool operator!=(const Name &a, const Name &b)
      {
        return !a.equals(b);
      }

      friend inline ::std::ostream& operator<<(::std::ostream &out, const Name &name)
      {
        out << name._source;
        if (name._version != 0) {
          out << '{' << name._version << '}';
        }
        return out;
      }

    public:
      ::std::string toString() const;

    public:
      inline const mylang::names::Name::Ptr& source() const
      {
        return _source;
      }

    public:
      inline size_t version() const
      {
        return _version;
      }

      /** The globally unique name on which this name is based */
    private:
      mylang::names::Name::Ptr _source;

      /** The current counter value */
    private:
      size_t _version;

      /** The shared counter (primarily used for SSA form */
    private:
      ::std::shared_ptr<Counter> _counter;
    };

  }
}
#endif
