#include <mylang/ethir/Name.h>
#include <iostream>

namespace mylang {
  namespace ethir {

    Name::Name(mylang::names::Name::Ptr name)
        : _source(name), _version(0), _counter(::std::make_shared<Counter>(1))
    {
    }

    Name::Name(const Name &name)
        : _source(name._source), _version(name._version), _counter(name._counter)
    {
    }

    Name::Name(mylang::names::Name::Ptr name, ::std::shared_ptr<Counter> counter)
        : _source(name), _version(counter->next()), _counter(counter)
    {
    }

    Name::~Name()
    {
    }

    Name Name::newVersion() const
    {
      return Name(_source, _counter);
    }

    Name Name::resetVersion() const
    {
      Name res(*this);
      res._version = 0;
      return res;
    }

    Name& Name::operator=(const Name &name)
    {
      _source = name._source;
      _counter = name._counter;
      _version = name._version;
      return *this;
    }

    Name Name::create(mylang::names::Name::Ptr src)
    {
      return Name(src);
    }

    Name Name::create(const ::std::string &prefix)
    {
      return create(mylang::names::Name::create(prefix));
    }

    Name Name::rename(mylang::names::Name::Ptr src) const
    {
      Name c(*this);
      c._source = src;
      return c;
    }

    bool Name::equals(const Name &name) const
    {
      return _source == name._source && _version == name._version;
    }
    bool Name::less(const Name &name) const
    {
      if (_source < name._source) {
        return true;
      }
      if (_source == name._source && _version < name._version) {
        return true;
      }
      return false;
    }

    int Name::compare(const Name &name) const
    {
      if (less(name)) {
        return -1;
      } else if (equals(name)) {
        return 0;
      } else {
        return 1;
      }
    }

    ::std::string Name::toString() const
    {
      if (_version == 0) {
        return _source->uniqueFullName(".");
      } else {
        ::std::string res(_source->uniqueFullName("."));
        res += '{';
        res += ::std::to_string(_version);
        res += '}';
        return res;
      }
    }

  }
}
