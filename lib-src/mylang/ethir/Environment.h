#ifndef CLASS_MYLANG_ETHIR_SSA_ENVIRONMENT_H
#define CLASS_MYLANG_ETHIR_SSA_ENVIRONMENT_H

#ifndef CLASS_MYLANG_ETHIR_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VALUEDECL_H
#include <mylang/ethir/ir/ValueDecl.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_PARAMETER_H
#include <mylang/ethir/ir/Parameter.h>
#endif

#include <memory>
#include <map>

namespace mylang {
  namespace ethir {

    /**
     * This class maintains the binding of variables
     * to their values.
     */
    class Environment: public std::enable_shared_from_this<Environment>
    {
      Environment(const Environment&) = delete;
      Environment& operator=(const Environment&) = delete;

    public:
      typedef ::std::shared_ptr<Environment> Ptr;

    public:
      typedef mylang::ethir::ir::Variable::VariablePtr Var;

      /** A binding */
    public:
      struct Binding
      {
        ::std::optional<mylang::ethir::ir::Variable::Scope> scope;
        /** THe value of the binding or nullptr if this is a parameter binding */
        EExpression value;
      };

      /** A map of binding */
    public:
      typedef ::std::map<Name, Binding> Bindings;

      /**
       * Create a default environment.
       */
    public:
      Environment();

      /**
       * Create an new environment.
       * @param parent a parent environment
       */
    public:
      Environment(Ptr parent);

      /** Destructor */
    public:
      virtual ~Environment();

      /**
       * Create a new environment.
       * @return a new environment based on this one.
       */
    public:
      virtual Ptr create();

      /**
       * Get this type.
       * @return this type instance
       */
    public:
      template<class T = Environment>
      inline ::std::shared_ptr<T> self()
      {
        return ::std::dynamic_pointer_cast<T>(shared_from_this());
      }

      /**
       * Get this type.
       * @return this type instance
       */
    public:
      template<class T = Environment>
      inline ::std::shared_ptr<const T> self() const
      {
        return ::std::dynamic_pointer_cast<const T>(shared_from_this());
      }

      /**
       * Get the parent binding.
       * @return the parent binding
       */
    public:
      inline ::std::shared_ptr<const Environment> parent() const
      {
        return _parent;
      }

      /**
       * Add a new binding into the environment.
       * @param name the name under which the value is to be bound
       * @param scope the scope of the name
       * @param value the expression to be bound
       */
    public:
      virtual void addBinding(const Name &name,
          ::std::optional<mylang::ethir::ir::Variable::Scope> scope, const EExpression &value);

      /**
       * Add a new binding into the environment.
       * @param name the name under which the value is to be bound
       * @param value the expression to be bound
       */
    public:
      void addBinding(const Name &name, const EExpression &value);

      /**
       * Add a new binding into the environment.
       * @param v a variable
       * @param value the expression to be bound
       */
    public:
      void addBinding(const EVariable &v, const EExpression &value);

      /**
       * Lookup a binding.
       * @param name the name to lookup
       * @return a binding or nullopt if no binding exists for the name
       */
    public:
      virtual ::std::optional<Environment::Binding> lookupBinding(const Name &name) const;

      /**
       * Lookup a binding for variable by name.
       * @param name a name
       * @return the expression that is bound to the name or nullptr
       */
    public:
      EExpression lookup(const Name &name) const;

      /**
       * Lookup a binding for variable by name.
       * @param name a name
       * @return the expression that is bound to the name or nullptr
       */
    public:
      template<class T = ir::Expression>
      ::std::shared_ptr<const T> find(const Name &name) const
      {
        EExpression e = lookup(name);
        if (e) {
          return e->self<T>();
        } else {
          return nullptr;
        }
      }

      /**
       * Lookup a binding for variable by name.
       * @param var a variable
       * @return the epxression that is bound to the name or nullptr
       */
    public:
      template<class T = ir::Expression>
      ::std::shared_ptr<const T> find(const ir::Variable::VariablePtr &var) const
      {
        if (var) {
          return find<T>(var->name);
        } else {
          return nullptr;
        }
      }

      /** The parent environment, which is immutable  */
    private:
      const Ptr _parent;

      /** The bindings in this environment */
    private:
      Bindings bindings;
    };

  }
}
#endif
