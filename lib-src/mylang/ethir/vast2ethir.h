#ifndef FILE_MYLANG_ETHIR_VAST2ETHIR_H
#define FILE_MYLANG_ETHIR_VAST2ETHIR_H

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_PROGRAM_H
#include <mylang/ethir/ir/Program.h>
#endif

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

namespace mylang {
  namespace ethir {

    /**
     * Transform an VAST node into an ethir node.
     * @param vastNode a vast node
     * @return an ethir program node
     */
    EProgram vast2ethir(mylang::GroupNodeCPtr node);

  }
}
#endif
