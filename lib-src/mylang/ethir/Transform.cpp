#include <mylang/ethir/Transform.h>
#include <mylang/BigInt.h>
#include <mylang/BigReal.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/types/types.h>
#include <memory>
#include <optional>
#include <string>
#include <utility>

namespace mylang {
  namespace ethir {

    Transform::Transform()
    {
    }
    Transform::~Transform()
    {
    }

    EStatement Transform::nop(EStatement next)
    {
      return ir::NoStatement::create(next);
    }

    ENode Transform::apply(const ENode &node)
    {
      ENode result = node;
      while (true) {
        ENode next = visitNode(result);
        if (next) {
          result = next;
        } else {
          break;
        }
      }
      return result;
    }

    EType Transform::visitType(const EType&)
    {
      return nullptr;
    }

    Transform::Tx<EType> Transform::transformType(const EType &type)
    {
      if (!type) {
        return Tx<EType>(type, false);
      }
      auto res = visitType(type);
      if (res) {
        return Tx<EType>(res->self<types::Type>(), res != type);
      } else {
        return Tx<EType>(type, false);
      }
    }

    Transform::Tx<ENode> Transform::transformNode(const ENode &node)
    {
      if (!node) {
        return Tx<ENode>(node, false);
      }
      auto res = visitNode(node);
      if (res) {
        return Tx<ENode>(res->self<ir::Node>(), res != node);
      } else {
        return Tx<ENode>(node, false);
      }
    }

    Transform::Tx<ELiteral> Transform::transformLiteral(const ELiteral &expr)
    {
      if (!expr) {
        return Tx<ELiteral>(expr, false);
      }
      auto res = visitNode(expr);
      if (res) {
        return Tx<ELiteral>(res->self<ir::Literal>(), res != expr);
      } else {
        return Tx<ELiteral>(expr, false);
      }
    }

    Transform::Tx<EExpression> Transform::transformExpr(const EExpression &expr)
    {
      if (!expr) {
        return Tx<EExpression>(expr, false);
      }
      auto res = visitNode(expr);
      if (res) {
        return Tx<EExpression>(res->self<ir::Expression>(), res != expr);
      } else {
        return Tx<EExpression>(expr, false);
      }
    }

    Transform::Tx<EVariable> Transform::transformExpression(const EVariable &expr)
    {
      if (!expr) {
        return Tx<EVariable>(expr, false);
      }
      auto res = visitNode(expr);
      if (res) {
        return Tx<EVariable>(res->self<ir::Variable>(), res != expr);
      } else {
        return Tx<EVariable>(expr, false);
      }
    }

    Transform::Tx<EStatement> Transform::transformStatement(const EStatement &stmt)
    {
      if (!stmt) {
        return Tx<EStatement>(stmt, false);
      }
      auto res = visitNode(stmt);
      if (res) {
        return Tx<EStatement>(res->self<ir::Statement>(), res != stmt);
      } else {
        return Tx<EStatement>(stmt, false);
      }
    }

    Transform::Tx<EDeclaration> Transform::transformDeclaration(const EDeclaration &decl)
    {
      if (!decl) {
        return Tx<EDeclaration>(decl, false);
      }
      auto res = visitNode(decl);
      if (res) {
        return Tx<EDeclaration>(res->self<ir::Declaration>(), res != decl);
      } else {
        return Tx<EDeclaration>(decl, false);
      }
    }

    Transform::Tx<::std::vector<EVariable>> Transform::transformExpressions(
        const ::std::vector<EVariable> &exprs)
    {
      return transformNodes<ir::Variable>(exprs);
    }

    Transform::Tx<::std::vector<ELiteral>> Transform::transformLiterals(
        const ::std::vector<ELiteral> &literals)
    {
      return transformNodes<ir::Literal>(literals);
    }

    Transform::Tx<ir::Loop::Expressions> Transform::transformExpressions(
        const ir::Loop::Expressions &expr)
    {
      ir::Loop::Expressions res;
      bool changed = false;
      for (const auto &e : expr) {
        auto name = transformExpression(e.first);
        auto value = transformExpr(e.second);
        changed = changed || name.isNew || value.isNew;
        res.emplace(name.actual, value.actual);
      }
      return Tx<ir::Loop::Expressions>(res, changed);
    }

    Transform::Tx<::std::vector<ir::Parameter::ParameterPtr>> Transform::transformParameters(
        const ::std::vector<ir::Parameter::ParameterPtr> &params)
    {
      return transformNodes<ir::Parameter>(params);
    }

    Transform::Tx<ir::Loop::States> Transform::transformLoopStates(
        const ir::Loop::States &loopstates)
    {
      ir::Loop::States states;
      bool changed = false;
      for (const auto &e : loopstates) {
        auto v = transformNode(e.second);
        states.emplace(e.first, v.actual->self<ir::Loop::Step>());
        changed = changed || v.isNew;
      }
      return Tx<ir::Loop::States>(states, changed);
    }

    ENode Transform::visit(const ir::LetExpression::LetExpressionPtr &node)
    {
      auto var = transformExpression(node->variable);
      auto value = transformExpr(node->value);
      auto result = transformExpr(node->result);
      if (var.isNew || value.isNew || result.isNew) {
        return ir::LetExpression::create(var.actual, value.actual, result.actual);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::NewProcess::NewProcessPtr &node)
    {
      auto exprs = transformExpressions(node->arguments);
      auto type = transformType(node->type);
      if (exprs.isNew || type.isNew) {
        return ir::NewProcess::create(type.actual, node->processName, node->constructorName,
            exprs.actual);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::NewUnion::NewUnionPtr &node)
    {
      auto value = transformExpression(node->value);
      auto type = transformType(node->type);
      if (value.isNew || type.isNew) {
        return ir::NewUnion::create(type.actual, node->member, value.actual);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::LiteralChar::LiteralCharPtr &e)
    {
      auto ty = transformType(e->type);
      if (ty.isNew) {
        return ir::LiteralChar::create(ty.actual->self<types::CharType>(), e->value);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::LiteralReal::LiteralRealPtr &e)
    {
      auto ty = transformType(e->type);
      if (ty.isNew) {
        return ir::LiteralReal::create(ty.actual->self<types::RealType>(), e->value);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::LiteralBit::LiteralBitPtr &e)
    {
      auto ty = transformType(e->type);
      if (ty.isNew) {
        return ir::LiteralBit::create(ty.actual->self<types::BitType>(), e->value);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::LiteralBoolean::LiteralBooleanPtr &e)
    {
      auto ty = transformType(e->type);
      if (ty.isNew) {
        return ir::LiteralBoolean::create(ty.actual->self<types::BooleanType>(), e->value);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::LiteralInteger::LiteralIntegerPtr &e)
    {
      auto ty = transformType(e->type);
      if (ty.isNew) {
        return ir::LiteralInteger::create(ty.actual->self<types::IntegerType>(), e->value);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::LiteralString::LiteralStringPtr &e)
    {
      auto ty = transformType(e->type);
      if (ty.isNew) {
        return ir::LiteralString::create(ty.actual->self<types::StringType>(), e->value);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::ProcessVariableUpdate::ProcessVariableUpdatePtr &node)
    {
      auto target = transformExpression(node->target);
      auto value = transformExpression(node->value);
      auto next = transformStatement(node->next);
      if (target.isNew || next.isNew || value.isNew) {
        return ir::ProcessVariableUpdate::create(target.actual, value.actual, next.actual);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::VariableUpdate::VariableUpdatePtr &node)
    {
      auto target = transformExpression(node->target);
      auto value = transformExpression(node->value);
      auto next = transformStatement(node->next);
      if (target.isNew || next.isNew || value.isNew) {
        return ir::VariableUpdate::create(target.actual, value.actual, next.actual);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::IfStatement::IfStatementPtr &node)
    {
      auto cond = transformExpression(node->condition);
      auto iftrue = transformStatement(node->iftrue);
      auto iffalse = transformStatement(node->iffalse);
      auto next = transformStatement(node->next);
      if (cond.isNew || iftrue.isNew || iffalse.isNew || next.isNew) {
        return ir::IfStatement::create(cond.actual, iftrue.actual, iffalse.actual, next.actual);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::TryCatch::TryCatchPtr &node)
    {
      auto tryB = transformStatement(node->tryBody);
      auto catchB = transformStatement(node->catchBody);
      auto next = transformStatement(node->next);
      if (tryB.isNew || catchB.isNew || next.isNew) {
        return ir::TryCatch::create(tryB.actual, catchB.actual, next.actual);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::Phi::PhiPtr &node)
    {
      auto exprs = transformExpressions(node->arguments);
      if (exprs.isNew) {
        return ir::Phi::create(exprs.actual);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::ToArray::ToArrayPtr &node)
    {
      auto loop = transformNode(node->loop);
      auto type = transformType(node->type);
      if (loop.isNew || type.isNew) {
        return ir::ToArray::create(type.actual->self<types::ArrayType>(),
            loop.actual->self<ir::Loop>());
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::OperatorExpression::OperatorExpressionPtr &node)
    {
      auto exprs = transformExpressions(node->arguments);
      auto type = transformType(node->type);
      if (type.isNew || exprs.isNew) {
        return ir::OperatorExpression::create(type.actual, node->name, exprs.actual);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::NoValue::NoValuePtr &node)
    {
      auto type = transformType(node->type);
      if (type.isNew) {
        return ir::NoValue::create(type.actual);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::OpaqueExpression::OpaqueExpressionPtr &node)
    {
      auto exprs = transformExpressions(node->arguments);
      auto type = transformType(node->type);
      if (type.isNew) {
        //FIXME: we can't really transform the type of an opaque expression, because that would mean
        // transforming the contained expression
        throw ::std::runtime_error("Type of an opaque expression cannot be transformed");
      }
      if (exprs.isNew) {
        return node->changeArguments(::std::move(exprs.actual));
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::VariableDecl::VariableDeclPtr &node)
    {
      auto v = transformExpression(node->variable);
      auto init = transformExpression(node->value);
      auto next = transformStatement(node->next);
      if (v.isNew || init.isNew || next.isNew) {
        return ir::VariableDecl::create(v.actual, init.actual, next.actual);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::ProcessValue::ProcessValuePtr &node)
    {
      auto v = transformExpression(node->variable);
      auto init = transformExpr(node->value);
      if (v.isNew || init.isNew) {
        return ir::ProcessValue::create(v.actual, init.actual);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::ProcessVariable::ProcessVariablePtr &node)
    {
      auto v = transformExpression(node->variable);
      auto init = transformExpression(node->value);
      if (v.isNew || init.isNew) {
        return ir::ProcessVariable::create(v.actual, init.actual);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::ProcessDecl::ProcessDeclPtr &node)
    {
      ir::ProcessDecl::Ports publicPorts;
      ir::ProcessDecl::Declarations variables;
      ir::ProcessDecl::Constructors constructors;
      ir::ProcessDecl::Blocks blocks;

      bool isNew = false;
      for (auto p : node->publicPorts) {
        auto np = transformStatement(p);
        if (np.isNew || !np.actual) {
          isNew = true;
        }
        if (np.actual) {
          publicPorts.push_back(np.actual->self<ir::PortDecl>());
        }
      }
      for (auto p : node->variables) {
        auto np = transformStatement(p);
        if (np.isNew || !np.actual) {
          isNew = true;
        }
        if (np.actual) {
          variables.push_back(np.actual->self<ir::Declaration>());
        }
      }
      for (auto p : node->constructors) {
        auto np = transformStatement(p);
        if (np.isNew || !np.actual) {
          isNew = true;
        }
        if (np.actual) {
          constructors.push_back(np.actual->self<ir::ProcessConstructor>());
        }
      }
      for (auto p : node->blocks) {
        auto np = transformStatement(p);
        if (np.isNew || !np.actual) {
          isNew = true;
        }
        if (np.actual) {
          blocks.push_back(np.actual->self<ir::ProcessBlock>());
        }
      }
      auto type = transformType(node->type);
      if (isNew || type.isNew) {
        return ir::ProcessDecl::create(node->scope, node->name,
            type.actual->self<types::ProcessType>(), publicPorts, variables, constructors, blocks);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::Parameter::ParameterPtr &p)
    {
      auto tx = transformExpression(p->variable);
      if (tx.isNew) {
        return ir::Parameter::create(tx.actual->name, tx.actual->type);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::BreakStatement::BreakStatementPtr&)
    {
      return nullptr;
    }
    ENode Transform::visit(const ir::ContinueStatement::ContinueStatementPtr&)
    {
      return nullptr;
    }
    ENode Transform::visit(const ir::ReturnStatement::ReturnStatementPtr &stmt)
    {
      if (stmt->value == nullptr) {
        return nullptr;
      }
      auto expr = transformExpression(stmt->value);
      if (expr.isNew) {
        return ir::ReturnStatement::create(expr.actual);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::AbortStatement::AbortStatementPtr &stmt)
    {
      if (stmt->value == nullptr) {
        return nullptr;
      }
      auto expr = transformExpression(stmt->value);
      if (expr.isNew) {
        return ir::AbortStatement::create(expr.actual);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::ThrowStatement::ThrowStatementPtr &stmt)
    {
      if (stmt->value == nullptr) {
        return nullptr;
      }
      auto expr = transformExpression(stmt->value);
      if (expr.isNew) {
        return ir::ThrowStatement::create(expr.actual);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::LambdaExpression::LambdaExpressionPtr &node)
    {
      auto body = transformStatement(node->body);
      auto type = transformType(node->type);
      auto params = transformParameters(node->parameters);
      if (type.isNew || params.isNew || body.isNew) {
        auto ty = type.actual->self<types::FunctionType>();
        return ir::LambdaExpression::create(node->tag, ty, params.actual, body.actual);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::GetDiscriminant::GetDiscriminantPtr &node)
    {
      auto obj = transformExpression(node->object);
      auto type = transformType(node->type);
      if (type.isNew || obj.isNew) {
        return ir::GetDiscriminant::create(type.actual, obj.actual);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::GetMember::GetMemberPtr &node)
    {
      auto obj = transformExpression(node->object);
      auto type = transformType(node->type);
      if (type.isNew || obj.isNew) {
        return ir::GetMember::create(type.actual, obj.actual, node->name);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::LiteralArray::LiteralArrayPtr &node)
    {
      auto ty = transformType(node->type);
      auto elem = transformLiterals(node->elements);
      if (ty.isNew || elem.isNew) {
        return ir::LiteralArray::create(ty.actual->self<types::ArrayType>(), elem.actual);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::LiteralNamedType::LiteralNamedTypePtr &node)
    {
      auto ty = transformType(node->type);
      auto v = transformLiteral(node->value);
      if (ty.isNew || v.isNew) {
        return ir::LiteralNamedType::create(ty.actual->self<types::NamedType>(), v.actual);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::LiteralOptional::LiteralOptionalPtr &node)
    {
      auto ty = transformType(node->type);
      auto v = transformLiteral(node->value);
      if (ty.isNew || v.isNew) {
        return ir::LiteralOptional::create(ty.actual->self<types::OptType>(), v.actual);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::LiteralStruct::LiteralStructPtr &node)
    {
      auto ty = transformType(node->type);
      auto elem = transformLiterals(node->members);
      if (ty.isNew || elem.isNew) {
        return ir::LiteralStruct::create(ty.actual->self<types::StructType>(), elem.actual);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::LiteralTuple::LiteralTuplePtr &node)
    {
      auto ty = transformType(node->type);
      auto elem = transformLiterals(node->elements);
      if (ty.isNew || elem.isNew) {
        return ir::LiteralTuple::create(ty.actual->self<types::TupleType>(), elem.actual);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::LiteralUnion::LiteralUnionPtr &node)
    {
      auto ty = transformType(node->type);
      auto v = transformLiteral(node->value);
      if (ty.isNew || v.isNew) {
        return ir::LiteralUnion::create(ty.actual->self<types::UnionType>(), node->member, v.actual);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::LiteralByte::LiteralBytePtr &e)
    {
      auto ty = transformType(e->type);
      if (ty.isNew) {
        return ir::LiteralByte::create(ty.actual->self<types::ByteType>(), e->value);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::Variable::VariablePtr &v)
    {
      auto ty = transformType(v->type);
      if (ty.isNew) {
        return ir::Variable::create(v->scope, ty.actual, v->name);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::OwnConstructorCall::OwnConstructorCallPtr &node)
    {
      auto pre = transformStatement(node->pre);
      auto args = transformExpressions(node->arguments);
      if (pre.isNew || args.isNew) {
        return ir::OwnConstructorCall::create(pre.actual, args.actual);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::Program::ProgramPtr &node)
    {
      ir::Program::Processes processes;
      ir::Program::Globals globals;
      ir::Program::NamedTypes types;

      bool isNew = false;
      for (auto t : node->types) {
        auto nt = transformType(t);
        if (nt.isNew) {
          isNew = true;
        }
        if (nt.actual) {
          auto namedTy = nt.actual->self<types::NamedType>();
          types.push_back(namedTy);
        }
      }
      for (auto p : node->globals) {
        auto np = transformStatement(p);
        if (np.isNew || !np.actual) {
          isNew = true;
        }
        if (np.actual) {
          globals.push_back(np.actual->self<ir::GlobalValue>());
        }
      }
      for (auto p : node->processes) {
        auto np = transformStatement(p);
        if (np.isNew || !np.actual) {
          isNew = true;
        }
        if (np.actual) {
          processes.push_back(np.actual->self<ir::ProcessDecl>());
        }
      }
      if (isNew) {
        return ir::Program::create(globals, processes, types);
      }

      return nullptr;
    }
    ENode Transform::visit(const ir::LoopStatement::LoopStatementPtr &node)
    {
      auto body = transformStatement(node->body);
      auto next = transformStatement(node->next);
      if (body.isNew || next.isNew) {
        return ir::LoopStatement::create(node->name, body.actual, next.actual);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::GlobalValue::GlobalValuePtr &node)
    {
      auto v = transformExpression(node->variable);
      auto val = transformExpr(node->value);
      if (v.isNew || val.isNew) {
        return ir::GlobalValue::create(v.actual, val.actual, node->kind);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::ProcessBlock::ProcessBlockPtr &node)
    {
      auto body = transformStatement(node->body);
      if (body.isNew) {
        return ir::ProcessBlock::create(node->name, body.actual);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::ProcessConstructor::ProcessConstructorPtr &node)
    {
      auto body = transformStatement(node->body);
      auto ctor = transformStatement(node->callConstructor);
      auto parameters = transformParameters(node->parameters);
      if (body.isNew || ctor.isNew || parameters.isNew) {
        auto ownCtorCall = ctor.actual ? ctor.actual->self<ir::OwnConstructorCall>() : nullptr;
        return ir::ProcessConstructor::create(parameters.actual, ownCtorCall, body.actual);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::InputPortDecl::InputPortDeclPtr &node)
    {
      auto var = transformExpression(node->variable);
      if (var.isNew) {
        return ir::InputPortDecl::create(var.actual, node->publicName);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::OutputPortDecl::OutputPortDeclPtr &node)
    {
      auto var = transformExpression(node->variable);
      if (var.isNew) {
        return ir::OutputPortDecl::create(var.actual, node->publicName);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::ValueDecl::ValueDeclPtr &node)
    {
      auto v = transformExpression(node->variable);
      auto init = transformExpr(node->value);
      auto next = transformStatement(node->next);
      if (v.isNew || init.isNew || next.isNew
          || (init.actual && init.actual->self<ir::LetExpression>())) {
        return ir::ValueDecl::createSimplified(v.actual, init.actual, next.actual);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::ForeachStatement::ForeachStatementPtr &node)
    {
      auto v = transformExpression(node->variable);
      auto data = transformExpression(node->data);
      auto body = transformStatement(node->body);
      auto next = transformStatement(node->next);
      if (v.isNew || data.isNew || body.isNew || next.isNew) {
        return ir::ForeachStatement::create(node->name, v.actual, data.actual, body.actual,
            next.actual);
      }
      return nullptr;
    }
    ENode Transform::visit(const ir::StatementBlock::StatementBlockPtr &node)
    {
      auto body = transformStatement(node->statements);
      auto next = transformStatement(node->next);
      if (body.isNew || next.isNew) {
        return ir::StatementBlock::create(body.actual, next.actual);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::CommentStatement::CommentStatementPtr &node)
    {
      auto next = transformStatement(node->next);
      if (next.isNew) {
        return ir::CommentStatement::create(node->comment, next.actual);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::NoStatement::NoStatementPtr &node)
    {
      if (node->next) {
        auto next = transformStatement(node->next);
        if (next.isNew) {
          return ir::NoStatement::create(next.actual);
        }
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::YieldStatement::YieldStatementPtr &node)
    {
      auto val = transformExpression(node->value);
      auto skip = transformStatement(node->skip);

      if (val.isNew || skip.isNew) {
        return ir::YieldStatement::create(val.actual, skip.actual->self<ir::SkipStatement>());
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::SkipStatement::SkipStatementPtr &node)
    {
      ir::Loop::Expressions updates;
      bool changed = false;
      for (const auto &e : node->updates) {
        auto k = transformExpression(e.first);
        auto v = transformExpr(e.second);
        updates[k.actual] = v.actual;
        changed = changed || v.isNew || k.isNew;
      }
      if (changed) {
        return ir::SkipStatement::create(::std::move(updates), node->nextState,
            node->resetNextState);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::IterateArrayStep::IterateArrayStepPtr &node)
    {
      auto nextState = transformStatement(node->nextState);
      auto array = transformExpression(node->array);
      auto counter = transformExpression(node->counter);
      auto value = transformExpression(node->value);
      auto inits = transformExpressions(node->initializers);
      auto body = transformStatement(node->body);
      if (nextState.isNew || array.isNew || counter.isNew || value.isNew || body.isNew
          || inits.isNew) {
        return ir::IterateArrayStep::create(node->reverse,
            nextState.actual->self<ir::SkipStatement>(), array.actual, counter.actual, value.actual,
            inits.actual, body.actual);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::SequenceStep::SequenceStepPtr &node)
    {
      auto nextState = transformStatement(node->nextState);
      auto iterationCount = transformExpression(node->iterationCount);
      auto counter = transformExpression(node->counter);
      auto inits = transformExpressions(node->initializers);
      auto body = transformStatement(node->body);
      if (nextState.isNew || iterationCount.isNew || counter.isNew || body.isNew || inits.isNew) {
        return ir::SequenceStep::create(nextState.actual->self<ir::SkipStatement>(),
            iterationCount.actual, counter.actual, inits.actual, body.actual);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::SingleValueStep::SingleValueStepPtr &node)
    {
      auto value = transformExpression(node->value);
      auto body = transformStatement(node->body);
      if (value.isNew || body.isNew) {
        return ir::SingleValueStep::create(value.actual, body.actual);
      }
      return nullptr;
    }

    ENode Transform::visit(const ir::Loop::LoopPtr &node)
    {
      auto states = transformLoopStates(node->states);

      if (states.isNew) {
        return ir::Loop::create(node->start, states.actual);
      }
      return nullptr;
    }

  }
}
