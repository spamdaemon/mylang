#ifndef CLASS_MYLANG_ETHIR_SSA_MAINTAINSSAPASS_H
#define CLASS_MYLANG_ETHIR_SSA_MAINTAINSSAPASS_H

#ifndef CLASS_MYLANG_ETHIR_SSA_SSAPASS_H
#include <mylang/ethir/ssa/SSAPass.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ssa {

      /**
       * This pass cleanups PHI functions that use variables that are no longer in use.
       * This pass should be run if blocks of code are removed.
       */
      class MaintainSSAPass: public SSAPass
      {
      public:
        MaintainSSAPass();

        /** Destructor */
      public:
        virtual ~MaintainSSAPass();

        /**
         * Apply this transform the specified node which is in ssa form.
         * @param node a node
         */
      protected:
        EProgram transform(EProgram src) override final;
        virtual bool requiresMaintainSSAPass() const override final;

      };
    }

  }
}
#endif
