#include <mylang/defs.h>
#include <mylang/ethir/ethir.h>
#include <mylang/ethir/ir/AbortStatement.h>
#include <mylang/ethir/ir/BreakStatement.h>
#include <mylang/ethir/ir/ContinueStatement.h>
#include <mylang/ethir/ir/ForeachStatement.h>
#include <mylang/ethir/ir/GlobalValue.h>
#include <mylang/ethir/ir/IfStatement.h>
#include <mylang/ethir/ir/InputPortDecl.h>
#include <mylang/ethir/ir/LambdaExpression.h>
#include <mylang/ethir/ir/Loop.h>
#include <mylang/ethir/ir/LoopStatement.h>
#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/OpaqueExpression.h>
#include <mylang/ethir/ir/OwnConstructorCall.h>
#include <mylang/ethir/ir/Phi.h>
#include <mylang/ethir/ir/ProcessBlock.h>
#include <mylang/ethir/ir/ProcessConstructor.h>
#include <mylang/ethir/ir/ProcessValue.h>
#include <mylang/ethir/ir/ProcessVariable.h>
#include <mylang/ethir/ir/Program.h>
#include <mylang/ethir/ir/ReturnStatement.h>
#include <mylang/ethir/ir/SkipStatement.h>
#include <mylang/ethir/ir/StatementBuilder.h>
#include <mylang/ethir/ir/ThrowStatement.h>
#include <mylang/ethir/ir/ToArray.h>
#include <mylang/ethir/ir/TryCatch.h>
#include <mylang/ethir/ir/ValueDecl.h>
#include <mylang/ethir/ir/Variable.h>
#include <mylang/ethir/ir/VariableDecl.h>
#include <mylang/ethir/ir/VariableUpdate.h>
#include <mylang/ethir/ir/YieldStatement.h>
#include <mylang/ethir/Name.h>
#include <mylang/ethir/prettyPrint.h>
#include <mylang/ethir/queries/FindBlocks.h>
#include <mylang/ethir/ssa/ToSSA.h>
#include <mylang/ethir/Transform.h>
#include <mylang/ethir/Visitor.h>

#include <mylang/names/Name.h>
#include <mylang/names.h>
#include <cassert>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

namespace mylang {
  namespace ethir {
    using namespace ir;
    using namespace types;
    namespace ssa {
      namespace {
        typedef queries::FindBlocks::Block::Ptr FindBlockPtr;

        typedef ::std::map<Name, EVariable> PhiVars;
        typedef ::std::map<NamePtr, PhiVars> PhiSet;
        typedef ::std::map<NamePtr, EVariable> PhiNames;

#if 0
        void printDebug(const PhiSet &s)
        {
          for (auto &i : s) {
            ::std::cerr << i.first->fullName() << " : ";
            for (auto &j : i.second) {
              ::std::cerr << ' ' << j.first;
            }
            ::std::cerr << ::std::endl;
          }
        }
#endif

        /** Keep track of the latest variables for a name */
        struct LoopInfo
        {
          LoopInfo(const Name &n)
              : name(n)
          {
          }

          Name name; // the name of the loop
          FindBlockPtr block;

          /** The current versions of a variable when leaving a loop or continuing a loop */
          PhiSet breakVars;
          PhiSet continueVars;
        };

        struct Try
        {
          FindBlockPtr tryBlock;
          /** Variables that may be active when throwing an exception or error */
          PhiSet catchVars;
        };

        struct Block
        {
          Block(Block *xparent)
              : parent(xparent), loop(nullptr), exceptionHandler(nullptr)
          {
          }

          void collectLatest(const Name &n, PhiSet &res) const
          {
            EVariable v = search(n.source());
            if (v) {
              res[v->name.source()][v->name] = v;
            }
          }

          void collectLatest(const ::std::set<Name> &names, PhiSet &res) const
          {
            for (auto n : names) {
              collectLatest(n, res);
            }
          }

          void collectLatest(const PhiNames &names, PhiSet &res) const
          {
            for (auto n : names) {
              collectLatest(n.second->name, res);
            }
          }

          void doReturn()
          {
            // TODO: what to do?
          }
          void doThrow()
          {
            Try *handler = findTry();
            if (handler) {
              collectLatest(handler->tryBlock->updates, handler->catchVars);
            }
          }

          void continueLoop(const Name &name)
          {
            LoopInfo &xloop = findLoop(name);

            // all the possibly updated variables in current loop need to be added
            collectLatest(xloop.block->updates, xloop.continueVars);
          }

          void breakLoop(const Name &name)
          {
            LoopInfo &xloop = findLoop(name);

            // all the possibly updated variables in current loop need to be added
            collectLatest(xloop.block->updates, xloop.breakVars);
          }

          Try* findTry()
          {
            Block *THIS = this;
            while (THIS) {
              if (THIS->exceptionHandler) {
                return THIS->exceptionHandler;
              }
              THIS = THIS->parent;
            }
            return nullptr;
          }

          LoopInfo& findLoop(const Name &name)
          {
            Block *THIS = this;
            while (THIS) {
              if (THIS->loop && THIS->loop->name == name) {
                return *THIS->loop;
              }
              THIS = THIS->parent;
            }
            throw ::std::runtime_error("No such loop " + name.toString());
          }

          // create a new version of name and save it as the latest
          EVariable initialVersion(const EVariable &v)
          {
            auto n = search(v->name.source());
            if (n) {
              throw ::std::runtime_error("Already have name " + v->name.toString());
            }
            EVariable res = v; //ir::Variable::create(v->scope, v->type, v->name.newVersion());
            latest[v->name.source()] = res;
            return res;
          }

          // create a new version of name and save it as the latest
          EVariable newVersion(const EVariable &var)
          {
            EVariable n = search(var->name.source());
            if (!n) {
              throw ::std::runtime_error("Name not found " + var->name.toString());
            }
            EVariable res = ir::Variable::create(n->scope, n->type, n->name.newVersion());
            latest[var->name.source()] = res;
            return res;
          }

          // create a new version of name and save it as the latest
          void setLatest(const EVariable &var)
          {
            latest[var->name.source()] = var;
          }

          void createPhiNames(const ::std::set<Name> &names, PhiNames &phiNames)
          {
            for (auto n : names) {
              EVariable var = search(n.source());
              if (!var) {
                throw ::std::runtime_error("Name not found " + n.toString());
              }
              EVariable res = ir::Variable::create(var->scope, var->type, var->name.newVersion());
              latest[var->name.source()] = res;
              phiNames[n.source()] = res;
            }
          }

          EVariable find(const NamePtr &name) const
          {
            auto i = latest.find(name);
            if (i == latest.end()) {
              return nullptr;
            } else {
              return i->second;
            }
          }

          EVariable search(const NamePtr &name) const
          {
            const Block *THIS = this;
            while (THIS) {
              auto v = THIS->find(name);
              if (v) {
                return v;
              }
              THIS = THIS->parent;
            }
            return nullptr;
          }

          ::std::map<NamePtr, EVariable> latest;

          Block *parent;
          LoopInfo *loop;
          Try *exceptionHandler;
        }
        ;

        struct ToSSAPass: public Transform
        {
          ToSSAPass(ToSSA::ThrowNodes throwers, const queries::FindBlocks::Blocks &findBlocks)
              : top(nullptr), current(&top), throwNodes(throwers), blocks(findBlocks)
          {
          }

          ~ToSSAPass()
          {
          }

          ENode visit(const ir::LambdaExpression::LambdaExpressionPtr &node) override
          {
            Block *restore = current;
            Block b(current);
            current = &b;
            ENode res = Transform::visit(node);
            current = restore;
            return res;
          }

          ENode visit(const ir::ProcessConstructor::ProcessConstructorPtr &node)
          {
            Block *restore = current;
            Block b(nullptr);
            current = &b;
            ENode res = Transform::visit(node);
            current = restore;
            return res;
          }

          ENode visit(const ir::ProcessBlock::ProcessBlockPtr &node)
          {
            Block *restore = current;
            Block b(nullptr);
            current = &b;
            ENode res = Transform::visit(node);
            current = restore;
            return res;
          }

          ENode visit(const ir::Variable::VariablePtr &node) override
          {
            EVariable v = current->search(node->name.source());
            if (v && v != node) {
              return v;
            } else {
              // could be a process variable or global variable
              return nullptr;
            }
          }

          ENode visit(const ir::ValueDecl::ValueDeclPtr &node) override
          {
            checkThrowing(node->value);
            // don't remap if we have a value
            if (node->value) {
              return Transform::visit(node);
            }

            current->initialVersion(node->variable);

            auto next = transformStatement(node->next);
            return nop(next.actual);
          }

          ENode visit(const ir::VariableDecl::VariableDeclPtr &node) override
          {
            auto initial = current->initialVersion(node->variable);
            auto next = transformStatement(node->next);

            if (node->value) {
              auto expr = transformExpr(node->value);
              return ir::ValueDecl::create(initial, expr.actual, next.actual);
            } else {
              auto expr = ir::NoValue::create(node->type);
              return ir::ValueDecl::create(initial, expr, next.actual);
            }
          }

          ENode visit(const ir::VariableUpdate::VariableUpdatePtr &node) override
          {
            checkThrowing(node);
            auto value = transformExpression(node->value);
            auto newName = current->newVersion(node->target);
            auto newVar = transformExpression(node->target);
            assert(newName->name == newVar.actual->name);
            auto next = transformStatement(node->next);
            return ir::ValueDecl::createSimplified(newVar.actual, value.actual, next.actual);
          }

          queries::FindBlocks::Block::Ptr findBlock(const EStatement &stmt)
          {
            auto i = blocks.find(stmt);
            assert(i != blocks.end());
            return i->second;
          }

          ENode visit(const ir::IfStatement::IfStatementPtr &node) override
          {
            auto cond = transformExpression(node->condition);

            Block *restore(current);
            Block tBlock(current);
            Block fBlock(current);

            current = &tBlock;
            auto iftrue = transformStatement(node->iftrue);

            current = &fBlock;
            auto iffalse = transformStatement(node->iffalse);
            current = restore;

            // collect the updates that were made in both branches
            ::std::set<Name> updates;
            const auto &tb = findBlock(node->iftrue)->updates;
            updates.insert(tb.begin(), tb.end());
            const auto &fb = findBlock(node->iffalse)->updates;
            updates.insert(fb.begin(), fb.end());

            PhiSet appendPhi;
            tBlock.collectLatest(updates, appendPhi);
            fBlock.collectLatest(updates, appendPhi);

            StatementBuilder sb;
            // IMPORTANT: create the phi before processing the next statement!!!
            bool havePhi = createPhi(current, nullptr, appendPhi, sb);

            auto next = transformStatement(node->next);
            if (havePhi || cond.isNew || iftrue.isNew || iffalse.isNew || next.isNew) {
              return ir::IfStatement::create(cond.actual, iftrue.actual, iffalse.actual,
                  sb.build(next.actual));
            }
            return nullptr;
          }

          ENode visit(const ir::ForeachStatement::ForeachStatementPtr &node) override
          {
            auto data = transformExpression(node->data);

            Block *restore = current;
            Block loopBody(current);
            LoopInfo loop(node->name);

            // each modified variable in the loop body needs to get a phi name
            loop.block = findBlock(node->body);
            loopBody.loop = &loop;

            current->collectLatest(loop.block->updates, loop.continueVars);
            current->collectLatest(loop.block->updates, loop.breakVars);

            current = &loopBody;
            PhiNames phiNames;
            current->createPhiNames(loop.block->updates, phiNames);

            auto body = transformStatement(node->body);
            current->collectLatest(loop.block->updates, loop.continueVars);
            current->collectLatest(loop.block->updates, loop.breakVars);

            current = restore;

            StatementBuilder prependPhi, appendPhi;
            bool prepend = createPhi(&loopBody, &phiNames, loop.continueVars, prependPhi);
            bool append = createPhi(current, nullptr, loop.breakVars, appendPhi);

            auto next = transformStatement(node->next);
            if (prepend || append || data.isNew || body.isNew || next.isNew) {
              return ir::ForeachStatement::create(node->name, node->variable, data.actual,
                  prependPhi.build(body.actual), appendPhi.build(next.actual));
            }
            return nullptr;
          }

          ENode visit(const ir::LoopStatement::LoopStatementPtr &node) override
          {
            Block *restore = current;

            Block loopBody(current);
            LoopInfo loop(node->name);
            loop.name = node->name;
            loop.block = findBlock(node->body);
            loopBody.loop = &loop;

            current->collectLatest(loop.block->updates, loop.continueVars);

            current = &loopBody;
            PhiNames phiNames;
            current->createPhiNames(loop.block->updates, phiNames);

            auto body = transformStatement(node->body);
            current->collectLatest(loop.block->updates, loop.continueVars);
            current = restore;

            StatementBuilder prependPhi, appendPhi;
            bool prepend = createPhi(&loopBody, &phiNames, loop.continueVars, prependPhi);
            bool append = createPhi(current, nullptr, loop.breakVars, appendPhi);

            auto next = transformStatement(node->next);
            if (prepend || append || body.isNew || next.isNew) {
              return ir::LoopStatement::create(node->name, prependPhi.build(body.actual),
                  appendPhi.build(next.actual));
            }
            return nullptr;
          }

          ENode visit(const ir::BreakStatement::BreakStatementPtr &node) override
          {
            current->breakLoop(node->scope);
            return nullptr;
          }

          ENode visit(const ir::ContinueStatement::ContinueStatementPtr &node) override
          {
            current->continueLoop(node->scope);
            return nullptr;
          }

          ENode visit(const ir::TryCatch::TryCatchPtr &node) override
          {
            PhiSet phi;

            Block *restore = current;

            Block tryBlock(current);
            Block catchBlock(&tryBlock);
            Try handler;
            handler.tryBlock = findBlock(node->tryBody);
            tryBlock.exceptionHandler = &handler;
            current = &tryBlock;
            auto doTry = transformStatement(node->tryBody);
            current->collectLatest(handler.tryBlock->updates, phi);
            current->collectLatest(findBlock(node->catchBody)->updates, phi);

            // now that we're done we need to clear the exception handler
            // so that exceptions in the catch bubble up past the try
            tryBlock.exceptionHandler = nullptr;

            StatementBuilder catchBuilder;
            current = &catchBlock;
            std::set<Name> additionalNames;
            bool beforeCatch = createPhi(current, nullptr, handler.catchVars, catchBuilder,
                &additionalNames);

            auto doCatch = transformStatement(node->catchBody);

            current->collectLatest(findBlock(node->catchBody)->updates, phi);
            current->collectLatest(additionalNames, phi);
            current = restore;

            StatementBuilder tryCatchBuilder;
            bool append = createPhi(current, nullptr, phi, tryCatchBuilder);
            auto next = transformStatement(node->next);

            if (append || beforeCatch || doTry.isNew || doCatch.isNew || next.isNew) {
              return ir::TryCatch::create(doTry.actual, catchBuilder.build(doCatch.actual),
                  tryCatchBuilder.build(next.actual));
            }
            return nullptr;
          }

          ENode visit(const ir::ThrowStatement::ThrowStatementPtr &node) override
          {
            auto value = transformExpression(node->value);
            current->doThrow();

            if (value.isNew) {
              return ThrowStatement::create(value.actual);
            }
            return nullptr;
          }

          ENode visit(const ir::AbortStatement::AbortStatementPtr &node) override
          {
            auto message = transformExpression(node->value);
            current->doThrow();

            if (message.isNew) {
              return ir::AbortStatement::create(message.actual);
            }
            return nullptr;
          }

          ENode visit(const ir::ReturnStatement::ReturnStatementPtr &node) override
          {
            auto res = transformExpression(node->value);
            current->doReturn();

            if (res.isNew) {
              return ir::ReturnStatement::create(res.actual);
            }
            return nullptr;
          }

          ENode visit(const ir::Phi::PhiPtr&) override
          {
            // since the arguments to a phi node are always values, not variables,
            // they can never be assigned and cannot ever change
            // this way: toSSA(toSSA(program)) == toSSA(program)
            return nullptr;
          }

          ENode visit(const ir::YieldStatement::YieldStatementPtr&) override
          {
            // it's a bug if we ever get here! Yield statements only appear
            // in a ir::Loop which will not be converted to SSA form.
            throw ::std::runtime_error("Unexpected YieldStatement in toSSA");
          }

          ENode visit(const ir::SkipStatement::SkipStatementPtr&) override
          {
            // it's a bug if we ever get here! See for YieldStatement above for details.
            throw ::std::runtime_error("Unexpected SkipStatement in toSSA");
          }

          ENode visit(const ir::Loop::LoopPtr&) override
          {
            throw ::std::runtime_error("Unexpected Loop node in toSSA");
          }

          ENode visit(const ir::ToArray::ToArrayPtr &node) override
          {
            auto opaque = OpaqueExpression::create(node);
            OpaqueExpression::Variables vars(opaque->arguments);
            for (auto &v : vars) {
              v = transformExpression(v).actual;
            }
            return opaque->changeArguments(vars);
          }

          ENode visit(const ir::InputPortDecl::InputPortDeclPtr &node) override
          {
            return Transform::visit(node);
          }

          ENode visit(const ir::OwnConstructorCall::OwnConstructorCallPtr &node) override
          {
            checkThrowing(node);
            return Transform::visit(node);
          }

          ENode visit(const ir::GlobalValue::GlobalValuePtr &node) override
          {
            checkThrowing(node);
            return Transform::visit(node);
          }

          ENode visit(const ir::ProcessValue::ProcessValuePtr &node) override
          {
            checkThrowing(node);
            return Transform::visit(node);
          }

          ENode visit(const ir::ProcessVariable::ProcessVariablePtr &node) override
          {
            checkThrowing(node);
            return Transform::visit(node);
          }

        private:
          void checkThrowing(const EStatement &node)
          {
            if (throwNodes.count(node) != 0) {
              // since the statement may throw, we need to
              current->doThrow();
            }
          }
          void checkThrowing(const EExpression &node)
          {
            if (node) {
              auto op = node->self<OperatorExpression>();
              if (op && op->name == OperatorExpression::OP_CALL) {
                // threat calls as always throwing
                // FIXE: try to prove that it doesn't throw
                current->doThrow();
              }
            }
          }

          bool createPhi(Block *block, const PhiNames *phiNames, const PhiSet &phis,
              StatementBuilder &b, ::std::set<Name> *optNames = nullptr) const
          {

            bool havePhi = false;
            for (auto &phi : phis) {

              // completely ignore the phi, because there
              // is only choice
              if (phi.second.size() == 1) {
                auto var = *phi.second.begin();
                // we don't need to create a new node, we just
                // need to record that the variable is the latest
                block->setLatest(var.second);
                continue;
              }

              EVariable phi0 = phi.second.begin()->second;
              EVariable phiName;
              if (phiNames) {
                phiName = phiNames->find(phi0->name.source())->second;
              } else {
                phiName = block->newVersion(phi0);
              }

              ir::Phi::Arguments args;
              for (auto &v : phi.second) {
                if (phiName->name != v.second->name) {
                  args.push_back(v.second);
                }
              }
              if (!args.empty()) {
                auto phiExpr = ir::Phi::create(args);
                b.declareValue(phiName, phiExpr);
                if (optNames) {
                  optNames->insert(phiName->name);
                }
                havePhi = true;
              }
            }
            return havePhi;
          }

          /** A dummy block */
          Block top;

          /** The current basic block */
          Block *current;

          /** The functions that will throw */
          const ToSSA::ThrowNodes &throwNodes;

          /** The blocks with variables updates and definitions */
          const queries::FindBlocks::Blocks &blocks;
        };

      }

      mylang::ethir::ir::Program::ProgramPtr ToSSA::transform(
          mylang::ethir::ir::Program::ProgramPtr node)
      {
        const queries::FindBlocks::Blocks blocks = queries::FindBlocks::find(node);
        ThrowNodes throwers;
        ToSSAPass tx(throwers, blocks);
        auto prg = tx.visitNode(node);
        if (prg) {
          return prg->self<Program>();
        }
        return node;
      }

      mylang::ethir::ir::LambdaExpression::LambdaExpressionPtr ToSSA::transform(
          mylang::ethir::ir::LambdaExpression::LambdaExpressionPtr node,
          const ToSSA::ThrowNodes &throwers)
      {
        const queries::FindBlocks::Blocks blocks = queries::FindBlocks::find(node);
        ToSSAPass tx(throwers, blocks);
        auto prg = tx.visitNode(node);
        if (prg) {
          return prg->self<LambdaExpression>();
        }
        return node;
      }

    }
  }
}
