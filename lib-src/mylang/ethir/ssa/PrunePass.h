#ifndef CLASS_MYLANG_ETHIR_SSA_PRUNEPASS_H
#define CLASS_MYLANG_ETHIR_SSA_PRUNEPASS_H

#ifndef CLASS_MYLANG_ETHIR_SSA_SSAPASS_H
#include <mylang/ethir/ssa/SSAPass.h>
#endif

#include <functional>

namespace mylang {
  namespace ethir {
    namespace ssa {

      /**
       * The prune pass performs three tasks:
       * 1. it removes unused variables
       * 2. it replaces occurrences of `y` with variable `x` if `val y=x`
       * 3. it remove statements that are NOPs, such as `if(x) {} else {}`
       * Even though specific passes may still be required for each of those,
       * because the cost of implementing these tasks together is small, it makes
       * sense to them together, so that subsequent passes can be performed faster.
       */
      class PrunePass: public SSAPass
      {
        /** A filter the determines eligibility of global variables for pruning */
      public:
        typedef ::std::function<bool(EGlobal)> Filter;

      public:
        PrunePass();

      public:
        PrunePass(Filter f);

        /** Destructor */
      public:
        virtual ~PrunePass();

        /**
         * Apply this transform the specified node which is in ssa form.
         * @param node a node
         */
      protected:
        EProgram transform(EProgram src) override final;

        /** True if well-known globals are eligible for pruning */
      public:
        const Filter globalValueFilter;
      };
    }

  }
}
#endif
