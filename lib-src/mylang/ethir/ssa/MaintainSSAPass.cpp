#include <mylang/ethir/ssa/MaintainSSAPass.h>
#include <mylang/ethir/Environment.h>
#include <mylang/ethir/ssa/SSATransform.h>
#include <mylang/ethir/types/types.h>

#include <cassert>
#include <map>

namespace mylang {
  namespace ethir {
    using namespace ir;
    namespace ssa {
      namespace {
        static EProgram cleanupPhiNodes(EProgram src)
        {
          struct CollectVariables: public Transform
          {
            CollectVariables()
                : env(::std::make_shared<Environment>())
            {
            }
            ~CollectVariables()
            {
            }

            ENode visit(const ir::ValueDecl::ValueDeclPtr &node) override
            {
              env->addBinding(node->name, node->value);
              return Transform::visit(node);
            }

            /** The current environment */
            Environment::Ptr env;
          };

          struct CleanupPhi: public SSATransform
          {
            CleanupPhi(Environment::Ptr xenv)
                : env(xenv)
            {
            }
            ~CleanupPhi()
            {
            }

            ENode visit(const ir::Phi::PhiPtr &node) override
            {
              bool found = true;
              for (auto arg : node->arguments) {
                if (!env->lookup(arg->name)) {
                  found = false;
                  break;
                }
              }
              if (found) {
                return nullptr;
              }
#if 1
              ::std::set<Name> removed;
#endif
              Phi::Arguments args;
              for (auto arg : node->arguments) {
                if (env->lookup(arg->name)) {
                  args.push_back(arg);
                } else {
#if 1
                  removed.insert(arg->name);
#endif
                }
              }

#if 1
              if (args.size() == 0) {
                ::std::cerr << "Invalid phi name removals ";
                for (auto n : removed) {
                  ::std::cerr << n << ::std::endl;
                }
              }
#endif
              assert(args.size() > 0);
              return ir::Phi::create(args);
            }

            /** The current environment */
            Environment::Ptr env;

          };

          CollectVariables pass1;
          pass1.visitNode(src);
          CleanupPhi pass2(pass1.env);
          auto res = pass2.visitNode(src);
          return res ? res->self<Program>() : nullptr;
        }
      }
      MaintainSSAPass::MaintainSSAPass()
      {
      }

      MaintainSSAPass::~MaintainSSAPass()
      {
      }

      EProgram MaintainSSAPass::transform(EProgram src)
      {
        return cleanupPhiNodes(src);
      }

      bool MaintainSSAPass::requiresMaintainSSAPass() const
      {
        return false;
      }

    }

  }
}
