#ifndef CLASS_MYLANG_ETHIR_SSA_INLINEPASS_H
#define CLASS_MYLANG_ETHIR_SSA_INLINEPASS_H

#ifndef CLASS_MYLANG_ETHIR_SSA_SSAPASS_H
#include <mylang/ethir/ssa/SSAPass.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_LAMBDAEXPRESSION_H
#include <mylang/ethir/ir/LambdaExpression.h>
#endif

#include <functional>

namespace mylang {
  namespace ethir {
    namespace ssa {

      /**
       * The inline pass is used to inline functions. A predicate is used to determine
       * if a function should be inlined.
       */
      class InlinePass: public SSAPass
      {
        /** The predicate that determines if a function should be inlined */
      public:
        typedef ::std::function<bool(const EVariable&, const ELambda&)> Predicate;

        /**
         * Create an inline pass that uses the specified predicate to determine if a function should
         * be inlined.
         * @param predicate a predicate
         */
      public:
        InlinePass(const Predicate &predicate);

        /** Destructor */
      public:
        virtual ~InlinePass();

        /**
         * Apply this transform the specified node which is in ssa form.
         * @param node a node
         */
      protected:
        EProgram transform(EProgram src) override final;

        /** A function that inlines a simple lambda expression */
      public:
        static bool isSimpleFunction(const EVariable &name,
            const mylang::ethir::ir::LambdaExpression::LambdaExpressionPtr &ptr, size_t n);

        /** True if the logging statements can be eliminated */
      public:
        const Predicate predicate;
      };
    }

  }
}
#endif
