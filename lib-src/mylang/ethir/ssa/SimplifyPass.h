#ifndef CLASS_MYLANG_ETHIR_SSA_SIMPLIFYPASS_H
#define CLASS_MYLANG_ETHIR_SSA_SIMPLIFYPASS_H

#ifndef CLASS_MYLANG_ETHIR_SSA_PEEPHOLE_H
#include <mylang/ethir/ssa/Peephole.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_SSA_SSAPASS_H
#include <mylang/ethir/ssa/SSAPass.h>
#endif

#include <memory>

namespace mylang {
  namespace ethir {
    namespace ssa {

      /**
       * Rewrite a program by simplifing some expressions. This is a peephole
       * optimization pass.
       */
      class SimplifyPass: public SSAPass
      {
        /** The peepholes to apply */
      public:
        typedef ::std::vector<Peephole::Ptr> Peepholes;

        /**
         * Create a simplify pass with the specified peepholes.
         * @param peepholes the peepholes to apply
         */
      public:
        SimplifyPass(Peepholes peepholes);

        /** Destructor */
      public:
        virtual ~SimplifyPass();

        /**
         * Apply this transform the specified node which is in ssa form.
         * @param node a node
         */
      protected:
        EProgram transform(EProgram src) override final;

        /** The peepholes to apply */
      private:
        Peepholes peepholes;
      };
    }

  }
}
#endif
