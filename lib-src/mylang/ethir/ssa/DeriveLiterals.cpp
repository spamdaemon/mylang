#include <bits/stdint-intn.h>
#include <bits/stdint-uintn.h>
#include <mylang/BigInt.h>
#include <mylang/BigReal.h>
#include <mylang/BigUInt.h>
#include <mylang/ethir/ethir.h>
#include <mylang/ethir/prettyPrint.h>
#include <mylang/ethir/Environment.h>
#include <mylang/ethir/ir/AbstractOperatorVisitor.h>
#include <mylang/ethir/ir/Declaration.h>
#include <mylang/ethir/ir/ForeachStatement.h>
#include <mylang/ethir/ir/GetDiscriminant.h>
#include <mylang/ethir/ir/GetMember.h>
#include <mylang/ethir/ir/GlobalValue.h>
#include <mylang/ethir/ir/IfStatement.h>
#include <mylang/ethir/ir/LambdaExpression.h>
#include <mylang/ethir/ir/Literal.h>
#include <mylang/ethir/ir/LiteralArray.h>
#include <mylang/ethir/ir/LiteralBit.h>
#include <mylang/ethir/ir/LiteralBoolean.h>
#include <mylang/ethir/ir/LiteralByte.h>
#include <mylang/ethir/ir/LiteralChar.h>
#include <mylang/ethir/ir/LiteralInteger.h>
#include <mylang/ethir/ir/LiteralNamedType.h>
#include <mylang/ethir/ir/LiteralOptional.h>
#include <mylang/ethir/ir/LiteralReal.h>
#include <mylang/ethir/ir/LiteralString.h>
#include <mylang/ethir/ir/LiteralStruct.h>
#include <mylang/ethir/ir/LiteralTuple.h>
#include <mylang/ethir/ir/LiteralUnion.h>
#include <mylang/ethir/ir/NewUnion.h>
#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/OperatorExpression.h>
#include <mylang/ethir/ir/ProcessDecl.h>
#include <mylang/ethir/ir/ProcessValue.h>
#include <mylang/ethir/ir/Program.h>
#include <mylang/ethir/ir/StatementBlock.h>
#include <mylang/ethir/ir/ValueDecl.h>
#include <mylang/ethir/ir/Variable.h>
#include <mylang/ethir/Name.h>
#include <mylang/ethir/limits/limits.h>
#include <mylang/ethir/ssa/DeriveLiterals.h>
#include <mylang/ethir/queries/FindLimits.h>
#include <mylang/ethir/types/ArrayType.h>
#include <mylang/ethir/types/BitType.h>
#include <mylang/ethir/types/BooleanType.h>
#include <mylang/ethir/types/ByteType.h>
#include <mylang/ethir/types/CharType.h>
#include <mylang/ethir/types/IntegerType.h>
#include <mylang/ethir/types/NamedType.h>
#include <mylang/ethir/types/OptType.h>
#include <mylang/ethir/types/RealType.h>
#include <mylang/ethir/types/StringType.h>
#include <mylang/ethir/types/StructType.h>
#include <mylang/ethir/types/TupleType.h>
#include <mylang/ethir/types/Type.h>
#include <mylang/ethir/types/UnionType.h>
#include <mylang/ethir/ssa/SSATransform.h>
#include <mylang/ethir/TypeName.h>
#include <mylang/ethir/Visitor.h>
#include <stddef.h>
#include <cassert>
#include <cstdio>
#include <iterator>
#include <map>
#include <memory>
#include <optional>
#include <stdexcept>
#include <string>
#include <vector>

namespace mylang {
  namespace ethir {
    using namespace ir;
    using namespace limits;
    namespace ssa {
      namespace {

        // TODO: can we use the EnvironmentalTransform here?
        struct Pass: public SSATransform
        {
          Pass(const mylang::ethir::queries::FindLimits::VarBounds &b)
              : bounds(b)
          {
          }
          ~Pass()
          {
          }

          ENode visit(const ir::ValueDecl::ValueDeclPtr &node) override
          {
            if (node->value->self<Literal>() || node->value->self<Variable>()) {
              return Transform::visit(node);
            }

            Tx<EExpression> value(node->value, false);
            auto next = transformStatement(node->next);

            auto lim = bounds.find(node->name);
            if (lim != bounds.end()) {
              // see if we can convert to a literal
              EExpression lit = lim->second->getLiteral(node->type);
              if (lit) {

                // we need to cast to the literal type
                if (!lit->type->isSameType(*node->type)) {
                  EVariable v = Variable::create(Variable::Scope::FUNCTION_SCOPE, lit->type,
                      Name::create("tmp"));
                  lit = LetExpression::create(v, lit,
                      OperatorExpression::create(node->type, OperatorExpression::OP_CHECKED_CAST,
                          v));
                }

                value = Tx<EExpression>(lit, true);
              }
            }

            if (value.isNew || next.isNew) {
              return ValueDecl::createSimplified(node->variable, value.actual, next.actual);
            }
            return nullptr;
          }

          const mylang::ethir::queries::FindLimits::VarBounds &bounds;
        };

      }
      DeriveLiterals::DeriveLiterals()
      {
      }

      DeriveLiterals::~DeriveLiterals()
      {
      }

      EProgram DeriveLiterals::transform(EProgram src)
      {
        mylang::ethir::queries::FindLimits::VarBounds limits =
            mylang::ethir::queries::FindLimits::find(src);
        Pass pass(limits);
        auto res = pass.visitNode(src);
        return res ? res->self<Program>() : nullptr;
      }

    }

  }
}

