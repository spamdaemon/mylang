#include <mylang/ethir/ssa/SSAPass.h>
#include <mylang/ethir/ssa/MaintainSSAPass.h>

namespace mylang {
  namespace ethir {
    namespace ssa {

      SSAPass::SSAPass()
      {
      }

      SSAPass::~SSAPass()
      {
      }

      bool SSAPass::transformProgram(SSAProgram &prg)
      {
        auto new_prg = transform(prg.program);
        if (new_prg) {
          bool retval = prg.reset(new_prg);
          if (retval && requiresMaintainSSAPass()) {
            MaintainSSAPass pass;
            pass.transformProgram(prg);
          }
          return retval;
        } else {
          return false;
        }
      }

      bool SSAPass::requiresMaintainSSAPass() const
      {
        return false;
      }
    }
  }
}
