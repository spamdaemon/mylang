#include <mylang/ethir/ssa/PrunePass.h>
#include <mylang/ethir/ssa/MaintainSSAPass.h>
#include <mylang/ethir/queries/SideEffects.h>
#include <mylang/ethir/Environment.h>
#include <mylang/ethir/ssa/SSATransform.h>
#include <mylang/ethir/transforms/PruneGlobals.h>
#include <mylang/ethir/queries/FindReachableVariables.h>
#include <mylang/ethir/queries/CollectVariables.h>
#include <mylang/ethir/ir/DefaultNodeVisitor.h>

#include <cassert>
#include <map>

namespace mylang {
  namespace ethir {
    using namespace queries;
    using namespace ir;
    namespace ssa {

      namespace {

        static bool PRUNE_COMMENTS = true;

        static PrunePass::Filter makeFilter(PrunePass::Filter f = PrunePass::Filter())
        {
          if (f) {
            return f;
          } else {
            return [](EGlobal g)
            {
              return g->kind != GlobalValue::DEFAULT;
            };
          }
        }
      }

      PrunePass::PrunePass()
          : globalValueFilter(makeFilter())
      {
      }

      PrunePass::PrunePass(Filter f)
          : globalValueFilter(makeFilter(::std::move(f)))
      {
      }

      PrunePass::~PrunePass()
      {
      }

      EProgram PrunePass::transform(EProgram src)
      {
        struct Use
        {
          Use()
              : used(false)
          {
          }
          Use(bool u)
              : used(u)
          {
          }
          bool used; // TODO: document what values this takes on
        };

        struct ForwardUse: public DefaultNodeVisitor
        {
          ForwardUse(::std::map<Name, Use> &u)
              : uses(u)
          {
          }
          ~ForwardUse()
          {
          }

          void visitLambdaExpression(LambdaExpression::LambdaExpressionPtr) override
          {
          }
          void visitVariable(Variable::VariablePtr expr) override
          {
            // generate a use for a variable, that has not been defined yet
            uses.try_emplace(expr->name, true);
          }

          ::std::map<Name, Use> &uses;
        };

        // TODO: can we use the EnvironmentalTransform here?
        struct Pass: public SSATransform
        {
          Pass(const Filter &xfilter)
              : env(::std::make_shared<Environment>()), filter(xfilter),
                  pruneComments(PRUNE_COMMENTS)
          {
          }
          ~Pass()
          {
          }

          // a simple function to remove side-effects
          bool hasSideEffects(const EExpression &expr)
          {
            return queries::SideEffects::test(*env, expr) != queries::SideEffects::EFFECT_NONE;
          }

          ENode visit(const ir::CommentStatement::CommentStatementPtr &node) override
          {
            auto next = transformStatement(node->next);
            if (pruneComments) {
              return nop(next.actual);
            }
            if (next.isNew) {
              return ir::CommentStatement::create(node->comment, next.actual);
            }
            return nullptr;
          }

          ENode visit(const ir::LambdaExpression::LambdaExpressionPtr &node) override
          {
            auto restore = env;
            env = env->create();
            auto res = Transform::visit(node);
            env = restore;
            return res;
          }

          ENode visit(const ir::ProcessDecl::ProcessDeclPtr &node) override
          {
            auto restore = env;
            env = env->create();
            auto res = Transform::visit(node);
            env = restore;
            return res;
          }

          ir::Variable::VariablePtr findRootVariable(EExpression e)
          {
            auto v = e->self<ir::Variable>();
            if (v) {
              auto expr = env->lookup(v->name);
              if (expr) {
                auto var = expr->self<Variable>();
                assert(var && "only variables can be in the environment");
                return var;
              }
              return v;
            }
            return nullptr;
          }

          ENode visit(const ir::Variable::VariablePtr &node) override
          {
            if (node->scope != ir::Declaration::Scope::FUNCTION_SCOPE) {
              return nullptr;
            }

            auto expr = env->lookup(node->name);
            if (expr) {
              auto var = expr->self<Variable>();
              assert(var && "only variables can be in the environment");
              uses.insert_or_assign(var->name, true);
              return var;
            }
            uses.insert_or_assign(node->name, true);
            return nullptr;
          }

          ENode visit(const ir::Phi::PhiPtr &node) override
          {
            // FIXME: what to do here
            if (node->arguments.size() == 1) {
              return transformExpression(node->arguments.at(0)).actual;
            }
            for (auto var : node->arguments) {
              uses.insert_or_assign(var->name, true);
            }

            return nullptr;
          }

          ENode visit(const ir::LetExpression::LetExpressionPtr &node) override
          {
            // get rid of any aliases
            if (auto v = findRootVariable(node->value); v) {
              // we need to add a redirect so that any references
              // to node->name are counted as references to v
              env->addBinding(node->variable, v);
            }

            // transform the next statements
            auto in = transformExpr(node->result);

            // if we don't have side effects and the value isn't used, then we just get rid of this decl
            if (!hasSideEffects(node->value)) {
              // need to be careful, only variables that are no already used can be removed!
              if (uses[node->variable->name].used == false) {
                return in.actual;
              }
            }

            // need to transform the value as well
            auto value = transformExpr(node->value);
            if (value.isNew || in.isNew) {
              return ir::LetExpression::create(node->variable, value.actual, in.actual);
            }
            return nullptr;
          }

          ENode visit(const ir::ValueDecl::ValueDeclPtr &node) override
          {
            // get rid of any aliases
            if (auto v = findRootVariable(node->value); v) {
              // we need to add a redirect so that any references
              // to node->name are counted as references to v
              env->addBinding(node->name, v);
            }

            // it is possible that this node already has a use
            // e..g. in a loop, like this:
            // val x0 = ...
            // while(true) {
            //   val x1 = phi(x0,x2);
            //   ....
            //   val x2 = 0;
            // }
            //
            // ensure we have an entry
            uses.try_emplace(node->name, Use());

            ForwardUse fwd(uses);
            fwd.visitNode(node->value);

            // transform the next statements
            auto next = transformStatement(node->next);

            // if we don't have side effects and the value isn't used, then we just get rid of this decl
            if (!hasSideEffects(node->value)) {
              // need to be careful, only variables that are no already used can be removed!
              if (uses.at(node->name).used == false) {
                return nop(next.actual);
              }
            }

            // visit the value, which may reference variables that have not
            // yet been defined
            auto value = transformExpr(node->value);

            // need to transform the value as well
            if (value.isNew || next.isNew) {
              return ValueDecl::createSimplified(node->variable, value.actual, next.actual);
            }
            return nullptr;
          }

          ENode visit(const ir::IfStatement::IfStatementPtr &node) override
          {
            // need to process next first, because there may be phi functions referencing
            // variables in an if-branch
            auto next = transformWithEnv(env->create(), node->next);

            auto iftrue = transformWithEnv(env->create(), node->iftrue);
            auto iffalse = transformWithEnv(env->create(), node->iffalse);

            // check if the if-statement is empty before we check the condition
            // so that the variable remains unused
            if (iffalse.actual->isNOP() && iftrue.actual->isNOP()) {
              return nop(next.actual);
            }

            auto cond = transformExpression(node->condition);

            if (cond.isNew || next.isNew || iftrue.isNew || iffalse.isNew) {
              return IfStatement::create(cond.actual, iftrue.actual, iffalse.actual, next.actual);
            }
            return nullptr;
          }

          ENode visit(const ir::TryCatch::TryCatchPtr &node) override
          {
            // need to process next first, because there may be phi functions referencing
            // variables in an try-catch block
            auto next = transformWithEnv(env->create(), node->next);

            auto catchBlock = transformWithEnv(env->create(), node->catchBody);
            auto tryBlock = transformWithEnv(env->create(), node->tryBody);

            if (tryBlock.actual->isNOP()) {
              return nop(next.actual);
            }
            if (tryBlock.isNew || catchBlock.isNew || next.isNew) {
              return TryCatch::create(tryBlock.actual, catchBlock.actual, next.actual);
            }
            return nullptr;
          }

          ENode visit(const ir::ForeachStatement::ForeachStatementPtr &node) override
          {
            // need to process next first, because there may be phi functions referencing
            // variables in an if-branch
            auto next = transformWithEnv(env->create(), node->next);

            auto body = transformWithEnv(env->create(), node->body);

            if (body.actual->isNOP()) {
              return nop(next.actual);
            }

            auto data = transformExpression(node->data);

            if (data.isNew || body.isNew || next.isNew) {
              return ForeachStatement::create(node->name, node->variable, data.actual, body.actual,
                  next.actual);
            }
            return nullptr;
          }

          ENode visit(const ir::LoopStatement::LoopStatementPtr &node) override
          {
            // need to process next first
            auto next = transformWithEnv(env->create(), node->next);
            auto body = transformWithEnv(env->create(), node->body);

            if (body.actual->isNOP()) {
              return nop(next.actual);
            }
            if (body.isNew || next.isNew) {
              return LoopStatement::create(node->name, body.actual, next.actual);
            }
            return nullptr;
          }

          ENode visit(const ir::StatementBlock::StatementBlockPtr &node) override
          {
            // need to process next first, because there may be phi functions referencing
            // variables in an if-branch
            auto next = transformWithEnv(env->create(), node->next);
            auto body = transformWithEnv(env->create(), node->statements);

            if (body.isNew || next.isNew) {
              return StatementBlock::create(body.actual, next.actual);
            }
            return nullptr;
          }

          ENode visit(const EProgram &node) override
          {
            queries::FindReachableVariables::Names reachables =
                queries::FindReachableVariables::find(node, filter);
            Program::Globals survivors;
            for (auto i = node->globals.begin(); i != node->globals.end(); ++i) {
              if (reachables.count((*i)->name) != 0) {
                survivors.push_back(*i);
              }
            }

            if (survivors.size() == node->globals.size()) {
              return Transform::visit(node);
            }

            EProgram prg = Program::create(survivors, node->processes, node->types);
            return transformStatement(prg).actual;
          }

        private:
          Tx<EStatement> transformWithEnv(const ::std::shared_ptr<Environment> &new_env,
              EStatement stmt)
          {
            auto restore = env;
            env = new_env;
            auto res = transformStatement(stmt);
            env = restore;
            return res;
          }

          /** The current environment */
          ::std::shared_ptr<Environment> env;

          ::std::map<Name, Use> uses;
          const Filter &filter;
          bool pruneComments;
        };

        Pass pass(globalValueFilter);
        auto res = pass.visitNode(src);
        return res ? res->self<Program>() : nullptr;

      }

    }

  }
}
