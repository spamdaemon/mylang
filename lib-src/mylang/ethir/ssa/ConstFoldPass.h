#ifndef CLASS_MYLANG_ETHIR_SSA_CONSTFOLDPASS_H
#define CLASS_MYLANG_ETHIR_SSA_CONSTFOLDPASS_H

#ifndef CLASS_MYLANG_ETHIR_SSA_SSAPASS_H
#include <mylang/ethir/ssa/SSAPass.h>
#endif

#include <set>

namespace mylang {
  namespace ethir {
    namespace ssa {

      /**
       * The const-fold pass tries to replace expressions with literals
       * where it can. This pass optimizes simple expression, such as "x+0" to 0,
       * but it cannot optimize expression like x[0], where x is an array. The rewrite
       * pass will be able to perform those kinds of optimizations.
       */
      class ConstFoldPass: public SSAPass
      {
      public:
        struct LoggingConfiguration
        {
          LoggingConfiguration()
              : disableLogging(false)
          {
          }

          /** If true, then logging is completely disabled */
          bool disableLogging;

          /**
           * If logging is enabled and the level is known at compile time,
           * then it is checked against these levels. If no levels are specified,
           * then logging is enabled is delegated to runtime checking.
           */
          ::std::set<::std::string> levels;
        };

      public:
        ConstFoldPass();

        /** Destructor */
      public:
        virtual ~ConstFoldPass();

        /**
         * Apply this transform the specified node which is in ssa form.
         * @param node a node
         */
      protected:
        EProgram transform(EProgram src) override final;
        bool requiresMaintainSSAPass() const override final;

        /** The logging configuration */
      public:
        LoggingConfiguration logConfig;
      };
    }

  }
}
#endif
