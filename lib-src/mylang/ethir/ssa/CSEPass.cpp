#include <mylang/ethir/ssa/CSEPass.h>
#include <mylang/ethir/queries/SideEffects.h>
#include <mylang/ethir/EnvironmentalTransform.h>
#include <mylang/ethir/prettyPrint.h>

#include <cassert>
#include <unordered_map>
#include <optional>

namespace mylang {
  namespace ethir {
    using namespace ir;
    namespace ssa {

      CSEPass::CSEPass(bool xeliminateCommonLogging)
          : eliminateCommonLogging(xeliminateCommonLogging)
      {
      }

      CSEPass::~CSEPass()
      {
      }

      EProgram CSEPass::transform(EProgram src)
      {
        struct HashEquals
        {
          size_t operator()(const EExpression &e) const
          {
            return static_cast<size_t>(e->hashcode().value());
          }
          bool operator()(const EExpression &e, const EExpression &e1) const
          {
            return Node::equals(e, e1);
          }
        };

        struct Env: public Environment
        {

          Env(::std::shared_ptr<Env> ptr, const CSEPass &pass)
              : Environment(ptr), exprs(128), cse(pass)
          {
          }

          ~Env()
          {
          }

          Ptr create() override final
          {
            return ::std::make_shared<Env>(self<Env>(), cse);
          }

          bool isCacheable(const EExpression &value)
          {
            if (value->self<Variable>()) {
              return false;
            }
            if (value->self<Literal>()) {
              return false;
            }
            switch (queries::SideEffects::test(self(), value)) {
            case queries::SideEffects::EFFECT_NONE:
              return true;
            case queries::SideEffects::EFFECT_LOGGING:
              return cse.eliminateCommonLogging;
            default:
              return false;
            }
          }

          void addBinding(const Name &name,
              ::std::optional<mylang::ethir::ir::Variable::Scope> scope, const EExpression &value)
          {
            Environment::addBinding(name, scope, value);
            // register the binding, but only if it's not a variable
            // <code>val x = name<code> will be resolved in an other optimization step.
            if (value && isCacheable(value)) {
              exprs.emplace(value, name);
            }
          }

          ::std::optional<Name> findExpression(const EExpression &e) const
          {
            auto THIS = self<Env>();
            while (THIS) {
              auto i = THIS->exprs.find(e);
              if (i != THIS->exprs.end()) {
                return i->second;
              }
              if (THIS->parent()) {
                THIS = THIS->parent()->self<Env>();
              } else {
                break;
              }
            }
            return ::std::nullopt;
          }

          ::std::unordered_map<EExpression, Name, HashEquals, HashEquals> exprs;
          const CSEPass &cse;
        };

        struct Pass: public EnvironmentalTransform
        {
          Pass(const CSEPass &cse)
              : EnvironmentalTransform(::std::make_shared<Env>(nullptr, cse))
          {
          }
          ~Pass()
          {
          }

          ENode visitNode(const ENode &node) override final
          {
            auto e = node->self<Expression>();
            if (e) {
              // e cannot be a variable and anyways we're not binding them in the first place
              auto cur = current()->self<Env>();
              auto name = cur->findExpression(e);
              if (name.has_value()) {
                auto binding = cur->lookupBinding(name.value());
                if (binding.has_value()) {
                  auto B = binding.value();
                  // we can run into some issues with global and process values
                  // so only consider local scopes
                  if (B.scope == Variable::Scope::FUNCTION_SCOPE && B.value) {
#if 0
                    ::std::cerr << "Common subexpression found : " << name.value() << " : "
                        << prettyPrint(e) << ::std::endl;
#endif
                    return Variable::create(B.scope.value(), e->type, name.value());
                  }
                }
              }
            }
            return EnvironmentalTransform::visitNode(node);
          }
        }
        ;

        Pass pass(*this);
        auto res = pass.visitNode(src);
        return res ? res->self<Program>() : nullptr;

      }

    }

  }
}
