#include <mylang/ethir/ssa/peepholes/Optionals.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/types/types.h>

namespace mylang {
  namespace ethir {
    using namespace ir;
    using namespace types;

    namespace ssa {
      namespace peepholes {

        Peephole::Ptr Optionals::getPeephole()
        {
          struct Impl: public Peephole
          {
            ~Impl()
            {
            }

            ENode apply(const ENode &node, const Environment &env) const
            {
              auto candidates = matches(node, env, ::std::nullopt, OperatorExpression::OP_NEW);
              if (!candidates.first || !candidates.second->type->self<OptType>()) {
                return nullptr;
              }

              switch (candidates.first->name) {
              case OperatorExpression::OP_IS_PRESENT: {
                auto present = candidates;
                return LiteralBoolean::create(present.second->arguments.size() != 0);
              }

              case OperatorExpression::OP_GET: {
                auto get = candidates;
                if (get.second->arguments.size() == 1) {
                  return get.second->arguments.at(0);
                }
                break;
              }
              case OperatorExpression::OP_MAP_OPTIONAL: {
                auto map_new = candidates;
                if (map_new.second->arguments.empty()) {
                  return OperatorExpression::create(map_new.first->type, OperatorExpression::OP_NEW);
                }
                if (map_new.second->arguments.size() == 1) {
                  return LetExpression::create(
                      OperatorExpression::create(OperatorExpression::OP_CALL,
                          map_new.first->arguments.at(1), map_new.second->arguments.at(0)),
                      [&](
                          EVariable res) {
                            return OperatorExpression::create(map_new.first->type,OperatorExpression::OP_NEW,res);
                          });
                }
                break;
              }
              default:
                break;
              }
              ;

              return nullptr;
            }

          };

          return ::std::make_shared<Impl>();
        }

      }
    }
  }
}
