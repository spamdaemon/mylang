#include <mylang/ethir/ssa/peepholes/BitLikeOps.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/types/types.h>

namespace mylang {
  namespace ethir {
    using namespace ir;
    using namespace types;

    namespace ssa {
      namespace peepholes {

        Peephole::Ptr BitLikeOps::getPeephole()
        {
          struct Impl: public Peephole
          {
            ~Impl()
            {
            }

            ENode apply(const ENode &node, const Environment &env) const
            {
              {
                auto not_not = matches(node, env, OperatorExpression::OP_NOT,
                    OperatorExpression::OP_NOT);
                if (not_not.second) {
                  return not_not.second->arguments.at(0);
                }
              }
              {
                auto and_or = matches(node,
                    { OperatorExpression::OP_AND, OperatorExpression::OP_OR });
                if (and_or && Node::equals(and_or->arguments.at(0), and_or->arguments.at(1))) {
                  return and_or->arguments.at(0);
                }
              }
              {
                auto x_or = matches(node, OperatorExpression::OP_XOR);
                if (x_or && Node::equals(x_or->arguments.at(0), x_or->arguments.at(1))) {
                  if (x_or->type->self<BooleanType>()) {
                    return LiteralBoolean::create(false);
                  } else if (x_or->type->self<BitType>()) {
                    return LiteralBit::create(false);
                  } else if (x_or->type->self<ByteType>()) {
                    return LiteralByte::create(0);
                  }
                }
              }
              return nullptr;
            }

          };

          return ::std::make_shared<Impl>();
        }

      }
    }
  }
}
