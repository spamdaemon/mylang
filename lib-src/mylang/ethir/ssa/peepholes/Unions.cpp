#include <mylang/ethir/ssa/peepholes/Unions.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/types/types.h>

namespace mylang {
  namespace ethir {
    using namespace ir;
    using namespace types;

    namespace ssa {
      namespace peepholes {

        Peephole::Ptr Unions::getPeephole()
        {
          struct Impl: public Peephole
          {
            ~Impl()
            {
            }

            ENode apply(const ENode &node, const Environment &env) const
            {
              auto get_member = node->self<GetMember>();
              if (get_member) {
                auto ctor = env.find<NewUnion>(get_member->object);
                if (ctor) {
                  if (ctor->member == get_member->name) {
                    return ctor->value;
                  }
                  return nullptr;
                }
              }
              auto get_disciminant = node->self<GetDiscriminant>();
              if (get_disciminant) {
                auto ctor = env.find<NewUnion>(get_disciminant->object);
                if (ctor) {
                  auto ty = ctor->type->self<UnionType>();
                  return ty->getMemberDiscriminant(ctor->member);
                }
              }

              return nullptr;
            }

          };

          return ::std::make_shared<Impl>();
        }

      }
    }
  }
}
