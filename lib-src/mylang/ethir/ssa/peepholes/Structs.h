#ifndef CLASS_MYLANG_ETHIR_SSA_PEEPHOLES_STRUCTS_H
#define CLASS_MYLANG_ETHIR_SSA_PEEPHOLES_STRUCTS_H

#ifndef CLASS_MYLANG_ETHIR_SSA_PEEPHOLE_H
#include <mylang/ethir/ssa/Peephole.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ssa {
      namespace peepholes {

        class Structs
        {
        private:
          ~Structs() = delete;

          /**
           * Get a peep hole that optimizes GET_MEMBER
           */
        public:
          static Peephole::Ptr getPeephole();
        };

      }
    }
  }
}
#endif
