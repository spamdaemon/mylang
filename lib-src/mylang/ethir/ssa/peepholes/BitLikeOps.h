#ifndef CLASS_MYLANG_ETHIR_SSA_PEEPHOLES_BITLIKEOPS_H
#define CLASS_MYLANG_ETHIR_SSA_PEEPHOLES_BITLIKEOPS_H

#ifndef CLASS_MYLANG_ETHIR_SSA_PEEPHOLE_H
#include <mylang/ethir/ssa/Peephole.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ssa {
      namespace peepholes {

        class BitLikeOps
        {
        private:
          ~BitLikeOps() = delete;

          /**
           * Peephole to optimize NOT, AND, OR, and XOR.
           */
        public:
          static Peephole::Ptr getPeephole();
        };

      }
    }
  }
}
#endif
