#ifndef CLASS_MYLANG_ETHIR_SSA_PEEPHOLES_ARRAYS_H
#define CLASS_MYLANG_ETHIR_SSA_PEEPHOLES_ARRAYS_H

#ifndef CLASS_MYLANG_ETHIR_SSA_PEEPHOLE_H
#include <mylang/ethir/ssa/Peephole.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ssa {
      namespace peepholes {

        class Arrays
        {
        private:
          ~Arrays() = delete;

          /**
           * Get a peep hole that optimizes OP_SIZE, INDEX, SUBRANGE, etc.
           */
        public:
          static Peephole::Ptr getPeephole();

          /**
           * Get a peephole that optimizes subrange
           * It only makes sense to optimize subrange:
           * 1. if we're the only user of the array
           * 2. if subrange duplicates arrays anyways
           */
        public:
          static Peephole::Ptr getSubrangePeephole();

          /**
           * Get a peephole that propagates reverse() up as much as possible.
           * E.g. reverse(map(A)) -> map(reverse(A))
           */
        public:
          static Peephole::Ptr getReversePeephole();
        };

      }
    }
  }
}
#endif
