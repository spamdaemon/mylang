#include <mylang/ethir/ssa/peepholes/Arrays.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/types/types.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    using namespace ir;
    using namespace types;

    namespace ssa {
      namespace peepholes {

        namespace {
          static ENode apply_OP_SIZE(const Environment &env,
              const ::std::pair<OperatorExpression::OperatorExpressionPtr,
                  OperatorExpression::OperatorExpressionPtr> &candidates)
          {
            switch (candidates.second->name) {
            case OperatorExpression::OP_NEW: {
              auto size_new = candidates;
              return LiteralInteger::create(size_new.first->type->self<IntegerType>(),
                  size_new.second->arguments.size());
            }
            case OperatorExpression::OP_SUBRANGE: {
              auto size_subrange = candidates;
              auto arrTy = size_subrange.second->type->self<ArrayType>();
              auto jstart = env.find<LiteralInteger>(size_subrange.second->arguments.at(1));
              auto jstop = env.find<LiteralInteger>(size_subrange.second->arguments.at(2));
              if (jstart && jstop) {
                auto jsize = jstop->value - jstart->value;

                if (jsize >= 0) {
                  return LiteralInteger::create(arrTy->lengthType, jsize);
                }
              }
              break;
            }
            default:
              break;
            }
            return nullptr;
          }

          static ENode apply_OP_INDEX(const Environment &env,
              const ::std::pair<OperatorExpression::OperatorExpressionPtr,
                  OperatorExpression::OperatorExpressionPtr> &candidates)
          {
            switch (candidates.second->name) {
            case OperatorExpression::OP_NEW: {
              auto index_new = candidates;
              auto at = env.find<LiteralInteger>(index_new.first->arguments.at(1));
              if (at) {
                auto i = at->value.unsignedValue();
                if (i < index_new.second->arguments.size()) {
                  return index_new.second->arguments.at(i);
                }
              }
              break;
            }
            case OperatorExpression::OP_SUBRANGE: {
              auto index_subrange = candidates;
              auto arrTy = index_subrange.second->type->self<ArrayType>();
              auto i = env.find<LiteralInteger>(index_subrange.first->arguments.at(1));
              auto jstart = env.find<LiteralInteger>(index_subrange.second->arguments.at(1));
              auto jstop = env.find<LiteralInteger>(index_subrange.second->arguments.at(2));
              if (i && jstart && jstop) {

                auto jsize = jstop->value - jstart->value;

                if (i->value < jsize) {
                  auto newIndex = LiteralInteger::create(arrTy->indexType.value(),
                      i->value + jstart->value);

                  return LetExpression::create(newIndex,
                      [&](
                          EVariable xindex) {
                            return OperatorExpression::create(index_subrange.first->type,OperatorExpression::OP_INDEX, {index_subrange.second->arguments.at(0),xindex});
                          });
                }
              }
              break;
            }
            default:
              break;
            }
            return nullptr;
          }

          static ENode apply_OP_SUBRANGE(const Environment &env,
              const ::std::pair<OperatorExpression::OperatorExpressionPtr,
                  OperatorExpression::OperatorExpressionPtr> &candidates)
          {
            switch (candidates.second->name) {
            case OperatorExpression::OP_SUBRANGE: {
              auto subrange_subrange = candidates;
              auto arrTy = subrange_subrange.second->type->self<ArrayType>();
              auto istart = env.find<LiteralInteger>(subrange_subrange.first->arguments.at(1));
              auto istop = env.find<LiteralInteger>(subrange_subrange.first->arguments.at(2));
              auto jstart = env.find<LiteralInteger>(subrange_subrange.second->arguments.at(1));
              auto jstop = env.find<LiteralInteger>(subrange_subrange.second->arguments.at(2));

              if (istart && istop && jstart && jstop && istart->value <= jstop->value
                  && jstart->value <= jstop->value) {

                auto isize = istop->value - istart->value;
                auto jsize = jstop->value - jstart->value;

                if (istop->value <= jsize) {
                  auto start = LiteralInteger::create(arrTy->counterType,
                      istart->value + jstart->value);
                  auto stop = LiteralInteger::create(arrTy->counterType, start->value + isize);

                  return LetExpression::create(start,
                      [&](
                          EVariable xstart) {
                            return LetExpression::create(stop,[&](EVariable xstop) {
                                  return OperatorExpression::create(subrange_subrange.first->type,OperatorExpression::OP_SUBRANGE, {subrange_subrange.second->arguments.at(0),xstart,xstop});
                                });
                          });
                }
              }
              break;
            }
            default:
              break;
            }
            return nullptr;
          }

        }

        Peephole::Ptr Arrays::getPeephole()
        {
          struct Impl: public Peephole
          {
            const ::std::vector<OperatorExpression::Operator> INNERS, OUTERS;

            Impl()
                : INNERS( { OperatorExpression::OP_NEW, OperatorExpression::OP_SUBRANGE }),
                    OUTERS( { OperatorExpression::OP_SIZE, OperatorExpression::OP_INDEX,
                        OperatorExpression::OP_SUBRANGE })
            {
            }

            ~Impl()
            {
            }

            ENode apply(const ENode &node, const Environment &env) const
            {
              auto candidates = matches(node, env, OUTERS, INNERS);
              if (!candidates.second || !candidates.second->type->self<ArrayType>()) {
                return nullptr;
              }

              switch (candidates.first->name) {
              case OperatorExpression::OP_SIZE: // OP_SIZE of (OP_NEW | OP_SUBRANGE)
                return apply_OP_SIZE(env, candidates);
              case OperatorExpression::OP_INDEX: // OP_INDEX of (OP_NEW | OP_SUBRANGE)
                return apply_OP_INDEX(env, candidates);
              case OperatorExpression::OP_SUBRANGE: // OP_SUBRANGE of (OP_SUBRANGE)
                return apply_OP_SUBRANGE(env, candidates);
              default:
                return nullptr;
              };
            }
          };

          return ::std::make_shared<Impl>();
        }

        Peephole::Ptr Arrays::getSubrangePeephole()
        {
          struct Impl: public Peephole
          {
            ~Impl()
            {
            }

            ENode apply(const ENode &node, const Environment &env) const
            {
              auto range = matches(node, env, OperatorExpression::OP_SUBRANGE,
                  OperatorExpression::OP_NEW);
              if (range.second && range.second->type->self<ArrayType>()) {
                auto start = env.find<LiteralInteger>(range.first->arguments.at(1));
                auto stop = env.find<LiteralInteger>(range.first->arguments.at(2));
                if (start && stop) {
                  auto i = start->value.unsignedValue();
                  auto j = stop->value.unsignedValue();
                  if (i <= j && j <= range.second->arguments.size()) {
                    OperatorExpression::Arguments args;
                    auto res = OperatorExpression::create(range.first->type,
                        OperatorExpression::OP_NEW, args);
                  }
                }
              }

              return nullptr;
            }
          };

          return ::std::make_shared<Impl>();
        }

        Peephole::Ptr Arrays::getReversePeephole()
        {
          struct Impl: public Peephole
          {
            ~Impl()
            {
            }

            ENode apply(const ENode &node, const Environment &env) const
            {
              auto [reverse, that] = matches(node, env, { OperatorExpression::OP_REVERSE }, {
                  OperatorExpression::OP_CHECKED_CAST, OperatorExpression::OP_UNCHECKED_CAST,
                  OperatorExpression::OP_REVERSE, OperatorExpression::OP_MAP_ARRAY,
                  OperatorExpression::OP_FILTER_ARRAY });
              if (!that || !reverse) {
                return nullptr;
              }

              auto arg0 = that->arguments.at(0);
              auto arg1 = that->arguments.size() >= 2 ? that->arguments.at(1) : nullptr;
              auto arg2 = that->arguments.size() >= 3 ? that->arguments.at(2) : nullptr;

              // FIXME: we should only do this, if there is a single use of the
              // arg0

              EExpression result;
              switch (that->name) {
              case OperatorExpression::OP_REVERSE:
                result = arg0;
                break;
              case OperatorExpression::OP_CHECKED_CAST:
              case OperatorExpression::OP_UNCHECKED_CAST: {
                assert(that->type->self<ArrayType>() && "expected an array cast");
                result = LetExpression::create(
                    OperatorExpression::create(OperatorExpression::OP_REVERSE, arg0),
                    [&](EVariable reversedArr) {
                      return OperatorExpression::create(reverse->type,that->name,reversedArr);
                    });
                break;
                break;
              }
              case OperatorExpression::OP_MAP_ARRAY:
              case OperatorExpression::OP_FILTER_ARRAY:
              case OperatorExpression::OP_PARTITION:
                result = LetExpression::create(
                    OperatorExpression::create(OperatorExpression::OP_REVERSE, arg0),
                    [&](EVariable reversedArr) {
                      return OperatorExpression::create(reverse->type,that->name,reversedArr,arg1);
                    });
                break;
              case OperatorExpression::OP_FLATTEN: {
                // reverse(flatten(x)) --> flatten(map(reverse(x),reverse))
                auto arrTy = arg0->type->self<ArrayType>();
                auto mapReverse =
                    LambdaExpression::create( { arrTy->element },
                        [&](LambdaExpression::Arguments xargs,
                            StatementBuilder &sb) {
                              auto tmp = sb.declareLocalValue( OperatorExpression::OP_REVERSE, {xargs.at(0)});
                              sb.appendReturn(tmp);
                              return tmp->type;
                            });

                result =
                    LetExpression::create(
                        { OperatorExpression::create(OperatorExpression::OP_REVERSE, arg0), },
                        [&](
                            ::std::vector<EVariable> xargs) {
                              return LetExpression::create(OperatorExpression::create(OperatorExpression::OP_MAP_ARRAY,xargs.at(0),xargs.at(1)),
                                  [&](EVariable mappedArray) {
                                    return OperatorExpression::create(reverse->type,that->name,mappedArray);
                                  });
                            });
                break;
              }
              default:
                break;
              };

              if (result) {
                assert(
                    result->type->isSameType(*reverse->type)
                        && "Peephole prduced different result type");
              }

              return result;
            }
          };

          return ::std::make_shared<Impl>();
        }

      }
    }
  }
}
