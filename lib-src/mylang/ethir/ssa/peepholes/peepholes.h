#ifndef FILE_MYLANG_ETHIR_SSA_PEEPHOLES_H
#define FILE_MYLANG_ETHIR_SSA_PEEPHOLES_H

#include <mylang/ethir/ssa/peepholes/Arrays.h>
#include <mylang/ethir/ssa/peepholes/BitLikeOps.h>
#include <mylang/ethir/ssa/peepholes/Optionals.h>
#include <mylang/ethir/ssa/peepholes/Structs.h>
#include <mylang/ethir/ssa/peepholes/Tuples.h>
#include <mylang/ethir/ssa/peepholes/Unions.h>

#endif
