#ifndef CLASS_MYLANG_ETHIR_SSA_PEEPHOLES_OPTIONALS_H
#define CLASS_MYLANG_ETHIR_SSA_PEEPHOLES_OPTIONALS_H

#ifndef CLASS_MYLANG_ETHIR_SSA_PEEPHOLE_H
#include <mylang/ethir/ssa/Peephole.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ssa {
      namespace peepholes {

        class Optionals
        {
        private:
          ~Optionals() = delete;

          /**
           * Get a peep hole that optimizes IS_PRESENT and GET functions.
           */
        public:
          static Peephole::Ptr getPeephole();
        };

      }
    }
  }
}
#endif
