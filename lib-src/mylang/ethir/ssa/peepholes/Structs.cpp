#include <mylang/ethir/ssa/peepholes/Structs.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/types/types.h>

namespace mylang {
  namespace ethir {
    using namespace ir;
    using namespace types;

    namespace ssa {
      namespace peepholes {

        Peephole::Ptr Structs::getPeephole()
        {
          struct Impl: public Peephole
          {
            ~Impl()
            {
            }

            ENode apply(const ENode &node, const Environment &env) const
            {
              auto get_member = node->self<GetMember>();
              if (!get_member) {
                return nullptr;
              }
              {
                auto ctor = env.find<OperatorExpression>(get_member->object);
                if (ctor && ctor->name == OperatorExpression::OP_NEW) {
                  auto structTy = ctor->type->self<StructType>();
                  if (structTy) {
                    auto i = structTy->indexOfMember(get_member->name);
                    return ctor->arguments.at(i);
                  }
                  return nullptr;
                }
              }

              return nullptr;
            }

          };

          return ::std::make_shared<Impl>();
        }

      }
    }
  }
}
