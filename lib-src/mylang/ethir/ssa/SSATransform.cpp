#include <mylang/ethir/ssa/SSATransform.h>
#include <mylang/ethir/ssa/MaintainSSAPass.h>

namespace mylang {
  namespace ethir {
    namespace ssa {

      SSATransform::SSATransform()
      {
      }

      SSATransform::~SSATransform()
      {
      }

      ENode SSATransform::visit(const ir::ToArray::ToArrayPtr &node)
      {
        unexpectNode(node);
        return nullptr;
      }
      ENode SSATransform::visit(const ir::Loop::LoopPtr &node)
      {
        unexpectNode(node);
        return nullptr;
      }
      ENode SSATransform::visit(const ir::IterateArrayStep::IterateArrayStepPtr &node)
      {
        unexpectNode(node);
        return nullptr;
      }
      ENode SSATransform::visit(const ir::SequenceStep::SequenceStepPtr &node)
      {
        unexpectNode(node);
        return nullptr;
      }
      ENode SSATransform::visit(const ir::SingleValueStep::SingleValueStepPtr &node)
      {
        unexpectNode(node);
        return nullptr;
      }
      ENode SSATransform::visit(const ir::YieldStatement::YieldStatementPtr &node)
      {
        unexpectNode(node);
        return nullptr;
      }
      ENode SSATransform::visit(const ir::SkipStatement::SkipStatementPtr &node)
      {
        unexpectNode(node);
        return nullptr;
      }
      ENode SSATransform::visit(const ir::VariableUpdate::VariableUpdatePtr &node)
      {
        unexpectNode(node);
        return nullptr;
      }
      ENode SSATransform::visit(const ir::VariableDecl::VariableDeclPtr &node)
      {
        unexpectNode(node);
        return nullptr;
      }

      void SSATransform::unexpectNode(const ENode&)
      {
        throw ::std::runtime_error("Unexpected node during SSA transformation");
      }
    }
  }
}
