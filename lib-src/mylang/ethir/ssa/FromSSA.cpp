#include <mylang/ethir/ssa/FromSSA.h>
#include <mylang/ethir/ssa/SSATransform.h>
#include <mylang/ethir/ir/StatementBuilder.h>
#include <mylang/ethir/queries/FindVariableScope.h>
#include <mylang/ethir/prettyPrint.h>
#include <cassert>
#include <map>

namespace mylang {
  namespace ethir {
    using namespace ir;
    using namespace types;
    namespace ssa {
      namespace {
        struct VInfo
        {
          VInfo()
              : nWrites(0), nReads(0)
          {
          }

          /* number of writes to the variable */
          size_t nWrites;
          /* number of reads from the variable */
          size_t nReads;
        };
        struct Context
        {
          typedef ::std::map<mylang::names::Name::Ptr, VInfo> Variables;
          Context(const Context&) = delete;
          Context& operator=(const Context&) = delete;

          Context(Context *p)
              : parent(p), isBlockContext(false)
          {
          }
          ~Context()
          {
          }

          bool isUndeclared(mylang::names::Name::Ptr name)
          {
            Context *THIS = this;
            while (THIS) {
              // this means we already found a use of an indeclared variable
              auto i = THIS->undeclared.find(name);
              if (i != THIS->undeclared.end()) {
                return false;
              }
              THIS = THIS->parent;
            }
            return true;
          }

          Context* find(mylang::names::Name::Ptr name)
          {
            Context *THIS = this;
            while (THIS) {
              auto i = THIS->info.find(name);
              if (i != THIS->info.end()) {
                return THIS;
              }
              THIS = THIS->parent;
            }
            return nullptr;
          }

          /** Add a read use */
          void addReadUse(const ir::Variable::VariablePtr &node)
          {
            auto name = node->name.source();
            if (isUndeclared(name)) {
              markUndeclared(node);
            }
            ++info[name].nReads;
          }

          /** Add a read use */
          void addWriteUse(const ir::Variable::VariablePtr &node)
          {
            auto name = node->name.source();
            if (isUndeclared(name)) {
              markUndeclared(node);
            }
            ++info[name].nWrites;
          }

          void markDeclared(const ir::Variable::VariablePtr &node)
          {
            auto name = node->name.source();
            undeclared.erase(name);
            info.erase(name);
          }

          void markUndeclared(const ir::Variable::VariablePtr &node)
          {
            auto name = node->name.source();
            undeclared[name] = node;
          }

          void updateInfo(mylang::names::Name::Ptr name, const VInfo &vinfo)
          {
            auto &xinfo = info[name];
            xinfo.nReads += vinfo.nReads;
            xinfo.nWrites += vinfo.nWrites;
          }

          /** Used variables */
          Variables info;

          /** The undeclared variables */
          ::std::map<mylang::names::Name::Ptr, ir::Variable::VariablePtr> undeclared;

          Context *parent;

          /** True if this this context is a block */
          bool isBlockContext;
        };

        struct Converter: public SSATransform
        {
          Converter()
              : context(nullptr)
          {
          }

          ~Converter()
          {
          }

          ENode visit(const ir::OpaqueExpression::OpaqueExpressionPtr &node) override
          {
            OpaqueExpression::Variables vars(node->arguments);
            for (auto &v : vars) {
              v = transformExpression(v).actual;
            }
            return node->changeArguments(vars)->restore();
          }

          ENode visit(const ir::Program::ProgramPtr &node) override
          {
            ENode res;
            Context ctx(context);
            context = &ctx;
            res = Transform::visit(node);
            context = ctx.parent;
            return res;
          }

          ENode visit(const ir::LambdaExpression::LambdaExpressionPtr &node) override
          {
            Context paramContext(context);
            context = &paramContext;
            auto params = transformParameters(node->parameters);
            context = paramContext.parent;

            Context ctx(&paramContext);
            auto body = transformWithContext(ctx, node->body);
            body = propagateVariables(ctx, body);
            if (params.isNew || body.isNew) {
              auto ty = node->type->self<types::FunctionType>();
              return ir::LambdaExpression::create(node->tag, ty, params.actual, body.actual);
            }
            return nullptr;
          }

          ENode visit(const ir::StatementBlock::StatementBlockPtr &node)
          {
            // if the parent context is a block, then we just flatten the block
            if (context->isBlockContext) {
              ENode res = Transform::visit(node);
              if (res == nullptr) {
                return node->statements;
              }
              StatementBlock::StatementBlockPtr block = res->self<StatementBlock>();
              if (block) {
                return block->statements;
              }
              return res;
            }

            bool resetScopes = false;
            if (scopes == nullptr) {
              scopes = queries::FindVariableScope::find(node);
              resetScopes = true;
            }

            // need to create a new context just for this block
            Context ctx(context);
            ctx.isBlockContext = true;
            auto body = transformWithContext(ctx, node->statements);
            body = createBlockVariables(ctx, node, body);
            propagateVariables(ctx, *context);

            // this uses the context that was active on entry to this function
            auto next = transformStatement(node->next);

            if (resetScopes) {
              scopes = nullptr;
            }

            if (body.isNew || next.isNew) {
              return ir::StatementBlock::create(body.actual, next.actual);
            }
            return nullptr;
          }

          ENode visit(const ir::ProcessDecl::ProcessDeclPtr &node)
          {
            Context ctx(context);
            context = &ctx;
            auto res = Transform::visit(node);
            context = ctx.parent;
            return res;
          }

          ENode visit(const ir::ProcessConstructor::ProcessConstructorPtr &node)
          {
            Context *restore = context;
            Context paramContext(nullptr);
            context = &paramContext;
            auto params = transformParameters(node->parameters);
            context = restore;

            Context ctx(&paramContext);
            auto body = transformWithContext(ctx, node->body);
            auto ctor = transformWithContext(ctx, node->callConstructor);
            body = propagateVariables(ctx, body);

            if (body.isNew || ctor.isNew) {
              auto ownCtorCall =
                  ctor.actual ? ctor.actual->self<ir::OwnConstructorCall>() : nullptr;
              return ir::ProcessConstructor::create(node->parameters, ownCtorCall, body.actual);
            }
            return nullptr;
          }

          ENode visit(const ir::ProcessBlock::ProcessBlockPtr &node)
          {
            Context ctx(nullptr);
            auto body = transformWithContext(ctx, node->body);
            body = propagateVariables(ctx, body);

            if (body.isNew) {
              return ir::ProcessBlock::create(node->name, body.actual);
            }
            return nullptr;
          }

          ENode visit(const ir::Variable::VariablePtr &node) override
          {
            context->addReadUse(node);
            if (node->name.version() != 0) {
              // drop the version
              return ir::Variable::create(node->scope, node->type, node->name.resetVersion());
            }

            return nullptr;
          }

          ENode visit(const ir::Phi::PhiPtr &node) override
          {
            // only need to visit the first variable
            for (auto v : node->arguments) {
              if (v->name.version() == 0) {
                return v;
              }
            }
            auto v = node->arguments.at(0);
            return ir::Variable::create(v->scope, v->type, v->name.resetVersion());
          }

#if 0
          ENode visit(const ir::VariableDecl::VariableDeclPtr &node) override
          {
            auto res = Transform::visit(node);
            // if we have a variable decl, then we know that we're not in SSA
            context->markDeclared(node->variable);
            return res;
          }
#endif

          ENode visit(const ir::ValueDecl::ValueDeclPtr &node) override
          {
            // define the value before the right hand side, because there may recursion
            // and the name needs to be available
            context->markUndeclared(node->variable);
            auto rhs = transformExpr(node->value);
            auto lhs = transformExpression(node->variable);

            auto name = lhs.actual->name;
            if (name == node->name) {
              // we have an original value
              context->markUndeclared(node->variable);
            }

            auto next = transformStatement(node->next);
            VInfo vinfo = context->info[name.source()];

            // we have an original variable decl if the names haven't changed
            if (name == node->name) {
              context->markDeclared(node->variable);

              // since we count the decl as a write, we need to check for 1 additional write
              // if there is no rhs, then we've already processed this node in a previous iteration
              if (vinfo.nWrites > 0 && rhs.actual) {
                auto rhsVar = rhs.actual->self<Variable>();
                if (rhsVar) {
                  return ir::VariableDecl::create(lhs.actual, rhsVar, next.actual);
                } else {
                  auto value = ir::Variable::create(lhs.actual->scope, lhs.actual->type,
                      Name::create("from_ssa"));
                  auto vardecl = ir::VariableDecl::create(lhs.actual, value, next.actual);
                  return ir::ValueDecl::createSimplified(value, rhs.actual, vardecl);
                }
              } else if (lhs.isNew || rhs.isNew || next.isNew) {
                return ir::ValueDecl::createSimplified(lhs.actual, rhs.actual, next.actual);
              } else {
                return nullptr;
              }
            }

            auto rhsVar = rhs.actual->self<Variable>();
            if (rhsVar) {
              if (rhsVar->name == lhs.actual->name) {
                // val x = x;
                return nop(next.actual);
              } else {
                context->addWriteUse(lhs.actual);
                return ir::VariableUpdate::create(lhs.actual, rhsVar, next.actual);
              }
            } else {
              auto value = ir::Variable::create(lhs.actual->scope, lhs.actual->type,
                  Name::create("from_ssa"));
              context->addWriteUse(lhs.actual);
              auto update = ir::VariableUpdate::create(lhs.actual, value, next.actual);
              return ir::ValueDecl::createSimplified(value, rhs.actual, update);
            }
          }

          ENode visit(const ir::IfStatement::IfStatementPtr &node) override
          {
            auto cond = transformExpression(node->condition);

            Context iftrueCtx(context);
            auto iftrue = transformWithContext(iftrueCtx, node->iftrue);

            Context iffalseCtx(context);
            auto iffalse = transformWithContext(iffalseCtx, node->iffalse);
            propagateVariables(iftrueCtx, iffalseCtx, *context);

            Context nextCtx(context);
            auto next = transformWithContext(nextCtx, node->next);
            propagateVariables(nextCtx, *context);

            if (cond.isNew || iftrue.isNew || iffalse.isNew || next.isNew) {
              return ir::IfStatement::create(cond.actual, iftrue.actual, iffalse.actual,
                  next.actual);
            }
            return nullptr;
          }

          ENode visit(const ir::TryCatch::TryCatchPtr &node)
          {

            Context tryCtx(context);
            auto tryBlock = transformWithContext(tryCtx, node->tryBody);
            propagateVariables(tryCtx, *context);

            Context catchCtx(context);
            auto catchBlock = transformWithContext(catchCtx, node->catchBody);
            propagateVariables(catchCtx, *context);

            Context nextCtx(context);
            auto next = transformWithContext(nextCtx, node->next);
            propagateVariables(nextCtx, *context);

            if (tryBlock.isNew || catchBlock.isNew || next.isNew) {
              return ir::TryCatch::create(tryBlock.actual, catchBlock.actual, next.actual);
            }
            return nullptr;
          }

          ENode visit(const ir::CommentStatement::CommentStatementPtr &node) override
          {
            auto next = transformStatement(node->next);
            if (node->comment.find("ToSSA:") == 0) {
              return nop(next.actual);
            } else if (next.isNew) {
              return ir::CommentStatement::create(node->comment, next.actual);
            } else {
              return nullptr;
            }
          }

          ENode visit(const ir::ForeachStatement::ForeachStatementPtr &node)
          {
            auto data = transformExpression(node->data);

            Context varCtx(context);
            auto v = transformWithContext(varCtx, node->variable);

            Context bodyCtx(&varCtx);
            auto body = transformWithContext(bodyCtx, node->body);
            propagateLoopVariables(bodyCtx, *context);

            Context nextCtx(context);
            auto next = transformWithContext(nextCtx, node->next);
            propagateVariables(nextCtx, *context);

            if (v.isNew || data.isNew || body.isNew || next.isNew) {
              return ir::ForeachStatement::create(node->name, v.actual, data.actual, body.actual,
                  next.actual);
            }
            return nullptr;
          }

          ENode visit(const ir::LoopStatement::LoopStatementPtr &node)
          {
            Context bodyCtx(context);
            auto body = transformWithContext(bodyCtx, node->body);
            propagateLoopVariables(bodyCtx, *context);

            Context nextCtx(context);
            auto next = transformWithContext(nextCtx, node->next);
            propagateVariables(nextCtx, *context);

            if (body.isNew || next.isNew) {
              return ir::LoopStatement::create(node->name, body.actual, next.actual);
            }
            return nullptr;
          }

          Tx<EStatement> transformWithContext(Context &ctx, EStatement nextNode)
          {
            Context *restore = context;
            context = &ctx;
            Tx<EStatement> res = transformStatement(nextNode);
            context = restore;
            return res;
          }

          Tx<EVariable> transformWithContext(Context &ctx, EVariable nextNode)
          {
            Context *restore = context;
            context = &ctx;
            Tx<EVariable> res = transformExpression(nextNode);
            context = restore;
            return res;
          }

          /**
           * The propagate the undeclared variables in the specified context to its parent
           * If there is no parent, then we need to declare variables
           */
          static void propagateVariables(Context &ctx, Context &target)
          {
            // we can propagate up, so no other action to take
            target.undeclared.insert(ctx.undeclared.begin(), ctx.undeclared.end());

            for (auto info : ctx.info) {
              target.updateInfo(info.first, info.second);
            }
          }

          /**
           * The propagate the undeclared variables in the specified context to its parent
           * If there is no parent, then we need to declare variables
           */
          Tx<EStatement> createBlockVariables(Context &ctx, StatementBlock::StatementBlockPtr block,
              Tx<EStatement> body)
          {
            Tx<EStatement> res = body;
            for (auto iter = ctx.undeclared.begin(); iter != ctx.undeclared.end();) {
              auto &undeclared = *iter;
              if (scopes) {
                auto i = scopes->find(undeclared.first);
                if (i != scopes->end() && i->second == block) {
                  // we need to define the variable here
                  auto vinfo = ctx.info[undeclared.first];
                  res = createFunctionVariableDecl(undeclared.second, vinfo, res);
                  ctx.undeclared.erase(iter++);
                } else {
                  ++iter;
                }
              } else {
                ++iter;
              }
            }

            return res;
          }

          /**
           * The propagate the undeclared variables in the specified context to its parent
           * If there is no parent, then we need to declare variables
           */
          static void propagateLoopVariables(Context &ctx, Context &target)
          {
            // we can propagate up, so no other action to take
            target.undeclared.insert(ctx.undeclared.begin(), ctx.undeclared.end());

            for (auto info : ctx.info) {
              if (info.second.nWrites > 0) {
                info.second.nWrites += 1;
              }
              target.updateInfo(info.first, info.second);
            }
          }

          /**
           * The propagate the undeclared variables in the specified context to its parent
           * If there is no parent, then we need to declare variables
           */
          static Tx<EStatement> propagateVariables(Context &ctx, Tx<EStatement> next)
          {
            Tx<EStatement> res = next;
            for (auto undeclared : ctx.undeclared) {
              auto vinfo = ctx.info[undeclared.first];
              res = createFunctionVariableDecl(undeclared.second, vinfo, res);
            }

            return res;
          }

          /**
           * Create a variable decl according to a vinfo.
           * @param var the variable
           * @param info a vinfo
           * @param next a next statement
           */
          static Tx<EStatement> createFunctionVariableDecl(Variable::VariablePtr var,
              const VInfo &info, Tx<EStatement> next)
          {
            // do not introduce a variable for one that is not a function variable
            if (var->scope != ir::Declaration::Scope::FUNCTION_SCOPE) {
              return next;
            }

            EStatement res;
            auto name = var->name.resetVersion();
            auto new_var = ir::Variable::create(var->scope, var->type, name);
            // we need more than 1 write, because the declaration doesn't count
            if (info.nWrites > 1) {
              res = ir::VariableDecl::create(new_var, next.actual);
            } else {
              res = ir::ValueDecl::create(new_var, next.actual);
            }
            return Tx<EStatement>(res, true);
          }

          /**
           * The propagate the undeclared variables in the specified context to its parent
           * If there is no parent, then we need to declare variables
           */
          static void propagateVariables(Context &ctx1, Context &ctx2, Context &target)
          {
            // we can propagate up, so no other action to take
            target.undeclared.insert(ctx1.undeclared.begin(), ctx1.undeclared.end());
            target.undeclared.insert(ctx2.undeclared.begin(), ctx2.undeclared.end());

            for (auto i = ctx1.info.begin(); i != ctx1.info.end(); ++i) {
              auto j = ctx2.info.find(i->first);
              if (j != ctx2.info.end()) {
                VInfo combined_info;
                combined_info.nReads = ::std::max(i->second.nReads, j->second.nReads);
                combined_info.nWrites = ::std::max(i->second.nWrites, j->second.nWrites);
                target.updateInfo(i->first, combined_info);
              } else {
                target.updateInfo(i->first, i->second);
              }
            }

            for (auto i = ctx2.info.begin(); i != ctx2.info.end(); ++i) {
              auto j = ctx1.info.find(i->first);
              if (j == ctx1.info.end()) {
                target.updateInfo(i->first, i->second);
              }
            }
          }

          Context *context;

          ::std::unique_ptr<queries::FindVariableScope::DeclarationScopes> scopes;
        }
        ;
      }

      mylang::ethir::ir::Program::ProgramPtr FromSSA::transform(
          mylang::ethir::ir::Program::ProgramPtr node)
      {
        Converter tx;
        auto res = tx.visitNode(node);
        if (res == nullptr) {
          res = node;
        }
        return res->self<Program>();
      }

      mylang::ethir::ir::LambdaExpression::LambdaExpressionPtr FromSSA::transform(
          mylang::ethir::ir::LambdaExpression::LambdaExpressionPtr node)
      {
        ;
        Converter tx;
        auto res = tx.visitNode(node);
        if (res == nullptr) {
          res = node;
        }
        return res->self<LambdaExpression>();
      }

    }
  }
}
