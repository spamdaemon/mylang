//#include <mylang/BigInt.h>
//#include <mylang/BigReal.h>
//#include <mylang/BigUInt.h>
#include <mylang/ethir/ethir.h>
//#include <mylang/ethir/Environment.h>
#include <mylang/ethir/queries/FindTypeShadows.h>
#include <mylang/ethir/queries/FindCallSites.h>
#include <mylang/ethir/transforms/ChangeSignature.h>
//#include <mylang/ethir/ir/AbstractOperatorVisitor.h>
//#include <mylang/ethir/ir/Declaration.h>
#include <mylang/ethir/ir/GlobalValue.h>
#include <mylang/ethir/ir/LambdaExpression.h>
#include <mylang/ethir/ir/OperatorExpression.h>
//#include <mylang/ethir/ir/ProcessValue.h>
//#include <mylang/ethir/ir/ValueDecl.h>
#include <mylang/ethir/ssa/ShadowTypingPass.h>
//#include <mylang/ethir/ssa/SSATransform.h>
#include <stddef.h>
#include <cassert>
//#include <cstdio>
//#include <iterator>
//#include <map>
#include <memory>
//#include <optional>
//#include <stdexcept>
//#include <string>
#include <vector>
#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/Program.h>
#include <mylang/ethir/types/FunctionType.h>
#include <mylang/ethir/types/Type.h>

namespace mylang {
  namespace ethir {
    using namespace ir;
    namespace ssa {

      ShadowTypingPass::ShadowTypingPass()
      {
      }

      ShadowTypingPass::~ShadowTypingPass()
      {
      }

      EProgram ShadowTypingPass::transform(EProgram src)
      {
        auto prog = src->self<ir::Program>();
        if (!prog) {
          return nullptr;
        }
        auto sites = mylang::ethir::queries::FindCallSites::find(prog);
        auto shadows = mylang::ethir::queries::FindTypeShadows::find(prog);

        transforms::ChangeSignature::Changes changeSignatures;
        // for each call site, find the shadow types
        for (auto i = sites.begin(); i != sites.end(); ++i) {
          auto lambda = i->second.decl->value->self<LambdaExpression>();
          assert(lambda != nullptr && "the target of a callsite must always be a lambda");

          // if the decl is an exported function, then we cannot change anything about it
          if (auto decl = i->second.decl->self<GlobalValue>(); decl) {
            if (decl->kind == GlobalValue::EXPORTED) {
              // we cannot change exported functions
              continue;
            }
          }

          for (auto j = i->second.sites.begin(); j != i->second.sites.end(); ++j) {
            auto op = (*j)->self<ir::OperatorExpression>();
            if (op && op->name == ir::OperatorExpression::OP_CALL) {
              types::FunctionType::Parameters params;
              size_t nShadowArgs = 0;
              for (size_t k = 1; k < op->arguments.size(); ++k) {
                auto arg = op->arguments.at(k);
                types::Type::Ptr argTy = arg->type;

                // we're not allowed to change named types are are exported
                auto shadow = shadows.find(arg->name);
                if (shadow != shadows.end()) {
                  // one condition for the rewriting of functions is that the
                  // arguments can be casted; e.g. unions cannot be casted
                  if (shadow->second.intersection->canCastBetween(*argTy)) {
                    ++nShadowArgs;
                    argTy = shadow->second.intersection;
                  }
                }

                params.push_back(argTy);
              }

              // if we have any shadow args, then we want to change the function signature
              if (nShadowArgs > 0) {
                auto sig = types::FunctionType::get(op->type, params);
                auto &changes = changeSignatures[i->first];
                size_t k = 0;
                for (k = 0; k < changes.size(); ++k) {
                  if (changes[k].signature->isSameType(*sig)) {
                    changes[k].callsites.insert(op);
                    break;
                  }
                }
                if (k == changes.size()) {
                  transforms::ChangeSignature::Change chg(lambda->tag, sig);
                  chg.callsites.insert(op);
                  changes.push_back(chg);

#if 0
                    ::std::cerr << "Found a shadow " << ::std::endl << i->first << " : "
                        << op->arguments.at(0)->type->toString() << ::std::endl << " ---> "
                        << sig->toString() << ::std::endl;
#endif
                }

              }
            }
          }
        }
        if (changeSignatures.empty()) {
          return nullptr;
        }
        src = transforms::ChangeSignature::transform(prog, changeSignatures);
        return src;
      }

      bool ShadowTypingPass::requiresMaintainSSAPass() const
      {
        return true;
      }
    }
  }
}

