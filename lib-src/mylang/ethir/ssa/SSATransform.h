#ifndef CLASS_MYLANG_ETHIR_SSA_SSATRANSFORM_H
#define CLASS_MYLANG_ETHIR_SSA_SSATRANSFORM_H

#ifndef CLASS_MYLANG_ETHIR_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TRANSFORM_H
#include <mylang/ethir/Transform.h>
#endif

#include <memory>

namespace mylang {
  namespace ethir {
    namespace ssa {

      /**
       * A variable declaration. Variables are mutable!
       */
      class SSATransform: public Transform
      {
        /**
         * Create a program
         * @param functions the global functions
         * @param values the global values
         * @param processes the defined processes
         */
      protected:
        SSATransform();

        /** Destructor */
      public:
        virtual ~SSATransform() = 0;

        /** These not expected to be encountered in a SSA transform */
        ENode visit(const ir::ToArray::ToArrayPtr &node) override final;
        ENode visit(const ir::Loop::LoopPtr &node) override final;
        ENode visit(const ir::YieldStatement::YieldStatementPtr &node) override final;
        ENode visit(const ir::SkipStatement::SkipStatementPtr &node) override final;
        ENode visit(const ir::IterateArrayStep::IterateArrayStepPtr &node) override final;
        ENode visit(const ir::SequenceStep::SequenceStepPtr &node) override final;
        ENode visit(const ir::SingleValueStep::SingleValueStepPtr &node) override final;
        ENode visit(const ir::VariableUpdate::VariableUpdatePtr &node) override final;
        ENode visit(const ir::VariableDecl::VariableDeclPtr &node) override final;

      protected:
        virtual void unexpectNode(const ENode &node);
      };
    }

  }
}
#endif
