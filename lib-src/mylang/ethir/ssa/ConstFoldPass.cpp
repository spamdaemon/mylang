#include <bits/stdint-intn.h>
#include <bits/stdint-uintn.h>
#include <mylang/BigInt.h>
#include <mylang/BigReal.h>
#include <mylang/BigUInt.h>
#include <mylang/ethir/ethir.h>
#include <mylang/ethir/prettyPrint.h>
#include <mylang/ethir/Environment.h>
#include <mylang/ethir/ir/AbstractOperatorVisitor.h>
#include <mylang/ethir/ir/Declaration.h>
#include <mylang/ethir/ir/ForeachStatement.h>
#include <mylang/ethir/ir/GetDiscriminant.h>
#include <mylang/ethir/ir/GetMember.h>
#include <mylang/ethir/ir/GlobalValue.h>
#include <mylang/ethir/ir/IfStatement.h>
#include <mylang/ethir/ir/LambdaExpression.h>
#include <mylang/ethir/ir/Literal.h>
#include <mylang/ethir/ir/LiteralArray.h>
#include <mylang/ethir/ir/LiteralBit.h>
#include <mylang/ethir/ir/LiteralBoolean.h>
#include <mylang/ethir/ir/LiteralByte.h>
#include <mylang/ethir/ir/LiteralChar.h>
#include <mylang/ethir/ir/LiteralInteger.h>
#include <mylang/ethir/ir/LiteralNamedType.h>
#include <mylang/ethir/ir/LiteralOptional.h>
#include <mylang/ethir/ir/LiteralReal.h>
#include <mylang/ethir/ir/LiteralString.h>
#include <mylang/ethir/ir/LiteralStruct.h>
#include <mylang/ethir/ir/LiteralTuple.h>
#include <mylang/ethir/ir/LiteralUnion.h>
#include <mylang/ethir/ir/NewUnion.h>
#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/OperatorExpression.h>
#include <mylang/ethir/ir/ProcessDecl.h>
#include <mylang/ethir/ir/ProcessValue.h>
#include <mylang/ethir/ir/Program.h>
#include <mylang/ethir/ir/StatementBlock.h>
#include <mylang/ethir/ir/ValueDecl.h>
#include <mylang/ethir/ir/Variable.h>
#include <mylang/ethir/Name.h>
#include <mylang/ethir/ssa/ConstFoldPass.h>
#include <mylang/ethir/types/ArrayType.h>
#include <mylang/ethir/types/BitType.h>
#include <mylang/ethir/types/BooleanType.h>
#include <mylang/ethir/types/ByteType.h>
#include <mylang/ethir/types/CharType.h>
#include <mylang/ethir/types/IntegerType.h>
#include <mylang/ethir/types/NamedType.h>
#include <mylang/ethir/types/OptType.h>
#include <mylang/ethir/types/RealType.h>
#include <mylang/ethir/types/StringType.h>
#include <mylang/ethir/types/StructType.h>
#include <mylang/ethir/types/TupleType.h>
#include <mylang/ethir/types/Type.h>
#include <mylang/ethir/types/UnionType.h>
#include <mylang/ethir/ssa/SSATransform.h>
#include <mylang/ethir/TypeName.h>
#include <mylang/ethir/Visitor.h>
#include <stddef.h>
#include <cassert>
#include <cstdio>
#include <iterator>
#include <map>
#include <memory>
#include <optional>
#include <stdexcept>
#include <string>
#include <vector>

namespace mylang {
  namespace ethir {
    using namespace ir;
    namespace ssa {
      namespace {

        struct ConstFoldOperatorExpression: public ir::AbstractOperatorVisitor
        {
          ConstFoldOperatorExpression(const ConstFoldPass::LoggingConfiguration &xlogging,
              const ::std::vector<EExpression> &args)
              : arguments(args), logging(xlogging)
          {
          }

          ~ConstFoldOperatorExpression()
          {
          }

          void visit_FROM_BITS(OperatorExpression::OperatorExpressionPtr e) override
          {
            // TODO: implement
          }
          void visit_TO_BITS(OperatorExpression::OperatorExpressionPtr e) override
          {
            // TODO: implement
          }
          void visit_FROM_BYTES(OperatorExpression::OperatorExpressionPtr e) override
          {
            // TODO: implement
          }
          void visit_TO_BYTES(OperatorExpression::OperatorExpressionPtr e) override
          {
            // TODO: implement
          }
          void visit_CLAMP(OperatorExpression::OperatorExpressionPtr e) override
          {
            if (e->type->isSameType(*arguments.at(0)->type)) {
              // no clamping necessary
              result = e->arguments.at(0);
              return;
            }
            auto lit = arguments.at(0)->self<LiteralInteger>();
            if (lit) {
              auto ty = e->type->self<types::IntegerType>();
              if (lit->value > ty->range.max()) {
                result = LiteralInteger::create(ty, *ty->range.max());
              } else if (lit->value < ty->range.min()) {
                result = LiteralInteger::create(ty, *ty->range.min());
              } else {
                result = LiteralInteger::create(ty, lit->value);
              }
              return;
            }
          }
          void visit_WRAP(OperatorExpression::OperatorExpressionPtr e) override
          {
            if (e->type->isSameType(*arguments.at(0)->type)) {
              // no wrapping necessary
              result = e->arguments.at(0);
              return;
            }

            auto lit = arguments.at(0)->self<LiteralInteger>();
            if (lit) {
              auto ty = e->type->self<types::IntegerType>();
              auto range = ty->range.size();
              if (lit->value > ty->range.max()) {
                // wrap 10 into 2-8 == 4
                // (10-2)%(9-2)+2 = 8 % 7+2 = 1+2 = 3
                // wrap 9 into 2-8
                // (9-2) % (9-2)+2 = 7%7 + 2 = 2
                auto diff = *(lit->value - ty->range.min());
                auto wrapped = *(ty->range.min() + *(diff % range));
                result = LiteralInteger::create(ty, *wrapped);
              } else if (lit->value < ty->range.min()) {
                // wrap -12 into 2-8 == 2
                // 8 - ( (8+12) % (9-2)) = 8 - 20 % 7 = 8-6 = 2
                // wrap 1 into 2-8
                // 8 - ((8-1) % (9-2)) = 8 - 7%7 = 8
                auto diff = *(ty->range.max() - lit->value);
                auto wrapped = *(ty->range.max() - *(diff % range));
                result = LiteralInteger::create(ty, *wrapped);
              } else {
                result = LiteralInteger::create(ty, lit->value);
              }
              return;
            }
          }
          void visit_BYTE_TO_UINT8(OperatorExpression::OperatorExpressionPtr e) override
          {
            auto lit = arguments.at(0)->self<LiteralByte>();
            if (lit) {
              auto ity = e->type->self<types::IntegerType>();
              result = LiteralInteger::create(ity, (::std::uint8_t) (lit->value));
            }
          }

          void visit_BYTE_TO_SINT8(OperatorExpression::OperatorExpressionPtr e) override
          {
            auto lit = arguments.at(0)->self<LiteralByte>();
            if (lit) {
              auto ity = e->type->self<types::IntegerType>();
              result = LiteralInteger::create(ity, (::std::int8_t) (lit->value));
            }
          }

          void visit_STRING_TO_CHARS(OperatorExpression::OperatorExpressionPtr e) override
          {
            auto lit = arguments.at(0)->self<LiteralString>();
            if (lit) {
              auto arrTy = e->type->self<types::ArrayType>();
              auto chTy = arrTy->element->self<types::CharType>();

              LiteralArray::Elements chars;
              for (size_t i = 0; i < lit->value.size(); ++i) {
                chars.push_back(LiteralChar::create(chTy, lit->value.at(i)));
              }
              result = LiteralArray::create(arrTy, chars);
              return;
            }

          }

          void visit_IS_PRESENT(OperatorExpression::OperatorExpressionPtr e) override
          {
            auto arg = arguments.at(0)->self<LiteralOptional>();
            if (arg) {
              result = LiteralBoolean::create(e->type->self<types::BooleanType>(),
                  arg->value != nullptr);
              return;
            }
          }

          void visit_GET(OperatorExpression::OperatorExpressionPtr e) override
          {
            auto arg = arguments.at(0)->self<LiteralOptional>();
            if (arg && arg->value) {
              result = arg->value;
              return;
            }
          }

          void visit_IS_LOGGABLE(OperatorExpression::OperatorExpressionPtr) override
          {
            if (logging.disableLogging) {
              result = LiteralBoolean::create(false);
              return;
            }
            if (logging.levels.empty()) {
              // since there are no log levels, we cannot determine anything else
              return;
            }
            auto lit = arguments.at(0)->self<LiteralString>();
            if (lit) {
              // if we have a literal and it matches the support levels
              result = LiteralBoolean::create(logging.levels.count(lit->value) != 0);
            } else {
              // the code generate will have to make a determination
            }

          }

          void visit_TO_STRING(OperatorExpression::OperatorExpressionPtr e) override
          {
            if (arguments.at(0)->type->self<types::StringType>()) {
              result = e->arguments.at(0);
              return;
            }
            auto lit = arguments.at(0)->self<Literal>();
            if (!lit) {
              return;
            }
            if (lit->self<LiteralBit>()) {
              result = LiteralString::create(
                  arguments.at(0)->self<LiteralBit>()->value ? '1' : '0');
            } else if (lit->self<LiteralBoolean>()) {
              result = LiteralString::create(
                  arguments.at(0)->self<LiteralBoolean>()->value ? "true" : "false");
            } else if (lit->self<LiteralByte>()) {
              char hexstr[3];
              int n = ::std::sprintf(hexstr, "%02x", arguments.at(0)->self<LiteralByte>()->value);
              assert(n == 2 && "expected 2 characters to have been written");
              result = LiteralString::create(hexstr);
            } else if (lit->self<LiteralChar>()) {
              result = LiteralString::create(arguments.at(0)->self<LiteralChar>()->value);
            } else if (lit->self<LiteralInteger>()) {
              result = LiteralString::create(
                  arguments.at(0)->self<LiteralInteger>()->value.toString());
            } else if (lit->self<LiteralReal>()) {
              result = LiteralString::create(
                  arguments.at(0)->self<LiteralReal>()->value.toString());
            }

          }

          void visit_INDEX(OperatorExpression::OperatorExpressionPtr e) override
          {
            auto litIndex = arguments.at(1)->self<LiteralInteger>();
            auto lit = arguments.at(0)->self<Literal>();
            if (litIndex == nullptr || lit == nullptr) {
              return;
            }
            auto index = litIndex->value.unsignedValue();

            if (lit->self<LiteralString>()) {
              auto str = arguments.at(0)->self<LiteralString>();
              auto charTy = e->type->self<types::CharType>();
              if (index < str->value.length()) {
                result = LiteralChar::create(charTy, str->value[index]);
              }
              return;
            }
            if (lit->self<LiteralArray>()) {
              auto arr = arguments.at(0)->self<LiteralArray>();
              if (index < arr->elements.size()) {
                result = arr->elements[index];
              }
              return;
            }
          }

          void visit_UNCHECKED_CAST(OperatorExpression::OperatorExpressionPtr e) override
          {
            auto expr = arguments.at(0)->self<Literal>();
            if (expr) {
              try {
                result = expr->castTo(e->type);
              } catch (...) {
                // if the cast fails, we just don't do anything
              }
            }
          }

          void visit_CHECKED_CAST(OperatorExpression::OperatorExpressionPtr e) override
          {
            auto expr = arguments.at(0)->self<Literal>();
            if (expr) {
              try {
                result = expr->castTo(e->type);
              } catch (...) {
                // if the cast fails, we just don't do anything
              }
              return;
            }

            if (e->type->isSameType(*arguments.at(0)->type)) {
              result = e->arguments.at(0);
            } else if (e->type->self<types::IntegerType>()
                && arguments.at(0)->self<LiteralInteger>()) {
              result = LiteralInteger::create(e->type->self<types::IntegerType>(),
                  arguments.at(0)->self<LiteralInteger>()->value);
            } else if (e->type->self<types::RealType>()
                && arguments.at(0)->self<LiteralInteger>()) {
              result = LiteralReal::create(e->type->self<types::RealType>(),
                  arguments.at(0)->self<LiteralInteger>()->value);
            } else if (e->type->self<types::BitType>() && arguments.at(0)->self<LiteralBit>()) {
              result = LiteralBit::create(e->type->self<types::BitType>(),
                  arguments.at(0)->self<LiteralBit>()->value);
            } else if (e->type->self<types::BooleanType>()
                && arguments.at(0)->self<LiteralBoolean>()) {
              result = LiteralBoolean::create(e->type->self<types::BooleanType>(),
                  arguments.at(0)->self<LiteralBoolean>()->value);
            } else if (e->type->self<types::ByteType>() && arguments.at(0)->self<LiteralByte>()) {
              result = LiteralByte::create(e->type->self<types::ByteType>(),
                  arguments.at(0)->self<LiteralByte>()->value);
            } else if (e->type->self<types::CharType>() && arguments.at(0)->self<LiteralChar>()) {
              result = LiteralChar::create(e->type->self<types::CharType>(),
                  arguments.at(0)->self<LiteralChar>()->value);
            } else if (e->type->self<types::RealType>() && arguments.at(0)->self<LiteralReal>()) {
              result = LiteralReal::create(e->type->self<types::RealType>(),
                  arguments.at(0)->self<LiteralReal>()->value);
            } else if (arguments.at(0)->self<LiteralString>()
                && arguments.at(0)->self<LiteralString>()) {
              result = LiteralString::create(e->type->self<types::StringType>(),
                  arguments.at(0)->self<LiteralString>()->value);
            }
          }

          void visit_SUBRANGE(OperatorExpression::OperatorExpressionPtr e) override
          {
            auto liti = arguments.at(1)->self<LiteralInteger>();
            auto litj = arguments.at(2)->self<LiteralInteger>();
            auto lit = arguments.at(0)->self<Literal>();

            if (!liti || !litj) {
              return;
            }
            auto i = liti->value.unsignedValue();
            auto j = litj->value.unsignedValue();

            if (i > j) {
              // this is an error, so don't optimize
              return;
            }

            if (lit) {
              if (lit->self<LiteralString>()) {
                auto str = lit->self<LiteralString>();
                auto strTy = e->type->self<types::StringType>();
                if (j <= str->value.size()) {
                  result = LiteralString::create(strTy, str->value.substr(i, j));
                }
                return;
              }

              if (lit->self<LiteralArray>()) {
                auto arr = lit->self<LiteralArray>();
                auto arrTy = e->type->self<types::ArrayType>();
                if (j <= arr->elements.size()) {
                  auto start = arr->elements.begin() + i;
                  auto end = arr->elements.begin() + j;
                  LiteralArray::Elements elems(start, end);
                  result = LiteralArray::create(arrTy, elems);
                }
                return;
              }
            }
            if (arguments.at(0)->type->self<types::ArrayType>()) {
              auto arrTy = arguments.at(0)->type->self<types::ArrayType>();
              if (arrTy->bounds.isFixed()) {
                if (i == 0 && j == arrTy->bounds.max()) {
                  result = OperatorExpression::create(e->type, OperatorExpression::OP_CHECKED_CAST,
                      e->arguments.at(0));
                }
              }
            }

          }

          void visit_CONCATENATE(OperatorExpression::OperatorExpressionPtr e) override
          {
            auto left = arguments.at(0)->self<Literal>();
            auto right = arguments.at(1)->self<Literal>();

            if (e->type->self<types::StringType>()) {
              auto strTy = e->type->self<types::StringType>();
              if (left && right) {
                ::std::string chars;
                if (left->self<LiteralString>()) {
                  auto strLit = left->self<LiteralString>();
                  chars += strLit->value;
                } else if (left->self<LiteralChar>()) {
                  auto chLit = left->self<LiteralChar>();
                  chars += chLit->value;
                } else {
                  return;
                }

                if (right->self<LiteralString>()) {
                  auto strLit = right->self<LiteralString>();
                  chars += strLit->value;
                } else if (right->self<LiteralChar>()) {
                  auto chLit = right->self<LiteralChar>();
                  chars += chLit->value;
                } else {
                  return;
                }
                result = LiteralString::create(strTy, chars);
                return;
              } else if (left && left->self<LiteralString>()) {
                auto strLit = left->self<LiteralString>();
                if (strLit->value.empty()) {
                  result = e->arguments.at(1);
                  return;
                }
              } else if (right && right->self<LiteralString>()) {
                auto strLit = right->self<LiteralString>();
                if (strLit->value.empty()) {
                  result = e->arguments.at(0);
                  return;
                }
              }
            }

            if (e->type->self<types::ArrayType>()) {
              auto arrTy = e->type->self<types::ArrayType>();
              if (left && right) {
                LiteralArray::Elements elems;
                if (arrTy->element->isSameType(*left->type)) {
                  elems.push_back(left);
                } else if (left->self<LiteralArray>()) {
                  auto arrLit = left->self<LiteralArray>();
                  elems.insert(elems.end(), arrLit->elements.begin(), arrLit->elements.end());
                } else {
                  return;
                }

                if (arrTy->element->isSameType(*right->type)) {
                  elems.push_back(right);
                } else if (right->self<LiteralArray>()) {
                  auto arrLit = right->self<LiteralArray>();
                  elems.insert(elems.end(), arrLit->elements.begin(), arrLit->elements.end());
                } else {
                  return;
                }
                result = LiteralArray::create(arrTy, elems);
                return;
              } else if (left && !arrTy->element->isSameType(*left->type)
                  && left->self<LiteralArray>()) {
                auto arrLit = left->self<LiteralArray>();
                if (arrLit->elements.empty()) {
                  result = e->arguments.at(1);
                  return;
                }
              } else if (right && !arrTy->element->isSameType(*right->type)
                  && right->self<LiteralArray>()) {
                auto arrLit = right->self<LiteralArray>();
                if (arrLit->elements.empty()) {
                  result = e->arguments.at(0);
                  return;
                }
              }
            }
          }

          void visit_LENGTH(OperatorExpression::OperatorExpressionPtr e) override
          {
            auto ity = e->type->self<types::IntegerType>();
            auto lit = arguments.at(0)->self<Literal>();

            if (lit) {
              if (lit->self<LiteralString>()) {
                auto str = lit->self<LiteralString>();
                if (str) {
                  result = LiteralInteger::create(ity, str->value.length());
                  return;
                }
              }

              if (lit->self<LiteralArray>()) {
                auto arr = lit->self<LiteralArray>();
                if (arr) {
                  result = LiteralInteger::create(ity, arr->elements.size());
                  return;
                }
              }
            }
            auto arrTy = arguments.at(0)->type->self<types::ArrayType>();
            if (arrTy) {
              if (arrTy->bounds.isFixed()) {
                result = LiteralInteger::create(ity, *arrTy->bounds.min());
                return;
              }
            }

          }

          void visit_OR(OperatorExpression::OperatorExpressionPtr e) override
          {
            if (e->type->self<types::BooleanType>()) {
              auto lhs = arguments.at(0)->self<LiteralBoolean>();
              auto rhs = arguments.at(1)->self<LiteralBoolean>();
              if (lhs && rhs) {
                result = LiteralBoolean::create(lhs->value || rhs->value);
              } else if (lhs) {
                if (lhs->value) {
                  result = lhs;
                } else {
                  result = e->arguments.at(1);
                }
              } else if (rhs) {
                if (rhs->value) {
                  result = rhs;
                } else {
                  result = e->arguments.at(0);
                }
              }
              return;
            }

            if (e->type->self<types::BitType>()) {
              auto lhs = arguments.at(0)->self<LiteralBit>();
              auto rhs = arguments.at(1)->self<LiteralBit>();
              if (lhs && rhs) {
                result = LiteralBit::create(lhs->value || rhs->value);
              } else if (lhs) {
                if (lhs->value) {
                  result = lhs;
                } else {
                  result = e->arguments.at(1);
                }
              } else if (rhs) {
                if (rhs->value) {
                  result = rhs;
                } else {
                  result = e->arguments.at(0);
                }
              }
              return;
            }
          }

          void visit_XOR(OperatorExpression::OperatorExpressionPtr e) override
          {
            if (e->type->self<types::BooleanType>()) {
              auto lhs = arguments.at(0)->self<LiteralBoolean>();
              auto rhs = arguments.at(1)->self<LiteralBoolean>();
              if (lhs && rhs) {
                result = LiteralBoolean::create(lhs->value ^ rhs->value);
              } else if (lhs) {
                if (lhs->value) {
                  result = OperatorExpression::create(e->type, OperatorExpression::OP_NOT,
                      e->arguments.at(1));
                } else {
                  result = e->arguments.at(1);
                }
              } else if (rhs) {
                if (rhs->value) {
                  result = OperatorExpression::create(e->type, OperatorExpression::OP_NOT,
                      e->arguments.at(0));
                } else {
                  result = e->arguments.at(0);
                }
              }
              return;
            }

            if (e->type->self<types::BitType>()) {
              auto lhs = arguments.at(0)->self<LiteralBit>();
              auto rhs = arguments.at(1)->self<LiteralBit>();
              if (lhs && rhs) {
                result = LiteralBit::create(lhs->value ^ rhs->value);
              } else if (lhs) {
                if (lhs->value) {
                  result = OperatorExpression::create(e->type, OperatorExpression::OP_NOT,
                      e->arguments.at(1));
                } else {
                  result = e->arguments.at(1);
                }
              } else if (rhs) {
                if (rhs->value) {
                  result = OperatorExpression::create(e->type, OperatorExpression::OP_NOT,
                      e->arguments.at(0));
                } else {
                  result = e->arguments.at(0);
                }
              }
              return;
            }
          }

          void visit_AND(OperatorExpression::OperatorExpressionPtr e) override
          {
            if (e->type->self<types::BooleanType>()) {
              auto lhs = arguments.at(0)->self<LiteralBoolean>();
              auto rhs = arguments.at(1)->self<LiteralBoolean>();
              if (lhs && rhs) {
                result = LiteralBoolean::create(lhs->value && rhs->value);
              } else if (lhs) {
                if (lhs->value) {
                  result = e->arguments.at(1);
                } else {
                  result = lhs;
                }
              } else if (rhs) {
                if (rhs->value) {
                  result = e->arguments.at(0);
                } else {
                  result = rhs;
                }
              }
              return;
            }

            if (e->type->self<types::BitType>()) {
              auto lhs = arguments.at(0)->self<LiteralBit>();
              auto rhs = arguments.at(1)->self<LiteralBit>();
              if (lhs && rhs) {
                result = LiteralBit::create(lhs->value && rhs->value);
              } else if (lhs) {
                if (lhs->value) {
                  result = e->arguments.at(1);
                } else {
                  result = lhs;
                }
              } else if (rhs) {
                if (rhs->value) {
                  result = e->arguments.at(0);
                } else {
                  result = rhs;
                }
              }
              return;
            }
          }

          void visit_NOT(OperatorExpression::OperatorExpressionPtr e) override
          {
            if (e->type->self<types::BooleanType>()) {
              auto arg = arguments.at(0)->self<LiteralBoolean>();
              if (arg) {
                result = LiteralBoolean::create(!arg->value);
              }
              return;
            }
            if (e->type->self<types::BitType>()) {
              auto arg = arguments.at(0)->self<LiteralBit>();
              if (arg) {
                result = LiteralBit::create(!arg->value);
              }
              return;
            }
          }

          void visit_LT(OperatorExpression::OperatorExpressionPtr) override
          {
            auto lhs = arguments.at(0)->self<Literal>();
            auto rhs = arguments.at(1)->self<Literal>();

            if (lhs && rhs) {
              auto cmp = lhs->compare(*rhs);
              result = LiteralBoolean::create(cmp.has_value() && cmp.value() < 0);
            }
          }

          void visit_LTE(OperatorExpression::OperatorExpressionPtr) override
          {
            auto lhs = arguments.at(0)->self<Literal>();
            auto rhs = arguments.at(1)->self<Literal>();

            if (lhs && rhs) {
              auto cmp = lhs->compare(*rhs);
              result = LiteralBoolean::create(cmp.has_value() && cmp.value() <= 0);
            }
          }

          void visit_EQ(OperatorExpression::OperatorExpressionPtr e) override
          {
            auto lhs = arguments.at(0)->self<Literal>();
            auto rhs = arguments.at(1)->self<Literal>();

            if (lhs && rhs) {
              result = LiteralBoolean::create(lhs->compare(*rhs) == 0);
            } else if (lhs && lhs->compare(*LiteralBoolean::create(true)) == 0) {
              result = e->arguments.at(1);
            } else if (lhs && lhs->compare(*LiteralBoolean::create(false)) == 0) {
              result = OperatorExpression::create(e->type, OperatorExpression::OP_NOT,
                  e->arguments.at(1));
            } else if (rhs && rhs->compare(*LiteralBoolean::create(true)) == 0) {
              result = e->arguments.at(0);
            } else if (rhs && rhs->compare(*LiteralBoolean::create(false)) == 0) {
              result = OperatorExpression::create(e->type, OperatorExpression::OP_NOT,
                  e->arguments.at(0));
            }
          }

          void visit_NEQ(OperatorExpression::OperatorExpressionPtr e) override
          {
            auto lhs = arguments.at(0)->self<Literal>();
            auto rhs = arguments.at(1)->self<Literal>();
            if (lhs && rhs) {
              result = LiteralBoolean::create(lhs->compare(*rhs) != 0);
            }
            if (lhs && lhs->compare(*LiteralBoolean::create(false)) == 0) {
              result = e->arguments.at(1);
            } else if (lhs && lhs->compare(*LiteralBoolean::create(true)) == 0) {
              result = OperatorExpression::create(e->type, OperatorExpression::OP_NOT,
                  e->arguments.at(1));
            } else if (rhs && rhs->compare(*LiteralBoolean::create(false)) == 0) {
              result = e->arguments.at(0);
            } else if (rhs && rhs->compare(*LiteralBoolean::create(true)) == 0) {
              result = OperatorExpression::create(e->type, OperatorExpression::OP_NOT,
                  e->arguments.at(0));
            }
          }

          void visit_NEG(OperatorExpression::OperatorExpressionPtr e) override
          {
            auto ity = e->type->self<types::IntegerType>();
            auto ilit = arguments.at(0)->self<LiteralInteger>();
            if (ilit) {
              result = LiteralInteger::create(ity, ilit->value.negate());
              return;
            }

            auto fty = e->type->self<types::RealType>();
            auto flit = arguments.at(0)->self<LiteralReal>();
            if (flit) {
              auto ty = flit->type->self<types::RealType>();
              result = LiteralReal::create(fty, flit->value.negate());
              return;
            }
          }
          void visit_ADD(OperatorExpression::OperatorExpressionPtr e) override
          {
            auto ity = e->type->self<types::IntegerType>();
            if (ity) {
              auto lhs = arguments.at(0)->self<LiteralInteger>();
              auto rhs = arguments.at(1)->self<LiteralInteger>();
              if (lhs && rhs) {
                result = LiteralInteger::create(ity, lhs->value.add(rhs->value));
              } else if (lhs && lhs->value == BigInt(0)) {
                result = e->arguments.at(1);
              } else if (rhs && rhs->value == BigInt(0)) {
                result = e->arguments.at(0);
              }
              return;
            }

            auto fty = e->type->self<types::RealType>();
            if (fty) {
              auto lhs = arguments.at(0)->self<LiteralReal>();
              auto rhs = arguments.at(1)->self<LiteralReal>();
              if (lhs && rhs) {
                result = LiteralReal::create(fty, lhs->value.add(rhs->value));
              } else if (lhs && lhs->value == BigReal(0)) {
                result = e->arguments.at(1);
              } else if (rhs && rhs->value == BigReal(0)) {
                result = e->arguments.at(0);
              }
              return;
            }
          }

          void visit_SUB(OperatorExpression::OperatorExpressionPtr e)
          override
          {
            auto ity = e->type->self<types::IntegerType>();
            if (ity) {
              auto lhs = arguments.at(0)->self<LiteralInteger>();
              auto rhs = arguments.at(1)->self<LiteralInteger>();
              if (lhs && rhs) {
                result = LiteralInteger::create(ity, lhs->value.subtract(rhs->value));
              } else if (lhs && lhs->value == BigInt(0)) {
                result = OperatorExpression::create(ity, ir::OperatorExpression::OP_NEG,
                    e->arguments.at(1));
              } else if (rhs && rhs->value == BigInt(0)) {
                result = e->arguments.at(0);
              }
              return;
            }

            auto fty = e->type->self<types::RealType>();
            if (fty) {
              auto lhs = arguments.at(0)->self<LiteralReal>();
              auto rhs = arguments.at(1)->self<LiteralReal>();
              if (lhs && rhs) {
                result = LiteralReal::create(fty, lhs->value.subtract(rhs->value));
              } else if (lhs && lhs->value == BigInt(0)) {
                result = OperatorExpression::create(fty, ir::OperatorExpression::OP_NEG,
                    e->arguments.at(1));
              } else if (rhs && rhs->value == BigInt(0)) {
                result = e->arguments.at(0);
              }
              return;
            }
          }
          void visit_MUL(OperatorExpression::OperatorExpressionPtr e)
          override
          {
            auto ity = e->type->self<types::IntegerType>();
            if (ity) {
              auto lhs = arguments.at(0)->self<LiteralInteger>();
              auto rhs = arguments.at(1)->self<LiteralInteger>();
              if (lhs && rhs) {
                result = LiteralInteger::create(ity, lhs->value.multiply(rhs->value));
              } else if (lhs && lhs->value == BigInt(0)) {
                result = lhs;
              } else if (rhs && rhs->value == BigInt(0)) {
                result = rhs;
              } else if (lhs && lhs->value == BigInt(1)) {
                result = e->arguments.at(1);
              } else if (rhs && rhs->value == BigInt(1)) {
                result = e->arguments.at(0);
              } else if (lhs && lhs->value == BigInt(-1)) {
                result = OperatorExpression::create(ity, ir::OperatorExpression::OP_NEG,
                    e->arguments.at(1));
              } else if (rhs && rhs->value == BigInt(-1)) {
                result = OperatorExpression::create(ity, ir::OperatorExpression::OP_NEG,
                    e->arguments.at(0));
              }
              return;
            }

            auto fty = e->type->self<types::RealType>();
            if (fty) {
              auto lhs = arguments.at(0)->self<LiteralReal>();
              auto rhs = arguments.at(1)->self<LiteralReal>();
              if (lhs && rhs) {
                result = LiteralReal::create(fty, lhs->value.multiply(rhs->value));
              } else if (lhs && lhs->value == BigReal(0)) {
                result = lhs;
              } else if (rhs && rhs->value == BigReal(0)) {
                result = rhs;
              } else if (lhs && lhs->value == BigReal(1)) {
                result = e->arguments.at(1);
              } else if (rhs && rhs->value == BigReal(1)) {
                result = e->arguments.at(0);
              } else if (lhs && lhs->value == BigReal(-1)) {
                result = OperatorExpression::create(fty, ir::OperatorExpression::OP_NEG,
                    e->arguments.at(1));
              } else if (rhs && rhs->value == BigInt(-1)) {
                result = OperatorExpression::create(fty, ir::OperatorExpression::OP_NEG,
                    e->arguments.at(0));
              }
              return;
            }
          }

          void visit_DIV(OperatorExpression::OperatorExpressionPtr e)
          override
          {
            // if the right hand side is 0, then we don't do anything; there should
            // be a guard that causes the program to be aborted and this expression will
            // never be evaluated
            auto ity = e->type->self<types::IntegerType>();
            if (ity) {
              auto lhs = arguments.at(0)->self<LiteralInteger>();
              auto rhs = arguments.at(1)->self<LiteralInteger>();
              if (rhs && rhs->value == BigInt(0)) {
                // ignore
              } else if (lhs && rhs) {
                result = LiteralInteger::create(ity, lhs->value.divide(rhs->value));
              } else if (lhs && lhs->value == BigInt(0)) {
                result = lhs;
              } else if (rhs && rhs->value == BigInt(1)) {
                result = e->arguments.at(0);
              } else if (rhs && rhs->value == BigInt(-1)) {
                result = OperatorExpression::create(ity, ir::OperatorExpression::OP_NEG,
                    e->arguments.at(0));
              }
              return;
            }

            auto fty = e->type->self<types::RealType>();
            if (fty) {
              auto lhs = arguments.at(0)->self<LiteralReal>();
              auto rhs = arguments.at(1)->self<LiteralReal>();
              if (rhs && rhs->value == BigReal(0)) {
                // ignore
              } else if (lhs && rhs) {
                result = LiteralReal::create(fty, lhs->value.divide(rhs->value));
              } else if (lhs && lhs->value == BigReal(0)) {
                result = lhs;
              } else if (rhs && rhs->value == BigReal(1)) {
                result = e->arguments.at(0);
              } else if (rhs && rhs->value == BigReal(-1)) {
                result = OperatorExpression::create(fty, ir::OperatorExpression::OP_NEG,
                    e->arguments.at(0));
              }
              return;
            }
          }

          void visit_REM(OperatorExpression::OperatorExpressionPtr e)
          override
          {
            // if the right hand side is 0, then we don't do anything; there should
            // be a guard that causes the program to be aborted and this expression will
            // never be evaluated
            auto ity = e->type->self<types::IntegerType>();
            if (ity) {
              auto lhs = arguments.at(0)->self<LiteralInteger>();
              auto rhs = arguments.at(1)->self<LiteralInteger>();
              if (rhs && rhs->value == BigInt(0)) {
                // ignore
              } else if (lhs && rhs) {
                result = LiteralInteger::create(ity, lhs->value.remainder(rhs->value));
              } else if (lhs && lhs->value == BigInt(0)) {
                result = lhs;
              } else if (rhs && rhs->value.abs() == BigInt(1)) {
                result = LiteralInteger::create(ity, BigInt(0));
              }
              return;
            }
          }

          void visit_MOD(OperatorExpression::OperatorExpressionPtr e)
          override
          {
            // if the right hand side is 0, then we don't do anything; there should
            // be a guard that causes the program to be aborted and this expression will
            // never be evaluated
            auto ity = e->type->self<types::IntegerType>();
            if (ity) {
              auto lhs = arguments.at(0)->self<LiteralInteger>();
              auto rhs = arguments.at(1)->self<LiteralInteger>();
              if (rhs && rhs->value == BigInt(0)) {
                // ignore
              } else if (lhs && rhs) {
                result = LiteralInteger::create(ity, lhs->value.mod(rhs->value));
              } else if (lhs && lhs->value == BigInt(0)) {
                result = lhs;
              } else if (rhs && rhs->value == BigInt(1)) {
                result = LiteralInteger::create(ity, BigInt(0));
              }
              return;
            }
          }

          void visit_NEW(OperatorExpression::OperatorExpressionPtr e) override
          {
            if (e->arguments.size() == 1 && e->arguments.at(0)->type->isSameType(*e->type)) {
              // not really an const fold, but close enough
              result = e->arguments.at(0);
              return;
            }

            ::std::vector<Literal::LiteralPtr> args;
            for (auto arg : arguments) {
              auto lit = arg->self<Literal>();
              if (!lit) {
                return;
              }
              args.push_back(lit);
            }

            if (e->type->self<types::ArrayType>()) {
              result = LiteralArray::create(e->type->self<types::ArrayType>(), args);
              return;
            }
            if (e->type->self<types::StructType>()) {
              result = LiteralStruct::create(e->type->self<types::StructType>(), args);
              return;
            }
            if (e->type->self<types::TupleType>()) {
              result = LiteralTuple::create(e->type->self<types::TupleType>(), args);
              return;
            }
            if (e->type->self<types::OptType>()) {
              if (args.empty()) {
                result = LiteralOptional::create(e->type->self<types::OptType>());
              } else {
                result = LiteralOptional::create(e->type->self<types::OptType>(), args.at(0));
              }
              return;
            }
            if (e->type->self<types::NamedType>()) {
              result = LiteralNamedType::create(e->type->self<types::NamedType>(), args.at(0));
              return;
            }
          }

          const ::std::vector<EExpression> &arguments;
          const ConstFoldPass::LoggingConfiguration &logging;
          EExpression result;
        }
        ;

        struct LiteralComparator
        {
          bool operator()(const ir::Literal::LiteralPtr &a, const ir::Literal::LiteralPtr &b) const
          {
            if (!a->type->isSameType(*b->type)) {
              return a->type->name() < b->type->name();
            }

            auto cmp = a->compare(*b);
            assert(cmp.has_value());
            return cmp.value() < 0;
          }
        };

        // TODO: can we use the EnvironmentalTransform here?
        struct Pass: public SSATransform
        {
          Pass(const ConstFoldPass::LoggingConfiguration &xlogging)
              : globalLiterals(LiteralComparator()), globalEnv(::std::make_shared<Environment>()),
                  env(globalEnv), logging(xlogging)
          {
          }
          ~Pass()
          {
          }

          /**
           * Find a literal.
           * @param v a variable
           * @return a literal or nullptr
           */
          template<class T = ir::Literal>
          ::std::shared_ptr<const T> search(ir::Variable::VariablePtr v)
          {
            ::std::shared_ptr<const T> lit;
            while (v) {
              auto e = env->find(v);
              if (e == nullptr) {
                break;
              }
              lit = e->self<T>();
              if (lit) {
                break;
              }

              if (auto vv = e->self<ir::Variable>(); vv) {
                v = vv;
              } else {
                break;
              }
            }
            return lit;
          }

          /**
           * Turn a literal into a unique global value that we'll reference from now on.
           */
          Variable::VariablePtr createLiteral(Literal::LiteralPtr literal)
          {
            auto i = globalLiterals.find(literal);
            if (i != globalLiterals.end()) {
              return i->second;
            }
            Name n = Name::create("literal");
            Variable::VariablePtr var = Variable::create(Variable::Scope::GLOBAL_SCOPE,
                literal->type, n);
            newGlobals.push_back(GlobalValue::create(var, literal, ir::GlobalValue::DEFAULT));
            globalLiterals[literal] = var;
            globalEnv->addBinding(var, literal);
            return var;
          }

          ENode visit(const ir::Program::ProgramPtr &node) override
          {
            for (auto gv : node->globals) {
              auto value = gv->value->self<ir::Literal>();
              if (value) {
                globalLiterals[value] = gv->variable;
                globalEnv->addBinding(gv->name, value);
              }
            }
            auto newPrg = Transform::visit(node);
            if (newGlobals.empty()) {
              return newPrg;
            }
            ir::Program::ProgramPtr prg;
            if (newPrg) {
              prg = newPrg->self<ir::Program>();
            } else {
              newPrg = node;
            }
            ir::Program::Globals globals(prg->globals);
            globals.insert(globals.end(), newGlobals.begin(), newGlobals.end());
            return ir::Program::create(globals, prg->processes, prg->types);
          }

          ENode visit(const ir::ProcessDecl::ProcessDeclPtr &node) override
          {
            for (auto vv : node->variables) {
              auto pv = vv->self<ir::ProcessValue>();
              if (pv && pv->value) {
                auto value = pv->value->self<ir::Literal>();
                if (value) {
                  env->addBinding(pv->name, value);
                }
              }
            }
            return Transform::visit(node);
          }

          ENode visit(const ir::LambdaExpression::LambdaExpressionPtr &node) override
          {
            auto restore = env;
            env = env->create();
            auto res = Transform::visit(node);
            env = restore;
            return res;
          }

          ENode visit(const ir::ValueDecl::ValueDeclPtr &node) override
          {
            auto value = transformExpr(node->value);
            ir::Literal::LiteralPtr literal;
            if (auto v = value.actual->self<ir::Variable>(); v) {
              literal = search(v);
            } else {
              literal = value.actual->self<ir::Literal>();
            }
            if (literal) {
              auto lit = createLiteral(literal);
              // turn the literal into global variable and reference it instead
              value = Tx<EExpression>(lit, lit != node->value);
            }
            env->addBinding(node->name, value.actual);

            auto next = transformStatement(node->next);
            if (value.isNew || next.isNew) {
              return ValueDecl::createSimplified(node->variable, value.actual, next.actual);
            }
            return nullptr;
          }

          ENode visit(const ir::GetMember::GetMemberPtr &node) override
          {
            auto obj = search(node->object);
            if (!obj) {
              return Transform::visit(node);
            }

            if (obj->self<LiteralStruct>()) {
              auto lit = obj->self<LiteralStruct>();
              auto ty = obj->type->self<types::StructType>();
              auto i = ty->indexOfMember(node->name);
              return lit->members.at(i);
            }

            if (obj->self<LiteralUnion>()) {
              auto lit = obj->self<LiteralUnion>();
              auto ty = obj->type->self<types::UnionType>();
              assert(lit->value->type->isSameType(*node->type));
              return lit->value;
            }

            if (obj->self<LiteralTuple>()) {
              auto lit = obj->self<LiteralTuple>();
              auto ty = obj->type->self<types::TupleType>();
              auto i = ty->indexOfMember(node->name);
              return lit->elements.at(i);
            }

            return Transform::visit(node);
          }

          ENode visit(const ir::GetDiscriminant::GetDiscriminantPtr &node) override
          {
            auto obj = search<LiteralUnion>(node->object);
            if (obj) {
              auto ty = obj->type->self<types::UnionType>();
              auto i = ty->indexOfMember(obj->member);
              return ty->members.at(i).discriminant;
            }
            return Transform::visit(node);
          }

          ENode visit(const ir::IfStatement::IfStatementPtr &node) override
          {
            auto cond = search<LiteralBoolean>(node->condition);
            if (cond == nullptr) {
              auto restore = env;
              env = restore->create();
              env->addBinding(node->condition, LiteralBoolean::create(true));
              auto ift = transformStatement(node->iftrue);
              env = restore->create();
              env->addBinding(node->condition, LiteralBoolean::create(false));
              auto iff = transformStatement(node->iffalse);
              env = restore;
              auto next = transformStatement(node->next);
              if (ift.isNew || iff.isNew || next.isNew) {
                return ir::IfStatement::create(node->condition, ift.actual, iff.actual, next.actual);
              }
              return nullptr;
            }

            StatementBlock::StatementBlockPtr block;
            if (cond->value) {
              block = StatementBlock::create(node->iftrue, node->next);
            } else {
              block = StatementBlock::create(node->iffalse, node->next);
            }
            // flatten the block
            return transformStatement(block->statements).actual;
          }

          ENode visit(const ir::ForeachStatement::ForeachStatementPtr &node) override
          {
            auto data = env->find<OperatorExpression>(node->data);
            if (data == nullptr) {
              return Transform::visit(node);
            }
            if (data->name != OperatorExpression::OP_NEW) {
              return Transform::visit(node);
            }
            if (data->type->self<types::ArrayType>() == nullptr) {
              return Transform::visit(node);
            }

            // if there are no arguments, then we don't execute the loop
            // we remove the body completely
            if (data->arguments.empty()) {
              return nop(transformStatement(node->next).actual);
            }

            // otherwise, do the normal transform
            return Transform::visit(node);
          }

          ENode visit(const mylang::ethir::ir::NewUnion::NewUnionPtr &node) override
          {
            auto ty = node->type->self<types::UnionType>();
            auto lit = search(node->value);
            if (lit) {
              return LiteralUnion::create(ty, node->member, lit);
            }
            return Transform::visit(node);
          }

          ENode visit(const ir::OperatorExpression::OperatorExpressionPtr &node) override
          {

            ::std::vector<EExpression> exprs;
            bool someLiterals = false;
            // gather any constants
            for (auto var : node->arguments) {
              auto lit = search<ir::Literal>(var);
              if (lit) {
                exprs.push_back(lit);
                someLiterals = true;
              } else {
                exprs.push_back(var);
              }
            }
            // if there are no literal at all, then apply the default transform
            if (someLiterals == false) {
              return Transform::visit(node);
            }

            ConstFoldOperatorExpression opfold(logging, exprs);
            opfold.visit(node);
            if (opfold.result) {
              if (!opfold.result->type->isSameType(*node->type)) {
                throw ::std::runtime_error(
                    "ConstFold failed to produce the same expression type : expected "
                        + node->type->toString() + ", but found " + opfold.result->type->toString()
                        + "; operator was "
                        + ::std::string(OperatorExpression::operatorName(node->name)));
              }
            }
            if (opfold.result == nullptr) {
              return Transform::visit(node);
            }
            return opfold.result;
          }

          ENode visit(const ir::Phi::PhiPtr&) override
          {
            // we cannot modify the result of a phi function
            return nullptr;
          }

          ENode visit(const ir::Variable::VariablePtr &node) override
          {
            // resolve the far as as possible
            ir::Variable::VariablePtr v = node;
            do {
              auto e = env->find(v);
              if (!e) {
                break; // return v
              }
              if (auto vv = e->self<ir::Variable>(); vv) {
                v = vv;
              } else {
                break; // return v
              }
            } while (v);

            return v == node ? nullptr : v;
          }

          /** The global literals */
          ::std::map<ir::Literal::LiteralPtr, Variable::VariablePtr, LiteralComparator> globalLiterals;

          /** The new globals */
          ::std::vector<GlobalValue::GlobalValuePtr> newGlobals;

          /** The current environment */
          ::std::shared_ptr<Environment> globalEnv;

          /** The current environment */
          ::std::shared_ptr<Environment> env;

          const ConstFoldPass::LoggingConfiguration &logging;

        };

      }
      ConstFoldPass::ConstFoldPass()
      {
      }

      ConstFoldPass::~ConstFoldPass()
      {
      }

      EProgram ConstFoldPass::transform(EProgram src)
      {
        Pass pass(logConfig);
        auto res = pass.visitNode(src);
        return res ? res->self<Program>() : nullptr;
      }

      bool ConstFoldPass::requiresMaintainSSAPass() const
      {
        return true;
      }

    }

  }
}

