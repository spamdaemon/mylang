#include <mylang/ethir/ssa/Peephole.h>
#include <mylang/ethir/ir/LambdaExpression.h>
#include <mylang/ethir/ir/nodes.h>

namespace mylang {
  namespace ethir {
    using namespace ir;
    namespace ssa {

      Peephole::Peephole()
      {
      }
      Peephole::~Peephole()
      {
      }

      ir::ReturnStatement::ReturnStatementPtr Peephole::matchReturn(const EStatement &s)
      {
        if (!s) {
          return nullptr;
        }
        auto cur = s->skipComments();
        if (!cur) {
          return nullptr;
        }
        auto ret = cur->self<ir::ReturnStatement>();
        if (ret && ret->value) {
          return ret;
        }
        return nullptr;
      }

      ::std::vector<EStatement> Peephole::matchStatements(const EStatement &s,
          ::std::optional<size_t> N)
      {
        ::std::vector<EStatement> stmts;
        EStatement cur = s;
        while (cur) {
          cur = cur->skipComments();
          if (cur->isNOP()) {
            // ignore
          } else if (!N.has_value() || stmts.size() < N.value()) {
            stmts.push_back(cur);
          } else {
            break;
          }
          cur = cur->next;
        }
        return stmts;
      }

      ::std::pair<ir::OperatorExpression::OperatorExpressionPtr,
          ir::OperatorExpression::OperatorExpressionPtr> Peephole::matches(const ENode &node,
          const Environment &env, ::std::optional<OperatorExpression::Operator> op1,
          ::std::optional<OperatorExpression::Operator> op2, size_t i)
      {
        auto e1 = matches(node, op1);
        if (e1 && i < e1->arguments.size()) {
          auto tmp = env.find(e1->arguments.at(i));
          if (tmp) {
            auto e2 = matches(tmp, op2);
            if (e2) {
              return ::std::make_pair(e1, e2);
            }
          }
        }
        return ::std::make_pair(nullptr, nullptr);
      }

      ir::OperatorExpression::OperatorExpressionPtr Peephole::matches(const ENode &node,
          ::std::optional<ir::OperatorExpression::Operator> op)
      {
        auto e = node->self<OperatorExpression>();
        if (e && (!op.has_value() || e->name == op.value())) {
          return e;
        }
        return nullptr;
      }

      ::std::pair<ir::OperatorExpression::OperatorExpressionPtr,
          ir::OperatorExpression::OperatorExpressionPtr> Peephole::matches(const ENode &node,
          const Environment &env, const ::std::vector<OperatorExpression::Operator> &op1,
          const ::std::vector<OperatorExpression::Operator> &op2, size_t i)
      {
        auto e1 = matches(node, op1);
        if (e1 && i < e1->arguments.size()) {
          auto tmp = env.find(e1->arguments.at(i));
          if (tmp) {
            auto e2 = matches(tmp, op2);
            if (e2) {
              return ::std::make_pair(e1, e2);
            }
          }
        }
        return ::std::make_pair(nullptr, nullptr);
      }

      ir::OperatorExpression::OperatorExpressionPtr Peephole::matches(const ENode &node,
          const ::std::vector<ir::OperatorExpression::Operator> &ops)
      {
        auto e = node->self<OperatorExpression>();
        if (e) {
          if (ops.empty()) {
            return e;
          }
          for (ir::OperatorExpression::Operator op : ops) {
            if (op == e->name) {
              return e;
            }
          }
        }
        return nullptr;
      }

      ::std::shared_ptr<Peephole> Peephole::createMapIdentity()
      {
        struct Impl: public Peephole
        {
          ~Impl()
          {
          }

          ENode apply(const ENode &node, const Environment &env) const override
          {
            auto e = node->self<OperatorExpression>();

            if (e
                && (e->name == OperatorExpression::OP_MAP_ARRAY
                    || e->name == OperatorExpression::OP_MAP_OPTIONAL)) {
              auto fn = env.find<LambdaExpression>(e->arguments.at(1));
              if (fn && fn->isIdentity()) {
                return e->arguments.at(0);
              }
            }
            return nullptr;
          }
        };

        return ::std::make_shared<Impl>();
      }

      Peephole::Ptr Peephole::createCallWrapper()
      {
        struct Impl: public Peephole
        {
          ~Impl()
          {
          }

          ENode apply(const ENode &node, const Environment &env) const override
          {
            auto e = matches(node, OperatorExpression::OP_CALL);

            // we need at least 2 arguments for the call, the function itself
            // and at least 1 argument
            if (e) {
              auto fn = env.find<LambdaExpression>(e->arguments.at(0));
              if (fn && fn->parameters.size() > 0) {
                auto ret = matchReturn(fn->body->statements);
                if (!ret) {
                  return nullptr;
                }
                auto arg = ret->value;

                // since the scope is global or process, we can just return the value
                // of that variable
                if (arg->scope != Variable::Scope::FUNCTION_SCOPE) {
                  return arg;
                }
                // if we find the name of the argument in the caller's environment, then we can just return it
                if (env.find(arg->name)) {
                  return arg;
                }

                // if we return the argument, the replace it with the initial argument to the call
                for (size_t p = 0; p < fn->parameters.size(); ++p) {
                  if (arg->name == fn->parameters.at(p)->variable->name) {
                    return e->arguments.at(1 + p);
                  }
                }
              }
            }
            return nullptr;
          }
        };

        return ::std::make_shared<Impl>();
      }

    }
  }
}
