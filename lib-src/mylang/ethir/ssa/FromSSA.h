#ifndef CLASS_MYLANG_ETHIR_SSA_FROMSSA_H
#define CLASS_MYLANG_ETHIR_SSA_FROMSSA_H

#ifndef CLASS_MYLANG_ETHIR_IR_PROGRAM_H
#include <mylang/ethir/ir/Program.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_LAMBDAEXPRESSION_H
#include <mylang/ethir/ir/LambdaExpression.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ssa {

      /**
       * This transform replaces certain operators with functions that implement the operator
       * in terms of other functions.
       */
      class FromSSA
      {
        /**
         * Convert a program into an equivalent program using SSA form.
         * @param node a program
         * @return a program in SSA form
         */
      public:
        static mylang::ethir::ir::Program::ProgramPtr transform(
            mylang::ethir::ir::Program::ProgramPtr node);

        /**
         * Convert a function into an equivalent function using SSA form.
         * @param node a function
         * @return a function in SSA form
         */
      public:
        static mylang::ethir::ir::LambdaExpression::LambdaExpressionPtr transform(
            mylang::ethir::ir::LambdaExpression::LambdaExpressionPtr node);

      };
    }
  }
}
#endif
