#include <mylang/ethir/ssa/SimplifyPass.h>
#include <mylang/ethir/EnvironmentalTransform.h>

#include <cassert>
#include <map>

namespace mylang {
  namespace ethir {
    using namespace ir;
    namespace ssa {

      SimplifyPass::SimplifyPass(Peepholes xpeepholes)
          : peepholes(::std::move(xpeepholes))
      {
      }

      SimplifyPass::~SimplifyPass()
      {
      }

      EProgram SimplifyPass::transform(EProgram src)
      {

        struct Pass: public EnvironmentalTransform
        {
          Pass(const Peepholes &ph)
              : peepholes(ph)
          {
          }
          ~Pass()
          {
          }

          ENode visitNode(const ENode &node) override final
          {
            ENode n = EnvironmentalTransform::visitNode(node);
            if (n == nullptr) {
              n = node;
            }
            auto &E = *current();
            for (auto ph : peepholes) {
              ENode tmp = ph->apply(n, E);
              if (tmp) {
                n = tmp;
              }
            }
            if (n == node) {
              return nullptr;
            }
            return n;
          }

          const Peepholes &peepholes;

        };

        Pass pass(peepholes);
        auto res = pass.visitNode(src);
        return res ? res->self<Program>() : nullptr;
      }

    }

  }
}
