#ifndef CLASS_MYLANG_ETHIR_SSA_DERIVELITERALS_H
#define CLASS_MYLANG_ETHIR_SSA_DERIVELITERALS_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_SSA_SSAPASS_H
#include <mylang/ethir/ssa/SSAPass.h>
#endif

#include <set>

namespace mylang {
  namespace ethir {
    namespace ssa {

      /**
       * This pass derives literals from the bounds of an expressions type.
       * For example, if expression of two integer variables yields an integer
       * with a fixed bound, the the expression can be turned into a literal.
       */
      class DeriveLiterals: public SSAPass
      {
      public:
        DeriveLiterals();

        /** Destructor */
      public:
        virtual ~DeriveLiterals();

        /**
         * Apply this transform the specified node which is in ssa form.
         * @param node a node
         */
      protected:
        EProgram transform(EProgram src) override final;
      };
    }

  }
}
#endif
