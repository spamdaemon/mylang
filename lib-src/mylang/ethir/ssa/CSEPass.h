#ifndef CLASS_MYLANG_ETHIR_SSA_CSEPASS_H
#define CLASS_MYLANG_ETHIR_SSA_CSEPASS_H

#ifndef CLASS_MYLANG_ETHIR_SSA_SSAPASS_H
#include <mylang/ethir/ssa/SSAPass.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ssa {

      /**
       * Find expressions that compute the same value within the same scope
       * and replace the evaluation with result of the first evaluation. This is
       * call common subexpression evaluation.
       *
       * This pass does not eliminate redundant statements, only value declarations.
       *
       * Global values and process values are not addressed yet either.
       */
      class CSEPass: public SSAPass
      {
        /**
         * Create a CSE pass.
         * @param eliminateCommonLogging true to eliminate redundant logging statements
         */
      public:
        CSEPass(bool eliminateCommonLogging = false);

        /** Destructor */
      public:
        virtual ~CSEPass();

        /**
         * Apply this transform the specified node which is in ssa form.
         * @param node a node
         */
      protected:
        EProgram transform(EProgram src) override final;

        /** True if the logging statements can be eliminated */
      public:
        const bool eliminateCommonLogging;
      };
    }

  }
}
#endif
