#ifndef CLASS_MYLANG_ETHIR_SSA_SSAPASS_H
#define CLASS_MYLANG_ETHIR_SSA_SSAPASS_H

#ifndef CLASS_MYLANG_ETHIR_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_SSA_SSAPROGRAM_H
#include <mylang/ethir/ssa/SSAProgram.h>
#endif

#include <memory>

namespace mylang {
  namespace ethir {
    namespace ssa {

      /**
       * An SSA pass perform transforms on a program that requires
       * the program to be in SSA form.
       */
      class SSAPass
      {
        /**
         * Create a program
         * @param functions the global functions
         * @param values the global values
         * @param processes the defined processes
         */
      protected:
        SSAPass();

        /** Destructor */
      public:
        virtual ~SSAPass();

        /**
         * Transform the specified SSA program.
         * @param ssa program
         * @return true if the program was modified, false otherwise
         */
      public:
        bool transformProgram(SSAProgram &prg);

        /**
         * Apply this transform the specified node which is in ssa form.
         * @param node a node
         */
      protected:
        virtual EProgram transform(EProgram src) = 0;

        /**
         * Determine if this pass requires a cleanup of the SSA form.
         * @return true if this pass may have introduced non-SSA form constructs
         */
      protected:
        virtual bool requiresMaintainSSAPass() const;

      };
    }

  }
}
#endif
