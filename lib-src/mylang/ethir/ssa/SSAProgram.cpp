#include <mylang/ethir/ssa/SSAProgram.h>
#include <mylang/ethir/ssa/ToSSA.h>
#include <mylang/ethir/ssa/FromSSA.h>
#include <mylang/ethir/ssa/PruneUnreachableCode.h>
#include <mylang/ethir/ssa/PrunePass.h>
#include <mylang/ethir/ssa/MaintainSSAPass.h>

namespace mylang {
  namespace ethir {
    namespace ssa {

      SSAProgram::SSAProgram(ProgramPtr ssaProgram)
          : program(ssaProgram)
      {
      }

      SSAProgram::~SSAProgram()
      {
      }

      /**
       * Create the SSA form a program.
       * @param nonSSA a non-ssa program
       */
      SSAProgram::Ptr SSAProgram::createSSA(ProgramPtr nonSSA)
      {
        auto ssaForm = ToSSA::transform(nonSSA);
        if (ssaForm == nullptr) {
          return nullptr;
        }

        struct Impl: public SSAProgram
        {
          Impl(ProgramPtr ssaForm)
              : SSAProgram(ssaForm)
          {
          }
          ~Impl()
          {
          }
        };
        SSAProgram::Ptr ssa = ::std::make_unique<Impl>(ssaForm);

        // converting to SSA form introduce some artifacts that
        // need to be pruned to avoid infinite loops
        // example: unreachable code may be introduced
        // example:
        // if (x) {
        //     continue;
        // } else {
        //     continue;
        // }
        // val xN = phi(0,...,x1) // added artifact that is unreachable

        PruneUnreachableCode pruneUnreachable;
        PrunePass pruneUnused;
        MaintainSSAPass maintainSSA;

        for (bool modified = true; modified;) {
          modified = pruneUnreachable.transformProgram(*ssa);
          modified |= pruneUnused.transformProgram(*ssa);
          modified |= maintainSSA.transformProgram(*ssa);
        }

        return ssa;
      }

      bool SSAProgram::reset(ProgramPtr newPrg)
      {
        if (newPrg) {
          program = newPrg;
          nonSSA = nullptr;
          return true;
        }
        return false;
      }

      SSAProgram::ProgramPtr SSAProgram::getNonSSAForm(bool enable_cache) const
      {
        SSAProgram::ProgramPtr res = nonSSA;
        if (res == nullptr) {
          res = FromSSA::transform(program);
          if (enable_cache) {
            nonSSA = res;
          }
        }
        return res;
      }

      SSAProgram::ProgramPtr SSAProgram::getSSAForm() const
      {
        return program;
      }

    }
  }
}
