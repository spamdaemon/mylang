#ifndef CLASS_MYLANG_ETHIR_SSA_PEEPHOLE_H
#define CLASS_MYLANG_ETHIR_SSA_PEEPHOLE_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_ENVIRONMENT_H
#include <mylang/ethir/Environment.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_OPERATOREXPRESSION_H
#include <mylang/ethir/ir/OperatorExpression.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_RETURNSTATEMENT_H
#include <mylang/ethir/ir/ReturnStatement.h>
#endif

#include <vector>
#include <optional>
#include <utility>

namespace mylang {
  namespace ethir {
    namespace ssa {

      class Peephole
      {
        /** A pointer to a peephole */
      public:
        typedef ::std::shared_ptr<Peephole> Ptr;

        /** Default constructor */
      public:
        Peephole();

        /** Destructor */
      public:
        virtual ~Peephole() = 0;

        /**
         * Apply the peephole to the specified node
         * @param node a node
         * @param env a bind environment
         * @return a replacement for node or nullptr if the peephole did not apply
         */
      public:
        virtual ENode apply(const ENode &node, const Environment &env) const = 0;

        /**
         * Match a call combination.
         * @param node the node to consider
         * @param defs the definitions to lookup
         * @param op1 the operator expression that the node should have
         * @param op2 the operator expression for the i^th argument.
         * @param i the i^th argument to op1
         * @return a pair of operator expressions (first==op1, second==op2); if no match pair has nullptrs
         */
      public:
        static ::std::pair<ir::OperatorExpression::OperatorExpressionPtr,
            ir::OperatorExpression::OperatorExpressionPtr> matches(const ENode &node,
            const Environment &env, std::optional<ir::OperatorExpression::Operator> op1,
            std::optional<ir::OperatorExpression::Operator> op2, size_t i = 0);

        /**
         * Match an operator expression.
         * @param node the node to consider
         * @param op0 the operator expression that the node should have
         * @return true if there is a match
         */
      public:
        static ir::OperatorExpression::OperatorExpressionPtr matches(const ENode &node,
            std::optional<ir::OperatorExpression::Operator> op);

        /**
         * Match a call combination.
         * @param node the node to consider
         * @param defs the definitions to lookup
         * @param op1 the operator expression that the node should have
         * @param op2 the operator expression for the i^th argument.
         * @param i the i^th argument to op1
         * @return a pair of operator expressions (first==op1, second==op2); if no match pair has nullptrs
         */
      public:
        static ::std::pair<ir::OperatorExpression::OperatorExpressionPtr,
            ir::OperatorExpression::OperatorExpressionPtr> matches(const ENode &node,
            const Environment &env, const std::vector<ir::OperatorExpression::Operator> &op1,
            const std::vector<ir::OperatorExpression::Operator> &op2, size_t i = 0);

        /**
         * Match an operator expression.
         * @param node the node to consider
         * @param op0 the operator expression that the node should have
         * @return true if there is a match
         */
      public:
        static ir::OperatorExpression::OperatorExpressionPtr matches(const ENode &node,
            const std::vector<ir::OperatorExpression::Operator> &op);

        /**
         * Match upto a specified number of statements and return them in a vector.
         * NoStatement and comments are removed
         * @param s a statement
         * @param N number of statements to return
         * @return a vector of statements
         */
      public:
        static ::std::vector<EStatement> matchStatements(const EStatement &s,
            ::std::optional<size_t> N);

        /**
         * Match a return statement.
         * @param s the statement to check
         * @return a return statement or nullptr otherwise
         */
      public:
        static ir::ReturnStatement::ReturnStatementPtr matchReturn(const EStatement &s);

        // create recognizes a map with an identity function
      public:
        static Ptr createMapIdentity();

        // create a peephole that recognizes a call to a function that returns either
        // one of the arguments or a global
      public:
        static Ptr createCallWrapper();
      };
    }
  }
}
#endif
