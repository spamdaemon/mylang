#include <mylang/ethir/ethir.h>
#include <mylang/ethir/Environment.h>
#include <mylang/ethir/EnvironmentalTransform.h>
#include <mylang/ethir/ir/StatementBuilder.h>
#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/ir/OperatorExpression.h>
#include <mylang/ethir/ir/Variable.h>
#include <mylang/ethir/transforms/CopyRename.h>
#include <mylang/ethir/queries/CallGraph.h>
#include <mylang/ethir/queries/CountStatements.h>
#include <mylang/ethir/Name.h>
#include <mylang/ethir/ssa/InlinePass.h>
#include <mylang/ethir/ssa/SSATransform.h>
#include <mylang/ethir/Visitor.h>
#include <memory>
#include <optional>
#include <stdexcept>
#include <vector>
#include <cassert>

#define DEBUG_INLINE 0

namespace mylang {
  namespace ethir {
    using namespace ir;
    namespace ssa {

      namespace {

        static EStatement inlineLambda(const ELambda &lambda,
            const OperatorExpression::Arguments &args, EVariable output, EStatement next)
        {
          struct Pass: public SSATransform
          {
            Pass(EVariable xoutput, EStatement xnext)
                : output(xoutput), next(xnext)
            {
            }
            ~Pass() = default;

            ENode visit(const LambdaExpression::LambdaExpressionPtr&)
            override final
            {
              // do not traverse into a lambda function
              return nullptr;
            }

            ENode visit(const ReturnStatement::ReturnStatementPtr &node)
            override final
            {
              if (node->value) {
                return ValueDecl::create(output, node->value, next);
              } else {
                return next;
              }
            }

            const EVariable output;
            const EStatement next;
          };

          StatementBuilder b;
          for (size_t i = 0; i < lambda->parameters.size(); ++i) {
            auto param = lambda->parameters.at(i);
            auto arg = args.at(i);
            b.declareValue(param->variable, arg);
          }

          // convert the return statement in the body into an assign statement
          Pass pass(output, next);
          auto body = pass.transformStatement(lambda->body);
          return b.build(body.actual);
        }
      }

      InlinePass::InlinePass(const Predicate &xpredicate)
          : predicate(xpredicate)
      {
        if (!predicate) {
          throw ::std::invalid_argument("Missing predicate");
        }
      }

      InlinePass::~InlinePass()
      {
      }

      EProgram InlinePass::transform(EProgram src)
      {
        struct Env: public Environment
        {

          Env(::std::shared_ptr<Env> ptr, const InlinePass &xpass, const queries::CallGraph &cg)
              : Environment(ptr), pass(xpass), callgraph(cg)
          {
          }

          ~Env()
          {
          }

          Ptr create() override final
          {
            return ::std::make_shared<Env>(self<Env>(), pass, callgraph);
          }

          void addBinding(const Name &name,
              ::std::optional<mylang::ethir::ir::Variable::Scope> scope, const EExpression &value)
          {
            auto lambda = value->self<LambdaExpression>();
            if (!lambda) {
              return;
            }
            auto var = Variable::create(scope.value(), value->type, name);
            if (pass.predicate(var, lambda)) {
              if (!callgraph.isRecursive(name)) {
                if (scope.has_value()) {
#if DEBUG_INLINE == 1
                  ::std::cerr << "Function is eligible for inlining " << name << ::std::endl;
#endif
                  Environment::addBinding(name, scope, lambda);
                } else {
#if DEBUG_INLINE == 1
                  ::std::cerr << "Function has no scope " << name << ::std::endl;
#endif
                }
              } else {
#if DEBUG_INLINE == 1
                ::std::cerr << "Cannot inline recursive function " << name << ::std::endl;
#endif
              }
            } else {
#if DEBUG_INLINE == 1
              ::std::cerr << "Function does not pass predicate " << name << ::std::endl;
#endif
            }
          }

          const InlinePass &pass;
          const queries::CallGraph &callgraph;
        };

        struct Pass: public EnvironmentalTransform
        {
          Pass(const InlinePass &xpass, const queries::CallGraph &cg)
              : EnvironmentalTransform(::std::make_shared<Env>(nullptr, xpass, cg))
          {
          }
          ~Pass()
          {
          }
          ENode visit(const ir::GlobalValue::GlobalValuePtr &node)
          {
            auto res = EnvironmentalTransform::visit(node);
            if (node->value->self<LambdaExpression>()) {
              if (res) {
#if DEBUG_INLINE == 1
                ::std::cerr << "updated " << node->name << " due to inline" << ::std::endl;
#endif
              } else {
#if DEBUG_INLINE == 1
                ::std::cerr << "not updated " << node->name << " due to inline" << ::std::endl;
#endif
              }
            }

            return res;
          }

          ENode visit(const ValueDecl::ValueDeclPtr &node) override final
          {
            if (node->value) {
              auto expr = node->value->self<OperatorExpression>();
              if (expr && expr->name == OperatorExpression::OP_CALL) {
                auto lambdaRef = expr->arguments.at(0);
                auto lambda = current()->find<LambdaExpression>(lambdaRef);
                if (lambda) {
                  if (lambda->isNormalized()) {
#if DEBUG_INLINE == 1
                    ::std::cerr << "inline lambda " << lambdaRef->name << ":"
                        << lambda->type->toString() << ::std::endl;
#endif
                    lambda = transforms::CopyRename::copy(lambda)->self<LambdaExpression>();
                    // we're going to inline this lambda
                    auto retTy = lambda->type->self<types::FunctionType>()->returnType;
                    OperatorExpression::Arguments args(expr->arguments.begin() + 1,
                        expr->arguments.end());
                    EStatement inlinedLambda;

                    if (retTy->isVoid()) {
                      inlinedLambda = inlineLambda(lambda, args, nullptr, node->next);
                    } else {
                      assert(retTy->isSameType(*node->variable->type));
                      inlinedLambda = inlineLambda(lambda, args, node->variable, node->next);
                    }
                    return transformStatement(inlinedLambda).actual;
                  } else {
#if DEBUG_INLINE == 1
                    ::std::cerr << "cannot inline un-normalized lambda " << lambdaRef->name << ":"
                        << lambda->type->toString() << ::std::endl;
#endif
                  }
                } else {
#if DEBUG_INLINE == 1
                  ::std::cerr << "not a lambda expression " << lambdaRef->name << ":"
                      << ::std::endl;
#endif
                }
              }
            }

            return EnvironmentalTransform::visit(node);
          }
        }
        ;

        auto callgraph = queries::CallGraph::create(src);
        Pass pass(*this, *callgraph);
        ENode res = pass.visitNode(src);
        return res ? res->self<Program>() : nullptr;
      }

      bool InlinePass::isSimpleFunction(const EVariable &name,
          const mylang::ethir::ir::LambdaExpression::LambdaExpressionPtr &ptr, size_t nMax)
      {
        auto s = ptr->body->statements;
        if (s == nullptr) {
          return true;
        }
        if (name->scope == Variable::Scope::FUNCTION_SCOPE) {
#if DEBUG_INLINE == 1
          ::std::cerr << "Function is in function scope " << name->name << ::std::endl;
#endif
          return false;
        }
        size_t n = queries::CountStatements::countStatements(ptr->body);
#if DEBUG_INLINE == 1
        if (n > nMax) {
          ::std::cerr << "Function too large " << name->name << ", size = " << n << ::std::endl;
        } else {
          ::std::cerr << "Function is simple " << name->name << ::std::endl;
        }
#endif
        return n <= nMax;
      }
    }
  }
}
