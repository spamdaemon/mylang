#ifndef CLASS_MYLANG_ETHIR_SSA_PRUNEUNREACHABLECODE_H
#define CLASS_MYLANG_ETHIR_SSA_PRUNEUNREACHABLECODE_H

#ifndef CLASS_MYLANG_ETHIR_SSA_SSAPASS_H
#include <mylang/ethir/ssa/SSAPass.h>
#endif

namespace mylang {
  namespace ethir {
    namespace ssa {

      /**
       * Remove unreachable code.
       */
      class PruneUnreachableCode: public SSAPass
      {
      public:
        PruneUnreachableCode();

        /** Destructor */
      public:
        virtual ~PruneUnreachableCode();

        /**
         * Apply this transform the specified node which is in ssa form.
         * @param node a node
         */
      protected:
        EProgram transform(EProgram src) override final;
      };
    }

  }
}
#endif
