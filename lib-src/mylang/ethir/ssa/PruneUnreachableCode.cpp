#include <mylang/ethir/ssa/PruneUnreachableCode.h>
#include <mylang/ethir/queries/FindDefinedVariables.h>
#include <mylang/ethir/ssa/MaintainSSAPass.h>
#include <mylang/ethir/queries/SideEffects.h>
#include <mylang/ethir/Environment.h>
#include <mylang/ethir/ssa/SSATransform.h>

#include <cassert>
#include <map>
#include <memory>

namespace mylang {
  namespace ethir {
    using namespace queries;
    using namespace ir;
    namespace ssa {

      namespace {
        static ENode cleanupPhiNodes(ENode src)
        {
          struct CleanupPass: public SSATransform
          {
            CleanupPass(const FindDefinedVariables::Names &xnames)
                : names(xnames)
            {
            }

            ~CleanupPass()
            {
            }

            ENode visit(const ir::Phi::PhiPtr &node) override
            {
              Phi::Arguments args;

              for (auto arg : node->arguments) {
                if (names.count(arg->name) > 0) {
                  args.push_back(arg);
                }
              }
              if (args.size() > 0 && args.size() != node->arguments.size()) {
                return Phi::create(args);
              }
              return nullptr;
            }

            const FindDefinedVariables::Names &names;
          };

          if (src) {
            auto active = FindDefinedVariables::find(src);
            CleanupPass cleanup(active);
            auto res = cleanup.visitNode(src);
            if (res) {
              return res;
            }
          }
          return src;
        }
      }

      PruneUnreachableCode::PruneUnreachableCode()
      {
      }

      PruneUnreachableCode::~PruneUnreachableCode()
      {
      }

      EProgram PruneUnreachableCode::transform(EProgram src)
      {
        enum Termination
        {
          NONE = 0, RETURN = 1, THROW = 2, BREAK = 4, CONTINUE = 8, ABORT = 16, UNBOUNDED_LOOP = 32
        };
        struct Info
        {
          Info()
              : termination(NONE)
          {
          }

          /** The targets of a break statement */
          ::std::set<Name> breakTargets;

          /** True if statement sequence will terminate with a continue, return, throw, or break statement. */
          int termination;
        };

        struct Pass: public SSATransform
        {
          Pass()
              : info(::std::make_shared<Info>())
          {
          }

          ~Pass()
          {
          }

          ENode visit(const ir::LambdaExpression::LambdaExpressionPtr &node) override
          {
            auto restore = ::std::move(info);
            info = ::std::make_shared<Info>();
            auto body = Transform::visit(node);
            info = ::std::move(restore);

            return body;
          }

          ENode visit(const ir::BreakStatement::BreakStatementPtr &node) override
          {
            info->breakTargets.insert(node->scope);
            info->termination |= BREAK;
            return nullptr;
          }

          ENode visit(const ir::AbortStatement::AbortStatementPtr&) override
          {
            info->termination |= ABORT;
            return nullptr;
          }

          ENode visit(const ir::ContinueStatement::ContinueStatementPtr&) override
          {
            info->termination |= CONTINUE;
            return nullptr;
          }

          ENode visit(const ir::ThrowStatement::ThrowStatementPtr&) override
          {
            info->termination |= THROW;
            return nullptr;
          }

          ENode visit(const ir::ReturnStatement::ReturnStatementPtr&) override
          {
            info->termination |= RETURN;
            return nullptr;
          }

          ENode visit(const ir::IfStatement::IfStatementPtr &node) override
          {
            auto restore = ::std::move(info);
            auto iftrueInfo = ::std::make_shared<Info>();
            info = iftrueInfo;
            auto iftrue = transformStatement(node->iftrue);

            auto iffalseInfo = ::std::make_shared<Info>();
            info = iffalseInfo;
            auto iffalse = transformStatement(node->iffalse);
            info = ::std::move(restore);

            info->breakTargets.insert(iftrueInfo->breakTargets.begin(),
                iftrueInfo->breakTargets.end());
            info->breakTargets.insert(iffalseInfo->breakTargets.begin(),
                iffalseInfo->breakTargets.end());

            if (iftrueInfo->termination == NONE || iffalseInfo->termination == NONE) {
              info->termination = NONE;
            } else {
              info->termination = iftrueInfo->termination | iffalseInfo->termination;
            }

            if (info->termination && node->next) {
              return IfStatement::create(node->condition, iftrue.actual, iffalse.actual, nullptr);
            } else {
              auto next = transformStatement(node->next);
              if (next.isNew || iffalse.isNew || iftrue.isNew) {
                return IfStatement::create(node->condition, iftrue.actual, iffalse.actual,
                    next.actual);
              }
            }

            return nullptr;
          }

          ENode visit(const ir::TryCatch::TryCatchPtr &node) override
          {
            auto restore = ::std::move(info);
            auto tryInfo = ::std::make_shared<Info>();
            info = tryInfo;
            auto tryBlock = transformStatement(node->tryBody);

            auto catchInfo = ::std::make_shared<Info>();
            info = catchInfo;
            auto catchBlock = transformStatement(node->catchBody);
            info = ::std::move(restore);
            info->breakTargets.insert(tryInfo->breakTargets.begin(), tryInfo->breakTargets.end());
            info->breakTargets.insert(catchInfo->breakTargets.begin(),
                catchInfo->breakTargets.end());

            if (tryInfo->termination == NONE || catchInfo->termination == NONE) {
              info->termination = NONE;
            } else {
              info->termination = tryInfo->termination | catchInfo->termination;
            }

            if (info->termination && (node->next || tryBlock.isNew || catchBlock.isNew)) {
              return TryCatch::create(tryBlock.actual, catchBlock.actual, nullptr);
            } else {
              auto next = transformStatement(node->next);
              if (next.isNew || tryBlock.isNew || catchBlock.isNew) {
                return TryCatch::create(tryBlock.actual, catchBlock.actual, next.actual);

              }
            }
            return nullptr;
          }

          ENode visit(const ir::ForeachStatement::ForeachStatementPtr &node) override
          {
            auto restore = ::std::move(info);
            auto loopInfo = ::std::make_shared<Info>();
            info = loopInfo;
            auto body = transformStatement(node->body);
            info = ::std::move(restore);

            info->breakTargets.insert(loopInfo->breakTargets.begin(), loopInfo->breakTargets.end());
            info->termination = NONE;
            auto next = transformStatement(node->next);
            if (body.isNew || next.isNew) {
              return ForeachStatement::create(node->name, node->variable, node->data, body.actual,
                  next.actual);
            }
            return nullptr;
          }

          ENode visit(const ir::LoopStatement::LoopStatementPtr &node) override
          {
            auto restore = ::std::move(info);
            auto loopInfo = ::std::make_shared<Info>();
            info = loopInfo;
            auto body = transformStatement(node->body);
            info = ::std::move(restore);

            info->breakTargets.insert(loopInfo->breakTargets.begin(), loopInfo->breakTargets.end());

            if (loopInfo->breakTargets.count(node->name) != 0) {
              info->termination = NONE;
              auto next = transformStatement(node->next);
              if (body.isNew || next.isNew) {
                return LoopStatement::create(node->name, body.actual, next.actual);
              } else {
                return nullptr;
              }
            }

            // loops without break statements will never terminate
            info->termination = UNBOUNDED_LOOP;
            if (node->next || body.isNew) {
              return LoopStatement::create(node->name, body.actual, nullptr);
            }
            return nullptr;
          }

          ENode visit(const ir::StatementBlock::StatementBlockPtr &node) override
          {
            auto restore = ::std::move(info);
            auto blockInfo = ::std::make_shared<Info>();
            info = blockInfo;
            auto body = transformStatement(node->statements);
            info = ::std::move(restore);

            info->breakTargets.insert(blockInfo->breakTargets.begin(),
                blockInfo->breakTargets.end());
            info->termination = blockInfo->termination;

            if (info->termination && node->next) {

              return StatementBlock::create(body.actual, nullptr);
            } else {
              auto next = transformStatement(node->next);
              if (next.isNew || body.isNew) {
                return StatementBlock::create(body.actual, next.actual);
              }
            }
            return nullptr;
          }

          /** The current info details */
          ::std::shared_ptr<Info> info;

        };

        Pass pass;
        auto res = pass.visitNode(src);
        // we may have deleted variable declarations that are being referred to
        // in phi nodes above (e.g. when looping)
//        res = cleanupPhiNodes(res);

        return res ? res->self<Program>() : nullptr;

      }

    }

  }
}
