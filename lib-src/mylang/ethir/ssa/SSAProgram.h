#ifndef CLASS_MYLANG_ETHIR_SSA_SSAPROGRAM_H
#define CLASS_MYLANG_ETHIR_SSA_SSAPROGRAM_H

#ifndef CLASS_MYLANG_ETHIR_IR_PROGRAM_H
#include <mylang/ethir/ir/Program.h>
#endif

#include <memory>

namespace mylang {
  namespace ethir {
    namespace ssa {

      /**
       * A variable declaration. Variables are mutable!
       */
      class SSAProgram
      {
        friend class SSAPass;

      public:
        typedef ::std::unique_ptr<SSAProgram> Ptr;

      public:
        typedef mylang::ethir::ir::Program::ProgramPtr ProgramPtr;

        /**
         * Create a program
         * @param functions the global functions
         * @param values the global values
         * @param processes the defined processes
         */
      protected:
        SSAProgram(ProgramPtr ssaProgram);

        /** Destructor */
      public:
        virtual ~SSAProgram();

        /**
         * Create the SSA form a program.
         * @param nonSSA a non-ssa program
         */
      public:
        static Ptr createSSA(ProgramPtr nonSSA);

        /**
         * Convert this program to non-ssa form.
         * @param enableCache if true, the result is locally cached
         * @return the non-ssa form
         */
      public:
        ProgramPtr getNonSSAForm(bool enableCache = false) const;

        /**
         * Get the SSA form of the program.
         * @return the ssa form of the program
         */
      public:
        ProgramPtr getSSAForm() const;

        /**
         * Reset the program.
         * @param newPrg
         * @return false if newPrg==nullptr, true otherwise
         */
      private:
        bool reset(ProgramPtr newPrg);

        /** The program in SSA form */
      private:
        ProgramPtr program;

        /** The non-ssa form */
      private:
        mutable ProgramPtr nonSSA;
      };
    }

  }
}
#endif
