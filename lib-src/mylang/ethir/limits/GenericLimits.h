#ifndef CLASS_MYLANG_ETHIR_LIMITS_GENERICLIMITS_H
#define CLASS_MYLANG_ETHIR_LIMITS_GENERICLIMITS_H

#ifndef CLASS_MYLANG_ETHIR_LIMITS_LIMITS_H
#include <mylang/ethir/limits/Limits.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_LIMITS_INTEGERLIMITS_H
#include <mylang/ethir/limits/IntegerLimits.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_LIMITS_BOOLEANLIMITS_H
#include <mylang/ethir/limits/BooleanLimits.h>
#endif

#include <vector>

namespace mylang {
  namespace ethir {
    namespace limits {

      class GenericLimits: public Limits
      {
      public:
        typedef ::std::shared_ptr<const GenericLimits> Ptr;

        /**
         * Constructor
         */
      protected:
        GenericLimits(::std::vector<LimitsPtr> objects);

        /** Destructor */
      public:
        ~GenericLimits();

      public:
        static Ptr get(::std::vector<LimitsPtr> members);
        static Ptr get(LimitsPtr value);

      public:
        bool isUnbounded() const override final;
        LimitsPtr merge(const LimitsPtr &other) const override final;
        ::std::shared_ptr<const BooleanLimits> compareEQ(const LimitsPtr &other,
            const EExpression &expr) const override final;
        ::std::shared_ptr<const BooleanLimits> compareLT(const LimitsPtr &other,
            const EExpression &expr) const override final;

      public:
        const ::std::vector<LimitsPtr> objects;
      };

    }
  }
}
#endif
