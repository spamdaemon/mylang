#ifndef CLASS_MYLANG_ETHIR_LIMITS_OPTLIMITS_H
#define CLASS_MYLANG_ETHIR_LIMITS_OPTLIMITS_H

#ifndef CLASS_MYLANG_ETHIR_LIMITS_LIMITS_H
#include <mylang/ethir/limits/Limits.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_LIMITS_INTEGERLIMITS_H
#include <mylang/ethir/limits/IntegerLimits.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_LIMITS_BOOLEANLIMITS_H
#include <mylang/ethir/limits/BooleanLimits.h>
#endif

namespace mylang {
  namespace ethir {
    namespace limits {

      class OptLimits: public Limits
      {
      public:
        typedef ::std::shared_ptr<const OptLimits> Ptr;

        /**
         * Constructor
         */
      protected:
        OptLimits(BooleanLimits::Ptr present, LimitsPtr element);

        /** Destructor */
      public:
        ~OptLimits();

      public:
        static Ptr get(BooleanLimits::Ptr present, LimitsPtr element);
        static Ptr getNil();

      public:
        bool isUnbounded() const override final;
        LimitsPtr merge(const LimitsPtr &other) const override final;
        ::std::shared_ptr<const BooleanLimits> compareEQ(const LimitsPtr &other,
            const EExpression &expr) const override final;
        ::std::shared_ptr<const BooleanLimits> compareLT(const LimitsPtr &other,
            const EExpression &expr) const override final;
        ELiteral getLiteral(const EType &ty) const override final;

      public:
        const BooleanLimits::Ptr present;

      public:
        const LimitsPtr element;
      };

    }
  }
}
#endif
