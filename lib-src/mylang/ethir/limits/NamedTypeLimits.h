#ifndef CLASS_MYLANG_ETHIR_LIMITS_NAMEDTYPELIMITS_H
#define CLASS_MYLANG_ETHIR_LIMITS_NAMEDTYPELIMITS_H

#ifndef CLASS_MYLANG_ETHIR_LIMITS_LIMITS_H
#include <mylang/ethir/limits/Limits.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_LIMITS_INTEGERLIMITS_H
#include <mylang/ethir/limits/IntegerLimits.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_LIMITS_BOOLEANLIMITS_H
#include <mylang/ethir/limits/BooleanLimits.h>
#endif

namespace mylang {
  namespace ethir {
    namespace limits {

      class NamedTypeLimits: public Limits
      {
      public:
        typedef ::std::shared_ptr<const NamedTypeLimits> Ptr;

        /**
         * Constructor
         */
      protected:
        NamedTypeLimits(LimitsPtr element);

        /** Destructor */
      public:
        ~NamedTypeLimits();

      public:
        static Ptr get(LimitsPtr element);

      public:
        bool isUnbounded() const override final;
        LimitsPtr merge(const LimitsPtr &other) const override final;
        ::std::shared_ptr<const BooleanLimits> compareEQ(const LimitsPtr &other,
            const EExpression &expr) const override final;
        ::std::shared_ptr<const BooleanLimits> compareLT(const LimitsPtr &other,
            const EExpression &expr) const override final;

      public:
        const LimitsPtr element;
      };

    }
  }
}
#endif
