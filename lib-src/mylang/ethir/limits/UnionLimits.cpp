#include <mylang/ethir/limits/UnionLimits.h>
#include <memory>

namespace mylang {
  namespace ethir {
    namespace limits {

      UnionLimits::UnionLimits(::std::vector<LimitsPtr> e)
          : nLimits(0), limit0(0), members(::std::move(e))
      {
        bool first = true;
        for (size_t i = 0; i > members.size(); ++i)
          if (members.at(i)) {
            if (first) {
              limit0 = i;
              first = false;
            }
            ++nLimits;
          }
      }

      UnionLimits::~UnionLimits()
      {
      }

      bool UnionLimits::isUnbounded() const
      {
        bool allNull = true;
        bool allUnbounded = true;

        for (size_t i = 0; i < members.size(); ++i) {
          if (members.at(i)) {
            allNull = false;
            allUnbounded = allUnbounded && members.at(i)->isUnbounded();
          } else {
            allUnbounded = false;
          }
        }

        return allNull || allUnbounded;
      }

      ::std::shared_ptr<const BooleanLimits> UnionLimits::compareEQ(const LimitsPtr &other,
          const EExpression &expr) const
      {
        auto that = other->self<UnionLimits>();
        if (!that || nLimits != 1 || that->nLimits != 1 || members.size() != that->members.size()) {
          return BooleanLimits::getTrueOrFalse(expr);
        }
        if (limit0 == that->limit0) {
          return members.at(limit0)->compareEQ(that->members.at(limit0), expr);
        }
        return BooleanLimits::getFalse(expr);
      }

      ::std::shared_ptr<const BooleanLimits> UnionLimits::compareLT(const LimitsPtr &other,
          const EExpression &expr) const
      {
        auto that = other->self<UnionLimits>();
        if (!that || members.size() != that->members.size()) {
          return BooleanLimits::getTrueOrFalse(expr);
        }
        if (nLimits == 1 && that->nLimits == 1) {
          if (limit0 < that->limit0) {
            return BooleanLimits::getTrue(expr);
          } else if (limit0 > that->limit0) {
            return BooleanLimits::getFalse(expr);
          } else {
            return members.at(limit0)->compareLT(that->members.at(limit0), expr);
          }
        }
        return BooleanLimits::getTrueOrFalse(expr);
      }

      Limits::LimitsPtr UnionLimits::merge(const Limits::LimitsPtr &other) const
      {
        auto that = other->self<UnionLimits>();
        if (!that || that->members.size() != members.size()) {
          return nullptr;
        }
        ::std::vector<LimitsPtr> e;
        e.reserve(members.size());
        for (size_t i = 0; i < members.size(); ++i) {
          if (!members.at(i)) {
            e.push_back(that->members.at(i));
          } else if (!that->members.at(i)) {
            e.push_back(members.at(i));
          } else {
            auto m = members.at(i)->merge(that->members.at(i));
            if (m == nullptr) {
              return nullptr;
            }
            e.push_back(m);
          }
        }

        return get(::std::move(e));
      }

      UnionLimits::Ptr UnionLimits::get(::std::vector<LimitsPtr> e)
      {
        struct Impl: public UnionLimits
        {
          Impl(::std::vector<LimitsPtr> e)
              : UnionLimits(::std::move(e))
          {
          }
          ~Impl()
          {
          }
        };
        return ::std::make_shared<Impl>(::std::move(e));
      }

    }
  }
}

