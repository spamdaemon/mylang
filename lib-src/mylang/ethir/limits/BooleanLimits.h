#ifndef CLASS_MYLANG_ETHIR_LIMITS_BOOLEANLIMITS_H
#define CLASS_MYLANG_ETHIR_LIMITS_BOOLEANLIMITS_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_LIMITS_LIMITS_H
#include <mylang/ethir/limits/Limits.h>
#endif

#include <optional>
#include <set>

namespace mylang {
  namespace ethir {
    namespace limits {

      class BooleanLimits: public Limits
      {
      public:
        typedef ::std::shared_ptr<const BooleanLimits> Ptr;

        /**
         * Constructor
         */
      protected:
        BooleanLimits(::std::optional<bool> xlimits, ::std::set<EExpression> exprs);

        /** Destructor */
      public:
        ~BooleanLimits();

        /**
         *  Perform a not of this limit.
         */
      public:
        BooleanLimits::Ptr NOT() const;

        /**
         * The AND of two boolean limits.
         * @param a a limit
         * @param b a limit
         * @return a boolean limit
         */
      public:
        static BooleanLimits::Ptr AND(const BooleanLimits::Ptr &a, const BooleanLimits::Ptr &b);

        /**
         * Determine if this limit contains the specified boolean value
         * @param c a value
         */
      public:
        inline bool contains(bool value) const
        {
          return !bounds.has_value() || bounds.value() == value;
        }

        /**
         * Determine if this limit contains only the specified boolean value
         * @param c a value
         */
      public:
        inline bool containsOnly(bool value) const
        {
          return bounds.has_value() && bounds.value() == value;
        }

        /**
         * Create the limit that contains only true
         */
      public:
        inline static Ptr get(bool b, const EExpression &expr = nullptr)
        {
          return b ? getTrue(expr) : getFalse(expr);
        }

        /**
         * Create the limit that contains only true
         */
      public:
        static Ptr getTrue(::std::set<EExpression> exprs);

        /**
         * Create the limit that contains only false
         */
      public:
        static Ptr getFalse(::std::set<EExpression> exprs);

        /**
         * Create the limit that contains true or false for the given expression.
         * @param expr an expression
         */
      public:
        static Ptr getTrueOrFalse(::std::set<EExpression> exprs);

        /**
         * Create the limit that contains only true
         */
      public:
        static Ptr getTrue(const EExpression &expr);

        /**
         * Create the limit that contains only false
         */
      public:
        static Ptr getFalse(const EExpression &expr);

        /**
         * Create the limit that contains true or false for the given expression.
         * @param expr an expression
         */
      public:
        static Ptr getTrueOrFalse(const EExpression &expr);

        /**
         * Create the limit that contains only true
         */
      public:
        inline static Ptr getTrue()
        {
          return getTrue(nullptr);
        }

        /**
         * Create the limit that contains only false
         */
      public:
        inline static Ptr getFalse()
        {
          return getFalse(nullptr);
        }

        /**
         * Create the limit that contains true or false for the given expression.
         * @param expr an expression
         */
      public:
        inline static Ptr getTrueOrFalse()
        {
          return getTrueOrFalse(nullptr);
        }

      public:
        bool isUnbounded() const override final;
        LimitsPtr merge(const LimitsPtr &other) const override final;
        ::std::shared_ptr<const BooleanLimits> compareEQ(const LimitsPtr &other,
            const EExpression &expr) const override final;
        ::std::shared_ptr<const BooleanLimits> compareLT(const LimitsPtr &other,
            const EExpression &expr) const override final;
        ELiteral getLiteral(const EType &ty) const override final;

        /** Drop the expressions from this limits object and get an other limits object */
      private:
        Ptr dropExpressions() const;

      private:
        ::std::optional<bool> bounds;

        /** If we assume that the result of boolean is true, then all implications are also true */
      public:
        const ::std::set<EExpression> iftrue;
      };

    }
  }
}
#endif
