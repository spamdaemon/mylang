#include <mylang/ethir/ir/Expression.h>
#include <mylang/ethir/limits/ArrayLimits.h>
#include <mylang/ethir/limits/BooleanLimits.h>
#include <mylang/ethir/limits/GenericLimits.h>
#include <mylang/ethir/limits/IntegerLimits.h>
#include <mylang/ethir/limits/Limits.h>
#include <mylang/ethir/limits/NamedTypeLimits.h>
#include <mylang/ethir/limits/OptLimits.h>
#include <mylang/ethir/limits/StructLimits.h>
#include <mylang/ethir/limits/TupleLimits.h>
#include <mylang/ethir/limits/UnionLimits.h>
#include <mylang/ethir/types/ArrayType.h>
#include <mylang/ethir/types/BooleanType.h>
#include <mylang/ethir/types/GenericType.h>
#include <mylang/ethir/types/IntegerType.h>
#include <mylang/ethir/types/NamedType.h>
#include <mylang/ethir/types/OptType.h>
#include <mylang/ethir/types/StructType.h>
#include <mylang/ethir/types/TupleType.h>
#include <mylang/ethir/types/Type.h>
#include <mylang/ethir/types/UnionType.h>
#include <stdexcept>
#include <vector>

namespace mylang {
  namespace ethir {
    namespace limits {
      using namespace types;

      namespace {
        struct TypeLimits: public Limits
        {
          TypeLimits(EType xtype)
              : type(xtype)
          {
          }
          ~TypeLimits()
          {
          }

          bool isUnbounded() const override
          {
            return true;
          }
          LimitsPtr merge(const LimitsPtr &other) const override
          {
            auto that = other->self<TypeLimits>();
            if (that) {
              if (type->isSameType(*that->type)) {
                return that;
              }
            }
            return nullptr;
          }

          ::std::shared_ptr<const BooleanLimits> compareEQ(const LimitsPtr &other,
              const EExpression &expr) const override
          {
            auto that = other->self<TypeLimits>();
            if (that) {
              return BooleanLimits::getTrueOrFalse(expr);
            }
            return nullptr;
          }

          ::std::shared_ptr<const BooleanLimits> compareLT(const LimitsPtr &other,
              const EExpression &expr) const override
          {
            auto that = other->self<TypeLimits>();
            if (that) {
              return BooleanLimits::getTrueOrFalse(expr);
            }
            return nullptr;
          }

          const EType type;
        };

      }

      Limits::Limits()
      {
      }

      Limits::~Limits()
      {
      }

      Limits::LimitsPtr Limits::of(const EType &type)
      {
        TypeSet types;
        return of(type, types);
      }

      Limits::LimitsPtr Limits::of(const EType &type, TypeSet &types)
      {
        auto xty = types.add(type);
        if (xty != type) {
          if (xty->self<NamedType>()) {
            return ::std::make_shared<TypeLimits>(type);
          }
        }
        if (type->self<ArrayType>()) {
          auto ty = type->self<ArrayType>();
          return ArrayLimits::get(of(ty->lengthType, types)->self<IntegerLimits>(),
              of(ty->element, types));
        } else if (type->self<BooleanType>()) {
          return BooleanLimits::getTrueOrFalse(nullptr);
        } else if (type->self<GenericType>()) {
          return GenericLimits::get(nullptr);
        } else if (type->self<IntegerType>()) {
          auto ty = type->self<IntegerType>();
          return IntegerLimits::get(ty->range);
        } else if (type->self<NamedType>()) {
          auto ty = type->self<NamedType>();
          return NamedTypeLimits::get(of(ty->getRecursiveReference(), types));
        } else if (type->self<OptType>()) {
          auto ty = type->self<OptType>();
          return OptLimits::get(BooleanLimits::getTrueOrFalse(nullptr), of(ty->element, types));
        } else if (type->self<StructType>()) {
          auto ty = type->self<StructType>();
          ::std::vector<LimitsPtr> e;
          for (auto m : ty->members) {
            e.push_back(of(m.type, types));
          }
          return StructLimits::get(e);
        } else if (type->self<TupleType>()) {
          auto ty = type->self<TupleType>();
          ::std::vector<LimitsPtr> e;
          for (auto m : ty->types) {
            e.push_back(of(m, types));
          }
          return TupleLimits::get(e);
        }
        if (type->self<UnionType>()) {
          auto ty = type->self<UnionType>();
          ::std::vector<LimitsPtr> e;
          for (auto m : ty->members) {
            e.push_back(of(m.type, types));
          }
          return UnionLimits::get(e);
        }

        return ::std::make_shared<TypeLimits>(type);
      }

      Limits::LimitsPtr Limits::merge(const LimitsPtr &a, const LimitsPtr &b)
      {
        if (a == nullptr) {
          return b;
        }
        if (b == nullptr) {
          return a;
        }
        if (a == b) {
          return a;
        }
        return a->merge(b);
      }

      BooleanLimits::Ptr Limits::compareEQ(const LimitsPtr &a, const LimitsPtr &b,
          const EExpression &expr)
      {
        if (expr && !expr->type->self<types::BooleanType>()) {
          throw ::std::runtime_error("Not a boolean expression");
        }

        if (a && b) {
          return a->compareEQ(b, expr);
        }
        return BooleanLimits::getTrueOrFalse(expr);
      }

      BooleanLimits::Ptr Limits::compareLT(const LimitsPtr &a, const LimitsPtr &b,
          const EExpression &expr)
      {
        if (expr && !expr->type->self<types::BooleanType>()) {
          throw ::std::runtime_error("Not a boolean expression");
        }
        if (a && b) {
          return a->compareLT(b, expr);
        }
        return BooleanLimits::getTrueOrFalse(expr);
      }

      ELiteral Limits::getLiteral(const EType&) const
      {
        return nullptr;
      }
    }
  }
}

