#ifndef CLASS_MYLANG_ETHIR_LIMITS_INTEGERLIMITS_H
#define CLASS_MYLANG_ETHIR_LIMITS_INTEGERLIMITS_H

#include <memory>

#include "../../BigInt.h"
#include "../ethir.h"

#ifndef CLASS_MYLANG_ETHIR_LIMITS_LIMITS_H
#include <mylang/ethir/limits/Limits.h>
#endif

#ifndef CLASS_MYLANG_INTERVAL_H
#include <mylang/Interval.h>
#endif

#include <optional>

namespace mylang {
  namespace ethir {
    namespace limits {

      class IntegerLimits: public Limits
      {
      public:
        typedef ::std::shared_ptr<const IntegerLimits> Ptr;

      private:
        using Range = Interval;

        /**
         * Constructor
         */
      protected:
        IntegerLimits(Range xlim);

        /** Destructor */
      public:
        ~IntegerLimits();

        /**
         * Create limits defined by the specified range.
         * @param value a range
         */
      public:
        static Ptr get(const Range &value);

      public:
        bool isUnbounded() const override final;
        LimitsPtr merge(const LimitsPtr &other) const override final;
        ::std::shared_ptr<const BooleanLimits> compareEQ(const LimitsPtr &other,
            const EExpression &expr) const override final;
        ::std::shared_ptr<const BooleanLimits> compareLT(const LimitsPtr &other,
            const EExpression &expr) const override final;
        ELiteral getLiteral(const EType &ty) const override final;

      public:
        const Range bounds;
      };

    }
  }
}
#endif
