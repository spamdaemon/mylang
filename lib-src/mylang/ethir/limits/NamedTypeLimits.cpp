#include <mylang/ethir/limits/NamedTypeLimits.h>

namespace mylang {
  namespace ethir {
    namespace limits {

      NamedTypeLimits::NamedTypeLimits(LimitsPtr e)
          : element(::std::move(e))
      {
      }

      NamedTypeLimits::~NamedTypeLimits()
      {
      }

      bool NamedTypeLimits::isUnbounded() const
      {
        return element->isUnbounded();
      }

      ::std::shared_ptr<const BooleanLimits> NamedTypeLimits::compareEQ(const LimitsPtr &other,
          const EExpression &expr) const
      {
        auto that = other->self<NamedTypeLimits>();
        if (that) {
          return element->compareEQ(that->element, nullptr);
        }
        return BooleanLimits::getTrueOrFalse(expr);
      }

      ::std::shared_ptr<const BooleanLimits> NamedTypeLimits::compareLT(const LimitsPtr &other,
          const EExpression &expr) const
      {
        auto that = other->self<NamedTypeLimits>();
        if (that) {
          return element->compareLT(that->element, expr);
        }
        return BooleanLimits::getTrueOrFalse(expr);
      }

      Limits::LimitsPtr NamedTypeLimits::merge(const Limits::LimitsPtr &other) const
      {
        auto that = other->self<NamedTypeLimits>();
        if (that) {

          auto e = element->merge(that->element);
          if (e) {
            return get(e);
          }
        }
        return nullptr;
      }

      NamedTypeLimits::Ptr NamedTypeLimits::get(LimitsPtr e)
      {
        struct Impl: public NamedTypeLimits
        {
          Impl(LimitsPtr e)
              : NamedTypeLimits(::std::move(e))
          {
          }
          ~Impl()
          {
          }
        };
        return ::std::make_shared<Impl>(::std::move(e));
      }

    }
  }
}

