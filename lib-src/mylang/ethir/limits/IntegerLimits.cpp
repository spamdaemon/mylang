#include <mylang/BigInt.h>
#include <mylang/ethir/ir/LiteralInteger.h>
#include <mylang/ethir/limits/BooleanLimits.h>
#include <mylang/ethir/limits/IntegerLimits.h>
#include <memory>

namespace mylang {
  namespace ethir {
    namespace limits {

      IntegerLimits::IntegerLimits(Range xlimits)
          : bounds(xlimits)
      {
      }

      IntegerLimits::~IntegerLimits()
      {
      }

      bool IntegerLimits::isUnbounded() const
      {
        return bounds.isInfinite();
      }

      ELiteral IntegerLimits::getLiteral(const EType &ty) const
      {
        ELiteral res;
        if (ty->self<types::IntegerType>()) {
          if (bounds.isFixed()) {
            res = ir::LiteralInteger::create(*bounds.min());
            if (ty->canSafeCastFrom(*res->type)) {
              res = res->castTo(ty);
            } else {
              res = nullptr;
            }
          }
        }
        return res;
      }

      ::std::shared_ptr<const BooleanLimits> IntegerLimits::compareEQ(const LimitsPtr &other,
          const EExpression &expr) const
      {
        auto that = other->self<IntegerLimits>();
        if (that) {
          if (bounds.isFixed() && that->bounds.isFixed()) {
            return BooleanLimits::get(bounds == that->bounds, expr);
          }

          auto res = bounds.intersectWith(that->bounds);
          if (!res.has_value()) {
            return BooleanLimits::getFalse(expr);
          }
        }
        return BooleanLimits::getTrueOrFalse(expr);
      }

      ::std::shared_ptr<const BooleanLimits> IntegerLimits::compareLT(const LimitsPtr &other,
          const EExpression &expr) const
      {
        auto that = other->self<IntegerLimits>();
        if (that) {
          if (bounds.max() < that->bounds.min()) {
            return BooleanLimits::getTrue(expr);
          }
          if (bounds.min() > that->bounds.max()) {
            return BooleanLimits::getFalse(expr);
          }
        }
        return BooleanLimits::getTrueOrFalse(expr);
      }

      Limits::LimitsPtr IntegerLimits::merge(const Limits::LimitsPtr &other) const
      {
        auto that = other->self<IntegerLimits>();
        if (!that) {
          return nullptr;
        }
        if (bounds == that->bounds) {
          return that;
        }
        return get(bounds.unionWith(that->bounds));
      }

      IntegerLimits::Ptr IntegerLimits::get(const Range &value)
      {
        struct Impl: public IntegerLimits
        {
          Impl(const Range &value)
              : IntegerLimits(value)
          {
          }
          ~Impl()
          {
          }
        };
        return ::std::make_shared<Impl>(value);
      }

    }
  }
}

