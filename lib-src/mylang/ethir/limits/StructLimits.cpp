#include <mylang/ethir/limits/StructLimits.h>

namespace mylang {
  namespace ethir {
    namespace limits {

      StructLimits::StructLimits(::std::vector<LimitsPtr> e)
          : members(::std::move(e))
      {
      }

      StructLimits::~StructLimits()
      {
      }

      bool StructLimits::isUnbounded() const
      {
        for (auto e : members) {
          if (e->isUnbounded()) {
            return true;
          }
        }
        return false;
      }

      ::std::shared_ptr<const BooleanLimits> StructLimits::compareEQ(const LimitsPtr &other,
          const EExpression &expr) const
      {
        auto that = other->self<StructLimits>();
        if (!that || that->members.size() != members.size()) {
          return BooleanLimits::getTrueOrFalse(expr);
        }
        BooleanLimits::Ptr lim = BooleanLimits::getTrue(expr);
        for (size_t i = 0; i < members.size(); ++i) {
          if (members.at(i) && that->members.at(i)) {
            auto t = members.at(i)->compareEQ(that->members.at(i), nullptr);
            if (t->containsOnly(false)) {
              lim = BooleanLimits::getFalse(expr);
              break;
            } else if (t->isUnbounded()) {
              lim = BooleanLimits::getTrueOrFalse(expr);
            } else {
              // keep the current limit
            }
          } else {
            lim = BooleanLimits::getTrueOrFalse(expr);
          }
        }
        return lim;
      }

      ::std::shared_ptr<const BooleanLimits> StructLimits::compareLT(const LimitsPtr &other,
          const EExpression &expr) const
      {
        auto that = other->self<StructLimits>();
        if (!that || that->members.size() != members.size()) {
          return BooleanLimits::getTrueOrFalse(expr);
        }
        BooleanLimits::Ptr lim = BooleanLimits::getTrue(expr);
        for (size_t i = 0; i < members.size(); ++i) {
          if (members.at(i) && that->members.at(i)) {
            auto t = members.at(i)->compareLT(that->members.at(i), nullptr);
            if (t->containsOnly(false)) {
              lim = BooleanLimits::getFalse(expr);
              break;
            } else if (t->isUnbounded()) {
              lim = BooleanLimits::getTrueOrFalse(expr);
              break;
            } else {
              // keep the current limit which must still be true
            }
          } else {
            lim = BooleanLimits::getTrueOrFalse(expr);
            break;
          }
        }
        return lim;
      }

      Limits::LimitsPtr StructLimits::merge(const Limits::LimitsPtr &other) const
      {
        auto that = other->self<StructLimits>();
        if (!that || that->members.size() != members.size()) {
          return nullptr;
        }
        ::std::vector<LimitsPtr> e;
        e.reserve(members.size());
        for (size_t i = 0; i < members.size(); ++i) {
          auto m = members.at(i)->merge(that->members.at(i));
          if (m == nullptr) {
            return nullptr;
          }
          e.push_back(m);
        }

        return get(::std::move(e));
      }

      StructLimits::Ptr StructLimits::get(::std::vector<LimitsPtr> e)
      {
        struct Impl: public StructLimits
        {
          Impl(::std::vector<LimitsPtr> e)
              : StructLimits(::std::move(e))
          {
          }
          ~Impl()
          {
          }
        };
        return ::std::make_shared<Impl>(::std::move(e));
      }

    }
  }
}

