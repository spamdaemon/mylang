#include <mylang/ethir/limits/BooleanLimits.h>
#include <mylang/ethir/types/BooleanType.h>
#include <mylang/ethir/types/Type.h>
#include <mylang/ethir/ir/Expression.h>
#include <mylang/ethir/ir/LiteralBoolean.h>
#include <mylang/ethir/prettyPrint.h>
#include <algorithm>
#include <memory>
#include <stdexcept>

namespace mylang {
  namespace ethir {
    namespace limits {

      static ::std::set<EExpression> mergeExprs(const ::std::set<EExpression> &l,
          const ::std::set<EExpression> &r)
      {
        ::std::set<EExpression> exprs;
        if (!l.empty() && !r.empty()) {
          exprs.insert(l.begin(), l.end());
          exprs.insert(r.begin(), r.end());
        }
        return exprs;
      }

      static bool eqExprs(const ::std::set<EExpression> &l, const ::std::set<EExpression> &r)
      {
        return l == r;
      }

      BooleanLimits::BooleanLimits(::std::optional<bool> xlimits, ::std::set<EExpression> exprs)
          : bounds(xlimits), iftrue(::std::move(exprs))
      {
        for (const EExpression &e : iftrue) {
          if (e->type->self<types::BooleanType>() == nullptr) {
            throw ::std::runtime_error("Not a boolean expression");
          }
        }
      }

      BooleanLimits::Ptr BooleanLimits::dropExpressions() const
      {
        if (iftrue.empty()) {
          return self<BooleanLimits>();
        }
        if (bounds.has_value()) {
          return get(bounds.value());
        } else {
          return getTrueOrFalse(nullptr);
        }
      }

      BooleanLimits::~BooleanLimits()
      {
      }

      BooleanLimits::Ptr BooleanLimits::NOT() const
      {
        if (bounds.has_value()) {
          return get(!bounds.value());
        } else {
          return dropExpressions();
        }
      }

      BooleanLimits::Ptr BooleanLimits::AND(const BooleanLimits::Ptr &l,
          const BooleanLimits::Ptr &r)
      {
        if (l && r) {
          ::std::set<EExpression> exprs(mergeExprs(l->iftrue, r->iftrue));
          if (l->containsOnly(false) || r->containsOnly(false)) {
            return BooleanLimits::getFalse(exprs);
          }
          if (l->containsOnly(true) && r->containsOnly(true)) {
            return BooleanLimits::getTrue(exprs);
          }
          return BooleanLimits::getTrueOrFalse(exprs);
        }
        return BooleanLimits::getTrueOrFalse(nullptr);
      }

      bool BooleanLimits::isUnbounded() const
      {
        return bounds.has_value() == false;
      }

      ::std::shared_ptr<const BooleanLimits> BooleanLimits::compareEQ(const LimitsPtr &other,
          const EExpression &expr) const
      {
        auto that = other->self<BooleanLimits>();
        if (!that) {
          return getTrueOrFalse(expr);
        }

        // we merge the expression:
        // b0 = x > 1
        // b1 = y > 2
        // c  = b0 == b1
        //  --> x>1 && y>2
        auto exprs = mergeExprs(iftrue, that->iftrue);
        exprs.insert(expr);
        if (!bounds.has_value() || !that->bounds.has_value()
            || bounds.value() != that->bounds.value()) {
          return getTrueOrFalse(exprs);
        }
        if (bounds.value()) {
          return getTrue(exprs);
        } else {
          return getFalse(exprs);
        }
      }

      ::std::shared_ptr<const BooleanLimits> BooleanLimits::compareLT(const LimitsPtr &other,
          const EExpression &expr) const
      {
        auto that = other->self<BooleanLimits>();
        if (!that) {
          return getTrueOrFalse(expr);
        }
        if (that->bounds.has_value() && that->bounds.value() == false) {
          // ANY  < false ==> false
          return getFalse(expr);
        }

        if (that->bounds.has_value() && bounds.has_value()) {
          return get(bounds.value() == false && that->bounds.value() == true, expr);
        }
        return getTrueOrFalse(expr);
      }

      Limits::LimitsPtr BooleanLimits::merge(const Limits::LimitsPtr &other) const
      {
        auto that = other->self<BooleanLimits>();
        if (!that) {
          return nullptr;
        }
        if (that->bounds == this->bounds) {
          if (eqExprs(that->iftrue, iftrue)) {
            return that;
          }
          return that->dropExpressions();
        } else if (that->isUnbounded()) {
          if (eqExprs(that->iftrue, iftrue)) {
            return that;
          }
          return that->dropExpressions();
        } else if (isUnbounded()) {
          if (eqExprs(that->iftrue, iftrue)) {
            return self<>();
          }
          return dropExpressions();
        } else {
          if (eqExprs(that->iftrue, iftrue)) {
            return getTrueOrFalse(iftrue);
          }
          return getTrueOrFalse(nullptr);
        }
      }

      BooleanLimits::Ptr BooleanLimits::getTrue(::std::set<EExpression> exprs)
      {
        struct Impl: public BooleanLimits
        {
          Impl(::std::set<EExpression> exprs)
              : BooleanLimits(true, exprs)
          {
          }
          ~Impl()
          {
          }
        };

        return ::std::make_shared<Impl>(exprs);
      }

      BooleanLimits::Ptr BooleanLimits::getFalse(::std::set<EExpression> exprs)
      {
        struct Impl: public BooleanLimits
        {
          Impl(::std::set<EExpression> exprs)
              : BooleanLimits(false, exprs)
          {
          }
          ~Impl()
          {
          }
        };
        return ::std::make_shared<Impl>(exprs);
      }

      BooleanLimits::Ptr BooleanLimits::getTrueOrFalse(::std::set<EExpression> exprs)
      {
        struct Impl: public BooleanLimits
        {
          Impl(::std::set<EExpression> exprs)
              : BooleanLimits(::std::nullopt, exprs)
          {
          }
          ~Impl()
          {
          }
        };
        return ::std::make_shared<Impl>(exprs);
      }

      BooleanLimits::Ptr BooleanLimits::getTrueOrFalse(const EExpression &expr)
      {
        ::std::set<EExpression> exprs;
        if (expr) {
          exprs.insert(expr);
        }
        return getTrueOrFalse(exprs);
      }
      BooleanLimits::Ptr BooleanLimits::getTrue(const EExpression &expr)
      {
        ::std::set<EExpression> exprs;
        if (expr) {
          exprs.insert(expr);
        }
        return getTrue(exprs);
      }
      BooleanLimits::Ptr BooleanLimits::getFalse(const EExpression &expr)
      {
        ::std::set<EExpression> exprs;
        if (expr) {
          exprs.insert(expr);
        }
        return getFalse(exprs);
      }

      ELiteral BooleanLimits::getLiteral(const EType &ty) const
      {
        if (ty->self<types::BooleanType>()) {
          if (containsOnly(true)) {
            return ir::LiteralBoolean::create(true);
          }
          if (containsOnly(false)) {
            return ir::LiteralBoolean::create(false);
          }
        }
        return nullptr;
      }

    }
  }
}

