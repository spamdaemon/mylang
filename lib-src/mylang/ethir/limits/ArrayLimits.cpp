#include <mylang/ethir/limits/ArrayLimits.h>

namespace mylang {
  namespace ethir {
    namespace limits {

      ArrayLimits::ArrayLimits(IntegerLimits::Ptr l, LimitsPtr e)
          : length(l), elements(e)
      {
      }

      ArrayLimits::~ArrayLimits()
      {
      }

      bool ArrayLimits::isUnbounded() const
      {
        return length->isUnbounded() && (elements == nullptr || elements->isUnbounded());
      }

      ::std::shared_ptr<const BooleanLimits> ArrayLimits::compareEQ(const LimitsPtr &other,
          const EExpression &expr) const
      {
        auto that = other->self<ArrayLimits>();
        if (that) {
          auto x = length->compareEQ(that->length, expr);
          if (x->containsOnly(false)) {
            return x;
          }
          auto y = Limits::compareEQ(elements, that->elements, expr);
          if (x->containsOnly(true) && (y->containsOnly(true) || y->containsOnly(false))) {
            return y;
          }
        }
        return BooleanLimits::getTrueOrFalse(expr);
      }

      ::std::shared_ptr<const BooleanLimits> ArrayLimits::compareLT(const LimitsPtr &other,
          const EExpression &expr) const
      {
        auto that = other->self<ArrayLimits>();
        if (that) {
          auto x = length->compareLT(that->length, expr);
          if (x->containsOnly(true)) {
            return x;
          }
        }
        return BooleanLimits::getTrueOrFalse(expr);
      }

      Limits::LimitsPtr ArrayLimits::merge(const Limits::LimitsPtr &other) const
      {
        auto that = other->self<ArrayLimits>();
        if (!that) {
          return nullptr;
        }

        auto x = length->merge(that->length);
        auto y = Limits::merge(elements, that->elements);

        return get(x->self<IntegerLimits>(), y);
      }

      ArrayLimits::Ptr ArrayLimits::get(IntegerLimits::Ptr l, LimitsPtr e)
      {
        struct Impl: public ArrayLimits
        {
          Impl(IntegerLimits::Ptr l, LimitsPtr e)
              : ArrayLimits(l, e)
          {
          }

          ~Impl()
          {
          }
        };
        return ::std::make_shared<Impl>(l, e);
      }

      ArrayLimits::Ptr ArrayLimits::get(::std::vector<LimitsPtr> elements)
      {
        LimitsPtr p;

        for (auto e : elements) {
          p = Limits::merge(e, p);
        }

        return get(IntegerLimits::get(elements.size()), p);
      }

    }
  }
}

