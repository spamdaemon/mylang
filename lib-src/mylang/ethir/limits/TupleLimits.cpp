#include <mylang/ethir/limits/TupleLimits.h>

namespace mylang {
  namespace ethir {
    namespace limits {

      TupleLimits::TupleLimits(::std::vector<LimitsPtr> e)
          : elements(::std::move(e))
      {
      }

      TupleLimits::~TupleLimits()
      {
      }

      bool TupleLimits::isUnbounded() const
      {
        for (auto e : elements) {
          if (e->isUnbounded()) {
            return true;
          }
        }
        return false;
      }

      ::std::shared_ptr<const BooleanLimits> TupleLimits::compareEQ(const LimitsPtr &other,
          const EExpression &expr) const
      {
        auto that = other->self<TupleLimits>();
        if (!that || that->elements.size() != elements.size()) {
          return BooleanLimits::getTrueOrFalse(expr);
        }

        BooleanLimits::Ptr lim = BooleanLimits::getTrue(expr);
        for (size_t i = 0; i < elements.size(); ++i) {
          if (elements.at(i) && that->elements.at(i)) {
            auto t = elements.at(i)->compareEQ(that->elements.at(i), nullptr);
            if (t->containsOnly(false)) {
              lim = BooleanLimits::getFalse(expr);
              break;
            } else if (t->isUnbounded()) {
              lim = BooleanLimits::getTrueOrFalse(expr);
            } else {
              // keep the current limit
            }
          } else {
            lim = BooleanLimits::getTrueOrFalse(expr);
          }
        }
        return lim;
      }

      ::std::shared_ptr<const BooleanLimits> TupleLimits::compareLT(const LimitsPtr &other,
          const EExpression &expr) const
      {
        auto that = other->self<TupleLimits>();
        if (!that || that->elements.size() != elements.size()) {
          return BooleanLimits::getTrueOrFalse(expr);
        }
        BooleanLimits::Ptr lim = BooleanLimits::getTrue(expr);
        for (size_t i = 0; i < elements.size(); ++i) {
          if (elements.at(i) && that->elements.at(i)) {
            auto t = elements.at(i)->compareLT(that->elements.at(i), nullptr);
            if (t->containsOnly(false)) {
              lim = BooleanLimits::getFalse(expr);
              break;
            } else if (t->isUnbounded()) {
              lim = BooleanLimits::getTrueOrFalse(expr);
              break;
            } else {
              // keep the current limit which must still be true
            }
          } else {
            lim = BooleanLimits::getTrueOrFalse(expr);
            break;
          }
        }
        return lim;
      }

      Limits::LimitsPtr TupleLimits::merge(const Limits::LimitsPtr &other) const
      {
        auto that = other->self<TupleLimits>();
        if (!that || that->elements.size() != elements.size()) {
          return nullptr;
        }
        ::std::vector<LimitsPtr> e;
        e.reserve(elements.size());
        for (size_t i = 0; i < elements.size(); ++i) {
          auto m = elements.at(i)->merge(that->elements.at(i));
          if (m == nullptr) {
            return nullptr;
          }
          e.push_back(m);
        }

        return get(::std::move(e));
      }

      TupleLimits::Ptr TupleLimits::get(::std::vector<LimitsPtr> e)
      {
        struct Impl: public TupleLimits
        {
          Impl(::std::vector<LimitsPtr> e)
              : TupleLimits(::std::move(e))
          {
          }
          ~Impl()
          {
          }
        };
        return ::std::make_shared<Impl>(::std::move(e));
      }

    }
  }
}

