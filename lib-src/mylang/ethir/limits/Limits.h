#ifndef CLASS_MYLANG_ETHIR_LIMITS_LIMITS_H
#define CLASS_MYLANG_ETHIR_LIMITS_LIMITS_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef FILE_MYLANG_ETHIR_TYPES_TYPESET_H
#include <mylang/ethir/types/TypeSet.h>
#endif

#include <memory>

namespace mylang {
  namespace ethir {
    namespace limits {

      class BooleanLimits;

      class Limits: public ::std::enable_shared_from_this<Limits>
      {

        /** A pointer to a limit */
      public:
        typedef ::std::shared_ptr<const Limits> LimitsPtr;

        /**
         * Constructor
         */
      protected:
        Limits();

        /** Destructor */
      public:
        virtual ~Limits() = 0;

        /**
         * Get this type.
         * @return this type instance
         */
      public:
        template<class T = Limits>
        inline ::std::shared_ptr<const T> self() const
        {
          return ::std::dynamic_pointer_cast<const T>(shared_from_this());
        }

        /**
         * A limit constructor.
         * @param type a type
         * @return a limits object for the specified type.
         */
      public:
        static LimitsPtr of(const EType &type);

        /**
         * Get a literal expression corresponding to this limits object. The
         * resulting type must be convertible to the given type.
         * @param ty a guide to the result type
         * @return a literal or nullptr if none could be created
         */
      public:
        virtual ELiteral getLiteral(const EType &ty) const;

        /**
         * Check if the limits are unbounded, or, in other words, encompass
         * all possible values.
         * @return true if the limits are unbounded
         */
      public:
        virtual bool isUnbounded() const = 0;

        /**
         * Merge two limits. If one of the limits is nullptr
         * then the other limits are returned, otherwise, if both
         * are not-null, they are merged.
         */
      public:
        static LimitsPtr merge(const LimitsPtr &a, const LimitsPtr &b);

        /**
         * Compare two limits.
         * @param a limit
         * @param b a limit
         * @param expr an expression
         * @return a boolean limit
         */
      public:
        static ::std::shared_ptr<const BooleanLimits> compareEQ(const LimitsPtr &a,
            const LimitsPtr &b, const EExpression &expr);

        /**
         * Compare two limits.
         * @return a boolean limit
         */
      public:
        static ::std::shared_ptr<const BooleanLimits> compareLT(const LimitsPtr &a,
            const LimitsPtr &b, const EExpression &expr);

        /**
         * Merge two limits
         * @param other another limit
         * @return a new limit
         */
      public:
        virtual LimitsPtr merge(const LimitsPtr &other) const = 0;

        /**
         * Compare two limits.
         * @return a boolean limit
         */
      public:
        virtual ::std::shared_ptr<const BooleanLimits> compareEQ(const LimitsPtr &other,
            const EExpression &expr) const = 0;

        /**
         * Compare two limits.
         * @return a boolean limit
         */
      public:
        virtual ::std::shared_ptr<const BooleanLimits> compareLT(const LimitsPtr &other,
            const EExpression &expr) const = 0;

        /**
         * A limit constructor.
         * @param type a type
         * @return a limits object for the specified type.
         */
      private:
        static LimitsPtr of(const EType &type, types::TypeSet &types);
      };

    }
  }
}
#endif
