#include <mylang/ethir/limits/GenericLimits.h>

namespace mylang {
  namespace ethir {
    namespace limits {

      GenericLimits::GenericLimits(::std::vector<LimitsPtr> e)
          : objects(::std::move(e))
      {
      }

      GenericLimits::~GenericLimits()
      {
      }

      bool GenericLimits::isUnbounded() const
      {
        return objects.empty();
      }

      ::std::shared_ptr<const BooleanLimits> GenericLimits::compareEQ(const LimitsPtr &other,
          const EExpression &expr) const
      {
        auto that = other->self<GenericLimits>();
        if (!that) {
          return BooleanLimits::getTrueOrFalse(expr);
        }
        if (objects.size() == 1 && that->objects.size() == 1) {
          return objects.at(0)->compareEQ(that->objects.at(0), expr);
        }
        return BooleanLimits::getTrueOrFalse(expr);
      }

      ::std::shared_ptr<const BooleanLimits> GenericLimits::compareLT(const LimitsPtr &other,
          const EExpression &expr) const
      {
        auto that = other->self<GenericLimits>();
        if (!that) {
          return BooleanLimits::getTrueOrFalse(expr);
        }
        if (objects.size() == 1 && that->objects.size() == 1) {
          return objects.at(0)->compareLT(that->objects.at(0), expr);
        }
        return BooleanLimits::getTrueOrFalse(expr);
      }

      Limits::LimitsPtr GenericLimits::merge(const Limits::LimitsPtr &other) const
      {
        auto that = other->self<GenericLimits>();
        if (!that) {
          return nullptr;
        }
        // if we merge an unbounded object with a bounded object we always end up
        // with an unbounded object
        if (objects.empty()) {
          return self();
        } else if (that->objects.empty()) {
          return that;
        }

        ::std::vector<LimitsPtr> obj(objects);
        ::std::vector<LimitsPtr> remaining(that->objects);

        for (size_t i = 0; i < obj.size(); ++i) {
          for (size_t j = 0; j < remaining.size(); ++j) {
            auto m = obj.at(i)->merge(that->objects.at(j));
            if (m) {
              obj[i] = m;
              remaining[j] = *remaining.rbegin();
              remaining.erase(remaining.end() - 1);
              break;
            }
          }
        }
        obj.insert(obj.end(), remaining.begin(), remaining.end());
        return get(::std::move(obj));
      }

      GenericLimits::Ptr GenericLimits::get(::std::vector<LimitsPtr> e)
      {
        struct Impl: public GenericLimits
        {
          Impl(::std::vector<LimitsPtr> e)
              : GenericLimits(::std::move(e))
          {
          }
          ~Impl()
          {
          }
        };
        return ::std::make_shared<Impl>(::std::move(e));
      }

      GenericLimits::Ptr GenericLimits::get(LimitsPtr e)
      {
        ::std::vector<LimitsPtr> v;
        if (e) {
          v.push_back(e);
        }
        return get(v);
      }

    }
  }
}

