#ifndef FILE_MYLANG_ETHIR_LIMITS_H
#define FILE_MYLANG_ETHIR_LIMITS_H

#include <mylang/ethir/limits/ArrayLimits.h>
#include <mylang/ethir/limits/BooleanLimits.h>
#include <mylang/ethir/limits/GenericLimits.h>
#include <mylang/ethir/limits/IntegerLimits.h>
#include <mylang/ethir/limits/NamedTypeLimits.h>
#include <mylang/ethir/limits/OptLimits.h>
#include <mylang/ethir/limits/StructLimits.h>
#include <mylang/ethir/limits/TupleLimits.h>
#include <mylang/ethir/limits/UnionLimits.h>
#endif
