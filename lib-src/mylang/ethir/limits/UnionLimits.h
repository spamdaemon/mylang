#ifndef CLASS_MYLANG_ETHIR_LIMITS_UNIONLIMITS_H
#define CLASS_MYLANG_ETHIR_LIMITS_UNIONLIMITS_H

#ifndef CLASS_MYLANG_ETHIR_LIMITS_LIMITS_H
#include <mylang/ethir/limits/Limits.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_LIMITS_INTEGERLIMITS_H
#include <mylang/ethir/limits/IntegerLimits.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_LIMITS_BOOLEANLIMITS_H
#include <mylang/ethir/limits/BooleanLimits.h>
#endif

#include <vector>

namespace mylang {
  namespace ethir {
    namespace limits {

      class UnionLimits: public Limits
      {
      public:
        typedef ::std::shared_ptr<const UnionLimits> Ptr;

        /**
         * Constructor
         */
      protected:
        UnionLimits(::std::vector<LimitsPtr> members);

        /** Destructor */
      public:
        ~UnionLimits();

      public:
        static Ptr get(::std::vector<LimitsPtr> members);

      public:
        bool isUnbounded() const override final;
        LimitsPtr merge(const LimitsPtr &other) const override final;
        ::std::shared_ptr<const BooleanLimits> compareEQ(const LimitsPtr &other,
            const EExpression &expr) const override final;
        ::std::shared_ptr<const BooleanLimits> compareLT(const LimitsPtr &other,
            const EExpression &expr) const override final;

        /** The number of non-null members */
      private:
        size_t nLimits;

        /** index of the first non-null member */
      private:
        size_t limit0;

        /** The limits of each member; if member is nullptr, then that limits the range of the discriminant */
      public:
        const ::std::vector<LimitsPtr> members;
      };

    }
  }
}
#endif
