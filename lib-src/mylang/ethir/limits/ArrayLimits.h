#ifndef CLASS_MYLANG_ETHIR_LIMITS_ARRAYLIMITS_H
#define CLASS_MYLANG_ETHIR_LIMITS_ARRAYLIMITS_H

#include <memory>

#ifndef CLASS_MYLANG_ETHIR_LIMITS_LIMITS_H
#include <mylang/ethir/limits/Limits.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_LIMITS_INTEGERLIMITS_H
#include <mylang/ethir/limits/IntegerLimits.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_LIMITS_BOOLEANLIMITS_H
#include <mylang/ethir/limits/BooleanLimits.h>
#endif

#include <vector>

namespace mylang {
  namespace ethir {
    namespace limits {

      class ArrayLimits: public Limits
      {
      public:
        typedef ::std::shared_ptr<const ArrayLimits> Ptr;

        /**
         * Constructor
         */
      protected:
        ArrayLimits(IntegerLimits::Ptr length, LimitsPtr elements);

        /** Destructor */
      public:
        ~ArrayLimits();

        /**
         * Get array limits.
         * @param length the limit on the length of the array
         * @param elements the limit on the types of elements in the array
         */
      public:
        static Ptr get(IntegerLimits::Ptr length, LimitsPtr elements);
        static Ptr get(::std::vector<LimitsPtr> elements);

      public:
        bool isUnbounded() const override final;
        LimitsPtr merge(const LimitsPtr &other) const override final;
        ::std::shared_ptr<const BooleanLimits> compareEQ(const LimitsPtr &other,
            const EExpression &expr) const override final;
        ::std::shared_ptr<const BooleanLimits> compareLT(const LimitsPtr &other,
            const EExpression &expr) const override final;

      public:
        const IntegerLimits::Ptr length;

      public:
        const LimitsPtr elements;
      };

    }
  }
}
#endif
