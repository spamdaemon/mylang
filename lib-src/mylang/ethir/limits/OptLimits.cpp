#include <mylang/ethir/limits/OptLimits.h>
#include <mylang/ethir/ir/LiteralOptional.h>
#include <algorithm>
#include <memory>
#include <stdexcept>
#include <cassert>

namespace mylang {
  namespace ethir {
    namespace limits {

      OptLimits::OptLimits(BooleanLimits::Ptr p, LimitsPtr e)
          : present(::std::move(p)), element(::std::move(e))
      {
      }

      OptLimits::~OptLimits()
      {
      }

      ELiteral OptLimits::getLiteral(const EType &ty) const
      {
        auto optTy = ty->self<types::OptType>();
        if (optTy) {
          if (present->containsOnly(false)) {
            return ir::LiteralOptional::create(optTy);
          }
          if (element && present->containsOnly(true)) {
            auto v = element->getLiteral(optTy->element);
            if (v) {
              return ir::LiteralOptional::create(optTy, v);
            }
          }
        }
        return nullptr;
      }
      bool OptLimits::isUnbounded() const
      {
        return present->isUnbounded() && (element == nullptr || element->isUnbounded());
      }

      ::std::shared_ptr<const BooleanLimits> OptLimits::compareEQ(const LimitsPtr &other,
          const EExpression &expr) const
      {
        auto that = other->self<OptLimits>();
        if (that) {

          if (present->containsOnly(false) && that->present->containsOnly(false)) {
            return BooleanLimits::getTrue(expr);
          }
          if (present->containsOnly(true) && that->present->containsOnly(true)) {
            return Limits::compareEQ(element, that->element, expr);
          }

          if (present->containsOnly(false) && that->present->containsOnly(true)) {
            return BooleanLimits::getFalse(expr); // FALSE
          }
          if (present->containsOnly(true) && that->present->containsOnly(false)) {
            return BooleanLimits::getFalse(expr); // FALSE
          }
        }
        return BooleanLimits::getTrueOrFalse(expr);
      }

      ::std::shared_ptr<const BooleanLimits> OptLimits::compareLT(const LimitsPtr &other,
          const EExpression &expr) const
      {
        auto that = other->self<OptLimits>();
        if (that) {

          if (present->containsOnly(false) && that->present->containsOnly(true)) {
            return BooleanLimits::getTrue(expr); // TRUE
          }
          if (that->present->containsOnly(false)) {
            return BooleanLimits::getFalse(expr); // FALSE
          }
          if (present->containsOnly(true) && that->present->containsOnly(true)) {
            return Limits::compareLT(element, that->element, expr);
          }
        }
        return BooleanLimits::getTrueOrFalse(expr);
      }

      Limits::LimitsPtr OptLimits::merge(const Limits::LimitsPtr &other) const
      {
        auto that = other->self<OptLimits>();
        if (that) {
          auto p = present->merge(that->present)->self<BooleanLimits>();
          if (p->containsOnly(false)) {
            return getNil();
          }
          auto e = Limits::merge(element, that->element);
          return get(p, e);
        }
        return nullptr;
      }

      OptLimits::Ptr OptLimits::get(BooleanLimits::Ptr p, LimitsPtr e)
      {
        struct Impl: public OptLimits
        {
          Impl(BooleanLimits::Ptr p, LimitsPtr e)
              : OptLimits(std::move(p), ::std::move(e))
          {
          }
          ~Impl()
          {
          }
        };
        if (p->containsOnly(false)) {
          return ::std::make_shared<Impl>(::std::move(p), nullptr);
        } else {
          return ::std::make_shared<Impl>(::std::move(p), ::std::move(e));
        }
      }

      OptLimits::Ptr OptLimits::getNil()
      {
        return get(BooleanLimits::getFalse(nullptr), nullptr);
      }

    }
  }
}

