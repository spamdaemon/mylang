#include <mylang/ethir/TypeMismatchError.h>
#include <mylang/ethir/types/types.h>

#include <string>

namespace mylang {
  namespace ethir {

    static ::std::string make_message(const EType &expected, const EType &actual)
    {
      ::std::string msg;
      msg += "Expected type ";
      msg += expected->toString();
      msg += ", but found ";
      if (actual) {
        msg += actual->toString();
      } else {
        msg += "null";
      }
      return msg;
    }

    TypeMismatchError::TypeMismatchError(const EType &xexpected, const EType &xactual)
        : ::std::runtime_error(make_message(xexpected, xactual)), expected(xexpected),
            actual(xactual)
    {
    }

    TypeMismatchError::~TypeMismatchError()
    {
    }

    void TypeMismatchError::check(const EType &expected, const EType &actual)
    {
      if (actual && expected->isSameType(*actual)) {
        return;
      }
      throw TypeMismatchError(expected, actual);
    }

  }
}
