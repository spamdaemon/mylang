#ifndef FILE_MYLANG_ETHIR_H
#define FILE_MYLANG_ETHIR_H
#include <memory>

namespace mylang {
  namespace ethir {

    namespace types {
      class Type;
    }

    namespace ir {
      class Node;
      class Statement;
      class Expression;
      class Literal;
      class Variable;
      class Declaration;
      class Program;
      class LambdaExpression;
      class GlobalValue;
    }

    typedef ::std::shared_ptr<const mylang::ethir::types::Type> EType;
    typedef ::std::shared_ptr<const mylang::ethir::ir::Node> ENode;
    typedef ::std::shared_ptr<const mylang::ethir::ir::Statement> EStatement;
    typedef ::std::shared_ptr<const mylang::ethir::ir::Expression> EExpression;
    typedef ::std::shared_ptr<const mylang::ethir::ir::Literal> ELiteral;
    typedef ::std::shared_ptr<const mylang::ethir::ir::Variable> EVariable;
    typedef ::std::shared_ptr<const mylang::ethir::ir::Program> EProgram;
    typedef ::std::shared_ptr<const mylang::ethir::ir::Declaration> EDeclaration;
    typedef ::std::shared_ptr<const mylang::ethir::ir::LambdaExpression> ELambda;
    typedef ::std::shared_ptr<const mylang::ethir::ir::GlobalValue> EGlobal;

    class Name;
  }
}
#endif
