#include <mylang/ethir/TypeName.h>

namespace mylang {
  namespace ethir {

    TypeName::TypeName(mylang::names::Name::Ptr name)
        : _source(name)
    {
    }

    TypeName::TypeName(const TypeName &name)
        : _source(name._source)
    {
    }

    TypeName::~TypeName()
    {
    }

    TypeName& TypeName::operator=(const TypeName &name)
    {
      _source = name._source;
      return *this;
    }

    TypeName TypeName::create(mylang::names::Name::Ptr src)
    {
      return TypeName(src);
    }

    TypeName TypeName::create()
    {
      return create(mylang::names::Name::create());
    }

    TypeName TypeName::create(const ::std::string &prefix)
    {
      return create(mylang::names::Name::create(prefix));
    }

    bool TypeName::equals(const TypeName &name) const
    {
      return _source == name._source;
    }
    bool TypeName::less(const TypeName &name) const
    {
      return _source < name._source;
    }
    int TypeName::compare(const TypeName &name) const
    {
      if (_source < name._source) {
        return -1;
      } else if (_source == name._source) {
        return 0;
      } else {
        return 1;
      }
    }
  }
}
