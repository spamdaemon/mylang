#include <mylang/ethir/prettyPrint.h>
#include <mylang/ethir/ir/NodeVisitor.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/text/AsciiTable.h>
#include <iostream>
#include <sstream>

namespace mylang {
  namespace ethir {

    namespace {

      struct Indent
      {
        Indent(::std::string newIndent, const ::std::string &xamount)
            : current(newIndent), amount(xamount)
        {
        }

        Indent indent() const
        {
          return Indent(current + amount, amount);
        }

        friend ::std::ostream& operator<<(::std::ostream &out, const Indent &i)
        {
          return out << i.current;
        }

        const ::std::string current;
        const ::std::string &amount;
      };

      static void prettyPrint(const ENode &node, ::std::ostream &out, const Indent &indent)
      {
        struct V: public ir::NodeVisitor
        {
          V(::std::ostream &xout, const Indent &xindent)
              : out(xout), indent(xindent)
          {
          }
          ~V()
          {
          }

          void visitNext(ir::Statement::StatementPtr node)
          {
            if (node->next) {
              out << ::std::endl;
              node->next->accept(*this);
            }
          }
          static ::std::string toString(const EType &type)
          {
            return toString(type->name());
          }

          static ::std::string toString(const mylang::names::Name::Ptr &name)
          {
            ::std::string res;
            res += '"';
            res += mylang::text::AsciiTable::escapeText(mylang::text::AsciiTable::JAVA,
                name->uniqueFullName("."));
            res += '"';
            return res;
          }

          static ::std::string toString(const Tag &name)
          {
            ::std::string res;
            res += "(tag ";
            res += toString(name.source());
            res += ")";
            return res;
          }

          static ::std::string toString(const Name &name)
          {
            ::std::string res;
            res += "(name ";
            res += toString(name.source());
            if (name.version() != 0) {
              res += '{';
              res += ::std::to_string(name.version());
              res += '}';
            }
            res += ")";
            return res;
          }

          static ::std::string toString(const ir::Declaration::Scope &scope)
          {
            switch (scope) {
            case ir::Declaration::Scope::FUNCTION_SCOPE:
              return "function-scope";
            case ir::Declaration::Scope::GLOBAL_SCOPE:
              return "global-scope";
            case ir::Declaration::Scope::PROCESS_SCOPE:
              return "process-scope";
            default:
              throw ::std::runtime_error("Unknown scope");
            }

          }

          static ::std::string toString(const TypeName &name)
          {
            return toString(name.source());
          }

          static ::std::string toString(const ir::Variable::VariablePtr &var)
          {
            ::std::ostringstream sbuf;
            sbuf << "(ref " << toString(var->name) << " " << toString(var->scope) << " "
                << toString(var->type) << ')';
            return sbuf.str();
          }

          static ::std::string toString(const ::std::vector<ir::Variable::VariablePtr> &vars,
              const char *sep = "")
          {
            ::std::string res;
            for (auto var : vars) {
              res += sep;
              res += toString(var);
              sep = " ";
            }
            return res;
          }

          void visitProgram(ir::Program::ProgramPtr node) override
          {
            for (auto g : node->globals) {
              g->accept(*this);
              out << ::std::endl;
            }
            for (auto p : node->processes) {
              p->accept(*this);
              out << ::std::endl;
            }
          }

          void visitGlobalValue(ir::GlobalValue::GlobalValuePtr node) override
          {
            out << indent;
            switch (node->kind) {
            case ir::GlobalValue::EXPORTED:
              out << "(export ";
              break;
            case ir::GlobalValue::WELL_KNOWN:
              out << "(well-known ";
              break;
            default:
              out << "(val ";
              break;
            }
            out << toString(node->variable) << ::std::endl;
            Indent newIndent = indent.indent();
            out << newIndent;
            prettyPrint(node->value, out, newIndent);
            out << ')';
            visitNext(node);
          }

          void visitNewProcess(mylang::ethir::ir::NewProcess::NewProcessPtr expr) override
          {
            out << "(new-process " << toString(expr->processName) << ' '
                << toString(expr->constructorName);
            for (auto arg : expr->arguments) {
              out << ' ';
              out << toString(arg) << ')';
            }
            out << ')';
          }

          void visitNewUnion(mylang::ethir::ir::NewUnion::NewUnionPtr expr) override
          {
            out << "(new-union " << expr->member << ' ';
            out << toString(expr->value) << ')';
          }

          void visitLiteralChar(mylang::ethir::ir::LiteralChar::LiteralCharPtr expr) override
          {
            auto text = mylang::text::AsciiTable::escapeText(mylang::text::AsciiTable::JAVA,
                expr->text);
            out << "'" << text << "'";
          }
          void visitLiteralString(mylang::ethir::ir::LiteralString::LiteralStringPtr expr)
          override
          {
            auto text = mylang::text::AsciiTable::escapeText(mylang::text::AsciiTable::JAVA,
                expr->text);
            out << '"' << text << '"';
          }

          void visitVariableUpdate(mylang::ethir::ir::VariableUpdate::VariableUpdatePtr stmt)
          override
          {
            out << indent << "(update-local-var " << toString(stmt->target) << ' '
                << toString(stmt->value) << ')';
            visitNext(stmt);
          }

          void visitProcessVariableUpdate(
              mylang::ethir::ir::ProcessVariableUpdate::ProcessVariableUpdatePtr stmt)
              override
          {
            out << indent << "(update-process-var " << toString(stmt->target) << ' '
                << toString(stmt->value) << ')';
            visitNext(stmt);
          }

          void visitCommentStatement(mylang::ethir::ir::CommentStatement::CommentStatementPtr stmt)
          override
          {
            out << indent << "(comment \"" << stmt->comment << "\")";
            visitNext(stmt);
          }

          void visitNoStatement(mylang::ethir::ir::NoStatement::NoStatementPtr stmt)
          override
          {
            out << indent << "(nop)";
            visitNext(stmt);
          }

          void visitIfStatement(mylang::ethir::ir::IfStatement::IfStatementPtr stmt) override
          {
            out << indent << "(if " << toString(stmt->condition) << ::std::endl;
            prettyPrint(stmt->iftrue, out, indent.indent());
            out << ::std::endl;
            prettyPrint(stmt->iffalse, out, indent.indent());
            out << ')';
            visitNext(stmt);
          }
          void visitTryCatch(mylang::ethir::ir::TryCatch::TryCatchPtr stmt) override
          {
            out << indent << "(try " << ::std::endl;
            prettyPrint(stmt->tryBody, out, indent.indent());
            out << ::std::endl;
            prettyPrint(stmt->catchBody, out, indent.indent());
            out << ')';
            visitNext(stmt);
          }
          void visitLiteralReal(mylang::ethir::ir::LiteralReal::LiteralRealPtr expr) override
          {
            out << expr->text;
          }

          void visitToArray(mylang::ethir::ir::ToArray::ToArrayPtr expr) override
          {
            out << "(to-array";
            out << " (type " << toString(expr->type) << ")";
            out << ::std::endl;
            prettyPrint(expr->loop, out, indent.indent());
            out << ')';
          }

          void visitPhi(mylang::ethir::ir::Phi::PhiPtr expr) override
          {
            out << "(phi";
            out << " (type " << toString(expr->type) << ")";
            out << toString(expr->arguments, " ");
            out << ')';
          }

          void visitNoValue(mylang::ethir::ir::NoValue::NoValuePtr)
          override
          {
            out << "(no-value)";
          }

          void visitOpaqueExpression(mylang::ethir::ir::OpaqueExpression::OpaqueExpressionPtr expr)
          override
          {
            out << "(opaque" << "(" << toString(expr->arguments, " ") << ")" << "("
                << toString(expr->freeVariables, " ") << ")";
            prettyPrint(expr->expression, out, indent);
            out << ")";
          }

          void visitOperatorExpression(
              mylang::ethir::ir::OperatorExpression::OperatorExpressionPtr expr) override
          {
            out << "(apply-operator ";
            switch (expr->name) {
            case ir::OperatorExpression::OP_NEW:
              out << "new";
              break;
            case ir::OperatorExpression::OP_CALL:
              out << "call";
              break;
            case ir::OperatorExpression::OP_IS_LOGGABLE:
              out << "is-loggable";
              break;
            case ir::OperatorExpression::OP_CHECKED_CAST:
              out << "checked_cast";
              break;
            case ir::OperatorExpression::OP_UNCHECKED_CAST:
              out << "unchecked_cast";
              break;
            case ir::OperatorExpression::OP_NEG:
              out << "neg";
              break;
            case ir::OperatorExpression::OP_ADD:
              out << "add";
              break;
            case ir::OperatorExpression::OP_SUB:
              out << "sub";
              break;
            case ir::OperatorExpression::OP_MUL:
              out << "mul";
              break;
            case ir::OperatorExpression::OP_DIV:
              out << "div";
              break;
            case ir::OperatorExpression::OP_MOD:
              out << "mod";
              break;
            case ir::OperatorExpression::OP_REM:
              out << "rem";
              break;

            case ir::OperatorExpression::OP_INDEX:
              out << "index";
              break;
            case ir::OperatorExpression::OP_SUBRANGE:
              out << "subrange";
              break;
            case ir::OperatorExpression::OP_EQ:
              out << "eq";
              break;
            case ir::OperatorExpression::OP_NEQ:
              out << "neq";
              break;
            case ir::OperatorExpression::OP_LT:
              out << "lt";
              break;
            case ir::OperatorExpression::OP_LTE:
              out << "lte";
              break;
            case ir::OperatorExpression::OP_AND:
              out << "and";
              break;
            case ir::OperatorExpression::OP_OR:
              out << "or";
              break;
            case ir::OperatorExpression::OP_XOR:
              out << "xor";
              break;
            case ir::OperatorExpression::OP_NOT:
              out << "not";
              break;
            case ir::OperatorExpression::OP_GET:
              out << "get";
              break;
            case ir::OperatorExpression::OP_IS_PRESENT:
              out << "is-present";
              break;
            case ir::OperatorExpression::OP_IS_INPUT_NEW:
              out << "is-input-new";
              break;
            case ir::OperatorExpression::OP_IS_INPUT_CLOSED:
              out << "is-input-closed";
              break;
            case ir::OperatorExpression::OP_IS_OUTPUT_CLOSED:
              out << "is-output-closed";
              break;
            case ir::OperatorExpression::OP_IS_PORT_READABLE:
              out << "is-readable";
              break;
            case ir::OperatorExpression::OP_IS_PORT_WRITABLE:
              out << "is-writable";
              break;
            case ir::OperatorExpression::OP_SIZE:
              out << "length";
              break;
            case ir::OperatorExpression::OP_CONCATENATE:
              out << "concat";
              break;
            case ir::OperatorExpression::OP_MERGE_TUPLES:
              out << "merge";
              break;
            case ir::OperatorExpression::OP_ZIP:
              out << "zip";
              break;
            case ir::OperatorExpression::OP_FROM_BITS:
              out << "from-bits";
              break;
            case ir::OperatorExpression::OP_TO_BITS:
              out << "to-bits";
              break;
            case ir::OperatorExpression::OP_FROM_BYTES:
              out << "from-bytes";
              break;
            case ir::OperatorExpression::OP_TO_BYTES:
              out << "to-bytes";
              break;
            case ir::OperatorExpression::OP_BITS_TO_BYTES:
              out << "bits-to-bytes";
              break;
            case ir::OperatorExpression::OP_BYTES_TO_BITS:
              out << "bytes-to-bits";
              break;
            case ir::OperatorExpression::OP_CLAMP:
              out << "clamp";
              break;
            case ir::OperatorExpression::OP_WRAP:
              out << "wrap";
              break;
            case ir::OperatorExpression::OP_BYTE_TO_UINT8:
              out << "byte-to-uint8";
              break;
            case ir::OperatorExpression::OP_BYTE_TO_SINT8:
              out << "byte-to-sint8";
              break;
            case ir::OperatorExpression::OP_STRING_TO_CHARS:
              out << "string-to-chars";
              break;
            case ir::OperatorExpression::OP_INTERPOLATE_TEXT:
              out << "interpolate-text";
              break;
            case ir::OperatorExpression::OP_TO_STRING:
              out << "to-string";
              break;
            case ir::OperatorExpression::OP_FOLD:
              out << "fold";
              break;
            case ir::OperatorExpression::OP_PAD:
              out << "pad";
              break;
            case ir::OperatorExpression::OP_TRIM:
              out << "trim";
              break;
            case ir::OperatorExpression::OP_DROP:
              out << "drop";
              break;
            case ir::OperatorExpression::OP_TAKE:
              out << "take";
              break;
            case ir::OperatorExpression::OP_MAP_ARRAY:
              out << "map-array";
              break;
            case ir::OperatorExpression::OP_MAP_OPTIONAL:
              out << "map-optional";
              break;
            case ir::OperatorExpression::OP_FILTER_ARRAY:
              out << "filter-array";
              break;
            case ir::OperatorExpression::OP_FILTER_OPTIONAL:
              out << "filter-optional";
              break;
            case ir::OperatorExpression::OP_FIND:
              out << "find";
              break;
            case ir::OperatorExpression::OP_REVERSE:
              out << "reverse";
              break;
            case ir::OperatorExpression::OP_FLATTEN:
              out << "flatten";
              break;
            case ir::OperatorExpression::OP_PARTITION:
              out << "partition";
              break;
            case ir::OperatorExpression::OP_HEAD:
              out << "head";
              break;
            case ir::OperatorExpression::OP_TAIL:
              out << "tail";
              break;
            case ir::OperatorExpression::OP_STRUCT_TO_TUPLE:
              out << "struct-to-tuple";
              break;
            case ir::OperatorExpression::OP_TUPLE_TO_STRUCT:
              out << "tuple-to-struct";
              break;
            case ir::OperatorExpression::OP_BASETYPE_CAST:
              out << "to-basetype";
              break;
            case ir::OperatorExpression::OP_ROOTTYPE_CAST:
              out << "to-roottype";
              break;
            case ir::OperatorExpression::OP_READ_PORT:
              out << "read-port";
              break;
            case ir::OperatorExpression::OP_SET:
              out << "set";
              break;
            case ir::OperatorExpression::OP_BLOCK_READ:
              out << "block-read";
              break;
            case ir::OperatorExpression::OP_BLOCK_WRITE:
              out << "block-write";
              break;
            case ir::OperatorExpression::OP_CLEAR_PORT:
              out << "clear-port";
              break;
            case ir::OperatorExpression::OP_CLOSE_PORT:
              out << "close-port";
              break;
            case ir::OperatorExpression::OP_WAIT_PORT:
              out << "wait-port";
              break;
            case ir::OperatorExpression::OP_WRITE_PORT:
              out << "write-port";
              break;
            case ir::OperatorExpression::OP_LOG:
              out << "log";
              break;
            case ir::OperatorExpression::OP_APPEND:
              out << "append";
              break;
            case ir::OperatorExpression::OP_BUILD:
              out << "build";
              break;
            }
            out << " (type " << toString(expr->type) << ")";
            out << toString(expr->arguments, " ");
            out << ')';
          }
          void visitLiteralInteger(mylang::ethir::ir::LiteralInteger::LiteralIntegerPtr expr)
          override
          {
            out << expr->text;
          }
          void visitValueDecl(mylang::ethir::ir::ValueDecl::ValueDeclPtr stmt) override
          {
            out << indent << "(val " << toString(stmt->variable);
            if (stmt->value) {
              out << ' ';
              stmt->value->accept(*this);
            }
            out << ')';
            visitNext(stmt);
          }
          void visitVariableDecl(mylang::ethir::ir::VariableDecl::VariableDeclPtr stmt) override
          {
            out << indent << "(var " << toString(stmt->variable);
            if (stmt->value) {
              out << ' ';
              stmt->value->accept(*this);
            }
            out << ')';
            visitNext(stmt);
          }
          void visitProcessValue(mylang::ethir::ir::ProcessValue::ProcessValuePtr stmt) override
          {
            out << indent << "(val " << toString(stmt->variable);
            if (stmt->value) {
              out << ' ';
              stmt->value->accept(*this);
            }
            out << ')';
            visitNext(stmt);
          }
          void visitProcessVariable(mylang::ethir::ir::ProcessVariable::ProcessVariablePtr stmt)
          override
          {
            out << indent << "(var " << toString(stmt->variable);
            if (stmt->value) {
              out << ' ';
              stmt->value->accept(*this);
            }
            out << ')';
          }
          void visitProcessDecl(mylang::ethir::ir::ProcessDecl::ProcessDeclPtr stmt) override
          {
            out << indent << "(defproc " << toString(stmt->name) << ::std::endl;
            for (auto g : stmt->publicPorts) {
              g->accept(*this);
              out << ::std::endl;
            }
            for (auto p : stmt->variables) {
              p->accept(*this);
              out << ::std::endl;
            }
            for (auto p : stmt->constructors) {
              p->accept(*this);
              out << ::std::endl;
            }
            for (auto p : stmt->blocks) {
              p->accept(*this);
              out << ::std::endl;
            }
            visitNext(stmt);
          }
          void visitParameter(mylang::ethir::ir::Parameter::ParameterPtr param) override
          {
            out << indent << "(parameter " << toString(param->variable->name) << ' '
                << toString(param->variable->type) << ')';
          }
          void visitLiteralBoolean(mylang::ethir::ir::LiteralBoolean::LiteralBooleanPtr expr)
          override
          {
            out << expr->text;
          }
          void visitBreakStatement(mylang::ethir::ir::BreakStatement::BreakStatementPtr stmt)
          override
          {
            out << indent << "(break " << toString(stmt->scope) << ')';
          }
          void visitContinueStatement(
              mylang::ethir::ir::ContinueStatement::ContinueStatementPtr stmt)
              override
          {
            out << indent << "(continue " << toString(stmt->scope) << ')';
          }
          void visitLambdaExpression(mylang::ethir::ir::LambdaExpression::LambdaExpressionPtr expr)
          override
          {
            out << "(lambda " << toString(expr->tag) << ::std::endl;

            for (auto p : expr->parameters) {
              prettyPrint(p, out, indent.indent());
              out << ::std::endl;
            }
            prettyPrint(expr->body, out, indent.indent());
            out << ')';
          }

          void visitGetDiscriminant(mylang::ethir::ir::GetDiscriminant::GetDiscriminantPtr expr)
          override
          {
            out << "(discriminant " << toString(expr->object) << ')';
          }
          void visitGetMember(mylang::ethir::ir::GetMember::GetMemberPtr expr) override
          {
            out << "(member " << expr->name << ' ' << toString(expr->object) << ')';
          }
          void visitLiteralByte(mylang::ethir::ir::LiteralByte::LiteralBytePtr expr) override
          {
            out << expr->text;
          }
          void visitVariable(mylang::ethir::ir::Variable::VariablePtr expr) override
          {
            out << toString(expr);
          }

          void visitOwnConstructorCall(
              mylang::ethir::ir::OwnConstructorCall::OwnConstructorCallPtr stmt) override
          {
            out << indent << "(self ";
            prettyPrint(stmt->pre, out, indent);
            out << indent << toString(stmt->arguments, " ") << ')';
            visitNext(stmt);
          }
          void visitLetExpression(mylang::ethir::ir::LetExpression::LetExpressionPtr expr)
          override
          {
            out << "(let " << toString(expr->variable) << ::std::endl;
            prettyPrint(expr->value, out, indent.indent());
            out << ::std::endl;
            prettyPrint(expr->result, out, indent.indent());
            out << ')';
          }
          void visitLoopStatement(mylang::ethir::ir::LoopStatement::LoopStatementPtr stmt)
          override
          {
            out << indent << "(loop " << toString(stmt->name) << ::std::endl;
            prettyPrint(stmt->body, out, indent.indent());
            out << ')';
            visitNext(stmt);
          }
          void visitProcessBlock(mylang::ethir::ir::ProcessBlock::ProcessBlockPtr stmt) override
          {
            out << indent << "(process-block";
            if (stmt->name.has_value()) {
              out << ' ' << *stmt->name;
            }
            out << ::std::endl;
            auto newIndent = indent.indent();
            prettyPrint(stmt->body, out, newIndent);
            out << ')';
            visitNext(stmt);
          }

          void visitAbortStatement(mylang::ethir::ir::AbortStatement::AbortStatementPtr stmt)
          override
          {
            out << indent << "(abort";
            if (stmt->value) {
              out << ' ' << toString(stmt->value);
            }
            out << ")";
          }

          void visitReturnStatement(mylang::ethir::ir::ReturnStatement::ReturnStatementPtr stmt)
          override
          {
            out << indent << "(return";
            if (stmt->value) {
              out << ' ';
              out << toString(stmt->value);
            }
            out << ")";
          }
          void visitYieldStatement(mylang::ethir::ir::YieldStatement::YieldStatementPtr stmt)
          override
          {
            out << indent << "(yield " << toString(stmt->value) << ")";
            visitNext(stmt);
          }

          ::std::string toString(const mylang::ethir::ir::Loop::State &s) const
          {
            if (s == mylang::ethir::ir::Loop::CURRENT_STATE) {
              return "current-state";
            } else if (s == mylang::ethir::ir::Loop::STOP_STATE) {
              return "stop-state";
            } else {
              ::std::ostringstream sout;
              sout << s;
              return sout.str();
            }
          }

          void printLoopExpressions(const mylang::ethir::ir::Loop::Expressions &exprs,
              const Indent &i)
          {
            if (exprs.size() > 0) {
              for (const auto &e : exprs) {
                out << i << "(";
                prettyPrint(e.first, out, i);
                out << " = ";
                prettyPrint(e.second, out, i);
                out << ")" << ::std::endl;
              }
            }
          }

          void visitIterateArrayStep(mylang::ethir::ir::IterateArrayStep::IterateArrayStepPtr stmt)
          override
          {
            out << indent << "(iterate-array-step " << ::std::endl;
            auto newIndent = indent.indent();
            out << newIndent << "(nextState ";
            prettyPrint(stmt->nextState, out, newIndent.indent());
            out << ')' << ::std::endl;
            out << newIndent << "(reverse " << stmt->reverse << ")" << ::std::endl;
            out << newIndent << "(array " << toString(stmt->array) << ")" << ::std::endl;
            out << newIndent << "(counter " << toString(stmt->counter) << ")" << ::std::endl;
            out << newIndent << "(value " << toString(stmt->value) << ")" << ::std::endl;
            out << newIndent << "(initializers " << ::std::endl;
            printLoopExpressions(stmt->initializers, newIndent.indent());
            out << newIndent << ")" << ::std::endl;
            prettyPrint(stmt->body, out, newIndent.indent());
            out << ")";
          }

          void visitSequenceStep(mylang::ethir::ir::SequenceStep::SequenceStepPtr stmt)
          override
          {
            out << indent << "(sequence-step " << ::std::endl;
            auto newIndent = indent.indent();
            out << newIndent << "(nextState ";
            prettyPrint(stmt->nextState, out, newIndent.indent());
            out << ')' << ::std::endl;
            out << newIndent << "(max-count " << toString(stmt->iterationCount) << ")"
                << ::std::endl;
            out << newIndent << "(counter " << toString(stmt->counter) << ")" << ::std::endl;
            out << newIndent << "(initializers " << ::std::endl;
            printLoopExpressions(stmt->initializers, newIndent.indent());
            out << newIndent << ")" << ::std::endl;
            prettyPrint(stmt->body, out, newIndent.indent());
            out << ")";
          }

          void visitSingleValueStep(mylang::ethir::ir::SingleValueStep::SingleValueStepPtr stmt)
          override
          {
            out << indent << "(single-value-step " << ::std::endl;
            auto newIndent = indent.indent();
            out << newIndent << "(value " << toString(stmt->value) << ")" << ::std::endl;
            prettyPrint(stmt->body, out, newIndent.indent());
            out << ")";
          }

          void visitSkipStatement(mylang::ethir::ir::SkipStatement::SkipStatementPtr stmt)
          override
          {
            out << indent << "(skip ";
            out << "(nextState " << toString(stmt->nextState) << ") ";
            printLoopExpressions(stmt->updates, indent.indent());

            out << ")";
            visitNext(stmt);
          }
          void visitLoop(mylang::ethir::ir::Loop::LoopPtr loop)
          override
          {
            out << indent << "(loop-node ";
            auto newIndent = indent.indent();
            out << toString(loop->start) << ::std::endl;
            out << newIndent << "(states ";
            auto newIndent2 = newIndent.indent();
            for (const auto &e : loop->states) {

              out << ::std::endl;
              out << newIndent2 << '[' << toString(e.first) << "] -> ";
              prettyPrint(e.second, out, newIndent2);
            }
            out << ")";
            out << ")";
          }

          void visitThrowStatement(mylang::ethir::ir::ThrowStatement::ThrowStatementPtr stmt)
          override
          {
            out << indent << "(throw";
            if (stmt->value) {
              out << ' ';
              out << toString(stmt->value);
            }
            out << ")";
          }

          void visitProcessConstructor(
              mylang::ethir::ir::ProcessConstructor::ProcessConstructorPtr stmt) override
          {
            out << indent << "(constructor";
            auto newIndent = indent.indent();
            for (auto p : stmt->parameters) {
              out << ::std::endl;
              prettyPrint(p, out, newIndent);
            }
            if (stmt->callConstructor) {
              out << ::std::endl;
              prettyPrint(stmt->callConstructor, out, newIndent);
            }
            out << ::std::endl;
            prettyPrint(stmt->body, out, newIndent);
            out << ')';
            visitNext(stmt);
          }

          void visitLiteralBit(mylang::ethir::ir::LiteralBit::LiteralBitPtr expr) override
          {
            out << expr->text;
          }
          void visitLiteralArray(mylang::ethir::ir::LiteralArray::LiteralArrayPtr expr) override
          {
            out << "(literal-array ";
            out << toString(expr->type);
            for (auto e : expr->elements) {
              out << ' ';
              prettyPrint(e, out, indent);
            }
            out << ")";
          }

          void visitLiteralNamedType(mylang::ethir::ir::LiteralNamedType::LiteralNamedTypePtr expr)
          override
          {
            out << "(literal-named ";
            out << toString(expr->type);
            out << ' ';
            prettyPrint(expr->value, out, indent);
            out << ")";
          }

          void visitLiteralOptional(mylang::ethir::ir::LiteralOptional::LiteralOptionalPtr expr)
          override
          {
            out << "(literal-optional ";
            out << toString(expr->type);
            if (expr->value) {
              out << ' ';
              prettyPrint(expr->value, out, indent);
            }
            out << ")";
          }
          void visitLiteralStruct(mylang::ethir::ir::LiteralStruct::LiteralStructPtr expr)
          override
          {
            out << "(literal-struct ";
            out << toString(expr->type);
            for (auto e : expr->members) {
              out << ' ';
              prettyPrint(e, out, indent);
            }
            out << ")";
          }
          void visitLiteralTuple(mylang::ethir::ir::LiteralTuple::LiteralTuplePtr expr) override
          {
            out << "(literal-tuple ";
            out << toString(expr->type);
            for (auto e : expr->elements) {
              out << ' ';
              prettyPrint(e, out, indent);
            }
            out << ")";
          }
          void visitLiteralUnion(mylang::ethir::ir::LiteralUnion::LiteralUnionPtr expr) override
          {
            auto text = mylang::text::AsciiTable::escapeText(mylang::text::AsciiTable::JAVA,
                expr->member);
            out << '"' << text << '"';

            out << "(literal-union ";
            out << toString(expr->type);
            out << '"' << text << '"' << ' ';
            prettyPrint(expr->value, out, indent);
            out << ")";
          }

          void visitInputPortDecl(mylang::ethir::ir::InputPortDecl::InputPortDeclPtr stmt)
          override
          {
            out << indent << "(input-port " << toString(stmt->variable) << ')';
            visitNext(stmt);
          }
          void visitOutputPortDecl(mylang::ethir::ir::OutputPortDecl::OutputPortDeclPtr stmt)
          override
          {
            out << indent << "(output-port " << toString(stmt->variable) << ")";
            visitNext(stmt);
          }
          void visitStatementBlock(mylang::ethir::ir::StatementBlock::StatementBlockPtr stmt)
          override
          {
            out << indent << "(block";
            if (stmt->statements) {
              out << ::std::endl;
              prettyPrint(stmt->statements, out, indent.indent());
            }
            out << ')';
            visitNext(stmt);
          }
          void visitForeachStatement(mylang::ethir::ir::ForeachStatement::ForeachStatementPtr stmt)
          override
          {
            out << indent << "(for " << toString(stmt->name) << ' ' << toString(stmt->data)
                << ::std::endl;
            prettyPrint(stmt->body, out, indent.indent());
            out << ')';
            visitNext(stmt);
          }

          ::std::ostream &out;
          const Indent &indent;
        };

        V v(out, indent);
        node->accept(v);
      }

    }

    void prettyPrint(const ENode &node, ::std::ostream &out)
    {
      ::std::string amount = "  ";
      prettyPrint(node, out, Indent("", amount));
    }

    ::std::string prettyPrint(const ENode &node)
    {
      std::ostringstream sout;
      prettyPrint(node, sout);
      return sout.str();
    }
  }
}
