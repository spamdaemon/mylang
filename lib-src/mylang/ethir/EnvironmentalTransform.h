#ifndef CLASS_MYLANG_ETHIR_ENVIRONMENTALTRANSFORM_H
#define CLASS_MYLANG_ETHIR_ENVIRONMENTALTRANSFORM_H

#ifndef CLASS_MYLANG_ETHIR_TRANSFORM_H
#include <mylang/ethir/Transform.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_ENVIRONMENT_H
#include <mylang/ethir/Environment.h>
#endif

namespace mylang {
  namespace ethir {

    /**
     * The prune pass performs three tasks:
     * 1. it removes unused variables
     * 2. it replaces occurrences of `y` with variable `x` if `val y=x`
     * 3. it remove statements that are NOPs, such as `if(x) {} else {}`
     * Even though specific passes may still be required for each of those,
     * because the cost of implementing these tasks together is small, it makes
     * sense to them together, so that subsequent passes can be performed faster.
     */
    class EnvironmentalTransform: public Transform
    {
      /**
       * Create a transform with an inital environment.
       * @param initial an initial environment or nullptr to use a default environment
       */
    public:
      EnvironmentalTransform(Environment::Ptr initial);

    public:
      EnvironmentalTransform();

      /** Destructor */
    public:
      virtual ~EnvironmentalTransform();

      ENode visit(const ir::LambdaExpression::LambdaExpressionPtr &node) override;
      ENode visit(const ir::Loop::LoopPtr &node) override;
      ENode visit(const ir::LetExpression::LetExpressionPtr &node) override;
      ENode visit(const ir::GlobalValue::GlobalValuePtr &node) override;
      ENode visit(const ir::ProcessValue::ProcessValuePtr &node) override;
      ENode visit(const ir::ValueDecl::ValueDeclPtr &node) override;
      ENode visit(const ir::IfStatement::IfStatementPtr &node) override;
      ENode visit(const ir::TryCatch::TryCatchPtr &node) override;
      ENode visit(const ir::ForeachStatement::ForeachStatementPtr &node) override;
      ENode visit(const ir::LoopStatement::LoopStatementPtr &node) override;
      ENode visit(const ir::StatementBlock::StatementBlockPtr &node) override;
      ENode visit(const ir::ProcessDecl::ProcessDeclPtr &node) override;
      ENode visit(const ir::ProcessBlock::ProcessBlockPtr &node) override;
      ENode visit(const ir::ProcessConstructor::ProcessConstructorPtr &node) override;
      ENode visit(const EProgram &node) override;

      /**
       * Get the current environment.
       */
    protected:
      inline ::std::shared_ptr<Environment> current() const
      {
        return env;
      }

      /** Transform with a new environment */
    protected:
      Tx<EStatement> transformWithEnv(const ::std::shared_ptr<Environment> &new_env,
          EStatement stmt);

      /** Transform with a new environment */
    protected:
      Tx<EExpression> transformWithEnv(const ::std::shared_ptr<Environment> &new_env,
          EExpression expr);

      /** Transform with a new environment */
    protected:
      Tx<ENode> transformNodeWithEnv(const ::std::shared_ptr<Environment> &new_env, ENode node);

      /** The current environment */
    private:
      ::std::shared_ptr<Environment> env;
    };
  }
}
#endif
