#include <mylang/ethir/Tag.h>
#include <iostream>

namespace mylang {
  namespace ethir {

    Tag::Tag(mylang::names::Name::Ptr name)
        : _source(name)
    {
    }

    Tag::Tag(const Tag &name)
        : _source(name._source)
    {
    }

    Tag::~Tag()
    {
    }

    Tag& Tag::operator=(const Tag &name)
    {
      _source = name._source;
      return *this;
    }

    Tag Tag::create(mylang::names::Name::Ptr src)
    {
      return Tag(src);
    }

    Tag Tag::create(const ::std::string &prefix)
    {
      return create(mylang::names::Name::create(prefix));
    }

    bool Tag::equals(const Tag &name) const
    {
      return _source == name._source;
    }
    bool Tag::less(const Tag &name) const
    {
      if (_source < name._source) {
        return true;
      }
      return false;
    }

    int Tag::compare(const Tag &name) const
    {
      if (less(name)) {
        return -1;
      } else if (equals(name)) {
        return 0;
      } else {
        return 1;
      }
    }

    ::std::string Tag::toString() const
    {
      return _source->uniqueFullName(".");
    }

  }
}
