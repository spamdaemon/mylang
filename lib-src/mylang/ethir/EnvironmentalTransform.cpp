#include "EnvironmentalTransform.h"

namespace mylang {
  namespace ethir {
    using namespace ir;

    EnvironmentalTransform::EnvironmentalTransform(Environment::Ptr initial)
        : env(initial ? initial : ::std::make_shared<Environment>())
    {
    }

    EnvironmentalTransform::EnvironmentalTransform()
        : env(::std::make_shared<Environment>())
    {
    }
    EnvironmentalTransform::~EnvironmentalTransform()
    {
    }

    ENode EnvironmentalTransform::visit(const ir::LambdaExpression::LambdaExpressionPtr &node)
    {
      auto restore = env;
      env = env->create();
      auto res = Transform::visit(node);
      env = restore;
      return res;
    }

    ENode EnvironmentalTransform::visit(const ir::Loop::LoopPtr &node)
    {
      auto restore = env;
      env = env->create();

      Loop::States states;

      bool mkNew = false;
      // inject the loop variables into the environment

      // now, process each state within its own environment
      for (auto &s : node->states) {
        auto step = transformNodeWithEnv(env->create(), s.second);
        mkNew = mkNew || step.isNew;
        states.emplace(s.first, step.actual->self<ir::Loop::Step>());
      }
      Loop::LoopPtr res;
      if (mkNew) {
        res = Loop::create(node->start, ::std::move(states));
      }
      env = restore;
      return res;
    }

    ENode EnvironmentalTransform::visit(const ir::LetExpression::LetExpressionPtr &node)
    {
      auto value = transformWithEnv(env->create(), node->value);
      env->addBinding(node->variable, value.actual);
      auto in = transformExpr(node->result);

      if (value.isNew || in.isNew) {
        return ir::LetExpression::create(node->variable, value.actual, in.actual);
      }
      return nullptr;
    }

    ENode EnvironmentalTransform::visit(const ir::GlobalValue::GlobalValuePtr &node)
    {
      auto value = transformExpr(node->value);
      env->addBinding(node->variable, value.actual);
      if (value.isNew) {
        return GlobalValue::create(node->name, value.actual, node->kind);
      }
      return nullptr;
    }

    ENode EnvironmentalTransform::visit(const ir::ProcessValue::ProcessValuePtr &node)

    {
      if (node->value) {
        auto value = transformExpr(node->value);
        env->addBinding(node->variable, value.actual);
        if (value.isNew) {
          return ProcessValue::create(node->name, value.actual);
        }
      }
      return nullptr;
    }

    ENode EnvironmentalTransform::visit(const ir::ProcessDecl::ProcessDeclPtr &node)
    {
      auto restore = env;

      env = env->create();
      for (auto pv : node->variables) {
        auto v = pv->self<ProcessValue>();
        if (v && v->value) {
          env->addBinding(v->variable, v->value);
        }
      }

      auto res = Transform::visit(node);
      env = restore;
      return res;
    }

    ENode EnvironmentalTransform::visit(const ir::ProcessBlock::ProcessBlockPtr &node)
    {
      auto restore = env;
      env = env->create();
      auto res = Transform::visit(node);
      env = restore;
      return res;
    }
    ENode EnvironmentalTransform::visit(const ir::ProcessConstructor::ProcessConstructorPtr &node)
    {
      auto restore = env;
      env = env->create();
      auto res = Transform::visit(node);
      env = restore;
      return res;
    }

    ENode EnvironmentalTransform::visit(const ir::ValueDecl::ValueDeclPtr &node)
    {
      auto value = transformExpr(node->value);
      if (value.actual) {
        env->addBinding(node->variable, value.actual);
      }
      auto next = transformStatement(node->next);
      if (value.isNew || next.isNew) {
        return ValueDecl::createSimplified(node->variable, value.actual, next.actual);
      }
      return nullptr;
    }

    ENode visit(const ir::VariableUpdate::VariableUpdatePtr &node)
    {
      auto target = node->target;
      throw ::std::runtime_error(
          "There should be no variable updates in SSA:" + target->name.toString());
    }

    ENode EnvironmentalTransform::visit(const ir::IfStatement::IfStatementPtr &node)
    {
      // need to process next first, because there may be phi functions referencing
      // variables in an if-branch
      auto cond = transformExpression(node->condition);
      auto next = transformWithEnv(env->create(), node->next);

      auto iftrue = transformWithEnv(env->create(), node->iftrue);
      auto iffalse = transformWithEnv(env->create(), node->iffalse);

      if (cond.isNew || next.isNew || iftrue.isNew || iffalse.isNew) {
        return IfStatement::create(cond.actual, iftrue.actual, iffalse.actual, next.actual);
      }
      return nullptr;
    }

    ENode EnvironmentalTransform::visit(const ir::TryCatch::TryCatchPtr &node)
    {
      // need to process next first, because there may be phi functions referencing
      // variables in an if-branch
      auto next = transformWithEnv(env->create(), node->next);

      auto catchBlock = transformWithEnv(env->create(), node->catchBody);
      auto tryBlock = transformWithEnv(env->create(), node->tryBody);

      if (tryBlock.isNew || catchBlock.isNew || next.isNew) {
        return TryCatch::create(tryBlock.actual, catchBlock.actual, next.actual);
      }
      return nullptr;
    }

    ENode EnvironmentalTransform::visit(const ir::ForeachStatement::ForeachStatementPtr &node)
    {
      // need to process next first, because there may be phi functions referencing
      // variables in an if-branch
      auto data = transformExpression(node->data);
      auto next = transformWithEnv(env->create(), node->next);
      auto body = transformWithEnv(env->create(), node->body);

      if (data.isNew || body.isNew || next.isNew) {
        return ForeachStatement::create(node->name, node->variable, data.actual, body.actual,
            next.actual);
      }
      return nullptr;
    }

    ENode EnvironmentalTransform::visit(const ir::LoopStatement::LoopStatementPtr &node)
    {
      // need to process next first
      auto next = transformWithEnv(env->create(), node->next);
      auto body = transformWithEnv(env->create(), node->body);

      if (body.isNew || next.isNew) {
        return LoopStatement::create(node->name, body.actual, next.actual);
      }
      return nullptr;
    }

    ENode EnvironmentalTransform::visit(const ir::StatementBlock::StatementBlockPtr &node)
    {
      // need to process next first, because there may be phi functions referencing
      // variables in an if-branch
      auto next = transformWithEnv(env->create(), node->next);
      auto body = transformWithEnv(env->create(), node->statements);

      if (body.isNew || next.isNew) {
        return StatementBlock::create(body.actual, next.actual);
      }
      return nullptr;
    }

    ENode EnvironmentalTransform::visit(const EProgram &node)
    {
      for (auto g : node->globals) {
        env->addBinding(g->variable, g->value);
      }
      return Transform::visit(node);
    }

    Transform::Tx<EStatement> EnvironmentalTransform::transformWithEnv(
        const ::std::shared_ptr<Environment> &new_env, EStatement stmt)
    {
      auto restore = env;
      env = new_env;
      auto res = transformStatement(stmt);
      env = restore;
      return res;
    }

    Transform::Tx<EExpression> EnvironmentalTransform::transformWithEnv(
        const ::std::shared_ptr<Environment> &new_env, EExpression expression)
    {
      auto restore = env;
      env = new_env;
      auto res = transformExpr(expression);
      env = restore;
      return res;
    }

    Transform::Tx<ENode> EnvironmentalTransform::transformNodeWithEnv(
        const ::std::shared_ptr<Environment> &new_env, ENode node)
    {
      auto restore = env;
      env = new_env;
      auto res = transformNode(node);
      env = restore;
      return res;
    }

  }

}
