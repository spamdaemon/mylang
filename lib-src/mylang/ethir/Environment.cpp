#include <mylang/ethir/Environment.h>

namespace mylang {
  namespace ethir {

    Environment::Environment()
        : _parent(nullptr)
    {
    }

    Environment::Environment(Ptr x_parent)
        : _parent(x_parent)
    {
    }

    Environment::~Environment()
    {
    }

    Environment::Ptr Environment::create()
    {
      return ::std::make_shared<Environment>(shared_from_this());
    }

    void Environment::addBinding(const Name &name,
        ::std::optional<mylang::ethir::ir::Variable::Scope> scope, const EExpression &value)
    {
      bindings[name] = Binding { scope, value };
    }

    void Environment::addBinding(const Name &name, const EExpression &value)
    {
      addBinding(name, ::std::nullopt, value);
    }

    void Environment::addBinding(const EVariable &v, const EExpression &value)
    {
      addBinding(v->name, v->scope, value);
    }

    ::std::optional<Environment::Binding> Environment::lookupBinding(const Name &name) const
    {
      const Environment *THIS = this;
      do {
        auto i = THIS->bindings.find(name);
        if (i != THIS->bindings.end()) {
          return i->second;
        }
        THIS = THIS->_parent.get();
      } while (THIS);
      return ::std::nullopt;
    }

    EExpression Environment::lookup(const Name &name) const
    {
      auto binding = lookupBinding(name);
      if (binding.has_value()) {
        return binding.value().value;
      } else {
        return nullptr;
      }
    }

  }
}
