#ifndef CLASS_MYLANG_ETHIR_TYPENAME_H
#define CLASS_MYLANG_ETHIR_TYPENAME_H

#ifndef CLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif

namespace mylang {
  namespace ethir {

    class TypeName
    {
      /** Create a name for a global name */
    private:
      TypeName(mylang::names::Name::Ptr name);

    public:
      TypeName(const TypeName &name);
      TypeName& operator=(const TypeName &name);
      ~TypeName();

      /**
       * Create a new name from a global name.
       * @param src the source name
       */
    public:
      static TypeName create(mylang::names::Name::Ptr src);

      /**
       * Create an anonymous typename.
       */
    public:
      static TypeName create();

      /**
       * Create a temporary name.
       * @param prefix an optional prefix
       */
    public:
      static TypeName create(const ::std::string &prefix);

      /**
       * Compare for equality.
       * @param name a name
       * @return true if equal, false otherwise
       */
    public:
      bool equals(const TypeName &name) const;

      /**
       * Compare for less than.
       * @param name a name
       * @return true if this name is less than the specified name.
       */
    public:
      bool less(const TypeName &name) const;

      /**
       * Compare this name to the specified name.
       * @param name a name
       * @return -1 if this is  < , 0 if equal, 1 if greater
       */
    public:
      int compare(const TypeName &name) const;

      friend inline bool operator<(const TypeName &a, const TypeName &b)
      {
        return a.less(b);
      }

      friend inline bool operator==(const TypeName &a, const TypeName &b)
      {
        return a.equals(b);
      }

      friend inline bool operator!=(const TypeName &a, const TypeName &b)
      {
        return !a.equals(b);
      }

      friend inline ::std::ostream& operator<<(::std::ostream &out, const TypeName &name)
      {
        return out << name._source;
      }

    public:
      inline const mylang::names::Name::Ptr& source() const
      {
        return _source;
      }

      /** The globally unique name on which this name is based */
    private:
      mylang::names::Name::Ptr _source;
    };

  }
}
#endif
