#include <mylang/ethir/HashCode.h>
#include <mylang/ethir/Name.h>
#include <mylang/ethir/Tag.h>
#include <mylang/ethir/ir/Node.h>
#include <mylang/ethir/types/Type.h>

namespace mylang {
  namespace ethir {

    HashCode& HashCode::mix(const HashCode &hc)
    {
      auto lo = (hashcode & 0x0ffffffffL) << 32;
      auto hi = (hashcode >> 32) & 0x0ffffffffL;
      hashcode = (lo | hi) ^ hc.hashcode;
      return *this;
    }

    HashCode& HashCode::mix(::std::uint64_t v)
    {
      auto lo = (hashcode & 0x0ffffffffL) << 32;
      auto hi = (hashcode >> 32) & 0x0ffffffffL;
      hashcode = (lo | hi) ^ v;
      return *this;
    }

    HashCode& HashCode::mix(const void *v)
    {
      auto lo = (hashcode & 0x0ffffffffL) << 32;
      auto hi = (hashcode >> 32) & 0x0ffffffffL;
      hashcode = (lo | hi) ^ reinterpret_cast<::std::uint64_t>(v);
      return *this;
    }

    HashCode& HashCode::mix(const EType &value)
    {
      if (value) {
        return mix(value->hashcode());
      } else {
        return mix(HashCode());
      }
    }

    HashCode& HashCode::mix(const ENode &value)
    {
      if (value) {
        return mix(value->hashcode());
      } else {
        return mix(HashCode());
      }
    }

    HashCode& HashCode::mix(bool v)
    {
      auto lo = (hashcode & 0x0ffffffffL) << 32;
      auto hi = (hashcode >> 32) & 0x0ffffffffL;
      // pick some random numbers
      hashcode = (v ? 0xac9388fd6f84beccL : 0x0a02a5769991db9aL) ^ lo ^ hi;
      return *this;
    }

    HashCode& HashCode::mix(const ::std::string &v)
    {
      int i = 0;
      for (auto ch : v) {
        hashcode = hashcode ^ ((::std::uint64_t) ch << i);
        i = (i + 8) % 64;
      }
      return *this;
    }

    HashCode& HashCode::mixString(const char *v)
    {
      int i = 0;
      for (const char *ch = v; *ch; ++ch) {
        hashcode = hashcode ^ ((::std::uint64_t) *ch << i);
        i = (i + 8) % 64;
      }
      return *this;
    }

    HashCode& HashCode::mix(const Tag &tag)
    {
      return mix(tag.source());
    }

    HashCode& HashCode::mix(const Name &name)
    {
      mix(name.version());
      return mix(name.source());
    }

    HashCode& HashCode::mix(const mylang::names::Name::Ptr &name)
    {
      return mix(name->fullName("."));
    }

    HashCode& HashCode::mix(const ::std::type_info &id)
    {
      return mixString(id.name());
    }
    HashCode& HashCode::mix(const BigInt &value)
    {
      return mix(value.toString());
    }

    HashCode& HashCode::mix(const BigUInt &value)
    {
      return mix(value.toString());
    }

    HashCode& HashCode::mix(const BigReal &value)
    {
      return mix(value.toString());
    }

  }
}
