#ifndef CLASS_MYLANG_ETHIR_ANALYSIS_TYPEANALYSIS_H
#define CLASS_MYLANG_ETHIR_ANALYSIS_TYPEANALYSIS_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#include <map>

namespace mylang {
  namespace ethir {
    namespace analysis {

      /**
       * Type analysis will determines the most constrained type for a variable.
       */
      class TypeAnalysis
      {
        /** A mapping for each variable name to the statement block in which it must be declared */
      public:
        typedef ::std::map<NamePtr, EType> Types;

      private:
        ~TypeAnalysis() = delete;

        /**
         * Analysis the code below the node and return the most
         * constrained type for each variable
         * @param node a node
         * @return
         */
      public:
        static Types get(const ENode &node);

      };
    }
  }
}
#endif
