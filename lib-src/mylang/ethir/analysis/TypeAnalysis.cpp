#include <mylang/ethir/analysis/TypeAnalysis.h>
#include <mylang/ethir/ir/DefaultNodeVisitor.h>
#include <mylang/ethir/ir/GlobalValue.h>
#include <mylang/ethir/ir/IfStatement.h>
#include <mylang/ethir/ir/LambdaExpression.h>
#include <mylang/ethir/ir/LoopStatement.h>
#include <mylang/ethir/ir/OperatorExpression.h>
#include <mylang/ethir/ir/Parameter.h>
#include <mylang/ethir/ir/Phi.h>
#include <mylang/ethir/ir/ProcessValue.h>
#include <mylang/ethir/ir/ProcessVariable.h>
#include <mylang/ethir/ir/ProcessVariableUpdate.h>
#include <mylang/ethir/ir/ValueDecl.h>
#include <mylang/ethir/ir/Variable.h>
#include <mylang/ethir/Name.h>
#include <mylang/ethir/types/Type.h>
#include <mylang/ethir/queries/FindDefinedVariables.h>
#include <stddef.h>
#include <iterator>
#include <memory>
#include <optional>
#include <utility>
#include <vector>

namespace mylang {
  namespace ethir {
    using namespace ir;
    using namespace types;
    namespace analysis {

      static const bool DEBUG_IT = false;

      namespace {

        // capture the possible values a type my take on
        struct SumType
        {
          SumType()
          {
          }
          SumType(const EType &e)
              : types( { e })
          {
          }

          void add(const EType &e)
          {
            types.push_back(e);
            reduce();
          }

          void add(const SumType &e)
          {
            types.insert(types.end(), e.types.begin(), e.types.end());
            reduce();
          }

          friend ::std::ostream& operator<<(::std::ostream &out, const SumType &ty)
          {
            out << "[ ";
            for (auto t : ty.types) {
              out << t->toString() << ",";
            }
            out << " ]";
            return out;
          }

          void reduce()
          {
            // stupid implementation
            for (size_t i = 0; i < types.size(); ++i) {
              bool inc = true;
              for (size_t j = i + 1; j < types.size(); ++j) {
                auto t = types[i]->joinWith(types[j]);
                if (t) {
                  types[i] = t;
                  types.erase(types.begin() + j);
                  inc = false;
                  break;
                }
              }
              if (inc) {
                ++i;
              } else {
                i = 0;
              }
            }
          }

          bool eq(const SumType &that) const
          {
            for (size_t i = 0; i < types.size(); ++i) {
              bool found = false;
              for (size_t j = 0; j < that.types.size(); ++j) {
                if (types[i]->isSameType(*that.types[j])) {
                  found = true;
                  break;
                }
              }
              if (!found) {
                return false;
              }
            }

            return true;
          }

          bool operator==(const SumType &that) const
          {
            return eq(that) && that.eq(*this);
          }

          EType getType() const
          {
            EType res;
            for (EType t : types) {
              if (res) {
                res = t->unionWith(res);
              } else {
                res = t;
              }
            }
            return res;
          }

          ::std::vector<EType> types;
        };

        struct AllTypes
        {
          AllTypes()
              : changed(false)
          {
          }

          void put(const Name &name, const SumType &ty)
          {
            auto i = vars.find(name);
            if (i == vars.end()) {
              if (DEBUG_IT)
                ::std::cerr << "Putting new variable " << name << ": " << ty << ::std::endl;
              vars.insert( { name, ::std::move(ty) });
              changed = true;
            } else if (ty == i->second) {
              if (DEBUG_IT)
                ::std::cerr << "Ignoring redundant variable " << name << ": " << ty << ::std::endl;
            } else {
              if (DEBUG_IT)
                ::std::cerr << "Updating variable " << name << ": " << i->second << " --> " << ty
                    << ::std::endl;
              i->second = ::std::move(ty);
              changed = true;
            }
          }

          SumType* get(const Name &name)
          {
            auto i = vars.find(name);
            if (i == vars.end()) {
              return nullptr;
            } else {
              return &i->second;
            }
          }

          ::std::map<Name, SumType> vars;
          bool changed;
        };

        struct TConstraint
        {

          void put(const Name &varName, const EType &varType)
          {
            constraints[varName].add(varType);
          }

          ::std::map<Name, SumType> constraints;

        };

        // the constraint context keeps track of the possible types a variable may take on
        // a relational operator;
        // e.g. b:bool = v:integer < 10
        // keeps associated the type (;9) with the true b==true, and (10;) with b==false
        // any time we use a variable in the IR, we can walk the context tree to find out what type
        // it should be
        struct ConstraintContext: public std::enable_shared_from_this<ConstraintContext>
        {
          typedef ::std::pair<Name, bool> Key;
          ConstraintContext()
          {
          }

          ConstraintContext(const ::std::shared_ptr<ConstraintContext> &p,
              const ::std::optional<Key> &xchoice)
              : parent(p), choice(xchoice)
          {
          }

          // this function is called whenever we create a boolean value from a comparison
          void put(const Name &boolVar, const Name &varName, const EType &ifTrue,
              const EType &ifFalse)
          {
            if (ifTrue) {
              if (DEBUG_IT)
                ::std::cerr << "TRUE-constraint: " << varName << " :: " << ifTrue << ::std::endl;
              choices[ { boolVar, true }].put(varName, ifTrue);
            }
            if (ifFalse) {
              if (DEBUG_IT)
                ::std::cerr << "FALSE-constraint: " << varName << " :: " << ifFalse << ::std::endl;
              choices[ { boolVar, false }].put(varName, ifFalse);
            }
          }

          ::std::shared_ptr<ConstraintContext> branch(const Name &boolVar, bool boolVal)
          {
            return ::std::make_shared<ConstraintContext>(shared_from_this(),
                Key { boolVar, boolVal });
          }

          ::std::shared_ptr<ConstraintContext> branch()
          {
            return ::std::make_shared<ConstraintContext>(shared_from_this(), ::std::nullopt);
          }

          // get the sumtype associated with the specified variable in this context
          // we may need to do some caching to improve this
          SumType* get(const Name &varName)
          {
            if (choice.has_value()) {
              // ::std::cerr << "Looking for " << varName << "with choice " << choice.value().first
              // << ", " << choice.value().second << ::std::endl;
              auto THIS = shared_from_this();
              while (THIS->parent) {
                auto i = THIS->parent->choices.find(choice.value());
                if (i != THIS->parent->choices.end()) {
                  auto j = i->second.constraints.find(varName);
                  if (j != i->second.constraints.end()) {
                    return &j->second;
                  }
                }
                // ask the parent if there is a variable with that name
                THIS = THIS->parent;
              }
            }
            if (parent) {
              // could not find anything based on the choice in this context
              return parent->get(varName);
            }
            return nullptr;
          }

          ::std::shared_ptr<ConstraintContext> parent;
          ::std::map<Key, TConstraint> choices; // constraints defined in this context
          const ::std::optional<Key> choice; // the choice we've made in this context
        };

        // a visitor that computes the SumType for a given expression
        struct EvalVisitor
        {

        };

        // visit the entire tree
        struct Visitor: public DefaultNodeVisitor
        {
          Visitor()
          {
            sumtype = &defaultSumType;
          }

          ~Visitor()
          {
          }

          void visitPhi(Phi::PhiPtr expr) override
          {
            SumType res;
            for (auto e : expr->arguments) {
              auto c = typeOf(e);
              if (c) {
                res.add(*c);
              } else {
//                return;
              }
            }
            // ::std::cerr << "PHI : " << expr->rootName() << " :: " << res << ::std::endl;
            *sumtype = ::std::move(res);
          }

          void updateRelBounds(const OperatorExpression::OperatorExpressionPtr &expr,
              const Name &boolVar)
          {
            if (expr->arguments.size() != 2) {
              return;
            }
            auto left = expr->arguments.at(0);
            auto right = expr->arguments.at(1);
            EType lIfTrue, lIfFalse, rIfTrue, rIfFalse;
            auto l = findSumType(left);
            auto r = findSumType(right);
            if (l.has_value() && r.has_value()) {
              switch (expr->name) {
              case OperatorExpression::OP_EQ:
                l->getType()->inferConstraints(Type::OP_EQ, r->getType(), lIfTrue, lIfFalse,
                    rIfTrue, rIfFalse);
                break;
              case OperatorExpression::OP_NEQ:
                l->getType()->inferConstraints(Type::OP_NEQ, r->getType(), lIfTrue, lIfFalse,
                    rIfTrue, rIfFalse);
                break;
              case OperatorExpression::OP_LT:
                l->getType()->inferConstraints(Type::OP_LT, r->getType(), lIfTrue, lIfFalse,
                    rIfTrue, rIfFalse);
                break;
              case OperatorExpression::OP_LTE:
                l->getType()->inferConstraints(Type::OP_LTE, r->getType(), lIfTrue, lIfFalse,
                    rIfTrue, rIfFalse);
                break;
              default:
                break;
              }
              context->put(boolVar, left->name, lIfTrue, lIfFalse);
              context->put(boolVar, right->name, rIfTrue, rIfFalse);
            }
          }

          void visitOperatorExpression(OperatorExpression::OperatorExpressionPtr expr) override
          {

            ::std::vector<SumType> args;
            bool makeNew = false;
            for (auto arg : expr->arguments) {
              auto tmp = findSumType(arg);
              if (tmp.has_value()) {
                args.push_back(tmp.value());
                makeNew = true;
              } else {
                args.push_back(SumType(arg->type));
              }
            }
            if (!makeNew) {
              *sumtype = ::std::nullopt;
              return;
            }

            if (DEBUG_IT) {
              ::std::cerr << "OP " << OperatorExpression::operatorName(expr->name) << ":"
                  << ::std::endl;
              for (const auto &arg : args) {
                ::std::cerr << " arg : " << arg << ::std::endl;
              }
            }
            struct Helper
            {
              static void findSumTypes(SumType &out, OperatorExpression::Types &argTypes,
                  OperatorExpression::Operator op, const ::std::vector<SumType> &xargs, size_t i)
              {
                if (i >= xargs.size()) {
                  auto ty = OperatorExpression::typeFor(op, argTypes, nullptr);
                  if (ty) {
                    out.add(ty);
                  }
                  return;
                }
                for (auto &ty : xargs[i].types) {
                  argTypes[i] = ty;
                  findSumTypes(out, argTypes, op, xargs, i + 1);
                }
              }
            };

            OperatorExpression::Types argTypes;
            SumType resTy;
            argTypes.resize(args.size());
            Helper::findSumTypes(resTy, argTypes, expr->name, args, 0);
            if (!resTy.types.empty()) {
              *sumtype = resTy;
              if (DEBUG_IT)
                ::std::cerr << "== " << sumtype->value() << ::std::endl;
            } else {
              *sumtype = ::std::nullopt;
              if (DEBUG_IT)
                ::std::cerr << "== NULL" << ::std::endl;
            }

          }

          void visitLiteralInteger(const ir::LiteralInteger::LiteralIntegerPtr node)
          override
          {
            *sumtype = SumType(node->getSourceType());
          }

          void visitLambdaExpression(LambdaExpression::LambdaExpressionPtr expr)
          override
          {
            auto ctx = context;
            context = context->branch();
            for (auto i : expr->parameters) {
              visitNode(i);
            }
            visitNode(expr->body);
            *sumtype = ::std::nullopt;
            context = ctx;
          }

          SumType* typeOf(const Variable::VariablePtr &expr)
          {
            // see if we have any constraints on the variable
            SumType *c = context->get(expr->name);
            if (DEBUG_IT)
              ::std::cerr << (c ? "" : "not ") << "in context: " << expr->name << ::std::endl;
            if (!c) {
              c = types.get(expr->name);
              if (DEBUG_IT)
                ::std::cerr << (c ? "" : "not ") << "mapped: " << expr->name << ::std::endl;
            }

            return c;
          }

          void visitVariable(Variable::VariablePtr expr)
          override
          {
            auto c = typeOf(expr);
            if (c) {
              *sumtype = *c;
            } else {
              *sumtype = SumType(expr->type);
            }
          }

          void visitIfStatement(IfStatement::IfStatementPtr stmt)
          override
          {
            auto ctx = context;
            context = ctx->branch(stmt->condition->name, true);
            visitNode(stmt->iftrue);
            context = ctx->branch(stmt->condition->name, false);
            visitNode(stmt->iffalse);
            context = ctx;
            visitNode(stmt->next);
          }
          void visitLoopStatement(LoopStatement::LoopStatementPtr stmt)
          override
          {
            auto ctx = context;
            context = ctx->branch();
            visitNode(stmt->body);
            context = ctx;
            visitNode(stmt->next);
          }

          void visitProcessVariable(ProcessVariable::ProcessVariablePtr stmt)
          override
          {
            auto i = types.get(stmt->name);
            if (i == nullptr) {
              types.put(stmt->name, stmt->type);
            }
            if (stmt->value) {
              auto tmp = findSumType(stmt->value);
              if (tmp.has_value()) {
                types.put(stmt->name, tmp.value());
              }
            }
            visitNode(stmt->next);
          }

          void visitProcessValue(ProcessValue::ProcessValuePtr stmt)
          override
          {
            initVar(*stmt);
          }

          void visitProcessVariableUpdate(ProcessVariableUpdate::ProcessVariableUpdatePtr stmt)
          override
          {
            auto tmp = findSumType(stmt->value);
            if (tmp.has_value()) {
              types.put(stmt->target->name, tmp.value());
            }
            visitNode(stmt->next);
          }

          void visitValueDecl(ValueDecl::ValueDeclPtr stmt)
          override
          {
            initVar(*stmt);
          }

          void visitGlobalValue(GlobalValue::GlobalValuePtr node)
          override
          {
            initVar(*node);
          }

          void visitParameter(Parameter::ParameterPtr param)
          override
          {
            auto i = types.get(param->variable->name);
            if (i == nullptr) {
              types.put(param->variable->name, param->variable->type);
            }
          }

          void initVar(const AbstractValueDeclaration &decl)
          {
            auto i = types.get(decl.name);
            if (i == nullptr) {
              types.put(decl.name, decl.type);
            }
            auto tmp = findSumType(decl.value);
            if (tmp.has_value()) {
              if (DEBUG_IT)
                ::std::cerr << "Setting " << decl.name << " to " << tmp.value() << ::std::endl;
              types.put(decl.name, tmp.value());
            }
            if (decl.value) {
              auto op = decl.value->self<OperatorExpression>();
              if (op) {
                updateRelBounds(op, decl.name);
              }
            }
            visitNode(decl.next);
          }

          void visitNode(const ENode &node)
          override final
          {
            if (node) {
              ::std::optional<SumType> *bak = sumtype;
              defaultSumType = ::std::nullopt;
              sumtype = &defaultSumType;
              DefaultNodeVisitor::visitNode(node);
              sumtype = bak;
            }
          }

          ::std::optional<SumType> findSumType(const ENode &node)
          {
            ::std::optional<SumType> *bak = sumtype;
            ::std::optional<SumType> res;
            sumtype = &res;
            DefaultNodeVisitor::visitNode(node);
            sumtype = bak;
            return res;
          }

          AllTypes types;
          ::std::shared_ptr<ConstraintContext> context;

          ::std::optional<SumType> *sumtype;
          ::std::optional<SumType> defaultSumType;
        }
        ;

        static EType getType(const SumType &t)
        {
          return t.getType();
        }
        static EType getType(const EVariable &t)
        {
          return t->type;
        }

        template<class T>
        bool copyTypes(const T &from, TypeAnalysis::Types &res)
        {
          bool changed = false;
          for (const auto &e : from) {
            auto name = e.first.source();
            auto i = res.find(name);
            auto ty = getType(e.second);
            if (i == res.end()) {
              res[name] = ty;
              changed = true;
            } else {
              auto newTy = ty->unionWith(i->second);
              if (!newTy->isSameType(*i->second)) {
                res[name] = newTy;
                changed = true;
              }
            }
          }
          return changed;
        }
      }

      TypeAnalysis::Types TypeAnalysis::get(const ENode &node)
      {
        TypeAnalysis::Types initial, latest;

        auto allVars = queries::FindDefinedVariables::find(node);
        copyTypes(allVars, initial);

        Visitor v;
        // the main loop runs until we've reached a fixed point, i.e.,
        // there are no changes to the types we've discovered
        size_t loopCounter = 0;
        size_t maxLoops = 20;
        do {
          v.context = ::std::make_shared<ConstraintContext>();
          v.types.changed = false;
          node->accept(v);
          // ::std::cerr << "Type analysis loop " << loopCounter << ::std::endl;
          if (loopCounter++ > maxLoops) {
            // ::std::cerr << "Type analysis; too many loops" << ::std::endl;
            break;
          }
        } while (v.types.changed);

        copyTypes(v.types.vars, latest);

        ::std::cerr << "\n\n*****TYPE ANALYSIS RESULTS *******\n";
        for (auto e : initial) {
          auto i = latest.find(e.first);
          if (i != latest.end()) {
            if (i->second->isSameType(*e.second)) {
              latest.erase(i);
            } else {
              ::std::cerr << i->first->uniqueFullName(".") << " : " << e.second->toString()
                  << " -> " << i->second->toString() << ::std::endl;
            }
          }
        }
        ::std::cerr << "************************************\n\n";

        return latest;
      }
    }
  }
}
