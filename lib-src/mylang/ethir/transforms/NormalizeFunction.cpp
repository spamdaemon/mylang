#include <mylang/ethir/transforms/NormalizeFunction.h>
#include <mylang/ethir/Transform.h>
#include <mylang/ethir/ir/nodes.h>

#include <cassert>
#include <functional>
#include <map>
#include <optional>

namespace mylang {
  namespace ethir {
    using namespace ir;
    using namespace types;
    namespace transforms {

      namespace {

        struct Pass: public Transform
        {
          Pass(const EType &outputType)
              : output(outputType->isVoid() ? EVariable() : Variable::createLocal(outputType)),
                  needReturn(Variable::createLocal(types::BooleanType::create())),
                  falseValue(Variable::createLocal(types::BooleanType::create()))
          {
          }

          ~Pass()
          {
          }

          void initOutput(StatementBuilder &sb)
          {
            auto trueValue = sb.declareLocalValue(LiteralBoolean::create(true));
            sb.declareValue(falseValue, LiteralBoolean::create(false));
            sb.declareVariable(needReturn, trueValue);
            if (output) {
              auto nil = sb.declareLocalValue(NoValue::create(output->type));
              sb.declareVariable(output, nil);
            }
          }

          EVariable getOutput(StatementBuilder&)
          {
            return output;
          }

          EStatement guardStatement(EStatement body, EStatement optElse)
          {
            return IfStatement::create(needReturn, body, optElse, nullptr);
          }

          ENode visit(const ReturnStatement::ReturnStatementPtr &node)
          override final
          {
            StatementBuilder sb;
            if (output) {
              sb.appendUpdate(output, node->value);
            }
            sb.appendUpdate(needReturn, falseValue);

            return sb.build(breakStmt);
          }

          ENode visit(const LambdaExpression::LambdaExpressionPtr&)
          override final
          {
            // do not traverse into a lambda function
            return nullptr;
          }

          ENode visit(const IfStatement::IfStatementPtr &node)
          {
            // no need to transform the condition
            auto ift = transformStatement(node->iftrue);
            auto iff = transformStatement(node->iffalse);
            auto next = transformStatement(node->next);

            if (ift.isNew || iff.isNew || next.isNew) {

              auto actualNext = next.actual;

              // we have two options here:
              // 1. we could just keep the if as-is and add a condition
              //    execute next only if the condition is true
              // 2. we could pull next into each branch
              //

              // I'm choosing #1 here, to keep the code as small as possible
              // so we wrap the next in a condition
              if (ift.isNew || iff.isNew) {
                actualNext = guardStatement(actualNext, breakStmt);
              }
              return IfStatement::create(node->condition, ift.actual, iff.actual, actualNext);
            }

            return nullptr;
          }

          ENode visit(const TryCatch::TryCatchPtr &node)
          {
            // no need to transform the condition
            auto tryBody = transformStatement(node->tryBody);
            auto catchBody = transformStatement(node->catchBody);
            auto next = transformStatement(node->next);

            if (tryBody.isNew || catchBody.isNew || next.isNew) {

              auto actualNext = next.actual;

              // we have two options here:
              // 1. we could just keep the if as-is and add a condition
              //    execute next only if the condition is true
              // 2. we could pull next into each branch
              //

              // I'm choosing #1 here, to keep the code as small as possible
              // so we wrap the next in a condition
              if (tryBody.isNew || catchBody.isNew) {
                actualNext = guardStatement(actualNext, breakStmt);
              }
              return TryCatch::create(tryBody.actual, catchBody.actual, actualNext);
            }

            return nullptr;
          }

          ENode visit(const ForeachStatement::ForeachStatementPtr &node)
          {
            ENode res;
            const EStatement breakStmtBak = breakStmt;
            breakStmt = BreakStatement::create(node->name);
            auto body = transformStatement(node->body);
            breakStmt = breakStmtBak;

            auto next = transformStatement(node->next);
            if (body.isNew || next.isNew) {
              auto actualNext = next.actual;
              if (body.isNew) {
                actualNext = guardStatement(actualNext, breakStmt);
              }
              res = ForeachStatement::create(node->name, node->variable, node->data, body.actual,
                  actualNext);
            }

            return res;
          }

          ENode visit(const LoopStatement::LoopStatementPtr &node)
          {
            ENode res;
            const EStatement breakStmtBak = breakStmt;
            breakStmt = BreakStatement::create(node->name);
            auto body = transformStatement(node->body);
            breakStmt = breakStmtBak;

            auto next = transformStatement(node->next);
            if (body.isNew || next.isNew) {
              auto actualNext = next.actual;
              if (body.isNew) {
                actualNext = guardStatement(actualNext, breakStmt);
              }
              res = LoopStatement::create(node->name, body.actual, actualNext);
            }

            return res;
          }

          /** The output of the function (nullptr if void) */
          const EVariable output;

          /** The value that keeps track of wheter we need a return or not */
          const EVariable needReturn;

          /** The value that holds a false value */
          const EVariable falseValue;

          /** A break statement to leave the outer most loop */
          EStatement breakStmt;
        };

      }

      TransformResult<ELambda> NormalizeFunction::normalize(ELambda node)
      {
        if (node->isNormalized()) {
          return {node,false};
        }

        Pass transform(node->signature()->returnType);
        StatementBuilder sb;
        transform.initOutput(sb);
        auto trueValue = sb.declareLocalValue(LiteralBoolean::create(true));
        sb.appendBlock(transform.transformStatement(node->body).actual);
        auto retValue = transform.getOutput(sb);
        sb.appendReturn(retValue);

        auto lambda = LambdaExpression::create(node->tag, node->signature(), node->parameters,
            sb.build());

        return {lambda,true};

      }

      TransformResult<ENode> NormalizeFunction::normalizeFunctions(ENode node)
      {
        struct Pass: public Transform
        {
          ~Pass()
          {
          }

          ENode visit(const LambdaExpression::LambdaExpressionPtr &node)
          override final
          {
            // first, recurse
            auto recNode = Transform::visit(node);

            NormalizeFunction nf;
            ENode res;
            if (recNode) {
              res = nf.normalize(recNode->self<LambdaExpression>()).actual;
            } else {
              res = nf.normalize(node).actual;
            }

            if (res != node) {
              return res;
            } else {
              return nullptr;
            }
          }
        };
        Pass p;
        return p.transformNode(node);

      }
    }
  }
}

