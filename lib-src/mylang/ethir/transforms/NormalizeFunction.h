#ifndef CLASS_MYLANG_ETHIR_TRANSFORMS_NORMALIZEFUNCTION_H
#define CLASS_MYLANG_ETHIR_TRANSFORMS_NORMALIZEFUNCTION_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TRANSFORMRESULT_H
#include<mylang/ethir/TransformResult.h>
#endif

namespace mylang {
  namespace ethir {
    namespace transforms {

      /**
       * Rewrite functions to have only a single return statement.
       */
      class NormalizeFunction
      {
        /** Destructor */
      public:
        ~NormalizeFunction() = default;

        /**
         * Normalize a lambda, such that there is a single return statement in the body of the lambda.
         * The returned lambda will NOT be in SSA form.
         * @param lambda a lambda
         * @return a transform result
         */
      public:
        TransformResult<ELambda> normalize(ELambda src);

        /**
         * Normalize all all lambda recursively.
         * @param src a node
         * @return a transform result
         */
      public:
        TransformResult<ENode> normalizeFunctions(ENode src);

      };
    }
  }
}
#endif
