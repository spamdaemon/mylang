#ifndef CLASS_MYLANG_ETHIR_TRANSFORMS_CHANGESIGNATURE_H
#define CLASS_MYLANG_ETHIR_TRANSFORMS_CHANGESIGNATURE_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TAG_H
#include <mylang/ethir/Tag.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_OPERATOREXPRESSION_H
#include <mylang/ethir/ir/OperatorExpression.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TYPES_FUNCTIONTYPE_H
#include <mylang/ethir/types/FunctionType.h>
#endif

#include <set>
#include <map>
#include <vector>

namespace mylang {
  namespace ethir {
    namespace transforms {

      /**
       * This transform replaces certain operators with functions that implement the operator
       * in terms of other functions.
       */
      class ChangeSignature
      {
        ChangeSignature& operator=(const ChangeSignature&) = delete;
        ChangeSignature(const ChangeSignature&) = delete;

        /** The update instruction */
      public:
        struct Change
        {
          Change(const mylang::ethir::Tag &xtag,
              mylang::ethir::types::FunctionType::FunctionTypePtr signature);

          /** The function tag we're supposed to use */
          mylang::ethir::Tag tag;

          /** The new function signature */
          mylang::ethir::types::FunctionType::FunctionTypePtr signature;

          /** The set of callsites to convert to this signature */
          ::std::set<mylang::ethir::ir::OperatorExpression::OperatorExpressionPtr> callsites;
        };

      public:
        typedef ::std::map<Name, ::std::vector<Change>> Changes;

      private:
        ~ChangeSignature() = delete;

        /**
         * Transforms the functions in the specified program.
         * @param prog a program
         * @return a program
         */
      public:
        static EProgram transform(EProgram prog, const Changes &updates);
      };
    }
  }
}
#endif
