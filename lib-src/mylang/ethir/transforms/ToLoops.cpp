#include <mylang/ethir/transforms/ToLoops.h>
#include <mylang/ethir/ir/StatementBuilder.h>
#include <mylang/ethir/transforms/RenameFreeVariables.h>
#include <mylang/ethir/types/types.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    using namespace ir;
    using namespace types;
    namespace transforms {

      namespace {

        static EExpression toExpression(EStatement s, EType retTy)
        {
          auto renamed = RenameFreeVariables::rename(s);

          EStatement body = renamed.result->self<Statement>();

          ::std::vector<Parameter::ParameterPtr> params;
          ::std::vector<EVariable> args;
          args.push_back(nullptr); // placeholder for target of the call
          for (auto &e : renamed.old2new) {
            args.push_back(e.first); // old variable becomes an argument
            EVariable newVar = e.second;
            params.push_back(Parameter::create(::std::move(newVar->name), newVar->type));
          }

          auto lambdaE = LambdaExpression::create(params, retTy, body);
          return LetExpression::create(lambdaE, [&](EVariable v) {
            args[0] = v; // first arg needs to be the target of the call
              return OperatorExpression::create(retTy,OperatorExpression::OP_CALL,args);
            });
        }

        static EExpression createEmptyArray(::std::shared_ptr<const ArrayType> arrTy)
        {
          assert(arrTy->minSize == 0);
          return ir::LiteralArray::create(arrTy, { });
        }

        static EExpression createSingletonArray(Variable::VariablePtr a)
        {
          auto arrTy = ArrayType::get(a->type, 1, 1);
          return ir::OperatorExpression::create(arrTy, ir::OperatorExpression::OP_NEW, a);
        }

        static EExpression doCheckedCast(EType target, Variable::VariablePtr var)
        {
          if (var->type->isSameType(*target)) {
            return var;
          }
          return ir::OperatorExpression::create(target, ir::OperatorExpression::OP_CHECKED_CAST,
              var);
        }
      }

      ToLoops::ToLoops()
      {
      }

      ToLoops::~ToLoops()
      {
      }

      ENode ToLoops::visit(const ir::ToArray::ToArrayPtr &e)
      {
        // the return type of the lambda we'll use
        auto retTy = e->type->self<ArrayType>();

        StatementBuilder b;
        // the array type where we accumulate the result
        // TODO: eventually we should use a buffer type
        auto accumTy = BuilderType::get(retTy);
        auto accumVar = b.declareLocalValue(accumTy, OperatorExpression::OP_NEW, { });

        // the accumulation action
        auto accumFn = [&](EVariable v, StatementBuilder &sb) {
          sb.declareLocalValue(OperatorExpression::OP_APPEND, {accumVar,v});
        };

        e->loop->toStatements(accumFn, b);
        auto res = b.declareLocalValue(OperatorExpression::OP_BUILD, { accumVar });
        b.appendReturn(res);
        auto expr = toExpression(b.build(), res->type);
        return expr;

      }

    }
  }
}
