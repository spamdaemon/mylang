#include <mylang/ethir/transforms/PruneProcessMembers.h>
#include <mylang/ethir/Visitor.h>
#include <mylang/ethir/Transform.h>
#include <mylang/ethir/queries/FindReachableVariables.h>
#include <mylang/ethir/ir/DefaultNodeVisitor.h>
#include <mylang/ethir/ir/nodes.h>

#include <cassert>
#include <set>

namespace mylang {
  namespace ethir {
    using namespace queries;
    using namespace ir;
    namespace transforms {
      namespace {

        struct FindUnusedMembersPass: public DefaultNodeVisitor
        {
          ~FindUnusedMembersPass()
          {
          }

          void visitProcessDecl(ProcessDecl::ProcessDeclPtr node) override
          {
            for (auto decl : node->variables) {
              unusedMembers.insert(decl->name);
            }
            DefaultNodeVisitor::visitProcessDecl(node);
          }

          void visitVariable(Variable::VariablePtr node) override
          {
            unusedMembers.erase(node->name);
          }

          void visitProcessVariable(ProcessVariable::ProcessVariablePtr node) override
          {
            visitNode(node->value);
            visitNode(node->next);
          }

          void visitProcessValue(ProcessValue::ProcessValuePtr node) override
          {
            visitNode(node->value);
            visitNode(node->next);
          }

          void visitProcessVariableUpdate(ProcessVariableUpdate::ProcessVariableUpdatePtr node)
          override
          {
            visitNode(node->value);
            visitNode(node->next);
          }

          ::std::set<Name> unusedMembers;
        };

        struct RemoveUnusedMembers: public Transform
        {
          RemoveUnusedMembers(const ::std::set<Name> &unused)
              : unusedMembers(unused)
          {
          }
          ~RemoveUnusedMembers()
          {
          }

          ENode visit(const ProcessDecl::ProcessDeclPtr &node) override
          {
            auto ports = node->publicPorts;
            ProcessDecl::Declarations vars;
            bool changed = false;
            for (auto decl : node->variables) {
              if (isUnused(decl->name)) {
                changed = true;
              } else {
                auto d2 = transformDeclaration(decl);
                changed = changed || d2.isNew;
                vars.push_back(d2.actual);
              }
            }
            auto blocks = transformNodes<ProcessBlock>(node->blocks);
            auto ctors = transformNodes<ProcessConstructor>(node->constructors);

            changed = changed || blocks.isNew || ctors.isNew;

            if (changed) {
              return ProcessDecl::create(node->scope, node->name,
                  node->type->self<types::ProcessType>(), ports, vars, ctors.actual, blocks.actual);
            }
            return nullptr;
          }

          ENode visit(const ProcessVariableUpdate::ProcessVariableUpdatePtr &node) override
          {
            auto next = transformStatement(node->next);
            if (isUnused(node->target->name)) {
              return nop(next.actual);
            }
            if (next.isNew) {
              return ProcessVariableUpdate::create(node->target, node->value, next.actual);
            }
            return nullptr;
          }

          inline bool isUnused(const Name &name) const
          {
            return unusedMembers.find(name) != unusedMembers.end();
          }

          const ::std::set<Name> &unusedMembers;
        };

      }

      TransformResult<EProgram> PruneProcessMembers::transform(EProgram src)
      {
        TransformResult<EProgram> res { src, false };
        do {
          FindUnusedMembersPass findPass;
          res.actual->accept(findPass);
          if (findPass.unusedMembers.empty()) {
            break;
          }
          RemoveUnusedMembers transformPass(findPass.unusedMembers);
          auto tmp = transformPass.transformStatement(res.actual);
          if (!tmp.isNew) {
            ::std::cerr << "Internal error: failed to remove unused members" << ::std::endl;
            break;
          }
          res = { tmp.actual->self<Program>(), true };

        } while (true);

        return res;
      }
    }
  }
}
