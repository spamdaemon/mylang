#include <mylang/ethir/transforms/RenameFreeVariables.h>
#include <mylang/ethir/queries/FindFreeVariables.h>
#include <mylang/ethir/Transform.h>
#include <mylang/ethir/ir/Variable.h>
#include <mylang/ethir/prettyPrint.h>

#include <cassert>
#include <functional>
#include <map>

namespace mylang {
  namespace ethir {
    using namespace ir;
    namespace transforms {

      namespace {

        struct Rename: public Transform
        {
          Rename(const ::std::map<NamePtr, EVariable> &names)
              : newNames(names)
          {
          }
          ~Rename()
          {
          }

          ENode visit(const EVariable &node) override final
          {
            auto i = newNames.find(node->name.source());
            if (i == newNames.end()) {
              return nullptr;
            }
            return i->second;
          }

          const ::std::map<NamePtr, EVariable> &newNames;
        };

      }

      RenameFreeVariables::Result RenameFreeVariables::rename(ENode node)
      {

        auto freeVars = queries::FindFreeVariables::find(node);

        // create a new name for each free variable
        ::std::map<NamePtr, EVariable> newNames;
        Result res;
        for (auto &v : freeVars) {
          if (v.second->scope == Variable::Scope::FUNCTION_SCOPE) {
            auto oldVar = v.second;
            auto newVar = v.second->rename(Name::create(v.first.source()->createSibling()));
            newNames.emplace(v.first.source(), newVar);
            res.old2new.emplace(oldVar, newVar);
          }
        }

        Rename pass(newNames);

        res.result = pass.visitNode(node);
        if (res.result == nullptr) {
          res.result = node;
        } else {

        }
        return res;
      }
    }
  }
}

