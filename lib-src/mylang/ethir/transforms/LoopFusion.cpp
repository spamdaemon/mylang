#include <mylang/ethir/Transform.h>
#include <mylang/ethir/transforms/LoopFusion.h>
#include <mylang/ethir/transforms/TransformSkip.h>
#include <mylang/ethir/transforms/TransformYield.h>
#include <mylang/ethir/transforms/CopyRename.h>
#include <mylang/ethir/queries/FindDefUse.h>
#include <mylang/ethir/Transform.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/types/types.h>
#include <iostream>
#include <cassert>

namespace mylang {
  namespace ethir {
    using namespace ir;
    using namespace types;
    namespace transforms {
      using DefUse = queries::FindDefUse::DefUse;
      using Use = queries::FindDefUse::Use;

      namespace {

        struct StepTransform: public Transform
        {
          ~StepTransform()
          {
          }

          ENode visit(const ir::Loop::LoopPtr&) override final
          {
            return nullptr;
          }

        public:
          ENode visit(const IterateArrayStep::IterateArrayStepPtr &expr) override final
          {
            Loop::Expressions tmp = ::std::move(initializers);
            auto res = Transform::visit(expr);
            if (!initializers.empty()) {
              auto iter = expr;
              if (res) {
                iter = res->self<IterateArrayStep>();
              }
              initializers.insert(iter->initializers.begin(), iter->initializers.end());
              res = IterateArrayStep::create(iter->reverse, iter->nextState, iter->array,
                  iter->counter, iter->value, ::std::move(initializers), iter->body);
            }
            initializers = ::std::move(tmp);
            return res;
          }
          Loop::Expressions initializers;
        };

        static bool onlyOnce(const Use &use)
        {
          return use.useCount == 1 && use.isDefined && use.initialValue && use.updates.empty()
              && use.initialValue->self<ToArray>();
        }

        static void filterOnlyOnce(DefUse &defuse)
        {
          for (auto i = defuse.begin(); i != defuse.end();) {
            if (!onlyOnce(i->second)) {
              i = defuse.erase(i);
            } else {
              ++i;
            }
          }
        }

        struct Fuser: public Transform
        {
          Fuser(const DefUse &defuse)
              : defUse(defuse)
          {
          }

          ~Fuser()
          {
          }

          Loop::LoopPtr findFuseLoop(IterateArrayStep::IterateArrayStepPtr step)
          {
            auto use = defUse.find(step->array);
            if (use == defUse.end()) {
              // could not find the array; probably there are more uses for the array
              return nullptr;
            }
            // we know that use is a ToArray function as well
            auto src = use->second.initialValue->self<ToArray>();
            if (src == nullptr) {
              return nullptr;
            }
            auto fused_iterator = fused.find(src);
            if (fused_iterator != fused.end()) {
              return fused_iterator->second->loop;
            } else {
              return src->loop;
            }
          }

          // replace the iterate step with the contents of the loop array
          TransformResult<Loop::LoopPtr> fuse(Loop::LoopPtr currentLoop,
              const Loop::State currentState)
          {
            auto stepIter = currentLoop->states.find(currentState);
            assert(stepIter != currentLoop->states.end());
            auto currentStep = stepIter->second->self<IterateArrayStep>();
            if (currentStep == nullptr) {
              return {currentLoop, false};
            }
            // we cannot fuse a with a reverse loop
            if (currentStep->reverse) {
              return {currentLoop, false};
            }
            auto fuseLoop = findFuseLoop(currentStep);
            if (fuseLoop == nullptr) {
              return {currentLoop, false};
            }

            // first, since we're incorporating the array loop, we need to make a copy of that loop
            fuseLoop = CopyRename::copy(fuseLoop)->self<Loop>();
            fuseLoop = TransformSkip::replaceCurrentState(fuseLoop).actual->self<Loop>();

            // since we're replacing the currentState with the fuseLoop,
            // the first state of the fuseLoop becomes equivalent to the current state.
            // so, any state that transitions into the current state, now needs to transition into the start
            // start of the fuseLoop
            // with in the current loop, replace all skips into the current state with skips in the first state of the fuseloop
            auto currentStart =
                currentLoop->start == currentState ? fuseLoop->start : currentLoop->start;
            auto currentStates = TransformSkip::replaceNextStates(currentLoop->states,
                [&](Loop::State s) {
                  return s==currentState ? fuseLoop->start : s;
                }).actual;
            // remember the current step, because we're going to remove it as it will be replace by the fuseLoop
            currentStep = currentStates.find(currentState)->second->self<IterateArrayStep>();

            // termination of the fuseLoop is the same as going to the next state in the currentStep
            auto fuseStates = TransformSkip::replaceNextStates(fuseLoop->states,
                [&](Loop::State s) {
                  // FIXME: we may need to fuse the next states
                    return s==Loop::STOP_STATE ? currentStep->nextState->nextState : s;
                  }).actual;

            fuseStates =
                TransformSkip::replaceSkips(fuseStates,
                    [&](
                        SkipStatement::SkipStatementPtr s) {
                          if (s->nextState == Loop::STOP_STATE) {
                            Loop::Expressions updates(currentStep->nextState->updates);
                            updates.insert(s->updates.begin(),s->updates.end());
                            return SkipStatement::create(::std::move(updates),currentStep->nextState->nextState);
                          }
                          return s;
                        }).actual;

            struct YieldTx: public StepTransform
            {
              YieldTx(const IterateArrayStep::IterateArrayStepPtr &s)
                  : currentStep(s)
              {
              }
              ~YieldTx()
              {
              }

              ENode visit(const YieldStatement::YieldStatementPtr &yield) override final
              {
                auto step = CopyRename::copy(currentStep)->self<IterateArrayStep>();
                initializers.insert(step->initializers.begin(), step->initializers.end());

                auto body =
                    TransformSkip::replaceSkip(step->body,
                        [&](
                            SkipStatement::SkipStatementPtr skip) {
                              Loop::Expressions fusedUpdates(yield->skip->updates);
                              fusedUpdates.insert(skip->updates.begin(),skip->updates.end());
                              return SkipStatement::create(fusedUpdates,skip->nextState,skip->resetNextState);
                            }).actual->self<Statement>();
                body = CopyRename::copy(body)->self<Statement>();

                StatementBuilder sb;
                sb.declareVariable(step->value, yield->value);
                return sb.build(body);
              }
            private:
              const IterateArrayStep::IterateArrayStepPtr currentStep;

            } tx(currentStep);

            fuseStates = tx.transformLoopStates(fuseStates).actual;

            // at this point, we can remove the current state and adopt the fuse states
            currentStates.erase(currentState);
            currentStates.insert(fuseStates.begin(), fuseStates.end());

            return {Loop::create(currentStart, ::std::move(currentStates)), true};
          }

          ENode visit(const ToArray::ToArrayPtr &expr) override final
          {
            // things will be easier if we don't have to deal with CURRENT_STATE in the code
            auto fusedLoop = TransformSkip::replaceCurrentState(expr->loop).actual->self<Loop>();
            for (auto s : expr->loop->states) {
              fusedLoop = fuse(fusedLoop, s.first).actual;
            }
            if (fusedLoop == expr->loop) {
              return nullptr;
            }
            // we should make a full copy of the loop with
            // fresh names and everything
            fusedLoop = CopyRename::copy(fusedLoop)->self<Loop>();

            // after fusion the input loop should be pruned away, because the
            // current loop was the only reference to it
            auto result = ToArray::create(expr->type->self<ArrayType>(), fusedLoop);
            fused.insert( { expr, result });
            return result;
          }

          ::std::map<ToArray::ToArrayPtr, ToArray::ToArrayPtr> fused;
          const DefUse &defUse;
        };

      }

      TransformResult<ENode> LoopFusion::fuseLoops(ENode root)
      {
        auto defuse = queries::FindDefUse::find(root);
        filterOnlyOnce(defuse);

        Fuser v(defuse);
        // we only do one pass
        return v.transformNode(root);
      }

    }
  }
}
