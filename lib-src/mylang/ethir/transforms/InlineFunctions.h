#ifndef CLASS_MYLANG_ETHIR_TRANSFORMS_INLINEFUNCTIONS_H
#ifndef CLASS_MYLANG_ETHIR_TRANSFORMS_INLINEFUNCTIONS_H

#define FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#include <functional>

namespace mylang {
  namespace ethir {
    namespace transforms {

      /**
       * A transformations that inlines functions according to some criterion.
       */
      class InlineFunctions
      {
        /** The predicate that determines if a function should be inlined */
      public:
        typedef ::std::function<bool(const EVariable&, const ELambda&)> Predicate;

        /**
         * Create an inline pass that uses the specified predicate to determine if a function should
         * be inlined.
         * @param predicate a predicate
         */
      public:
        InlineFunctions(const Predicate &predicate);

        /** Destructor */
      public:
        virtual ~InlineFunctions();

        /**
         * Inline functions.
         */
      public:
        TransformResult<EProgram> apply(EProgram src) const;

        /** A function that inlines a simple lambda expression */
      public:
        static bool isSimpleFunction(const EVariable &name, const ELambda &ptr, size_t n);

        /** True if the logging statements can be eliminated */
      public:
        const Predicate predicate;
      };
    }

  }
}
#endif
