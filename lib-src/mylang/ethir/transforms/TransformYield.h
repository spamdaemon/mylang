#ifndef CLASS_MYLANG_ETHIR_TRANSFORMS_TRANSFORMYIELD_H
#define CLASS_MYLANG_ETHIR_TRANSFORMS_TRANSFORMYIELD_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TRANSFORMRESULT_H
#include<mylang/ethir/TransformResult.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_LOOP_H
#include<mylang/ethir/ir/Loop.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_YIELDSTATEMENT_H
#include<mylang/ethir/ir/YieldStatement.h>
#endif

#include <functional>

namespace mylang {
  namespace ethir {
    namespace transforms {

      /**
       * Fuse loops into single loops to avoid generation of temporary
       * objects.
       */
      class TransformYield
      {
      public:
        typedef ::std::function<EStatement(ir::YieldStatement::YieldStatementPtr)> ReplaceYieldFN;

        /**
         * Replace a yield statement with a new statement.
         * This function will not traverse into Loops.
         * @param root whose yield statements should be replaced
         * @param replacement a replacement function
         */
      public:
        static TransformResult<ENode> replaceYield(ENode root, ReplaceYieldFN replacement);

        /**
         * Replace all yields in the specified functions.
         * This function will not traverse into Loops.
         * @param root whose yield statements should be replaced
         * @param replacement a replacement function
         */
      public:
        static TransformResult<ir::Loop::States> replaceYields(const ir::Loop::States &states,
            ReplaceYieldFN replacement);
      };
    }
  }
}
#endif
