#ifndef CLASS_MYLANG_ETHIR_TRANSFORMS_PRUNEPROCESSMEMBERS_H
#define CLASS_MYLANG_ETHIR_TRANSFORMS_PRUNEPROCESSMEMBERS_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef FILE_MYLANG_ETHIR_TRANSFORMRESULT_H
#include <mylang/ethir/TransformResult.h>
#endif

#ifndef FILE_MYLANG_ETHIR_IR_PROCESSDECL_H
#include <mylang/ethir/ir/ProcessDecl.h>
#endif

namespace mylang {
  namespace ethir {
    namespace transforms {

      /**
       * Remove unused process values or variables.
       *
       * Limitations:
       * <ul>
       * <li>If a function is recursive, then it won't be removed.
       * </ul>
       */
      class PruneProcessMembers
      {
        /** Destructor */
      private:
        ~PruneProcessMembers() = delete;

        /**
         * Remove any unused globals from the program.
         * @return a program
         */
      public:
        static TransformResult<ir::ProcessDecl::ProcessDeclPtr> transformProcess(
            ir::ProcessDecl::ProcessDeclPtr src);

        /**
         * Remove any unused globals from the program.
         * @return a program
         */
      public:
        static TransformResult<EProgram> transform(EProgram src);
      };
    }
  }
}
#endif
