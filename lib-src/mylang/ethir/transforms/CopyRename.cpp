#include <mylang/ethir/transforms/CopyRename.h>
#include <mylang/ethir/Transform.h>
#include <mylang/ethir/ir/DefaultNodeVisitor.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/prettyPrint.h>
#include <cassert>
#include <functional>

namespace mylang {
  namespace ethir {
    using namespace ir;
    namespace transforms {

      namespace {

        /** Introduce the mappings for all names introduced under a node */
        struct Pass1: public DefaultNodeVisitor
        {
          Pass1()
          {
          }
          Pass1(::std::map<mylang::names::Name::Ptr, mylang::names::Name::Ptr> xNewNames)
              : newNames(::std::move(xNewNames))
          {
          }
          ~Pass1()
          {
          }

          Loop::State makeNewState(const Loop::State &state)
          {
            if (state == Loop::STOP_STATE || state == Loop::CURRENT_STATE) {
              return state;
            }

            auto i = newStates.find(state);
            if (i == newStates.end()) {
              auto newState = Loop::newState();
              newStates.insert( { state, newState });
              return newState;
            }
            return i->second;
          }

          NamePtr makeNewName(const NamePtr &name)
          {
            if (!name) {
              return name;
            }
            auto i = newNames.find(name);
            if (i == newNames.end()) {
              auto newName = name->createSibling();
              newNames[name] = newName;
              return newName;
            }
            return i->second;
          }

          void makeNewName(const Name &name)
          {
            makeNewName(name.source());
          }

          void visitGlobalValue(GlobalValue::GlobalValuePtr node)
          override final
          {
            makeNewName(node->name);
            DefaultNodeVisitor::visitGlobalValue(node);
          }
          void visitProcessValue(ProcessValue::ProcessValuePtr node)
          override final
          {
            makeNewName(node->name);
            return DefaultNodeVisitor::visitProcessValue(node);
          }
          void visitProcessVariable(ProcessVariable::ProcessVariablePtr node)
          override final
          {
            makeNewName(node->name);
            return DefaultNodeVisitor::visitProcessVariable(node);
          }
          void visitValueDecl(ValueDecl::ValueDeclPtr node)
          override final
          {
            makeNewName(node->name);
            return DefaultNodeVisitor::visitValueDecl(node);
          }
          void visitVariableDecl(VariableDecl::VariableDeclPtr node)
          override final
          {
            makeNewName(node->name);
            return DefaultNodeVisitor::visitVariableDecl(node);
          }
          void visitProcessDecl(ProcessDecl::ProcessDeclPtr node)
          override final
          {
            makeNewName(node->name);
            return DefaultNodeVisitor::visitProcessDecl(node);
          }
          void visitLetExpression(LetExpression::LetExpressionPtr node)
          override final
          {
            makeNewName(node->variable->name);
            return DefaultNodeVisitor::visitLetExpression(node);
          }
          void visitParameter(Parameter::ParameterPtr node)
          override final
          {
            makeNewName(node->variable->name);
            return DefaultNodeVisitor::visitParameter(node);
          }

          void visitLoopStatement(LoopStatement::LoopStatementPtr node)
          override final
          {
            makeNewName(node->name);
            return DefaultNodeVisitor::visitLoopStatement(node);
          }

          void visitForeachStatement(ForeachStatement::ForeachStatementPtr node)
          override final
          {
            makeNewName(node->name);
            makeNewName(node->variable->name);
            return DefaultNodeVisitor::visitForeachStatement(node);
          }

          void visitLoop(Loop::LoopPtr node) override final
          {
            // create a new name for each state
            makeNewState(node->start);
            for (auto &s : node->states) {
              makeNewState(s.first);
            }
            return DefaultNodeVisitor::visitLoop(node);
          }

          ::std::map<mylang::names::Name::Ptr, mylang::names::Name::Ptr> newNames;
          ::std::map<Loop::State, Loop::State> newStates;
        };

        struct Pass2: public Transform
        {
          Pass2(const ::std::map<mylang::names::Name::Ptr, mylang::names::Name::Ptr> &names,
              const ::std::map<Loop::State, Loop::State> &states)
              : newNames(names), newStates(states)
          {
          }
          ~Pass2()
          {
          }

          template<class T>
          ENode mapNode(const NamePtr &n, const ::std::shared_ptr<const T> &node,
              ::std::function<ENode(const NamePtr&)> fn)
          {
            auto i = newNames.find(n);
            if (i == newNames.end()) {
              return Transform::visit(node);
            }
            auto newName = i->second;
            return fn(newName);
          }

          template<class T>
          ENode mapNode(const Name &n, const ::std::shared_ptr<const T> &node,
              ::std::function<ENode(const Name&)> fn)
          {
            return mapNode(n.source(), node, [&](const NamePtr &nn) {
              auto newName = n.rename(nn);
              return fn(newName);
            });
          }

          template<class T>
          ENode mapNode(const Loop::State &n, const ::std::shared_ptr<const T> &node,
              ::std::function<ENode(const Loop::State&)> fn)
          {
            auto i = newStates.find(n);
            if (i == newStates.end()) {
              return Transform::visit(node);
            }
            auto newState = i->second;
            return fn(newState);
          }

          ENode visit(const Variable::VariablePtr &node) override final
          {
            return mapNode(node->name, node, [&](const Name &name) {
              return Variable::create(node->scope, node->type, name);
            });
          }

          ENode visit(const ProcessDecl::ProcessDeclPtr &node)
          override final
          {
            return mapNode(node->name, node,
                [&](
                    const Name &name) {
                      ir::ProcessDecl::Ports publicPorts;
                      ir::ProcessDecl::Declarations variables;
                      ir::ProcessDecl::Constructors constructors;
                      ir::ProcessDecl::Blocks blocks;

                      for (auto p : node->publicPorts) {
                        publicPorts.push_back(transformStatement(p).actual->self<ir::PortDecl>());
                      }
                      for (auto p : node->variables) {
                        variables.push_back(transformStatement(p).actual->self<ir::Declaration>());
                      }
                      for (auto p : node->constructors) {
                        constructors.push_back(transformStatement(p).actual->self<ir::ProcessConstructor>());
                      }
                      for (auto p : node->blocks) {
                        blocks.push_back(transformStatement(p).actual->self<ir::ProcessBlock>());
                      }
                      auto pTy = node->type->self<types::ProcessType>();
                      return ProcessDecl::create(node->scope, name, pTy, publicPorts, variables, constructors,
                          blocks);
                    });
          }

          ENode visit(const LoopStatement::LoopStatementPtr &node)
          override final
          {
            return mapNode(node->name, node, [&](const Name &name) {
              auto body = transformStatement(node->body);
              auto next = transformStatement(node->next);
              return LoopStatement::create(name, body.actual, next.actual);
            });
          }

          ENode visit(const ForeachStatement::ForeachStatementPtr &node)
          override final
          {
            return mapNode(node->name, node, [&](const Name &name) {
              auto loopVar = transformExpression(node->variable);
              auto data = transformExpression(node->data);
              auto body = transformStatement(node->body);
              auto next = transformStatement(node->next);

              return ForeachStatement::create(name, loopVar.actual, data.actual, body.actual,
                  next.actual);
            });
          }

          ENode visit(const BreakStatement::BreakStatementPtr &node)
          override final
          {
            return mapNode(node->scope, node, [&](const Name &name) {
              return BreakStatement::create(name);
            });
          }
          ENode visit(const ContinueStatement::ContinueStatementPtr &node)
          override final
          {
            return mapNode(node->scope, node, [&](const Name &name) {
              return ContinueStatement::create(name);
            });
          }
          ENode visit(const SkipStatement::SkipStatementPtr &node) override final
          {
            return mapNode(node->nextState, node, [&](const Loop::State &nextState) {
              auto updates = transformExpressions(node->updates).actual;
              return SkipStatement::create(::std::move(updates), nextState,node->resetNextState);
            });
          }

          ENode visit(const Loop::LoopPtr &node) override final
          {
            return mapNode(node->start, node, [&](const Loop::State &start) {
              Loop::States states;
              for (auto &s : node->states) {
                // all the other names need to be remapped as well
                auto name = newStates.find(s.first)->second;
                auto step = transformNode(s.second).actual->self<Loop::Step>();
                states.insert( {name,step});
              }
              return Loop::create(start, states);
            });
          }

          const ::std::map<mylang::names::Name::Ptr, mylang::names::Name::Ptr> &newNames;
          const ::std::map<Loop::State, Loop::State> &newStates;

        };

      }

      ENode CopyRename::copy(ENode node)
      {
        return copy(node, { });
      }

      ENode CopyRename::copy(ENode node,
          ::std::map<mylang::names::Name::Ptr, mylang::names::Name::Ptr> newNames)
      {
        // we do this in 2 passes, because SSA form destroys scope information
        // the first pass finds all those names that should be renamed, regardless of scope
        // and the second pass replaces all occurrences of the mapped names
        Pass1 pass1(::std::move(newNames));
        pass1.visitNode(node);
        Pass2 pass2(pass1.newNames, pass1.newStates);
        auto res = pass2.visitNode(node);
        if (res) {
//          ::std::cerr << "CopyRename : " << typeid(*node).name() << " --> " << typeid(*res).name()
//              << ::std::endl;

//          ::std::cerr << ">>>>>" << ::std::endl << prettyPrint(node) << ::std::endl;
//          ::std::cerr << "=====" << ::std::endl << prettyPrint(res) << ::std::endl;
//          ::std::cerr << "<<<<<" << ::std::endl;

          return res;
        } else {
//          ::std::cerr << "CopyRename : " << typeid(*node).name() << " --> no change" << ::std::endl;
          return node;
        }
      }
    }
  }
}

