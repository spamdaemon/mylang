#include <mylang/ethir/transforms/SingleReturn.h>
#include <mylang/ethir/Transform.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/types/FunctionType.h>
#include <mylang/ethir/types/VoidType.h>
#include <mylang/ethir/prettyPrint.h>

#include <cassert>
#include <functional>
#include <map>

namespace mylang {
  namespace ethir {
    using namespace ir;
    using namespace types;
    namespace transforms {

      namespace {

        // check if the lambda is already normalized, i.e has a single return at the end
        static bool endsWithReturn(ELambda lambda)
        {
          EStatement s = lambda->body->statements;
          while (s->next) {
            s = s->next;
          }
          if (s->self<ReturnStatement>()) {
            return true;
          }
          return false;
        }

        struct Pass: public Transform
        {
          Pass(const BreakStatement::BreakStatementPtr &xbreakStatement, const EVariable &xoutput)
              : breakStatement(xbreakStatement), output(xoutput), nReturns(0)
          {
          }
          ~Pass()
          {
          }

          ENode visit(const LambdaExpression::LambdaExpressionPtr &node)
          override final
          {
            // do not traverse into a lambda function
            return nullptr;
          }

          ENode visit(const ReturnStatement::ReturnStatementPtr &node)
          override final
          {
            EStatement res = breakStatement;
            if (node->value) {
              res = VariableUpdate::create(output, node->value, res);
            }
            ++nReturns;
            return res;
          }

          /** Each return is replace with this loop statement */
        private:
          const BreakStatement::BreakStatementPtr breakStatement;

        private:
          const EVariable output;

        public:
          size_t nReturns;
        };

      }

      ELambda SingleReturn::normalize(ELambda node)
      {
        if (node->body->isNOP()) {
          return nullptr;
        }

        auto terminatesWithReturn = endsWithReturn(node);

        auto loopName = Name::create("singleReturn");
        auto breakStmt = BreakStatement::create(loopName);
        EVariable output;
        auto retTy = node->type->self<FunctionType>()->returnType;
        if (!retTy->isVoid()) {
          output = Variable::create(Variable::Scope::FUNCTION_SCOPE, retTy,
              Name::create("singleReturnResult"));
        }

        Pass pass(breakStmt, output);
        ENode body = pass.visitNode(node->body);
        if (node == nullptr) {
          return nullptr;
        } else if (pass.nReturns == 1 && terminatesWithReturn) {
          // if there is a single return that was modified and we know that body ends with a return
          // statement, then we don't need to change anything
          return nullptr;
        } else {
          // transform the body of the lambda into loop like this:
          // var output;
          // loop L:
          //   old body with return statements replaced by break L
          //   break L;
          // return output;
          // since we only want to execute the loop body

          EStatement lambdaBody = ReturnStatement::create(output);
          EStatement loopBody = body->self<Statement>();
          loopBody = StatementBlock::create(loopBody, breakStmt);
          lambdaBody = LoopStatement::create(loopName, loopBody, lambdaBody);
          if (output) {
            lambdaBody = VariableDecl::create(output, lambdaBody);
          }
          return LambdaExpression::create(node->tag, node->parameters, retTy, lambdaBody);
        }
      }
    }
  }
}

