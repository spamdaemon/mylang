#ifndef CLASS_MYLANG_ETHIR_TRANSFORMS_LOOPFUSION_H
#define CLASS_MYLANG_ETHIR_TRANSFORMS_LOOPFUSION_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TRANSFORMRESULT_H
#include <mylang/ethir/TransformResult.h>
#endif

namespace mylang {
  namespace ethir {
    namespace transforms {

      /**
       * Fuse loops into single loops to avoid generation of temporary
       * objects.
       */
      class LoopFusion
      {
        /**
         * Fuse loops.
         * @param a node
         * @return a new new or nullptr
         */
      public:
        static TransformResult<ENode> fuseLoops(ENode root);
      };
    }
  }
}
#endif
