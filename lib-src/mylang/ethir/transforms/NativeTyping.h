#ifndef CLASS_MYLANG_ETHIR_TRANSFORMS_NATIVETYPING_H
#define CLASS_MYLANG_ETHIR_TRANSFORMS_NATIVETYPING_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#include <functional>

namespace mylang {
  namespace ethir {
    namespace transforms {

      /**
       * This transforms replaces integers with integers aligned with the native
       * integer types. Any integer that cannot be converted to a native type
       * is turned into an unbounded integer. 
       */
      class NativeTyping
      {
        /**
         * The conversion function from a builtin type to a native type.
         */
      public:
        typedef ::std::function<EType(const EType&)> Converter;

      public:
        NativeTyping(Converter converter);

      public:
        virtual ~NativeTyping();

      public:
        virtual EProgram transform(mylang::ethir::EProgram node) const;

        /** The converter */
      private:
        const Converter converter;
      };
    }
  }
}
#endif
