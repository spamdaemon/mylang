#include <mylang/ethir/transforms/ChangeSignature.h>
#include <mylang/ethir/transforms/CopyRename.h>
#include <mylang/ethir/queries/FindFunctionTags.h>
#include <mylang/ethir/Transform.h>
#include <mylang/ethir/ir/nodes.h>
#include <mylang/ethir/ir/StatementBuilder.h>
#include <mylang/ethir/types/types.h>
#include <mylang/ethir/prettyPrint.h>
#include <cassert>

using namespace mylang::ethir::ir;
using namespace mylang::ethir::types;

namespace mylang {
  namespace ethir {
    namespace transforms {

      namespace {

        static Name newName(const char *prefix)
        {
          if (prefix) {
            return Name::create(prefix);
          } else {
            return Name::create("tmp");
          }
        }

        struct ChangeReturnType: public Transform
        {
          ChangeReturnType(Type::Ptr retTy)
              : type(retTy)
          {
          }
          ~ChangeReturnType()
          {
          }

          Type::Ptr type;

          ENode visit(const ReturnStatement::ReturnStatementPtr &node) override
          {
            if (nullptr == node->value) {
              return nullptr;
            }
            if (node->value->type->isSameType(*type)) {
              return nullptr;
            }
            StatementBuilder b;
            auto name = b.declareLocalValue(
                ir::OperatorExpression::create(type, OperatorExpression::OP_CHECKED_CAST,
                    node->value));
            b.appendReturn(name);
            return b.build();
          }

          ENode visit(const LambdaExpression::LambdaExpressionPtr&) override
          {
            return nullptr;
          }

        };

        struct ChangeCallSites: public Transform
        {
          ChangeCallSites(const ChangeSignature::Changes &xupdates,
              const queries::FindFunctionTags::FunctionsByTag &functions)
              : applyChanges(xupdates), functionsByTag(functions)
          {
          }

          ~ChangeCallSites()
          {
          }

          const std::vector<ChangeSignature::Change>* findChanges(const Name &name) const
          {
            auto i = applyChanges.find(name);
            if (i == applyChanges.end()) {
              return nullptr;
            }
            return &i->second;
          }

          ENode visit(const ir::OperatorExpression::OperatorExpressionPtr &e)
          {
            if (e->name != ir::OperatorExpression::OP_CALL) {
              return nullptr;
            }
            // the function we call is a variable, but it might point to one we
            // know
            auto fn = e->arguments.at(0);
            auto changes = findChanges(fn->name);
            if (!changes) {
              return nullptr;
            }
            // check if this is a call site we want to change
            const ChangeSignature::Change *change = nullptr;
            for (auto &c : *changes) {
              if (c.callsites.count(e) != 0) {
                change = &c;
                break;
              }
            }
            if (!change) {
              return nullptr;
            }

            FunctionType::FunctionTypePtr fnTy = change->signature;

            auto tagged = functionsByTag.find(change->tag);
            assert(tagged != functionsByTag.end() && "functions by tag cannot be empty here");
            EVariable fnName = tagged->second.findBySignature(fnTy);

            if (fnName == nullptr) {
              // there is not existing function we can call, so see if there is another function we've marked new already
              for (auto entry : functionsToDuplicate[fn->name]) {
                if (fnTy->isSameType(*entry->type)) {
                  // we've already have a function that we'll need to duplicate
                  fnName = entry;
                }
              }
              if (fnName == nullptr) {
                // generate a new function name for the tag using the tag itself
                fnName = Variable::create(fn->scope, fnTy,
                    Name::create(fn->name.source()->createSibling()));
                // we need a new function
                functionsToDuplicate[fn->name].push_back(fnName);
              }
            }

            ::std::vector<EVariable> args;
            args.push_back(fnName);
            for (size_t i = 1; i < e->arguments.size(); ++i) {
              args.push_back(
                  Variable::create(Variable::Scope::FUNCTION_SCOPE, fnTy->parameters.at(i - 1),
                      newName("p")));
            }
            EExpression res = OperatorExpression::create(OperatorExpression::OP_CALL, args);
            for (size_t i = 1; i < args.size(); ++i) {
              auto cast = OperatorExpression::create(fnTy->parameters.at(i - 1),
                  OperatorExpression::OP_CHECKED_CAST, e->arguments.at(i));
              res = LetExpression::create(args.at(i), cast, res);
            }
            EVariable tmp = Variable::create(Variable::Scope::FUNCTION_SCOPE, fnTy->returnType,
                newName("tmp"));
            res = LetExpression::create(tmp, res,
                OperatorExpression::create(e->type, OperatorExpression::OP_CHECKED_CAST, tmp));

            ::std::cerr << "Callsite changed from " << fn->name << " to " << fnName->name << "("
                << e << ")" << ::std::endl;
            return res;
          }

          /** The changes we want to apply */
        public:
          const ChangeSignature::Changes &applyChanges;
          const queries::FindFunctionTags::FunctionsByTag &functionsByTag;
          ::std::map<Name, ::std::vector<EVariable>> functionsToDuplicate;
        };

        struct DuplicateFunctions: public Transform
        {
          DuplicateFunctions(const ChangeSignature::Changes &xupdates,
              const ::std::map<Name, ::std::vector<EVariable>> &xfunctionsToDuplicate)
              : applyChanges(xupdates), functionsToDuplicate(xfunctionsToDuplicate)
          {
          }

          ~DuplicateFunctions()
          {
          }

          ENode visit(const ir::Program::ProgramPtr &node)
          {
            auto res = Transform::visit(node);

            if (!newGlobals.empty()) {
              if (!res) {
                res = node;
              }
              auto prg = res->self<Program>();

              // create a new result and add the new functions in
              newGlobals.insert(newGlobals.end(), prg->globals.begin(), prg->globals.end());
              res = Program::create(::std::move(newGlobals), prg->processes, prg->types);
            }

            return res;
          }

          ENode visit(const ir::ProcessDecl::ProcessDeclPtr &node)
          {
            auto res = Transform::visit(node);

            if (!newProcessValues.empty()) {
              if (!res) {
                res = node;
              }
              auto proc = res->self<ProcessDecl>();

              // create a new result and add the new functions in
              newProcessValues.insert(newProcessValues.end(), proc->variables.begin(),
                  proc->variables.end());
              res = ProcessDecl::create(proc->scope, proc->name, proc->type->self<ProcessType>(),
                  proc->publicPorts, newProcessValues, proc->constructors, proc->blocks);
            }

            return res;
          }

          const std::vector<EVariable>* findFunctionToDuplicate(const Name &name) const
          {
            auto i = functionsToDuplicate.find(name);
            if (i == functionsToDuplicate.end()) {
              return nullptr;
            }
            return &i->second;
          }

          /**
           * Create a duplicate of the specified oldFN and give it a new signature.
           */
          LambdaExpression::LambdaExpressionPtr resignFunction(
              LambdaExpression::LambdaExpressionPtr oldFN,
              const FunctionType::FunctionTypePtr &newSignature) const
          {
            LambdaExpression::Parameters params;

            StatementBuilder b;
            for (size_t i = 0; i < oldFN->parameters.size(); ++i) {
              auto oldParam = oldFN->parameters.at(i);
              auto newParam = Parameter::create(newName("arg"), newSignature->parameters.at(i));
              params.push_back(newParam);
              b.declareValue(oldParam->variable,
                  OperatorExpression::create(oldParam->variable->type,
                      OperatorExpression::OP_CHECKED_CAST, newParam->variable));
            }

            Statement::StatementPtr body = oldFN->body;
            if (!newSignature->returnType->isSameType(
                *oldFN->type->self<FunctionType>()->returnType)) {
              ChangeReturnType crt(newSignature->returnType);
              body = crt.apply(body)->self<Statement>();
            }

            auto lambda = LambdaExpression::create(oldFN->tag, params, newSignature->returnType,
                b.build(body));

            // we could return the lambda as is, but we'll make a copy so that all
            // the names defined within the lambda's body are new
            auto copy = CopyRename::copy(lambda);

            return copy->self<LambdaExpression>();
          }

          ENode visit(const ir::GlobalValue::GlobalValuePtr &e)
          {
            auto node = Transform::visit(e);
            auto dups = findFunctionToDuplicate(e->name);
            if (dups) {
              auto decl = (node ? node : e)->self<GlobalValue>();
              for (auto fnVar : *dups) {
                // need to use the new lambda function
                EExpression changedFN = resignFunction(decl->value->self<LambdaExpression>(),
                    fnVar->type->self<types::FunctionType>());
                // we cannot change exported values, so we create a new one
                auto global = GlobalValue::create(fnVar, changedFN, GlobalValue::DEFAULT);
                newGlobals.push_back(global);
              }
            }
            return node;
          }

          ENode visit(const ir::ProcessValue::ProcessValuePtr &e)
          {
            auto node = Transform::visit(e);
            auto dups = findFunctionToDuplicate(e->name);
            if (dups) {
              auto decl = (node ? node : e)->self<ProcessValue>();

              for (auto fnVar : *dups) {
                // need to use the new lambda function
                EExpression changedFN = resignFunction(decl->value->self<LambdaExpression>(),
                    fnVar->type->self<types::FunctionType>());
                auto proc = ProcessValue::create(fnVar, changedFN);
                newProcessValues.push_back(proc);
              }
            }
            return node;
          }

          ENode visit(const ir::ValueDecl::ValueDeclPtr &e)
          {
            auto node = Transform::visit(e);
            auto dups = findFunctionToDuplicate(e->name);
            if (dups) {
              auto decl = (node ? node : e)->self<ValueDecl>();
              Statement::StatementPtr res = decl;

              for (auto fnVar : *dups) {
                // need to use the new lambda function
                EExpression changedFN = resignFunction(decl->value->self<LambdaExpression>(),
                    fnVar->type->self<types::FunctionType>());
                res = ValueDecl::createSimplified(fnVar, changedFN, res);
              }
              node = res;
            }
            return node;
          }

          /** The changes we want to apply */
        public:
          const ChangeSignature::Changes &applyChanges;

          /** The functions to duplicate */
          const ::std::map<Name, ::std::vector<EVariable>> &functionsToDuplicate;

          /** The currently known globals program */
        private:
          ::std::vector<ir::GlobalValue::GlobalValuePtr> newGlobals;

          /** The currently known globals program */
        private:
          ::std::vector<ir::Declaration::DeclarationPtr> newProcessValues;
        };
      }

      ChangeSignature::Change::Change(const Tag &xtag,
          mylang::ethir::types::FunctionType::FunctionTypePtr xsignature)
          : tag(xtag), signature(xsignature)
      {

      }

      EProgram ChangeSignature::transform(EProgram prog, const Changes &updates)
      {
        ::std::cerr << "<<<<<<<<<<<<<<<<<<< BEGIN >>>>>>>>>>>>>>>>>" << ::std::endl;
        auto functionsByTag = queries::FindFunctionTags::find(prog);

        ChangeCallSites pass1(updates, functionsByTag);
        auto res = pass1.transformStatement(prog);

        DuplicateFunctions pass2(updates, pass1.functionsToDuplicate);

        res = pass2.transformStatement(res.actual);
        ::std::cerr << "<<<<<<<<<<<<<<<<<<< END >>>>>>>>>>>>>>>>>" << ::std::endl;
        return res.actual->self<Program>();
      }

    }
  }
}

