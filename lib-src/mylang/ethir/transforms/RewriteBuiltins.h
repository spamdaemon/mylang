#ifndef CLASS_MYLANG_ETHIR_TRANSFORMS_REWRITEBUILTINS_H
#define CLASS_MYLANG_ETHIR_TRANSFORMS_REWRITEBUILTINS_H

#ifndef CLASS_MYLANG_ETHIR_TRANSFORM_H
#include <mylang/ethir/Transform.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_LAMBDAEXPRESSION_H
#include <mylang/ethir/ir/LambdaExpression.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_OPERATOREXPRESSION_H
#include <mylang/ethir/ir/OperatorExpression.h>
#endif

#include <set>
#include <optional>

namespace mylang {
  namespace ethir {
    namespace transforms {

#define USE_LOOPS 1

      /**
       * Transform a node by replacing some builtins with combinations
       * of more primitive builtins.
       */
      class RewriteBuiltins: public Transform
      {
        RewriteBuiltins& operator=(const RewriteBuiltins&) = delete;
        RewriteBuiltins(const RewriteBuiltins&) = delete;

      public:
        typedef ir::Expression::ExpressionPtr ExpressionPtr;
        typedef ir::LambdaExpression::Parameters LambdaParameters;
        typedef ::std::vector<ir::Variable::VariablePtr> Arguments;

        /** The operators we want to replace */
      public:
        typedef ::std::set<ir::OperatorExpression::Operator> Operators;

        /**
         * Create a transform to replace only the specified operators.
         */
      public:
        RewriteBuiltins(Operators ops);

        /**
         * Create a transform to replace ALL operators
         */
      public:
        RewriteBuiltins();

      public:
        ~RewriteBuiltins();

        /**
         * Replace the specified operator expression with a simpler one if possible.
         * @param e an expression
         * @return a new expression or null if nothing needed to be replaced
         */
      public:
        ENode visit(const ir::OperatorExpression::OperatorExpressionPtr &e) override final;

        /**
         * We need the program node so we can find out about all the currently known functions.
         */
      public:
        ENode visit(const ir::Program::ProgramPtr &node) override final;

      protected:
        virtual ExpressionPtr replace(const ir::OperatorExpression::OperatorExpressionPtr &e);

#if USE_LOOPS==1
        virtual ExpressionPtr replace_MAP_ARRAY(EType retTy, const Arguments &e);
#else
        virtual ExpressionPtr replace_MAP_ARRAY(EType retTy, const LambdaParameters &e);
#endif

#if USE_LOOPS==1
        virtual ExpressionPtr replace_FILTER_ARRAY(EType retTy, const Arguments &e);
#else
        virtual ExpressionPtr replace_FILTER_ARRAY(EType retTy, const LambdaParameters &e);
#endif
#if USE_LOOPS==1
        virtual ExpressionPtr replace_REVERSE_ARRAY(EType retTy, const Arguments &e);
#else
        virtual ExpressionPtr replace_REVERSE_ARRAY(EType retTy, const LambdaParameters &e);
#endif
#if USE_LOOPS==1
        virtual ExpressionPtr replace_FLATTEN_ARRAY(EType retTy, const Arguments &e);
#else
        virtual ExpressionPtr replace_FLATTEN_ARRAY(EType retTy, const LambdaParameters &e);
#endif
#if USE_LOOPS==1
        virtual ExpressionPtr replace_PARTITION_ARRAY(EType retTy, const Arguments &e);
#else
        virtual ExpressionPtr replace_PARTITION_ARRAY(EType retTy, const LambdaParameters &e);
#endif

#if USE_LOOPS==1
        virtual ExpressionPtr replace_CHECKEDCAST_ARRAY(EType retTy, const Arguments &e);
#else
        virtual ExpressionPtr replace_CHECKEDCAST_ARRAY(EType retTy, const LambdaParameters &e);
#endif

        virtual ExpressionPtr replace_CONCATENATE_ARRAY(EType retTy, const Arguments &e);
        virtual ExpressionPtr replace_PAD_ARRAY(EType retTy, const Arguments &e);
        virtual ExpressionPtr replace_TRIM_ARRAY(EType retTy, const Arguments &e);
        virtual ExpressionPtr replace_DROP_ARRAY(EType retTy, const Arguments &e);
        virtual ExpressionPtr replace_TAKE_ARRAY(EType retTy, const Arguments &e);

#if USE_LOOPS==1
        virtual ExpressionPtr replace_ZIP_ARRAY(EType retTy, const Arguments &e);
#else
        virtual ExpressionPtr replace_ZIP_ARRAY(EType retTy, const LambdaParameters &e);
#endif
        virtual ExpressionPtr replace_FIND_ARRAY(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_FOLD_ARRAY(EType retTy, const LambdaParameters &e);

        virtual ExpressionPtr replace_MERGE_TUPLES(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_FLATTEN_OPTIONAL(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_FILTER_OPTIONAL(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_MAP_OPTIONAL(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_HEAD_TUPLE(EType retTy, const Arguments &args);
        virtual ExpressionPtr replace_TAIL_TUPLE(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_HEAD_ARRAY(EType retTy, const Arguments &args);
        virtual ExpressionPtr replace_TAIL_ARRAY(EType retTy, const Arguments &args);
        virtual ExpressionPtr replace_CLAMP_INTEGER(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_WRAP_INTEGER(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_STRUCT_TO_TUPLE(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_TUPLE_TO_STRUCT(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_INTERPOLATE_TEXT(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_ENCODE_BIT(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_DECODE_BIT(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_ENCODE_BOOLEAN(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_DECODE_BOOLEAN(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_ENCODE_BYTE(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_DECODE_BYTE(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_BIT_TO_BITS(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_BITS_TO_BIT(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_BOOLEAN_TO_BITS(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_BITS_TO_BOOLEAN(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_BYTE_TO_BITS(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_BITS_TO_BYTE(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_CHAR_TO_BITS(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_BITS_TO_CHAR(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_REAL_TO_BITS(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_BITS_TO_REAL(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_BYTES_TO_BITS(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_BITS_TO_BYTES(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_ROOTTYPE_CAST(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_NEW(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_EQ_OPTIONAL(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_EQ_ARRAY(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_EQ_TUPLE(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_EQ_STRUCT(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_EQ_UNION(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_EQ_NAMED(EType retTy, const Arguments &args);
        virtual ExpressionPtr replace_LT_OPTIONAL(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_LT_ARRAY(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_LT_TUPLE(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_LT_STRUCT(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_LT_UNION(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_LT_NAMED(EType retTy, const Arguments &args);
        virtual ExpressionPtr replace_NEQ(EType retTy, const Arguments &args);
        virtual ExpressionPtr replace_LTE(EType retTy, const Arguments &args);
        virtual ExpressionPtr replace_STRINGIFY_BIT(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_STRINGIFY_BOOLEAN(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_STRINGIFY_CHAR(EType retTy, const Arguments &args);
        virtual ExpressionPtr replace_STRINGIFY_STRING(EType retTy, const Arguments &args);
        virtual ExpressionPtr replace_STRINGIFY_OPT(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_UNCHECKEDCAST_INTEGER_TO_INTEGER(EType retTy,
            const LambdaParameters &e);
        virtual ExpressionPtr replace_UNCHECKEDCAST_ARRAY(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_UNCHECKEDCAST_FUNCTION(EType retTy,
            const LambdaParameters &e);
        virtual ExpressionPtr replace_UNCHECKEDCAST_OPT(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_UNCHECKEDCAST_STRUCT(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_UNCHECKEDCAST_TUPLE(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_UNCHECKEDCAST_NAMEDTYPE(EType retTy,
            const LambdaParameters &e);
        virtual ExpressionPtr replace_CHECKEDCAST_FUNCTION(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_CHECKEDCAST_OPT(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_CHECKEDCAST_STRUCT(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_CHECKEDCAST_TUPLE(EType retTy, const LambdaParameters &e);
        virtual ExpressionPtr replace_CHECKEDCAST_NAMEDTYPE(EType retTy, const LambdaParameters &e);

        /** The operators to be replaced. If no set, all operators are to be replaced */
      public:
        const ::std::optional<Operators> operators;

        /** The current program */
      private:
        ir::Program::ProgramPtr program;

        /** The currently known globals program */
      private:
        ::std::vector<ir::GlobalValue::GlobalValuePtr> newFunctions;
      };
    }
  }
}
#endif
