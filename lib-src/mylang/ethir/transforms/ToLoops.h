#ifndef CLASS_MYLANG_ETHIR_TRANSFORMS_TOLOOPS_H
#define CLASS_MYLANG_ETHIR_TRANSFORMS_TOLOOPS_H

#ifndef CLASS_MYLANG_ETHIR_TRANSFORM_H
#include <mylang/ethir/Transform.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_TOARRAY_H
#include <mylang/ethir/ir/ToArray.h>
#endif

namespace mylang {
  namespace ethir {
    namespace transforms {

      /**
       * Replace any ToArray expression with an equivalent expression that collects
       * the elements produced by ToArray into an array.
       */
      class ToLoops: public Transform
      {
        ToLoops& operator=(const ToLoops&) = delete;
        ToLoops(const ToLoops&) = delete;

        /**
         * Create a transform to replace ALL operators
         */
      public:
        ToLoops();

      public:
        ~ToLoops();

        /**
         * Replace each to-array with a call to a lambda expression.
         * @param e an expression
         * @return a new expression or null if nothing needed to be replaced
         */
      public:
        ENode visit(const ir::ToArray::ToArrayPtr &e) override;
      };
    }
  }
}
#endif
