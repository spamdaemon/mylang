#ifndef CLASS_MYLANG_ETHIR_TRANSFORMS_PRUNEGLOBALS_H
#define CLASS_MYLANG_ETHIR_TRANSFORMS_PRUNEGLOBALS_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

namespace mylang {
  namespace ethir {
    namespace transforms {

      /**
       * Remove unused global variables.
       */
      class PruneGlobals
      {
        /** Destructor */
      private:
        ~PruneGlobals() = delete;

        /**
         * Remove any unused globals from the program.
         * @return a program
         */
      public:
        static EProgram transform(EProgram src);
      };
    }
  }
}
#endif
