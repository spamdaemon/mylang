#include <mylang/ethir/transforms/PruneGlobals.h>
#include <mylang/ethir/queries/FindReachableVariables.h>
#include <mylang/ethir/ir/nodes.h>

#include <cassert>
#include <map>

namespace mylang {
  namespace ethir {
    using namespace queries;
    using namespace ir;
    namespace transforms {

      EProgram PruneGlobals::transform(EProgram node)
      {

        FindReachableVariables::Names reachables = FindReachableVariables::find(node,
            [](EGlobal g) {return g->kind != GlobalValue::DEFAULT;});
        Program::Globals survivors;
        for (auto g : node->globals) {
          if (reachables.count(g->name) != 0) {
            survivors.push_back(g);
          }
        }

        if (survivors.size() == node->globals.size()) {
          return node;
        }

        return Program::create(survivors, node->processes, node->types);
      }
    }
  }
}
