#ifndef CLASS_MYLANG_ETHIR_TRANSFORMS_TRANSFORMSKIP_H
#define CLASS_MYLANG_ETHIR_TRANSFORMS_TRANSFORMSKIP_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_TRANSFORMRESULT_H
#include<mylang/ethir/TransformResult.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_LOOP_H
#include<mylang/ethir/ir/Loop.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_SKIPSTATEMENT_H
#include<mylang/ethir/ir/SkipStatement.h>
#endif

#include <functional>

namespace mylang {
  namespace ethir {
    namespace transforms {

      /**
       * Fuse loops into single loops to avoid generation of temporary
       * objects.
       */
      class TransformSkip
      {
        /**
         * A function to replaces a state with a new state.
         */
      public:
        typedef ::std::function<ir::Loop::State(ir::Loop::State)> ReplaceStateFN;

        /**
         * A function that replaces a skip statement with a new statement.
         * Returning nullptr is equivalent to return the argument.
         */
      public:
        typedef ::std::function<EStatement(ir::SkipStatement::SkipStatementPtr)> ReplaceSkipFN;

        /**
         * Replace current state with the actual state in any skip.
         * @param loop a loop
         */
      public:
        static TransformResult<ENode> replaceCurrentState(ENode loop);

        /**
         * Replace the next state in any skip statement or loop loop step.
         * This function will not traverse into Loops.
         * @param a node
         * @param replacement a replacement function
         */
      public:
        static TransformResult<ENode> replaceNextState(ENode root, ReplaceStateFN replacement);

        /**
         * Replace a skip statement with a new statement. This function will not traverse into Loops.
         * @param root whose yield statements should be replaced
         * @param replacement a replacement function
         */
      public:
        static TransformResult<ENode> replaceSkip(ENode root, ReplaceSkipFN replacement);
        /**
         * Replace the next state in any skip statement or loop loop step.
         * This function will not traverse into Loops.
         * @param a node
         * @param replacement a replacement function
         * @return loop states
         */
      public:
        static TransformResult<ir::Loop::States> replaceNextStates(const ir::Loop::States &states,
            ReplaceStateFN replacement);

        /**
         * Replace a skip statement with a new statement. This function will not traverse into Loops.
         * @param root whose yield statements should be replaced
         * @param replacement a replacement function
         * @return loop states
         */
      public:
        static TransformResult<ir::Loop::States> replaceSkips(const ir::Loop::States &states,
            ReplaceSkipFN replacement);
      };
    }
  }
}
#endif
