#include <mylang/ethir/transforms/NativeTyping.h>
#include <mylang/ethir/Transform.h>
#include <mylang/ethir/ethir.h>
#include <mylang/ethir/types/types.h>
#include <mylang/ethir/types/TypeVisitor.h>

#include <memory>
#include <stdexcept>
#include <vector>
#include <limits>

namespace mylang {
  namespace ethir {
    namespace transforms {
      namespace {
        static EVariable createTmp(const EType &ty)
        {
          Name name = Name::create("tmp");
          return ir::Variable::create(ir::Variable::Scope::FUNCTION_SCOPE, ty, name);
        }

        struct Pass: public Transform, public types::TypeVisitor
        {
          const NativeTyping::Converter &converter;

          Pass(const NativeTyping::Converter &xconverter, const EProgram &xprogram)
              : converter(xconverter), program(xprogram)
          {
          }

          ~Pass()
          {
          }

          EType toNativeType(const EType &type)
          {
            return converter(type);
          }

          EType toType(EType ty)
          {
            if (!ty) {
              return ty;
            }
            auto i = convertedTypes.find(ty);
            if (i != convertedTypes.end()) {
              return i->second;
            }

            resultType = nullptr;
            ty->accept(*this);
            if (!resultType) {
              throw ::std::runtime_error(
                  "internal error: could not transform type " + ty->toString());
            }
            convertedTypes[ty] = resultType;
            convertedTypes[resultType] = resultType;
            return resultType;
          }

          EType visitType(const EType &type) override
          {
            auto ty = toType(type);
            if (ty == type) {
              return nullptr;
            }
            if (ty) {
              auto nt1 = ty->self<types::NamedType>();
              auto nt2 = type->self<types::NamedType>();
              if (nt1 && nt2) {
                if (nt1->isResolved() && nt2->isResolved()) {
                  if (nt1->isSameType(*nt2) && nt1->resolve()->isSameType(*nt2->resolve())) {
                    return nullptr;
                  }
                }
                return ty;
              }
              if (ty->isSameType(*type)) {
                return nullptr;
              }
            }

            return ty;
          }

          void visit(const std::shared_ptr<const types::CharType> &ty) override
          {
            resultType = ty;
          }

          void visit(const std::shared_ptr<const types::BuilderType> &type)
          override
          {
            auto retTy = toType(type->product)->self<types::BuilderType::ProductType>();
            resultType = types::BuilderType::get(retTy);
          }

          void visit(const std::shared_ptr<const types::FunctionType> &type)
          override
          {
            auto retTy = toType(type->returnType);
            types::FunctionType::Parameters params;
            for (auto p : type->parameters) {
              params.push_back(toType(p));
            }
            resultType = types::FunctionType::get(retTy, params);
          }

          void visit(const std::shared_ptr<const types::GenericType> &ty) override
          {
            resultType = ty;
          }

          void visit(const std::shared_ptr<const types::RealType> &ty) override
          {
            resultType = ty;
          }

          void visit(const std::shared_ptr<const types::OutputType> &type) override
          {
            auto ty = toType(type->element);
            resultType = types::OutputType::get(ty);
          }

          void visit(const std::shared_ptr<const types::BitType> &ty) override
          {
            resultType = ty;
          }

          void visit(const std::shared_ptr<const types::BooleanType> &ty) override
          {
            resultType = ty;
          }

          void visit(const std::shared_ptr<const types::ByteType> &ty) override
          {
            resultType = ty;
          }
          void visit(const std::shared_ptr<const types::IntegerType> &ty) override
          {
            resultType = toNativeType(ty);
          }

          void visit(const std::shared_ptr<const types::InputType> &type) override
          {
            auto ty = toType(type->element);
            resultType = types::InputType::get(ty);
          }

          void visit(const std::shared_ptr<const types::OptType> &type) override
          {
            auto ty = toType(type->element);
            resultType = types::OptType::get(ty);
          }

          void visit(const std::shared_ptr<const types::NamedType> &type) override
          {
            struct Holder
            {
              EType type;
            };

            struct Resolver
            {
              Resolver()
                  : holder(::std::make_shared<Holder>())
              {
              }
              EType operator()() const
              {
                return holder->type;
              }
              ::std::shared_ptr<Holder> holder;
            };

            Resolver resolver;
            ::std::function<EType()> fn(resolver);

            auto resolvedType = type->resolve();
            auto newName = type->name(); // TypeName::create();
            auto namedType = types::NamedType::get(newName, resolver);
            // prevent recursion when converting the derived type
            convertedTypes[type] = namedType;
            convertedTypes[namedType] = namedType;
            resolver.holder->type = toType(resolvedType);
            resultType = namedType;
          }

          void visit(const std::shared_ptr<const types::ArrayType> &type) override
          {
            auto ty = toType(type->element);
            auto lengthTy = toType(type->lengthType)->self<types::IntegerType>();
            auto indexTy = type->indexType;
            if (indexTy.has_value()) {
              indexTy = toType(indexTy.value())->self<types::IntegerType>();
            }
            auto counterTy = toType(type->counterType)->self<types::IntegerType>();

            resultType = types::ArrayType::get(ty, lengthTy, indexTy, counterTy);
          }

          void visit(const std::shared_ptr<const types::StructType> &type) override
          {
            ::std::vector<types::StructType::Member> members;
            for (auto m : type->members) {
              auto ty = toType(m.type);
              members.emplace_back(m.name, ty);
            }
            resultType = types::StructType::get(members);
          }

          void visit(const std::shared_ptr<const types::VoidType> &ty) override
          {
            resultType = ty;
          }

          void visit(const std::shared_ptr<const types::UnionType> &type) override
          {
            auto discriminant = types::UnionType::Discriminant(type->discriminant.name,
                toType(type->discriminant.type));
            ::std::vector<types::UnionType::Member> members;

            for (auto m : type->members) {
              auto ty = toType(m.type);
              auto lit = transformExpr(m.discriminant).actual->self<ir::Literal>()->castTo(
                  discriminant.type);
              members.emplace_back(lit, m.name, ty);
            }
            resultType = types::UnionType::get(discriminant, members);
          }

          void visit(const std::shared_ptr<const types::StringType> &ty) override
          {
            resultType = ty;
          }

          void visit(const std::shared_ptr<const types::TupleType> &type) override
          {
            ::std::vector<EType> types;
            for (auto ty : type->types) {
              types.push_back(toType(ty));
            }
            resultType = types::TupleType::get(types);
          }

          void visit(const std::shared_ptr<const types::MutableType> &type) override
          {
            auto ty = toType(type->element);
            resultType = types::MutableType::get(ty);
          }

          void visit(const std::shared_ptr<const types::ProcessType> &type) override
          {
            types::ProcessType::Ports ports;
            for (auto p : type->ports) {
              ports[p.first] = toType(p.second)->self<types::InputOutputType>();
            }
            resultType = types::ProcessType::get(ports);
          }

          ENode transformArithmeticOp(const ir::OperatorExpression::OperatorExpressionPtr &e)
          {
            auto lhs = e->arguments.at(0);
            auto rhs = e->arguments.at(1);

            // if we don't have an integer type then do the default transform
            // probably have to change this eventually for float32 and float64
            auto lhsTy = lhs->type->self<types::IntegerType>();
            auto rhsTy = rhs->type->self<types::IntegerType>();

            if (nullptr == lhsTy || nullptr == rhsTy) {
              return Transform::visit(e);
            }

            auto resultTy = toNativeType(e->type);
            auto operandTy = toNativeType(lhsTy->unionWith(rhsTy)->unionWith(resultTy));

            bool isNew = !operandTy->isSameType(*resultTy);

            EExpression left, right, tmp_result, result;
            if (operandTy->isSameType(*lhsTy)) {
              left = lhs;
            } else {
              isNew = true;
              left = ir::OperatorExpression::create(operandTy,
                  ir::OperatorExpression::OP_CHECKED_CAST, lhs);
            }
            if (operandTy->isSameType(*rhsTy)) {
              right = rhs;
            } else {
              isNew = true;
              right = ir::OperatorExpression::create(operandTy,
                  ir::OperatorExpression::OP_CHECKED_CAST, rhs);
            }
            if (!isNew) {
              return nullptr;
            }

            EVariable varLeft = createTmp(operandTy);
            EVariable varRight = createTmp(operandTy);
            EVariable varResult = createTmp(operandTy);

            tmp_result = ir::OperatorExpression::create(operandTy, e->name, varLeft, varRight);
            result = ir::OperatorExpression::create(resultTy,
                ir::OperatorExpression::OP_CHECKED_CAST, varResult);
            return ir::LetExpression::create(varLeft, left,
                ir::LetExpression::create(varRight, right,
                    ir::LetExpression::create(varResult, tmp_result, result)));
          }

          ENode transformComparisonOp(const ir::OperatorExpression::OperatorExpressionPtr &e)
          {
            auto lhs = e->arguments.at(0);
            auto rhs = e->arguments.at(1);

            // if we don't have an integer type then do the default transform
            // probably have to change this eventually for float32 and float64
            auto lhsTy = lhs->type->self<types::IntegerType>();
            auto rhsTy = rhs->type->self<types::IntegerType>();

            if (nullptr == lhsTy || nullptr == rhsTy) {
              return Transform::visit(e);
            }

            auto operandTy = toNativeType(lhsTy->unionWith(rhsTy));

            bool isNew = false;

            EExpression left, right, result;
            if (operandTy->isSameType(*lhsTy)) {
              left = lhs;
            } else {
              isNew = true;
              left = ir::OperatorExpression::create(operandTy,
                  ir::OperatorExpression::OP_CHECKED_CAST, lhs);
            }
            if (operandTy->isSameType(*rhsTy)) {
              right = rhs;
            } else {
              isNew = true;
              right = ir::OperatorExpression::create(operandTy,
                  ir::OperatorExpression::OP_CHECKED_CAST, rhs);
            }
            if (!isNew) {
              return nullptr;
            }

            EVariable varLeft = createTmp(operandTy);
            EVariable varRight = createTmp(operandTy);

            result = ir::OperatorExpression::create(e->type, e->name, varLeft, varRight);
            return ir::LetExpression::create(varLeft, left,
                ir::LetExpression::create(varRight, right, result));
          }

          ENode visit(const ir::OperatorExpression::OperatorExpressionPtr &e)
          {
            switch (e->name) {
            case ir::OperatorExpression::OP_ADD:
            case ir::OperatorExpression::OP_SUB:
            case ir::OperatorExpression::OP_MUL:
            case ir::OperatorExpression::OP_DIV:
            case ir::OperatorExpression::OP_MOD:
            case ir::OperatorExpression::OP_REM:
              return transformArithmeticOp(e);
            case ir::OperatorExpression::OP_EQ:
            case ir::OperatorExpression::OP_NEQ:
            case ir::OperatorExpression::OP_LT:
            case ir::OperatorExpression::OP_LTE:
              return transformComparisonOp(e);

            default:
              return Transform::visit(e);
            }
          }

          EType resultType;
          ::std::map<EType, EType> convertedTypes;
          const EProgram &program;
        };

      }

      NativeTyping::NativeTyping(Converter conv)
          : converter(conv)
      {
      }

      NativeTyping::~NativeTyping()
      {
      }

      mylang::ethir::EProgram NativeTyping::transform(mylang::ethir::EProgram prog) const
      {
        Pass pass(converter, prog);

        auto node = pass.apply(prog);
        return node->self<ir::Program>();
      }
    }
  }
}
