#ifndef CLASS_MYLANG_ETHIR_TRANSFORMS_RENAMEFREEVARIABLES_H
#define CLASS_MYLANG_ETHIR_TRANSFORMS_RENAMEFREEVARIABLES_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_ETHIR_IR_VARIABLE_H
#include <mylang/ethir/ir/Variable.h>
#endif

#ifndef FILE_MYLANG_DEFS_H
#include <mylang/defs.h>
#endif

#include <map>

namespace mylang {
  namespace ethir {
    namespace transforms {

      /**
       * Rename the free variables in a node, if they are of FUNCTION_SCOPE.
       */
      class RenameFreeVariables
      {
        struct Result
        {
          /** The resulting new node */
          ENode result;

          /** A mapping of names of free variables to new variables.
           * The key is the previous name, the value is the new name
           */
          ir::Variable::Map<EVariable> old2new;
        };

        /** Destructor */
      private:
        ~RenameFreeVariables() = delete;

        /**
         * Copy a node and rename any variables in the scope of the node.
         * @return a program
         */
      public:
        static Result rename(ENode src);

      };
    }
  }
}
#endif
