#ifndef CLASS_MYLANG_ETHIR_TRANSFORMS_COPYRENAME_H
#define CLASS_MYLANG_ETHIR_TRANSFORMS_COPYRENAME_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#ifndef CLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif

#include <map>

namespace mylang {
  namespace ethir {
    namespace transforms {

      /**
       * Copy a node and rename all names that introduced with the scope
       * of the node. Names external to the node are not renamed.
       */
      class CopyRename
      {
        /** Destructor */
      private:
        ~CopyRename() = delete;

        /**
         * Copy a node and rename any variables in the scope of the node.
         * @return a program
         */
      public:
        static ENode copy(ENode src);

        /**
         * Copy a node and rename any variables in the scope of the node.
         * Any variables that are encountered and not in the newName list
         * are given random new names.
         * @param newNames a list of newNames
         * @return a program
         */
      public:
        static ENode copy(ENode src,
            ::std::map<mylang::names::Name::Ptr, mylang::names::Name::Ptr> newNames);

      };
    }
  }
}
#endif
