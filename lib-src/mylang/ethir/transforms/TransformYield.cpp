#include <mylang/ethir/transforms/TransformYield.h>
#include <mylang/ethir/Transform.h>
#include <mylang/ethir/ir/nodes.h>

namespace mylang {
  namespace ethir {
    using namespace ir;
    using namespace types;
    namespace transforms {

      TransformResult<ENode> TransformYield::replaceYield(ENode root, ReplaceYieldFN replacement)
      {
        struct T: public Transform
        {
          T(ReplaceYieldFN fn)
              : replace(fn)
          {
          }
          ~T()
          {
          }

          ENode visit(const Loop::LoopPtr&) override final
          {
            return nullptr;
          }
          ENode visit(const YieldStatement::YieldStatementPtr &yield) override final
          {
            return replace(yield);
          }

          ReplaceYieldFN replace;
        };
        T tx(replacement);
        return tx.transformNode(root);
      }

      TransformResult<ir::Loop::States> TransformYield::replaceYields(
          const ir::Loop::States &states, ReplaceYieldFN replacement)
      {
        ir::Loop::States res;
        bool isNew = false;
        for (auto s : states) {
          auto t = replaceYield(s.second, replacement);
          isNew = isNew || t.isNew;
          res.insert( { s.first, t.actual->self<ir::Loop::Step>() });
        }
        return {::std::move(res),isNew};
      }

    }
  }
}
