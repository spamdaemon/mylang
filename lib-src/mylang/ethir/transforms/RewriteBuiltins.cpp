#include <mylang/ethir/transforms/RewriteBuiltins.h>
#include <mylang/ethir/ir/StatementBuilder.h>
#include <mylang/ethir/ir/Program.h>
#include <mylang/ethir/types/types.h>
#include <mylang/ethir/ir/nodes.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    using namespace ir;
    using namespace types;
    namespace transforms {
      namespace {

        static EExpression makeConditionalExpr(EExpression condExpr, EExpression iftrue,
            EExpression iffalse)
        {
          auto retTy = iftrue->type->unionWith(iffalse->type);
          auto lambda = LambdaExpression::create( { },
              [&](LambdaExpression::Arguments, StatementBuilder &sb) {
                auto cond = sb.declareLocalValue(condExpr);
                sb.appendIf(cond, [&](StatementBuilder& st) {
                      auto r = st.declareLocalValue(iftrue);
                      st.appendReturn(r);
                    }, [&](StatementBuilder& sf) {
                      auto r = sf.declareLocalValue(iffalse);
                      sf.appendReturn(r);
                    });
                return retTy;
              });
          return LetExpression::create(lambda, [&](EVariable l) {
            return OperatorExpression::create(retTy,OperatorExpression::OP_CALL, {l});
          });
        }

        static EExpression makeMaxExpr(EExpression lhs, EExpression rhs)
        {
          auto lhsTy = lhs->type->self<IntegerType>();
          auto rhsTy = rhs->type->self<IntegerType>();
          assert(lhsTy && rhsTy && "makeMaxExpr only works on integers");
          auto retTy = IntegerType::maxOf(lhsTy, rhsTy);
          return LetExpression::create(lhs, [&](EVariable left) {
            return LetExpression::create(rhs,[&](EVariable right) {
                  auto lhsIsMax = OperatorExpression::checked_cast(retTy, left);
                  auto rhsIsMax = OperatorExpression::checked_cast(retTy, right);
                  auto cond = OperatorExpression::create(OperatorExpression::OP_LT, left, right);
                  return makeConditionalExpr(cond,rhsIsMax,lhsIsMax);
                });
          });
        }

        static EExpression makeMinExpr(EExpression lhs, EExpression rhs)
        {
          auto lhsTy = lhs->type->self<IntegerType>();
          auto rhsTy = rhs->type->self<IntegerType>();
          assert(lhsTy && rhsTy && "makeMinExpr only works on integers");
          auto retTy = IntegerType::minOf(lhsTy, rhsTy);
          return LetExpression::create(lhs, [&](EVariable left) {
            return LetExpression::create(rhs,[&](EVariable right) {
                  auto lhsIsMin = OperatorExpression::checked_cast(retTy, left);
                  auto rhsIsMin = OperatorExpression::checked_cast(retTy, right);
                  auto cond = OperatorExpression::create(OperatorExpression::OP_LT, left, right);
                  return makeConditionalExpr(cond,lhsIsMin,rhsIsMin);
                });
          });
        }

        static EExpression isLTExpr(EExpression lhs, EExpression rhs)
        {
          return LetExpression::create(lhs, [&](EVariable left) {
            return LetExpression::create(rhs,[&](EVariable right) {
                  return OperatorExpression::create(OperatorExpression::OP_LT,left,right);
                });
          });
        }

        static LambdaExpression::Parameters getParameters(
            const ir::OperatorExpression::OperatorExpressionPtr &e)
        {
          LambdaExpression::Parameters p;
          for (size_t i = 0; i < e->arguments.size(); ++i) {
            auto arg_i = Name::create("arg_" + ::std::to_string(i));
            p.push_back(Parameter::create(arg_i, e->arguments.at(i)->type));
          }
          return p;
        }

        static ::std::shared_ptr<const ArrayType> toUnboundedArray(
            ::std::shared_ptr<const ArrayType> ty)
        {
          if (ty->minSize.is0()) {
            return ty;
          } else {
            return ArrayType::get(ty->element, Integer::ZERO(), ty->maxSize);
          }
        }

        static Name newName(const char *prefix)
        {
          if (prefix) {
            return Name::create(prefix);
          } else {
            return Name::create("tmp");
          }
        }

        static Variable::VariablePtr newLocal(const char *prefix, EType ty)
        {
          return Variable::create(Variable::Scope::FUNCTION_SCOPE, ::std::move(ty), newName(prefix));
        }

        static Variable::VariablePtr newLocal(EType ty)
        {
          return newLocal(nullptr, ::std::move(ty));
        }

        static Variable::VariablePtr createEmpty(StatementBuilder &builder,
            ::std::shared_ptr<const ArrayType> arrTy)
        {
          assert(arrTy->minSize == 0);
          ::std::vector<Variable::VariablePtr> args;
          auto expr = ir::OperatorExpression::create(arrTy, ir::OperatorExpression::OP_NEW, args);
          return builder.declareLocalValue(expr);
        }

        static Variable::VariablePtr createSingleton(StatementBuilder &builder,
            Variable::VariablePtr a)
        {
          auto arrTy = ArrayType::get(a->type, 1, 1);
          auto expr = ir::OperatorExpression::create(arrTy, ir::OperatorExpression::OP_NEW, a);
          return builder.declareLocalValue(expr);
        }

        static Variable::VariablePtr doCheckedCast(StatementBuilder &builder, EType target,
            Variable::VariablePtr var)
        {
          if (var->type->isSameType(*target)) {
            return var;
          }
#ifdef DEBUG
          ::std::cerr << "CHECKED CAST : " << target->toString() << " from " << var->type->toString() << ::std::endl;
#endif

          return builder.declareLocalValue(
              ir::OperatorExpression::create(target, ir::OperatorExpression::OP_CHECKED_CAST, var));
        }

        static Variable::VariablePtr doUncheckedCast(StatementBuilder &builder, EType target,
            Variable::VariablePtr var)
        {
          if (var->type->isSameType(*target)) {
            return var;
          }
          ::std::cerr << "UNCHECKED CAST : " << target->toString() << " from "
              << var->type->toString() << ::std::endl;
          return builder.declareLocalValue(
              ir::OperatorExpression::create(target, ir::OperatorExpression::OP_UNCHECKED_CAST,
                  var));
        }

        static Variable::VariablePtr doOperator(StatementBuilder &builder,
            OperatorExpression::Operator op, const ir::OperatorExpression::Arguments &args)
        {
          auto expr = ir::OperatorExpression::create(op, args);
          return builder.declareLocalValue(expr);
        }

        static LetExpression::LetExpressionPtr doLet(Expression::ExpressionPtr e,
            ::std::function<Expression::ExpressionPtr(Variable::VariablePtr)> in)
        {
          auto v = Variable::create(Variable::Scope::FUNCTION_SCOPE, e->type, newName("let"));
          return LetExpression::create(v, e, in(v));
        }

        static LetExpression::LetExpressionPtr doLet(OperatorExpression::Operator op,
            const ir::OperatorExpression::Arguments &args,
            ::std::function<Expression::ExpressionPtr(Variable::VariablePtr)> in)
        {
          auto e = OperatorExpression::create(op, args);
          return doLet(e, in);
        }

        static Variable::VariablePtr doOperator(StatementBuilder &builder, EType retTy,
            OperatorExpression::Operator op, const ir::OperatorExpression::Arguments &args)
        {
          auto expr = ir::OperatorExpression::create(retTy, op, args);
          return builder.declareLocalValue(expr);
        }

        static Variable::VariablePtr doArrayIndex(StatementBuilder &builder,
            const Variable::VariablePtr &arr, const Variable::VariablePtr &idx)
        {
          auto arrTy = arr->type->self<ArrayType>();
          return doOperator(builder, OperatorExpression::OP_INDEX, { arr, idx });
        }

        static Variable::VariablePtr doArraySubrange(StatementBuilder &builder, EType retTy,
            const Variable::VariablePtr &arr, const Variable::VariablePtr &start,
            const Variable::VariablePtr &end)
        {
          return doOperator(builder, retTy, OperatorExpression::OP_SUBRANGE, { arr, start, end });
        }

        static Variable::VariablePtr doArrayLen(StatementBuilder &builder,
            const Variable::VariablePtr &arr)
        {
          auto arrTy = arr->type->self<ArrayType>();
          return doOperator(builder, OperatorExpression::OP_SIZE, { arr });
        }

        static Variable::VariablePtr doIsPresent(StatementBuilder &builder,
            const Variable::VariablePtr &opt)
        {
          return doOperator(builder, OperatorExpression::OP_IS_PRESENT, { opt });
        }

        static Variable::VariablePtr doGetOpt(StatementBuilder &builder,
            const Variable::VariablePtr &opt)
        {
          auto ty = opt->type->self<OptType>();
          return doOperator(builder, OperatorExpression::OP_GET, { opt });
        }

        static Variable::VariablePtr doOptify(StatementBuilder &builder,
            const Variable::VariablePtr &obj)
        {
          auto ty = OptType::get(obj->type);
          return doOperator(builder, ty, OperatorExpression::OP_NEW, { obj });
        }

        static Variable::VariablePtr doCall(StatementBuilder &builder, Variable::VariablePtr fn,
            const ir::OperatorExpression::Arguments &args)
        {
          auto fnTy = fn->type->self<FunctionType>();
          assert(fnTy->parameters.size() == args.size() && "Argument mismatch");

          ir::OperatorExpression::Arguments arguments;
          arguments.push_back(fn);
          arguments.insert(arguments.end(), args.begin(), args.end());
          return doOperator(builder, ir::OperatorExpression::OP_CALL, arguments);
        }

        static EExpression incrementIndex(EVariable indexVar, EType ty)
        {
          return LetExpression::create(LiteralInteger::create(1),
              [&](
                  EVariable one) {
                    return LetExpression::create(OperatorExpression::create(OperatorExpression::OP_ADD,indexVar,one),[&](EVariable v) {
                          EExpression res;
                          if (v->type->isSameType(*ty)) {
                            res = v;
                          } else {
                            res= OperatorExpression::create(ty,OperatorExpression::OP_CHECKED_CAST,v);
                          }
                          return res;
                        });
                  });
        }

        static void incrementIndex(StatementBuilder &builder, Variable::VariablePtr indexVar)
        {
          auto ity = indexVar->type->self<IntegerType>();
          auto one = builder.declareLocalValue(LiteralInteger::create(ity, 1));
          auto inc = doOperator(builder, OperatorExpression::OP_ADD, { indexVar, one });
          auto casted = doCheckedCast(builder, ity, inc);
          builder.appendUpdate(indexVar, casted);
        }

        static Variable::VariablePtr doMapArray(StatementBuilder &builder,
            Variable::VariablePtr array,
            ::std::function<Variable::VariablePtr(StatementBuilder&, Variable::VariablePtr)> fn)
        {
          auto arrTy = array->type->self<ArrayType>();
          auto lambda = builder.declareLocalLambda( { arrTy->element },
              [&](StatementBuilder::LambdaParameters params) {
                StatementBuilder b;
                auto res = fn(b,params.at(0)->variable);
                b.appendReturn(res);
                return LambdaExpression::create(params, res->type, b.build());
              });
          auto retTy = arrTy->copy(lambda->type->self<FunctionType>()->returnType);
          return doOperator(builder, retTy, OperatorExpression::OP_MAP_ARRAY, { array, lambda });
        }

        static Variable::VariablePtr doMapOpt(StatementBuilder &builder, EType outElemTy,
            Variable::VariablePtr opt,
            ::std::function<Variable::VariablePtr(StatementBuilder&, Variable::VariablePtr)> fn)
        {
          auto optTy = opt->type->self<OptType>();
          auto retTy = optTy->copy(outElemTy);
          auto lambda = builder.declareLocalLambda( { optTy->element },
              [&](StatementBuilder::LambdaParameters params) {
                StatementBuilder b;
                auto res = fn(b,params.at(0)->variable);
                b.appendReturn(res);
                return LambdaExpression::create(params, outElemTy, b.build());
              });
          return doOperator(builder, retTy, OperatorExpression::OP_MAP_OPTIONAL, { opt, lambda });
        }

        static Variable::VariablePtr doAppendElement(StatementBuilder &builder,
            Variable::VariablePtr array, Variable::VariablePtr element)
        {
          auto arrTy = array->type->self<ArrayType>();
          auto retTy = ArrayType::get(arrTy->element, arrTy->minSize.increment(),
              arrTy->maxSize.increment());

          auto rhs = createSingleton(builder, element);
          return builder.declareLocalValue(
              ir::OperatorExpression::create(retTy, ir::OperatorExpression::OP_CONCATENATE, { array,
                  rhs }));
        }

#if USE_LOOPS==0
        static Variable::VariablePtr doPrependElement(StatementBuilder &builder,
            Variable::VariablePtr array, Variable::VariablePtr element)
        {
          auto arrTy = array->type->self<ArrayType>();
          auto retTy = arrTy;
          if (arrTy->maxSize.has_value()) {
            retTy = ArrayType::get(arrTy->element, arrTy->minSize + 1, arrTy->maxSize.value() + 1);
          } else {
            retTy = ArrayType::get(arrTy->element, arrTy->minSize + 1, ::std::nullopt);
          }

          auto rhs = createSingleton(builder, element);
          return builder.declareLocalValue(
              ir::OperatorExpression::create(retTy, ir::OperatorExpression::OP_CONCATENATE, { rhs,
                  array }));
        }
#endif

        static bool matches(GlobalValue::GlobalValuePtr g, const ::std::string &operatorName,
            OperatorExpression::OperatorExpressionPtr op)
        {
          auto fn = g->type->self<FunctionType>();
          if (fn && g->name.source()->localName() == operatorName) {
            if (fn->parameters.size() == op->arguments.size()
                && fn->returnType->isSameType(*op->type)) {
              for (size_t i = 0; i < fn->parameters.size(); ++i) {
                if (!fn->parameters.at(i)->isSameType(*op->arguments.at(i)->type)) {
                  return false;
                }
              }
              return true;
            }
          }
          return false;
        }

        static GlobalValue::GlobalValuePtr findFunction(Program::ProgramPtr prg,
            const Program::Globals &globals, const ::std::string &name,
            OperatorExpression::OperatorExpressionPtr op)
        {
          for (auto g : globals) {
            if (matches(g, name, op)) {
              return g;
            }
          }
          for (auto g : prg->globals) {
            if (matches(g, name, op)) {
              return g;
            }
          }
          return nullptr;
        }
      }

      RewriteBuiltins::RewriteBuiltins()
      {
      }

      RewriteBuiltins::RewriteBuiltins(Operators ops)
          : operators(ops)
      {

      }

      RewriteBuiltins::~RewriteBuiltins()
      {
      }

      ENode RewriteBuiltins::visit(const ir::Program::ProgramPtr &node)
      {
        if (program) {
          throw ::std::runtime_error("Program already exists");
        }
        program = node;
        auto res = Transform::visit(node);

        if (!newFunctions.empty()) {
          if (!res) {
            res = node;
          }
          auto prg = res->self<Program>();

          // create a new result and add the new functions in
          newFunctions.insert(newFunctions.end(), prg->globals.begin(), prg->globals.end());
          res = Program::create(::std::move(newFunctions), prg->processes, prg->types);
        }
        program = nullptr;

        return res;
      }

      ENode RewriteBuiltins::visit(const ir::OperatorExpression::OperatorExpressionPtr &e)
      {
        if (operators.has_value() && operators.value().count(e->name) == 0) {
          return Transform::visit(e);
        }

        auto tmp = Transform::visit(e);
        if (tmp) {
          return tmp;
        }

        // assume the arguments have already been dealt with

        auto fnName = OperatorExpression::operatorName(e->name);
        auto gfunc = findFunction(program, newFunctions, fnName, e);
        Variable::VariablePtr lambda;
        if (gfunc) {
          lambda = gfunc->variable;
        } else {
          // create a lambda expression
          Expression::ExpressionPtr expr = replace(e);
          if (!expr) {
            return nullptr;
          }
          if (auto lambdaFN = expr->self<LambdaExpression>()) {
            lambda = Variable::create(Variable::Scope::GLOBAL_SCOPE, lambdaFN->type,
                newName(fnName));
            gfunc = GlobalValue::create(lambda, lambdaFN, GlobalValue::WELL_KNOWN);
            newFunctions.push_back(gfunc);
          } else {
            assert(expr->type->isSameType(*e->type));
            return expr;
          }
        }

        OperatorExpression::Arguments args;
        args.push_back(lambda);
        args.insert(args.end(), e->arguments.begin(), e->arguments.end());
        auto res = OperatorExpression::create(e->type, OperatorExpression::OP_CALL, args);
        return res;
      }

      Expression::ExpressionPtr RewriteBuiltins::replace(
          const ir::OperatorExpression::OperatorExpressionPtr &e)
      {
        switch (e->name) {
        case OperatorExpression::OP_WRAP:
          return replace_WRAP_INTEGER(e->type, getParameters(e));
        case OperatorExpression::OP_CLAMP:
          return replace_CLAMP_INTEGER(e->type, getParameters(e));
        case OperatorExpression::OP_HEAD:
          if (e->arguments.at(0)->type->self<ArrayType>()) {
            return replace_HEAD_ARRAY(e->type, e->arguments);
          } else {
            return replace_HEAD_TUPLE(e->type, e->arguments);
          }
        case OperatorExpression::OP_TAIL:
          if (e->arguments.at(0)->type->self<ArrayType>()) {
            return replace_TAIL_ARRAY(e->type, e->arguments);
          } else {
            return replace_TAIL_TUPLE(e->type, getParameters(e));
          }
        case OperatorExpression::OP_FOLD:
          return replace_FOLD_ARRAY(e->type, getParameters(e));
        case OperatorExpression::OP_FIND:
          return replace_FIND_ARRAY(e->type, getParameters(e));
        case OperatorExpression::OP_CONCATENATE:
          if (e->type->self<ArrayType>()) {
            return replace_CONCATENATE_ARRAY(e->type, e->arguments);
          } else {
            // no string concatenate yet
            return nullptr;
          }

        case OperatorExpression::OP_PAD:
          if (e->type->self<ArrayType>()) {
            return replace_PAD_ARRAY(e->type, e->arguments);
          } else {
            // no string concatenate yet
            return nullptr;
          }

        case OperatorExpression::OP_TRIM:
          if (e->type->self<ArrayType>()) {
            return replace_TRIM_ARRAY(e->type, e->arguments);
          } else {
            // no string concatenate yet
            return nullptr;
          }

        case OperatorExpression::OP_TAKE:
          if (e->type->self<ArrayType>()) {
            return replace_TAKE_ARRAY(e->type, e->arguments);
          } else {
            // no string concatenate yet
            return nullptr;
          }

        case OperatorExpression::OP_DROP:
          if (e->type->self<ArrayType>()) {
            return replace_DROP_ARRAY(e->type, e->arguments);
          } else {
            // no string concatenate yet
            return nullptr;
          }

        case OperatorExpression::OP_MAP_ARRAY:
#if USE_LOOPS==1
          return replace_MAP_ARRAY(e->type, e->arguments);
#else
          return replace_MAP_ARRAY(e->type, getParameters(e));
#endif
        case OperatorExpression::OP_MAP_OPTIONAL:
          return replace_MAP_OPTIONAL(e->type, getParameters(e));
        case OperatorExpression::OP_FILTER_ARRAY:
#if USE_LOOPS==1
          return replace_FILTER_ARRAY(e->type, e->arguments);
#else
          return replace_FILTER_ARRAY(e->type, getParameters(e));
#endif
        case OperatorExpression::OP_FILTER_OPTIONAL:
          return replace_FILTER_OPTIONAL(e->type, getParameters(e));
        case OperatorExpression::OP_REVERSE:
#if USE_LOOPS==1
          return replace_REVERSE_ARRAY(e->type, e->arguments);
#else
          return replace_REVERSE_ARRAY(e->type, getParameters(e));
#endif
        case OperatorExpression::OP_ZIP:
#if USE_LOOPS==1
          return replace_ZIP_ARRAY(e->type, e->arguments);
#else
          return replace_ZIP_ARRAY(e->type, getParameters(e));
#endif
        case OperatorExpression::OP_MERGE_TUPLES:
          return replace_MERGE_TUPLES(e->type, getParameters(e));
        case OperatorExpression::OP_STRUCT_TO_TUPLE:
          return replace_STRUCT_TO_TUPLE(e->type, getParameters(e));
        case OperatorExpression::OP_TUPLE_TO_STRUCT:
          return replace_TUPLE_TO_STRUCT(e->type, getParameters(e));
        case OperatorExpression::OP_INTERPOLATE_TEXT:
          return replace_INTERPOLATE_TEXT(e->type, getParameters(e));
        case OperatorExpression::OP_PARTITION:
#if USE_LOOPS==1
          return replace_PARTITION_ARRAY(e->type, e->arguments);
#else
          return replace_PARTITION_ARRAY(e->type, getParameters(e));
#endif
        case OperatorExpression::OP_FLATTEN:
          if (e->arguments.at(0)->type->self<ArrayType>()) {
#if USE_LOOPS==1
            return replace_FLATTEN_ARRAY(e->type, e->arguments);
#else
            return replace_FLATTEN_ARRAY(e->type, getParameters(e));
#endif
          } else if (e->arguments.at(0)->type->self<OptType>()) {
            return replace_FLATTEN_OPTIONAL(e->type, getParameters(e));
          }
          break;
        case OperatorExpression::OP_TO_BYTES:
          if (e->arguments.at(0)->type->self<BitType>()) {
            return replace_ENCODE_BIT(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<BooleanType>()) {
            return replace_ENCODE_BOOLEAN(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<ByteType>()) {
            return replace_ENCODE_BYTE(e->type, getParameters(e));
          }
          break;
        case OperatorExpression::OP_FROM_BYTES:
          if (e->type->self<BitType>()) {
            return replace_DECODE_BIT(e->type, getParameters(e));
          } else if (e->type->self<BooleanType>()) {
            return replace_DECODE_BOOLEAN(e->type, getParameters(e));
          } else if (e->type->self<ByteType>()) {
            return replace_DECODE_BYTE(e->type, getParameters(e));
          }
          break;
        case OperatorExpression::OP_BITS_TO_BYTES:
          return replace_BITS_TO_BYTES(e->type, getParameters(e));
          break;
        case OperatorExpression::OP_BYTES_TO_BITS:
          return replace_BYTES_TO_BITS(e->type, getParameters(e));
        case OperatorExpression::OP_TO_BITS:
          if (e->arguments.at(0)->type->self<BitType>()) {
            return replace_BIT_TO_BITS(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<BooleanType>()) {
            return replace_BOOLEAN_TO_BITS(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<ByteType>()) {
            return replace_BYTE_TO_BITS(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<CharType>()) {
            return replace_CHAR_TO_BITS(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<RealType>()) {
            return replace_REAL_TO_BITS(e->type, getParameters(e));
          }
          break;
        case OperatorExpression::OP_FROM_BITS:
          if (e->type->self<BitType>()) {
            return replace_BITS_TO_BIT(e->type, getParameters(e));
          } else if (e->type->self<BooleanType>()) {
            return replace_BITS_TO_BOOLEAN(e->type, getParameters(e));
          } else if (e->type->self<ByteType>()) {
            return replace_BITS_TO_BYTE(e->type, getParameters(e));
          } else if (e->type->self<CharType>()) {
            return replace_BITS_TO_CHAR(e->type, getParameters(e));
          } else if (e->type->self<RealType>()) {
            return replace_BITS_TO_REAL(e->type, getParameters(e));
          }
          break;
        case OperatorExpression::OP_ROOTTYPE_CAST:
          return replace_ROOTTYPE_CAST(e->type, getParameters(e));
        case OperatorExpression::OP_NEW:
          return replace_NEW(e->type, getParameters(e));
        case OperatorExpression::OP_EQ:
          if (e->arguments.at(0)->type->self<OptType>()) {
            return replace_EQ_OPTIONAL(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<ArrayType>()) {
            return replace_EQ_ARRAY(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<StructType>()) {
            return replace_EQ_STRUCT(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<TupleType>()) {
            return replace_EQ_TUPLE(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<UnionType>()) {
            return replace_EQ_UNION(e->type, getParameters(e));
          } else if (e->arguments.at(1)->type->self<NamedType>()
              || e->arguments.at(1)->type->self<NamedType>()) {
            return replace_EQ_NAMED(e->type, e->arguments);
          }
          break;
        case OperatorExpression::OP_LT:
          if (e->arguments.at(0)->type->self<OptType>()) {
            return replace_LT_OPTIONAL(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<ArrayType>()) {
            return replace_LT_ARRAY(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<StructType>()) {
            return replace_LT_STRUCT(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<TupleType>()) {
            return replace_LT_TUPLE(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<UnionType>()) {
            return replace_LT_UNION(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<NamedType>()
              || e->arguments.at(1)->type->self<NamedType>()) {
            return replace_LT_NAMED(e->type, e->arguments);
          }
          break;
        case OperatorExpression::OP_NEQ:
          return replace_NEQ(e->type, e->arguments);
        case OperatorExpression::OP_LTE:
          return replace_LTE(e->type, e->arguments);

        case OperatorExpression::OP_TO_STRING:
          if (e->arguments.at(0)->type->self<BitType>()) {
            return replace_STRINGIFY_BIT(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<BooleanType>()) {
            return replace_STRINGIFY_BOOLEAN(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<CharType>()) {
            return replace_STRINGIFY_CHAR(e->type, e->arguments);
          } else if (e->arguments.at(0)->type->self<StringType>()) {
            return replace_STRINGIFY_STRING(e->type, e->arguments);
          } else if (e->arguments.at(0)->type->self<OptType>()) {
            return replace_STRINGIFY_OPT(e->type, getParameters(e));
          }
          break;
        case OperatorExpression::OP_UNCHECKED_CAST:
          if (e->arguments.at(0)->type->isSameType(*e->type)) {
            return e->arguments.at(0);
          } else if (e->arguments.at(0)->type->self<ArrayType>()) {
            return replace_UNCHECKEDCAST_ARRAY(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<FunctionType>()) {
            return replace_UNCHECKEDCAST_FUNCTION(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<OptType>()) {
            return replace_UNCHECKEDCAST_OPT(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<StructType>()) {
            return replace_UNCHECKEDCAST_STRUCT(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<TupleType>()) {
            return replace_UNCHECKEDCAST_TUPLE(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<NamedType>()) {
            return replace_UNCHECKEDCAST_NAMEDTYPE(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<IntegerType>()
              && e->type->self<IntegerType>()) {
            return replace_UNCHECKEDCAST_INTEGER_TO_INTEGER(e->type, getParameters(e));
          }
          break;
        case OperatorExpression::OP_CHECKED_CAST:
          if (e->arguments.at(0)->type->isSameType(*e->type)) {
            return e->arguments.at(0);
          }
          if (e->arguments.at(0)->type->self<ArrayType>()) {
#if USE_LOOPS==1
            return replace_CHECKEDCAST_ARRAY(e->type, e->arguments);
#else
            return replace_CHECKEDCAST_ARRAY(e->type, getParameters(e));
#endif
          } else if (e->arguments.at(0)->type->self<FunctionType>()) {
            return replace_CHECKEDCAST_FUNCTION(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<OptType>()) {
            return replace_CHECKEDCAST_OPT(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<StructType>()) {
            return replace_CHECKEDCAST_STRUCT(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<TupleType>()) {
            return replace_CHECKEDCAST_TUPLE(e->type, getParameters(e));
          } else if (e->arguments.at(0)->type->self<NamedType>()) {
            return replace_CHECKEDCAST_NAMEDTYPE(e->type, getParameters(e));
          }
          break;
        case OperatorExpression::OP_CALL:
        case OperatorExpression::OP_NEG:
        case OperatorExpression::OP_ADD:
        case OperatorExpression::OP_SUB:
        case OperatorExpression::OP_MUL:
        case OperatorExpression::OP_DIV:
        case OperatorExpression::OP_MOD:
        case OperatorExpression::OP_REM:
        case OperatorExpression::OP_INDEX:
        case OperatorExpression::OP_SUBRANGE:
        case OperatorExpression::OP_NOT:
        case OperatorExpression::OP_AND:
        case OperatorExpression::OP_OR:
        case OperatorExpression::OP_XOR:
        case OperatorExpression::OP_GET:
        case OperatorExpression::OP_IS_LOGGABLE:
        case OperatorExpression::OP_IS_PRESENT:
        case OperatorExpression::OP_IS_INPUT_NEW:
        case OperatorExpression::OP_IS_INPUT_CLOSED:
        case OperatorExpression::OP_IS_OUTPUT_CLOSED:
        case OperatorExpression::OP_IS_PORT_READABLE:
        case OperatorExpression::OP_IS_PORT_WRITABLE:
        case OperatorExpression::OP_SIZE:
        case OperatorExpression::OP_BYTE_TO_UINT8:
        case OperatorExpression::OP_BYTE_TO_SINT8:
        case OperatorExpression::OP_STRING_TO_CHARS:
        case OperatorExpression::OP_BASETYPE_CAST:
        case OperatorExpression::OP_READ_PORT:
        case OperatorExpression::OP_BLOCK_READ:
        case OperatorExpression::OP_BLOCK_WRITE:
        case OperatorExpression::OP_CLEAR_PORT:
        case OperatorExpression::OP_CLOSE_PORT:
        case OperatorExpression::OP_WAIT_PORT:
        case OperatorExpression::OP_WRITE_PORT:
        case OperatorExpression::OP_LOG:
        case OperatorExpression::OP_SET:
        case OperatorExpression::OP_APPEND:
        case OperatorExpression::OP_BUILD:
          return nullptr;
        }
        return nullptr;
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_MERGE_TUPLES(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        StatementBuilder b;
        ::std::vector<Variable::VariablePtr> args;

        {
          auto arg = params.at(0)->variable;
          auto tup = arg->type->self<types::TupleType>();
          for (size_t i = 0; i < tup->types.size(); ++i) {
            auto v = b.declareLocalValue(ir::GetMember::create(tup->types.at(i), arg, i));
            args.push_back(v);
          }
        }
        {
          auto arg = params.at(1)->variable;
          auto tup = arg->type->self<types::TupleType>();
          for (size_t i = 0; i < tup->types.size(); ++i) {
            auto v = b.declareLocalValue(ir::GetMember::create(tup->types.at(i), arg, i));
            args.push_back(v);
          }
        }
        auto res = doOperator(b, retTy, OperatorExpression::OP_NEW, args);
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

#if USE_LOOPS == 0
      ir::Expression::ExpressionPtr RewriteBuiltins::replace_PARTITION_ARRAY(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;

        auto outTy = retTy->self<ArrayType>();
        auto init = createEmpty(b, toUnboundedArray(outTy));
        auto out = b.declareLocalVariable(init);

        auto arr = params.at(0)->variable;
        auto arrTy = arr->type->self<ArrayType>();
        if (arrTy->indexType.has_value() == false) {
          return ir::LiteralArray::create(outTy, { });
        }

        auto size = params.at(1)->variable;

        // loop over the source array
        auto zero = b.declareLocalValue(ir::LiteralInteger::create(arrTy->counterType, 0));
        auto index = b.declareLocalVariable(zero);
        auto len = doArrayLen(b, arr);
        auto loopName = newName("partitionArray");

        b.appendLoop(loopName, [&](StatementBuilder &body) {
          auto not_done = doOperator(body, OperatorExpression::OP_LT, {index,len});
          body.appendIf(not_done,[&](StatementBuilder& sb) {
                // determine the end of the subrange
            auto inc = doOperator(sb,OperatorExpression::OP_ADD, {index,size});
            auto endIndex = doCheckedCast(sb,index->type,inc);

            // auto subrange
            auto subrange = doArraySubrange(sb, outTy->element,arr,index, endIndex);
            auto tmp = doAppendElement(sb, out, subrange);
            auto casted_tmp = doCheckedCast(sb,out->type,tmp);
            sb.appendUpdate(out, casted_tmp);

            // update the index
            sb.appendUpdate(index,endIndex);

          },[&](StatementBuilder& sb) {
            sb.appendBreak(loopName);
          });
    });
        auto res = doCheckedCast(b, retTy, out);
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }
#endif

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_FLATTEN_OPTIONAL(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;

        auto nil = doOperator(b, retTy, OperatorExpression::OP_NEW, { });
        ::std::function<void(StatementBuilder&, Variable::VariablePtr)> deoptify;

        deoptify = [&](StatementBuilder &builder, Variable::VariablePtr opt) {
          auto optTy = opt->type->self<OptType>();
          if (optTy->element->self<OptType>()) {
            auto present = doIsPresent(builder, opt);
            builder.appendIf(present,[&](StatementBuilder& sb) {
                  auto next = doGetOpt(sb,opt);
                  deoptify(sb,next);
                },[&](StatementBuilder& sb) {
                  sb.appendReturn(nil);
                });
          }
          else {
            builder.appendReturn(opt);
          }
        };

        auto opt = params.at(0)->variable;
        deoptify(b, opt);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

#if USE_LOOPS==0
      ir::Expression::ExpressionPtr RewriteBuiltins::replace_ZIP_ARRAY(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto lhs = params.at(0)->variable;
        auto rhs = params.at(1)->variable;
        auto arrTy = retTy->self<ArrayType>();
        if (arrTy->indexType.has_value() == false) {
          return ir::LiteralArray::create(arrTy, { });
        }

        auto tupTy = arrTy->element->self<TupleType>();
        // setup the output
        auto init = createEmpty(b, toUnboundedArray(arrTy));
        auto out = b.declareLocalVariable(init);
        // init the counter
        auto zero = b.declareLocalValue(ir::LiteralInteger::create(arrTy->counterType, 0));
        auto index = b.declareLocalVariable(zero);
        // get the length of the input array
        auto len = doArrayLen(b, lhs);
        // do the loop
        auto loopName = newName("zipArray");
        b.appendLoop(loopName,
            [&](
                StatementBuilder &body) {
                  auto not_done = doOperator(body, OperatorExpression::OP_LT, {index,len});
                  body.appendIf(not_done,[&](StatementBuilder& notDone) {
                        auto lhs_val = doArrayIndex(notDone, lhs, index);
                        auto rhs_val = doArrayIndex(notDone, rhs, index);
                        auto lhs_casted = doCheckedCast(notDone,tupTy->types.at(0),lhs_val);
                        auto rhs_casted = doCheckedCast(notDone,tupTy->types.at(1),rhs_val);
                        auto tmp = doOperator(notDone, tupTy,OperatorExpression::OP_NEW, {lhs_casted,rhs_casted});
                        auto append = doAppendElement(notDone, out, tmp);
                        auto append_casted = doCheckedCast(notDone,out->type, append);
                        notDone.appendUpdate(out, append_casted);
                        incrementIndex(notDone,index);
                      },[&](StatementBuilder& loopDone) {
                        loopDone.appendBreak(loopName);
                      });
                });
        auto res = doCheckedCast(b, retTy, out);
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }
#endif

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_FIND_ARRAY(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arrTy = params.at(0)->variable->type->self<ArrayType>();
        auto optTy = retTy->self<OptType>();
        if (arrTy->indexType.has_value() == false) {
          return ir::LiteralOptional::create(optTy);
        }

        auto initIndex = doCheckedCast(b, arrTy->counterType, params.at(2)->variable);
        auto index = b.declareLocalVariable(initIndex);
        auto defaultResult = b.declareLocalValue(
            OperatorExpression::create(retTy, OperatorExpression::OP_NEW));
        auto result = b.declareLocalVariable(defaultResult);

        auto loopName = newName("findArray");
        auto loopOver = params.at(0)->variable;
        auto elemTy = loopOver->type->self<ArrayType>()->element;
        auto loopVar = newLocal("elem", elemTy);
        auto len = doArrayLen(b, params.at(0)->variable);

        auto predicateFN = params.at(1)->variable;
        b.appendLoop(loopName,
            [&](
                StatementBuilder &body) {
                  auto checkIndex = body.declareLocalValue(
                      ir::OperatorExpression::create(PrimitiveType::getBoolean(), OperatorExpression::OP_LT,
                          index, len));
                  body.appendIf(checkIndex,[&](StatementBuilder& inrange) {
                        auto elem = inrange.declareLocalValue(OperatorExpression::create(elemTy,OperatorExpression::OP_INDEX,params.at(0)->variable,index));
                        auto testPredicate = doCall(inrange, predicateFN, {elem});
                        inrange.appendIf(testPredicate,[&](StatementBuilder& found) {
                              auto casted = doCheckedCast(found,optTy->element,index);
                              auto optified = doOptify(found, casted);
                              found.appendUpdate(result, optified);
                              found.appendBreak(loopName);
                            },[&](StatementBuilder& inc) {
                              incrementIndex(inc, index);
                            });
                      },[&] (StatementBuilder& outofrange) {
                        outofrange.appendBreak(loopName);
                      });
                });
        b.appendReturn(result);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_FILTER_OPTIONAL(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arg = params.at(0)->variable;
        auto check = doIsPresent(b, arg);

        auto res = b.declareLocalVariable(retTy);
        b.appendIf(check, [&](StatementBuilder &present) {
          auto value = doGetOpt(present, arg);
          auto pred_check = doCall(present,params.at(1)->variable, {value});
          present.appendIf(pred_check,[&](StatementBuilder& pass) {
                pass.appendUpdate(res, arg);
              },[&](StatementBuilder& fail) {
                auto literal_nil = doOperator(fail, retTy, OperatorExpression::OP_NEW, {});
                fail.appendUpdate(res,literal_nil);
              });
        }, [&](StatementBuilder &not_present) {
          not_present.appendUpdate(res, arg);
        });
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_FOLD_ARRAY(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto accum = b.declareLocalVariable(params.at(2)->variable);
        auto loopName = newName("foldArray");
        auto loopOver = params.at(0)->variable;

        auto elemTy = loopOver->type->self<ArrayType>()->element;
        auto loopVar = newLocal("elem", elemTy);

        ir::StatementBuilder body;
        // do the map
        auto mapped = doCall(body, params.at(1)->variable, { loopVar, accum });
        auto casted = doCheckedCast(body, accum->type, mapped);
        body.appendUpdate(accum, casted);
        b.appendForEach(loopName, loopVar, loopOver, body.build());
        b.appendReturn(accum);

        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_BITS_TO_BYTES(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        // map(partition(bits),FROM_BITS)
        ir::StatementBuilder b;
        auto outTy = retTy->self<ArrayType>();

        auto partitionSz = b.declareLocalValue(LiteralInteger::create(8));
        auto parted = b.declareLocalValue(OperatorExpression::OP_PARTITION, {
            params.at(0)->variable, partitionSz });
        auto partedTy = parted->type->self<ArrayType>();

        auto out = doMapArray(b, parted, [&](StatementBuilder &sb, Variable::VariablePtr v) {
          return sb.declareLocalValue(outTy->element,OperatorExpression::OP_FROM_BITS, {v});
        });

        auto res = doCheckedCast(b, retTy, out);
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_BYTES_TO_BITS(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        // flatten(map(bytes,TO_BITS))
        auto array = params.at(0)->variable;
        auto arrTy = array->type->self<ArrayType>();

        ir::StatementBuilder b;
        auto mapped = doMapArray(b, array, [&](StatementBuilder &sb, Variable::VariablePtr v) {
          return sb.declareLocalValue(OperatorExpression::OP_TO_BITS, {v});
        });

        auto res = b.declareLocalValue(OperatorExpression::OP_FLATTEN, { mapped });
        auto castResult = doCheckedCast(b, retTy, res);
        b.appendReturn(castResult);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

#if USE_LOOPS == 1
      ir::Expression::ExpressionPtr RewriteBuiltins::replace_CHECKEDCAST_ARRAY(EType retTy,
          const Arguments &args)
      {

        //FIXME: we only should replace this if it's fusable;
        // otherwise, the code generator can probably optimze this way better!
        auto arrTy = retTy->self<ArrayType>();
        if (arrTy->indexType.has_value() == false) {
          return ir::LiteralArray::create(arrTy, { });
        }
        auto srcObj = args.at(0);
        auto srcTy = srcObj->type->self<ArrayType>();
        if (arrTy->element->isSameType(*srcTy->element)) {
          // we're getting infinite loops with checked casts;
          // each time we append to a buffer and then cast back!
          return nullptr;
        }

        auto step =
            IterateArrayStep::create(false, srcObj,
                [&](EVariable value,
                    StatementBuilder &sb) {
                      auto tmp = sb.declareLocalValue(OperatorExpression::checked_cast(arrTy->element,value));
                      sb.appendYield(YieldStatement::create(tmp,SkipStatement::create( {},Loop::CURRENT_STATE)));
                      return Loop::Expressions {};
                    });
        auto res = ToArray::create(arrTy, Loop::create(step));
        return res;
      }
#endif

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_CONCATENATE_ARRAY(EType retTy,
          const Arguments &args)
      {
        auto arrTy = retTy->self<ArrayType>();
        if (arrTy->indexType.has_value() == false) {
          return ir::LiteralArray::create(arrTy, { });
        }
        Loop::State s1 = Loop::newState();
        Loop::State s2 = Loop::newState();

        auto src1 = args.at(0);
        auto src2 = args.at(1);

        auto step1 = IterateArrayStep::create(false, src1, s2,
            [&](EVariable value, StatementBuilder &sb) {
              auto skip = SkipStatement::create( {},Loop::CURRENT_STATE);
              sb.appendYield(YieldStatement::create(value,skip));
              return Loop::Expressions {};
            });
        auto step2 = IterateArrayStep::create(false, src2,
            [&](EVariable value, StatementBuilder &sb) {
              auto skip = SkipStatement::create( {},Loop::CURRENT_STATE);
              sb.appendYield(YieldStatement::create(value,skip));
              return Loop::Expressions {};
            });
        auto res = ToArray::create(arrTy, Loop::create(s1, { { s1, step1 }, { s2, step2 } }));
        return res;
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_TRIM_ARRAY(EType retTy,
          const Arguments &args)
      {
        // trim is the same as taking from the front
        return replace_TAKE_ARRAY(retTy, args);
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_TAKE_ARRAY(EType retTy,
          const Arguments &args)
      {
        auto arrTy = retTy->self<ArrayType>();
        if (arrTy->indexType.has_value() == false) {
          return ir::LiteralArray::create(arrTy, { });
        }
        auto src1 = args.at(0);
        auto n = args.at(1);

        auto lenExpr = OperatorExpression::create(OperatorExpression::OP_SIZE, src1);
        auto zeroExpr = LiteralInteger::create(0);

        return LetExpression::create(lenExpr,
            [&](
                EVariable len) {
                  return LetExpression::create(zeroExpr,[&](EVariable z) {
                        auto from_i = makeMaxExpr(LiteralInteger::create(0),OperatorExpression::create(OperatorExpression::OP_ADD,len,n));
                        return makeConditionalExpr(isLTExpr(n, z),
                            // take from the back
                            LetExpression::create(from_i,[&](EVariable i) {
                                  return OperatorExpression::checked_cast(arrTy,OperatorExpression::create(OperatorExpression::OP_SUBRANGE,src1,i,len));
                                }),

                            // if n is non-negative, we take from the front, which is
                            LetExpression::create(makeMinExpr(n,lenExpr),[&](EVariable k) {
                                  return OperatorExpression::checked_cast(arrTy,OperatorExpression::create(OperatorExpression::OP_SUBRANGE,src1,z,k));
                                }));
                      });
                });
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_DROP_ARRAY(EType retTy,
          const Arguments &args)
      {
        auto arrTy = retTy->self<ArrayType>();
        if (arrTy->indexType.has_value() == false) {
          return ir::LiteralArray::create(arrTy, { });
        }
        auto src1 = args.at(0);
        auto n = args.at(1);

        auto lenExpr = OperatorExpression::create(OperatorExpression::OP_SIZE, src1);
        auto zeroExpr = LiteralInteger::create(0);

        return LetExpression::create(lenExpr,
            [&](
                EVariable len) {
                  return LetExpression::create(zeroExpr,[&](EVariable z) {
                        auto to_k = makeMaxExpr(z,OperatorExpression::create(OperatorExpression::OP_ADD,len,n));
                        return makeConditionalExpr(isLTExpr(n, z),
                            // take from the back
                            LetExpression::create(to_k,[&](EVariable k) {
                                  return OperatorExpression::checked_cast(arrTy,OperatorExpression::create(OperatorExpression::OP_SUBRANGE,src1,z,k));
                                }),

                            // if n is non-negative, we drop from the front, which is
                            LetExpression::create(makeMinExpr(n,lenExpr),[&](EVariable k) {
                                  return OperatorExpression::checked_cast(arrTy,OperatorExpression::create(OperatorExpression::OP_SUBRANGE,src1,k,len));
                                }));
                      });
                });

      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_PAD_ARRAY(EType retTy,
          const Arguments &args)
      {
        // FIXME: rewrite this as concatenate(src1,ToArray(n,init))
        auto arrTy = retTy->self<ArrayType>();
        if (arrTy->indexType.has_value() == false) {
          return ir::LiteralArray::create(arrTy, { });
        }
        Loop::State s1 = Loop::newState();
        Loop::State s2 = Loop::newState();

        auto src1 = args.at(0);
        auto sz = args.at(1);
        auto init = args.at(2);

        auto lenExpr = OperatorExpression::create(OperatorExpression::OP_SIZE, src1);
        auto lenVar = Variable::createLocal(lenExpr->type);
        auto nExpr = LetExpression::create(
            OperatorExpression::create(OperatorExpression::OP_SUB, sz, lenVar), [&](EVariable n) { //
                  return LetExpression::create(LiteralInteger::create(0), [&](EVariable z) { //
                        return makeMaxExpr(z, n);
                      });
                });
        auto nVar = Variable::createLocal(nExpr->type);

        auto step1 = IterateArrayStep::create(false, src1, s2,
            [&](EVariable value, StatementBuilder &sb) {
              auto skip = SkipStatement::create( {},Loop::CURRENT_STATE);
              sb.appendYield(YieldStatement::create(value,skip));
              return Loop::Expressions {};
            });
        auto step2 = SequenceStep::create(nVar, [&](EVariable, StatementBuilder &sb) {
          auto skip = SkipStatement::create( {},Loop::CURRENT_STATE);
          sb.appendYield(YieldStatement::create(init,skip));
          return Loop::Expressions {};
        });

        auto res = LetExpression::create(lenVar, lenExpr,
            LetExpression::create(nVar, nExpr, ToArray::create(arrTy, Loop::create(s1, {
                { s1, step1 }, { s2, step2 } }))));

        return res;
      }

#if USE_LOOPS == 1
      ir::Expression::ExpressionPtr RewriteBuiltins::replace_MAP_ARRAY(EType retTy,
          const Arguments &args)
      {
        auto arrTy = retTy->self<ArrayType>();
        if (arrTy->indexType.has_value() == false) {
          return ir::LiteralArray::create(arrTy, { });
        }

        auto mapObj = args.at(0);
        auto mapFn = args.at(1);

        auto step = IterateArrayStep::create(false, mapObj,
            [&](EVariable value, StatementBuilder &sb) {
              auto yieldValue = sb.declareLocalValue(OperatorExpression::OP_CALL, {mapFn,value});
              auto skip = SkipStatement::create( {},Loop::CURRENT_STATE);
              sb.appendYield(YieldStatement::create(yieldValue,skip));
              return Loop::Expressions {};
            });
        auto res = ToArray::create(arrTy, Loop::create(step));
        return res;
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_FILTER_ARRAY(EType retTy,
          const Arguments &args)
      {
        auto arrTy = retTy->self<ArrayType>();
        if (arrTy->indexType.has_value() == false) {
          return ir::LiteralArray::create(arrTy, { });
        }

        auto mapObj = args.at(0);
        auto filterFn = args.at(1);

        auto step = IterateArrayStep::create(false, mapObj,
            [&](EVariable value, StatementBuilder &sb) {
              auto cond = sb.declareLocalValue(OperatorExpression::OP_CALL, {filterFn,value});
              auto skip = SkipStatement::create( {},Loop::CURRENT_STATE);
              sb.appendIf(cond,YieldStatement::create(value,skip),skip);
              return Loop::Expressions {};
            });
        auto res = ToArray::create(arrTy, Loop::create(step));
        return res;
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_REVERSE_ARRAY(EType retTy,
          const Arguments &args)
      {
        auto arrTy = retTy->self<ArrayType>();
        if (arrTy->indexType.has_value() == false) {
          return ir::LiteralArray::create(arrTy, { });
        }

        auto mapObj = args.at(0);

        auto step = IterateArrayStep::createDefault(true, mapObj);
        auto res = ToArray::create(arrTy, Loop::create(step));
        return res;
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_FLATTEN_ARRAY(EType retTy,
          const Arguments &args)
      {
        auto arrTy = retTy->self<ArrayType>();
        if (arrTy->indexType.has_value() == false) {
          return ir::LiteralArray::create(arrTy, { });
        }

        auto mapObj = args.at(0);
        auto outerArrayTy = mapObj->type->self<ArrayType>();

        auto outerState = Loop::newState();
        auto innerState = Loop::newState();

        auto innerArray = Variable::createLocal(outerArrayTy->element);

        auto innerStep = IterateArrayStep::createDefault(false, innerArray, outerState);

        auto outerStep = IterateArrayStep::create(false, mapObj,
            [&](EVariable value, StatementBuilder &sb) {
              // FIXME: the ordering of the updates becomes important under some circumstances.
                Loop::Expressions updates;
                updates[innerArray] = value;
                // we need to reset the state for the second loop
                // FIXME: I don't think this is right anymore
                auto inits = innerStep->getInitializers();
                updates.insert(inits.begin(),inits.end());
                sb.appendSkip(SkipStatement::create(::std::move(updates),innerState,true));
                Loop::Expressions extraInits = { {innerArray, NoValue::create(innerArray->type)}};
                return extraInits;
              });
        Loop::States states = { { outerState, outerStep }, { innerState, innerStep } };
        auto res = ToArray::create(arrTy, Loop::create(outerState, states));
        return res;
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_ZIP_ARRAY(EType retTy,
          const Arguments &args)
      {
        auto arrTy = retTy->self<ArrayType>();
        if (arrTy->indexType.has_value() == false) {
          return ir::LiteralArray::create(arrTy, { });
        }
        auto elemTy = arrTy->element->self<TupleType>();
        auto arr1 = args.at(0);
        auto arr2 = args.at(1);
        auto arr1Ty = arr1->type->self<ArrayType>();
        auto arr2Ty = arr1->type->self<ArrayType>();

        auto s1 = Loop::newState();
        auto s2 = Loop::newState();

        auto elem1Ty = elemTy->types.at(0);
        auto elem2Ty = elemTy->types.at(1);

        auto arr1Elem = Variable::createLocal(elem1Ty);
        // take one item of the first array
        auto step1 = IterateArrayStep::create(false, arr1,
            [&](EVariable value, StatementBuilder &sb) {
              Loop::Expressions updates;
              updates[arr1Elem] = OperatorExpression::checked_cast(elem1Ty,value);
              auto skip = SkipStatement::create(::std::move(updates),s2);
              sb.appendSkip(skip);
              return Loop::Expressions { {arr1Elem, NoValue::create(arr1Ty->element)}};
            });
        auto step2 =
            IterateArrayStep::create(false, arr2,
                [&](EVariable value,
                    StatementBuilder &sb) {
                      auto skip = SkipStatement::create( {},s1);
                      auto arr2Elem = sb.declareLocalValue(OperatorExpression::checked_cast(elem2Ty,value));
                      auto zip = sb.declareLocalValue(elemTy,OperatorExpression::OP_NEW, {arr1Elem,arr2Elem});
                      sb.appendYield(YieldStatement::create(zip,skip));
                      return Loop::Expressions {};
                    });

        Loop::States states = { { s1, step1 }, { s2, step2 } };
        auto res = ToArray::create(arrTy, Loop::create(s1, states));
        return res;
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_PARTITION_ARRAY(EType retTy,
          const Arguments &args)
      {
        auto arrTy = retTy->self<ArrayType>();
        if (arrTy->indexType.has_value() == false) {
          return ir::LiteralArray::create(arrTy, { });
        }
        auto elemTy = arrTy->element->self<ArrayType>();

        auto mapObj = args.at(0);
        auto partSize = args.at(1);

        auto step =
            IterateArrayStep::create(false, mapObj,
                [&](EVariable value,
                    StatementBuilder &sb) {
                      auto bufferTy = BuilderType::get(elemTy);
                      auto buffer = Variable::createLocal(bufferTy);
                      auto remainingTy= elemTy->counterType;
                      auto remaining = Variable::createLocal(remainingTy);

                      Loop::Expressions extraInits;
                      extraInits[remaining] = OperatorExpression::checked_cast(remainingTy,partSize);
                      extraInits[buffer] = OperatorExpression::create(bufferTy,OperatorExpression::OP_NEW, {partSize});

                      auto zero = sb.declareLocalValue(LiteralInteger::create(remainingTy,0));
                      auto one = sb.declareLocalValue(LiteralInteger::create(1));
                      auto tmp = sb.declareLocalValue(OperatorExpression::OP_APPEND, {buffer,value});
                      auto tmpRemaining = sb.declareLocalValue(OperatorExpression::checked_cast(remainingTy,
                              OperatorExpression::create(OperatorExpression::OP_SUB,remaining,one)));
                      auto done = sb.declareLocalValue(OperatorExpression::OP_EQ, {tmpRemaining,zero});
                      sb.appendIf(done,[&](StatementBuilder& bb) {

                            EVariable yield = bb.declareLocalValue(OperatorExpression::OP_BUILD, {buffer});
                            // we need to reset the extra variables we're maintaining, so just use the initializer
                            bb.appendYield(YieldStatement::create(yield,
                                    SkipStatement::create(extraInits,Loop::CURRENT_STATE)));
                          },[&](StatementBuilder& bb) {
                            Loop::Expressions updates;
                            updates[remaining]=tmpRemaining;
                            bb.appendSkip(SkipStatement::create(::std::move(updates),Loop::CURRENT_STATE));
                          });
                      return extraInits;
                    });
        auto res = ToArray::create(arrTy, Loop::create(step));
        return res;
      }

#else
      ir::Expression::ExpressionPtr RewriteBuiltins::replace_MAP_ARRAY(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arrTy = retTy->self<ArrayType>();
        if (arrTy->indexType.has_value() == false) {
          return ir::LiteralArray::create(arrTy, { });
        }

        auto init = createEmpty(b, toUnboundedArray(arrTy));
        auto out = b.declareLocalVariable(init);
        auto loopName = newName("mapArray");
        auto loopOver = params.at(0)->variable;
        auto elemTy = loopOver->type->self<ArrayType>()->element;
        auto loopVar = newLocal("elem", elemTy);

        ir::StatementBuilder body;
        // do the map
        auto mapped = doCall(body, params.at(1)->variable, { loopVar });
        auto append = doAppendElement(body, out, mapped);
        auto casted = doCheckedCast(body, out->type, append);
        body.appendUpdate(out, casted);
        b.appendForEach(loopName, loopVar, loopOver, body.build());

        auto castResult = doCheckedCast(b, retTy, out);
        b.appendReturn(castResult);

        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_FILTER_ARRAY(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arrTy = retTy->self<ArrayType>();
        if (arrTy->indexType.has_value() == false) {
          return ir::LiteralArray::create(arrTy, { });
        }
        auto init = createEmpty(b, toUnboundedArray(arrTy));
        auto out = b.declareLocalVariable(init);
        auto loopName = newName("filterArray");
        auto loopOver = params.at(0)->variable;
        auto elemTy = loopOver->type->self<ArrayType>()->element;
        auto loopVar = newLocal("elem", elemTy);

        ir::StatementBuilder body;
        // do the check
        auto check = doCall(body, params.at(1)->variable, { loopVar });
        ir::StatementBuilder filterIn;
        {
          auto append = doAppendElement(filterIn, out, loopVar);
          auto casted = doCheckedCast(filterIn, out->type, append);
          filterIn.appendUpdate(out, casted);
        }

        body.appendIf(check, filterIn.build(), nullptr);
        b.appendForEach(loopName, loopVar, loopOver, body.build());

        auto castResult = doCheckedCast(b, retTy, out);
        b.appendReturn(castResult);

        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_REVERSE_ARRAY(EType retTy,
          const LambdaExpression::Parameters &params)
      {

        ir::StatementBuilder b;
        auto arrTy = retTy->self<ArrayType>();
        if (arrTy->indexType.has_value() == false) {
          return ir::LiteralArray::create(arrTy, { });
        }
        auto init = createEmpty(b, toUnboundedArray(arrTy));
        auto out = b.declareLocalVariable(init);
        auto loopName = newName("reverseArray");
        auto loopOver = params.at(0);
        auto loopVar = newLocal("elem", arrTy->element);

        ir::StatementBuilder body;
        // do the map
        auto reversed = doPrependElement(body, out, loopVar);
        auto casted = doCheckedCast(body, out->type, reversed);
        body.appendUpdate(out, casted);
        b.appendForEach(loopName, loopVar, loopOver->variable, body.build());

        auto castResult = doCheckedCast(b, arrTy, out);
        b.appendReturn(castResult);

        return ir::LambdaExpression::create(params, retTy, b.build());
      }
      ir::Expression::ExpressionPtr RewriteBuiltins::replace_FLATTEN_ARRAY(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arrTy = retTy->self<ArrayType>();
        if (arrTy->indexType.has_value() == false) {
          return ir::LiteralArray::create(arrTy, { });
        }
        auto init = createEmpty(b, toUnboundedArray(arrTy));
        auto out = b.declareLocalVariable(init);
        auto loopName = newName("flattenArray");
        auto loopOver = params.at(0)->variable;
        auto loopVar = newLocal("elem", loopOver->type->self<ArrayType>()->element);

        b.appendForEach(loopName, loopVar, loopOver, [&](StatementBuilder &body) {
          auto update = doOperator(body,OperatorExpression::OP_CONCATENATE, {out, loopVar});
          auto casted = doCheckedCast(body, out->type, update);
          body.appendUpdate(out, casted);
        });

        auto castResult = doCheckedCast(b, arrTy, out);
        b.appendReturn(castResult);

        return ir::LambdaExpression::create(params, retTy, b.build());
      }
#endif
      ir::Expression::ExpressionPtr RewriteBuiltins::replace_MAP_OPTIONAL(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        auto optTy = retTy->self<OptType>();

        ir::StatementBuilder b;
        auto isPresent = doIsPresent(b, params.at(0)->variable);
        auto res = b.declareLocalValue(optTy);
        b.appendIf(isPresent, [&](ir::StatementBuilder &ifPresent) {
          auto value = doGetOpt(ifPresent, params.at(0)->variable);
          auto mapped = doCall(ifPresent, params.at(1)->variable, {value});
          auto casted = doCheckedCast(ifPresent, optTy->element, mapped);
          auto mappedResult = doOptify(ifPresent, casted);
          ifPresent.appendUpdate(res, mappedResult);
        }, [&](ir::StatementBuilder &ifNotPresent) {
          auto value = doOperator(ifNotPresent, optTy, OperatorExpression::OP_NEW, {});
          ifNotPresent.appendUpdate(res, value);
        });
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_HEAD_ARRAY(EType retTy,
          const Arguments &args)
      {
        auto arr = args.at(0);
        return doLet(ir::LiteralInteger::create(0), [&](Variable::VariablePtr zero) {
          return ir::OperatorExpression::create(retTy,OperatorExpression::OP_INDEX, {arr,zero});
        });
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_TAIL_ARRAY(EType retTy,
          const Arguments &args)
      {
        auto arr = args.at(0);
        return doLet(ir::LiteralInteger::create(1),
            [&](
                Variable::VariablePtr one) {
                  return doLet(OperatorExpression::OP_SIZE, {arr},[&](Variable::VariablePtr len) {
                        return doLet(OperatorExpression::OP_SUBRANGE, {arr,one,len},[&](Variable::VariablePtr subrange) {
                              return OperatorExpression::create(retTy,OperatorExpression::OP_CHECKED_CAST,subrange);
                            });
                      });
                });
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_HEAD_TUPLE(EType retTy,
          const Arguments &args)
      {
        return GetMember::create(retTy, args.at(0), 0);
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_TAIL_TUPLE(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto tupTy = retTy->self<TupleType>();
        auto tup = params.at(0)->variable;
        ::std::vector<Variable::VariablePtr> args;
        for (size_t i = 0; i < tupTy->types.size(); ++i) {
          args.push_back(b.declareLocalValue(GetMember::create(tupTy->types.at(i), tup, 1 + i)));
        }
        auto res = b.declareLocalValue(
            OperatorExpression::create(retTy, OperatorExpression::OP_NEW, args));
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_WRAP_INTEGER(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto iTy = retTy->self<IntegerType>();
        auto arg = params.at(0)->variable;
        auto argTy = arg->type->self<IntegerType>();

        auto one = b.declareLocalValue(LiteralInteger::create(1));
        auto range = b.declareLocalValue(LiteralInteger::create(*iTy->range.size()));
        auto upper_limit = b.declareLocalValue(LiteralInteger::create(*iTy->range.max()));
        auto lower_limit = b.declareLocalValue(LiteralInteger::create(*iTy->range.min()));
        auto is_below = doOperator(b, OperatorExpression::OP_LT, { arg, lower_limit });

        b.appendIf(is_below, [&](StatementBuilder &wrap) {
          auto diff = doOperator(wrap, OperatorExpression::OP_SUB, {lower_limit,arg});

          // based on types alone we can determine if this operation is possible
            if (OperatorExpression::typeFor( OperatorExpression::OP_MOD, {diff,range})) {
              auto mod = doOperator(wrap, OperatorExpression::OP_MOD, {diff,range});
              auto mod_minus_1 = doOperator(wrap, OperatorExpression::OP_SUB, {mod,one});
              auto res = doOperator(wrap, OperatorExpression::OP_SUB, {upper_limit,mod_minus_1});
              auto casted = doCheckedCast(wrap,iTy,res);
              wrap.appendReturn(casted);
            }
          });

        auto is_above = doOperator(b, OperatorExpression::OP_LT, { upper_limit, arg });
        b.appendIf(is_above, [&](StatementBuilder &wrap) {
          auto diff = doOperator(wrap, OperatorExpression::OP_SUB, {arg,upper_limit});
          // based on types alone we can determine if this operation is possible
            if (OperatorExpression::typeFor( OperatorExpression::OP_MOD, {diff,range})) {
              auto mod = doOperator(wrap, OperatorExpression::OP_MOD, {diff,range});
              auto mod_minus_1 = doOperator(wrap, OperatorExpression::OP_SUB, {mod,one});
              auto res = doOperator(wrap, OperatorExpression::OP_ADD, {lower_limit,mod_minus_1});
              auto casted = doCheckedCast(wrap,iTy,res);
              wrap.appendReturn(casted);
            }
          });

        auto casted = doCheckedCast(b, iTy, arg);
        b.appendReturn(casted);

        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_CLAMP_INTEGER(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto iTy = retTy->self<IntegerType>();
        auto arg = params.at(0)->variable;
        auto argTy = arg->type->self<IntegerType>();

        if (iTy->range.min().isFinite()) {
          auto lower_limit = b.declareLocalValue(LiteralInteger::create(iTy, *iTy->range.min()));
          auto is_below = doOperator(b, OperatorExpression::OP_LT, { arg, lower_limit });
          b.appendIf(is_below, [&](StatementBuilder &clamp) {
            clamp.appendReturn(lower_limit);
          });
        }

        if (iTy->range.max().isFinite()) {
          auto upper_limit = b.declareLocalValue(LiteralInteger::create(iTy, *iTy->range.max()));
          auto is_above = doOperator(b, OperatorExpression::OP_LT, { upper_limit, arg });
          b.appendIf(is_above, [&](StatementBuilder &clamp) {
            clamp.appendReturn(upper_limit);
          });
        }
        auto casted = doCheckedCast(b, iTy, arg);
        b.appendReturn(casted);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_STRUCT_TO_TUPLE(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        ::std::vector<Variable::VariablePtr> args;
        auto sval = params.at(0)->variable;
        auto sty = sval->type->self<StructType>();
        for (size_t i = 0; i < sty->members.size(); ++i) {
          auto &m = sty->members.at(i);
          auto v = b.declareLocalValue(ir::GetMember::create(m.type, sval, m.name));
          args.push_back(v);
        }
        auto res = doOperator(b, retTy, OperatorExpression::OP_NEW, args);
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_TUPLE_TO_STRUCT(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        ::std::vector<Variable::VariablePtr> args;
        auto tval = params.at(0)->variable;
        auto tty = tval->type->self<TupleType>();
        for (size_t i = 0; i < tty->types.size(); ++i) {
          auto v = b.declareLocalValue(ir::GetMember::create(tty->types.at(i), tval, i));
          args.push_back(v);
        }
        auto res = doOperator(b, retTy, OperatorExpression::OP_NEW, args);
        b.appendReturn(res);

        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_INTERPOLATE_TEXT(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        if (params.empty()) {
          return LiteralString::createEmpty();
        }
        ir::StatementBuilder b;
        auto res = b.declareLocalVariable(params.at(0)->variable);
        for (size_t i = 1; i < params.size(); ++i) {
          auto tmp = b.declareLocalValue(doOperator(b, OperatorExpression::OP_CONCATENATE, { res,
              params.at(i)->variable }));
          b.appendUpdate(res, tmp);
        }
        b.appendReturn(res);

        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_ENCODE_BIT(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto res = b.declareLocalVariable(retTy);
        auto bit = params.at(0)->variable;
        auto on = b.declareLocalValue(LiteralBit::create(true));
        auto check = doOperator(b, OperatorExpression::OP_EQ, { bit, on });
        b.appendIf(params.at(0)->variable, [&](StatementBuilder &sb) {
          auto t = sb.declareLocalValue(LiteralByte::create(0xff));
          auto singleton = createSingleton(sb, t);
          auto casted = doCheckedCast(sb,retTy,singleton);
          sb.appendUpdate(res,casted);
        }, [&](StatementBuilder &sb) {
          auto t = sb.declareLocalValue(LiteralByte::create(0x00));
          auto singleton = createSingleton(sb, t);
          auto casted = doCheckedCast(sb,retTy,singleton);
          sb.appendUpdate(res,casted);
        });
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_DECODE_BIT(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto res = b.declareLocalVariable(retTy);
        auto arr = params.at(0)->variable;
        auto arrTy = arr->type->self<ArrayType>();
        auto zeroIndex = b.declareLocalValue(LiteralInteger::create(arrTy->indexType.value(), 0));
        auto zero = b.declareLocalValue(LiteralByte::create(0));
        auto enc = doArrayIndex(b, arr, zeroIndex);
        auto is_zero = doOperator(b, OperatorExpression::OP_EQ, { zero, enc });
        b.appendIf(is_zero, [&](StatementBuilder &sb) {
          auto off = sb.declareLocalValue(LiteralBit::create(false));
          auto casted = doCheckedCast(b, retTy, off);
          sb.appendUpdate(res, casted);
        }, [&](StatementBuilder &sb) {
          auto on = sb.declareLocalValue(LiteralBit::create(true));
          auto casted = doCheckedCast(b, retTy, on);
          sb.appendUpdate(res, casted);
        });
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_ENCODE_BOOLEAN(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto res = b.declareLocalVariable(retTy);
        b.appendIf(params.at(0)->variable, [&](StatementBuilder &sb) {
          auto t = sb.declareLocalValue(LiteralByte::create(0xff));
          auto singleton = createSingleton(sb, t);
          auto casted = doCheckedCast(sb,retTy,singleton);
          sb.appendUpdate(res,casted);
        }, [&](StatementBuilder &sb) {
          auto t = sb.declareLocalValue(LiteralByte::create(0x00));
          auto singleton = createSingleton(sb, t);
          auto casted = doCheckedCast(sb,retTy,singleton);
          sb.appendUpdate(res,casted);
        });
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_DECODE_BOOLEAN(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arr = params.at(0)->variable;
        auto arrTy = arr->type->self<ArrayType>();
        auto zeroIndex = b.declareLocalValue(LiteralInteger::create(arrTy->indexType.value(), 0));
        auto zero = b.declareLocalValue(LiteralByte::create(0));

        auto enc = doArrayIndex(b, arr, zeroIndex);
        auto not_zero = doOperator(b, OperatorExpression::OP_NEQ, { zero, enc });
        auto casted = doCheckedCast(b, retTy, not_zero);
        b.appendReturn(casted);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_ENCODE_BYTE(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto res = createSingleton(b, params.at(0)->variable);
        auto casted = doCheckedCast(b, retTy, res);
        b.appendReturn(casted);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_DECODE_BYTE(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arr = params.at(0)->variable;
        auto arrTy = arr->type->self<ArrayType>();
        auto zeroIndex = b.declareLocalValue(LiteralInteger::create(arrTy->indexType.value(), 0));

        auto enc = doArrayIndex(b, arr, zeroIndex);
        auto casted = doCheckedCast(b, retTy, enc);
        b.appendReturn(casted);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_BIT_TO_BITS(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto res = createSingleton(b, params.at(0)->variable);
        auto casted = doCheckedCast(b, retTy, res);
        b.appendReturn(casted);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_BITS_TO_BIT(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arr = params.at(0)->variable;
        auto arrTy = arr->type->self<ArrayType>();
        auto zeroIndex = b.declareLocalValue(LiteralInteger::create(arrTy->indexType.value(), 0));

        auto enc = doArrayIndex(b, arr, zeroIndex);
        auto casted = doCheckedCast(b, retTy, enc);
        b.appendReturn(casted);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_BOOLEAN_TO_BITS(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto out = b.declareLocalVariable(BitType::create());
        b.appendIf(params.at(0)->variable, [&](StatementBuilder &sb) {
          auto bit = sb.declareLocalValue(LiteralBit::create(true));
          sb.appendUpdate(out,bit);
        }, [&](StatementBuilder &sb) {
          auto bit = sb.declareLocalValue(LiteralBit::create(false));
          sb.appendUpdate(out,bit);
        });
        auto res = createSingleton(b, out);
        auto casted = doCheckedCast(b, retTy, res);
        b.appendReturn(casted);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_BITS_TO_BOOLEAN(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arr = params.at(0)->variable;
        auto arrTy = arr->type->self<ArrayType>();
        auto zeroIndex = b.declareLocalValue(LiteralInteger::create(arrTy->indexType.value(), 0));

        auto on = b.declareLocalValue(LiteralBit::create(true));
        auto enc = doArrayIndex(b, arr, zeroIndex);
        auto res = doOperator(b, OperatorExpression::OP_EQ, { on, enc });
        auto casted = doCheckedCast(b, retTy, res);
        b.appendReturn(casted);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_BYTE_TO_BITS(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;

        ::std::vector<Variable::VariablePtr> bits;
        auto val = params.at(0)->variable;
        auto zero = b.declareLocalValue(LiteralByte::create(0));
        auto on = b.declareLocalValue(LiteralBit::create(true));
        auto off = b.declareLocalValue(LiteralBit::create(false));

        for (size_t i = 0; i < 8; ++i) {
          auto mask = b.declareLocalValue(LiteralByte::create(1 << i));
          auto masked = doOperator(b, OperatorExpression::OP_AND, { val, mask });
          auto is_not_set = doOperator(b, OperatorExpression::OP_EQ, { masked, zero });
          auto bit = b.declareLocalValue(on->type);
          b.appendIf(is_not_set, [&](StatementBuilder &sb) {
            sb.appendUpdate(bit,off);
          }, [&](StatementBuilder &sb) {
            sb.appendUpdate(bit,on);
          });
          bits.push_back(bit);
        }

        auto res = doOperator(b, retTy, OperatorExpression::OP_NEW, bits);
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_BITS_TO_BYTE(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;

        auto arr = params.at(0)->variable;
        auto arrTy = arr->type->self<ArrayType>();

        auto zero = b.declareLocalValue(LiteralByte::create(0));
        auto res = b.declareLocalVariable(zero);
        auto on = b.declareLocalValue(LiteralBit::create(true));

        for (size_t i = 0; i < 8; ++i) {
          auto index = b.declareLocalValue(LiteralInteger::create(arrTy->indexType.value(), i));
          auto bit = doArrayIndex(b, arr, index);
          auto is_set = doOperator(b, OperatorExpression::OP_EQ, { on, bit });
          b.appendIf(is_set, [&](StatementBuilder &sb) {
            auto mask = sb.declareLocalValue(LiteralByte::create(1<<i));
            auto tmp = doOperator(sb,OperatorExpression::OP_OR, {res,mask});
            auto casted = doCheckedCast(sb,res->type,tmp);
            sb.appendUpdate(res,casted);
          });
        }

        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_CHAR_TO_BITS(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;

        auto bytes = doOperator(b, OperatorExpression::OP_TO_BYTES, { params.at(0)->variable });
        auto bits = doOperator(b, OperatorExpression::OP_BYTES_TO_BITS, { bytes });
        auto casted = doCheckedCast(b, retTy, bits);
        b.appendReturn(casted);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_BITS_TO_CHAR(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto bytes = doOperator(b, OperatorExpression::OP_BITS_TO_BYTES,
            { params.at(0)->variable });
        auto res = doOperator(b, retTy, OperatorExpression::OP_FROM_BYTES, { bytes });
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_REAL_TO_BITS(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;

        auto bytes = doOperator(b, OperatorExpression::OP_TO_BYTES, { params.at(0)->variable });
        auto bits = doOperator(b, OperatorExpression::OP_BYTES_TO_BITS, { bytes });
        auto casted = doCheckedCast(b, retTy, bits);
        b.appendReturn(casted);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_BITS_TO_REAL(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto bytes = doOperator(b, OperatorExpression::OP_BITS_TO_BYTES,
            { params.at(0)->variable });
        auto res = doOperator(b, retTy, OperatorExpression::OP_FROM_BYTES, { bytes });
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_ROOTTYPE_CAST(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arg = params.at(0)->variable;
        while (arg->type->self<types::NamedType>()) {
          arg = doOperator(b, OperatorExpression::OP_BASETYPE_CAST, { arg });
        }
        b.appendReturn(arg);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_NEW(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        if (params.size() != 1) {
          return nullptr;
        }

        ir::StatementBuilder b;
        auto arg = params.at(0)->variable;
        auto argTy = arg->type;

        Variable::VariablePtr res;
        if (argTy->isSameType(*retTy)) {
          res = arg;
        } else if (retTy->self<types::BitType>() && argTy->self<types::IntegerType>()) {
          res = b.declareLocalVariable(retTy);
          auto zero = b.declareLocalValue(ir::LiteralInteger::create(0));
          auto is_zero = doOperator(b, OperatorExpression::OP_EQ, { zero, arg });
          b.appendIf(is_zero, [&](StatementBuilder &sb) {
            auto bit = b.declareLocalValue(ir::LiteralBit::create(true));
            sb.appendUpdate(res,bit);
          }, [&](StatementBuilder &sb) {
            auto bit = b.declareLocalValue(ir::LiteralBit::create(false));
            sb.appendUpdate(res,bit);
          });
        }
        if (res == nullptr) {
          return nullptr;
        }
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_EQ_NAMED(EType retTy,
          const Arguments &args)
      {
        assert(retTy->self<BooleanType>());
        return doLet(OperatorExpression::OP_ROOTTYPE_CAST, { args.at(0) },
            [&](
                Variable::VariablePtr lhs) {
                  return doLet(OperatorExpression::OP_ROOTTYPE_CAST, {args.at(1)}, [&](Variable::VariablePtr rhs) {
                        return OperatorExpression::create(OperatorExpression::OP_EQ, {lhs,rhs});
                      });
                });
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_LT_NAMED(EType retTy,
          const Arguments &args)
      {
        assert(retTy->self<BooleanType>());
        return doLet(OperatorExpression::OP_ROOTTYPE_CAST, { args.at(0) },
            [&](
                Variable::VariablePtr lhs) {
                  return doLet(OperatorExpression::OP_ROOTTYPE_CAST, {args.at(1)}, [&](Variable::VariablePtr rhs) {
                        return OperatorExpression::create(OperatorExpression::OP_LT, {lhs,rhs});
                      });
                });
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_NEQ(EType retTy, const Arguments &args)
      {
        assert(retTy->self<BooleanType>());
        return doLet(OperatorExpression::OP_EQ, { args.at(0), args.at(1) },
            [&](Variable::VariablePtr v) {
              return OperatorExpression::create(OperatorExpression::OP_NOT,v);
            });
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_LTE(EType retTy, const Arguments &args)
      {
        assert(retTy->self<BooleanType>());
        return doLet(OperatorExpression::OP_LT, { args.at(1), args.at(0) },
            [&](Variable::VariablePtr v) {
              return OperatorExpression::create(OperatorExpression::OP_NOT,v);
            });
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_LT_OPTIONAL(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arg0 = params.at(0)->variable;
        auto arg1 = params.at(1)->variable;

        auto present0 = doIsPresent(b, arg0);
        auto present1 = doIsPresent(b, arg1);
        auto lt = doOperator(b, OperatorExpression::OP_LT, { present0, present1 });
        b.appendIf(lt, [&](StatementBuilder &sb) {
          sb.appendReturn(lt);
        });
        auto eq = doOperator(b, OperatorExpression::OP_EQ, { present0, present1 });
        b.appendIf(eq, [&](StatementBuilder &sb) {
          sb.appendIf(present0,[&](StatementBuilder& eqb) {
                auto get0 = doGetOpt(eqb,arg0);
                auto get1 = doGetOpt(eqb,arg1);
                auto are_equal = doOperator(eqb, OperatorExpression::OP_LT, {get0,get1});
                eqb.appendReturn(are_equal);
              });
        });
        b.appendReturn(eq);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_EQ_OPTIONAL(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arg0 = params.at(0)->variable;
        auto arg1 = params.at(1)->variable;

        auto present0 = doIsPresent(b, arg0);
        auto present1 = doIsPresent(b, arg1);
        auto same_presence = doOperator(b, OperatorExpression::OP_EQ, { present0, present1 });
        auto res = b.declareLocalVariable(same_presence);
        b.appendIf(same_presence, [&](StatementBuilder &sb) {
          sb.appendIf(present0,[&](StatementBuilder& eqb) {
                auto get0 = doGetOpt(eqb,arg0);
                auto get1 = doGetOpt(eqb,arg1);
                auto are_equal = doOperator(eqb, OperatorExpression::OP_EQ, {get0,get1});
                eqb.appendUpdate(res,are_equal);
              });
        });
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_LT_ARRAY(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arg0 = params.at(0)->variable;
        auto arg1 = params.at(1)->variable;

        auto len0 = doArrayLen(b, arg0);
        auto len1 = doArrayLen(b, arg1);
        auto lt = doOperator(b, OperatorExpression::OP_LT, { len0, len1 });
        b.appendIf(lt, [&](StatementBuilder &sb) {
          sb.appendReturn(lt);
        });

        auto eq = doOperator(b, OperatorExpression::OP_EQ, { len0, len1 });
        auto res = b.declareLocalVariable(eq);
        b.appendIf(eq, [&](StatementBuilder &sb) {

          auto arrTy= arg0->type->self<ArrayType>();
          auto zero = sb.declareLocalValue(ir::LiteralInteger::create(arrTy->counterType, 0));
          auto index = sb.declareLocalVariable(zero);
          auto loopName = newName("lessThanArray");

          sb.appendLoop(loopName, [&](StatementBuilder& body) {
                auto is_done = doOperator(body, OperatorExpression::OP_EQ, {index,len0});
                body.appendIf(is_done,[&](StatementBuilder& brk) {
                      brk.appendBreak(loopName);
                    });
                auto elem0 = doArrayIndex(body, arg0, index);
                auto elem1 = doArrayIndex(body, arg1, index);

                lt = doOperator(body, OperatorExpression::OP_LT, {elem0,elem1});
                body.appendIf(lt,[&](StatementBuilder& brk) {
                      brk.appendUpdate(res,lt);
                      brk.appendBreak(loopName);
                    });

                auto are_equal = doOperator(body, OperatorExpression::OP_EQ, {elem0,elem1});
                body.appendIf(are_equal,[&](StatementBuilder& cont) {
                      incrementIndex(cont, index);
                    },[&](StatementBuilder& brk) {
                      brk.appendUpdate(res,are_equal);
                      brk.appendBreak(loopName);
                    });
              });

        });
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_EQ_ARRAY(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arg0 = params.at(0)->variable;
        auto arg1 = params.at(1)->variable;

        auto len0 = doArrayLen(b, arg0);
        auto len1 = doArrayLen(b, arg1);
        auto same_length = doOperator(b, OperatorExpression::OP_EQ, { len0, len1 });
        auto res = b.declareLocalVariable(same_length);
        b.appendIf(same_length, [&](StatementBuilder &sb) {

          auto arrTy= arg0->type->self<ArrayType>();
          auto zero = sb.declareLocalValue(ir::LiteralInteger::create(arrTy->counterType, 0));
          auto index = sb.declareLocalVariable(zero);
          auto loopName = newName("equalsArray");

          sb.appendLoop(loopName, [&](StatementBuilder& body) {
                auto is_done = doOperator(body, OperatorExpression::OP_EQ, {index,len0});
                body.appendIf(is_done,[&](StatementBuilder& brk) {
                      brk.appendBreak(loopName);
                    });
                auto elem0 = doArrayIndex(body, arg0, index);
                auto elem1 = doArrayIndex(body, arg1, index);

                auto are_equal = doOperator(body, OperatorExpression::OP_EQ, {elem0,elem1});
                body.appendIf(are_equal,[&](StatementBuilder& cont) {
                      incrementIndex(cont, index);
                    },[&](StatementBuilder& brk) {
                      brk.appendUpdate(res,are_equal);
                      brk.appendBreak(loopName);
                    });
              });

        });
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_LT_STRUCT(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arg0 = params.at(0)->variable;
        auto arg1 = params.at(1)->variable;

        auto arg0Ty = arg0->type->self<StructType>();
        auto arg1Ty = arg1->type->self<StructType>();

        if (!arg0Ty->isSameType(*arg1Ty)) {
          throw ::std::runtime_error("Trying to compare different kinds of structs");
        }

        for (auto m : arg0Ty->members) {
          auto e0 = b.declareLocalValue(ir::GetMember::create(m.type, arg0, m.name));
          auto e1 = b.declareLocalValue(ir::GetMember::create(m.type, arg1, m.name));
          auto lt = doOperator(b, OperatorExpression::OP_LT, { e0, e1 });
          b.appendIf(lt, [&](StatementBuilder &sb) {
            sb.appendReturn(lt); // EQ will be false here!
            });
          auto eq = doOperator(b, OperatorExpression::OP_EQ, { e0, e1 });
          auto neq = doOperator(b, OperatorExpression::OP_NOT, { eq });
          b.appendIf(neq, [&](StatementBuilder &sb) {
            sb.appendReturn(eq); // EQ will be false here!
            });
        }
        auto t = b.declareLocalValue(LiteralBoolean::create(false));
        b.appendReturn(t);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_EQ_STRUCT(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arg0 = params.at(0)->variable;
        auto arg1 = params.at(1)->variable;

        auto arg0Ty = arg0->type->self<StructType>();
        auto arg1Ty = arg1->type->self<StructType>();

        if (!arg0Ty->isSameType(*arg1Ty)) {
          throw ::std::runtime_error("Trying to compare different kinds of structs");
        }

        for (auto m : arg0Ty->members) {
          auto e0 = b.declareLocalValue(ir::GetMember::create(m.type, arg0, m.name));
          auto e1 = b.declareLocalValue(ir::GetMember::create(m.type, arg1, m.name));
          auto eq = doOperator(b, OperatorExpression::OP_EQ, { e0, e1 });
          auto neq = doOperator(b, OperatorExpression::OP_NOT, { eq });
          b.appendIf(neq, [&](StatementBuilder &sb) {
            sb.appendReturn(eq); // EQ will be false here!
            });
        }
        auto t = b.declareLocalValue(LiteralBoolean::create(true));
        b.appendReturn(t);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_LT_TUPLE(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arg0 = params.at(0)->variable;
        auto arg1 = params.at(1)->variable;

        auto arg0Ty = arg0->type->self<TupleType>();
        auto arg1Ty = arg1->type->self<TupleType>();
        if (!arg0Ty->isSameType(*arg1Ty)) {
          throw ::std::runtime_error("Trying to compare different kinds of tuples");
        }

        for (size_t i = 0; i < arg0Ty->types.size(); ++i) {
          auto e0 = b.declareLocalValue(ir::GetMember::create(arg0Ty->types.at(i), arg0, i));
          auto e1 = b.declareLocalValue(ir::GetMember::create(arg1Ty->types.at(i), arg1, i));
          auto lt = doOperator(b, OperatorExpression::OP_LT, { e0, e1 });
          b.appendIf(lt, [&](StatementBuilder &sb) {
            sb.appendReturn(lt);
          });
          auto eq = doOperator(b, OperatorExpression::OP_EQ, { e0, e1 });
          auto neq = doOperator(b, OperatorExpression::OP_NOT, { eq });
          b.appendIf(neq, [&](StatementBuilder &sb) {
            sb.appendReturn(eq); // EQ will be false here!
            });
        }
        auto t = b.declareLocalValue(LiteralBoolean::create(false));
        b.appendReturn(t);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_EQ_TUPLE(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arg0 = params.at(0)->variable;
        auto arg1 = params.at(1)->variable;

        auto arg0Ty = arg0->type->self<TupleType>();
        auto arg1Ty = arg1->type->self<TupleType>();
        if (!arg0Ty->isSameType(*arg1Ty)) {
          throw ::std::runtime_error("Trying to compare different kinds of tuples");
        }

        for (size_t i = 0; i < arg0Ty->types.size(); ++i) {
          auto e0 = b.declareLocalValue(ir::GetMember::create(arg0Ty->types.at(i), arg0, i));
          auto e1 = b.declareLocalValue(ir::GetMember::create(arg1Ty->types.at(i), arg1, i));
          auto eq = doOperator(b, OperatorExpression::OP_EQ, { e0, e1 });
          auto neq = doOperator(b, OperatorExpression::OP_NOT, { eq });
          b.appendIf(neq, [&](StatementBuilder &sb) {
            sb.appendReturn(eq); // EQ will be false here!
            });
        }
        auto t = b.declareLocalValue(LiteralBoolean::create(true));
        b.appendReturn(t);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_LT_UNION(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arg0 = params.at(0)->variable;
        auto arg1 = params.at(1)->variable;

        auto arg0Ty = arg0->type->self<UnionType>();
        auto arg1Ty = arg1->type->self<UnionType>();

        if (!arg0Ty->isSameType(*arg1Ty)) {
          throw ::std::runtime_error("Trying to compare different kinds of unions");
        }

        auto d = arg0Ty->discriminant;
        // TODO: introduce a discriminant index function so we don't have to compare in
        // discriminants by value
        auto d0 = b.declareLocalValue(ir::GetDiscriminant::create(d.type, arg0));
        auto d1 = b.declareLocalValue(ir::GetDiscriminant::create(d.type, arg1));
        auto lt = doOperator(b, OperatorExpression::OP_LT, { d0, d1 });
        b.appendIf(lt, [&](StatementBuilder &sb) {
          sb.appendReturn(lt); // EQ will be false here!
          });
        auto eq = doOperator(b, OperatorExpression::OP_EQ, { d0, d1 });
        auto neq = doOperator(b, OperatorExpression::OP_NOT, { eq });
        b.appendIf(neq, [&](StatementBuilder &sb) {
          sb.appendReturn(eq); // EQ will be false here!
          });

        // check all but the first
        for (size_t i = 1; i < arg0Ty->members.size(); ++i) {
          auto m = arg0Ty->members.at(i);
          auto dm = b.declareLocalValue(m.discriminant);
          eq = doOperator(b, OperatorExpression::OP_EQ, { d0, dm });
          b.appendIf(eq, [&](StatementBuilder &sb) {
            // TODO: since we're safely accessing the current member, we don't need to
            // get an optional!!
              auto e0 = b.declareLocalValue(ir::GetMember::create(m.type,arg0,m.name));
              auto e1 = b.declareLocalValue(ir::GetMember::create(m.type,arg1,m.name));
              lt = doOperator(b, OperatorExpression::OP_LT, {e0,e1});
              sb.appendReturn(lt); // EQ will be false here!
            });
        }
        // now, we no that the first value in the union is set
        auto m = arg0Ty->members.at(0);
        auto e0 = b.declareLocalValue(ir::GetMember::create(m.type, arg0, m.name));
        auto e1 = b.declareLocalValue(ir::GetMember::create(m.type, arg1, m.name));
        lt = doOperator(b, OperatorExpression::OP_LT, { e0, e1 });
        b.appendReturn(lt);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_EQ_UNION(EType retTy,
          const LambdaExpression::Parameters &params)
      {
        ir::StatementBuilder b;
        auto arg0 = params.at(0)->variable;
        auto arg1 = params.at(1)->variable;

        auto arg0Ty = arg0->type->self<UnionType>();
        auto arg1Ty = arg1->type->self<UnionType>();

        if (!arg0Ty->isSameType(*arg1Ty)) {
          throw ::std::runtime_error("Trying to compare different kinds of unions");
        }

        auto d = arg0Ty->discriminant;
        // TODO: introduce a discriminant index function so we don't have to compare in
        // discriminants by value
        auto d0 = b.declareLocalValue(ir::GetDiscriminant::create(d.type, arg0));
        auto d1 = b.declareLocalValue(ir::GetDiscriminant::create(d.type, arg1));
        auto eq = doOperator(b, OperatorExpression::OP_EQ, { d0, d1 });
        auto neq = doOperator(b, OperatorExpression::OP_NOT, { eq });
        b.appendIf(neq, [&](StatementBuilder &sb) {
          sb.appendReturn(eq); // EQ will be false here!
          });

        // check all but the first
        for (size_t i = 1; i < arg0Ty->members.size(); ++i) {
          auto m = arg0Ty->members.at(i);
          auto dm = b.declareLocalValue(m.discriminant);
          eq = doOperator(b, OperatorExpression::OP_EQ, { d0, dm });
          b.appendIf(eq, [&](StatementBuilder &sb) {
            // TODO: since we're safely accessing the current member, we don't need to
            // get an optional!!
              auto e0 = b.declareLocalValue(ir::GetMember::create(m.type,arg0,m.name));
              auto e1 = b.declareLocalValue(ir::GetMember::create(m.type,arg1,m.name));
              eq = doOperator(b, OperatorExpression::OP_EQ, {e0,e1});
              sb.appendReturn(eq); // EQ will be false here!
            });
        }
        // now, we no that the first value in the union is set
        auto m = arg0Ty->members.at(0);
        auto e0 = b.declareLocalValue(ir::GetMember::create(m.type, arg0, m.name));
        auto e1 = b.declareLocalValue(ir::GetMember::create(m.type, arg1, m.name));
        eq = doOperator(b, OperatorExpression::OP_EQ, { e0, e1 });

        b.appendReturn(eq);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_STRINGIFY_BIT(EType retTy,
          const LambdaParameters &params)
      {
        StatementBuilder b;
        auto bit = b.declareLocalValue(LiteralBit::create(true));
        auto eq = doOperator(b, OperatorExpression::OP_EQ, { bit, params.at(0)->variable });
        b.appendIf(eq, [&](StatementBuilder &sb) {
          auto str = sb.declareLocalValue(LiteralString::create("1"));
          auto casted = doCheckedCast(sb,retTy,str);
          sb.appendReturn(casted);
        }, [&](StatementBuilder &sb) {
          auto str = sb.declareLocalValue(LiteralString::create("0"));
          auto casted = doCheckedCast(sb,retTy,str);
          sb.appendReturn(casted);
        });
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_STRINGIFY_BOOLEAN(EType retTy,
          const LambdaParameters &params)
      {
        StatementBuilder b;
        auto bit = b.declareLocalValue(LiteralBit::create(true));
        auto eq = doOperator(b, OperatorExpression::OP_EQ, { bit, params.at(0)->variable });
        b.appendIf(eq, [&](StatementBuilder &sb) {
          auto str = sb.declareLocalValue(LiteralString::create("true"));
          auto casted = doCheckedCast(sb,retTy,str);
          sb.appendReturn(casted);
        }, [&](StatementBuilder &sb) {
          auto str = sb.declareLocalValue(LiteralString::create("false"));
          auto casted = doCheckedCast(sb,retTy,str);
          sb.appendReturn(casted);
        });
        return ir::LambdaExpression::create(params, retTy, b.build());
      }
      ir::Expression::ExpressionPtr RewriteBuiltins::replace_STRINGIFY_CHAR(EType retTy,
          const Arguments &args)
      {
        return OperatorExpression::create(retTy, OperatorExpression::OP_NEW, args.at(0));
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_STRINGIFY_STRING(EType,
          const Arguments &args)
      {
        return args.at(0);
      }
      ir::Expression::ExpressionPtr RewriteBuiltins::replace_STRINGIFY_OPT(EType retTy,
          const LambdaParameters &params)
      {
        StatementBuilder b;
        auto opt = params.at(0)->variable;
        auto flat = doOperator(b, OperatorExpression::OP_FLATTEN, { opt });
        auto present = doIsPresent(b, flat);
        b.appendIf(present, [&](StatementBuilder &sb) {
          auto v = doGetOpt(sb,flat);
          auto str = doOperator(sb,retTy,OperatorExpression::OP_TO_STRING, {v});
          sb.appendReturn(str);
        }, [&](StatementBuilder &sb) {
          auto str = sb.declareLocalValue(LiteralString::createEmpty());
          auto casted = doCheckedCast(sb,retTy,str);
          sb.appendReturn(casted);
        });
        return ir::LambdaExpression::create(params, retTy, b.build());

      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_UNCHECKEDCAST_INTEGER_TO_INTEGER(
          EType retTy, const LambdaParameters &params)
      {
        StatementBuilder b;
        auto outTy = retTy->self<IntegerType>();
        auto inVar = params.at(0)->variable;
        auto inTy = inVar->type->self<IntegerType>();

        Variable::VariablePtr casted;
        if (!outTy->range.contains(inTy->range)) {
          // perform a bounds check
          if (outTy->range.min().isFinite()) {
            auto min = b.declareLocalValue(LiteralInteger::create(*outTy->range.min()));
            auto cond = doOperator(b, OperatorExpression::OP_LT, { inVar, min });
            b.appendIf(cond, [&](StatementBuilder &sb) {
              sb.appendAbort("integer value is too small");
            });
          }

          if (outTy->range.max().isFinite()) {
            auto max = b.declareLocalValue(LiteralInteger::create(outTy, *outTy->range.max()));
            auto cond = doOperator(b, OperatorExpression::OP_LT, { max, inVar });
            b.appendIf(cond, [&](StatementBuilder &sb) {
              sb.appendAbort("integer value is too large");
            });
          }
        }

        auto res = doCheckedCast(b, outTy, inVar);
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_UNCHECKEDCAST_ARRAY(EType retTy,
          const LambdaParameters &params)
      {
        StatementBuilder b;
        auto outTy = retTy->self<ArrayType>();
        auto inVar = params.at(0)->variable;
        auto inTy = inVar->type->self<ArrayType>();

        // perform a bounds check
        if (!outTy->bounds.contains(inTy->bounds)) {
          auto len = doArrayLen(b, inVar);
          if (inTy->bounds.min() < outTy->bounds.min()) {
            auto min = b.declareLocalValue(
                LiteralInteger::create(outTy->counterType, *outTy->bounds.min()));
            auto cond = doOperator(b, OperatorExpression::OP_LT, { len, min });
            b.appendIf(cond, [&](StatementBuilder &sb) {
              sb.appendAbort("array-length is too small");
            });
          }
          if (inTy->bounds.max() > outTy->bounds.max()) {
            auto max = b.declareLocalValue(
                LiteralInteger::create(outTy->counterType, *outTy->bounds.max()));
            auto cond = doOperator(b, OperatorExpression::OP_LT, { max, len });
            b.appendIf(cond, [&](StatementBuilder &sb) {
              sb.appendAbort("array-length is too large");
            });
          }
        }
        auto res = inVar;
        if (!inTy->element->isSameType(*outTy->element)) {
          res = doMapArray(b, inVar, [&](StatementBuilder &sb, Variable::VariablePtr v) {
            return doUncheckedCast(sb,outTy->element,v);
          });
        }
        auto casted = doCheckedCast(b, outTy, res);
        b.appendReturn(casted);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_UNCHECKEDCAST_OPT(EType retTy,
          const LambdaParameters &params)
      {
        StatementBuilder b;
        auto outTy = retTy->self<OptType>();
        auto inVar = params.at(0)->variable;
        auto inTy = inVar->type->self<OptType>();

        auto res = doMapOpt(b, outTy->element, inVar,
            [&](StatementBuilder &sb, Variable::VariablePtr v) {
              return doUncheckedCast(sb,outTy->element,v);
            });
        auto casted = doCheckedCast(b, outTy, res);
        b.appendReturn(casted);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

#if USE_LOOPS==0
      ir::Expression::ExpressionPtr RewriteBuiltins::replace_CHECKEDCAST_ARRAY(EType retTy,
          const LambdaParameters &params)
      {
        StatementBuilder b;
        auto outTy = retTy->self<ArrayType>();
        auto inVar = params.at(0)->variable;
        auto inTy = inVar->type->self<ArrayType>();

        if (inTy->element->isSameType(*outTy->element)) {
          return nullptr;
        }

        auto res = doMapArray(b,inVar,
            [&](StatementBuilder &sb, Variable::VariablePtr v) {
              return doCheckedCast(sb,outTy->element,v);
            });
        auto casted = doCheckedCast(b, outTy, res);
        b.appendReturn(casted);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }
#endif

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_CHECKEDCAST_OPT(EType retTy,
          const LambdaParameters &params)
      {
        StatementBuilder b;
        auto outTy = retTy->self<OptType>();
        auto inVar = params.at(0)->variable;
        auto inTy = inVar->type->self<OptType>();
        if (inTy->element->isSameType(*outTy->element)) {
          return nullptr;
        }

        auto res = doMapOpt(b, outTy->element, inVar,
            [&](StatementBuilder &sb, Variable::VariablePtr v) {
              return doCheckedCast(sb,outTy->element,v);
            });
        auto casted = doCheckedCast(b, outTy, res);
        b.appendReturn(casted);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_UNCHECKEDCAST_TUPLE(EType retTy,
          const LambdaParameters &params)
      {
        StatementBuilder b;

        auto inVar = params.at(0)->variable;
        auto inTy = inVar->type->self<TupleType>();
        auto outTy = retTy->self<TupleType>();

        ::std::vector<Variable::VariablePtr> vars;
        for (size_t i = 0; i < inTy->types.size(); ++i) {
          auto e = b.declareLocalValue(ir::GetMember::create(inTy->types.at(i), inVar, i));
          auto casted = doUncheckedCast(b, outTy->types.at(i), e);
          vars.push_back(casted);
        }
        auto res = doOperator(b, retTy, OperatorExpression::OP_NEW, vars);
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_CHECKEDCAST_TUPLE(EType retTy,
          const LambdaParameters &params)
      {
        StatementBuilder b;

        auto inVar = params.at(0)->variable;
        auto inTy = inVar->type->self<TupleType>();
        auto outTy = retTy->self<TupleType>();

        ::std::vector<Variable::VariablePtr> vars;
        for (size_t i = 0; i < inTy->types.size(); ++i) {
          auto e = b.declareLocalValue(ir::GetMember::create(inTy->types.at(i), inVar, i));
          auto casted = doCheckedCast(b, outTy->types.at(i), e);
          vars.push_back(casted);
        }
        auto res = doOperator(b, retTy, OperatorExpression::OP_NEW, vars);
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_UNCHECKEDCAST_STRUCT(EType retTy,
          const LambdaParameters &params)
      {
        StatementBuilder b;

        auto inVar = params.at(0)->variable;
        auto inTy = inVar->type->self<StructType>();
        auto outTy = retTy->self<StructType>();

        ::std::vector<Variable::VariablePtr> vars;
        vars.resize(inTy->members.size());
        for (auto m : inTy->members) {
          auto e = b.declareLocalValue(ir::GetMember::create(m.type, inVar, m.name));
          auto i = outTy->indexOfMember(m.name);
          auto casted = doUncheckedCast(b, outTy->getMemberType(m.name), e);
          vars.at(i) = casted;
        }
        auto res = doOperator(b, retTy, OperatorExpression::OP_NEW, vars);
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_CHECKEDCAST_STRUCT(EType retTy,
          const LambdaParameters &params)
      {
        StatementBuilder b;

        auto inVar = params.at(0)->variable;
        auto inTy = inVar->type->self<StructType>();
        auto outTy = retTy->self<StructType>();

        ::std::vector<Variable::VariablePtr> vars;
        vars.resize(inTy->members.size());
        for (auto m : inTy->members) {
          auto e = b.declareLocalValue(ir::GetMember::create(m.type, inVar, m.name));
          auto casted = doCheckedCast(b, outTy->getMemberType(m.name), e);
          auto i = outTy->indexOfMember(m.name);
          vars.at(i) = casted;
        }
        auto res = doOperator(b, retTy, OperatorExpression::OP_NEW, vars);
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_UNCHECKEDCAST_FUNCTION(EType retTy,
          const LambdaParameters &params)
      {
        StatementBuilder b;

        auto inVar = params.at(0)->variable;
        auto inTy = inVar->type->self<FunctionType>();
        auto outTy = retTy->self<FunctionType>();

        auto res = b.declareLocalLambda(outTy->parameters, [&](LambdaParameters p) {
          StatementBuilder sb;
          ::std::vector<Variable::VariablePtr> args;
          for (size_t i=0;i<p.size();++i) {
            auto arg = doUncheckedCast(sb,inTy->parameters.at(i),p.at(i)->variable);
            args.push_back(arg);
          }
          auto tmp = doCall(sb, inVar, args);
          auto casted = doUncheckedCast(sb,outTy->returnType,tmp);
          sb.appendReturn(casted);
          return ir::LambdaExpression::create(p, outTy->returnType, sb.build());
        });
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_CHECKEDCAST_FUNCTION(EType retTy,
          const LambdaParameters &params)
      {
        StatementBuilder b;

        auto inVar = params.at(0)->variable;
        auto inTy = inVar->type->self<FunctionType>();
        auto outTy = retTy->self<FunctionType>();

        auto res = b.declareLocalLambda(outTy->parameters, [&](LambdaParameters p) {
          StatementBuilder sb;
          ::std::vector<Variable::VariablePtr> args;
          for (size_t i=0;i<p.size();++i) {
            auto arg = doCheckedCast(sb,inTy->parameters.at(i),p.at(i)->variable);
            args.push_back(arg);
          }
          auto tmp = doCall(sb, inVar, args);
          auto casted = doCheckedCast(sb,outTy->returnType,tmp);
          sb.appendReturn(casted);
          return ir::LambdaExpression::create(p, outTy->returnType, sb.build());
        });
        b.appendReturn(res);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_CHECKEDCAST_NAMEDTYPE(EType retTy,
          const LambdaParameters &params)
      {
        StatementBuilder b;

        auto inVar = params.at(0)->variable;
        auto inTy = inVar->type->self<NamedType>();

        auto asBase = doOperator(b, OperatorExpression::OP_BASETYPE_CAST, { inVar });
        auto casted = doCheckedCast(b, retTy, asBase);
        b.appendReturn(casted);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

      ir::Expression::ExpressionPtr RewriteBuiltins::replace_UNCHECKEDCAST_NAMEDTYPE(EType retTy,
          const LambdaParameters &params)
      {
        StatementBuilder b;

        auto inVar = params.at(0)->variable;
        auto inTy = inVar->type->self<NamedType>();

        auto asBase = doOperator(b, OperatorExpression::OP_BASETYPE_CAST, { inVar });
        auto casted = doUncheckedCast(b, retTy, asBase);
        b.appendReturn(casted);
        return ir::LambdaExpression::create(params, retTy, b.build());
      }

    }
  }
}
