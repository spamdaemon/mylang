#ifndef CLASS_MYLANG_ETHIR_TRANSFORMS_SINGLERETURN_H
#define CLASS_MYLANG_ETHIR_TRANSFORMS_SINGLERETURN_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

namespace mylang {
  namespace ethir {
    namespace transforms {

      /**
       * Copy a node and rename all names that introduced with the scope
       * of the node. Names external to the node are not renamed.
       */
      class SingleReturn
      {
        /** Destructor */
      private:
        ~SingleReturn() = delete;

        /**
         * Normalize a lambda, such that there is a single return statement in the body of the lambda.
         * The returned lambda will NOT be in SSA form.
         * @param lambda a lambda
         * @return a new lambda or nullptr if no changge
         */
      public:
        static ELambda normalize(ELambda src);

      };
    }
  }
}
#endif
