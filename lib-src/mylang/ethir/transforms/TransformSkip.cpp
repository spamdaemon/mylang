#include <mylang/ethir/transforms/TransformSkip.h>
#include <mylang/ethir/Transform.h>
#include <mylang/ethir/ir/nodes.h>
#include <cassert>

namespace mylang {
  namespace ethir {
    using namespace ir;
    using namespace types;
    namespace transforms {

      TransformResult<ENode> TransformSkip::replaceCurrentState(ENode root)
      {
        struct T: public Transform
        {
          T()
              : current(Loop::STOP_STATE) // just need some value
          {
          }
          ~T()
          {
          }

          ENode visit(const Loop::LoopPtr &loop) override final
          {
            Loop::States states;
            bool isNew = false;
            ir::Loop::State s = current;
            for (auto [cs, step] : loop->states) {
              current = cs;
              auto t = transformNode(step);
              isNew = isNew || t.isNew;
              states.insert( { cs, t.actual->self<ir::Loop::Step>() });
            }
            current = s;
            if (isNew) {
              return Loop::create(loop->start, ::std::move(states));
            }
            return nullptr;
          }

          ENode visit(const SkipStatement::SkipStatementPtr &node) override final
          {
            assert(current != Loop::STOP_STATE);
            if (node->nextState == Loop::CURRENT_STATE) {
              auto u = transformExpressions(node->updates);
              return SkipStatement::create(u.actual, current, node->resetNextState);
            }
            return nullptr;
          }

          ir::Loop::State current;
        };
        T tx;
        return tx.transformNode(root);
      }

      TransformResult<ENode> TransformSkip::replaceNextState(ENode root, ReplaceStateFN replacement)
      {
        struct T: public Transform
        {
          T(ReplaceStateFN fn)
              : replace(fn)
          {
          }
          ~T()
          {
          }

          ENode visit(const Loop::LoopPtr&) override final
          {
            return nullptr;
          }

          ENode visit(const SkipStatement::SkipStatementPtr &skip) override final
          {
            auto s = replace(skip->nextState);
            auto u = transformExpressions(skip->updates);
            if (u.isNew || s != skip->nextState) {
              return SkipStatement::create(u.actual, s, skip->resetNextState);
            }
            return nullptr;
          }

          ReplaceStateFN replace;
        };
        T tx(replacement);
        return tx.transformNode(root);
      }

      TransformResult<ENode> TransformSkip::replaceSkip(ENode root, ReplaceSkipFN replacement)
      {
        struct T: public Transform
        {
          T(ReplaceSkipFN fn)
              : replace(fn)
          {
          }
          ~T()
          {
          }
          ENode visit(const Loop::LoopPtr&) override final
          {
            return nullptr;
          }

          ENode visit(const SkipStatement::SkipStatementPtr &skip) override final
          {
            return replace(skip);
          }

          ReplaceSkipFN replace;
        };
        T tx(replacement);
        return tx.transformNode(root);
      }

      TransformResult<ir::Loop::States> TransformSkip::replaceSkips(const ir::Loop::States &states,
          ReplaceSkipFN replacement)
      {
        ir::Loop::States res;
        bool isNew = false;
        for (auto s : states) {
          auto t = replaceSkip(s.second, replacement);
          isNew = isNew || t.isNew;
          res.insert( { s.first, t.actual->self<ir::Loop::Step>() });
        }
        return {::std::move(res),isNew};
      }

      TransformResult<ir::Loop::States> TransformSkip::replaceNextStates(
          const ir::Loop::States &states, ReplaceStateFN replacement)
      {
        ir::Loop::States res;
        bool isNew = false;
        for (auto s : states) {
          auto t = replaceNextState(s.second, replacement);
          isNew = isNew || t.isNew;
          res.insert( { s.first, t.actual->self<ir::Loop::Step>() });
        }
        return {::std::move(res),isNew};
      }

    }
  }
}
