#ifndef CLASS_MYLANG_ETHIR_TRANSFORMRESULT_H
#define CLASS_MYLANG_ETHIR_TRANSFORMRESULT_H

namespace mylang {
  namespace ethir {

    template<class E>
    struct TransformResult
    {
      TransformResult(E xactual, bool xtransformed)
          : actual(xactual), isNew(xtransformed)
      {
      }

      E actual;
      bool isNew;
    };

  }
}
#endif
