#ifndef CLASS_MYLANG_ETHIR_TAG_H
#define CLASS_MYLANG_ETHIR_TAG_H

#ifndef CLASS_MYLANG_NAMES_NAME_H
#include <mylang/names/Name.h>
#endif

namespace mylang {
  namespace ethir {

    /* TODO: what's this for */
    class Tag
    {

      /** Create a tag for a global tag */
    private:
      Tag(mylang::names::Name::Ptr tag);

    public:
      Tag(const Tag &tag);
      Tag& operator=(const Tag &tag);
      ~Tag();

      /**
       * Create a new tag from a global tag.
       * @param src the source tag
       */
    public:
      static Tag create(mylang::names::Name::Ptr src);

      /**
       * Create a temporary tag.
       * @param prefix an optional prefix
       */
    public:
      static Tag create(const ::std::string &prefix);

      /**
       * Compare for equality.
       * @param tag a tag
       * @return true if equal, false otherwise
       */
    public:
      bool equals(const Tag &tag) const;

      /**
       * Compare for less than.
       * @param tag a tag
       * @return true if this tag is less than the specified tag.
       */
    public:
      bool less(const Tag &tag) const;

      /**
       * Compare this tag to the specified tag.
       * @param tag a tag
       * @return -1 if this is  < , 0 if equal, 1 if greater
       */
    public:
      int compare(const Tag &tag) const;

      friend inline bool operator<(const Tag &a, const Tag &b)
      {
        return a.less(b);
      }

      friend inline bool operator==(const Tag &a, const Tag &b)
      {
        return a.equals(b);
      }

      friend inline bool operator!=(const Tag &a, const Tag &b)
      {
        return !a.equals(b);
      }

      friend inline ::std::ostream& operator<<(::std::ostream &out, const Tag &tag)
      {
        return out << tag._source;
      }

    public:
      ::std::string toString() const;

    public:
      inline const mylang::names::Name::Ptr& source() const
      {
        return _source;
      }

      /** The globally unique tag on which this tag is based */
    private:
      mylang::names::Name::Ptr _source;
    };

  }
}
#endif
