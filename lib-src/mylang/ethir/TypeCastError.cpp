#include <mylang/ethir/TypeCastError.h>
#include <mylang/ethir/types/types.h>

#include <string>

namespace mylang {
  namespace ethir {

    static ::std::string make_message(const EType &from, const EType &to)
    {
      ::std::string msg;
      msg += "Cannot cast from ";
      msg += from->toString();
      msg += " to ";
      msg += to->toString();
      return msg;
    }

    TypeCastError::TypeCastError(const EType &xfrom, const EType &xto)
        : ::std::runtime_error(make_message(xfrom, xto)), from(xfrom), to(xto)
    {
    }

    TypeCastError::~TypeCastError()
    {
    }

    void TypeCastError::checkSafeCast(const EType &xfrom, const EType &xto)
    {
      if (!xto->canSafeCastFrom(*xfrom)) {
        throw TypeCastError(xfrom, xto);
      }
    }

    void TypeCastError::checkCast(const EType &xfrom, const EType &xto)
    {
      if (!xto->canCastFrom(*xfrom)) {
        throw TypeCastError(xfrom, xto);
      }
    }

  }
}
