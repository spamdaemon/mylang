#ifndef CLASS_MYLANG_ETHIR_TYPEMISMATCHERROR_H
#define CLASS_MYLANG_ETHIR_TYPEMISMATCHERROR_H

#ifndef FILE_MYLANG_ETHIR_H
#include <mylang/ethir/ethir.h>
#endif

#include <stdexcept>

namespace mylang {
  namespace ethir {

    /**
     * A type mismatch.
     */
    class TypeMismatchError: public ::std::runtime_error
    {
      /**
       * Create a type mismatch error.
       * @param expected the expected type
       * @pram actual the actual type
       */
    public:
      TypeMismatchError(const EType &expected, const EType &actual);

      /** Destructor */
    public:
      virtual ~TypeMismatchError();

      /**
       * Perform a type check and throw this error on mismatch.
       * @param expected an expected type
       * @param actuak the actual type
       */
    public:
      static void check(const EType &expected, const EType &actual);

      /** The type that was expected */
    public:
      const EType expected;

      /** The actual type */
    public:
      const EType actual;
    };
  }
}
#endif
