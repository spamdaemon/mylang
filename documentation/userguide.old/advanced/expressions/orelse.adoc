[#orelse]
==== Optify Expression

The or-else operator `?:` is variant of the <<deoptify,deoptify>> operator and will 
cause an error if the optional is not set.

===== Syntax
The general syntax is `<optional-expr> ?: <else-expr>`.

The operator is syntactic sugar for the following expression:

[source,mylang]
----
with
 a = <optional-expr>;
do
 a.present ? *a : <else-expr>
----

The else epxression is not evaluated if the optional value is present.
The operator is also right-associative

===== Return Type
The return type is that of the else expression.


===== Examples

This simple example shows that else expression is never evaluated and thus
does not cause a runtime error:

[source,mylang]
----
include::examples/orelse_1.txt[]
----

This example shows nesting of the or-else expressions:

[source,mylang]
----
include::examples/orelse_2.txt[]
----

