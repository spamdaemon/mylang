[#index_op]
==== Index Operator

The index operator is used to access individual characters in a string.


===== Syntax

The syntax `<string-expr>[<integer-expr>]` yields a character. `string-expr` must be a string, and `integer-expr` must have integer type.

#TODO: implement substring support as well#

[CAUTION]
====
If this index is negative or greater-than-or-equal to the string's length, then a runtime 
error occurs.
====

===== Return Type
The result of the index operator is always a <<char,char>>.

===== Examples

This yields the character `W`:
[source,mylang]
----
include::examples/index_op_1.txt[]
----

This example would cause a runtime exception:
[source,mylang]
----
include::examples/index_op_2.txt[]
----
