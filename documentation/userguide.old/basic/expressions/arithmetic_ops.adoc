[#arithmetic_ops]
==== Arithmetic Expressions

<<integer,Integers>> and <<real,real numbers>> support the fundamental arithmetic 
operators `add`,`subtract`, `multiply`,`divide`, `modulus`, `remainder`, and `negate`.


===== Syntax

[options="header"]
|===
|Operator |Syntax
|negate   | `- <expr>`
|multiply | `<expr> * <expr>`
|divide   | `<expr> / <expr>`
|modulus  | `<expr> % <expr>`
|remainder| `<expr> %% <expr>`
|add      | `<expr> + <expr>`
|subtract | `<expr> - <expr>`
|===

===== Return Type
The return type of an arithmetic expression is the same as operands.

===== Precedence

Precedence is the standard precedence. The precedence of the operators highest to lowest is
negate, multiply&divide&modulus&remainder, and add & subtract.
Were ambiguous, precedence is left-factored. For  example, `4/2*2` yields `4`.

===== Division
Division by `0` is not allowed and will cause a runtime error. 

[CAUTION]
====
If the compiler can determine that a function will execute a division by `0` then it is allowed to 
emit a compiler time error.
====

===== Modulus
Modulus is not defined for real numbers and the remainder operation should be used instead.
Modulus by `0` or a negative number is not allowed and will cause a runtime error. In the expression `a % b`, the result will
always be in the range `0 to b-1` regardless of the sign of `a`.

[CAUTION]
====
If the compiler can determine that a function will execute a modulus operation which would cause a 
runtime error, then it is allowed to emit a compile time error.
====

===== Remainder
Remainder is similar to modules, but works with negative numbers and may produce negative numbers.
Remainder by `0` is of course undefined and will cause a runtime error. 

[CAUTION]
====
If the compiler can determine that a function will execute a remainder operation which would cause a 
runtime error, then it is allowed to emit a compile time error.
====


===== Examples

This simple expression yields the value 7:
[source,mylang]
----
include::examples/arithmetic_ops_1.txt[]
----

As equal precendeces are left-factored, the following yields 4:
[source,mylang]
----
include::examples/arithmetic_ops_2.txt[]
----

Use parentheses to group expressions:
[source,mylang]
----
include::examples/arithmetic_ops_3.txt[]
----
yields 1.0.

This example will cause a runtime error due to a modulus by 0:
[source,mylang]
----
include::examples/arithmetic_ops_4.txt[]
----

This example yields `-2` number due to the divisior being negative:
[source,mylang]
----
include::examples/arithmetic_ops_5.txt[]
----


