[#cast]
==== Cast Expressions

Sometimes it is necessary to change the type of an expression to another type. This can be accomplished via typecasts.
There are two types of typecasts, `safe_cast` and `unsafe_cast` expressions. A `safe_cast` can be used when the type
of an expression can safely be changed into another type without any side-effects, loss of precision, etc. On the other
hand, an `unsafe_cast` must be used if a typechange could possibly cause an error or lead to a loss of data.

NOTE: ```safe_cast``` is normally not needed, as the compiler will add such casts where necessary.

===== Syntax
`safe_cast< <totype> >( <expr> )`

`unsafe_cast< <totype> > ( <expr> )`

===== Examples

The following casts an integer to a real number, which is always safe, because
the a real number can also represent any integer:

[source,mylang]
----
include::examples/cast_1.txt[]
----


