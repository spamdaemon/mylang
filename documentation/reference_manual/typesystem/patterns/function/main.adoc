[#typesystem_patterns_function]
= Function type
(((patterns, function)))

The function pattern is the dual to the function type.

== Syntax
`"(" {params:pattern\...} ")" ":" {return:pattern}`

