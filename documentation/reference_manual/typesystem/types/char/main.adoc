[#typesystem_types_char]
= Character Type
(((types, char)))

The character type represents text characters. The full range of unicode characters can be expected to be available.

== Syntax
`"char"`

== Literals
A character literal is a single character enclosed in single quotes. If the character is special character, or is not available, then TBD escape sequence can be used to represent any unicode character.

== Operators
No special operators exist for characters.


== Constraints
N/A

== Ordering
Characters are ordered by their unicode value, and so smaller unicode values compare as true against larger ones.

