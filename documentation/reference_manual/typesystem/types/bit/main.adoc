[#typesystem_types_bit]
= Bit Type
(((types, bit)))

The `bit` type is used to represent a single bit. By themselves, single bits are not very useful, but they can used to define
more complex types when used in a composite datatypes, such as an array.

== Syntax
`"bit"`

== Literals
The literal `0b0` represents the bit value 0, and `0b1` represents bit value 1.


== Operators
[options="header"]
|===
|Operator|Unary/Binary|Description
|&&      | Binary     |Compute the AND of two bits. If the left value is 0, the the right value is ignored
| \|\|   | Binary     |Computes the OR of two bits. If the left value is 1, the the right value is ignored
|&       | Binary     |Compute the AND of two bits.  
|\|      | Binary     |Compute the OR of two bits.  
|^       | Binary     |Compute the XOR of two bits.
|!       | Unary      |Compute the NOT of a bit. 
|===

== Constraints
N/A

== Ordering
`0b0 < 0b1` always compares true.
