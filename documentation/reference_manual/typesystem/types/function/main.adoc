[#typesystem_types_function]
= Function type
(((types, function)))

Function types represent the signature of a function or <<lambda,lambda function>>. 
A signature consists of the return type, and the list of argument types.

== Syntax
`"(" {params:type\...} ")" ":" {return:type}`

== Literals
N/A

== Operators
[options="header"]
|===
|Operator|Unary/Binary|Description
|()      | Binary     |Applies the function defined by the left-hand-side to the arguments.
|===


== Constraints
N/A

== Ordering
Ordering is implementation defined. If ordering is used then a conforming parser must issue a portability warning.
