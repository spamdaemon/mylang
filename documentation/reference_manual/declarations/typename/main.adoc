[#declaration_typename]
= Typename Declaration
(((declarations, typename)))

The typename statement provide a more convenient way to refer to
a type by introducing an alias. Using the typename is equivalent
to using the aliased type (and vice versa).
Since the typename statement is syntactic sugar it has no linkage, which
means it can only be used within the scope of a function.

[CAUTION]
==
It is not possible to refer to the alias using a fully qualified name.
==

== Syntax

`"typename" {id} "=" {type}";`

== Side Effects
None

== Examples

The following examples defines an 8-bit integer as a uint8:

[source,mylang]
----
typename uint8 = integer{min:0;max:255};
defun x(v:uint8) = v;
----
