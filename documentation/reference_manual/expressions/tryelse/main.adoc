[#expression_tryelse]
= Try-Else Expression
((( expressions, try-else )))

The `try-else` expression prevents the bubbling up of unexpected errors, 
such as a divide by zero or failure to evaluate functions exhibiting the <<sideeffect_partial,Partial side-effect>>.

[CAUTION]
==
If the compiler can determine that the try expression always fails, or always succeeds, then it is allowed 
to reject the code.
==

== Syntax
The general syntax is 
`"try" {try:expr} "else" {fail:expr}`

== Value Type
The type of this expression is the union of the types of the try and fail blocks.

== Side Effects
Any side-effects associated with the fail expression. The side-effects of the try-expression
are suppressed.

== Examples

This example shows how the or-else expression could be implemented in terms of try-else:

[source,mylang]
----
defun or_else(x:integer?,y:integer) = try *x else y;
defun main() = or_else(nil,0);
----
