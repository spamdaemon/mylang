[#expression_optify]
= Optify Expression
((( expressions, optify )))

Turn a value into an optional that is set.

== Syntax

`"&" {expr}`

[CAUTION]
==
An empty array `[]` or `nil` cannot be optified.
==

== Value Type
The return type is an optional whose element is that of the operand.

== Side Effects
None

== Examples

This simple example optifies a literal

[source,mylang]
----
defmacro optify(x) = &x;
defun get_optional(x:integer) = optify(x);
----

