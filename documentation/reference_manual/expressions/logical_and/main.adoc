[#expression_logical_and]
= Logical And Expressions
((( expressions, logical, and )))

Compute the logical AND of two boolean values. There are two flavors of AND expressions, a 
short-circuiting and a non-short-circuiting AND. The short-circuit AND will never evaluate
the second value, if the first value is `false`.

== Syntax

`{lhs:expr:boolean} "&" {rhs:expr:boolean}`

The short-circuiting AND:
`{lhs:expr:boolean} "&&" {rhs:expr:boolean}`


== Value Type
The type of this expression is a boolean.

== Side Effects
None

== Examples

Use a short-circuit AND to prevent evaluation of the second argument
in case the array has zero-size:
[source,mylang]
----
defun check(a:integer[]) =  a.length>0 && a[0]>0;
defun main() = check([]);
----

The following example may throw an exception at runtime, depending on the length of 
the array.
[source,mylang]
----
defun check(a:integer[]) =  a.length>0 & a[0]>0;
defun main() = check([]);
----
