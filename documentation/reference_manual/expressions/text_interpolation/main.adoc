[#expression_interpolate]
= Text Interpolation
((( expressions, interpolate )))

It is possible to use define multiline strings with embedded expressions. 
This commonly referred to as text interpolation. An embeeded expression must be surrounded by
a pair of `{` and `}`. Escape sequences are generally not recognized, with the exception of these
sequence:

`\{`:: escapes the `{` character
`\'`:: escapes the `'` character, which is only required in some circumstances
`\\`:: escapes the `\` character, but that is only required in some circumstance. If generated text must contain two `\\`, then `\\\\` must be used.
`\%`:: escapes the `%` character
`\$`:: escapes the `$` character

NOTE: the characters `%` and `$` are reserved and should be escaped.

== Syntax

The basic syntax is `"''" ({text}| ("{" {expr} "}")* "''"`.

== Value Type
The type of this expression is always a string, which is the concatenation of the literals and expressions. 

== Side Effects
None

== Examples

A simple example produce the text ``Hello, World!``:
[source,mylang]
----
defun greet (user:string) = ''Hello, {user}!'';
defun say_hello() = greet("World");
----

This example shows that character do not have to be escaped:
[source,mylang]
----
defun intro (user:string) = 
''
Hello, {{user}}!

How may I be of assistance?
'';

defun say_hello(argv:string[]) = intro(argv.length==0 ? "unknown-user" : argv[0]);
----

An example showing the use of escape sequences. The brace is always escaped, but in this example, the
the backslash can be used as.
[source,mylang]
----
defun gen_main() = 
''
#include <stdio.h>
int main(int argc, const char** argv)
\{
    printf("Hello, World!\n");
    return 0;
}
'';
----

