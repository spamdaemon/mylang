[#expression_unsafecast]
= Unsafe Cast Expressions
((( expressions, cast, unsafe )))

This expression performs an unsafe cast, which may fail at runtime, or lead to loss of data.

== Syntax
`"unsafe_cast" "<" {to:type} ">" "(" {expr} ")"`


== Value Type
The type of this expression is always the `to` type.

== Side Effects
<<sideeffect_partial,Partial>>

== Examples

The following casts an integer to a real number, which is always safe, because
the a real number can also represent any integer:

[source,mylang]
----
defun f(arr:integer[1:10]) = arr[0];
defun main()
{
  val arr:integer[] = [1];
  return unsafe_cast<integer[1:10]>(arr);
}
----

The following example will lead to a runtime error:
[source,mylang]
----
defun f(arr:integer[1:10]) = arr[0];
defun main()
{
  val arr:integer[] = [];
  return unsafe_cast<integer[1:10]>(arr);
}
----

