[#library_function_drop]
= Drop
((( library,functions, drop )))

Remove N elements from the beginning or the end of an array. If
the number of elements N to be dropped is positive, then the first N
elements are removed, otherwise the last -N elements are removed.

== Signature

drop(array:T[X], N:integer):T[max(0,X-abs(N))] 

== Value Type
The value of the expression is the sum of the two expression and
the value's type bounds the possible values given the input types.

== Side Effects
None

== Reference Implementation
[source,mylang]
----
include::impl.ka[]
----

== Examples

=== Example 1
[source,mylang]
----
[1,2,3,4,5].drop(2);
----
yields the array `[3,4,5]`

=== Example 2
[source,mylang]
----
[1,2,3,4,5].drop(-2);
----
yields the array `[1,2,3]`

=== Example 3
[source,mylang]
----
[1,2,3,4,5].drop(6);
----
yields the array `[]`


