[#library_function_map_optional]
= Map (optional)
((( library,functions,map )))

Apply a function to a non-nil optional to return a new optional. If nil, no mapping is performed.

== Signature

map(opt:T?, f:(e:T):U):U?

== Value Type

N/A

== Side Effects
Any side-effects of the provided mapping function f are also the side-effects of this function.

== Reference Implementation
[source,mylang]
----
include::impl.ka[]
----


== Examples

=== Example 1
[source,mylang]
----
nil.map((x:integer} { return x*2; });
----
yields the value `nil`

=== Example 1
[source,mylang]
----
&1.map((x:integer} { return x*2; });
----
yields the optional `&2`

