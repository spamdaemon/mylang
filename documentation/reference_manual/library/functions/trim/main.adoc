[#library_function_trim]
= Trim
((( library,functions,trim )))

Return an array that has been trimmed to the specified size. If the size of the array
is already at or below the threshold, then this is a no-op. 

== Signature

trim(array:T[X], N:integer{min:0}):T[min(X,N)]

== Value Type

N/A

== Side Effects
None

== Reference Implementation
[source,mylang]
----
include::impl.ka[]
----


== Examples

=== Example 1
[source,mylang]
----
[1,2,3,4,5].trim(2);
----
yields the array `[1,2]`

=== Example 2
[source,mylang]
----
[1,2,3,4,5].trim(6);
----
yields the array `[1,2,3,4,5]`

== See Also 
* <<library_function_take,take>>
* <<library_function_pad,pad>> 

