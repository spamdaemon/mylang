[#library_function_flatten_optional]
= Flatten (optional)
((( library,functions,flatten )))

Remove all but the last level of nesting from a nested optional.

== Signature

flatten(opt:T??):T?

== Value Type

N/A

== Side Effects
None

== Reference Implementation
[source,mylang]
----
include::impl.ka[]
----


== Examples

== Example 1
[source,mylang]
----
&&1.flatten();
----
yields the optional `&1`.

== Example 2
[source,mylang]
----
&nil.flatten();
----
yields `nil`.


